//--[Allegro Version]
//--The GameGear makes extensive use of the event procedures, so it's split up into versions for
//  each of the handlers available for events. This is the Allegro version.
#if defined _ALLEGRO_PROJECT_

//--Base
#include "GameGear.h"

//--Classes
#include "PlayerPony.h"
#include "VisualLevel.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarCamera2D.h"
#include "SugarFont.h"
#include "SugarLinkedList.h"

//--Definitions
#include "DebugDefinitions.h"
#include "Global.h"
#include "Program.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "CameraManager.h"
#include "ControlManager.h"
#include "DebugManager.h"
#include "DisplayManager.h"
#include "EntityManager.h"
#include "MapManager.h"
#include "NetworkManager.h"
#include "TransitionManager.h"
#include "OptionsManager.h"

//--Function forward declarations.
void RenderDebugStrings();
void RenderFPS();
void RenderJoypadDepression();

#include "LoadInterrupt.h"

void GameGear::EventHandler(DriveShaft &sDriveShaft)
{
    //--Event handler, any event other than FORM_TIMER_EXPIRED should be handled here.
    GLOBAL *rGlobal = Global::Shared();
    ALLEGRO_EVENT tEvent = rGlobal->gEvent;
    DebugManager::PushPrint(false, "[Event Handler] Begin %i\n", tEvent.type);

    //--Logic tick is passed to the function pointed at.
    if(tEvent.type == ALLEGRO_EVENT_TIMER)
    {
        if(sDriveShaft.GameTime >= 1.0f) Global::Shared()->gTicksElapsed ++;
        rGlobal->rCurrentLogicHandler(sDriveShaft);
    }
    //--User clicks the big fat X button on their window.
    else if(tEvent.type == ALLEGRO_EVENT_DISPLAY_CLOSE)
    {
        rGlobal->gQuit = true;
    }
    //--Order the ControlManager to update key states on the next pass.
    else if(tEvent.type == ALLEGRO_EVENT_KEY_DOWN)
    {
        al_get_keyboard_state(&sDriveShaft.KeyboardState);
        ControlManager::Fetch()->rKeyboardStatePtr = &sDriveShaft.KeyboardState;
        ControlManager::Fetch()->InformOfPress(tEvent.keyboard.keycode, -1, -1);
    }
    //--Order the ControlManager to update key states on the next pass.
    else if(tEvent.type == ALLEGRO_EVENT_KEY_UP)
    {
        al_get_keyboard_state(&sDriveShaft.KeyboardState);
        ControlManager::Fetch()->SetShiftFlag(tEvent.keyboard.modifiers & ALLEGRO_KEYMOD_SHIFT);
        ControlManager::Fetch()->rKeyboardStatePtr = &sDriveShaft.KeyboardState;
    }
    //--Specifies the position of the mouse to the ControlManager, rather than polling it.
    else if(tEvent.type == ALLEGRO_EVENT_MOUSE_AXES || tEvent.type == ALLEGRO_EVENT_MOUSE_ENTER_DISPLAY)
    {
        ControlManager::Fetch()->SetMouseCoordsByDisplay(tEvent.mouse.x, tEvent.mouse.y, tEvent.mouse.z);
    }
    //--Order the ControlManager to update mouse button states on the next pass.  Likewise, the event
    //  is checked against the GUI.  If it is not caught by the UI, it may be passed down to the
    //  game entities to check mouse position for clicks.
    else if(tEvent.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN)
    {
        //--Controls
        al_get_mouse_state(&sDriveShaft.MouseState);
        ControlManager::Fetch()->rMouseStatePtr = &sDriveShaft.MouseState;
        ControlManager::Fetch()->InformOfPress(-1, tEvent.mouse.button, -1);
    }
    //--Order the ControlManager to update mouse button states on the next pass.  Likewise, the event
    //  is checked against the GUI.  If it is not caught by the UI, it may be passed down to the
    //  game entities to check mouse position for clicks.
    else if(tEvent.type == ALLEGRO_EVENT_MOUSE_BUTTON_UP)
    {
        //--Controls
        al_get_mouse_state(&sDriveShaft.MouseState);
        ControlManager::Fetch()->rMouseStatePtr = &sDriveShaft.MouseState;
    }
    //--Order the ControlManager to update Joystick button states on the next pass.  These event
    //  types are not checked if the gJoystick was never booted.
    else
    {
        if(tEvent.type == ALLEGRO_EVENT_JOYSTICK_BUTTON_DOWN || tEvent.type == ALLEGRO_EVENT_JOYSTICK_BUTTON_UP ||
           tEvent.type == ALLEGRO_EVENT_JOYSTICK_AXIS)
        {
            ControlManager::Fetch()->InformOfPress(-1, -1, tEvent.joystick.button);
        }
    }

    //--Debug
    DebugManager::PopPrint("[Event Handler] Complete\n");
}
void GameGear::LogicHandler(DriveShaft &sDriveShaft)
{
    //--FORM_TIMER_EXPIRED occupies such a large amount of game logic that it gets its own function.
    GLOBAL *rGlobal = Global::Shared();
    DebugManager::PushPrint(false, "[Timer] Begin\n");

    //--Fast-access pointers.
    CameraManager *rCameraManager = rGlobal->gCameraManager;
    ControlManager *rControlManager = rGlobal->gControlManager;
    EntityManager *rEntityManager = rGlobal->gEntityManager;
    MapManager *rMapManager = rGlobal->gMapManager;
    NetworkManager *rNetworkManager = rGlobal->gNetworkManager;
    TransitionManager *rTransitionManager = rGlobal->gTransitionManager;

    //--Network logic goes first.
    DebugManager::Print("Network\n");
    rNetworkManager->Update();

    //--Controls are only actually updated here, instead of during keypress logics.  If no keys
    //  were pressed since the last logic tick, nothing happens.
    DebugManager::Print("Controls\n");
    rControlManager->Update(true, false);
    rControlManager->CheckHotkeys();

    //--If the load interrupt is waiting for keypresses, that handles here.
    LoadInterrupt *rLoadInterrupt = LoadInterrupt::Fetch();
    if(rLoadInterrupt && rLoadInterrupt->IsAwaitingKeypress() && SugarBitmap::xInterruptCall)
    {
        SugarBitmap::xInterruptCall();
        if(rControlManager->IsAnyKeyPressed()) rLoadInterrupt->SetKeypressFlag(false);
        DebugManager::PopPrint("[Timer] Complete by Interrupt\n");
        return;
    }

    DebugManager::Print("Map Manager\n");
    rMapManager->Update();
    if(rGlobal->gQuit) return;

    DebugManager::Print("Transition Manager\n");
    rTransitionManager->Update();

    //--If the TransitionManager requests it, suspend the game state while transitioning.
    if(!rTransitionManager->IsTransitioning())
    {
        //--Special Case: If the player is being healed by an item, do not let the EntityManager update
        //  any of the entities nor the camera so the camera doesn't center on the player as the
        //  healing occurs.
        //--Another special case:  Do not allow these updates if the game is currently on the pause
        //  screen.
        DebugManager::Print("Entity Manager\n");
        rEntityManager->Update();

        DebugManager::Print("Camera Manager\n");
        rCameraManager->Update();
    }
    else
    {
    }

    sDriveShaft.ScreenNeedsUpdate = true;
    DebugManager::PopPrint("[Timer] Complete\n");

    //--Post-tick updates.
    PostLogicHandler();
}
void GameGear::RenderHandler(DriveShaft &sDriveShaft)
{
    //--Rendering handler, exactly what it says on the tin.
    GLOBAL *rGlobal = Global::Shared();

    //--If the load interrupt is waiting for keypresses, no rendering occurs.
    LoadInterrupt *rLoadInterrupt = LoadInterrupt::Fetch();
    if(rLoadInterrupt && rLoadInterrupt->IsAwaitingKeypress())
    {
        return;
    }

    //--Error check
    DebugManager::PushPrint(false, "[GameGear Render Begin]\n");

    //--Only renders if the event queue has no pending events, and at least one logic tick passed.
    if(!sDriveShaft.ScreenNeedsUpdate || !al_is_event_queue_empty(rGlobal->gEventQueue))
    {
        DebugManager::PopPrint("[GameGear Render Unnecessary]\n");
        return;
    }
    DebugManager::Print("Clearing\n");
    sDriveShaft.ScreenNeedsUpdate = false;

    //--Is there a VisualLevel? If so, the DisplayManager does not use hard letterboxes, since widescreen
    //  will render more 3D space.
    DisplayManager::xUseHardLetterbox = (VisualLevel::Fetch() == NULL);

    //--Setup GL
    DisplayManager::LetterboxOffsets();
    DisplayManager::StdOrtho();

    //--Draw to the front buffer directly. Bypass Allegro's backbuffer options.
    glDrawBuffer(GL_BACK);

    //--Clear the screen off
    glClearDepth(1.0f);
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glClearStencil(0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

    //--[Immediate Rendering]
    if(!rGlobal->gDisplayManager->IsUsingSortedRendering())
    {
        //--If the TransitionManager requests it, don't render anything else.  It has captured the
        //  screen and is drawing an overlay.
        if(!TransitionManager::Fetch()->IsTransitioning())
        {
            //--Screen shaking.
            float tShakeX, tShakeY;
            MapManager::Fetch()->GetShakeFactors(tShakeX, tShakeY);
            glTranslatef(tShakeX, tShakeY, 0.0f);

            //--Render the Entities.
            DebugManager::Print("Rendering Entities\n");
            EntityManager::Fetch()->Render();
            DebugManager::Print("Rendered Entities\n");

            //--Render the foreground.
            MapManager::Fetch()->Render();

            //--TransitionManager goes last to grab the screen.
            TransitionManager::Fetch()->Render();
            DebugManager::Print("Rendered TransitionManager\n");
        }
        //--Only render the TransitionManager
        else
        {
            //--Transition managers renders its screen grab.
            TransitionManager::Fetch()->Render();
        }

        //--Debug render calls.
        RenderDebugStrings();
        RenderFPS();
        RenderJoypadDepression();
    }
    //--[Delayed Rendering]
    //--Rendering mode which uses a sorted depth list to render all objects in the depth order,
    //  which is back-to-front to allow proper alpha layering. This is only really useful in a
    //  2D application, since 3D graphics don't necessarily have an easily sorted depth list.
    else
    {
        //--Transition manager can screenshot to get rendering.
        if(!TransitionManager::Fetch()->IsTransitioning())
        {
            //--Screen shaking.
            float tShakeX, tShakeY;
            MapManager::Fetch()->GetShakeFactors(tShakeX, tShakeY);
            glTranslatef(tShakeX, tShakeY, 0.0f);

            //--Reset the rendering list.
            SugarLinkedList *rRenderingList = DisplayManager::FetchRenderingList();
            rRenderingList->ClearList();

            //--Render the Background.
            MapManager::Fetch()->AddToRenderList(rRenderingList);
            DebugManager::Print("Queued Map\n");

            //--Render the Entities.
            EntityManager::Fetch()->AddToRenderingList(rRenderingList);
            DebugManager::Print("Queued Entities\n");

            //--Sort the rendering list.
            rRenderingList->SortListUsing(&IRenderable::CompareRenderable);
            DebugManager::Print("Sorted Entities\n");

            //--Render all the objects on the rendering list.
            IRenderable *rRenderObject = (IRenderable *)rRenderingList->PushIterator();
            while(rRenderObject)
            {
                rRenderObject->Render();
                rRenderObject = (IRenderable *)rRenderingList->AutoIterate();
            }
            DebugManager::Print("Rendered all objects.\n");

            //--TransitionManager goes last to grab the screen.
            TransitionManager::Fetch()->Render();
            DebugManager::Print("Handled TransitionManager\n");
        }
        //--Only render the TransitionManager
        else
        {
            //--Transition managers renders its screen grab. This is the same as immediate mode.
            TransitionManager::Fetch()->Render();
        }

        //--Debug render calls. These are technically part of the GUI.
        RenderDebugStrings();
        RenderFPS();
        RenderJoypadDepression();
    }

    /*
    if(SugarBitmap::xrActiveAtlas)
    {
        SugarBitmap::xrActiveAtlas->Bind();
        glBegin(GL_QUADS);
            glTexCoord2f(0.00f, 0.0000f); glVertex2f(   0.0f,   0.0f);
            glTexCoord2f(0.25f, 0.0000f); glVertex2f(1024.0f,   0.0f);
            glTexCoord2f(0.25f, 0.1875f); glVertex2f(1024.0f, 768.0f);
            glTexCoord2f(0.00f, 0.1875f); glVertex2f(   0.0f, 768.0f);
        glEnd();
    }*/

    //--In either case, send the data to the screen.
    al_flip_display();

    //--Clean
    DisplayManager::CleanOrtho();

    //--Toggle this flag.
    DisplayManager::xHasHadRenderPassSinceFullscreen = true;

    //--<DEBUG>
    LogFPS(sDriveShaft);
    DebugManager::PopPrint("[GameGear Render Complete]\n");
}
void GameGear::ShiftTo()
{
    //--Shifts the program into GameGear
    GLOBAL *rGlobal = Global::Shared();
    rGlobal->rCurrentEventHandler  = &GameGear::EventHandler;
    rGlobal->rCurrentLogicHandler  = &GameGear::LogicHandler;
    rGlobal->rCurrentRenderHandler = &GameGear::RenderHandler;
}

//--[Worker Function]
void RenderDebugStrings()
{
    //--Error in the fonts.
    GLOBAL *rGlobal = Global::Shared();
    if(!rGlobal->gBitmapFont) return;

    //--Setup
    const float cScale = 0.25f;

    //--For each debug line, independent of what's on the line, render...
    for(int i = 0; i < DEBUG_LINES_TOTAL; i ++)
    {
        //--Make sure there's something to render.
        if(!rGlobal->gDebugPrintStrings[i]) continue;

        //--Position/Scale
        glTranslatef(0.0f, 40.0f * cScale * (i+1), 0.0f);
        glScalef(cScale, -cScale, 1.0f);

        //--Render
        rGlobal->gBitmapFont->DrawText(0.0f, 0.0f, 0, rGlobal->gDebugPrintStrings[i]);

        //--Clean
        glScalef(1.0f / cScale, 1.0f / -cScale, 1.0f);
        glTranslatef(0.0f * -1.0f, -40.0f * cScale * (i+1), 0.0f);
    }
}
void RenderFPS()
{
    //--Render the current FPS in the top right, averaged over a few seconds by the logger. This can
    //  be disabled with a define change.
    #ifndef DBG_SHOW_FPS
        return;
    #endif
    return;

    //--Error in the fonts.
    GLOBAL *rGlobal = Global::Shared();
    if(!rGlobal->gBitmapFont) return;

    //--Setup
    float tRenderX = VIRTUAL_CANVAS_X - 40.0f;
    const float cScale = 0.25f;
    char tBufferFPS[32];
    sprintf(tBufferFPS, "%04.1f", rGlobal->gActualFPS);

    //--Position/Scale
    glTranslatef(tRenderX, 40.0f * cScale, 0.0f);
    glScalef(cScale, -cScale, 1.0f);

    //--Render
    rGlobal->gBitmapFont->DrawText(0.0f,   0.0f, SUGARFONT_AUTOCENTER_X, tBufferFPS);

    //--Clean
    glScalef(1.0f / cScale, 1.0f / -cScale, 1.0f);
    glTranslatef(tRenderX * -1.0f, -40.0f * cScale, 0.0f);
}
void RenderJoypadDepression()
{
    //--Renders text in the bottom left of the canvas indicating which joypad keys are currently down.
    //  This can be disabled with a define change.
    #ifndef DBG_SHOW_JOYPAD_DEPRESSION
        return;
    #endif

    //--Setup
    GLOBAL *rGlobal = Global::Shared();
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Buffering
    char tNumBuf[8];
    char tNameBuf[32];
    char tBuffer[256];
    strcpy(tBuffer, "Joypad: ");

    //--Iterate across. If no joypad is hooked up, nothing will print.
    ALLEGRO_JOYSTICK *rJoystick = al_get_joystick(0);
    if(rJoystick)
    {
        //--If a key is down, it gets appended. If it's not down, it is ignored.
        for(int i = 0; i < al_get_joystick_num_buttons(rJoystick); i ++)
        {
            sprintf(tNameBuf, "DBGJoy%02i", i);
            if(rControlManager->GetControlState(tNameBuf)->mIsDown)
            {
                sprintf(tNumBuf, "%i, ", i);
                strcat(tBuffer, tNumBuf);
            }
        }

        //--Check sticks as well.
        for(int i = 0; i < al_get_joystick_num_sticks(rJoystick); i ++)
        {
            for(int p = 0; p < 4; p ++)
            {
                int tIndex = JOYSTICK_OFFSET_STICK + (i * JOYSTICK_STICK_CONSTANT)  + p;
                sprintf(tNameBuf, "DBGStk%03i", tIndex);
                if(rControlManager->GetControlState(tNameBuf)->mIsDown)
                {
                    sprintf(tNumBuf, "%i, ", tIndex);
                    strcat(tBuffer, tNumBuf);
                }
            }
        }
    }

    //--Remove the last ','
    if(tBuffer[8] != '\0')
    {
        uint32_t tLen = strlen(tBuffer);
        tBuffer[tLen-2] = '\0';
    }

    //--Rendering
    const float cScale = 0.25f;
    float tRenderY = VIRTUAL_CANVAS_Y - 5.0f;
    glColor3f(1.0f, 0.0f, 0.0f);
    glTranslatef(0.0f, tRenderY, 0.0f);
    glScalef(cScale, -cScale, 1.0f);
    rGlobal->gBitmapFont->DrawText( 0.0f, 0.0f, 0, tBuffer);

    //--Clean
    glScalef(1.0f / cScale, 1.0f / -cScale, 1.0f);
    glTranslatef(0.0f, tRenderY, 0.0f);
    glColor3f(1.0f, 1.0f, 1.0f);
}
#endif // defined
