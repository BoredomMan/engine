//--[GameGear]
//--The main game's Gear, all standard update behaviors and rendering.

//--[What is a Gear?]
//--A gear is how the managers and parts of the engine interact, what order they interact in, and
//  in some cases, whether or not they interact at all.  A gear is effectively a set of instructions
//  the gets executed every logic tick or render tick, encapsulated within a class.
//--They manifest as function pointers within the Global.  To change which gear you're using, call
//  the static ShiftTo function.  Some gears may require transitions to shift to, others may be
//  able to shift without any special operations.

#pragma once

#include "Definitions.h"
#include "Structures.h"

class GameGear
{
    private:

    protected:

    public:
    static void EventHandler(DriveShaft &sDriveShaft);
    static void LogicHandler(DriveShaft &sDriveShaft);
    static void RenderHandler(DriveShaft &sDriveShaft);
    static void ShiftTo();
    static void PostLogicHandler();
};
