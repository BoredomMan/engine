//--[ISaveable]
//--Interface for anything which can be regged to the SaveManager. Saveable objects are also expected
//  to be loadable by the same measure, though the loading process may be a bit different, as objects
//  may be created and registered during loading.
//--Note that the interface is assumed to be automatically using the SugarLumpManager for exports.

#pragma once

//--[Includes]
#include "Definitions.h"
#include "Structures.h"

//--Undefine this to switch off the extended interface, which your program may not need.
#define _ISAVEABLE_EXTENDED_INTERFACE_

//--[Nonstandard Features]
#ifdef _ISAVEABLE_EXTENDED_INTERFACE_
#include "SugarAutoBuffer.h"
#include "SugarLumpManager.h"
#endif

class ISaveable
{
    private:

    protected:

    public:
    //--System
    virtual ~ISaveable() {};

    //--Public Variables
    //--Property Queries
    //--Manipulators
    //--Core Methods
    private:
    //--Private Core Methods
    public:
    //--Update
    //--File I/O
    virtual void SaveToFile() = 0;
    virtual void LoadFromFile() = 0;

    //--[Extended Interface]
    //--This provides some useful behavior which is common to a lot of classes that inherit the
    //  ISaveable interface. However, it's optional to include it, as it uses other Sugar^3 classes.
    //  If you don't need those, you can implement it yourself.
    #ifdef _ISAVEABLE_EXTENDED_INTERFACE_
    static RootLump *StdSaveBoot(const char *pLumpName, const char *pLumpHeader, SugarAutoBuffer *&sBuffer)
    {
        //--Creates and returns a new RootLump with the requested name and header. This is a
        //  standard, but classes don't have to use this function.
        //--The SugarAutoBuffer pointer doesn't need to be initialized or null'd.
        RootLump *nNewLump = SugarLumpManager::CreateLump(pLumpName);

        //--AutoBuffer object dynamically sizes the buffer for writing. Handy!
        sBuffer = new SugarAutoBuffer();
        sBuffer->AppendStringWithoutNull(pLumpHeader);

        //--Pass it back.
        return nNewLump;
    }
    static void StdSaveComplete(SugarAutoBuffer *pBuffer, RootLump *pLump)
    {
        //--Takes the given SugarAutoBuffer, pulls out its data, and puts it into the RootLump.
        //  This is the complement to StdSaveBoot. Your class doesn't necessarily need to use it.
        //--This class is assuming the buffer and lump are valid and were created by StdSaveBoot.
        //  If they weren't, don't blame me!
        if(!pBuffer || !pLump) return;

        //--Pull the data out and insert it into the lump.
        pLump->mDataSize = pBuffer->GetCursor();
        pLump->mData = pBuffer->LiberateData();
        delete pBuffer;

        //--Register it and clean up.
        SugarLumpManager::Fetch()->RegisterLump(pLump);
    }
    #endif

    //--Drawing
    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
};

//--Hooking Functions

