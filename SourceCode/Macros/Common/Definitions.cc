#include "Definitions.h"
#include "Structures.h"
#include "DataLibrary.h"

//--[Memory Functions]
//--If this definition is set to true: starmalloc just becomes malloc. This speeds up program execution, but may
//  make tracking down memory bugs harder.
//--Usage:
// SetMemoryData(__FILE__, __LINE__);
// Thing *nThing = (Thing *)starmemoryalloc(sizeof(Thing));
char gxMemoryFile[256]; //Last-used malloc filename.
int gxMemoryLine; //Last-used malloc line.

//--Using an internal malloc interception:
#ifdef USE_INTERNAL_MALLOC
    void SetMemoryData(const char *pFile, int pLine)
    {
        //--Stores the last-used file and line in that file for malloc. Used for printing to the console so the
        //  programmer knows exactly which version of starmemoryalloc() failed.
        //--__LINE__ and __FILE__ macros are usually used for this task. 256 characters are allocated for the job.
        if(!pFile || !pLine) return;
        strncpy(gxMemoryFile, pFile, 255);
        gxMemoryLine = pLine;
    }
    void *starmemoryalloc(size_t pSize)
    {
        //--Starlight engine wrapper of starmemoryalloc(). Performs the malloc operation and returns the results. If by some
        //  horrible accident the malloc operation fails (due to Windows being a bad OS, usually), returns NULL and
        //  prints the provided error notice.
        //--These error notices should be forwarded to the programmer. It's why the game has a console open.
        void *nPtr = malloc(pSize);
        if(nPtr) return nPtr;

        //--Error case.
        fprintf(stderr, "Error, unable to allocate memory. Report: %s line %i\n", gxMemoryFile, gxMemoryLine);
        sprintf(gxLastIssue, "Error, unable to allocate memory. Report: %s line %i\n", gxMemoryFile, gxMemoryLine);
        LogError();
        return NULL;
    }

//--starmemoryalloc is just a direct alias of malloc, and SetMemoryData() does nothing. This is strictly faster
//  but obviously has no debug properties.
#else

#endif

//--[Error Logging]
//--Writes an error to the error log.
char gxLastIssue[256]; //What to actually write.
void LogError()
{
    //--Get the data.
    time_t tRawTime;
    struct tm *tTimeInfo;
    time (&tRawTime);
    tTimeInfo = localtime(&tRawTime);

    //--Log it. Close to flush the data.
    FILE *fOutfile = fopen("ErrorLog.txt", "a");
    fprintf(fOutfile, "%s - %s\n", asctime(tTimeInfo), gxLastIssue);
    fclose(fOutfile);

    //--Reset the last issue so erroneous loggings don't produce duplicates.
    strcpy(gxLastIssue, "No issue.");
}

//--[General-Purpose Functions]
float GetGameTime()
{
    //--Returns the time since execution as represented by a floating point number. Basically does
    //  the low-level library work for you.

    //--Allegro uses seconds directly.
    #if defined _ALLEGRO_PROJECT_
        return al_get_time();

    //--SDL uses milliseconds.
    #elif defined _SDL_PROJECT_
        return (SDL_GetTicks() / 1000.0f);
    #endif

    //--If somehow neither library was declared, error out.
    return 0.0f;
}

float GetRandomPercent()
{
    //--Returns a random floating point number between 0.0 and 1.0, inclusive.  The float has up
    //  to three digits of signifigance.
    return ((float)(rand() % 1001)) / 1000.0f;
}
int CompareSortingPackages(const void *elem1, const void *elem2)
{
    //--This is a prototype and/or do-nothing sorting function that can be used with qsort.
    return 0;
}
uint32_t GetNearestSquare(uint32_t pBase)
{
    //--Returns the next-nearest power of two number.  In this case, we go up one (you will need to
    //  pad the edges).
    //--This is used in OpenGL when the ForcePowerOfTwo flag is on (mostly on very old hardware).
    if(pBase == 0) return 0;
    if(pBase == 1) return 1;

    //--Check each bit starting at the top.
    uint32_t tBit = 0x80000000;
    while(tBit > 1)
    {
        if(pBase & tBit)
        {
            return tBit;
        }
        tBit = tBit / 2;
    }
    return 0;
}
void ClampToCircle(float &sValue)
{
    //--Clamps a circular angle (in degrees) within that of a circle, from 0 to 360.
    while(sValue <    0.0f) sValue = sValue + 360.0f;
    while(sValue >= 360.0f) sValue = sValue - 360.0f;
}

//--[String Functions]
void ResetString(char *&sString, const char *pNewString)
{
    //--Deallocates a string and sets it to the pNewString.  Passing NULL for the pNewString will
    //  simply deallocate and NULL the sString.
    //--MAKE SURE the sString was either NULL or a valid string before passing in to this, this call
    //  should not be used on raw allocated memory.
    if(sString == pNewString) return;

    free(sString);
    sString = NULL;
    if(!pNewString) return;

    sString = (char *)starmemoryalloc(sizeof(char) * (strlen(pNewString) + 1));
    strcpy(sString, pNewString);
}
char *InitializeString(const char *pFormat, ...)
{
    //--Returns a newly allocated heap string with the provided format/arguments. Can never return
    //  null, as it will return a single null character string on error instead.
    //--The returned string must be freed as normal.

    if(!pFormat)
    {
        SetMemoryData(__FILE__, __LINE__);
        char *nString = (char *)starmemoryalloc(sizeof(char) * 1);
        nString[0] = '\0';
        return nString;
    }

    //--Get the variable arg list.
    va_list tArgList;
    va_start (tArgList, pFormat);

    //--Print the args into a buffer.
    static char xBuffer[32768];
    vsprintf(xBuffer, pFormat, tArgList);

    //--Now that the length is known, ResetString can allocate it properly.
    SetMemoryData(__FILE__, __LINE__);
    char *nString = (char *)starmemoryalloc(sizeof(char) * (strlen(xBuffer) + 1));
    strcpy(nString, xBuffer);

    //--Clean up.
    va_end (tArgList);
    return nString;
}

//--[Console Commands]
#include "OptionsManager.h"
int ScanToArgument(const char *pString, int pArgument)
{
    //--Worker function, scans ahead in the given string and returns the index of the ith argument.
    //  Returns 0 on error, but doesn't throw the error flag.
    //--Remember:  The 0th argument is the first one after the function name!  This is C, not Lua.
    //--Passing a negative number will cause this function to return how many arguments there were
    //  in total, and is effectively a scanning function.
    if(!pString)
    {
        OptionsManager::xErrorFlag = true;
        return 0;
    }
    int tArgumentsFound = 0;

    //--Alternating space case.  Uses " as a stacker, ignoring delimiters when using " until
    //  the stack hits zero.  Use \" to cancel this behavior.
    bool mSpaceMode = false;
    int tLen = strlen(pString);
    bool mQuoteMode = false;
    for(int i = 0; i < tLen; i ++)
    {
        //--If the quote stack is 0, we are in normal delimiter behavior.
        if(!mQuoteMode)
        {
            //--Normally, finding spaces switches modes.
            if(!mSpaceMode)
            {
                if(pString[i] == ' ')
                {
                    mSpaceMode = true;
                }
            }
            //--If in SpaceMode, then we're looking for non-spaces to stop delimiters.
            else if(pString[i] != ' ')
            {
                //--Switch off.
                mSpaceMode = false;

                //--If looking for a specific argument, return the next letter as the start of
                //  the argument.
                if(pArgument == tArgumentsFound) return i;
                tArgumentsFound ++;
            }

            //--Finding a quote here begins quote mode.
            if(pString[i] == '"' && pString[i-1] != '\\')
            {
                mQuoteMode = true;
            }
        }
        //--In quote mode, ignore everything until we find another quote.
        else
        {
            if(pString[i] == '"' && pString[i-1] != '\\')
            {
                mQuoteMode = false;
            }
        }
    }

    //--If we got this far, and we have a negative number as our argument count, then pass back
    //  how many arguments we found total.
    if(pArgument < 0) return tArgumentsFound;

    //--If we got this far, but had a positive number of arguments, then we didn't find the argument
    //  and will throw an error.
    OptionsManager::xErrorFlag = true;
    return 0;
}
int GetTotalArguments(const char *pString)
{
    //--Macro to use the worker function to get how many arguments there are.
    //--Returns 0 on error.
    return ScanToArgument(pString, -1);
}

//--[Console Handling]
#include "DebugManager.h"
char *GetArgumentS(const char *pString, int pNumberToGet)
{
    //--Returns the ith argument of the given string.  If the argument is not found or is out
    //  of range, or is not a string (all strings are to be explicitly surrounded by quotes!) then
    //  NULL is passed back, and an error is thrown.
    //--The string passed back is a heap string.  You are responsible for deallocating it.
    //--We use a multipass technique to avoid unnecessary reallocs.
    //--Strings can now optionally force quote requirements with a static flag.
    static bool xForceQuotes = false;

    DebugManager::PushPrint(false, "[Parsing] %s\n", pString);
    if(!pString || pNumberToGet < 0)
    {
        OptionsManager::xErrorFlag = true;
        DebugManager::PopPrint("[Parsing] Failed, invalid header.\n", pString);
        return NULL;
    }

    //--Find out which letter is the first one in the argument.
    int tStartLetter = ScanToArgument(pString, pNumberToGet);
    if(!tStartLetter)
    {
        OptionsManager::xErrorFlag = true;
        DebugManager::PopPrint("[Parsing] 0th Letter is invalid.\n");
        return NULL;
    }

    //--The letter *MUST* be a quote, or we throw an error.
    bool tMustEndInQuote = false;
    if(xForceQuotes)
    {
        tMustEndInQuote = true;
        if(pString[tStartLetter] != '"')
        {
            OptionsManager::xErrorFlag = true;
            DebugManager::PopPrint("[Parsing] String was not a quote-surrounded string, starting at %i.\n", tStartLetter);
            return NULL;
        }
    }
    else
    {
        if(pString[tStartLetter] == '"') tMustEndInQuote = true;
    }

    //--Find out how many letters long the string arg is.
    int tLetterToBeginIteration = tStartLetter+1;
    if(!tMustEndInQuote) tLetterToBeginIteration --;
    int tLetters = 0;
    int tLen = strlen(pString);
    for(int i = tLetterToBeginIteration; i < tLen; i ++)
    {
       // fprintf(stderr, "Scan |%c|\n", pString[i]);
        if(pString[i] == '"' && pString[i-1] != '\\')
        {
            break;
        }
        else if(!tMustEndInQuote && (pString[i] == ' ' || pString[i] == '\0'))
        {
            break;
        }
        else
        {
            tLetters ++;
        }
    }
   // fprintf(stderr, "Argument %i is %i letters long\n", pNumberToGet, tLetters);
    if(tLetters < 1)
    {
        OptionsManager::xErrorFlag = true;
        DebugManager::PopPrint("[Parsing] Error, string computed zero characters.\n");
        return NULL;
    }

    //--Allocate space, and dump the string in that space.
    int tCountLetter = 0;
    SetMemoryData(__FILE__, __LINE__);
    char *nString = (char *)starmemoryalloc(sizeof(char) * (tLetters + 1));
    for(int i = tLetterToBeginIteration; i < tLen; i ++)
    {
        if(pString[i] == '"' && pString[i-1] != '\\')
        {
            break;
        }
        else if(!tMustEndInQuote && pString[i] == ' ')
        {
            break;
        }
        else if(pString[i] == '"' && pString[i-1] == '\\')
        {
            tCountLetter --;
            nString[tCountLetter] = pString[i];
            nString[tCountLetter+1] = '\0';
        }
        else
        {
            nString[tCountLetter] = pString[i];
            nString[tCountLetter+1] = '\0';
        }
        tCountLetter ++;
    }

    //--Pass it back!
    DebugManager::PopPrint("[Parsing] Completed |%s|\n", nString);
    return nString;
}
float GetArgumentF(const char *pString, int pNumberToGet)
{
    //--Returns a floating point value at the ith argument in the string.  If not found or out of
    //  range, returns 0.0f and throws an error.
    int tStartLetter = ScanToArgument(pString, pNumberToGet);
    if(!tStartLetter) return 0.0f;

    //--Use sscanf to extract the information.
    float tReturn = 0.0f;
    sscanf(&pString[tStartLetter], "%f", &tReturn);

    return tReturn;
}
int GetArgumentI(const char *pString, int pNumberToGet)
{
    //--Identical to GetArgumentF, except will automatically round off the decimal component and
    //  pass it back as an integer.
    float tReturn = GetArgumentF(pString, pNumberToGet);
    return (int)tReturn;
}

//--[Spatial Macros]
void GetCenterPoints3DReal(ThreeDimensionReal pDimensions, float &sXCenter, float &sYCenter)
{
    //--Returns the X and Y center points.  The mXCenter/mYCenter values may be misleading, as they
    //  are the entity's signifigance center, and not necessarily the literal center.
    sXCenter = (pDimensions.mLft + pDimensions.mRgt) / 2.0f;
    sYCenter = (pDimensions.mTop + pDimensions.mBot) / 2.0f;
}
void GetCenterPoints2DReal(TwoDimensionReal pDimensions, float &sXCenter, float &sYCenter)
{
    //--Same as above, but uses a different structure.
    sXCenter = (pDimensions.mLft + pDimensions.mRgt) / 2.0f;
    sYCenter = (pDimensions.mTop + pDimensions.mBot) / 2.0f;
}
float GetDistanceBetween3DReal(ThreeDimensionReal pDimensionsA, ThreeDimensionReal pDimensionsB)
{
    //--Returns the centroid-centroid distance, ignoring the Z coordinate.
    float tXDist = ((pDimensionsA.mLft + pDimensionsA.mRgt) / 2.0f) - ((pDimensionsB.mLft + pDimensionsB.mRgt) / 2.0f);
    float tYDist = ((pDimensionsA.mTop + pDimensionsA.mBot) / 2.0f) - ((pDimensionsB.mTop + pDimensionsB.mBot) / 2.0f);
    return sqrtf((tXDist * tXDist) + (tYDist * tYDist));
}

//--[Memory Macros]
bool VerifyStructure(void *pStructure, size_t pSize, size_t pSizePerElement)
{
    return VerifyStructure(pStructure, pSize, pSizePerElement, false);
}
bool VerifyStructure(void *pStructure, size_t pSize, size_t pSizePerElement, bool pHasDebug)
{
    //--Verifies that all components of the given structure are non-NULL. This is used to assess
    //  if a structure is "valid", such as a collection of images or other classes. If any of them
    //  are NULL, we would reject the whole batch and tell the user to check their datafiles.
    if(!pStructure || pSize < 1 || pSizePerElement < 1) return false;

    //--Casting. Very dangerous!
    bool tReturnFlag = true;
    if(pHasDebug) fprintf(stderr, "Begin.\n");
    intptr_t tMemoryAddress = (intptr_t)pStructure;

    //--Check each element.
    for(unsigned int i = 0; i < pSize / pSizePerElement; i ++)
    {
        //--Get the pointer position.
        void **rPtr = (void **)tMemoryAddress;

        //--Debug:
        if(pHasDebug) fprintf(stderr, " Slot %i: %p\n", i, (*rPtr));

        //--If it's null, fail!
        if(!(*rPtr))
        {
            //--If debug is enabled, don't stop.
            if(pHasDebug)
            {
                tReturnFlag = false;
            }
            //--If debug is disabled, no need to continue.
            else
            {
                return false;
            }
        }

        //--Next element.
        tMemoryAddress += pSizePerElement;
    }

    //--Send back the return flag. If debug is disabled, this means the structure was filled.
    //  If the debug is enabled, the structure may or may not be filled.
    return tReturnFlag;
}
void Crossload(void *pStructure, size_t pSize, size_t pSizePerElement, const char *pPathPattern, const char **pNameList)
{
    Crossload(pStructure, pSize, pSizePerElement, pPathPattern, pNameList, false);
}
void Crossload(void *pStructure, size_t pSize, size_t pSizePerElement, const char *pPathPattern, const char **pNameList, bool pHasDebug)
{
    //--[Documentation and Setup]
    //--A subroutine that crossloads image pointers using a list. The pStructure is expected to be a series
    //  of pointers (nominally SugarBitmap *, but theoretically any kind) that receive entries from the
    //  DataLibrary. The entries are retrieved via pPathPattern populated sequentially by a list of
    //  strings. This allows rapidly populating a set of images.
    //--Obviously, the gain here is speed at a cost of safety. Make sure the populating list is of the
    //  same size as the number of elements because this *will* overrun the array boundaries!
    char tBuffer[256];
    DataLibrary *rDataLibrary = DataLibrary::Fetch();

    //--Cast.
    intptr_t tMemoryAddress = (intptr_t)pStructure;
    if(pHasDebug) fprintf(stderr, "Starting block is %p\n", pStructure);

    //--Iterate across the lists.
    for(unsigned int i = 0; i < pSize / pSizePerElement; i ++)
    {
        //--Get the pointer position.
        void **rPtr = (void **)tMemoryAddress;

        //--Buffer.
        sprintf(tBuffer, pPathPattern, pNameList[i]);

        //--Populate it.
        *rPtr = rDataLibrary->GetEntry(tBuffer);
        if(pHasDebug) fprintf(stderr, " Crossload %s to %p, result %p\n", tBuffer, rPtr, *rPtr);

        //--Next element.
        tMemoryAddress += pSizePerElement;
    }
}


//--[Spine Extension Functions]
#include "spine/extension.h"
#include "DataLibrary.h"
#include "SugarBitmap.h"
#include "SpineExpression.h"
char  *_spUtil_readFile(const char *pPath, int *pLength)
{
    return _spReadFile(pPath, pLength);
}
void _spAtlasPage_createTexture(spAtlasPage *pSelf, const char *pPath)
{
    //--The image in question is always named "Atlas".
    SugarBitmap *nBitmap = (SugarBitmap *)DataLibrary::Fetch()->GetEntry(SpineExpression::xLoadPath);
    if(!nBitmap) return;

   //--Store the bitmap on the renderer object's void pointer. It will be used later during rendering.
   pSelf->rendererObject = nBitmap;

   //--Store the width and height so spine can compute the atlas sizes.
   pSelf->width = nBitmap->GetWidth();
   pSelf->height = nBitmap->GetHeight();
}
void _spAtlasPage_disposeTexture(spAtlasPage* pSelf)
{
    delete ((SugarBitmap *)pSelf->rendererObject);
}

//--[Strings]
#if defined _USE_LOCAL_STRCASECMP_
static const char starcharmap[] = {
    '\000', '\001', '\002', '\003', '\004', '\005', '\006', '\007',
    '\010', '\011', '\012', '\013', '\014', '\015', '\016', '\017',
    '\020', '\021', '\022', '\023', '\024', '\025', '\026', '\027',
    '\030', '\031', '\032', '\033', '\034', '\035', '\036', '\037',
    '\040', '\041', '\042', '\043', '\044', '\045', '\046', '\047',
    '\050', '\051', '\052', '\053', '\054', '\055', '\056', '\057',
    '\060', '\061', '\062', '\063', '\064', '\065', '\066', '\067',
    '\070', '\071', '\072', '\073', '\074', '\075', '\076', '\077',
    '\100', '\141', '\142', '\143', '\144', '\145', '\146', '\147',
    '\150', '\151', '\152', '\153', '\154', '\155', '\156', '\157',
    '\160', '\161', '\162', '\163', '\164', '\165', '\166', '\167',
    '\170', '\171', '\172', '\133', '\134', '\135', '\136', '\137',
    '\140', '\141', '\142', '\143', '\144', '\145', '\146', '\147',
    '\150', '\151', '\152', '\153', '\154', '\155', '\156', '\157',
    '\160', '\161', '\162', '\163', '\164', '\165', '\166', '\167',
    '\170', '\171', '\172', '\173', '\174', '\175', '\176', '\177',
    '\200', '\201', '\202', '\203', '\204', '\205', '\206', '\207',
    '\210', '\211', '\212', '\213', '\214', '\215', '\216', '\217',
    '\220', '\221', '\222', '\223', '\224', '\225', '\226', '\227',
    '\230', '\231', '\232', '\233', '\234', '\235', '\236', '\237',
    '\240', '\241', '\242', '\243', '\244', '\245', '\246', '\247',
    '\250', '\251', '\252', '\253', '\254', '\255', '\256', '\257',
    '\260', '\261', '\262', '\263', '\264', '\265', '\266', '\267',
    '\270', '\271', '\272', '\273', '\274', '\275', '\276', '\277',
    '\300', '\341', '\342', '\343', '\344', '\345', '\346', '\347',
    '\350', '\351', '\352', '\353', '\354', '\355', '\356', '\357',
    '\360', '\361', '\362', '\363', '\364', '\365', '\366', '\367',
    '\370', '\371', '\372', '\333', '\334', '\335', '\336', '\337',
    '\340', '\341', '\342', '\343', '\344', '\345', '\346', '\347',
    '\350', '\351', '\352', '\353', '\354', '\355', '\356', '\357',
    '\360', '\361', '\362', '\363', '\364', '\365', '\366', '\367',
    '\370', '\371', '\372', '\373', '\374', '\375', '\376', '\377',
};

int strcasecmp(const char *pStringA, const char *pStringB)
{
  const unsigned char *rStringA = (const unsigned char *) pStringA;
  const unsigned char *rStringB = (const unsigned char *) pStringB;
  int result;
  if (rStringA == rStringB) return 0;

  while ((result = tolower(*rStringA) - tolower(*rStringB++)) == 0)
    if (*rStringA++ == '\0')
      break;
  return result;
}
int strncasecmp(const char *s1, const char *s2, register size_t n)
{
    register unsigned char u1, u2;
    for (; n != 0; --n) {
        u1 = (unsigned char) *s1++;
        u2 = (unsigned char) *s2++;
        if (starcharmap[u1] != starcharmap[u2]) {
            return starcharmap[u1] - starcharmap[u2];
        }
        if (u1 == '\0') {
            return 0;
        }
    }
    return 0;
}
#endif

//--[Text]
#include "SugarFont.h"
void RenderTextToFit(SugarFont *pFont, float pX, float pY, float pMaxWid, const char *pText, ...)
{
    //--Error check.
    if(!pFont || !pText || pMaxWid < 1.0f) return;

    //--Get the variable arg list.
    va_list tArgList;
    va_start(tArgList, pText);

    //--Print the args into a buffer.
    char tBuffer[8192];
    vsprintf(tBuffer, pText, tArgList);

    //--Clean up.
    va_end (tArgList);

    //--Setup.
    float cXScale = 1.0f;
    float cTextWid = pFont->GetTextWidth(tBuffer);
    float cXPosition = pX - (cTextWid * 0.50f);
    float cYPosition = pY - (pFont->GetTextHeight() * 0.50f);
    if(cTextWid > pMaxWid)
    {
        cXScale = pMaxWid / cTextWid;
        cXPosition = pX - (pMaxWid * 0.50f);
    }

    //--Render the name
    glTranslatef(cXPosition, cYPosition, 0.0f);
    glScalef(cXScale, 1.0f, 1.0f);
    pFont->DrawText(0.0f, 0.0f, 0, 1.0f, tBuffer);
    glScalef(1.0f / cXScale, 1.0f, 1.0f);
    glTranslatef(cXPosition * -1.0f, cYPosition * -1.0f, 0.0f);
}
