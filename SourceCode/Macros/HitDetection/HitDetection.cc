//--Base
#include "HitDetection.h"

//--Classes
//--Definitions
//--Generics
//--GUI
//--Libraries
//--Managers

//--Point is within functions
bool IsPointWithin(float pX, float pY, float pLft, float pTop, float pRgt, float pBot)
{
    return (pX >= pLft && pX <= pRgt && pY >= pTop && pY <= pBot);
}
bool IsPointWithin2DReal(float pX, float pY, TwoDimensionReal pRect)
{
    return IsPointWithin(pX, pY, pRect.mLft, pRect.mTop, pRect.mRgt, pRect.mBot);
}
bool IsPointWithinExclusive(float pX, float pY, float pLft, float pTop, float pRgt, float pBot)
{
    //--Same as IsPointWithin, but will not consider the exact edges to be within the rectangle.
    return (pX > pLft && pX < pRgt && pY > pTop && pY < pBot);
}

//--Collision functions and macros
void Swap(int &s1, int &s2)
{
    int tTemp = s1;
    s1 = s2;
    s2 = tTemp;
}
void SwapF(float &s1, float &s2)
{
    float tTemp = s1;
    s1 = s2;
    s2 = tTemp;
}
bool IsCollision(float lft1, float top1, float rgt1, float bot1, float lft2, float top2, float rgt2, float bot2)
{
    //--Safety.
    if(lft1 > rgt1) SwapF(lft1, rgt1);
    if(top1 > bot1) SwapF(top1, bot1);
    if(lft2 > rgt2) SwapF(lft2, rgt2);
    if(top2 > bot2) SwapF(top2, bot2);


    return ((((lft1 >= lft2) && (lft1 <= rgt2))  ||
            (( rgt1 >= lft2) && (rgt1 <= rgt2))  ||
            (( lft1 >= lft2) && (rgt1 <= rgt2))  ||
            (( lft1 <= lft2) && (rgt1 >= rgt2))) &&
            (((top1 >= top2) && (top1 <= bot2))  ||
            (( bot1 >= top2) && (bot1 <= bot2))  ||
            (( top1 >= top2) && (bot1 <= bot2))  ||
            (( top1 <= top2) && (bot1 >= bot2))));
}
bool IsCollision3DReal(ThreeDimensionReal pDim1, int pLft2, int pTop2, int pRgt2, int pBot2)
{
    return IsCollision(pDim1.mLft, pDim1.mTop, pDim1.mRgt, pDim1.mBot, pLft2, pTop2, pRgt2, pBot2);
}
bool IsCollision3DReal(ThreeDimensionReal pDim1, ThreeDimensionReal pDim2)
{
    return IsCollision(pDim1.mLft, pDim1.mTop, pDim1.mRgt, pDim1.mBot, pDim2.mLft, pDim2.mTop, pDim2.mRgt, pDim2.mBot);
}
bool IsCollision2DReal(TwoDimensionReal pDim1, TwoDimensionReal pDim2)
{
    return IsCollision(pDim1.mLft, pDim1.mTop, pDim1.mRgt, pDim1.mBot, pDim2.mLft, pDim2.mTop, pDim2.mRgt, pDim2.mBot);
}
bool IsCollision2DReal(TwoDimensionReal pDim1, int pLft2, int pTop2, int pRgt2, int pBot2)
{
    return IsCollision(pDim1.mLft, pDim1.mTop, pDim1.mRgt, pDim1.mBot, pLft2, pTop2, pRgt2, pBot2);
}
bool IsCollision2D3DReal(ThreeDimensionReal pDim1, TwoDimensionReal pDim2)
{
    return IsCollision(pDim1.mLft, pDim1.mTop, pDim1.mRgt, pDim1.mBot, pDim2.mLft, pDim2.mTop, pDim2.mRgt, pDim2.mBot);
}
bool IsCollisionExclusive(float pLft, float pTop, float pRgt, float pBot, float pLft2, float pTop2, float pRgt2, float pBot2)
{
    //--Same as the other IsCollision sets, except a hit on the exact border is ignored.
    if(pLft  > pRgt)  SwapF(pLft,  pRgt);
    if(pTop  > pBot)  SwapF(pTop,  pBot);
    if(pLft2 > pRgt2) SwapF(pLft2, pRgt2);
    if(pTop2 > pBot2) SwapF(pTop2, pBot2);

    //--First, run the horizontal check with exclusivity:
    if((((pLft >  pLft2) && (pLft <  pRgt2))  ||
       (( pRgt >  pLft2) && (pRgt <  pRgt2))  ||
       (( pLft >  pLft2) && (pRgt <  pRgt2))  ||
       (( pLft <  pLft2) && (pRgt >  pRgt2))) &&
       (((pTop >= pTop2) && (pTop <= pBot2))  ||
       (( pBot >= pTop2) && (pBot <= pBot2))  ||
       (( pTop >= pTop2) && (pBot <= pBot2))  ||
       (( pTop <= pTop2) && (pBot >= pBot2))))
    {
        return true;
    }

    //--Now run the vertical check. If both fail, the result is false.
    return ((((pLft >= pLft2) && (pLft <= pRgt2))  ||
            (( pRgt >= pLft2) && (pRgt <= pRgt2))  ||
            (( pLft >= pLft2) && (pRgt <= pRgt2))  ||
            (( pLft <= pLft2) && (pRgt >= pRgt2))) &&
            (((pTop >  pTop2) && (pTop <  pBot2))  ||
            (( pBot >  pTop2) && (pBot <  pBot2))  ||
            (( pTop >  pTop2) && (pBot <  pBot2))  ||
            (( pTop <  pTop2) && (pBot >  pBot2))));
}

//--Strict rectangular collision.
bool IsCollisionStrict(int lft1, int top1, int rgt1, int bot1, int lft2, int top2, int rgt2, int bot2)
{
    //--Case A:  1 is totally within 2.
    if(lft1 >= lft2 && lft1 <= rgt2 && rgt1 >= lft1 && rgt1 <= rgt2 &&
       top1 >= top2 && top1 <= bot2 && bot1 >= top1 && bot1 <= bot2)
    {
        return true;
    }

    //--Case B:  2 is totally within 1.
    if(lft2 >= lft1 && lft2 <= rgt1 && rgt2 >= lft2 && rgt2 <= rgt1 &&
       top2 >= top1 && top2 <= bot1 && bot2 >= top2 && bot2 <= bot1)
    {
        return true;
    }

    //--Case C:  Neither, no strict collision.
    return false;
}
bool IsCollisionStrict(ThreeDimensionReal pDim1, int lft2, int top2, int rgt2, int bot2)
{
    return IsCollisionStrict(pDim1.mLft, pDim1.mTop, pDim1.mRgt, pDim1.mBot, lft2, top2, rgt2, bot2);
}
bool IsCollisionStrict(TwoDimensionReal pDim1, int lft2, int top2, int rgt2, int bot2)
{
    return IsCollisionStrict(pDim1.mLft, pDim1.mTop, pDim1.mRgt, pDim1.mBot, lft2, top2, rgt2, bot2);
}
bool IsCollisionStrict(ThreeDimensionReal pDim1, ThreeDimensionReal pDim2)
{
    return IsCollisionStrict(pDim1.mLft, pDim1.mTop, pDim1.mRgt, pDim1.mBot, pDim2.mLft, pDim2.mTop, pDim2.mRgt, pDim2.mBot);
}

//--Percentage of overlap
float PercentOverlap(int pLft1, int pTop1, int pRgt1, int pBot1, int pLft2, int pTop2, int pRgt2, int pBot2)
{
    //--Make sure there's a collision to begin with.
    if(!IsCollision(pLft1, pTop1, pRgt1, pBot1, pLft2, pTop2, pRgt2, pBot2)) return 0.0f;

    //--Calc area of Rect1.  Rect2's area is never checked.
    float tArea1 = (pRgt1 - pLft1) * (pBot1 - pTop1);
    if(tArea1 <= 0.0f) return 0.0f;

    //--Determine the boundaries of the crossover.  There are four possible combinations.
    float tLftBnd, tTopBnd, tRgtBnd, tBotBnd;
    if(pRgt2 >= pRgt1)
    {
        tLftBnd = pLft2;
        tRgtBnd = pRgt1;
    }
    else
    {
        tLftBnd = pLft1;
        tRgtBnd = pRgt2;
    }
    if(pBot2 >= pBot1)
    {
        tTopBnd = pTop2;
        tBotBnd = pBot1;
    }
    else
    {
        tTopBnd = pTop1;
        tBotBnd = pBot2;
    }

    //--Calc the size of this new rectangle.
    float tAreaC = (tRgtBnd - tLftBnd) * (tBotBnd - tTopBnd);

    //--Percentage cross is always returned in terms of the 1 rectangle.
    float tPercent = tAreaC / tArea1;
    if(tPercent >= 1.0f) tPercent = 1.0f;
    return tPercent;
}
float PercentOverlap(ThreeDimensionReal pDim1, int pLft2, int pTop2, int pRgt2, int pBot2)
{
    return PercentOverlap(pDim1.mLft, pDim1.mTop, pDim1.mRgt, pDim1.mBot, pLft2, pTop2, pRgt2, pBot2);
}

//--[Polygons]
//--Computes the area of any polygon, regular or irregular.
/*
float AreaOfPoly(float *pPolygon)
{
    //--Error check.
    if(!pPolygon) return 0.0f;

    //--Get the number of sides.
    int tSides = (int)pPolygon[0];

    //--Storage.
    float tXToYSum = 0.0f;
    float tYToXSum = 0.0f;

    //--For each point, computer x*ynext and y*xnext.
    for(int i = 0; i < tSides; i ++)
    {
        //--Resolve the current point
        float tCurX = pPolygon[(i*2)+1];
        float tCurY = pPolygon[(i*2)+2];

        //--Resolve the next array point. Loop back to the start if we go over.
        float tNexX, tNexY;
        if(i < tSides-1)
        {
            tNexX = pPolygon[((i+1)*2)+1];
            tNexY = pPolygon[((i+1)*2)+2];
        }
        //--Going over.
        else
        {
            tNexX = pPolygon[1];
            tNexY = pPolygon[2];
        }

        //--Multiply.
        tXToYSum = tXToYSum + (tCurX * tNexY);
        tYToXSum = tYToXSum + (tCurY * tNexX);
    }

    //--Final counts.
    return (tXToYSum - tYToXSum) / 2.0f;
}*/
#include "SugarLinkedList.h"
#include "WadStructures.h"
float AreaOfPoly(SugarLinkedList *pPolygon)
{
    //--Same algorithm, but uses a SugarLinkedList which contains the vertices as WAD_VERTEX objects.
    if(!pPolygon) return 0.0f;

    //--Storage.
    float tXToYSum = 0.0f;
    float tYToXSum = 0.0f;

    //--For each member...
    int tListSize = pPolygon->GetListSize();
    for(int i = 0; i < tListSize; i ++)
    {
        //--Get the current member and next member.
        UDMFVertex *rCurrent = (UDMFVertex *)pPolygon->GetElementBySlot(i);
        UDMFVertex *rNext    = (UDMFVertex *)pPolygon->GetElementBySlot((i+1) % tListSize);

        //--Multiply.
        tXToYSum = tXToYSum + (float)(rCurrent->mX * rNext->mY);
        tYToXSum = tYToXSum + (float)(rCurrent->mY * rNext->mX);
    }

    //--Final counts.
    return fabsf((tXToYSum - tYToXSum) / 2.0f);
}
float AreaOfPolyWithout(SugarLinkedList *pPolygon, int pIgnoreThisSlot)
{
    //--Computes the area of a polygon, but ignores the provided vertex. This is assuming the polygon is still
    //  legal without said vertex!
    if(!pPolygon) return 0.0f;

    //--Storage.
    float tXToYSum = 0.0f;
    float tYToXSum = 0.0f;

    //--For each member...
    int tListSize = pPolygon->GetListSize();
    for(int i = 0; i < tListSize; i ++)
    {
        //--Ignore this slot.
        if(pIgnoreThisSlot == i)
        {
            continue;
        }
        //--Normal case.
        else
        {
            //--Get the current member and next member.
            UDMFVertex *rCurrent = (UDMFVertex *)pPolygon->GetElementBySlot(i);
            UDMFVertex *rNext    = (UDMFVertex *)pPolygon->GetElementBySlot((i+1) % tListSize);
            if(((i+1) % tListSize) == pIgnoreThisSlot) rNext = (UDMFVertex *)pPolygon->GetElementBySlot((i+2) % tListSize);

            //--Multiply.
            tXToYSum = tXToYSum + (float)(rCurrent->mX * rNext->mY);
            tYToXSum = tYToXSum + (float)(rCurrent->mY * rNext->mX);
        }
    }

    //--Final counts.
    return fabsf((tXToYSum - tYToXSum) / 2.0f);
}
float AreaOfTriangle(float mXA, float mYA, float mXB, float mYB, float mXC, float mYC)
{
    //--Same as AreaOfPoly but with a known number of vertices.
    float tXToYSum = (mXA * mYB) + (mXB * mYC) + (mXC * mYA);
    float tYToXSum = (mYA * mXB) + (mYB * mXC) + (mYC * mXA);
    return fabsf((tXToYSum - tYToXSum) / 2.0f);
}

//--Point within Polygon
bool IsPolyCollisionRightHand(float pX, float pY, float *pArray)
{
    //--Returns true if the object is within the provided polygon.  The polygon
    //  is considered to be a regular polygon, and follows the right-hand rule.
    //--The 0th entry of the array is how many sides the polygon has.
    if(!pArray) return false;

    //--For each point...
    int tSides = (int)pArray[0];
    for(int i = 0; i < tSides; i ++)
    {
        //--Resolve the current point
        float tCurX = pArray[(i*2)+1];
        float tCurY = pArray[(i*2)+2];

        //--Resolve the next point.  If we're at tSides-1, then use the 0th
        //  point, otherwise use the next point.
        float tNexX, tNexY;
        if(i != tSides-1)
        {
            tNexX = pArray[(i*2)+3];
            tNexY = pArray[(i*2)+4];
        }
        else
        {
            tNexX = pArray[1];
            tNexY = pArray[2];
        }

        //--Get the angles between the two.  There is an optimization which
        //  only requires the slope and complex switch statements, nuts to that.
        float tNextAngle  = (atan2f(tNexY - tCurY, tNexX - tCurX) * TODEGREE);
        float tPointAngle = (atan2f(tNexY -    pY, tNexX -    pX) * TODEGREE);
        while(tNextAngle < -180.0f)  tNextAngle = tNextAngle + 360.0f;
        while(tPointAngle < -180.0f) tPointAngle = tPointAngle + 360.0f;

        //--A line is right-handed to another if it within +180 degrees, and
        //  left-handed if within -180 degrees.  We need to account for the 0
        //  wrap though.
        if(tNextAngle >= tPointAngle && tNextAngle - 180.0f <= tPointAngle)
        {
            //--Match, do nothing
        }
        else if(tNextAngle >= tPointAngle-360.0f && tNextAngle - 180.0f <= tPointAngle-360.0f)
        {
            //--Match, do nothing
        }
        else
        {
            //--Not within the specified zone, fail.
            return false;
        }
    }
    return true;
}
bool IsPolyCollisionRightHand(float pX, float pY, PolygonalHitbox pHitbox)
{
    //--Macro of above, packs the hitbox into an array.
    SetMemoryData(__FILE__, __LINE__);
    float *tArray = (float *)starmemoryalloc(sizeof(float) * (pHitbox.mPointsTotal * 2 + 1));
    tArray[0] = (float)pHitbox.mPointsTotal;
    for(int i = 0; i < pHitbox.mPointsTotal; i ++)
    {
        tArray[i*2+1] = pHitbox.mXVertices[i];
        tArray[i*2+2] = pHitbox.mYVertices[i];
    }

    bool tResult = IsPolyCollisionRightHand(pX, pY, tArray);
    free(tArray);
    return tResult;
}

//--Distance Calculations
float GetAngleBetween(float pX1, float pY1, float pX2, float pY2)
{
    return atan2f(pY2 - pY1, pX2 - pX1);
}
float GetPlanarDistance(float pX1, float pY1, float pX2, float pY2)
{
    return sqrtf(((pX1 - pX2) * (pX1 - pX2)) + ((pY1 - pY2) * (pY1 - pY2)));
}
float Get3DDistance(float pX1, float pY1, float pZ1, float pX2, float pY2, float pZ2)
{
    return sqrtf(((pX1 - pX2) * (pX1 - pX2)) + ((pY1 - pY2) * (pY1 - pY2)) + ((pZ1 - pZ2) * (pZ1 - pZ2)));
}
float Get3DDistance(Point3D pPointA, float pX2, float pY2, float pZ2)
{
    return sqrtf(((pPointA.mX - pX2) * (pPointA.mX - pX2)) + ((pPointA.mY - pY2) * (pPointA.mY - pY2)) + ((pPointA.mZ - pZ2) * (pPointA.mZ - pZ2)));
}
float Get3DDistance(Point3D pPointA, Point3D pPointB)
{
    return sqrtf(((pPointA.mX - pPointB.mX) * (pPointA.mX - pPointB.mX)) + ((pPointA.mY - pPointB.mY) * (pPointA.mY - pPointB.mY)) + ((pPointA.mZ - pPointB.mZ) * (pPointA.mZ - pPointB.mZ)));
}

//--[General Purpose Polygons]
//#define _SHOW_GL_DEBUG_
//#define _GET_EXTRA_ACCURACY_
void ProjectOntoAxis(float &sMin, float &sMax, float *pPolygon, float pNormalAngle)
{
    //--Given a particular polygon, projects it onto a given axis.  Because we only need to know
    //  if there is a collision along that axis, it is only necessary to get the X component.
    //--If the GL flag is on, we can score the Y component to make the render easier to see.
    int tPoints = (int)pPolygon[0];
    if(tPoints < 3) return;

    //--For each point on the polygon...
    #ifdef _SHOW_GL_DEBUG_
    glColor3f(0.0f, 0.0f, 1.0f);
    glBegin(GL_LINE_LOOP);
    #endif
    for(int i = 0; i < tPoints; i ++)
    {
        //--Figure out where the X value is stored.
        int tSlot = i*2+1;

        //--Optimized rotater.  We know the origin is zero, so the formulas are sped up.
        float tDistance = GetPlanarDistance(pPolygon[tSlot+0], pPolygon[tSlot+1], 0, 0);
        float tAngle = atan2(pPolygon[tSlot+1], pPolygon[tSlot+0]);
        tAngle = tAngle + pNormalAngle;
        float tX = cos(tAngle) * tDistance;
        #ifdef _SHOW_GL_DEBUG_
        float tY = sin(tAngle) * tDistance;
        glVertex2f(tX, tY);
        #endif

        //--Zeroth pass, always set.
        if(i == 0)
        {
            sMin = tX;
            sMax = tX;
        }
        //--Normal case
        else
        {
            if(tX < sMin) sMin = tX;
            if(tX > sMax) sMax = tX;
        }
    }
    glEnd();
}

#define DEG90INRAD 3.1415926f / 2.0f
#define DEG45INRAD 3.1415926f / 4.0f
#ifdef _SHOW_GL_DEBUG_
#include "DisplayManager.h"
#endif
bool IsCollisionPoly(float *pPolyA, float *pPolyB)
{
    //--This is a general-purpose polygon collision detection method using SAT (Seperating Axis Theorem)
    //  which is BOOLEAN in nature - I am only concerned if a polygon is colliding with another.
    //--Polygons are described as arrays.  The 0th member of the array describes how many points are
    //  in the array.  After that, they are pairs of X,Y point descriptions.
    //--The function WILL CRASH if an invalid polygon is passed in, we are concerned about speed.
    //  Do any checks necessary to ensure polygon validity before calling this.
    //--A polygon must be a CONVEX, CLOCKWISE WINDED polygon to function correctly.

    //--Debug Setup
    #ifdef _SHOW_GL_DEBUG_
    DisplayManager::StdModelPush();
    glTranslatef(400, 300, 0);
    glScalef(0.5f, 0.5f, 0.5f);
    #endif
    //fprintf(stderr, "Testing:\n");
    bool tSuccess = true;

    //--Get the points total for each polygon.
    int tPointsOnA = (int)pPolyA[0];
    int tPointsOnB = (int)pPolyB[0];

    //--Variables
    int tSlot1;
    int tSlot2;
    float tMinA, tMaxA;
    float tMinB, tMaxB;

    //--Check all points on A.
    for(int i = 0; i < tPointsOnA; i ++)
    {
        //--Setup
        tSlot1 = i*2+1;
        tSlot2 = (i+1)*2+1;
        if(i == tPointsOnA-1)
        {
            tSlot2 = 1;
        }

        //--Get the normal.
        float tAngle = atan2(pPolyA[tSlot2+1] - pPolyA[tSlot1+1], pPolyA[tSlot2] - pPolyA[tSlot1]);
        float tNormal = tAngle + DEG90INRAD;

        //--Project.
        ProjectOntoAxis(tMinA, tMaxA, pPolyA, tNormal);
        ProjectOntoAxis(tMinB, tMaxB, pPolyB, tNormal);

        //--Check crossing.  If they don't cross, we're done.  If they do, continue.
        if((tMinA >= tMinB && tMinA <= tMaxB) ||
           (tMaxA >= tMinB && tMaxA <= tMaxB))
        {

        }
        else
        {
            #ifndef _SHOW_GL_DEBUG_
            return false;
            #else
            tSuccess = false;
            #endif
        }

        //--Project again for extra accuracy.  This is only necessary for non space-filling polygons
        //  of at least 3 sides.  Set the #define if you don't need this behavior.
        #ifdef _GET_EXTRA_ACCURACY_
        tNormal = tAngle + DEG45INRAD;

        //--Project.
        ProjectOntoAxis(tMinA, tMaxA, pPolyA, tNormal);
        ProjectOntoAxis(tMinB, tMaxB, pPolyB, tNormal);

        //--Check crossing.  If they don't cross, we're done.  If they do, continue.
        if((tMinA >= tMinB && tMinA <= tMaxB) ||
           (tMaxA >= tMinB && tMaxA <= tMaxB))
        {

        }
        else
        {
            #ifndef _SHOW_GL_DEBUG_
            return false;
            #else
            tSuccess = false;
            #endif
        }
        #endif

        #ifdef _SHOW_GL_DEBUG_
        //--[Render the normal]
        float tMidX = (pPolyA[tSlot2+0] + pPolyA[tSlot1+0]) / 2.0f;
        float tMidY = (pPolyA[tSlot2+1] + pPolyA[tSlot1+1]) / 2.0f;
        float tNormalStartX = tMidX + (cos(tNormal) * 35.0f);
        float tNormalStartY = tMidY + (sin(tNormal) * 35.0f);
        glColor3f(1.0f, 1.0f, 0.0f);
        glBegin(GL_LINES);
            glVertex2f(tMidX, tMidY);
            glVertex2f(tNormalStartX, tNormalStartY);
        glEnd();

        //--Render a line representing the max and min positions.
        DisplayManager::StdModelPush();
        glColor3f(1.0f, 0.0f, 1.0f);
        //glTranslatef(400.0f, 300.0f, 0.f);
        //glRotatef(tAngle * TODEGREE, 0.0f, 0.0f, 1.0f);
        //glTranslatef(-400.0f, -300.0f, 0.f);
        glBegin(GL_LINES);
            glVertex2f(tMinA, 0);
            glVertex2f(tMaxA, 0);
        glEnd();
        glColor3f(0.0f, 1.0f, 1.0f);
        glBegin(GL_LINES);
            glVertex2f(tMinB, 0);
            glVertex2f(tMaxB, 0);
        glEnd();
        DisplayManager::StdModelPop();
        #endif
    }

    //--Check all points on B.
    for(int i = 0; i < tPointsOnB; i ++)
    {
        //--Setup
        tSlot1 = i*2+1;
        tSlot2 = (i+1)*2+1;
        if(i == tPointsOnB-1)
        {
            tSlot2 = 1;
        }

        //--Get the normal.  It doesn't matter if it's normalized.
        float tAngle = atan2(pPolyB[tSlot2+1] - pPolyB[tSlot1+1], pPolyB[tSlot2] - pPolyB[tSlot1]);
        float tNormal = tAngle + DEG90INRAD;

        //--Project.
        ProjectOntoAxis(tMinA, tMaxA, pPolyA, tNormal);
        ProjectOntoAxis(tMinB, tMaxB, pPolyB, tNormal);

        //--Check crossing.  If they don't cross, we're done.  If they do, continue.
        if((tMinA >= tMinB && tMinA <= tMaxB) ||
           (tMaxA >= tMinB && tMaxA <= tMaxB))
        {

        }
        else
        {
            #ifndef _SHOW_GL_DEBUG_
            return false;
            #else
            tSuccess = false;
            #endif
        }

        //--Project again for extra accuracy.
        #ifdef _GET_EXTRA_ACCURACY_
        tNormal = tAngle + DEG45INRAD;

        //--Project.
        ProjectOntoAxis(tMinA, tMaxA, pPolyA, tNormal);
        ProjectOntoAxis(tMinB, tMaxB, pPolyB, tNormal);

        //--Check crossing.  If they don't cross, we're done.  If they do, continue.
        if((tMinA >= tMinB && tMinA <= tMaxB) ||
           (tMaxA >= tMinB && tMaxA <= tMaxB))
        {

        }
        else
        {
            #ifndef _SHOW_GL_DEBUG_
            return false;
            #else
            tSuccess = false;
            #endif
        }
        #endif

        #ifdef _SHOW_GL_DEBUG_
        //--[Render the normal]
        float tMidX = (pPolyB[tSlot2+0] + pPolyB[tSlot1+0]) / 2.0f;
        float tMidY = (pPolyB[tSlot2+1] + pPolyB[tSlot1+1]) / 2.0f;
        float tNormalStartX = tMidX + (cos(tNormal) * 35.0f);
        float tNormalStartY = tMidY + (sin(tNormal) * 35.0f);
        glColor3f(1.0f, 1.0f, 0.0f);
        glBegin(GL_LINES);
            glVertex2f(tMidX, tMidY);
            glVertex2f(tNormalStartX, tNormalStartY);
        glEnd();

        //--Render a line representing the max and min positions.
        DisplayManager::StdModelPush();
        glColor3f(1.0f, 0.0f, 1.0f);
        //glTranslatef(400.0f, 300.0f, 0.f);
        //glRotatef(tAngle * TODEGREE, 0.0f, 0.0f, 1.0f);
        //glTranslatef(-400.0f, -300.0f, 0.f);
        glBegin(GL_LINES);
            glVertex2f(tMinA, 0);
            glVertex2f(tMaxA, 0);
        glEnd();
        glColor3f(0.0f, 1.0f, 1.0f);
        glBegin(GL_LINES);
            glVertex2f(tMinB, 0);
            glVertex2f(tMaxB, 0);
        glEnd();
        DisplayManager::StdModelPop();
        #endif
    }
    #ifdef _SHOW_GL_DEBUG_
    DisplayManager::StdModelPop();
    #endif

    //--If we got this far, all cases crossed!  That's a collision.
    return tSuccess;
}

//--[HomeInOnAngle]
//--Given a starting angle, moves along the shortest path towards the targer angle. Returns the resultant angle.
//  This version is in Radians.
float HomeInOnAngleRad(float pStartAngle, float pTargetAngle, float pEpsilon)
{
    //--Constants.
    float c2Pi = 3.1415926f * 2.0f;
    float cHPi = 3.1415926f * 0.5f;

    //--Special: If the epsilon is half a circle, then we can always immediately turn that far.
    //  Also fail if the epsilon is somehow negative.
    if(pEpsilon <= 0.0f || pEpsilon >= 3.1415926f) return pTargetAngle;

    //--Clamp both angles within the 2pi range.
	while(pStartAngle  < 0)    pStartAngle  = pStartAngle  + c2Pi;
	while(pStartAngle  > c2Pi) pStartAngle  = pStartAngle  - c2Pi;
	while(pTargetAngle < 0)    pTargetAngle = pTargetAngle + c2Pi;
	while(pTargetAngle > c2Pi) pTargetAngle = pTargetAngle - c2Pi;

	//--Angles are very close, so set them to equal and be done with it.
	float cfAbsDist = fabsf(pStartAngle - pTargetAngle);
	while(cfAbsDist  < 0)    cfAbsDist  = cfAbsDist  + c2Pi;
	while(cfAbsDist  > c2Pi) cfAbsDist  = cfAbsDist  - c2Pi;
	if(cfAbsDist <= pEpsilon) return pTargetAngle;

    //--If the target angle is counterclockwise:
    if(pStartAngle > pTargetAngle)
    {
        //--If the start angle is near 2pi and the target is near zero, then this is clockwise case:
        if(pStartAngle > 3.1415926f * 1.50f && pTargetAngle < cHPi)
        {
            return pStartAngle + pEpsilon;
        }
        //--All other cases are counterclockwise:
        else
        {
            return pStartAngle - pEpsilon;
        }
    }
    //--Target angle is clockwise:
    else
    {
        //--If the target angle is near 2pi and the start is near zero, then this is counterclockwise case:
        if(pTargetAngle > 3.1415926f * 1.50f && pStartAngle < cHPi)
        {
            return pStartAngle - pEpsilon;
        }
        //--All other cases are clockwise:
        else
        {
            return pStartAngle + pEpsilon;
        }
    }

    //--Not logically possible to reach this point. Some compilers freak out though.
    return pStartAngle;
}
//--As above but modified to be in degrees.
float HomeInOnAngleDeg(float pStartAngle, float pTargetAngle, float pEpsilon)
{
    return HomeInOnAngleRad(pStartAngle * TORADIAN, pTargetAngle * TORADIAN, pEpsilon * TORADIAN) * TODEGREE;
}
