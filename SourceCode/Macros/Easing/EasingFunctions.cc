#include "EasingFunctions.h"
#include "GlDfn.h"

//--[Normal Functions]
//--Easing functions with the minimum argument lists needed. It is considered implicit that the
//  percentage should not range further than 0.0f and 1.0f.
float EasingFunction::Linear(float pPercent)
{
    //--Linear: All percentages are equally distributed along the line.
    return pPercent;
}
float EasingFunction::QuadraticIn(float pPercent)
{
    //--Quadratic Easing In: Accelerates up from no velocity to slam into the end point.
    return (pPercent * pPercent);
}
float EasingFunction::QuadraticOut(float pPercent)
{
    //--Quadratic Easing Out: Decelerates to zero velocity, landing gently at the target.
    return 1.0f - ((1.0f - pPercent) * (1.0f - pPercent));
}
float EasingFunction::QuadraticInOut(float pPercent)
{
    //--Combines the in and out behavior into a function that's fast in the middle, slow on the edges.
    //  Dat's hot.
    pPercent = pPercent * 2.0f;

    //--For the first half, use QuadraticIn().
    if(pPercent < 1.0f)
    {
        return (pPercent * pPercent) / 2.0f;
    }
    //--Otherwise, use QuadraticOut(). This is frightfully complicated.
    pPercent = pPercent - 1.0f;
    return (1.0f - ((1.0f - pPercent) * (1.0f - pPercent) - 1.0f)) * 0.5f;
}
float EasingFunction::QuarticIn(float pPercent)
{
    return pPercent * pPercent * pPercent * pPercent;
}
float EasingFunction::QuarticOut(float pPercent)
{
    float t = 1.0f - pPercent;
    return 1.0f - (t * t * t * t);
}
float EasingFunction::QuarticInOut(float pPercent)
{
    //--In-Out using a quartic function.
    pPercent = pPercent * 2.0f;

    //--For the first half, use QuarticIn().
    if(pPercent < 1.0f)
    {
        return (pPercent * pPercent * pPercent * pPercent) / 2.0f;
    }
    //--Otherwise, use QuarticOut(). This is frightfully complicated.
    pPercent = pPercent - 1.0f;
    float t = 1.0f - pPercent;
    return (1.0f - (t * t * t * t - 1.0f)) * 0.5f;
}
float EasingFunction::SinusoidalIn(float pPercent)
{
    //--Uses a simple cos-wave.
    return 1.0f - cosf(pPercent * 3.1415926f * 0.5f);
}
float EasingFunction::SinusoidalOut(float pPercent)
{
    //--Uses a simple sin-wave.
    return sinf(pPercent * 3.1415926f * 0.5f);
}
float EasingFunction::SinusoidalInOut(float pPercent)
{
    //--Uses a hybrid sin-wave.
    return (cosf(pPercent * 3.1415926f) - 1.0f) * -1.0f * 0.5f;
}

//--[Overloads]
//--These overloads provide the same functions with different argument lists. The Numerator/Denominator
//  cases allow the user to calculate the percent without worrying about DIV0 errors.
float EasingFunction::Linear(float pNumerator, float pDenominator)
{
    if(pNumerator < 0.0f) return 0.0f;
    if(pDenominator == 0.0f) return 0.0f;
    if(pNumerator >= pDenominator) return 1.0f;
    return Linear(pNumerator / pDenominator);
}
float EasingFunction::QuadraticIn(float pNumerator, float pDenominator)
{
    //--Quadratic Easing In: Accelerates up from no velocity to slam into the end point.
    if(pNumerator < 0.0f) return 0.0f;
    if(pDenominator == 0.0f) return 1.0f;
    if(pNumerator >= pDenominator) return 1.0f;
    return QuadraticIn(pNumerator / pDenominator);
}
float EasingFunction::QuadraticOut(float pNumerator, float pDenominator)
{
    //--Quadratic Easing Out: Decelerates to zero velocity, landing gently at the target.
    if(pNumerator < 0.0f) return 0.0f;
    if(pDenominator == 0.0f) return 1.0f;
    if(pNumerator >= pDenominator) return 1.0f;
    return QuadraticOut(pNumerator / pDenominator);
}
float EasingFunction::QuadraticInOut(float pNumerator, float pDenominator)
{
    //--Combines the in and out behavior into a function that's fast in the middle, slow on the edges.
    //  Dat's hot.
    if(pNumerator < 0.0f) return 0.0f;
    if(pDenominator == 0.0f) return 1.0f;
    if(pNumerator >= pDenominator) return 1.0f;
    return QuadraticInOut(pNumerator / pDenominator);
}
float EasingFunction::QuarticIn(float pNumerator, float pDenominator)
{
    //--Quadratic Easing In: Accelerates up from no velocity to slam into the end point.
    if(pNumerator < 0.0f) return 0.0f;
    if(pDenominator == 0.0f) return 1.0f;
    if(pNumerator >= pDenominator) return 1.0f;
    return QuarticIn(pNumerator / pDenominator);
}
float EasingFunction::QuarticOut(float pNumerator, float pDenominator)
{
    //--Quadratic Easing Out: Decelerates to zero velocity, landing gently at the target.
    if(pNumerator < 0.0f) return 0.0f;
    if(pDenominator == 0.0f) return 1.0f;
    if(pNumerator >= pDenominator) return 1.0f;
    return QuarticOut(pNumerator / pDenominator);
}
float EasingFunction::QuarticInOut(float pNumerator, float pDenominator)
{
    //--Combines the in and out behavior into a function that's fast in the middle, slow on the edges.
    //  Dat's hot.
    if(pNumerator < 0.0f) return 0.0f;
    if(pDenominator == 0.0f) return 1.0f;
    if(pNumerator >= pDenominator) return 1.0f;
    return QuarticInOut(pNumerator / pDenominator);
}
float EasingFunction::SinusoidalIn(float pNumerator, float pDenominator)
{
    //--Uses a simple cos-wave.
    if(pDenominator == 0.0f) return 1.0f;
    if(pNumerator >= pDenominator) return 1.0f;
    return SinusoidalIn(pNumerator / pDenominator);
}
float EasingFunction::SinusoidalOut(float pNumerator, float pDenominator)
{
    //--Uses a simple sin-wave.
    if(pDenominator == 0.0f) return 1.0f;
    if(pNumerator >= pDenominator) return 1.0f;
    return SinusoidalOut(pNumerator / pDenominator);
}
float EasingFunction::SinusoidalInOut(float pNumerator, float pDenominator)
{
    //--Uses a hybrid sin-wave.
    if(pDenominator == 0.0f) return 1.0f;
    if(pNumerator >= pDenominator) return 1.0f;
    return SinusoidalInOut(pNumerator / pDenominator);
}
