//--[GUIExec_ShowHideObject]
//--Locates the Object given and shows/hides it.  Simple!

#pragma once

#include "Definitions.h"
#include "Structures.h"
#include "RootExecutor.h"
#include "GUI.h"
#include "GUIBaseObject.h"

class GUIExec_ShowHideObject : public RootExecutor
{
    private:
    char *mObjectName;
    bool mNewVisibility;

    protected:

    public:
    //--System
    GUIExec_ShowHideObject()
    {
        mObjectName = NULL;
        mNewVisibility = true;
    }
    virtual ~GUIExec_ShowHideObject()
    {
        free(mObjectName);
        mObjectName = NULL;
    }

    //--Manipulators
    void SetName(const char *pName)
    {
        ResetString(mObjectName, pName);
    }
    void SetNewVisibility(bool pFlag)
    {
        mNewVisibility = pFlag;
    }

    //--Executor
    virtual void Execute()
    {
        GUIBaseObject *rObject = GraphicalUserInterface::Fetch()->FindGUIObject(mObjectName);
        if(rObject) rObject->SetVisibility(mNewVisibility);
    }
};
