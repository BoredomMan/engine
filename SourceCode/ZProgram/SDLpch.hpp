//--[PCH]
//--Precompiled Header. If you don't know what that means:
//  A PCH contains the header information from all the headers within and compiles them. So long
//  as they do not change, the compiler will not need to recompile these headers when they are
//  included in other files. This speeds up compilation times *considerably* but the pch files
//  are 1) large and 2) take a while to recompile. Only system libraries should be in the PCH.
//--Since the file is precompiled, the pragma directive spits an error. Leave the header guards.
//--This is the SDL version of the PCH.

#ifndef _PCH_H_
#define _PCH_H_

//--[SDL Headers]
#if defined _SDL_PROJECT_
    #include <SDL.h>
    #include <SDL2/SDL_image.h>

    #if defined _TARGET_OS_WINDOWS_
        #include <gl/gl.h>
        #include <gl/glu.h>
        #include <gl/glext.h>
    #endif
    #if defined _TARGET_OS_MAC_
        #include <osxgl.h>
        #include <osxglu.h>
        #include <osxglext.h>
    #endif
#endif

//--[Stdlib Headers]
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <vector>
#include <fstream>
#include <iostream>
#include <string.h>
#include <assert.h>
#include <math.h>
#include <stdint.h>
#include <time.h>

//--[Lua]
extern "C"
{
#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"
}

//--[Audio]
#include "bass.h"

//--[Color]
//--Included in almost every file. StarlightColor is considered a basic engine part.
#include "StarlightColor.h"

//--[Memory Manager]
//--Static class, can be accessed from anywhere.
#include "MemoryManager.h"

#endif // _PCH_H_

