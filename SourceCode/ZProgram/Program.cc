//--Base
#include "Program.h"

//--Classes
//--CoreClasses
#include "SugarFont.h"

//--Definitions
#include "GameGear.h"
#include "Global.h"
#include "Startup.h"
#include "TakeDown.h"

//--GUI
//--Libraries
//--Managers
#include "ControlManager.h"
#include "DebugManager.h"
#include "OptionsManager.h"
#include "SteamManager.h"

//--OSX Only
#if defined _TARGET_OS_MAC_
    #include <unistd.h>
#endif

int main(int pArgsTotal, char **pArgs)
{
    //--Set all the Global variables to their default values.
    GLOBAL *rGlobal = Global::Shared();
    InitializeGlobals();

    //--On OSX, change the working directory.
    #if defined _TARGET_OS_MAC_

        //--Setup.
        char tSwapDirectory[512];
        strcpy(tSwapDirectory, pArgs[0]);

        //--Scan backwards, remove letters until a slash is hit.
        for(int i = strlen(tSwapDirectory) - 1; i >= 0; i --)
        {
            if(tSwapDirectory[i] == '/') break;
            tSwapDirectory[i] = '\0';
        }

        //--Set to correct directory.
        fprintf(stderr, "Swap Dir: %s\n", tSwapDirectory);
        int tReturn = chdir(tSwapDirectory);

    #endif

    //--Deal with the command line.
    HandleCommandLine(pArgsTotal, pArgs);

    //--Boot Steam.
    #if defined _STEAM_API_
    if(SteamAPI_RestartAppIfNecessary(SteamManager::xSteamAppID))
    {
        return 1;
    }
    #endif

    //--Boot Allegro/SDL/Bass/Whatever.
    if(!InitializeLibraries()) return 0;

    //--Boot all the managers and give them their basic resources.
    if(!InitializeManagers()) return 0;

    //--Event registration is only required for Allegro projects.
    #if defined _ALLEGRO_PROJECT_
        RegisterEvents(rGlobal->gEventQueue);
    #endif

    //--Load all resources necessary for basic functionality off the hard drive.
    LoadResources();

    //--Tie basic objects to their resources (Eg: The GUI).
    PostProcess();

    //--Program-specific events that occur after startup, such as showing the title screen.
    ProgramBoot();

    //--Variables structure for shuffling information between gears
    MainPackage mMainPackage;
    mMainPackage.ScreenNeedsUpdate = false;
    mMainPackage.FramesDoneThisTick = 0;
    mMainPackage.OldTime = 0.0f;
    mMainPackage.GameTime = 0.0f;

    //--Something went badly wrong in the init, so quit.
    if(rGlobal->gQuit) return 0;

    //--Store the time the program started at.
    #if defined _ALLEGRO_PROJECT_
    al_start_timer(rGlobal->gSpeedTimer);
    #endif
    mMainPackage.OldTime = GetGameTime();

    //--Wipe the state data in case something was in there from startup.
    ControlManager::Fetch()->NullifyStateData();

    //--[Main Program Loop]
    DebugManager::PushPrint(false, "[Begin Main Loop]\n");
    while(!rGlobal->gQuit)
    {
        DebugManager::PushPrint("Debug_MainLoop", "[Begin Main Iteration]\n");
        mMainPackage.StartTime = clock();

        //--[Allegro Main Loop]
        //--Allegro waits until an event occurs, which includes the timer events which regulate
        //  the ticks of the program.
        #if defined _ALLEGRO_PROJECT_
        {
            al_wait_for_event(rGlobal->gEventQueue, &rGlobal->gEvent);
        }
        //--[SDL Main Loop]
        //--SDL is quite similar to the Allegro version. SDL uses a threaded timer which pushes
        //  an event onto the stack every MILLISECONDSPERTICK milliseconds.
        #elif defined _SDL_PROJECT_
        {
            SDL_WaitEvent(&rGlobal->gEvent);
        }
        #endif

        //--The handling state is based on a function pointer.  To return to the standard state,
        //  just null the pointer!
        if(!rGlobal->rCurrentEventHandler) rGlobal->rCurrentEventHandler = &GameGear::EventHandler;
        rGlobal->rCurrentEventHandler(mMainPackage);
        DebugManager::Print("Handled Event\n");

        //--Drawing handling, same as event handling (state switch based on active gear).
        if(!rGlobal->rCurrentRenderHandler) rGlobal->rCurrentRenderHandler = &GameGear::RenderHandler;
        rGlobal->rCurrentRenderHandler(mMainPackage);
        DebugManager::Print("Handled Render\n");

        DebugManager::PopPrint("[Complete Main Iteration]\n");
    }

    //--[Options Saving]
    //--Before the program exits, store any values that may have changed due to hotkeys or options modification
    //  and write the savefiles.
    OptionsManager::Fetch()->WriteConfigFiles();

    //--[Take down the program]
    DebugManager::PopPrint("[End Main Loop]\n");
    Free_All_Memory();

    //--Final log operation.
    time_t tRawTime;
    struct tm *tTimeInfo;
    time (&tRawTime);
    tTimeInfo = localtime(&tRawTime);
    FILE *fOutfile = fopen("ErrorLog.txt", "a");
    fprintf(fOutfile, "%s - Program execution finished normally.\n", asctime(tTimeInfo));
    fclose(fOutfile);

    return 0;
}

//--FPS logging function used by the various Gears.
void LogFPS(MainPackage &sMainPackage)
{
    //--Get pointers
    GLOBAL *rGlobal = Global::Shared();

    //--Get the time of the previous tick.
    sMainPackage.GameTime = GetGameTime();

    //--If the time has passed one second...
    if(sMainPackage.GameTime - sMainPackage.OldTime >= 1.0)
    {
        rGlobal->gActualFPS = sMainPackage.FramesDoneThisTick / (sMainPackage.GameTime - sMainPackage.OldTime);
        rGlobal->gActualTPS = rGlobal->gTicksElapsed / (sMainPackage.GameTime - 1.0f);
        if(rGlobal->gWriteFPS) fprintf(stderr, "FPS: %f\n", rGlobal->gActualFPS);

        sMainPackage.FramesDoneThisTick = 0;
        sMainPackage.OldTime = sMainPackage.GameTime;
    }
    sMainPackage.FramesDoneThisTick ++;
}

//--[SDL Projects]
//--SDL does not automatically have a timer event, so this callback creates one. This callback
//  should be attached to an SDL timer object.
#if defined _SDL_PROJECT_
uint32_t Timer_Callback(uint32_t pInterval, void *pParameters)
{
    //--Setup.
    SDL_Event nEvent;
    SDL_UserEvent nUserEvent;

    //--Set up the userevent.
    nUserEvent.type = SDL_USEREVENT;
    nUserEvent.code = 0;
    nUserEvent.data1 = NULL;
    nUserEvent.data2 = NULL;

    //--Associate the event with the userevent.
    nEvent.type = SDL_USEREVENT;
    nEvent.user = nUserEvent;

    //--Push it onto the stack.
    SDL_PushEvent(&nEvent);
    return(pInterval);
}
#endif
