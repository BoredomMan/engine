//--Base
#include "SugarAnimation.h"

//--Classes
//--Definitions
//--Generics
//--GUI
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "DebugManager.h"

//=========================================== System ==============================================
SugarAnimation::SugarAnimation()
{
    //--[RootObject]
    mType = POINTER_TYPE_ANIMATION;

    //--[SugarAnimation]
    //--System
    mTotalFrames = 0;
    mFrameArray = NULL;

    //--Drawing Data
    mTicksPerFrame = 1.0f;

    //--Variable-rate Drawing
    mUseVariableRate = false;
    mTotalTicksToAnimate = 1;
    mTicksArray = NULL;
}
SugarAnimation::~SugarAnimation()
{
    free(mFrameArray);
    free(mTicksArray);
}

//====================================== Property Queries =========================================
int SugarAnimation::GetOneAnimationTicks()
{
    //--Returns how many ticks it will take to play one full sequence of the animation.
    return mTotalTicksToAnimate;
}
int SugarAnimation::GetFrameSlotByTimer(int pTimer)
{
    //--Given a specific input timer, returns the index of the corresponding frame.
    if(pTimer < 0) return 0;

    //--Make sure the timer doesn't roll over the edge of the frames.
    int tSlot = 0;
    int tTimer = pTimer % mTotalTicksToAnimate;

    //--Fixed-rate is simply a division.
    if(!mUseVariableRate)
    {
        tSlot = tTimer / mTicksPerFrame;
    }
    //--Variable rate may have frames that play for more ticks than others, so the only way to
    //  correctly calc which is visible is to run through the array.
    else
    {
        int i = 0;
        while(tTimer >= mTicksArray[i] && i < mTotalFrames)
        {
            tTimer = tTimer - mTicksArray[i];
            i ++;
        }
        tSlot = i;
    }
    return tSlot;
}

//========================================= Manipulators ==========================================
void SugarAnimation::Setup(int pTotalFrames)
{
    //--Clears off old data and sets how many frames are expected.  Pass 0 to
    //  clear the Animation.
    free(mFrameArray);
    mFrameArray = NULL;
    mTotalFrames = 0;

    if(pTotalFrames < 1) return;
    mTotalFrames = pTotalFrames;
    SetMemoryData(__FILE__, __LINE__);
    mFrameArray = (SugarBitmap **)starmemoryalloc(sizeof(SugarBitmap *) * mTotalFrames);
    for(int i = 0; i < mTotalFrames; i ++) mFrameArray[i] = NULL;
    mTotalTicksToAnimate = mTotalFrames * mTicksPerFrame;
}
void SugarAnimation::SetManualTiming(bool pFlag)
{
    //--Sets whether or not this Animation uses manual timing for each frame. Also resets the
    //  manual timing array.
    free(mTicksArray);
    mTicksArray = NULL;
    mUseVariableRate = pFlag;
    if(!mUseVariableRate || mTotalFrames == 0) return;

    SetMemoryData(__FILE__, __LINE__);
    mTicksArray = (int *)starmemoryalloc(sizeof(int) * mTotalFrames);
    for(int i = 0; i < mTotalFrames; i ++) mTicksArray[i] = 1;
}
void SugarAnimation::SetFrame(int pSlot, SugarBitmap *pFrame)
{
    //--Sets the frame to a specific bitmap.
    if(pSlot < 0 || pSlot >= mTotalFrames) return;
    mFrameArray[pSlot] = pFrame;
}
void SugarAnimation::SetFrame(int pSlot, SugarBitmap *pFrame, int pTicksThisFrame)
{
    //--Sets the frame, and how many ticks it should play.
    if(pSlot < 0 || pSlot >= mTotalFrames || !mTicksArray) return;
    mFrameArray[pSlot] = pFrame;
    mTicksArray[pSlot] = pTicksThisFrame;

    //--Count how many Animation frames total.
    mTotalTicksToAnimate = 0;
    for(int i = 0; i < mTotalFrames; i ++)
    {
        mTotalTicksToAnimate = mTotalTicksToAnimate + mTicksArray[i];
    }
}
void SugarAnimation::SetTicksPerFrame(float pTicksPerFrame)
{
    //--Sets how many ticks each frame should play for, when using a simplified Animation technique.
    if(pTicksPerFrame == 0.0f) pTicksPerFrame = 1.0f;
    mTicksPerFrame = pTicksPerFrame;
    mTotalTicksToAnimate = mTotalFrames * mTicksPerFrame;
}

//========================================= Core Methods ==========================================
//===================================== Private Core Methods ======================================
//============================================ Update =============================================
//=========================================== File I/O ============================================
//=========================================== Drawing =============================================
SugarBitmap *SugarAnimation::GetFrame(int pTimer)
{
    //--Returns a pointer to the bitmap that corresponds to the timer, or NULL on error.
    int tSlot = GetFrameSlotByTimer(pTimer);
    if(tSlot < 0 || tSlot >= mTotalFrames) return NULL;
    return mFrameArray[tSlot];
}
SugarBitmap *SugarAnimation::GetFrameBySlot(int pSlot)
{
    //--Returns a pointer to the bitmap with the corresponding slot, ignoring the timer. Returns
    //  NULL on error.
    if(pSlot < 0 || pSlot >= mTotalFrames) return NULL;
    return mFrameArray[pSlot];
}

//======================================= Pointer Routing =========================================
//====================================== Static Functions =========================================
//========================================= Lua Hooking ===========================================
void SugarAnimation::HookToLuaState(lua_State *pLuaState)
{
    /* Anim_Create(DLPath[])
       Creates a new Animation and pushes it as the active object.  Be sure
       to pop it once you're done with it. */
    lua_register(pLuaState, "Anim_Create", &Hook_Anim_Create);

    /* Anim_SetProperty("TotalFrames", Number)
       Anim_SetProperty("ManualTiming", Flag)
       Anim_SetProperty("Frame", DLPath[])
       Anim_SetProperty("TicksPerFrame", Number)
       Anim_SetProperty("Frame", Slot, DLPath[], TicksThisFrame)
       Sets the provided property. */
    lua_register(pLuaState, "Anim_SetProperty", &Hook_Anim_SetProperty);
}

//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
int Hook_Anim_Create(lua_State *L)
{
    //Anim_Create(DLPath[])
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    rDataLibrary->PushActiveEntity();

    //--Arg check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("Anim_Create");

    //--Create and push.
    SugarAnimation *nAnimation = new SugarAnimation();
    rDataLibrary->RegisterPointer(lua_tostring(L, 1), nAnimation, SugarAnimation::DeleteThis);
    rDataLibrary->rActiveObject = nAnimation;

    return 0;
}
int Hook_Anim_SetProperty(lua_State *L)
{
    //Anim_SetProperty("TotalFrames", Number)
    //Anim_SetProperty("ManualTiming", Flag)
    //Anim_SetProperty("Frame", Slot, DLPath[])
    //Anim_SetProperty("Frame", Slot, DLPath[], TicksThisFrame)
    //Anim_SetProperty("TicksPerFrame", Number)
    uint32_t tArgs = lua_gettop(L);
    if(tArgs < 1) return fprintf(stderr, "Anim_SetProperty: Failed, invalid argument list\n");

    //--Get and check the ActiveObject.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    if(!rDataLibrary->IsActiveValid(POINTER_TYPE_ANIMATION)) return fprintf(stderr, "Anim_SetProperty: Failed, invalid object\n");
    SugarAnimation *rAnimation = (SugarAnimation *)rDataLibrary->rActiveObject;

    const char *rTypeString = lua_tostring(L, 1);
    if(!strcmp(rTypeString, "TotalFrames") && tArgs == 2)
    {
        rAnimation->Setup(lua_tointeger(L, 2));
    }
    else if(!strcmp(rTypeString, "ManualTiming") && tArgs == 2)
    {
        rAnimation->SetManualTiming(lua_toboolean(L, 2));
    }
    else if(!strcmp(rTypeString, "Frame") && tArgs == 3)
    {
        SugarBitmap *rBitmap = (SugarBitmap *)rDataLibrary->GetEntry(lua_tostring(L, 3));
        rAnimation->SetFrame(lua_tointeger(L, 2), rBitmap);
    }
    else if(!strcmp(rTypeString, "Frame") && tArgs == 4)
    {
        SugarBitmap *rBitmap = (SugarBitmap *)rDataLibrary->GetEntry(lua_tostring(L, 3));
        rAnimation->SetFrame(lua_tointeger(L, 2), rBitmap, lua_tointeger(L, 4));
    }
    else if(!strcmp(rTypeString, "TicksPerFrame") && tArgs == 2)
    {
        rAnimation->SetTicksPerFrame(lua_tonumber(L, 2));
    }
    else
    {
        fprintf(stderr, "Anim_SetProperty:  Failed to resolve %s\n", rTypeString);
    }

    return 0;
}
