//--[SugarAnimation]
//--Animations are lists of bitmaps that are played in sequence. Depending on the circumstances,
//  they may automatically determine which frame to play, or have an outside formula determine it.
//--Animations generally do not assume ownership of the bitmaps within.
//--'Manual' timing refers to each bitmap having a tick count of 1, meaning the slot is equal to
//  the timer when animating, and the Animation will assume an outside formula is determining
//  which frame to display. 'Auto' timing uses internal tick counters.

#pragma once

#include "Definitions.h"
#include "Structures.h"

class SugarAnimation : public RootObject
{
    private:
    //--System
    int mTotalFrames;
    SugarBitmap **mFrameArray;

    //--Drawing Data
    int mTotalTicksToAnimate;
    float mTicksPerFrame;

    //--Variable-rate Drawing
    bool mUseVariableRate;
    int *mTicksArray;

    protected:

    public:
    //--System
    SugarAnimation();
    virtual ~SugarAnimation();

    //--Public Variables
    //--Property Queries
    int GetOneAnimationTicks();
    int GetFrameSlotByTimer(int pTimer);

    //--Manipulators
    void Setup(int pTotalFrames);
    void SetManualTiming(bool pFlag);
    void SetFrame(int pSlot, SugarBitmap *pFrame);
    void SetFrame(int pSlot, SugarBitmap *pFrame, int pTicksThisFrame);
    void SetTicksPerFrame(float pTicksPerFrame);

    //--Core Methods
    //--Update
    //--File I/O
    //--Drawing
    SugarBitmap *GetFrame(int pTimer);
    SugarBitmap *GetFrameBySlot(int pSlot);

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_Anim_Create(lua_State *L);
int Hook_Anim_SetProperty(lua_State *L);
