//--Base
#include "SugarLinkedList.h"

//--Classes
//--Definitions
//--Generics
//--GUI
//--Libraries
//--Managers

void *SugarLinkedList::RemoveElementS(const char *pSearch)
{
    //--Removes and deletes (if applicable) the element by its name. The first instance matching
    //  will be deleted, and its data returned (though it may be unstable if it was deleted). If
    //  no matches are found, NULL is returned.
    //--You can be sure all matches were removed by calling this in a while loop. It will return
    //  NULL when they're all gone.
    void *rDataPtr = NULL;

    //--Special cases for empty list, head, and tail. The tail deletion is one exception, the
    //  first instance found is deleted in all other cases.
    if(mListSize == 0) return NULL;
    if(!rStringCompareFuncPtr(mListHead->mName, pSearch)) return DeleteHead();
    if(!rStringCompareFuncPtr(mListTail->mName, pSearch)) return DeleteTail();

    //--General case, the target is somewhere in this list...
    SugarLinkedListEntry *rEntry = mListHead->rNext;
    for(int i = 1; i < mListSize-1; i ++)
    {
        if(!rStringCompareFuncPtr(rEntry->mName, pSearch))
        {
            SugarLinkedListEntry *pEntry = rEntry->rPrev;
            SugarLinkedListEntry *nEntry = rEntry->rNext;
            pEntry->rNext = nEntry;
            nEntry->rPrev = pEntry;

            rDataPtr = DeleteEntry(rEntry);
            DecrementList();
            return rDataPtr;
        }
        rEntry = rEntry->rNext;
    }

    //--The element was not found.
    return NULL;
}
void *SugarLinkedList::RemoveElementI(int pSlot)
{
    //--As above, but removes an element bases on its position in the list, with 0 being the first.
    //  Can return NULL if the position was outside the range of the list.

    //--Special cases.
    if(pSlot < 0 || pSlot >= mListSize) return NULL;
    if(pSlot == 0) return DeleteHead();
    if(pSlot == mListSize-1) return DeleteTail();

    //--General case
    void *rDataPtr = NULL;
    SugarLinkedListEntry *rEntry = mListHead;
    for(int i = 0; i < pSlot; i ++)
    {
        rEntry = rEntry->rNext;
    }

    //--Remove the marked entry from the list.
    SugarLinkedListEntry *rPrevEntry = rEntry->rPrev;
    SugarLinkedListEntry *rNextEntry = rEntry->rNext;
    rPrevEntry->rNext = rNextEntry;
    rNextEntry->rPrev = rPrevEntry;
    DecrementList();

    //--Delete it and return what's left.
    rDataPtr = DeleteEntry(rEntry);
    return rDataPtr;
}
bool SugarLinkedList::RemoveElementP(void *pPtr)
{
    //--Removes the element that contains the pointer provided. Does not return the pointer, since
    //  you'd have to have it in order to call this. Instead, provides a boolean indicating whether
    //  or not the pointer was found and removed.
    //--As usual, the first instance is the one removed, unless it's the tail.

    //--Special cases: RLL's cannot contain NULL entries. Also, empty list, head, and tail.
    if(!pPtr) return false;
    if(mListSize == 0) return false;
    if(mListHead->rData == pPtr) return DeleteHead();
    if(mListTail->rData == pPtr) return DeleteTail();

    SugarLinkedListEntry *rEntry = mListHead->rNext;
    for(int i = 1; i < mListSize - 1; i ++)
    {
        if(rEntry->rData == pPtr)
        {
            SugarLinkedListEntry *pEntry = rEntry->rPrev;
            SugarLinkedListEntry *nEntry = rEntry->rNext;
            pEntry->rNext = nEntry;
            nEntry->rPrev = pEntry;

            DeleteEntry(rEntry);
            DecrementList();
            return true;
        }

        rEntry = rEntry->rNext;
    }
    return false;
}
