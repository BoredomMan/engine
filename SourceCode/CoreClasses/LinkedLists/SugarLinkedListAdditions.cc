//--Base
#include "SugarLinkedList.h"
#include <string.h>

//--Classes
//--CoreClasses
//--Definitions
#include "Definitions.h"

//--GUI
//--Libraries
//--Managers

SugarLinkedListEntry *SugarLinkedList::AllocateEntry(const char *pName, void *pElement, DeletionFunctionPtr pDeletionPtr)
{
    //--Worker function, allocates and returns an RLLEntry with the matching data. Is a static public
    //  function, does not actually add anything to the list. If, for some strange reason, you want
    //  RLLEntries for something other than the list, feel free!
    //--Returns NULL on error.
    if(!pName || !pElement) return NULL;

    SetMemoryData(__FILE__, __LINE__);
    SugarLinkedListEntry *nEntry = (SugarLinkedListEntry *)starmemoryalloc(sizeof(SugarLinkedListEntry));
    nEntry->mName = (char *)starmemoryalloc(sizeof(char) * (strlen(pName)+1));
    strcpy(nEntry->mName, pName);
    nEntry->rNext = NULL;
    nEntry->rPrev = NULL;
    nEntry->rData = pElement;
    nEntry->rDeletionFunc = pDeletionPtr;
    return nEntry;
}
void *SugarLinkedList::AddElement(const char *pName, void *pElement)
{
    //--Adds the element to the end of the list. Note that this could change, it is not guaranteed
    //  that this invocation will always be a tail-add, it's an "I don't care" add. If your logic
    //  depends on a tail-add, use AddElementAsTail().
    //--Note: All the addition functions return the element that was being added (the data pointer,
    //  NOT the RLLEntry) in case you're using if(created) logic.
    return AddElementAsTail(pName, pElement);
}
void *SugarLinkedList::AddElement(const char *pName, void *pElement, DeletionFunctionPtr pDeletionPtr)
{
    //--Adds the element as the tail, and gives the DeletionFunction the passed pointer
    return AddElementAsTail(pName, pElement, pDeletionPtr);
}
void *SugarLinkedList::AddElementAsHead(const char *pName, void *pElement)
{
    //--Cascading overload of below. Passes NULL for the rDeletionFunction.
    return AddElementAsHead(pName, pElement, NULL);
}
void *SugarLinkedList::AddElementAsHead(const char *pName, void *pElement, DeletionFunctionPtr pDeletionPtr)
{
    //--Adds the specified element to the head of the list. The DeletionFunctionPtr can be NULL,
    //  which is identical to free().
    //--Returns NULL on error.
    SugarLinkedListEntry *nEntry = AllocateEntry(pName, pElement, pDeletionPtr);
    if(!nEntry) return NULL;

    //--If the list was empty, this becomes the head and tail.
    if(mListSize == 0)
    {
        mListHead = nEntry;
        mListTail = nEntry;
    }
    //--If the list had exactly one element, this becomes the head, and the other becomes the tail.
    else if(mListSize == 1)
    {
        mListHead = nEntry;
        mListHead->rNext = mListTail;
        mListTail->rPrev = mListHead;
    }
    //--In the general case, this becomes the head and nothing else changes.
    else
    {
        mListHead->rPrev = nEntry;
        nEntry->rNext = mListHead;
        mListHead = nEntry;
    }

    //--Out of politeness, we return a pointer to the data in case you needed it.
    mListSize ++;
    return nEntry->rData;
}
void *SugarLinkedList::AddElementAsTail(const char *pName, void *pElement)
{
    //--Same as adding to the head, except... to the tail. As usual, cascading overloads are provided.
    return AddElementAsTail(pName, pElement, NULL);
}
void *SugarLinkedList::AddElementAsTail(const char *pName, void *pElement, DeletionFunctionPtr pDeletionPtr)
{
    //--Adds the specified element to the tail of the list. The DeletionFunctionPtr can be NULL,
    //  which is identical to free().
    //--Returns NULL on error.
    SugarLinkedListEntry *nEntry = AllocateEntry(pName, pElement, pDeletionPtr);
    if(!nEntry) return NULL;

    //--If the list was empty, this becomes the head and tail.
    if(mListSize == 0)
    {
        mListHead = nEntry;
        mListTail = nEntry;
    }
    //--If the list had exactly one element, this becomes the tail, and the other becomes the head.
    else if(mListSize == 1)
    {
        mListTail = nEntry;
        mListHead->rNext = mListTail;
        mListTail->rPrev = mListHead;
    }
    //--In the general case, this becomes the tail and nothing else changes.
    else
    {
        mListTail->rNext = nEntry;
        nEntry->rPrev = mListTail;
        mListTail = nEntry;
    }

    //--Out of politeness, we return a pointer to the data in case you needed it.
    mListSize ++;
    return nEntry->rData;
}
void *SugarLinkedList::AddElementInSlot(const char *pName, void *pElement, int pSlot)
{
    return AddElementInSlot(pName, pElement, NULL, pSlot);
}
void *SugarLinkedList::AddElementInSlot(const char *pName, void *pElement, DeletionFunctionPtr pDeletionPtr, int pSlot)
{
    //--Adds the specified element in the given slot. Always adds and returns the element, though if the slot requested
    //  cannot be reached because it's out of range, the element may not be in the requested slot.
    //--The requested slot is the one that the new entry is expected to be at. Therefore, if the list is:
    //  A, B, C, D
    //  And we insert E at slot 1, the new list is:
    //  A, E, B, C, D

    //--If the requested position is 0 or negative, we just place it on the head.
    if(pSlot < 1) return AddElementAsHead(pName, pElement, pDeletionPtr);

    //--If the requested position is greater than the list's total size, place it on the tail.
    if(pSlot >= mListSize) return AddElementAsTail(pName, pElement, pDeletionPtr);

    //--If we got this far, we need to insert the element into an existing list. First, allocate it.
    SugarLinkedListEntry *nEntry = AllocateEntry(pName, pElement, pDeletionPtr);
    if(!nEntry) return NULL;

    //--Get the two list elements at the requested position and before it. The Lft entry is the requested position,
    //  the Rgt entry is the next one after insertion.
    SugarLinkedListEntry *rLftEntry = mListHead;
    SugarLinkedListEntry *rRgtEntry = mListHead->rNext;
    for(int i = 0; i < pSlot-1; i ++)
    {
        rLftEntry = rRgtEntry;
        rRgtEntry = rRgtEntry->rNext;
    }

    //--Entry points to these two adjacent entries.
    nEntry->rPrev = rLftEntry;
    nEntry->rNext = rRgtEntry;

    //--Break the chain.
    rLftEntry->rNext = nEntry;
    rRgtEntry->rPrev = nEntry;

    //--Finish up.
    mListSize ++;
    return nEntry->rData;
}
