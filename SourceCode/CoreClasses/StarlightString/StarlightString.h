//--[StarlightString]
//--Special string for the Starlight engine. This string can contain images in addition to
//  letters, and allows for advanced color/italics options.
//--These are not a replacement for normal strings, they should only be used when required
//  for advanced rendering as they are slower than conventional font rendering.
//--The formatting for the string is identical to the usual printf format, except [IMG0] will
//  be replaced in-stride with the provided image. The string is designed to precache itself
//  and needs to be modified whenever the variables/images change.
//--The StarlightString class uses [IMG0] [IMG1] [IMG2] etc to specify images, which must
//  be uploaded seperately. If an out-of-range index is specified, nothing renders.

#pragma once

//--[Includes]
#include "Definitions.h"
#include "Structures.h"

//--[Local Structures]
typedef struct StarlightStringBreakpoint
{
    //--Variables
    int mIndex;
    char *mString;
    float pYOffset;
    SugarBitmap *rImage;

    //--Functions
    void Initialize();
    static void DeleteThis(void *pPtr);
    int ComputeLength(SugarFont *pFont);
}StarlightStringBreakpoint;

//--[Local Definitions]
#define STARLIGHT_STRING_BUFFER_LETTERS 1024

//--[Classes]
class StarlightString : public RootObject
{
    private:
    //--Breakpoints
    SugarLinkedList *mBreakpointList; //StarlightStringBreakpoint *, master

    //--Images
    int mImagesTotal;
    float *mYOffsets;
    SugarBitmap **mrImages;

    protected:

    public:
    //--System
    StarlightString();
    virtual ~StarlightString();

    //--Public Variables
    //--Property Queries
    //--Manipulators
    void AllocateImages(int pTotal);
    void SetImageS(int pSlot, float pYOffset, const char *pDLPath);
    void SetImageP(int pSlot, float pYOffset, SugarBitmap *pImage);

    //--Core Methods
    void SetString(const char *pString, ...);
    void CrossreferenceImages();

    private:
    //--Private Core Methods
    public:
    //--Update
    //--File I/O
    //--Drawing
    void DrawText(float pX, float pY, int pFlags, float pScale, SugarFont *pFont);

    //--Pointer Routing
    StarlightStringBreakpoint *GetBreakpoint(int pIndex);

    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions

