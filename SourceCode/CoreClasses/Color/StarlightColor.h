//--[StarlightColor]
//--Represents a color mapping using four floating-point values, ranging from 0 to 1. Is considered to
//  be identical to an ALLEGRO_COLOR if Allegro is being used.
//--Contains some useful macros for the structure as well.

#pragma once

//--[Includes]
#include "Definitions.h"
#include "Structures.h"

//--[Local Structures]
//--[Local Definitions]
//--[Classes]
typedef struct StarlightColor
{
    public:
    //--Basics
    float r, g, b, a;

    protected:

    public:
    //--System
    //--Public Variables
    static StarlightColor cxWhite;
    static StarlightColor cxGrey;
    static StarlightColor cxBlack;

    //--Property Queries
    //--Manipulators
    void SetDefault();
    void SetRGBI(int pRed, int pGrn, int pBlu);
    void SetRGBAI(int pRed, int pGrn, int pBlu, int pAlp);
    void SetRGBF(float pRed, float pGrn, float pBlu);
    void SetRGBAF(float pRed, float pGrn, float pBlu, float pAlp);
    void CloneFrom(const StarlightColor pColor);

    //--Core Methods
    void SetAsMixer();
    void SetAsMixerAlpha(float pAlpha);
    StarlightColor Clone();

    private:
    //--Private Core Methods
    public:
    //--Update
    //--File I/O
    //--Drawing
    //--Pointer Routing
    //--Static Functions
    static void ConditionalMixer(bool pConditional, StarlightColor pTrueColor, StarlightColor pFalseColor);
    static void ClearMixer();
    static void SetMixer(float pRed, float pGrn, float pBlu, float pAlp);
    static StarlightColor MapRGBI(uint8_t pRed, uint8_t pGrn, uint8_t pBlu);
    static StarlightColor MapRGBAI(uint8_t pRed, uint8_t pGrn, uint8_t pBlu, uint8_t pAlp);
    static StarlightColor MapRGBF(float pRed, float pGrn, float pBlu);
    static StarlightColor MapRGBAF(float pRed, float pGrn, float pBlu, float pAlp);

    //--Lua Hooking
}StarlightColor;

//--Hooking Functions

