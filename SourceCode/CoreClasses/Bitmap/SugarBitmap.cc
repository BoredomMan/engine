//--Base
#include "SugarBitmap.h"

//--Classes
//--CoreClasses
#include "SugarLinkedList.h"

//--Definitions
#include "GlDfn.h"

//--GUI
//--Libraries
//--Managers

bool SugarBitmap::xForcePowerOfTwo = false;
bool SugarBitmap::gLockTarget = false;
bool SugarBitmap::xSuppressInterrupt = false;
InterruptPtr SugarBitmap::xInterruptCall = NULL;
bool SugarBitmap::xPadSpriteForEdging = false;

//--Statistics
int32_t SugarBitmap::xMaxTextureSize = 1024;

//--Multi-Texture
bool SugarBitmap::xThisIsMultiTexture = false;
int SugarBitmap::xMultiTexX = 0;
int SugarBitmap::xMultiTexY = 0;
int SugarBitmap::xMultiTexSizeX = 0;
int SugarBitmap::xMultiTexSizeY = 0;
uint32_t **SugarBitmap::xMultiGLHandles = NULL;

//=========================================== System ==============================================
void SugarBitmap::DefaultInit(uint32_t pGLHandle)
{
    //--System function, calls the common behavior between the constructors, becuase MinGW doesn't
    //  support C2011 or whatever yet.

    //--System
    mIsInternallyFlipped = false;
    mXOffset = 0;
    mYOffset = 0;
    mWidth = 0;
    mHeight = 0;
    mTrueWidth = 0;
    mTrueHeight = 0;
    mUseMipmapping = false;

    //--GL Stuff
    mOwnsHandle = true;
    mGLHandle = pGLHandle;
    for(int i = 0; i < SUGARBITMAP_LISTS; i ++) mGLLists[i] = 0;

    //--Atlasing
    mAtlasLft = 0.0f;
    mAtlasRgt = 1.0f;
    mAtlasTop = 0.0f;
    mAtlasBot = 1.0f;
    mDependencyList = NULL;

    //--Multi-part images for old systems.
    mUsesMultipleTextures = false;
    mMultiTexX = 0;
    mMultiTexY = 0;
    mMultiGLHandles = NULL;

    //--Cache
    mIsHoldingData = false;
    mDontDiscardDataOnUpload = false;
    mDataLength = 0;
    mCompressionType = cNoCompression;
    rLoadBuffer = NULL;

    //--If we already have a valid handle, get some data from it.
    if(mGLHandle)
    {
        glBindTexture(GL_TEXTURE_2D, mGLHandle);
        glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_WIDTH, &mWidth);
        glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_HEIGHT, &mHeight);
        mTrueWidth = mWidth;
        mTrueHeight = mHeight;
    }

    //--Hitboxes
    mTotalHitboxes = 0;
    mHitboxes = NULL;
}
SugarBitmap::SugarBitmap()
{
    //--Increment the static.
    xTotalBitmaps ++;

    //--'Zero' constructor.  A blank SugarBitmap which is populated with data later by a different
    //  function.
    DefaultInit(0);
}
SugarBitmap::SugarBitmap(uint32_t pGLHandle)
{
    //--Increment the static.
    xTotalBitmaps ++;

    //--Default constructor.  Accepts a GLHandle and wraps it.  Pass 0 to cause the "NULL" behavior.
    //  The Bitmap considers itself the texture's owner.
    DefaultInit(pGLHandle);
}
SugarBitmap::SugarBitmap(const char *pPath)
{
    //--Increment the static.
    xTotalBitmaps ++;

    //--Constructor which accepts a path from the hard drive, and loads the bitmap at that path.
    //  The texture is uploaded and the handle is found.
    //--Passing NULL will cause NULL behavior, no texture.
    uint32_t tHandle = SugarBitmap::ProcessBitmap(pPath);
    DefaultInit(tHandle);

    //--If the handle came back as zero, then this might be a multi-texture.
    if(!tHandle && xThisIsMultiTexture)
    {
        //--Copy over the multi-texture data.
        //fprintf(stderr, "Detected multi-texture in %i %s\n", tHandle, pPath);
        //fprintf(stderr, " Sizes were %ix%i textures\n", xMultiTexX, xMultiTexY);
        mUsesMultipleTextures = true;
        mMultiTexX = xMultiTexX;
        mMultiTexY = xMultiTexY;
        mMultiGLHandles = xMultiGLHandles;

        //--Calc sizes.
        mWidth = xMultiTexSizeX;
        mHeight = xMultiTexSizeY;

        //--Null off old data.
        xThisIsMultiTexture = false;
        xMultiGLHandles = NULL;
    }
}
SugarBitmap::SugarBitmap(uint8_t *pData, int pWidth, int pHeight)
{
    //--Increment the static.
    xTotalBitmaps ++;

    //--Constructor which accepts data (typically from the EIM) and uploads it to get the texture
    //  handle.  Doesn't free the data!
    uint32_t tHandle = SugarBitmap::UploadData(pData, pWidth, pHeight);
    DefaultInit(tHandle);
}
SugarBitmap::~SugarBitmap()
{
    //--Decrement the static.
    xTotalBitmaps --;

    //--Clear the texture, if you own it.
    if(mOwnsHandle && mGLHandle) glDeleteTextures(1, &mGLHandle);
    delete mDependencyList;

    //--Clear multi-part handles.
    for(int x = 0; x < mMultiTexX; x ++)
    {
        for(int y = 0; y < mMultiTexY; y ++)
        {
            //glDeleteTextures(1, &mMultiGLHandles[x][y]);
        }
        free(mMultiGLHandles[x]);
    }
    free(mMultiGLHandles);

    //--Clear any lists.
    for(int i = 0; i < SUGARBITMAP_LISTS; i ++)
    {
        if(mGLLists[i]) glDeleteLists(mGLLists[i], 1);
    }

    //--If there's anything in the load buffer, clear it.
    free(rLoadBuffer);
    rLoadBuffer = NULL;
    free(mHitboxes);
}
void SugarBitmap::DeleteThis(void *pPtr)
{
    delete ((SugarBitmap *)pPtr);
}

//--[Public Statics]
//--Tracks how many bitmaps exist. Can be used to make sure bitmaps are getting deallocated correctly.
int SugarBitmap::xTotalBitmaps = 0;

//====================================== Property Queries =========================================
bool SugarBitmap::IsSubBitmap()
{
    return mOwnsHandle;
}
int SugarBitmap::GetWidth()
{
    return mWidth;
}
int SugarBitmap::GetHeight()
{
    return mHeight;
}
int SugarBitmap::GetTrueWidth()
{
    return mTrueWidth;
}
int SugarBitmap::GetTrueHeight()
{
    return mTrueHeight;
}
int SugarBitmap::GetXOffset()
{
    return mXOffset;
}
int SugarBitmap::GetYOffset()
{
    return mYOffset;
}
uint32_t SugarBitmap::GetGLTextureHandle()
{
    return mGLHandle;
}
float SugarBitmap::GetAtlasLft()
{
    return mAtlasLft;
}
float SugarBitmap::GetAtlasTop()
{
    return mAtlasTop;
}
float SugarBitmap::GetAtlasRgt()
{
    return mAtlasRgt;
}
float SugarBitmap::GetAtlasBot()
{
    return mAtlasBot;
}

//========================================= Manipulators ==========================================
void SugarBitmap::Bind()
{
    glBindTexture(GL_TEXTURE_2D, mGLHandle);
}
void SugarBitmap::SetInternalFlip(bool pFlag)
{
    mIsInternallyFlipped = pFlag;
}
void SugarBitmap::SetSizes(int pXSize, int pYSize)
{
    mWidth = pXSize;
    mHeight = pYSize;
}
void SugarBitmap::SetOffset(int pXOffset, int pYOffset)
{
    mXOffset = pXOffset;
    mYOffset = pYOffset;
}
void SugarBitmap::SetTrueSizes(int pTrueWid, int pTrueHei)
{
    mTrueWidth = pTrueWid;
    mTrueHeight = pTrueHei;
}
void SugarBitmap::SetMipMapping(bool pFlag)
{
    mUseMipmapping = pFlag;
}
void SugarBitmap::AssignHandle(uint32_t pHandle, bool pTakeOwnership, bool pDontDeriveSizes)
{
    //--Assigns the handle to the bitmap, and also assigns ownership, if necessary. The old
    //  handle is deleted if it was owned. Passing zero will deallocate the handle and set the
    //  bitmap to NULL mode.
    if(mOwnsHandle && mGLHandle)
    {
        glDeleteTextures(1, &mGLHandle);
        mGLHandle = 0;
        mOwnsHandle = false;
    }
    if(!pHandle) return;

    //--Set handle and ownership flag.
    mGLHandle = pHandle;
    mOwnsHandle = pTakeOwnership;

    //--Retrieve data. Note that the 'true' values cannot be discerned from just the bitmap.
    if(!pDontDeriveSizes)
    {
        glBindTexture(GL_TEXTURE_2D, mGLHandle);
        glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_WIDTH, &mWidth);
        glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_HEIGHT, &mHeight);
        mTrueWidth = mWidth;
        mTrueHeight = mHeight;
    }

    //--Clear any lists. Any lists that existed before are now invalid.
    for(int i = 0; i < SUGARBITMAP_LISTS; i ++)
    {
        if(mGLLists[i]) glDeleteLists(mGLLists[i], 1);
        mGLLists[i] = 0;
    }
}
void SugarBitmap::SetAtlasCoordinates(float pLft, float pTop, float pRgt, float pBot)
{
    mAtlasLft = pLft;
    mAtlasTop = pTop;
    mAtlasRgt = pRgt;
    mAtlasBot = pBot;
}
void SugarBitmap::FlipAtlasCoordinates()
{
    mAtlasTop = 1.0f - mAtlasTop;
    mAtlasBot = 1.0f - mAtlasBot;
}
void SugarBitmap::MoveAtlasInBy(float pX, float pY)
{
    mAtlasLft = mAtlasLft + pX;
    mAtlasRgt = mAtlasRgt - pX;
    mAtlasTop = mAtlasTop - pY;
    mAtlasBot = mAtlasBot + pY;
}

//========================================= Core Methods ==========================================
//============================================ Update =============================================
//=========================================== File I/O ============================================
//=========================================== Drawing =============================================
//======================================= Pointer Routing =========================================
//====================================== Static Functions =========================================
void SugarBitmap::InterceptGlTexImage2D(GLenum pTarget, GLint pLevel, GLint pInternalFormat, GLsizei pWidth, GLsizei pHeight, GLenum pBorder, GLenum pFormat, GLenum pType, const GLvoid *pData)
{
    //--Special call that should be used in place of glTexImage2D. This one automatically trims the
    //  edges of the image off if it happens to be larger than the max texture size. Returns the
    //  texture handle that was generated.
    //--This is also needed for image atlasing.
    //--Data is always assumed to be in the RGBA format using 1 byte for each color.
    if(pWidth <= xMaxTextureSize && pHeight <= xMaxTextureSize)
    {
        glTexImage2D(pTarget, pLevel, pInternalFormat, pWidth, pHeight, pBorder, pFormat, pType, pData);
        return;
    }

    //--Warning print.
    static bool xHasPrintedWarning = false;
    if(!xHasPrintedWarning)
    {
        //--Flag.
        xHasPrintedWarning = true;

        //--Warning notice.
        fprintf(stderr, "==[WARNING]==\n The engine had to trim a bitmap off as it was too large for your OpenGL implementation. Visual bugs may result!\n");
    }

    //--Cast the array.
    uint8_t *rOldData = (uint8_t *)pData;

    //--If we got this far, we need to truncate off the edges of the data. This is an expensive operation,
    //  but at least the program will run.
    int tUseWid = pWidth;
    int tUseHei = pHeight;
    if(tUseWid >= xMaxTextureSize) tUseWid = xMaxTextureSize;
    if(tUseHei >= xMaxTextureSize) tUseHei = xMaxTextureSize;

    //--Allocate a new data array.
    SetMemoryData(__FILE__, __LINE__);
    uint8_t *tNewData = (uint8_t *)starmemoryalloc(sizeof(uint8_t) * tUseWid * tUseHei * 4);
    if(!tNewData)
    {
        fprintf(stderr, "Memory Error: Not enough space, malloc returned null.\n");
        return;
    }

    //--Copy across.
    for(int x = 0; x < tUseWid; x ++)
    {
        for(int y = 0; y < tUseHei; y ++)
        {
            //--Position in old array.
            int tOldPos = ((x) + (y * pWidth)) * 4;

            //--Position in new array.
            int tNewPos = ((x) + (y * tUseWid)) * 4;

            //--Copy.
            tNewData[tNewPos+0] = rOldData[tOldPos+0];
            tNewData[tNewPos+1] = rOldData[tOldPos+1];
            tNewData[tNewPos+2] = rOldData[tOldPos+2];
            tNewData[tNewPos+3] = rOldData[tOldPos+3];
        }
    }

    //--Issue the upload.
    glTexImage2D(pTarget, pLevel, pInternalFormat, tUseWid, tUseHei, pBorder, pFormat, pType, tNewData);

    //--Clean.
    free(tNewData);
}
void SugarBitmap::RestoreDefaultTextureFlags()
{
    //--Restores the texture flags back to program defaults.
    memcpy(&SugarBitmap::xStaticFlags, &SugarBitmap::xDefaultFlags, sizeof(SugarBitmapFlags));

    //--Set them.
    glBindTexture(GL_TEXTURE_2D, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, xStaticFlags.mUplMinFilter);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, xStaticFlags.mUplMagFilter);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, xStaticFlags.mUplBaseLevel);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, xStaticFlags.mUplMaxLevel);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, xStaticFlags.mUplWrapS);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, xStaticFlags.mUplWrapT);
}

//========================================= Lua Hooking ===========================================
void SugarBitmap::HookToLuaState(lua_State *pLuaState)
{
    /* ALB_SetTextureProperty(sPropertyName, iEnumeration)
       Sets the property specified for all future texture uploads. Maps ignore this setting. Should
       be called during loading at startup, or during enemy loading, only. */
    lua_register(pLuaState, "ALB_SetTextureProperty", &Hook_ALB_SetTextureProperty);

    /* Bitmap_ActivateAtlasing()
       Bitmap_DeactivateAtlasing()
       Activates/deactivates sprite atlas mode. Images will be placed on the same bitmap instead of
       many differing individual bitmaps. This reduces the number of bind operations that need to
       take place. */
    lua_register(pLuaState, "Bitmap_ActivateAtlasing",   &Hook_Bitmap_ActivateAtlasing);
    lua_register(pLuaState, "Bitmap_DeactivateAtlasing", &Hook_Bitmap_DeactivateAtlasing);
}

//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
int Hook_ALB_SetTextureProperty(lua_State *L)
{
    //ALB_SetTextureProperty("Special Sprite Padding", bValue)
    //ALB_SetTextureProperty("Default MagFilter", iEnumeration)
    //ALB_SetTextureProperty("Default MinFilter", iEnumeration)
    //ALB_SetTextureProperty("Default Wrap S", iEnumeration)
    //ALB_SetTextureProperty("Default Wrap T", iEnumeration)
    //ALB_SetTextureProperty("MagFilter", iEnumeration)
    //ALB_SetTextureProperty("MinFilter", iEnumeration)
    //ALB_SetTextureProperty("Wrap S", iEnumeration)
    //ALB_SetTextureProperty("Wrap T", iEnumeration)
    //ALB_SetTextureProperty("Restore Defaults")
    uint32_t tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("ALB_SetTextureProperty");

    //--Switcher.
    const char *pPropertyName = lua_tostring(L, 1);

    //--[Flags]
    if(!strcmp(pPropertyName, "Special Sprite Padding") && tArgs == 2)
    {
        SugarBitmap::xPadSpriteForEdging = lua_toboolean(L, 2);
    }
    //--[Modify Defaults]
    else if(!strcmp(pPropertyName, "Default MagFilter") && tArgs == 2)
    {
        SugarBitmap::xDefaultFlags.mUplMagFilter = lua_tointeger(L, 2);
    }
    else if(!strcmp(pPropertyName, "Default MinFilter") && tArgs == 2)
    {
        SugarBitmap::xDefaultFlags.mUplMinFilter = lua_tointeger(L, 2);
    }
    else if(!strcmp(pPropertyName, "Default Wrap S") && tArgs == 2)
    {
        SugarBitmap::xDefaultFlags.mUplWrapS = lua_tointeger(L, 2);
    }
    else if(!strcmp(pPropertyName, "Default Wrap T") && tArgs == 2)
    {
        SugarBitmap::xDefaultFlags.mUplWrapT = lua_tointeger(L, 2);
    }
    //--[Modify Current Version]
    else if(!strcmp(pPropertyName, "MagFilter") && tArgs == 2)
    {
        SugarBitmap::xStaticFlags.mUplMagFilter = lua_tointeger(L, 2);
    }
    else if(!strcmp(pPropertyName, "MinFilter") && tArgs == 2)
    {
        SugarBitmap::xStaticFlags.mUplMinFilter = lua_tointeger(L, 2);
    }
    else if(!strcmp(pPropertyName, "Wrap S") && tArgs == 2)
    {
        SugarBitmap::xStaticFlags.mUplWrapS = lua_tointeger(L, 2);
    }
    else if(!strcmp(pPropertyName, "Wrap T") && tArgs == 2)
    {
        SugarBitmap::xStaticFlags.mUplWrapT = lua_tointeger(L, 2);
    }
    //--[Restore Defaults]
    else if(!strcmp(pPropertyName, "Restore Defaults") && tArgs == 1)
    {
        SugarBitmap::RestoreDefaultTextureFlags();
    }

    return 0;
}
int Hook_Bitmap_ActivateAtlasing(lua_State *L)
{
    //Bitmap_ActivateAtlasing()

    //--Debug
    //fprintf(stderr, "Atlasing begins.\n");
    SugarBitmap::xActivationTime = GetGameTime();
    SugarBitmap::xIsAtlasModeActive = true;

    return 0;
}
int Hook_Bitmap_DeactivateAtlasing(lua_State *L)
{
    //Bitmap_DeactivateAtlasing()

    //--If an atlas is currently running, finish it.
    if(SugarBitmap::xrActiveAtlas)
    {
        //fprintf(stderr, "Finishing atlas.\n");
        SugarBitmap::xrActiveAtlas->FinishAtlas();
        //fprintf(stderr, "Finished atlas.\n");
        SugarBitmap::xrActiveAtlas = NULL;
    }

    //--Debug.
    SugarBitmap::xIsAtlasModeActive = false;

    return 0;
}
