//--Base
#include "SugarBitmap.h"

//--Classes
//--CoreClasses
//--Definitions
#include "GlDfn.h"

//--GUI
//--Libraries
//--Managers

//--Note:  All shader functions are covered with define blocks because not all programs may want
//  to do the requisite GLWrangling to make them work.
//--If the define is not set, a series of blank functions are defined and the calls do nothing.

#define _ALLOW_SHADERS_

#if defined _ALLOW_SHADERS_
void SugarBitmap::SetSwapColorsTotal(uint32_t pProgramHandle, int pTotal)
{
    //--Sets how many colors the shader is expected to iterate through.  Max is 32.
    if(pTotal > 32) pTotal = 32;
    uint32_t tTotalColorsHandle = sglGetUniformLocation(pProgramHandle, "uTotalColors");
    sglUniform1i(tTotalColorsHandle, pTotal);
}
void SugarBitmap::SetSwapColor(uint32_t pProgramHandle, int pSlot, StarlightColor pSrcColor, StarlightColor pDstColor)
{
    //--Modifies a swap color.  Max is 32.
    if(pSlot < 0 || pSlot >= 32) return;

    //--We need to assemble strings that correspond to the target name.
    char tName[32];

    sprintf(tName, "uSrcColor[%i]", pSlot);
    uint32_t tSrcColorHandle = sglGetUniformLocation(pProgramHandle, tName);
    sglUniform4f(tSrcColorHandle, pSrcColor.r, pSrcColor.g, pSrcColor.b, pSrcColor.a);

    sprintf(tName, "uDstColor[%i]", pSlot);
    uint32_t tDstColorHandle = sglGetUniformLocation(pProgramHandle, tName);
    sglUniform4f(tDstColorHandle, pDstColor.r, pDstColor.g, pDstColor.b, pDstColor.a);
}
#else
void SugarBitmap::SetSwapColorsTotal(int pTotal)
{
}
void SugarBitmap::SetSwapColor(int pSlot, StarlightColor pSrcColor, StarlightColor pDstColor)
{
}
#endif
