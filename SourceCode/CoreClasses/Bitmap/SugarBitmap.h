//--[SugarBitmap]
//--Originally a wrapper around Allegro's bitmaps, they have since shed almost all resemblence to
//  the fated API of old, and are now a wrapper around OpenGL behavior.  Most of the standards put
//  in place by Allegro remain, though.

#pragma once

#include "Definitions.h"
#include "Structures.h"

//--[Local Definitions]
#define FLIP_HORIZONTAL 1
#define FLIP_VERTICAL 2
#define FLIP_BOTH (FLIP_HORIZONTAL | FLIP_VERTICAL)
#define SUGAR_FLIP_HORIZONTAL 1
#define SUGAR_FLIP_VERTICAL 2
#define SUGAR_FLIP_BOTH (SUGAR_FLIP_HORIZONTAL | SUGAR_FLIP_VERTICAL)
typedef void(*InterruptPtr)();

#define SUGARBITMAP_LISTS 4

//--[Local Structures]
typedef struct SgHitbox
{
    uint16_t mLft;
    uint16_t mTop;
    uint16_t mRgt;
    uint16_t mBot;
}SgHitbox;
typedef struct
{
    uint32_t mUplMinFilter;
    uint32_t mUplMagFilter;
    uint32_t mUplBaseLevel;
    uint32_t mUplMaxLevel;
    uint32_t mUplWrapS;
    uint32_t mUplWrapT;
}SugarBitmapFlags;
typedef struct SugarBitmapPrecache
{
    uint8_t *mArray;
    int32_t mXOffset;
    int32_t mYOffset;
    int32_t mWidth;
    int32_t mHeight;
    int32_t mTrueWidth;
    int32_t mTrueHeight;
    uint32_t mForceHandle;
    uint8_t mCompressionType;
    uint32_t mDataSize;
    uint8_t mTotalHitboxes;
    SgHitbox *mHitboxes;
    SugarBitmapFlags mFlags;
}SugarBitmapPrecache;

class SugarBitmap
{
    //--[Constants]
    public:
    //--GL-compression codes.  These are mirrored in the SugarLumps program.
    static const uint8_t cInvalidCompression = 0;
    static const uint8_t cNoCompression = 1;
    static const uint8_t cPNGCompression = 2;
    static const uint8_t cDXT1Compression = 3;
    static const uint8_t cDXT1CompressionWithAlpha = 4;
    static const uint8_t cDXT3Compression = 5;
    static const uint8_t cDXT5Compression = 6;
    static const uint8_t cIsMonoColor = 7;
    static const uint8_t cHorizontalLineCompression = 8;
    static const uint8_t cVeritcalLineCompression = 9;
    static const uint8_t cSupportedCompressions = 10;
    static const int cGLLookups[4];

    //--[Debug]
    static int xTotalBitmaps;

    private:
    //--System
    bool mIsInternallyFlipped;
    int mXOffset, mYOffset;
    int mWidth, mHeight;
    int mTrueWidth, mTrueHeight;
    bool mUseMipmapping;

    //--GL Stuff
    bool mOwnsHandle;
    uint32_t mGLHandle;
    uint32_t mGLLists[SUGARBITMAP_LISTS];

    //--Atlasing
    float mAtlasLft, mAtlasRgt;
    float mAtlasTop, mAtlasBot;
    public:
    SugarLinkedList *mDependencyList;
    private:

    //--Multi-part images for old systems
    bool mUsesMultipleTextures;
    int mMultiTexX, mMultiTexY;
    uint32_t **mMultiGLHandles;

    //--Cache
    bool mIsHoldingData;
    bool mDataNeedsUpload;
    bool mDontDiscardDataOnUpload;
    uint32_t mDataLength;
    uint32_t mCompressionType;
    uint8_t *rLoadBuffer;

    //--Uploading
    static SugarLinkedList *mUploadingList;

    //--Hitboxes
    int mTotalHitboxes;
    SgHitbox *mHitboxes;

    //--Monocolor mode.
    bool mIsMonoColor;
    StarlightColor mMonoColor;

    protected:

    public:
    //--System
    void DefaultInit(uint32_t pGLHandle);
    SugarBitmap();
    SugarBitmap(uint32_t pGLHandle);
    SugarBitmap(const char *pPath);
    SugarBitmap(uint8_t *pData, int pWidth, int pHeight);
    ~SugarBitmap();
    static void DeleteThis(void *pPtr);

    //--Public Variables
    //--Static Atlas Variables
    static bool xDisallowAtlasing;
    static bool xIsAtlasModeActive;
    static float xActivationTime;
    static bool xCreatedAtlas;
    static SugarBitmap *xrActiveAtlas;

    //--Atlas Data Values
    static int xAtlasDataLen;
    static int xAtlasStride;
    static uint8_t *xAtlasData;
    static int xAtlasCurrentX;
    static int xAtlasCurrentY;
    static int xAtlasCurrentTallest;
    static float xAtlasMaxSize;
    static float xLastAtlasLft;
    static float xLastAtlasTop;
    static float xLastAtlasRgt;
    static float xLastAtlasBot;

    //--Static Variables (Defined in SugarBitmapPaletteMacros.cc)
    static bool gLockTarget;
    static bool xSuppressInterrupt;
    static InterruptPtr xInterruptCall;

    //--Static multi-texture checkers.
    static bool xThisIsMultiTexture;
    static int xMultiTexX, xMultiTexY;
    static int xMultiTexSizeX, xMultiTexSizeY;
    static uint32_t **xMultiGLHandles;

    //--Static Image Loading Variables
    static bool xPadSpriteForEdging;
    static SugarBitmapFlags xStaticFlags;
    static SugarBitmapFlags xDefaultFlags;

    //--Static Engine options
    static bool xForcePowerOfTwo;

    //--GL System data.
    static int32_t xMaxTextureSize;

    //--Property Queries
    bool IsSubBitmap();
    int GetWidth();
    int GetHeight();
    int GetTrueWidth();
    int GetTrueHeight();
    int GetXOffset();
    int GetYOffset();
    uint32_t GetGLTextureHandle();
    float GetAtlasLft();
    float GetAtlasTop();
    float GetAtlasRgt();
    float GetAtlasBot();

    //--Manipulators
    void Bind();
    void SetInternalFlip(bool pFlag);
    void SetSizes(int pXSize, int pYSize);
    void SetOffset(int pXOffset, int pYOffset);
    void SetTrueSizes(int pTrueWid, int pTrueHei);
    void SetMipMapping(bool pFlag);
    void AssignHandle(uint32_t pHandle, bool pTakeOwnership, bool pDontDeriveSizes);
    void SetAtlasCoordinates(float pLft, float pTop, float pRgt, float pBot);
    void FlipAtlasCoordinates();
    void MoveAtlasInBy(float pX, float pY);

    //--Core Methods
    void TranslateOffsets();
    void Rotate(float pAngle);
    void RenderAt();
    void Unrotate(float pAngle);
    void UntranslateOffsets();

    //--Atlasing
    void AddToAtlasList(SugarBitmap *pSubImage);
    static void PutDataIntoAtlas(uint8_t *pArray, int &sWidth, int &sHeight, int pCompressionType, int pDataSize);
    void FinishAtlas();

    //--Hitbox Methods
    void AllocateHitboxes(int pNewTotal);
    void SetHitbox(int pSlot, int pLft, int pTop, int pRgt, int pBot);
    void SetHitbox(int pSlot, SgHitbox pNewHitbox);
    int GetTotalHitboxes();
    SgHitbox GetHitbox(int pSlot);

    //--Cacheing Methods
    void UploadCheck();
    void SetHoldCacheFlag(bool pFlag);
    void CacheData(uint8_t *pData, uint32_t pDataLength, uint32_t pCompressionFlag);

    //--Loader Methods
    static void SetPrecacheFlags(SugarBitmapPrecache &sDataPack);
    static uint32_t ProcessBitmap(const char *pPath);
    static uint8_t *LoadBitmap(const char *pPath, int &sWidth, int &sHeight);
    #ifdef _ALLEGRO_PROJECT_
    static uint8_t *StripBitmap(ALLEGRO_BITMAP *pBitmap);
    #endif
    static uint8_t *GetDataFrom(uint8_t *pArray, int &sWidth, int &sHeight, int pCompressionType, int &sDataSize);
    static uint32_t UploadData(uint8_t *pArray, int pWidth, int pHeight);
    static uint32_t UploadData(uint8_t *pArray, int pWidth, int pHeight, uint32_t pForceHandle);
    static uint32_t UploadCompressed(SugarBitmapPrecache *pDataPack);
    static uint32_t UploadCompressed(uint8_t *pArray, int pWidth, int pHeight, uint32_t pForceHandle, int pCompressionType, int pDataSize);
    static uint32_t UploadMonoColor(uint8_t *pArray, uint32_t pForceHandle);
    static uint32_t UploadBitmap(uint8_t *pArray, int pWidth, int pHeight, uint32_t pForceHandle, int pCompressionType, int pDataSize);
    static void UploadBitmapMultiTexture(uint8_t *pArray, int pWidth, int pHeight, int pCompressionType, int pDataSize);

    //--Line Loaders
    static uint8_t *UncompressHorizontalData(uint8_t *pArray, int pDataSize, int &sWidth, int &sHeight);
    static uint8_t *UncompressVerticalData(uint8_t *pArray, int pDataSize, int &sWidth, int &sHeight);
    static void UncompressHorizontalDataIntoAtlas(uint8_t *pArray, int pDataSize, int &sWidth, int &sHeight);
    static void UncompressVerticalDataIntoAtlas(uint8_t *pArray, int pDataSize, int &sWidth, int &sHeight);
    static void UploadHorizontalCompressedBitmap(uint8_t *pArray, int pDataSize, int pWidth, int pHeight);
    static void UploadVerticalCompressedBitmap(uint8_t *pArray, int pDataSize, int pWidth, int pHeight);

    //--Shaders
    static void SetSwapColorsTotal(uint32_t pProgramHandle, int pTotal);
    static void SetSwapColor(uint32_t pProgramHandle, int pSlot, StarlightColor pSrcColor, StarlightColor pDstColor);

    //--Update
    //--File I/O
    //--Drawing
    void Draw(float pTargetX, float pTargetY, int pFlags);
    void Draw();
    void Draw(float pTargetX, float pTargetY);
    void DrawScaled(float pTargetX, float pTargetY, float pScaleX, float pScaleY);
    void DrawScaled(float pTargetX, float pTargetY, float pScaleX, float pScaleY, int pFlags);
    void DrawMultiTexture(int pTargetX, int pTargetY, int pFlags);

    //--Primitives
    static void DrawFullBlack(float pAlpha);
    static void DrawFullColor(StarlightColor pColor);
    static void DrawRect(float pLft, float pTop, float pRgt, float pBot, StarlightColor pColor, float pThickness);
    static void DrawRectFill(float pLft, float pTop, float pRgt, float pBot, StarlightColor pColor);
    static void DrawPolyLines(float *pArray, StarlightColor pColor, float pWidth);
    static void DrawHorizontalPercent(SugarBitmap *pBitmap, float pPercent);

    //--Pointer Routing
    //--Static Functions
    static void InterceptGlTexImage2D(GLenum pTarget, GLint pLevel, GLint pInternalFormat, GLsizei pWidth, GLsizei pHeight, GLenum pBorder, GLenum pFormat, GLenum pType, const GLvoid *pData);
    static void RestoreDefaultTextureFlags();

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_ALB_SetTextureProperty(lua_State *L);
int Hook_Bitmap_ActivateAtlasing(lua_State *L);
int Hook_Bitmap_DeactivateAtlasing(lua_State *L);
