//--[SugarTag]
//--A Tag is a key/value pair, both of which are always strings. Tags are stored in a static list
//  that can be checked at any time. If a new tag is created matching the name of an existing tag,
//  the old one is overwritten. If a tag is set but has not been created, it is created.
//--Tags simply allow generic information to be passed into the program, without the program needing
//  to know exactly what to do with the information until runtime.

#pragma once

#include "Definitions.h"
#include "Structures.h"

class SugarTag
{
    private:
    //--System
    //--Value
    char *mValue;

    protected:

    public:
    //--System
    SugarTag();
    ~SugarTag();
    static void DeleteThis(void *pPtr);

    //--Public Variables
    static SugarLinkedList *xTagList;

    //--Property Queries
    char *GetValue();
    float GetAsFloat();
    void GetXByY(float &sX, float &sY);
    void GetDimensions(float &sX, float &sY, float &sW, float &sH);

    //--Manipulators
    void SetValue(const char *pValue);
    void SetValueF(float pValue);

    //--Core Methods
    private:
    //--Private Core Methods
    public:
    //--Update
    //--File I/O
    //--Drawing
    //--Pointer Routing
    //--Static Functions
    static SugarTag *Get(const char *pName);
    static void Set(const char *pName, const char *pValue);
    static void SetF(const char *pName, float pValue);

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_Tag_Set(lua_State *L);
int Hook_Tag_Get(lua_State *L);

