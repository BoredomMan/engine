//--Base
#include "StarlightSocket.h"

//--Classes
//--CoreClasses
//--Definitions
//--Libraries
//--Managers
#include "NetworkManager.h"

//--Uses Winsock, only available on Windows or with a ported library.
#if defined _NETWORK_WINSOCK_

//=========================================== System ==============================================
StarlightSocket::StarlightSocket()
{
    //--[Construction]
    //--Boot Winsock if we haven't already.
    if(!xHasBootedWinsock)
    {
        //--Storage
        WSADATA tDataPack;
        WORD tVersion = MAKEWORD( 2, 0 );
        int tErrorCode = WSAStartup(tVersion, &tDataPack);
        if(tErrorCode) fprintf(stderr, "Attempted to boot Winsock, error code: %i\n", tErrorCode);
        xHasBootedWinsock = true;
    }

    //--[StarlightSocket]
    //--System
    mSocketIsStarted = false;

    //--Self-Data
    mSocketHandle = -1;

    //--Server
    mIsServerMode = false;
    memset(&mLocalSocketInfo, 0, sizeof(sockaddr_in));

    //--Client
    mIsClientMode = false;
    mClientHandle = SOCKET_ERROR;
    mClientLen = 0;
    mClientCheckConnect = false;
    memset(&mClientData, 0, sizeof(sockaddr_in));

    //--Meta-Information
    mHasOtherIdentified = false;
    mCounterForIdentify = -1;
    mOtherIdentifier = NULL;

    //--Packet controllers
    rPacketHandler = &StarlightSocket::HandlePacket;
}
StarlightSocket::~StarlightSocket()
{
    DisconnectSocket();
}
void StarlightSocket::StartSocket()
{
    //--Starts the socket and sets any flags it will need.  If the socket was already started, then
    //  do nothing.
    if(mSocketIsStarted) return;

    mSocketHandle = socket(AF_INET, SOCK_STREAM, 0);
    if(mSocketHandle == -1)
    {
        fprintf(stderr, "StarlightSocket reports unable to create new socket!\n");
        mSocketIsStarted = false;
        return;
    }

    //--Print success message.
    mSocketIsStarted = true;
    fprintf(stderr, "Booted StarlightSocket on handle %i\n", mSocketHandle);

    //--Set other static flags.
    ioctlsocket(mSocketHandle, FIONBIO, &xBlockingFlags);
    if(xBlockingFlags == STARLIGHTSOCKET_NONBLOCKING)
    {
        fprintf(stderr, "Booted in non-blocking mode.\n");
    }
}
void StarlightSocket::DisconnectSocket()
{
    //--Disconnects the socket from its mode and resets the StarlightSocket back to neutral.  Will
    //  not do anything if the socket never opened to begin with.
    if(!mSocketIsStarted) return;

    //--Close it.
    closesocket(mSocketHandle);

    //--Flip the flag off.
    mSocketIsStarted = false;
    mHasOtherIdentified = false;
    ResetString(mOtherIdentifier, NULL);
}
void StarlightSocket::DeleteThis(void *pPtr)
{
    delete ((StarlightSocket *)pPtr);
}
void StarlightSocket::TakeDownNetwork()
{
    //--Turns off and cleans the network up.
    xHasBootedWinsock = false;
    WSACleanup();
}
bool StarlightSocket::xHasBootedWinsock = false;
long unsigned int StarlightSocket::xBlockingFlags = STARLIGHTSOCKET_NONBLOCKING;

//====================================== Property Queries =========================================
bool StarlightSocket::IsSocketOpen()
{
    //--Returns whether or not the StarlightSocket is still functioning.  It does not necessarily need
    //  to be a client or a server, this only reports if the socket is open.
    return mSocketIsStarted;
}
int StarlightSocket::GetConnectionsTotal()
{
    //--In client mode, we *are* the connection!
    if(mIsClientMode) return 1;

    //--If not in client or server mode, zero.
    if(!mIsServerMode) return 0;

    //--Return all pending connections in server mode.
    if(mClientHandle != SOCKET_ERROR) return 1;
    return 0;
}

//========================================= Manipulators ==========================================
void StarlightSocket::StartServerOn(int pPort)
{
    //--Fail if already open or open as client.
    if(!mSocketIsStarted || mIsClientMode || mIsServerMode) return;

    //--Init the address data pack.
    memset(&mLocalSocketInfo, 0, sizeof(sockaddr_in));
    mLocalSocketInfo.sin_family = AF_INET;
    mLocalSocketInfo.sin_addr.s_addr = INADDR_ANY;
    mLocalSocketInfo.sin_port = htons(pPort);

    //--Bind it to a port.
    if(bind(mSocketHandle, (sockaddr *)&mLocalSocketInfo, sizeof(sockaddr_in)) == SOCKET_ERROR)
    {
        fprintf(stderr, "StarlightSocket:  Could not start server.  Aborting.\n");
        return;
    }

    //--Listen on this until we receive a connection.
    if(listen(mSocketHandle, 1) == SOCKET_ERROR)
    {
        fprintf(stderr, "StarlightSocket:  Error while listening.  Aborting.\n");
        return;
    }
    fprintf(stderr, "Socket has begun listening.\n");

    //--Reset data.
    mClientLen = sizeof(sockaddr_in);
    mClientHandle = SOCKET_ERROR;

    //--Wait for the client to connect.
    fprintf(stderr, "Awaiting a connecting client...\n");

    //--Set flags indicating the StarlightSocket's internal mode.
    mIsServerMode = true;
    mIsClientMode = false;
}
void StarlightSocket::ConnectTo(const char *pAddress, int pPort)
{
    //--Fail if already open or open as a server.
    if(!pAddress || !mSocketIsStarted || mIsClientMode || mIsServerMode) return;

    //--The address should be a string, such as "127.0.0.1".
    memset(&mLocalSocketInfo, 0, sizeof(sockaddr_in));
    mLocalSocketInfo.sin_family = AF_INET;
    mLocalSocketInfo.sin_addr.s_addr = inet_addr(pAddress);
    mLocalSocketInfo.sin_port = htons(pPort);

    //--Send the connection.  Server must exist or it will fail.  The connection call does not
    //  return instantly, but we need to check when we send data to make sure it worked.
    //--This checking takes place in the ClientUpdate() code below.
    mClientCheckConnect = false;
    if(connect(mSocketHandle, (sockaddr *)&mLocalSocketInfo, sizeof(sockaddr_in)))
    {
        //--If it's a "Would Block" error, then run a selection check until it connects.
        if(GetSocketErrorCode() == STARLIGHTSOCKET_ERROR_WOULDBLOCK)
        {
            //--Messages
            fprintf(stderr, "Socket is connecting...\n");
            mClientCheckConnect = true;
        }
        //--Other errors:  Fail immediately.
        else
        {
            fprintf(stderr, "StarlightSocket:  Error when connecting.\n");
            PrintError();
            return;
        }
    }
    //--Connect somehow passed on the first try.
    else
    {

    }

    //--Set flags indicating the StarlightSocket's internal mode.
    mIsServerMode = false;
    mIsClientMode = true;
}
void StarlightSocket::ForgetClient()
{
    //--Clears data associated with the client.
    mHasOtherIdentified = false;
    ResetString(mOtherIdentifier, NULL);
    mClientHandle = SOCKET_ERROR;
}
void StarlightSocket::SetConnecteeName(const char *pName)
{
    ResetString(mOtherIdentifier, pName);
}
void StarlightSocket::SetIdentifyTimer(int pTimer)
{
    mCounterForIdentify = pTimer;
}
void StarlightSocket::SetPacketHandler(StarlightPacketHandler pNewHandler)
{
    rPacketHandler = pNewHandler;
}

//========================================= Core Methods ==========================================
StarlightPacket StarlightSocket::PrefabPacket(int pType, void *pDataBuffer, int pLength)
{
    StarlightPacket nPacket;
    nPacket.mType = pType;
    nPacket.mLength = sizeof(uint32_t) + sizeof(uint16_t) + (sizeof(char) * pLength);
    memcpy(nPacket.mMessage, pDataBuffer, pLength);
    return nPacket;
}
void StarlightSocket::SendData(StarlightPacket pPacket)
{
    //--Sends a specific packet instead of a string packet.
    if(!mSocketIsStarted) return;

    //--Resolve which handle to send it to.  Clients send it to their local, the server sends it
    //  to the client.
    int tUseSocket = -1;
    if(mIsClientMode && !mClientCheckConnect)
    {
        tUseSocket = mSocketHandle;
    }
    else if(mIsServerMode && mClientHandle != SOCKET_ERROR)
    {
        tUseSocket = mClientHandle;
    }
    else
    {
        //fprintf(stderr, "StarlightSocket:  Cannot send data, connection not completed.\n");
        return;
    }

    //--Send and check the error code.
    int tErrorCode = send(tUseSocket, (char *)&pPacket, pPacket.mLength, 0);
    if(tErrorCode == SOCKET_ERROR)
    {
        fprintf(stderr, "StarlightSocket:  Sending data failed.\n");
        return;
    }

    //--Report successful send.
    //fprintf(stderr, "Successfully sent %i bytes of data.\n", tErrorCode);
}
void StarlightSocket::SendData(const char *pString)
{
    //--Overload, sends a string as the packet's data.
    if(!pString) return;

    //--Build a packet.
    StarlightPacket tPacket;
    tPacket.mType = STARLIGHTPACKET_STRING;
    strcpy(tPacket.mMessage, pString);
    tPacket.mLength = sizeof(uint32_t) + sizeof(uint16_t) + (sizeof(char) * strlen(tPacket.mMessage));

    SendData(tPacket);
}
void StarlightSocket::HandlePacket(StarlightSocket *rCaller, StarlightPacket &sPacket)
{
    //--Standard packet handler.  If a Socket does not override its logic caller, this one handles
    //  default behavior.  You can still call it directly if you don't care how a particular packet
    //  gets handled.
    if(!rCaller) return;

    //--Disconnect Instruction
    if(sPacket.mType == STARLIGHTPACKET_FORCEDISCONNECT)
    {
        rCaller->DisconnectSocket();
    }
    //--Requesting a handshake.  Send our local name.
    else if(sPacket.mType == STARLIGHTPACKET_REQUESTNAME)
    {
        char tDataBuffer[256];
        strcpy(tDataBuffer, NETWORK_PROGRAMNAME);
        StarlightPacket tPacket = PrefabPacket(STARLIGHTPACKET_ANSWERNAME, tDataBuffer, strlen(tDataBuffer));
        rCaller->SendData(tPacket);
    }
    //--Responding with the name from a request.
    else if(sPacket.mType == STARLIGHTPACKET_ANSWERNAME)
    {
        rCaller->SetConnecteeName(sPacket.mMessage);
        fprintf(stderr, "Connecter has identified itself as %s\n", sPacket.mMessage);
        rCaller->SetIdentifyTimer(-1);
    }
    //--Unknown packet.  Print a report.
    else
    {
        fprintf(stderr, "StarlightSocket:  Unknown packet read!  Contents:\n");
        fprintf(stderr, " Length: %i\n", sPacket.mLength);
        fprintf(stderr, " Type: %i\n", sPacket.mType);
        fprintf(stderr, " Contents: %s\n", sPacket.mMessage);
    }
}

//============================================ Update =============================================
void StarlightSocket::Update()
{
    //--Updates the socket (assuming it's running asycnhronously).  Sockets don't care what mode they
    //  are in, both can receive data.
    if(!mSocketIsStarted) return;
    int tUseHandle = -1;

    //--Server-mode.
    if(mIsServerMode)
    {
        ServerUpdate();
        tUseHandle = mClientHandle;
    }
    //--Client-mode.
    else if(mIsClientMode)
    {
        ClientUpdate();
        tUseHandle = mSocketHandle;
    }

    //--Fail if there's nobody to take data from.
    if(mIsServerMode && mClientHandle == SOCKET_ERROR) return;

    //--Fail if we're pending our connection.
    if(mIsClientMode && mClientCheckConnect) return;

    //--[Common Code]
    //--Both Client and Server must check for incoming data.
    //--Data is always transmitted in packets.  The packet always is of StarlightPacket type, which has
    //  additional data (type and length) for checking purposes.
    StarlightPacket tIncomingPacket;
    memset(&tIncomingPacket, 0, sizeof(StarlightPacket));
    int tBytesReceived = recv(tUseHandle, (char *)&tIncomingPacket, sizeof(StarlightPacket), 0);

    //--Connection was closed.
    if(tBytesReceived == 0)
    {
        fprintf(stderr, "StarlightSocket:  The remote client closed the connection.\n");
        if(mIsClientMode)
            DisconnectSocket();
        else
            mClientHandle = SOCKET_ERROR;
        return;
    }
    //--Error!
    else if(tBytesReceived == SOCKET_ERROR)
    {
        //--If the error could was "Would Block" then ignore it, that's an async safe error.
        if(GetSocketErrorCode() == STARLIGHTSOCKET_ERROR_WOULDBLOCK)
        {
        }
        //--Actual error!  Print it.
        else
        {
            fprintf(stderr, "StarlightSocket:  Error when reading from the socket.\n");
            PrintError();
            DisconnectSocket();
            return;
        }
    }
    //--Received a packet.  Let the logic function do it.
    else
    {
        if(rPacketHandler)
        {
            rPacketHandler(this, tIncomingPacket);
        }
    }

    //--[Identification Code]
    //--The other side of the connection should send a human-readable string so we have at least
    //  some idea who's talking.  This is by no means unique, it's up to the other programmer.
    if(mCounterForIdentify > 0) mCounterForIdentify --;
    if(mCounterForIdentify == 0 && !mHasOtherIdentified)
    {
        //--Identity has not been received yet!  Resend the handshake.
        if(!mOtherIdentifier)
        {
            char tDataBuffer[] = "Requesting handshake";
            StarlightPacket tPacket = PrefabPacket(STARLIGHTPACKET_REQUESTNAME, tDataBuffer, strlen(tDataBuffer));
            SendData(tPacket);
            fprintf(stderr, "Sent identification packet request\n");
            mCounterForIdentify = 30;
        }
        //--Identity has been received.  Negative this off.
        else
        {
            mCounterForIdentify = -1;
            mHasOtherIdentified = true;
        }
    }
}
void StarlightSocket::ClientUpdate()
{
    //--Subroutine which handles client-sided updates.
    if(!mClientCheckConnect) return;

    //--Timeout.
    timeval tTimeout;
    tTimeout.tv_sec = 0;
    tTimeout.tv_usec = 1000;

    //--Build a "set" of awaiting sockets.  This is probably bloatware...
    //  It's also Windows-specific.
    FD_SET tWriteSet;
    FD_ZERO(&tWriteSet);
    FD_SET(mSocketHandle, &tWriteSet);
    //fprintf(stderr, "Waiting for select - ");
    if(select(0, NULL, &tWriteSet, NULL, &tTimeout) == 0)
    {
        //fprintf(stderr, "Waiting to be connected...\n");
        return;
    }
    fprintf(stderr, "Done\n");

    //--If we got this far, we've connected successfully.
    mClientCheckConnect = false;
    fprintf(stderr, "Client connected to server!\n");

    //--Specify a request for handshake name.  This will be handled next update.
    mCounterForIdentify = 1;
}
void StarlightSocket::ServerUpdate()
{
    //--Subroutine which handles server-sided updates.
    if(mClientHandle != SOCKET_ERROR) return;

    //--Check if any new clients tried to connect.
    int tNewClientHandle = accept(mSocketHandle, (sockaddr *)&mClientData, &mClientLen);
    if(tNewClientHandle == SOCKET_ERROR) return;

    //--Store the client handle.
    mClientHandle = tNewClientHandle;
    fprintf(stderr, "Got client handle as %i\n", mClientHandle);

    //--Specify a request for handshake name.  This will be handled next update.
    mCounterForIdentify = 1;
}

//=========================================== File I/O ============================================
//=========================================== Drawing =============================================
//======================================= Pointer Routing =========================================
//====================================== Static Functions =========================================
int StarlightSocket::GetSocketErrorCode()
{
    //--OS-independent code for StarlightSocket error codes.
    int tErrorCode = WSAGetLastError();

    //--Scan against known codes.
    if(tErrorCode == WSAEWOULDBLOCK) return STARLIGHTSOCKET_ERROR_WOULDBLOCK;

    //--Reset the code to 0.
    WSASetLastError(0);

    //--Return failure, error code not known/handled.
    return STARLIGHTSOCKET_ERROR_NOTDEFINED;
}
void StarlightSocket::PrintError()
{
    //--Prints the last error that the StarlightSocket processed.  This is platform specific!
    int tErrorCode = WSAGetLastError();
    fprintf(stderr, "-Last Winsock error was %i\n", tErrorCode);

    //--Reset the code to 0.
    WSASetLastError(0);
}

//========================================= Lua Hooking ===========================================
//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================

#endif
