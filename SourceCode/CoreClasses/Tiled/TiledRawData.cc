//--Base
#include "TiledRawData.h"

//--Classes
#include "TiledLevel.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarLinkedList.h"

//--Definitions
//--GUI
//--Libraries
//--Managers
#include "DebugManager.h"

//=========================================== System ==============================================
TiledRawData::TiledRawData()
{
    //--[TiledRawData]
    //--System
    mHasReadFileSuccessfully = false;
    mName = InitializeString("Unnamed Tiled Raw Data");

    //--Image Storage
    mTilesetList = NULL;

    //--Layer Storage
    mWid = 0;
    mHei = 0;
    mLayerList = new SugarLinkedList(true);
}
TiledRawData::~TiledRawData()
{
    free(mName);
    delete mTilesetList;
    delete mLayerList;
}

//================================== Static Deletion Methods ======================================
void TiledRawData::DeleteLayerPack(void *pPtr)
{
    //--Basic
    if(!pPtr) return;
    TiledLayerPack *rPtr = (TiledLayerPack *)pPtr;

    //--Universal
    free(rPtr->mName);
    DeletePropertiesPack(rPtr->mProperties);

    //--Only used in a tile layer.
    free(rPtr->mTileData);

    //--Only used in an object layer.
    delete rPtr->mObjectInfo;

    //--Final.
    free(rPtr);
}
void TiledRawData::DeleteTilesetPack(void *pPtr)
{
    if(!pPtr) return;
    TilesetInfoPack *rPtr = (TilesetInfoPack *)pPtr;
    free(rPtr->mName);
    delete rPtr->mTileset;
}
void TiledRawData::DeletePropertiesPack(void *pPtr)
{
    if(!pPtr) return;
    PropertiesPack *rPtr = (PropertiesPack *)pPtr;
    for(int i = 0; i < rPtr->mPropertiesTotal; i ++)
    {
        free(rPtr->mKeys[i]);
        free(rPtr->mVals[i]);
    }
    free(rPtr->mKeys);
    free(rPtr->mVals);
    free(rPtr);
}
void TiledRawData::DeleteObjectPack(void *pPtr)
{
    if(!pPtr) return;
    ObjectInfoPack *rPack = (ObjectInfoPack *)pPtr;

    free(rPack->mName);
    free(rPack->mType);
    DeletePropertiesPack(rPack->mProperties);
    free(rPack);
}

//====================================== Property Queries =========================================
char *TiledRawData::GetName()
{
    return mName;
}
bool TiledRawData::WasReadSuccessful()
{
    return mHasReadFileSuccessfully;
}
int TiledRawData::GetLayersTotal()
{
    return mLayerList->GetListSize();
}
TiledLayerPack *TiledRawData::GetLayer(int pSlot)
{
    return (TiledLayerPack *)mLayerList->GetElementBySlot(pSlot);
}
int TiledRawData::GetTotalWidth()
{
    return mWid;
}
int TiledRawData::GetTotalHeight()
{
    return mHei;
}

//========================================= Manipulators ==========================================
//========================================= Core Methods ==========================================
TiledLayerPack *TiledRawData::CreateLayerPack()
{
    //--Creates and returns a blanked layer pack.
    SetMemoryData(__FILE__, __LINE__);
    TiledLayerPack *nPack = (TiledLayerPack *)starmemoryalloc(sizeof(TiledLayerPack));

    //--Universal
    nPack->mName = NULL;
    nPack->mLayerType = TLP_TYPE_FAIL;
    nPack->mProperties = NULL;

    //--Only used in a tile layer.
    nPack->mX = 0;
    nPack->mY = 0;
    nPack->mW = 0;
    nPack->mH = 0;
    nPack->mTileData = NULL;

    //--Only used in an object layer.
    nPack->mObjectInfo = new SugarLinkedList(true);
    return nPack;
}

//===================================== Private Core Methods ======================================
//============================================ Update =============================================
//=========================================== Drawing =============================================
//======================================= Pointer Routing =========================================
SugarLinkedList *TiledRawData::LiberateTilesetData()
{
    //--Removes local references to the tileset data. They will not be deleted when the class is.
    SugarLinkedList *rReturnList = mTilesetList;
    mTilesetList = NULL;
    return rReturnList;
}

//====================================== Static Functions =========================================
char *TiledRawData::GetValueFromKey(PropertiesPack *pProperties, const char *pKey)
{
    //--Returns the value associated with the given key in the properties pack. May legally return
    //  NULL if the key wasn't found.
    //--Do not deallocate the string, it's a direct pointer.
    if(!pProperties || !pKey) return NULL;

    //--Parse.
    for(int i = 0; i < pProperties->mPropertiesTotal; i ++)
    {
        if(!strcasecmp(pProperties->mKeys[i], pKey))
        {
            return pProperties->mVals[i];
        }
    }

    //--Not found. Return NULL.
    return NULL;
}

//========================================= Lua Hooking ===========================================
//======================================= Command Hooking =========================================
//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
//=================================================================================================
//                                       Command Functions                                       ==
//=================================================================================================
