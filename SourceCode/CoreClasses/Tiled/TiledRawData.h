//--[TiledRawData]
//--Class that has a plain representation of all the data that makes up a file exported from Tiled.
//  The file must be created from an .slf file that was exported by Tiled using a special routine.
//  The class may contain local image data representing tilesets which can be 'liberated' from the
//  class if desired.
//--A bitmap, once liberated, will not be deleted when the class is destroyed.

#pragma once

#include "Definitions.h"
#include "Structures.h"

//--[Local Definitions]
#define TLP_TYPE_FAIL 0
#define TLP_TYPE_TILE 1
#define TLP_TYPE_OBJECT 2
#define TLP_TYPE_IMAGE 3

//--[Forward Declarations]
struct TiledLayerPack;
struct TilesetInfoPack;
struct PropertiesPack;
struct ObjectInfoPack;

//--[Local Class-like Structures]
//--Layer of tiles. Can store additional properties and objects.
typedef struct TiledLayerPack
{
    //--Universal
    char *mName;
    uint8_t mLayerType;
    PropertiesPack *mProperties;

    //--Only used in a tile layer.
    int16_t mX;
    int16_t mY;
    int16_t mW;
    int16_t mH;
    int16_t *mTileData;

    //--Only used in an object layer.
    SugarLinkedList *mObjectInfo;
}TiledLayerPack;

//--[Local Structures]
//--[Class]
class TiledRawData
{
    private:
    //--System
    bool mHasReadFileSuccessfully;
    char *mName;

    //--Image Storage
    SugarLinkedList *mTilesetList;

    //--Layer Storage
    int mWid, mHei;
    SugarLinkedList *mLayerList;

    protected:

    public:
    //--System
    TiledRawData();
    ~TiledRawData();

    //--Static Deletion Methods
    static void DeleteLayerPack(void *pPtr);
    static void DeleteTilesetPack(void *pPtr);
    static void DeletePropertiesPack(void *pPtr);
    static void DeleteObjectPack(void *pPtr);

    //--Public Variables
    //--Property Queries
    char *GetName();
    bool WasReadSuccessful();
    int GetLayersTotal();
    TiledLayerPack *GetLayer(int pSlot);
    int GetTotalWidth();
    int GetTotalHeight();

    //--Manipulators
    //--Core Methods
    TiledLayerPack *CreateLayerPack();

    private:
    //--Private Core Methods
    public:
    //--Update
    //--File I/O
    void ReadLevel(const char *pPath);
    SugarLinkedList *ReadTilesets(SugarLumpManager *pSLM, VirtualFile *pVFile);
    TiledLayerPack *ReadLayer(VirtualFile *pVFile);
    void ReadTileData(VirtualFile *pVFile, TiledLayerPack *pDataPack);
    void ReadObjectData(VirtualFile *pVFile, TiledLayerPack *pDataPack);
    static PropertiesPack *ReadProperties(VirtualFile *pVFile);

    //--Drawing
    //--Pointer Routing
    SugarLinkedList *LiberateTilesetData();

    //--Static Functions
    static char *GetValueFromKey(PropertiesPack *pProperties, const char *pKey);

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions

