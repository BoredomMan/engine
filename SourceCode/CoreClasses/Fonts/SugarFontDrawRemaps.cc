//--Base
#include "SugarFont.h"

//--Classes
//--CoreClasses
#include "SugarBitmap.h"

//--Definitions
//--Libraries
#include "DataLibrary.h"

//--Managers

//--This is a freetype-only function.
#if defined _FONTTYPE_FREETYPE_
void SugarFont::DrawTextImg(float pX, float pY, int pFlags, float pSize, const char *pText, int pRemapsTotal, char **pRemaps, char **pImgPaths)
{
    //--Renders a block of text using the given properties. The extra capability of this function is that it can dynamically
    //  print images in-stride with the text. Obviously these images should be sized for the text because the program won't care.
    //--The way to print these images is to use [IMG|Name]. The Name is then checked against the pRemaps array, which is paralell
    //  with pImagPaths. If a match is found, the image from pImgPaths replaces the [IMG|Name] tag in its entirety.
    //--This routine is obviously computationally more expensive than normal rendering. Only use it in certain instances.
    if(!pText || !pRemaps || !pImgPaths) return;

    //--[Error Checking]
    if(!mPrecachedHandle || !mPrecacheData) return;
    if(pSize == 0.0f) pSize = 1.0f;

    //--[Positioning]
    //--Move cursor. If flagged, use this as the center point (mod the offset).
    if(pFlags & SUGARFONT_AUTOCENTER_X)
    {
        pX = pX - (GetTextWidth(pText) / 2.0f * pSize);
    }
    else if(pFlags & SUGARFONT_RIGHTALIGN_X)
    {
        pX = pX - (GetTextWidth(pText) * pSize);
    }
    if(pFlags & SUGARFONT_AUTOCENTER_Y)
    {
        pY = pY - (GetTextHeight() / 2.0f * pSize);
    }
    glTranslatef(pX, pY, 0.0f);

    //--Sizing. This is only used if the size is not 1.0f, since scaling is non-trivial.
    if(pSize != 1.0f)
    {
        float tHeight = GetTextHeight();
        glTranslatef(0.0f, tHeight / 2.0f, 0.0f);
        glScalef(pSize, pSize, 1.0f);
        glTranslatef(0.0f, tHeight / -2.0f, 0.0f);
    }

    //--[Rendering]
    //--Grab each glyph and render it in turn. First, bind the texture (precached mode).
    glBindTexture(GL_TEXTURE_2D, mPrecachedHandle);

    //--For each letter...
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    float tRunningOffset = 0.0f;
    int tLen = strlen(pText);
    for(int i = 0; i < tLen-1; i ++)
    {
        //--Get the index and store it as an integer. If the letter somehow does not have its
        //  glLists built, skip it.
        int8_t tLetter = pText[i];
        int8_t tNextLetter = pText[i+1];
        if(!mPrecacheData[tLetter].mGLList) continue;

        //--If the letter is a [, check for the [IMG| tag.
        if(pText[i+0] == '[' && pText[i+1] == 'I' && pText[i+2] == 'M' && pText[i+3] == 'G' && pText[i+4] == '|')
        {
            int c = 0;
            char tBuffer[256];
            for(int p = i+5; p < tLen; p ++)
            {
                if(pText[p] == ']')
                {
                    break;
                }
                else
                {
                    tBuffer[c+0] = pText[p];
                    tBuffer[c+1] = '\0';
                    c ++;
                }
                i = i + c - 1;
            }

            //--Check the remaps.
            for(int p = 0; p < pRemapsTotal; p ++)
            {
                //--Error check.
                if(pRemaps[p] == NULL) continue;
                if(strcmp(pRemaps[p], tBuffer)) continue;

                //--Match. Display it.
                SugarBitmap *rBitmap = (SugarBitmap *)rDataLibrary->GetEntry(pImgPaths[p]);
                if(rBitmap)
                {
                    rBitmap->Draw(0, 2);
                    glTranslatef(rBitmap->GetTrueWidth(), 0.0f, 0.0f);
                    tRunningOffset = tRunningOffset + rBitmap->GetTrueWidth();
                    glBindTexture(GL_TEXTURE_2D, mPrecachedHandle);
                }

                //--In all cases, stop remap checking.
                break;
            }

        }
        //--Normal case.
        else
        {
            //--Render.
            glCallList(mPrecacheData[tLetter].mGLList);

            //--Reposition the cursor.
            float cKernDistance = mPrecacheData[tLetter].mKerningDistance[tNextLetter];
            glTranslatef(cKernDistance, 0.0f, 0.0f);
            tRunningOffset = tRunningOffset + cKernDistance;
        }

    }

    //--Last letter. Doesn't need to use the kerning.
    int8_t tLastLetter = pText[tLen-1];
    if(mPrecacheData[tLastLetter].mGLList) glCallList(mPrecacheData[tLastLetter].mGLList);

    //--[Clean Up]
    //--Remove running offset.
    glTranslatef(tRunningOffset * -1.0f, 0.0f, 0.0f);

    //--Undo the size change.
    if(pSize != 1.0f)
    {
        float tHeight = GetTextHeight();
        glTranslatef(0.0f, tHeight / 2.0f, 0.0f);
        glScalef(1.0f / pSize, 1.0f / pSize, 1.0f);
        glTranslatef(0.0f, tHeight / -2.0f, 0.0f);
    }

    //--Undo by cursor.
    glTranslatef(pX * -1.0f, pY * -1.0f, 0.0f);
}

//--If freetype is not defined, a dummy version of the function is declared. It does nothing.
#else
void SugarFont::DrawTextImg(float pX, float pY, int pFlags, float pSize, const char *pText, int pRemapsTotal, char **pRemaps, char **pImgPaths)
{
    return;
}

#endif
