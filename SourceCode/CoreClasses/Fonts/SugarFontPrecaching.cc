//--Base
#include "SugarFont.h"

//--Classes
//--CoreClasses
#include "SugarBitmap.h"

//--Definitions
#include "GlDfn.h"

//--Libraries
//--Managers

//--[Notes]
//--This is designed to work only in Freetype. Bitmaps don't need it and no others are supported yet.

//--[Common]
void SugarFont::ClearPrecache()
{
    if(!mPrecacheData) return;
    glDeleteTextures(1, &mPrecachedHandle);
    for(int i = 0; i < SUGARFONT_PRECACHE_LETTERS; i ++)
    {
        glDeleteLists(mPrecacheData[i].mGLList, 1);
    }
    free(mPrecacheData);
    mPrecacheData = NULL;
}

#if defined _FONTTYPE_FREETYPE_
void SugarFont::PrecacheData(uint8_t pFilterFlag)
{
    //--With the font already loaded, place the data in a precached array. The images will all
    //  be amalgamated onto a single texture, and the offsets stored in data packs.
    //--We use a multipass technique to allocate space for all the letters.
    float cMaxSizeX = 1024.0f;
    float tWidestSoFar = 0.0f; //Used for italics.
    float tTallestSoFar = 0.0f;
    mTallestCharacter = 0.0f;
    float cPadding = 4.0f;
    float cBlackPadding = 0.0f;

    //--Black padding.
    if(pFilterFlag & SUGARFONT_PRECACHE_WITH_EDGE)
    {
        cPadding = cPadding + 2.0f;
        cBlackPadding = xOutlineSize;
    }

    //--Allocate software space.
    SetMemoryData(__FILE__, __LINE__);
    mPrecacheData = (SugarFontPrecacheData *)starmemoryalloc(sizeof(SugarFontPrecacheData) * SUGARFONT_PRECACHE_LETTERS);

    //--Make sure the GL state is correct.
    glEnable(GL_TEXTURE_2D);

    //--Running size counters.
    float tTotalPixelsX = cPadding + cBlackPadding;
    float tTotalPixelsY = cPadding + cBlackPadding;

    //--For each ASCII letter...
    for(int i = 0; i < SUGARFONT_PRECACHE_LETTERS; i ++)
    {
        //--[Setup]
        //--Fetch the letter.
        uint32_t tGlyphIndex = FT_Get_Char_Index(rFont, i);
        if(!tGlyphIndex) continue;
        FT_Load_Glyph(rFont, tGlyphIndex, FT_LOAD_RENDER);
        FT_GlyphSlot rGlyph = rFont->glyph;

        //--[Initialization]
        //--Store the offset data.
        mPrecacheData[i].mLftOffset = tTotalPixelsX;
        mPrecacheData[i].mTopOffset = tTotalPixelsY;

        //--Zero the list.
        mPrecacheData[i].mGLList = 0;
        mPrecacheData[i].mGLListItalic = 0;
        mPrecacheData[i].mGLListXMirror = 0;
        mPrecacheData[i].mGLListYMirror = 0;

        //--Fast-access copies of metrics
        mPrecacheData[i].mWidth = rGlyph->bitmap.width;
        mPrecacheData[i].mHeight = rGlyph->bitmap.rows;
        mPrecacheData[i].mAdvanceX = (rGlyph->advance.x >> 6) + cBlackPadding;
        mPrecacheData[i].mHoriBearingX = rGlyph->metrics.horiBearingX >> 6;
        mPrecacheData[i].mHoriBearingY = rGlyph->metrics.horiBearingY >> 6;
        mPrecacheData[i].mFlippedTopOffset = mInternalSize - (rGlyph->metrics.horiBearingY >> 6) + (rGlyph->bitmap.rows * 1.5f);

        //--Zero the kernings.
        memset(mPrecacheData[i].mKerningDistance, 0, sizeof(float) * SUGARFONT_PRECACHE_LETTERS);

        //--[Kernings]
        //--Certain letters have certain kerning properties. This is the auto-generated kerning,
        //  which can be replaced with manually set kernings later.
        //--First, Space, which has a fixed kerning.
        if(i == ' ')
        {
            for(int p = 0; p < SUGARFONT_PRECACHE_LETTERS; p ++)
            {
                mPrecacheData[i].mKerningDistance[p] = mInternalSize * 0.33f;
            }
        }
        //--These punctuation letters have extra width provided.
        else if(i == '.' || i == ',' || i == '\'' || i == '"' || i == ';' || i == ':' || i == '!')
        {
            for(int p = 0; p < SUGARFONT_PRECACHE_LETTERS; p ++)
            {
                //--Default.
                mPrecacheData[i].mKerningDistance[p] = mPrecacheData[i].mWidth;

                //--If the target is another punctuation, greatly reduce the bonus.
                if(p == '.' || p == ',' || p == '\'' || p == '"' || p == ';' || p == ':' || p == '!')
                {
                    mPrecacheData[i].mKerningDistance[p] = mPrecacheData[i].mKerningDistance[p] * 0.50f;
                }
                //--Spaces get a larger bonus after periods and commas.
                else if(p == ' ' && (i == '.' || i == ','))
                {
                    mPrecacheData[i].mKerningDistance[p] = mPrecacheData[i].mKerningDistance[p] * 1.20f;
                }
                //--All other letters.
                else
                {
                }
            }
        }
        //--All other letters use their width plus 1 pixel.
        else
        {
            for(int p = 0; p < SUGARFONT_PRECACHE_LETTERS; p ++)
            {
                //--Default.
                mPrecacheData[i].mKerningDistance[p] = mPrecacheData[i].mWidth + 1.0f;

                //--If the target is punctuation except semicolon, actually provide a penalty to width!
                if(p == '.' || p == ',' || p == '\'' || p == '"' || p == ':' || p == '!')
                {
                    mPrecacheData[i].mKerningDistance[p] = mPrecacheData[i].mKerningDistance[p] * 0.90f;
                }
                //--All other letters.
                else
                {
                }
            }
        }

        //--[Broken Sample Code]
        //--This is what Kernings look like, except they don't work on the font I need them to. Damn it.
        /*FT_Vector tVector;
        for(int p = 0; p < SUGARFONT_PRECACHE_LETTERS; p ++)
        {
            int tErrorCode = FT_Get_Kerning(rFont, tGlyphIndex, FT_Get_Char_Index(rFont, p), FT_KERNING_DEFAULT, &tVector);
            mPrecacheData[i].mKerningDistance[p] = tVector.x;
        }*/

        //--[Metrics]
        //--If this is the tallest character, store that for later.
        if(mTallestCharacter < mPrecacheData[i].mHeight) mTallestCharacter = mPrecacheData[i].mHeight;

        //--Position the cursor for this glyph. Go to the next line if it's too large.
        if(rGlyph->bitmap.rows > tTallestSoFar) tTallestSoFar = rGlyph->bitmap.rows;
        if(mPrecacheData[i].mWidth > tWidestSoFar) tWidestSoFar = mPrecacheData[i].mWidth;

        //--Advance the cursor.
        tTotalPixelsX = tTotalPixelsX + mPrecacheData[i].mWidth + cPadding + mEndPadding + cBlackPadding;
        if(tTotalPixelsX > cMaxSizeX)
        {
            tTotalPixelsY = tTotalPixelsY + tTallestSoFar + cPadding + mEndPadding + cBlackPadding + 10.0f;
            tTallestSoFar = rGlyph->bitmap.rows;
            mPrecacheData[i].mLftOffset = cPadding + cBlackPadding + mEndPadding;
            mPrecacheData[i].mTopOffset = tTotalPixelsY;
            tTotalPixelsX = cPadding + cBlackPadding + mEndPadding;
            tTotalPixelsX = tTotalPixelsX + mPrecacheData[i].mWidth + cPadding + mEndPadding + cBlackPadding;
        }
    }

    //--Add pixels for the last line.
    tTotalPixelsY = tTotalPixelsY + tTallestSoFar;

    //--Now that we know how big the texture ought to be, create an array that will hold all the data
    //  and pad the edges to 1024 X size.
    SetMemoryData(__FILE__, __LINE__);
    uint32_t *tTextureData = (uint32_t *)starmemoryalloc(sizeof(uint32_t) * 1024 * tTotalPixelsY);
    for(int i = 0; i < 1024 * tTotalPixelsY; i ++) tTextureData[i] = 0;

    //--For each ASCII letter...
    for(int i = 0; i < SUGARFONT_PRECACHE_LETTERS; i ++)
    {
        //--Fetch the letter.
        uint32_t tGlyphIndex = FT_Get_Char_Index(rFont, i);
        if(!tGlyphIndex) continue;
        FT_Load_Glyph(rFont, tGlyphIndex, FT_LOAD_RENDER);
        FT_GlyphSlot rGlyph = rFont->glyph;

        //--Place the bitmap data into the array. Remember that it's not a single line, but will be
        //  unpacked to a 2D texture!
        for(int y = 0; y < (int)rGlyph->bitmap.rows; y ++)
        {
            //--Compute the Y percentage.
            float cBaselineFactor = 1.0f;
            float tYPercent = (float)y / (float)rGlyph->bitmap.rows;

            //--When downfading, the baseline decreases as we get closer to the bottom of the glyph.
            if(pFilterFlag & SUGARFONT_PRECACHE_WITH_DOWNFADE)
            {
                cBaselineFactor = (xDownfadeBegin) + (tYPercent * (xDownfadeEnd - xDownfadeBegin));
            }

            //--Iterate across the row.
            for(int x = 0; x < (int)rGlyph->bitmap.width; x ++)
            {
                //--Needs a type cast.
                uint8_t *rBuffer = (uint8_t *)rGlyph->bitmap.buffer;

                //--Calculate the position for each pixel.
                int tPosition = (mPrecacheData[i].mLftOffset + x) + ((mPrecacheData[i].mTopOffset + y) * 1024);

                //--Get the baseline color.
                uint8_t tBaseline = rBuffer[x + (y * rGlyph->bitmap.width)];

                //--When using a black outline, the color greys but the opacity stays at 100%.
                if(pFilterFlag & SUGARFONT_PRECACHE_WITH_EDGE)
                {
                    //--Eliminate pixels with a low baseline.
                    if(tBaseline < xBlackOutlineCutoff)
                    {
                        tTextureData[tPosition] = 0x00000000;
                    }
                    //--Pixel displays but it may be mostly grey.
                    else
                    {
                        //--If the baseline is below 192, it becomes 192.
                        if(tBaseline < 255) tBaseline = 255;

                        //--Apply colors.
                        uint8_t tRed = tBaseline * cBaselineFactor;
                        uint8_t tGrn = tBaseline * cBaselineFactor;
                        uint8_t tBlu = tBaseline * cBaselineFactor;
                        uint8_t tAlp = 255;
                        tTextureData[tPosition] = (tAlp << 24) | (tBlu << 16) | (tGrn << 8) | (tRed);
                    }

                }
                //--When not using a black outline, the color is white but the alpha varies.
                else
                {
                    uint8_t tRed = 255 * cBaselineFactor;
                    uint8_t tGrn = 255 * cBaselineFactor;
                    uint8_t tBlu = 255 * cBaselineFactor;
                    uint8_t tAlp = tBaseline;
                    tTextureData[tPosition] = (tAlp << 24) | (tBlu << 16) | (tGrn << 8) | (tRed);
                }
            }
        }
    }

    //--Save some data about the texture.
    mPrecachedSizeX = 1024.0f;
    mPrecachedSizeY = tTotalPixelsY;

    //--[Black Edging]
    //--Puts a black outline around every letter, if flagged.
    if(pFilterFlag & SUGARFONT_PRECACHE_WITH_EDGE) ApplyBlackOutline(mPrecachedSizeX, mPrecachedSizeY, tTextureData);

    //--[Texture Upload]
    //--Generate.
    glGenTextures(1, &mPrecachedHandle);
    glBindTexture(GL_TEXTURE_2D, mPrecachedHandle);

    //--Filter using GL_NEAREST for magnification. The font will remain pixellated. Good for small fonts.
    if(pFilterFlag & SUGARFONT_PRECACHE_WITH_NEAREST)
    {
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    }
    //--Linear filtering. Good for fonts that are going to be large on screen.
    else
    {
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    }

    //--Common. These properties do not change with the filter flag.
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

    //--Upload.
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 1024, tTotalPixelsY, 0, GL_RGBA, GL_UNSIGNED_BYTE, tTextureData);

    //--Clean.
    free(tTextureData);

    //--[List Generation]
    //--Generates GL Lists for every single glyph. For now, this handles only ASCII characters.
    for(int i = 0; i < SUGARFONT_PRECACHE_LETTERS; i ++)
    {
        //--Fetch the letter.
        uint32_t tGlyphIndex = FT_Get_Char_Index(rFont, i);
        if(!tGlyphIndex) continue;
        FT_Load_Glyph(rFont, tGlyphIndex, FT_LOAD_RENDER);
        FT_GlyphSlot rGlyph = rFont->glyph;

        //--Temporary Variables
        float tLft = (rGlyph->metrics.horiBearingX >> 6)                       - cBlackPadding;
        float tTop = (mTallestCharacter - (rGlyph->metrics.horiBearingY >> 6)) - cBlackPadding;
        float tRgt = (tLft + rGlyph->bitmap.width) + (cBlackPadding * 2.0f) + mEndPadding;
        float tBot = (tTop + rGlyph->bitmap.rows)  + (cBlackPadding * 2.0f) + mEndPadding;
        if(pFilterFlag & SUGARFONT_PRECACHE_WITH_SPECIAL_S && i == 'S')
        {
            tTop = tTop + 0.5f;
            tBot = tBot + 0.5f;
        }

        //--Texture coordinates. Remember that OpenGL starts them upside-down.
        float tTrueWid = mPrecacheData[i].mWidth + mEndPadding;
        float tTrueHei = mPrecacheData[i].mHeight + mEndPadding;
        float tTexLft = (mPrecacheData[i].mLftOffset                            - cBlackPadding) / mPrecachedSizeX;
        float tTexTop = (mPrecacheData[i].mTopOffset                            - cBlackPadding) / mPrecachedSizeY;
        float tTexRgt = (mPrecacheData[i].mLftOffset + tTrueWid + cBlackPadding) / mPrecachedSizeX;
        float tTexBot = (mPrecacheData[i].mTopOffset + tTrueHei + cBlackPadding) / mPrecachedSizeY;

        //--Middle-positions. Used for X/Y mirroring.
        float cMidX = (tLft + tRgt) * 0.50f;
        float cMidY = (tTop + tBot) * 0.50f;
        float cTexMidX = (tTexLft + tTexRgt) * 0.50f;
        float cTexMidY = (tTexTop + tTexBot) * 0.50f;

        //--If using precached lists, we can speed up rendering considerably here.
        mPrecacheData[i].mGLList = glGenLists(1);
        glNewList(mPrecacheData[i].mGLList, GL_COMPILE);
        glBegin(GL_QUADS);
            glTexCoord2d(tTexLft, tTexTop); glVertex2f(tLft, tTop);
            glTexCoord2d(tTexLft, tTexBot); glVertex2f(tLft, tBot);
            glTexCoord2d(tTexRgt, tTexBot); glVertex2f(tRgt, tBot);
            glTexCoord2d(tTexRgt, tTexTop); glVertex2f(tRgt, tTop);
        glEnd();
        glEndList();

        //--Italics. Moves the top coordinates over a bit.
        if(i == '*')
        {
            mPrecacheData[i].mGLListItalic = glGenLists(1);
            glNewList(mPrecacheData[i].mGLListItalic, GL_COMPILE);
            glBegin(GL_QUADS);
                glTexCoord2d(tTexLft, tTexTop); glVertex2f(tLft, tTop);
                glTexCoord2d(tTexLft, tTexBot); glVertex2f(tLft, tBot);
                glTexCoord2d(tTexRgt, tTexBot); glVertex2f(tRgt, tBot);
                glTexCoord2d(tTexRgt, tTexTop); glVertex2f(tRgt, tTop);
            glEnd();
            glEndList();
        }
        else
        {
            float tQuarterWid = ((float)tWidestSoFar * 0.35f) * ((float)mPrecacheData[i].mHeight / (float)tTallestSoFar);
            mPrecacheData[i].mGLListItalic = glGenLists(1);
            glNewList(mPrecacheData[i].mGLListItalic, GL_COMPILE);
            glBegin(GL_QUADS);
                glTexCoord2d(tTexLft, tTexTop); glVertex2f(tLft + tQuarterWid, tTop);
                glTexCoord2d(tTexLft, tTexBot); glVertex2f(tLft +        0.0f, tBot);
                glTexCoord2d(tTexRgt, tTexBot); glVertex2f(tRgt +        0.0f, tBot);
                glTexCoord2d(tTexRgt, tTexTop); glVertex2f(tRgt + tQuarterWid, tTop);
            glEnd();
            glEndList();
        }

        //--Precache the X-Mirror rendering.
        mPrecacheData[i].mGLListXMirror = glGenLists(1);
        glNewList(mPrecacheData[i].mGLListXMirror, GL_COMPILE);
        glBegin(GL_QUADS);
            glTexCoord2d(tTexLft,  tTexTop); glVertex2f(tLft,  tTop);
            glTexCoord2d(tTexLft,  tTexBot); glVertex2f(tLft,  tBot);
            glTexCoord2d(cTexMidX, tTexBot); glVertex2f(cMidX, tBot);
            glTexCoord2d(cTexMidX, tTexTop); glVertex2f(cMidX, tTop);

            glTexCoord2d(cTexMidX, tTexTop); glVertex2f(cMidX, tTop);
            glTexCoord2d(cTexMidX, tTexBot); glVertex2f(cMidX, tBot);
            glTexCoord2d(tTexLft,  tTexBot); glVertex2f(tRgt,  tBot);
            glTexCoord2d(tTexLft,  tTexTop); glVertex2f(tRgt,  tTop);
        glEnd();
        glEndList();

        //--Precache the Y-Mirror rendering.
        mPrecacheData[i].mGLListYMirror = glGenLists(1);
        glNewList(mPrecacheData[i].mGLListYMirror, GL_COMPILE);
        glBegin(GL_QUADS);
            glTexCoord2d(tTexLft, tTexTop); glVertex2f(tLft, tTop);
            glTexCoord2d(tTexLft, cTexMidY); glVertex2f(tLft, cMidY);
            glTexCoord2d(tTexRgt, cTexMidY); glVertex2f(tRgt, cMidY);
            glTexCoord2d(tTexRgt, tTexTop); glVertex2f(tRgt, tTop);

            glTexCoord2d(tTexLft, cTexMidY); glVertex2f(tLft, cMidY);
            glTexCoord2d(tTexLft, tTexTop); glVertex2f(tLft, tBot);
            glTexCoord2d(tTexRgt, tTexTop); glVertex2f(tRgt, tBot);
            glTexCoord2d(tTexRgt, cTexMidY); glVertex2f(tRgt, cMidY);
        glEnd();
        glEndList();
    }

    //--Reset texture flags back to their default.
    SugarBitmap::RestoreDefaultTextureFlags();
}

#endif
