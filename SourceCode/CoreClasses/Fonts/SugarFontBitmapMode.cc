//--Base
#include "SugarFont.h"

//--Classes
//--CoreClasses
#include "SugarBitmap.h"
#include "UString.h"

//--Definitions
#include "GlDfn.h"

//--Generics

//--GUI
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "DisplayManager.h"
#include "LuaManager.h"

//--SugarFont implementation of Bitmap-mode.  Does not matter what library is currently in use, the
//  bitmap implementation uses OpenGL.  This is a companion piece to the library renderers.
//--Bitmap-mode calls are considered private, because they are designed to be executed as complements
//  to the existing library implementations.

//============================================ System =============================================
void SugarFont::SetInternalBitmap(SugarBitmap *pBitmap)
{
    //--Sets to bitmap-mode and uses the provided bitmap. Ownership is assumed to be disabled.
    if(!pBitmap) return;

    //--Bitmap Mode
    mIsBitmapMode = true;
    mOwnsBitmapFont = false;
    mIsBitmapFlipped = true;
    rAlphabetBitmap = pBitmap;
    mIsReady = true;
}
void SugarFont::RunVariableWidthScript(const char *pScriptPath)
{
    //--Given a script, sets the SugarFont to variable-width mode. The provided script should set
    //  the positions of all the characters using their ASCII codes.
    //--Passing NULL will clear off the existing data and unset variable-width mode.
    mIsBitmapFixedWidth = false;
    if(!pScriptPath)
    {
        mIsBitmapFixedWidth = true;
        free(mBitmapCharacters);
        mBitmapCharacters = NULL;
        return;
    }

    //--If the bitmap characters were already allocated, no need to do so again.
    if(!mBitmapCharacters)
    {
        SetMemoryData(__FILE__, __LINE__);
        mBitmapCharacters = (BitmapFontCharacter *)starmemoryalloc(sizeof(BitmapFontCharacter) * 256);
    }

    //--Initialize.
    for(int i = 0; i < 256; i ++)
    {
        mBitmapCharacters[i].mBmpLft = 0.0f;
        mBitmapCharacters[i].mBmpTop = 0.0f;
        mBitmapCharacters[i].mBmpRgt = 0.00001f;
        mBitmapCharacters[i].mBmpBot = 0.00001f;
        mBitmapCharacters[i].mPixelWid = 0;
    }

    //--Run the script. This will fill in all the useful data.
    LuaManager::Fetch()->PushExecPop(this, pScriptPath);

    //--In Allegro mode, if flagged, we will modify the bitmap to give it edging and send it back to the hard drive.
    //fprintf(stderr, "Bitmap was %p\n", rAlphabetBitmap);
    if(!rAlphabetBitmap || true) return;
    #if defined _ALLEGRO_PROJECT_

    //--Setup.
    rAlphabetBitmap->Bind();
    float cWid = rAlphabetBitmap->GetWidth();
    float cHei = rAlphabetBitmap->GetHeight();

    //--Get the RGBA data.
    SetMemoryData(__FILE__, __LINE__);
    uint8_t *tBitmapData = (uint8_t *)starmemoryalloc(sizeof(uint8_t) * cWid * cHei * 4);
    glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_UNSIGNED_BYTE, tBitmapData);

    //--Go through all the characters and figure out the largest characters in both dimensions.
    int tBigX = 0;
    int tBigY = 0;
    for(int i = 0; i < 128; i ++)
    {
        int tSizeX = (mBitmapCharacters[i].mBmpRgt - mBitmapCharacters[i].mBmpLft) * cWid;
        int tSizeY = abs((mBitmapCharacters[i].mBmpBot - mBitmapCharacters[i].mBmpTop) * cHei);
        //fprintf(stderr, "%3i: %i %i - %i\n", i, tSizeX, tSizeY, mBitmapCharacters[i].mPixelWid);
        if(tBigX < tSizeX) tBigX = tSizeX;
        if(tBigY < tSizeY) tBigY = tSizeY;
    }

    //--Print.
    if(tBigX < 1 || tBigY < 1)
    {
        fprintf(stderr, "Error, %i %i\n", tBigX, tBigY);
        return;
    }

    //--Add 2 to each size. This allows a 1-pixel edge.
    tBigX += 2;
    tBigY += 2;

    //--Create a new bitmap for output to the hard drive.
    ALLEGRO_BITMAP *nOutBitmap = al_create_bitmap(tBigX * 128, tBigY);
    al_set_target_bitmap(nOutBitmap);
    al_clear_to_color(al_map_rgba(0, 0, 0, 0));

    //--For each letter, copy the pixel data to the bitmap and edge it.
    for(int i = 0; i < 128; i ++)
    {
        //--Skip unset letters.
        if(mBitmapCharacters[i].mPixelWid == 0) continue;
        //fprintf(stderr, "Letter %i %c\n", i, i);

        //--Top-left of source.
        float cSrcLft = mBitmapCharacters[i].mBmpLft * cWid;
        float cSrcTop = ceilf((1.0f - mBitmapCharacters[i].mBmpTop) * cHei);
        //fprintf(stderr, " %f %f\n", cSrcLft, cSrcTop);

        //--Top left of destination.
        float cDstLft = i * tBigX;
        float cDstTop = 1;

        //--Copy the data over. Indent by 1x1.
        int cLetterW = (mBitmapCharacters[i].mBmpRgt - mBitmapCharacters[i].mBmpLft) * cWid + 1;
        int cLetterH = (mBitmapCharacters[i].mBmpBot - mBitmapCharacters[i].mBmpTop) * cHei;
        if(cLetterH < 0) cLetterH = cLetterH * -1;
        //fprintf(stderr, " %i %i\n", cLetterW, cLetterH);
        //int cYOffset = tBigY - cLetterH;
        for(int x = 0; x < cLetterW; x ++)
        {
            for(int y = 0; y < cLetterH; y ++)
            {
                //--Compute the slot for the source.
                int tXSlot = cSrcLft + x;
                int tYSlot = (cSrcTop + y);
                int tSlot = ((tYSlot * cWid) + tXSlot) * 4;

                //--Get the pixel in question.
                al_put_pixel(cDstLft + x, cDstTop + y, al_map_rgba(tBitmapData[tSlot+0], tBitmapData[tSlot+1], tBitmapData[tSlot+2], tBitmapData[tSlot+3]));
                if(i == 'a' || true)
                {
                    //fprintf(stderr, "Src: %f %f\n", cSrcLft + x, cSrcTop + y);
                    //fprintf(stderr, "Dest: %f %f\n", cDstLft + x, cDstTop + tBigY - y - cYOffset);
                }
            }
        }

        //--With the data copied over, edge it.
        for(int x = 0; x < cLetterW; x ++)
        {
            for(int y = 0; y < cLetterH; y ++)
            {
                //--Slot.
                int tX = cDstLft + x;
                int tY = cDstTop + y;

                //--Get the pixel. If it has an alpha of nonzero, then the surrounding pixels get edged.
                ALLEGRO_COLOR tCheckPixel;
                ALLEGRO_COLOR tColor = al_get_pixel(nOutBitmap, tX, tY);
                if(tColor.r != 0)
                {
                    //--Check left pixel.
                    if(tX > 0)
                    {
                        tCheckPixel = al_get_pixel(nOutBitmap, tX-1, tY);
                        if(tCheckPixel.a == 0) al_put_pixel(tX-1, tY, al_map_rgba(0, 0, 0, 255));
                    }
                    //--Check top pixel.
                    if(tY > 0)
                    {
                        tCheckPixel = al_get_pixel(nOutBitmap, tX, tY-1);
                        if(tCheckPixel.a == 0) al_put_pixel(tX, tY - 1, al_map_rgba(0, 0, 0, 255));
                    }

                    //--Right pixel.
                    tCheckPixel = al_get_pixel(nOutBitmap, tX+1, tY);
                    if(tCheckPixel.a == 0) al_put_pixel(tX+1, tY, al_map_rgba(0, 0, 0, 255));

                    //--Bottom pixel.
                    tCheckPixel = al_get_pixel(nOutBitmap, tX, tY+1);
                    if(tCheckPixel.a == 0) al_put_pixel(tX, tY+1, al_map_rgba(0, 0, 0, 255));
                }
            }
        }
    }

    //--Write.
    al_save_bitmap("Data/OutputFont.png", nOutBitmap);
    //fprintf(stderr, "==> EDGED FONT EDGED FONT!!!!\n");

    //--Clean.
    al_destroy_bitmap(nOutBitmap);
    free(tBitmapData);
    #endif
}

//======================================= Property Queries ========================================
bool SugarFont::IsBitmapMode()
{
    return mIsBitmapMode;
}
int SugarFont::GetBitmapTextWidth(const char *pString)
{
    //--Returns how wide the string is.  In bitmap mode, it's the width of one character times the
    //  number of letters, since bitmaps are always fixed-width fonts.
    //--It is assumed this is called after the pString checks are already performed.
    if(!rAlphabetBitmap) return 1;

    //--If the bitmap is fixed-width, then the length is just a multiplication.
    if(mIsBitmapFixedWidth)
    {
        float tLWid = rAlphabetBitmap->GetWidth() / 128.0f;
        return strlen(pString) * (tLWid+1.0f);
    }
    //--Otherwise, we need to run through each character. Illegal characters have width 0.
    else if(mBitmapCharacters)
    {
        int tRunningWidth = 0;
        for(int i = 0; i < (int)strlen(pString); i ++)
        {
            tRunningWidth += mBitmapCharacters[(int)pString[i]].mPixelWid;
        }
        return tRunningWidth;
    }

    //--Error case: Bitmap is not fixed-width, but doesn't have character data.
    return 1;
}
int SugarFont::GetBitmapTextHeight()
{
    //--Returns the height of the 'A' character, which is considered to be the standard height.
    if(!rAlphabetBitmap || !mBitmapCharacters) return 1;

    return mBitmapCharacters['A'].mActualHei;
}

//========================================= Manipulators ==========================================
void SugarFont::SetBitmapCharacterSizes(int pIndex, int pLft, int pTop, int pRgt, int pBot, int pForceSize)
{
    //--Sets a given bitmap character. Note that we're using pixel sizes here, so we need to translate
    //  those into percentages.
    if(mIsBitmapFixedWidth || !mBitmapCharacters || pIndex < 0 || pIndex >= 256) return;

    //--Force-size needs no conversion factor.
    mBitmapCharacters[pIndex].mPixelWid = pForceSize;

    //--Make sure the alphabet bitmap exists. Translate the coordinates over.
    if(!rAlphabetBitmap) return;
    float tBaseWid = rAlphabetBitmap->GetWidth();
    float tBaseHei = rAlphabetBitmap->GetHeight();
    mBitmapCharacters[pIndex].mActualWid = pRgt - pLft;
    mBitmapCharacters[pIndex].mActualHei = pBot - pTop;
    mBitmapCharacters[pIndex].mBmpLft = (float)pLft / tBaseWid;
    mBitmapCharacters[pIndex].mBmpTop = (float)pTop / tBaseHei;
    mBitmapCharacters[pIndex].mBmpRgt = (float)pRgt / tBaseWid;
    mBitmapCharacters[pIndex].mBmpBot = (float)pBot / tBaseHei;

    //--If the bitmap is flipped (because it was loaded from the DataLibrary) then set this to 1.0f
    //  to flip the coordinates.
    if(mIsBitmapFlipped)
    {
        mBitmapCharacters[pIndex].mBmpTop = 1.0f - mBitmapCharacters[pIndex].mBmpTop;
        mBitmapCharacters[pIndex].mBmpBot = 1.0f - mBitmapCharacters[pIndex].mBmpBot;
    }
}

//========================================= Core Methods ==========================================
//============================================ Update =============================================
//=========================================== File I/O ============================================
//=========================================== Drawing =============================================
void SugarFont::DrawTextBitmap(const char *pString)
{
    DrawTextBitmap(0.0f, 0.0f, 0, 1.0f, pString);
}
void SugarFont::DrawTextBitmap(float pX, float pY, const char *pString)
{
    DrawTextBitmap(pX, pY, 0, 1.0f, pString);
}
void SugarFont::DrawTextBitmap(float pX, float pY, int pFlags, const char *pString)
{
    DrawTextBitmap(pX, pY, pFlags, 1.0f, pString);
}
void SugarFont::DrawTextBitmap(float pX, float pY, int pFlags, float pScale, const char *pString)
{
    //--Check for actual bitmap!
    if(!rAlphabetBitmap || !pString || pScale == 0.0f) return;

    //--Fixed-width bitmaps are easier to render. Just render each letter at fixed spacing.
    if(mIsBitmapFixedWidth)
    {
        //--Compute X position. Changes when auto-centering.
        float tXPosition = pX;
        if(pFlags & SUGARFONT_AUTOCENTER_X)
            tXPosition = tXPosition - (GetTextWidth(pString) * 0.5f * pScale);
        else if(pFlags & SUGARFONT_RIGHTALIGN_X)
            tXPosition = tXPosition - (GetTextWidth(pString) * pScale);

        //--Compute the Y position. Changes when auto-centering.
        float tYPosition = pY;
        if(pFlags & SUGARFONT_AUTOCENTER_Y)
            tYPosition = tYPosition - (GetTextHeight() * 0.5f * pScale);

        //--Setup.
        glEnable(GL_TEXTURE_2D);
        rAlphabetBitmap->Bind();

        //--Temporary Variables
        float tLft, tRgt;

        //--The alphabet must be 128 characters wide.  Determine letter width.
        float tTrueWid = rAlphabetBitmap->GetWidth();
        float tLWid = rAlphabetBitmap->GetWidth() / 128.0f;
        float tLHei = rAlphabetBitmap->GetHeight();

        //--Positioning.
        float tXScale = pScale;
        float tYScale = pScale;
        if(!xScaleAffectsX) tXScale = 1.0f;
        if(!xScaleAffectsY) tYScale = 1.0f;
        glTranslatef(tXPosition, tYPosition, 0.0f);
        if(pScale != 1.0f)
        {
            glScalef(tXScale, tYScale, 1.0f);
        }

        //--For each letter...
        int tLen = strlen(pString);
        for(int i = 0; i < tLen; i ++)
        {
            //--Get the character.
            char tLetter = pString[i];

            //--Move over to render it.
            glTranslatef(tLWid + 1.0f, 0.0f, 0.0f);

            //--Texture points.
            tLft = (tLetter * tLWid) / tTrueWid;
            tRgt = (tLWid / tTrueWid) + tLft;

            //--Render it.
            glBegin(GL_QUADS);
                glTexCoord2f(tLft, 1); glVertex2f(0.0f,  tLHei);
                glTexCoord2f(tLft, 0); glVertex2f(0.0f,  0.0f);
                glTexCoord2f(tRgt, 0); glVertex2f(tLWid, 0.0f);
                glTexCoord2f(tRgt, 1); glVertex2f(tLWid, tLHei);
            glEnd();
        }

        //--Clean
        glTranslatef((tLWid + 1.0f) * tLen * -1.0f, 0.0f, 0.0f);
        if(pScale != 1.0f) glScalef(1.0f / tXScale, 1.0f / tYScale, 1.0f);
        glTranslatef(tXPosition * -1.0f, tYPosition * -1.0f, 0.0f);
    }
    //--Non-fixed bitmaps need to render with more specialized translation code.
    else
    {
        //--Compute X position. Changes when auto-centering.
        float tXOffset = pX;
        if(pFlags & SUGARFONT_AUTOCENTER_X)
            tXOffset = tXOffset - (GetTextWidth(pString) * 0.5f * pScale);
        else if(pFlags & SUGARFONT_RIGHTALIGN_X)
            tXOffset = tXOffset - (GetTextWidth(pString) * pScale);

        //--Compute the Y position. Changes when auto-centering.
        float tYOffset = pY;
        if(pFlags & SUGARFONT_AUTOCENTER_Y)
            tYOffset = tYOffset - (GetTextHeight() * 0.5f * pScale);

        //--Position.
        glTranslatef(tXOffset, tYOffset, 0.0f);
        float tXScale = pScale;
        float tYScale = pScale;
        if(!xScaleAffectsX) tXScale = 1.0f;
        if(!xScaleAffectsY) tYScale = 1.0f;
        if(pScale != 1.0f)
        {
            glScalef(tXScale, tYScale, 1.0f);
        }

        //--Setup.
        float tXPosition = 0.0f;
        rAlphabetBitmap->Bind();

        //--Begin rendering.
        glBegin(GL_QUADS);

        //--For each letter...
        int tLen = strlen(pString);
        for(int i = 0; i < tLen; i ++)
        {
            //--Get the character.
            int tLetter = pString[i];
            if(tLetter < 0) continue;

            //--Render.
            glTexCoord2f(mBitmapCharacters[tLetter].mBmpLft, mBitmapCharacters[tLetter].mBmpBot); glVertex2f(tXPosition,                                       0.0f+mBitmapCharacters[tLetter].mActualHei);
            glTexCoord2f(mBitmapCharacters[tLetter].mBmpLft, mBitmapCharacters[tLetter].mBmpTop); glVertex2f(tXPosition,                                       0.0f);
            glTexCoord2f(mBitmapCharacters[tLetter].mBmpRgt, mBitmapCharacters[tLetter].mBmpTop); glVertex2f(tXPosition+mBitmapCharacters[tLetter].mActualWid, 0.0f);
            glTexCoord2f(mBitmapCharacters[tLetter].mBmpRgt, mBitmapCharacters[tLetter].mBmpBot); glVertex2f(tXPosition+mBitmapCharacters[tLetter].mActualWid, 0.0f+mBitmapCharacters[tLetter].mActualHei);

            //--Next.
            tXPosition += mBitmapCharacters[tLetter].mPixelWid;
        }

        //--Clean up.
        glEnd();
        if(pScale != 1.0f) glScalef(1.0f / tXScale, 1.0f / tYScale, 1.0f);
        glTranslatef(tXOffset * -1.0f, -tYOffset, 0.0f);
    }
}

//======================================= Pointer Routing =========================================
//====================================== Static Functions =========================================
