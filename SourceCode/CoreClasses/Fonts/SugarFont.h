//--[SugarFont]
//--API-independent font wrapper, for use in hazardous environments involving strings that must
//  appear on the screen. Supports UTF-8, assuming the API does anyway.
//--When using TTF styles, the font pre-caches all the bitmap data and uses atlasing to speed
//  rendering up. This takes time at boot but massively improves rendering speeds. Precaching
//  only uses the 256 ASCII letters, ignoring the others.
//--The Bitmap version uses Freetype by default. Other APIs could be supported in the future.
//  Allegro support has been dropped.

#pragma once

//--Defines what type of fonts the project is using.
#define _FONTTYPE_FREETYPE_

//#define _SYSTEMFONT_TTF_
#define _SYSTEMFONT_BITMAP_

#include "Definitions.h"
#include "Structures.h"
#include "RootObject.h"

//--[Local typedefs]
#if defined _FONTTYPE_FREETYPE_
    #include <ft2build.h>
    #include FT_FREETYPE_H
    typedef FT_Face SugarInternalFont;
#endif

//--[Local Definitions]
#define SUGARFONT_NOEFFECTS 0x0000
#define SUGARFONT_BOLD 0x0001
#define SUGARFONT_ITALIC 0x0002
#define SUGARFONT_UNDERLINE 0x0004
#define SUGARFONT_AUTOCENTER_X 0x0008
#define SUGARFONT_AUTOCENTER_Y 0x0010
#define SUGARFONT_AUTOCENTER_XY (SUGARFONT_AUTOCENTER_X | SUGARFONT_AUTOCENTER_Y)
#define SUGARFONT_RIGHTALIGN_X 0x0020
#define SUGARFONT_MIRRORX 0x0040
#define SUGARFONT_MIRRORY 0x0080
#define SUGARFONT_DOUBLERENDERX 0x0100
#define SUGARFONT_DOUBLERENDERY 0x0200

#define SUGARFONT_PRECACHE_WITH_NEAREST 0x01
#define SUGARFONT_PRECACHE_WITH_EDGE 0x02
#define SUGARFONT_PRECACHE_WITH_DOWNFADE 0x04
#define SUGARFONT_PRECACHE_WITH_SPECIAL_S 0x08

#define SUGARFONT_DEFAULTSIZE 60.0f

#define SUGARFONT_PRECACHE_LETTERS 128

//--[Local Structures]
//--Used when precaching the data for a set of letters.
typedef struct
{
    //--OpenGL Rendering
    uint32_t mHandle;
    uint32_t mGLList;
    uint32_t mGLListItalic;
    uint32_t mGLListXMirror;
    uint32_t mGLListYMirror;

    //--Offset on bitmap
    float mLftOffset, mTopOffset;
    float mFlippedTopOffset;

    //--Fast-access copies of metrics
    float mWidth, mHeight;
    float mAdvanceX;
    float mHoriBearingX;
    float mHoriBearingY;

    //--Kernings
    float mKerningDistance[SUGARFONT_PRECACHE_LETTERS];

}SugarFontPrecacheData;

//--When in Bitmap Font mode, stores the texel positions of a character on a texture.
typedef struct
{
    float mBmpLft, mBmpTop, mBmpRgt, mBmpBot;
    int mActualWid, mActualHei;
    int mPixelWid;
}BitmapFontCharacter;

//--[Class]
class SugarFont : public RootObject
{
    private:
    //--System
    bool mIsReady;
    bool mOwnsFont;

    //--Font storage
    #if defined _FONTTYPE_FREETYPE_
    SugarInternalFont rFont;
    #endif

    //--Font precaching
    float mEndPadding;
    uint32_t mPrecachedHandle;
    float mTallestCharacter;
    SugarFontPrecacheData *mPrecacheData;
    float mPrecachedSizeX, mPrecachedSizeY;

    //--Precache Special Effects
    static float xDownfadeBegin;//SugarFontSpecialEffects.cc
    static float xDownfadeEnd;//SugarFontSpecialEffects.cc
    static int xBlackOutlineCutoff;//SugarFontSpecialEffects.cc
    static int xOutlineSize;//SugarFontSpecialEffects.cc

    //--Offsets
    float mInternalSize;

    //--Kerning
    float mKerningScaler;

    //--Bitmap Mode
    bool mOwnsBitmapFont;
    bool mIsBitmapMode;
    bool mIsBitmapFixedWidth;
    bool mIsBitmapFlipped;
    SugarBitmap *rAlphabetBitmap;
    BitmapFontCharacter *mBitmapCharacters;

    protected:

    public:
    //--System
    SugarFont();
    virtual ~SugarFont();
    void ConstructWith(const char *pLoadPath, int pFontSize, uint8_t pFlags);
    static void DeleteThis(void *pPtr);

    //--Public Variables
    static bool xScaleAffectsX;
    static bool xScaleAffectsY;
    #if defined _FONTTYPE_FREETYPE_
    static FT_Library xFTLibrary;
    #endif

    //--Property Queries
    bool IsReady();
    int GetTextWidth(const char *pString);
    int GetTextHeight();
    float GetKerningBetween(int pLetterLft, int pLetterRgt);

    //--Manipulators
    void Bind();
    void SetKerningScaler(float pAmount);
    void SetKerning(char pLft, char pRgt, float pAmount);

    //--Core Methods
    void PushPropertyStack();
    void PopPropertyStack();

    //--Precaching
    void ClearPrecache();
    void PrecacheData(uint8_t pFilterFlag);

    //--Special Effects
    static void SetDownfade(float pStart, float pEnd);
    static void SetOutlineWidth(int pPixels);
    static void ApplyBlackOutline(int pXSize, int pYSize, uint32_t *pTextureData);

    //--Bitmap Mode
    //private:
    void SetInternalBitmap(SugarBitmap *pBitmap);
    void RunVariableWidthScript(const char *pScriptPath);
    bool IsBitmapMode();
    int GetBitmapTextWidth(const char *pString);
    int GetBitmapTextHeight();
    void SetBitmapCharacterSizes(int pIndex, int pLft, int pTop, int pRgt, int pBot, int pForceSize);
    void DrawTextBitmap(const char *pString);
    void DrawTextBitmap(float pX, float pY, const char *pString);
    void DrawTextBitmap(float pX, float pY, int pFlags, const char *pString);
    void DrawTextBitmap(float pX, float pY, int pFlags, float pSize, const char *pString);

    public:
    //--Update
    //--File I/O
    //--Drawing
    void DrawText(const char *pText);
    void DrawText(float pX, float pY, const char *pText);
    void DrawText(float pX, float pY, int pFlags, const char *pText);
    void DrawTextColor(float pX, float pY, int pFlags, float pSize, bool pUseHighlight, StarlightColor pHighlightColor, StarlightColor pBaseColor, const char *pText);
    void DrawText(float pX, float pY, int pFlags, float pSize, const char *pText);
    void DrawTextArgs(float pX, float pY, int pFlags, float pSize, const char *pText, ...);
    void DrawTextColorArgs(float pX, float pY, int pFlags, float pSize, bool pUseHighlight, StarlightColor pHighlightColor, StarlightColor pBaseColor, const char *pText, ...);

    //--Single-Letter. Used for Major Dialogue cases.
    float DrawLetter(float pX, float pY, int pFlags, float pSize, char pLetter);
    float DrawLetter(float pX, float pY, int pFlags, float pSize, char pLetter, char pNextLetter);

    //--Drawing Text with Image Remaps
    void DrawTextImg(float pX, float pY, int pFlags, float pSize, const char *pText, int pRemapsTotal, char **pRemaps, char **pImgPaths);

    //--Pointer Routing
    //--Static Functions
    static SugarFont *FetchSystemFont();

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_SugarFont_SetBitmapCharacter(lua_State *pLuaState);
int Hook_SugarFont_SetKerning(lua_State *pLuaState);
