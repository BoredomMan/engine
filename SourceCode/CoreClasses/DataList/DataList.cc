//--Base
#include "DataList.h"

//--Classes
//--Definitions
//--Generics
#include "DeletionFunctions.h"
#include "SugarLinkedList.h"

//--GUI
//--Libraries
#include "DataLibrary.h"

//--Managers

//=========================================== System ==============================================
DataList::DataList()
{
    //--System
    mStrictFlag = false;
    mErrorFlag = DATALIST_ERROR_NONE;

    //--Storage
    mDataList = new SugarLinkedList(true);
}
DataList::~DataList()
{
    delete mDataList;
    mDataList = NULL;
}

//====================================== Property Queries =========================================
int DataList::GetDataErrorCode()
{
    return mErrorFlag;
}
int DataList::GetDataListSize()
{
    return mDataList->GetListSize();
}
void *DataList::FetchDataEntry(const char *pName)
{
    return mDataList->GetElementByName(pName);
}

//========================================= Manipulators ==========================================
void DataList::SetStrictFlag(bool pFlag)
{
    mStrictFlag = pFlag;
}
void DataList::ResetErrorCode()
{
    mErrorFlag = DATALIST_ERROR_NONE;
}
void DataList::DefineEntry(const char *pName, void *pPtr, DeletionFunctionPtr pDeletionPtr)
{
    //--Allows the DataList to add new entries if the mStrictFlag is on, by flipping it off first!
    //  Since this can only be done in DefineEntry, this is effectively an initializer.
    bool tOldFlag = mStrictFlag;
    mStrictFlag = false;
    AddDataEntry(pName, pPtr, pDeletionPtr);
    mStrictFlag = tOldFlag;
}
void DataList::AddDataEntry(const char *pName, void *pPtr, DeletionFunctionPtr pDeletionPtr)
{
    //--Prevent duplicates from appearing in the list, unless the name is "X", which is a special
    //  name indicating duplication is okay (since there will not be name fetching of that entry
    //  anyway).
    if(!pName || !pPtr) return;

    bool tFoundOnListAlready = false;
    if(strcmp(pName, "X") && strncmp(pName, "X|", 2))
    {
        int mSlot = mDataList->GetSlotOfElementByName(pName);
        while(mSlot != -1)
        {
            mDataList->RemoveElementI(mSlot);
            mSlot = mDataList->GetSlotOfElementByName(pName);
            tFoundOnListAlready = true;
        }
    }

    //--The strictness flag will block this if the entry was not found and deleted beforehand.
    if(mStrictFlag && !tFoundOnListAlready)
    {
        fprintf(stderr, "DataList::AddDataEntry:  Failed, strict flag is on and entry %s does not exist\n", pName);
        return;
    }
    mDataList->AddElement(pName, pPtr, pDeletionPtr);
}
void DataList::AddDataEntry(const char *pName, void *pPtr)
{
    AddDataEntry(pName, pPtr, &FreeThis);
}
void DataList::AddDataEntry(const char *pName, void *pPtr, bool pBlocker)
{
    AddDataEntry(pName, pPtr, &DontDeleteThis);
}
void DataList::AddDataEntryN(const char *pName, float pNumber)
{
    //--Macro to quickly add an entry as a floating point.
    if(!pName) return;
    SetMemoryData(__FILE__, __LINE__);
    float *nNumPtr = (float *)starmemoryalloc(sizeof(float));
    *nNumPtr = pNumber;
    AddDataEntry(pName, nNumPtr, &FreeThis);
}
void DataList::AddDataEntryS(const char *pName, const char *pString)
{
    //--Macro to quickly add an entry as a string.
    if(!pName || !pString) return;
    SetMemoryData(__FILE__, __LINE__);
    char *nString = (char *)starmemoryalloc(sizeof(char) * (strlen(pString) + 1));
    strcpy(nString, pString);
    AddDataEntry(pName, nString, &FreeThis);
}

//========================================= Core Methods ==========================================
void DataList::ClearDataList()
{
    mDataList->ClearList();
}

//============================================ Update =============================================
//=========================================== File I/O ============================================
//=========================================== Drawing =============================================
//======================================= Pointer Routing =========================================
//====================================== Static Functions =========================================
//========================================= Lua Hooking ===========================================
void DataList::HookToLuaState(lua_State *pLuaState)
{
    /* DataList_Define(sName[], "N", fNumber)
       DataList_Define(sName[], "S", sString[])
       Defines the data in advance of setting it.  Is otherwise identical to SetData, however, if
       the strict flag is on, then Define can create new entries while SetData cannot. */
    lua_register(pLuaState, "DataList_Define", &Hook_DataList_Define);

    /* DataList_SetData(sName[], "N", fNumber)
       DataList_SetData(sName[], "S", sString[])
       Pushes data onto the data list with the type specified.  Do NOT mix types or undefined
       behavior will result.  If an entry is not found, it is created.  It may be prudent, when
       naming your variables, to prefix them with their type ala Lua (iValue, fValue, sString) */
    lua_register(pLuaState, "DataList_SetData", &Hook_DataList_SetData);

    /* DataList_GetData(sName[], "N")
       DataList_GetData(sName[], "S")
       Returns data from the data list with the type specified.  Again, do NOT mix types!
       If an entry is not found, a default value of 0 is returned. */
    lua_register(pLuaState, "DataList_GetData", &Hook_DataList_GetData);
}

//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
int Hook_DataList_Define(lua_State *L)
{
    //DataList_Define(sName, "N", fNumber)
    //DataList_Define(sName, "S", sString)
    if(lua_gettop(L) != 3) return fprintf(stderr, "DataList_Define:  Failed, invalid argument list\n");

    DataList *rList = (DataList *)DataLibrary::Fetch()->rActiveObject;
    if(!rList) return fprintf(stderr, "DataList_Define:  Failed, no active object\n");

    //--Third arg is nil.
    if(lua_isnil(L, 3) == 1)
    {
        fprintf(stderr, "DataList_Define: %s %s - Failed, third argument was nil.\n", lua_tostring(L, 1), lua_tostring(L, 2));
        return 0;
    }

    const char *rTypeString = lua_tostring(L, 2);
    if(!strcmp(rTypeString, "N"))
    {
        SetMemoryData(__FILE__, __LINE__);
        float *nNumPtr = (float *)starmemoryalloc(sizeof(float));
        *nNumPtr = lua_tonumber(L, 3);

        rList->DefineEntry(lua_tostring(L, 1), nNumPtr, &FreeThis);
    }
    else if(!strcmp(rTypeString, "S"))
    {
        const char *rAddString = lua_tostring(L, 3);
        SetMemoryData(__FILE__, __LINE__);
        char *nString = (char *)starmemoryalloc(sizeof(char) * (strlen(rAddString)+ 1));
        strcpy(nString, rAddString);
        rList->DefineEntry(lua_tostring(L, 1), nString, &FreeThis);
    }
    else
    {
        fprintf(stderr, "DataList_Define:  Failed, cannot resolve type %s\n", rTypeString);
    }

    return 0;
}
int Hook_DataList_SetData(lua_State *L)
{
    //DataList_SetData(Name[], "N", Number)
    //DataList_SetData(Name[], "S", String[])
    if(lua_gettop(L) != 3) return fprintf(stderr, "DataList_SetData:  Failed, invalid argument list\n");

    DataList *rList = (DataList *)DataLibrary::Fetch()->rActiveObject;
    if(!rList) return fprintf(stderr, "DataList_SetData:  Failed, no active object\n");

    //--Third arg is nil.
    if(lua_isnil(L, 3) == 1)
    {
        fprintf(stderr, "DataList_SetData: %s %s - Failed, third argument was nil.\n", lua_tostring(L, 1), lua_tostring(L, 2));
        return 0;
    }

    const char *rTypeString = lua_tostring(L, 2);
    if(!strcmp(rTypeString, "N"))
    {
        SetMemoryData(__FILE__, __LINE__);
        float *nNumPtr = (float *)starmemoryalloc(sizeof(float));
        *nNumPtr = lua_tonumber(L, 3);

        rList->AddDataEntry(lua_tostring(L, 1), nNumPtr, &FreeThis);
    }
    else if(!strcmp(rTypeString, "S"))
    {
        const char *rAddString = lua_tostring(L, 3);
        SetMemoryData(__FILE__, __LINE__);
        char *nString = (char *)starmemoryalloc(sizeof(char) * (strlen(rAddString)+ 1));
        strcpy(nString, rAddString);
        rList->AddDataEntry(lua_tostring(L, 1), nString, &FreeThis);
    }
    else
    {
        fprintf(stderr, "DataList_SetData:  Failed, cannot resolve type %s\n", rTypeString);
    }

    return 0;
}
int Hook_DataList_GetData(lua_State *L)
{
    //--Fetches information off the DataList.  Type is the second string.
    //DataList_GetData(Name[], "N")
    //DataList_GetData(Name[], "S")
    if(lua_gettop(L) != 2)
    {
        fprintf(stderr, "DataList_GetData:  Failed, invalid argument list\n");
        lua_pushnumber(L, 0);
        return 1;
    }

    DataList *rList = (DataList *)DataLibrary::Fetch()->rActiveObject;
    if(!rList)
    {
        fprintf(stderr, "DataList_GetData:  Failed, no active object\n");
        lua_pushnumber(L, 0);
        return 1;
    }

    void *rCalledPtr = rList->FetchDataEntry(lua_tostring(L, 1));
    if(!rCalledPtr)
    {
        //fprintf(stderr, "DataList_GetData:  Failed, entry %s not found\n", lua_tostring(L, 1));
        lua_pushnumber(L, 0);
        return 1;
    }

    const char *rTypeString = lua_tostring(L, 2);
    if(!strcmp(rTypeString, "N"))
    {
        float *rFloatPtr = (float *)rCalledPtr;
        lua_pushnumber(L, *rFloatPtr);
    }
    else if(!strcmp(rTypeString, "S"))
    {
        char *rStringPtr = (char *)rCalledPtr;
        lua_pushstring(L, rStringPtr);
    }
    else
    {
        fprintf(stderr, "DataList_GetData:  Failed, cannot resolve type %s\n", rTypeString);
        lua_pushnumber(L, 0);
    }

    return 1;
}
