//--[SugarMenu]
//--A menu class. The class is abstract in that it doesn't actually render anything directly,
//  you should derive it to perform rendering. It does provide zone-abstraction for buttons
//  if necessary, but the rendering will need to make sure the buttons are aligned correctly.
//--Note: The menu inherits from the RootObject so it can be pushed for typing. It does not
//  inherit from the Renderables, since it doesn't render directly. The derived copy should
//  inherit from IRenderable if needed.

#pragma once

//--[Includes]
#include "Definitions.h"
#include "Structures.h"
#include "RootObject.h"

//--[Local Structures]
//--[Local Definitions]
//--[Classes]
class SugarMenu : public RootObject
{
    protected:
    //--System
    bool mIsPendingClose;

    //--Buttons
    SugarLinkedList *mButtonList;

    protected:

    public:
    //--System
    SugarMenu();
    virtual ~SugarMenu();

    //--Public Variables
    //--Property Queries
    bool HasPendingClose();

    //--Manipulators
    virtual void Clear();
    void SetClosingFlag(bool pFlag);
    virtual void RegisterButton(SugarButton *pButton);

    //--Core Methods
    private:
    //--Private Core Methods
    public:
    //--Update
    //--File I/O
    //--Drawing
    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions

