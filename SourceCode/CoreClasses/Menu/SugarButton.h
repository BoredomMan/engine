//--[SugarButton]
//--A virtualized button, used by the SugarMenu. The button has several optional characteristics,
//  such as rendering behaviors and physical locations for clicking. Derived copies should be used
//  for extensions, and should be used in derived SugarMenu objects.
//--Note: The base copy doesn't have a direct rendering function, but it has everything you'd need.
//  There is a flag to switch rendering on or off, but derived classes would need to be coded to
//  respect that flag.
//--SugarButtons are *not* part of the RootObject inheritance tree!

#pragma once

//--[Includes]
#include "Definitions.h"
#include "Structures.h"
#include "INameable.h"

//--[Local Structures]
typedef struct VirtualButtonExecutionPack
{
    //--System
    int mPriority;

    //--Lua Variables
    bool mIsLua;
    char *mLuaExec;
    float mFiringCode;

    //--Function Pointer Variables
    bool mIsFunctionPointer;
    LogicFnPtr rFunctionPtr;

    //--Functions
    void Initialize()
    {
        mPriority = 0;
        mIsLua = false;
        mLuaExec = NULL;
        mFiringCode = 0.0f;
        mIsFunctionPointer = false;
        rFunctionPtr = NULL;
    }
    static void DeleteThis(void *pPtr)
    {
        VirtualButtonExecutionPack *rPack = (VirtualButtonExecutionPack *)pPtr;
        free(rPack->mLuaExec);
        free(rPack);
    }
}VirtualButtonExecutionPack;

//--[Local Definitions]
//--[Classes]
class SugarButton : public INameable
{
    protected:
    //--System
    char *mLocalName;
    int mLocalPriority;

    //--Location
    TwoDimensionReal mCoordinates;

    //--Execution
    SugarLinkedList *mExecutionList;

    protected:

    public:
    //--System
    SugarButton();
    virtual ~SugarButton();
    static void DeleteThis(void *pPtr);

    //--Public Variables
    //--Property Queries
    virtual char *GetName();
    int GetPriority();

    //--Manipulators
    virtual void SetName(const char *pName);
    void SetPriority(int pPriority);
    void SetPosition(float pLft, float pTop);
    void SetDimensions(float pLft, float pTop, float pRgt, float pBot);
    void SetDimensionsWH(float pLft, float pTop, float pWid, float pHei);
    void RegisterExecPack(VirtualButtonExecutionPack *pPack);
    void RegisterLuaExecPack(int pPriority, const char *pScriptPath);
    void RegisterLuaExecPack(int pPriority, const char *pScriptPath, float pFiringCode);

    //--Core Methods
    void SortPackages();
    virtual void Execute();
    void ExecutePackage(VirtualButtonExecutionPack *pPack);

    private:
    //--Private Core Methods
    static int CompareButtonPacks(const void *pEntryA, const void *pEntryB);

    public:
    //--Update
    virtual void Update(int pMouseX, int pMouseY);

    //--File I/O
    //--Drawing
    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_SugarButton_SetProperty(lua_State *L);

