//--Base
#include "SugarCamera3D.h"

//--Classes
//--CoreClasses
//--Definitions
#include "3DStructures.h"
#include "DebugDefinitions.h"
#include "Global.h"

//--GUI
//--Libraries
//--Managers
#include "ControlManager.h"
#include "DebugManager.h"
#include "MapManager.h"

void SugarCamera3D::Update()
{
    //--If the camera is currently accepting controls, this is where they are handled.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Toggles the noclip flag. Can be toggled even if not holding down the unlock key.
    if(rControlManager->GetControlState("ToggleNoclip")->mIsFirstPress)
    {
        mNoClip = !mNoClip;
        #if defined DSLOT_NOCLIP
            if(mNoClip)
            {
                ResetString(Global::Shared()->gDebugStrings[DSLOT_NOCLIP], "Camera Noclip");
            }
            else
            {
                ResetString(Global::Shared()->gDebugStrings[DSLOT_NOCLIP], "Camera Clipped");
            }
        #endif
    }

    //--Camera speed handler.
    if(rControlManager->GetControlState("CameraSpeedUp")->mIsFirstPress)
    {
        mMoveSpeed = mMoveSpeed + 1.0f;
        if(mMoveSpeed >= mMoveSpeedMax) mMoveSpeed = mMoveSpeedMax;
        #if defined DSLOT_CAMSPEED
            char tBuffer[128];
            sprintf(tBuffer, "Camera Speed: %0.2f", mMoveSpeed);
            ResetString(Global::Shared()->gDebugStrings[DSLOT_CAMSPEED], tBuffer);
        #endif
    }
    if(rControlManager->GetControlState("CameraSpeedDn")->mIsFirstPress)
    {
        mMoveSpeed = mMoveSpeed - 1.0f;
        if(mMoveSpeed <= mMoveSpeedMin) mMoveSpeed = mMoveSpeedMin;
        #if defined DSLOT_CAMSPEED
            char tBuffer[128];
            sprintf(tBuffer, "Camera Speed: %0.2f", mMoveSpeed);
            ResetString(Global::Shared()->gDebugStrings[DSLOT_CAMSPEED], tBuffer);
        #endif
    }

    //--Right now, this is locked out if the player is not holding down the "Camera Unlock" button.
    if(!rControlManager->GetControlState("CameraUnlock")->mIsDown)
    {
        //--Send debug information to the screen.
        #ifdef DEBUG_LINE_CAMERA_XYZ
        if(DEBUG_LINE_CAMERA_XYZ >= 0 && DEBUG_LINE_CAMERA_XYZ < DEBUG_LINES_TOTAL)
        {
            char tBuffer[256];
            sprintf(tBuffer, "%5.1f %5.1f %5.1f", mCoordinates.mXCenter, mCoordinates.mYCenter, mCoordinates.mZCenter);
            ResetString(Global::Shared()->gDebugPrintStrings[DEBUG_LINE_CAMERA_XYZ], tBuffer);
        }
        #endif
        #ifdef DEBUG_LINE_CAMERA_PYR
        if(DEBUG_LINE_CAMERA_PYR >= 0 && DEBUG_LINE_CAMERA_PYR < DEBUG_LINES_TOTAL)
        {
            char tBuffer[256];
            sprintf(tBuffer, "%5.1f %5.1f %5.1f", mXRotation, mYRotation, mZRotation);
            ResetString(Global::Shared()->gDebugPrintStrings[DEBUG_LINE_CAMERA_PYR], tBuffer);
        }
        #endif
        return;
    }

    //--On the first press, zero off the coordinate deltas.
    if(rControlManager->GetControlState("CameraUnlock")->mIsFirstPress)
    {
        mOldMouseX = -100.0f;
    }

    //--Setup
    ControlState *rControlState = NULL;
    int tNewMouseX, tNewMouseY, tNewMouseZ;
    rControlManager->GetMouseCoords(tNewMouseX, tNewMouseY, tNewMouseZ);
    //fprintf(stderr, "Mouse coords %i %i\n", tNewMouseX, tNewMouseY);

    //--Mouse movement.
    float mXMove = 0;
    float mYMove = 0;
    //float mZMove = 0;
    if(mOldMouseX != -100.0f)
    {
        mXMove = tNewMouseX - mOldMouseX;
        mYMove = tNewMouseY - mOldMouseY;
        //mZMove = tNewMouseZ - mOldMouseZ;
    }
    mOldMouseX = tNewMouseX;
    mOldMouseY = tNewMouseY;

    //--Clamp.
    if(mXMove > 1000.0f)
    {
        fprintf(stderr, "Xmove was way too high %f.\n", mXMove);
        mXMove = 0.0f;
    }
    else if(mXMove < -1000.0f)
    {
        fprintf(stderr, "Xmove was way too low %f.\n", mXMove);
        mXMove = 0.0f;
    }

    //--Setup for collisions.
    float tNewX = mCoordinates.mXCenter;
    float tNewY = mCoordinates.mYCenter;
    float tNewZ = mCoordinates.mZCenter;

    //--Temporarily set the movement speed to 10.0f. This is just to track how far we are from a
    //  wall, not to actually move that fast.
    float tRealMoveSpeed = mMoveSpeed;

    //--Switch based on Camera Mode.  Free Cameras can move in any direction  with WSAD and look
    //  in any direction with the mouse.
    mMoveStore = 0.0f;
    if(true)
    {
        //--Rotation Handling
        mZRotation = mZRotation - (mXMove * mMouseSensitivity);
        mXRotation = mXRotation - (mYMove * mMouseSensitivity);

        //--Clamp rotations. It's really weird to look up and keep going.
        if(mXRotation <=   0) mXRotation =   0;
        if(mXRotation >= 180) mXRotation = 180;

        //--Forward
        rControlState = rControlManager->GetControlState("Up");
        if(rControlState->mIsDown)
        {
            //--Rotate around Y-axis:
            float tz1 = -cos(mYRotation * TORADIAN);
            float tz2 =  sin(mXRotation * TORADIAN);

            //Rotate around X-axis:
            tNewX = tNewX + (tz2 * sin(mZRotation * TORADIAN) * mMoveSpeed);
            tNewY = tNewY + (tz2 * cos(mZRotation * TORADIAN) * mMoveSpeed);
            tNewZ = tNewZ - (tz1 * cos(mXRotation * TORADIAN) * mMoveSpeed);
        }

        rControlState = rControlManager->GetControlState("Down");
        if(rControlState->mIsDown)
        {
            //--Rotate around Y-axis:
            float tz1 = -sin((mYRotation + 90.0) * TORADIAN);
            float tz2 =  sin(mXRotation * TORADIAN);

            //--Rotate around X-axis:
            tNewX = tNewX - (tz2 * sin(mZRotation * TORADIAN) * mMoveSpeed);
            tNewY = tNewY - (tz2 * cos(mZRotation * TORADIAN) * mMoveSpeed);
            tNewZ = tNewZ + (tz1 * cos(mXRotation * TORADIAN) * mMoveSpeed);
        }

        rControlState = rControlManager->GetControlState("Left");
        if(rControlState->mIsDown)
        {
            //Rotate around X-axis:
            mZRotation = mZRotation + 90;
            tNewX = tNewX + (sin(mZRotation * TORADIAN) * mMoveSpeed);
            tNewY = tNewY + (cos(mZRotation * TORADIAN) * mMoveSpeed);
            mZRotation = mZRotation - 90;
        }

        rControlState = rControlManager->GetControlState("Right");
        if(rControlState->mIsDown)
        {
            //--Rotate around X-axis:
            mZRotation = mZRotation + 90;
            tNewX = tNewX - (sin(mZRotation * TORADIAN) * mMoveSpeed);
            tNewY = tNewY - (cos(mZRotation * TORADIAN) * mMoveSpeed);
            mZRotation = mZRotation - 90;
        }
    }
    //--Chase Cameras have positions that are fixed relative to the Player, but can be zoomed
    //  in and out.
    /*
    else if(false)
    {
        //--Zoom handling.
        mScaler = mScaler - (mZMove * mScrollSensitivity);
        if(mScaler < 0.5f) mScaler = 0.5f;
        if(mScaler > 1.2f) mScaler = 1.2f;

        //--Left and right movement, attached to the mouse.  If the key is not
        //  pressed, just ignore all this.
        static float xLRSensitivity = 0.45f;
        mChaseAngle = mChaseAngle + (mXMove * xLRSensitivity);
        while(mChaseAngle <   0.0f) mChaseAngle = mChaseAngle + 360.0f;
        while(mChaseAngle > 360.0f) mChaseAngle = mChaseAngle - 360.0f;
        return;
    }*/

    //--Dif setup.
    float tXDif = tNewX - mCoordinates.mXCenter;
    float tYDif = tNewY - mCoordinates.mYCenter;
    float tZDif = tNewZ - mCoordinates.mZCenter;
    AttemptMove(false, tXDif, tYDif, tZDif);

    //--Reset the move speed.
    mMoveSpeed = tRealMoveSpeed;

    //--Clamping.
    while(mZRotation <   0.0f) mZRotation = mZRotation + 360.0f;
    while(mZRotation > 360.0f) mZRotation = mZRotation - 360.0f;

    //--[Debug]
    //--Send debug information to the screen.
    #ifdef DEBUG_LINE_CAMERA_XYZ
    if(DEBUG_LINE_CAMERA_XYZ >= 0 && DEBUG_LINE_CAMERA_XYZ < DEBUG_LINES_TOTAL)
    {
        char tBuffer[256];
        sprintf(tBuffer, "%0.1f %0.1f %0.1f", mCoordinates.mXCenter, mCoordinates.mYCenter, mCoordinates.mZCenter);
        ResetString(Global::Shared()->gDebugPrintStrings[DEBUG_LINE_CAMERA_XYZ], tBuffer);
    }
    #endif
    #ifdef DEBUG_LINE_CAMERA_PYR
    if(DEBUG_LINE_CAMERA_PYR >= 0 && DEBUG_LINE_CAMERA_PYR < DEBUG_LINES_TOTAL)
    {
        char tBuffer[256];
        sprintf(tBuffer, "%0.1f %0.1f %0.1f", mXRotation, mYRotation, mZRotation);
        ResetString(Global::Shared()->gDebugPrintStrings[DEBUG_LINE_CAMERA_PYR], tBuffer);
    }
    #endif
}
void SugarCamera3D::AttemptMove(bool pAllowSlide, float pXSpeed, float pYSpeed, float pZSpeed)
{
    //--Attempts to move the camera in 3D space by the provided speeds. If the BSP file reports
    //  that the move is not allowed, then the camera cannot move.
    //--If pAllowSlide is true, then the camera may attempt a second move to 'slide' along the
    //  edge. This will not be true twice, that is, the Camera may not attempt to slide multiple
    //  times along multiple edges.
    if(pXSpeed == 0.0f && pYSpeed == 0.0f && pZSpeed == 0.0f) return;

    //--Collision ignore flag. The Camera always completes its move when collisions are off.
    if(mNoClip || true)
    {
        mCoordinates.mXCenter = mCoordinates.mXCenter + pXSpeed;
        mCoordinates.mYCenter = mCoordinates.mYCenter + pYSpeed;
        mCoordinates.mZCenter = mCoordinates.mZCenter + pZSpeed;
        return;
    }
}
