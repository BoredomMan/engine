//--Base
#include "SugarCamera2D.h"

//--Classes
//--Definitions
//--Generics
//--GUI
//--Libraries
//--Managers
#include "CameraManager.h"
#include "DebugManager.h"

//========================================= Lua Hooking ===========================================
void SugarCamera2D::HookToLuaState(lua_State *pLuaState)
{
    /* Camera_GetProperty("Dimensions") (4 returns)
       Returns the specified property from the current active camera. */
    lua_register(pLuaState, "Camera_GetProperty", &Hook_Camera_GetProperty);

    /* Camera_SetProperty("Boundaries", iLft, iTop, iRgt, iBot)
       Sets the total boundaries around the whole level, which the Camera will
       never exceed.

       Camera_SetProperty("CurrentPos", fLft, fTop)
       Sets the current location of the camera's top left, ignore all other factors.

       Camera_SetProperty("PositiveActivity", bFlag)
       Sets whether or not the Camera is using positive activity to restrict
       2D movements.  If set to true but no zones are defined, the Camera will
       not move.

       Camera_SetProperty("ZonesTotal", iNewTotal)
       Sets how many camera zones are available, and clears any old data.

       Camera_SetProperty("ZoneData", iSlot, iLft, iTop, iRgt, iBot)
       Sets the position of a camera zone.

       Camera_SetProperty("ZoneActivity", iSlot, bFlag)
       Enables or Disables the specified zone.

       Camera_SetProperty("ScriptControl", sScriptPath[])
       Enables or Disables script control of the camera.  The provided path
       will take over the Camera's update pipeline.  Pass "NULL" to clear. */
    lua_register(pLuaState, "Camera_SetProperty", &Hook_Camera_SetProperty);

    /* Camera_CallFunc("CenterOnTarget", fXTarget, fYTarget)
       Calls the internal function to center on the specified target.

       Camera_CallFunc("CenterOnPlayer")
       Calls the internal function to automatically center on the Player.

       Camera_CallFunc("CenterOnBox", iLft, iTop, iRgt, iBot)
       Camera_CallFunc("CenterOnBox", iLft, iTop, iRgt, iBot, bLockScale, bIgnoreX, bIgnoreY)
       Calls the internal function to center the camera such that it fits the
       provided "box".  If bLockScale is true, the camera will not resize
       itself to fit the box.  If either of the Ignore flags are true, the
       Camera will not scroll in that dimension. */
    lua_register(pLuaState, "Camera_CallFunc", &Hook_Camera_CallFunc);

    /* Camera_SetLockingState(bState)
       Forces the camera to lock or unlock its pipeline.  Be DAMN sure you reset this to true after
       you call the needed functions! */
    lua_register(pLuaState, "Camera_SetLockingState", &Hook_Camera_SetLockingState);
}

//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
int Hook_Camera_GetProperty(lua_State *L)
{
    //Camera_GetProperty("Dimensions") (4 returns)
    uint32_t tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("Camera_GetProperty", L);

    //--Get the Camera. The DataLibrary does not handle the stack, the CameraManager does.
    //  It is also therefore impossible to have the wrong type or a NULL.
    int tReturns = 0;
    SugarCamera2D *rActiveCamera = CameraManager::FetchActiveCamera2D();
    const char *rTypeString = lua_tostring(L, 1);

    //--Dimensions, always in order of lft/top/rgt/bot
    if(!strcmp(rTypeString, "Dimensions"))
    {
        ThreeDimensionReal tDim = rActiveCamera->GetDimensions();
        lua_pushnumber(L, tDim.mLft);
        lua_pushnumber(L, tDim.mTop);
        lua_pushnumber(L, tDim.mRgt);
        lua_pushnumber(L, tDim.mBot);
        tReturns = 4;
    }
    //--Error.
    else
    {
        DebugManager::ForcePrint("Camera_GetProperty: Error, could not resolve type %s\n", rTypeString);
    }

    return tReturns;
}
int Hook_Camera_SetProperty(lua_State *L)
{
    //Camera_SetProperty("Boundaries", iLft, iTop, iRgt, iBot)
    //Camera_SetProperty("CurrentPos", fLft, fTop)
    //Camera_SetProperty("PositiveActivity", bFlag)
    //Camera_SetProperty("ZonesTotal", iNewTotal)
    //Camera_SetProperty("ZoneData", iSlot, iLft, iTop, iRgt, iBot)
    //Camera_SetProperty("ZoneActivity", iSlot, bFlag)
    //Camera_SetProperty("NewPusher", sName[], iIdealLft, iIdealTop, iLft, iTop, iWid, iHei)
    //Camera_SetProperty("PolygonMode", bFlag)
    //Camera_SetProperty("Polygon", iSides, ...)
    //Camera_SetProperty("Rectangle", iLft, iTop, iRgt, iBot)
    //Camera_SetProperty("RectangleWH", iLft, iTop, iWid, iHei)
    //Camera_SetProperty("ScriptControl", sScriptPath[])
    uint32_t tArgs = lua_gettop(L);
    if(tArgs < 1) return fprintf(stderr, "Camera_SetProperty:  Failed, invalid argument list\n");

    //--Get the Camera.  Note that it's whichever Camera the CameraManager holds
    //  as active, not the DataLibrary's.
    SugarCamera2D *rActiveCamera = CameraManager::FetchActiveCamera2D();
    const char *rTypeString = lua_tostring(L, 1);

    //--Current Position (overrides bounds)
    if(!strcmp(rTypeString, "CurrentPos") && tArgs == 3)
    {
        rActiveCamera->OverridePosition(lua_tonumber(L, 2), lua_tonumber(L, 3));
    }
    //--Positive activity flag
    else if(!strcmp(rTypeString, "PositiveActivity") && tArgs == 2)
    {
        rActiveCamera->SetZoneMode(lua_toboolean(L, 2));
    }
    //--Setting positive zones
    else if(!strcmp(rTypeString, "ZonesTotal") && tArgs == 2)
    {
        rActiveCamera->AllocZones(lua_tointeger(L, 2));
    }
    //--Polygon mode
    else if(!strcmp(rTypeString, "PolygonMode") && tArgs == 2)
    {
        rActiveCamera->SetPolygonMode(lua_toboolean(L, 2));
    }
    //--The points of a polygon
    else if(!strcmp(rTypeString, "Polygon") && tArgs >= 2)
    {
        //--How many points?
        int tPoints = lua_tointeger(L, 2);
        if(tPoints < 3)
        {
            fprintf(stderr, "Camera_SetProperty:  Polygon must have at least 3 sides.\n");
            return 0;
        }

        //--Make sure enough args were passed
        if(tPoints * 2 + 2 > (int)tArgs)
        {
            fprintf(stderr, "Camera_SetProperty:  Not enough arguments passed for %i points.\n", tPoints);
            return 0;
        }

        //--Allocate array.
        SetMemoryData(__FILE__, __LINE__);
        float *nArray = (float *)starmemoryalloc(sizeof(float) * ((tPoints * 2) + 1));
        nArray[0] = tPoints;

        //--Fill
        for(int i = 0; i < tPoints; i ++)
        {
            nArray[(i*2) + 1] = lua_tonumber(L, (i * 2) + 3);
            nArray[(i*2) + 2] = lua_tonumber(L, (i * 2) + 4);
        }

        //--Give it to the camera
        rActiveCamera->AddPolygon(nArray);
    }
    //--Adding a polygon which is known to be a rectangle.
    else if(!strcmp(rTypeString, "Rectangle") && tArgs == 5)
    {
        //--Allocate
        SetMemoryData(__FILE__, __LINE__);
        float *nArray = (float *)starmemoryalloc(sizeof(float) * (9));
        nArray[0] = 4.0f;

        //--Temps
        float tLft = lua_tonumber(L, 2);
        float tTop = lua_tonumber(L, 3);
        float tRgt = tLft + lua_tonumber(L, 4);
        float tBot = tTop + lua_tonumber(L, 5);

        //--Fill
        nArray[1] = tLft;
        nArray[2] = tTop;

        nArray[3] = tRgt;
        nArray[4] = tTop;

        nArray[5] = tRgt;
        nArray[6] = tBot;

        nArray[7] = tLft;
        nArray[8] = tBot;

        //--Give it to the camera
        rActiveCamera->AddPolygon(nArray);
    }
    //--Adding a polygon which is known to be a rectangle, using wid/hei.
    else if(!strcmp(rTypeString, "RectangleWH") && tArgs == 5)
    {
        //--Allocate
        SetMemoryData(__FILE__, __LINE__);
        float *nArray = (float *)starmemoryalloc(sizeof(float) * (9));
        nArray[0] = 4.0f;

        //--Temps
        float tLft = lua_tonumber(L, 2);
        float tTop = lua_tonumber(L, 3);
        float tRgt = tLft + lua_tonumber(L, 4);
        float tBot = tTop + lua_tonumber(L, 5);

        //--Fill
        nArray[1] = tLft;
        nArray[2] = tTop;

        nArray[3] = tRgt;
        nArray[4] = tTop;

        nArray[5] = tRgt;
        nArray[6] = tBot;

        nArray[7] = tLft;
        nArray[8] = tBot;

        //--Give it to the camera
        rActiveCamera->AddPolygon(nArray);
    }
    //Camera_SetProperty("Rectangle", iLft, iTop, iRgt, iBot)
    //--Adding a pusher.
    else if(!strcmp(rTypeString, "NewPusher") && tArgs == 8)
    {
        rActiveCamera->AddPusher(lua_tostring(L, 2), lua_tointeger(L, 3), lua_tointeger(L, 4), lua_tointeger(L, 5), lua_tointeger(L, 6), lua_tointeger(L, 7), lua_tointeger(L, 8));
    }
    //Camera_SetProperty("NewPusher", iIdealLft, iIdealTop, iLft, iTop, iWid, iHei)
    //--Setting the coordinates of a positive zone.
    else if(!strcmp(rTypeString, "ZoneData") && tArgs == 6)
    {
        rActiveCamera->SetZone(lua_tointeger(L, 2), lua_tointeger(L, 3), lua_tointeger(L, 4), lua_tointeger(L, 5), lua_tointeger(L, 6));
    }
    //--Setting the activity of a positive zone.
    else if(!strcmp(rTypeString, "ZoneActivity") && tArgs == 3)
    {
        rActiveCamera->SetZoneActivity(lua_tointeger(L, 2), lua_toboolean(L, 3));
    }
    else
    {
        fprintf(stderr, "Camera_SetProperty:  Failed to resolve %s with %i args\n", rTypeString, tArgs);
    }

    return 0;
}
int Hook_Camera_CallFunc(lua_State *L)
{
    //Camera_CallFunc("CenterOnTarget", iXTarget, iYTarget)
    //Camera_CallFunc("CenterOnPlayer")
    //Camera_CallFunc("CenterOnBox", iLft, iTop, iRgt, iBot)
    //Camera_CallFunc("CenterOnBox", iLft, iTop, iRgt, iBot, bLockScale, bIgnoreX, bIgnoreY)
    uint32_t tArgs = lua_gettop(L);
    if(tArgs < 1) return fprintf(stderr, "Camera_CallFunc:  Failed, invalid argument list\n");

    //--Get the Camera.  Note that it's whichever Camera the CameraManager holds
    //  as active, not the DataLibrary's.
    SugarCamera2D *rActiveCamera = CameraManager::FetchActiveCamera2D();
    const char *rTypeString = lua_tostring(L, 1);

    //--Calling the focus point function
    if(!strcmp(rTypeString, "CenterOnTarget") && tArgs == 3)
    {
        rActiveCamera->CenterOnPosition(lua_tointeger(L, 2), lua_tointeger(L, 3));
    }
    //--Calling the focus player function
    else if(!strcmp(rTypeString, "CenterOnPlayer") && tArgs == 1)
    {
        rActiveCamera->CenterOnPlayer();
    }
    //--Calling the focus box function
    else if(!strcmp(rTypeString, "CenterOnBox") && tArgs == 5)
    {
        rActiveCamera->CenterOnBox(lua_tointeger(L, 2), lua_tointeger(L, 3), lua_tointeger(L, 4), lua_tointeger(L, 5));
    }
    //--Calling the focus box function, with options
    else if(!strcmp(rTypeString, "CenterOnBox") && tArgs == 8)
    {
        rActiveCamera->CenterOnBox(lua_tointeger(L, 2), lua_tointeger(L, 3), lua_tointeger(L, 4), lua_tointeger(L, 5), lua_toboolean(L, 6), lua_toboolean(L, 7), lua_toboolean(L, 8));
    }
    else
    {
        fprintf(stderr, "Camera_CallFunc:  Failed to resolve %s with %i args\n", rTypeString, tArgs);
    }

    return 0;
}
int Hook_Camera_SetLockingState(lua_State *L)
{
    //Camera_SetLockingState(bState)
    uint32_t tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("Camera_SetLockingState");

    //--Get the Camera.  Note that it's whichever Camera the CameraManager holds
    //  as active, not the DataLibrary's.
    SugarCamera2D *rActiveCamera = CameraManager::FetchActiveCamera2D();
    rActiveCamera->SetLockingState(lua_toboolean(L, 1));
    return 0;
}
