//--Base
#include "LoadInterrupt.h"

//--Classes
//--CoreClasses
#include "SugarBitmap.h"
#include "SugarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "Global.h"

//--GUI
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "DebugManager.h"
#include "DisplayManager.h"
#include "SugarLumpManager.h"

//=========================================== System ==============================================
LoadInterrupt::LoadInterrupt()
{
    //--[LoadInterrupt]
    //--System
    mIsInitted = false;
    mIsAwaitingKeypress = false;

    //--Loading Bar
    mCurrent = 0;
    mCurrentMax = 1;

    //--Animation Data
    mFrame = 0;
    mFrameDenominator = 4;

    //--Graphics
    memset(&Images, 0, sizeof(Images));

    //--String Tyrant Mode
    mStringTyrantImages = NULL;

    //--Timing
    mForceNextUpdate = true;
    mLastUpdateCall = 0.0f;

    //--Public Variables
    mIsSuppressed = false;
}
LoadInterrupt::~LoadInterrupt()
{
    delete mStringTyrantImages;
}

//====================================== Property Queries =========================================
int LoadInterrupt::GetCallsSoFar()
{
    return mCurrent;
}
bool LoadInterrupt::IsAwaitingKeypress()
{
    return mIsAwaitingKeypress;
}

//========================================= Manipulators ==========================================
void LoadInterrupt::Init()
{
    //--Inits the object after the constructor. This requires Allegro/SDL to have been initialized.
    //  Because we cannot safely assume that any of the managers exist (because they might be loading)
    //  we use an internal copy of the SugarLumpManager.
    SugarLumpManager *tSLM = new SugarLumpManager();
    tSLM->Open("Data/Loading.slf");

    //--Suppress the load interrupt.
    bool tOldFlag = SugarBitmap::xSuppressInterrupt;
    SugarBitmap::xSuppressInterrupt = true;

    //--Running frames.
    Images.Data.mChristineFrames = tSLM->GetImage("ChristineRun");
    Images.Data.mRaijuFrames     = tSLM->GetImage("RaijuRun");

    //--Loading Text
    Images.Data.mAnyKeyOver   = tSLM->GetImage("AnyKeyOver");
    Images.Data.mAnyKeyUnder  = tSLM->GetImage("AnyKeyUnder");
    Images.Data.mLoadingOver  = tSLM->GetImage("LoadingOver");
    Images.Data.mLoadingUnder = tSLM->GetImage("LoadingUnder");

    //--Make sure everything resolved.
    Images.mIsReady = VerifyStructure(&Images.Data, sizeof(Images.Data), sizeof(void *));

    //--External setting. If anything went wrong, these won't get set, and nothing renders.
    SugarBitmap::xInterruptCall = &LoadInterrupt::Interrupt;

    //--Clean up.
    delete tSLM;
    SugarBitmap::xSuppressInterrupt = tOldFlag;
}
void LoadInterrupt::InitStringTyrant(const char *pDatafilePath)
{
    //--Inits the object into String Tyrant mode, which uses an image sequence.
    delete mStringTyrantImages;
    mStringTyrantImages = NULL;

    //--If "Null" is passed, or NULL, switch to normal interrupt mode.
    if(!pDatafilePath || !strcasecmp(pDatafilePath, "Null"))
    {
        SugarBitmap::xInterruptCall = &LoadInterrupt::Interrupt;
        if(!Images.mIsReady) SugarBitmap::xInterruptCall = NULL;
        return;
    }

    //--Make sure the file exists.
    SugarLumpManager *tSLM = new SugarLumpManager();
    tSLM->Open(pDatafilePath);
    if(!tSLM->IsFileOpen()) return;

    //--Suppress the load interrupt.
    bool tOldFlag = SugarBitmap::xSuppressInterrupt;
    SugarBitmap::xSuppressInterrupt = true;

    //--Load the frames into our local list.
    char tBuffer[128];
    mStringTyrantImages = new SugarLinkedList(true);
    for(int i = 0; i < MARY_FRAMES; i ++)
    {
        sprintf(tBuffer, "Img%03i", i);
        SugarBitmap *nFrame = tSLM->GetImage(tBuffer);
        mStringTyrantImages->AddElementAsTail("X", nFrame, &SugarBitmap::DeleteThis);
    }

    //--Set to the String Tyrant variant. If anything went wrong, the default one will be used.
    SugarBitmap::xInterruptCall = &LoadInterrupt::InterruptStringTyrant;

    //--Set this flag.
    mIsAwaitingKeypress = false;

    //--Clean up.
    delete tSLM;
    SugarBitmap::xSuppressInterrupt = tOldFlag;
}
void LoadInterrupt::SetKeypressFlag(bool pFlag)
{
    mIsAwaitingKeypress = pFlag;
}
void LoadInterrupt::Interrupt()
{
    //--Static Interrupt call, fetch the object and perform the Interrupt call.
    LoadInterrupt *rInterrupt = LoadInterrupt::Fetch();
    if(!rInterrupt) return;
    rInterrupt->Call(false);
}
void LoadInterrupt::InterruptStringTyrant()
{
    //--As above, but for the String Tyrant version.
    LoadInterrupt *rInterrupt = LoadInterrupt::Fetch();
    if(!rInterrupt) return;
    rInterrupt->CallStringTyrant(false);
}
void LoadInterrupt::WaitForKeypress()
{
    LoadInterrupt *rInterrupt = LoadInterrupt::Fetch();
    if(!rInterrupt) return;
    rInterrupt->SetKeypressFlag(true);
}

//========================================= Core Methods ==========================================
void LoadInterrupt::Reset(int pMaxCount)
{
    //--Resets the timers as required, meaning the logic will always run once the first time this
    //  call hits. Note that this does not re-parse the text file or reload any images.
    mForceNextUpdate = true;

    //--Reset variables.
    mFrame = 0;
    mCurrent = 0;
    mCurrentMax = pMaxCount;

    //--Clamp. mCurrentMax can never be less than one.
    if(mCurrentMax < 1) mCurrentMax = 1;

    //--Store the current time for the loading animations.
    mLastUpdateCall = GetGameTime();
}
void LoadInterrupt::Finish()
{
    //--Prints some debug data, if you need it.
    if(false) return;

    //--Call one last time.
    if(!mStringTyrantImages)
    {
        Call(true);
    }
    else
    {
        CallStringTyrant(true);
    }
    mCurrent --;

    //--Debug prints.
    //fprintf(stderr, "[Load Interrupt]\n");
    //fprintf(stderr, " Completed loading action!\n");
    //fprintf(stderr, " Counter was %i/%i\n", mCurrent, mCurrentMax);
}

//===================================== Private Core Methods ======================================
//============================================ Update =============================================
//=========================================== File I/O ============================================
//=========================================== Drawing =============================================
void LoadInterrupt::Call(bool pForceRender)
{
    //--Performs the actual logic of the interrupt sequence. This counts as a drawing action since
    //  the screen is drawn to IF the timers request it.
    DebugManager::PushPrint(false, "[LoadInterrupt] Begin\n");
    if(!Images.mIsReady || mIsSuppressed || SugarBitmap::xSuppressInterrupt)
    {
        DebugManager::PopPrint("[LoadInterrupt] Suppressed\n");
        return;
    }

    //--Increment the counter. This tracks how many items we've loaded and is used for the progress
    //  bar. The denominator mCurrentMax is a constant.
    mCurrent ++;

    //--Setup.
    float tCurrentTime = GetGameTime();

    //--Get how long it's been since last we rendered anything. This clamps the LoadInterrupt at
    //  60 fps (1.0 / 60.0 == 0.01667)
    //--pForceRender will bypass this check.
    //fprintf(stderr, "Time since last tick = %f\n", tCurrentTime - mLastUpdateCall);
    if(tCurrentTime - mLastUpdateCall < 0.01667f && !pForceRender)
    {
        DebugManager::PopPrint("[LoadInterrupt] Too Soon\n");
        return;
    }

    //--Store the time, increment the frame count.
    mLastUpdateCall = tCurrentTime;
    mFrame ++;

    //--Resolve how long the loading bar ought to be. Clipped 0 to 1.
    float tBarPercent = (float)mCurrent / ((float)(mCurrentMax) * 0.95f);
    if(tBarPercent > 1.0f) tBarPercent = 1.0f;
    if(tBarPercent < 0.0f) tBarPercent = 0.0f;

    //--We have determined the screen needs an update. We cannot assume the DisplayManager is
    //  available, so we need to run these GL commands the old fashioned way.
    //--Coordinates used in this section are hard-coded screen coordinates.
    DisplayManager::LetterboxOffsets();
    DisplayManager::StdOrtho();
    glDrawBuffer(GL_BACK);

        //--Clean the screen to dark grey.
        glClearDepth(1.0f);
        glClearColor(0.05f, 0.05f, 0.05f, 1.0f);
        glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

        //--[Animation Constants]
        //--These are the same between all animations.
        float cRenderScale = 2.0f;
        float cWidPerFrame = 100.0f;
        float cHeiPerFrame = 100.0f;

        //--[Christine's Animation]
        //--Compute the frame.
        int tChristineFrame = (mFrame / 1) % 40;

        //--Sizes.
        float cChristineTrueWid = Images.Data.mChristineFrames->GetTrueWidth();
        //float cChristineTrueHei = Images.Data.mChristineFrames->GetTrueHeight();

        //--Constants.
        float cLft = ((1366.0f - 150.0f) * 0.50f) - (cWidPerFrame * 0.50f * cRenderScale);
        float cTop = 200.0f;
        float cRgt = cLft + (cWidPerFrame * cRenderScale);
        float cBot = cTop + (cHeiPerFrame * cRenderScale);
        float cTxL = (tChristineFrame + 0) * (cWidPerFrame / cChristineTrueWid);
        float cTxT = 1.0f;
        float cTxR = (tChristineFrame + 1) * (cWidPerFrame / cChristineTrueWid);
        float cTxB = 0.0f;

        //--Setup.
        Images.Data.mChristineFrames->Bind();

        //--Render.
        glBegin(GL_QUADS);
            glTexCoord2f(cTxL, cTxT); glVertex2f(cLft, cTop);
            glTexCoord2f(cTxR, cTxT); glVertex2f(cRgt, cTop);
            glTexCoord2f(cTxR, cTxB); glVertex2f(cRgt, cBot);
            glTexCoord2f(cTxL, cTxB); glVertex2f(cLft, cBot);
        glEnd();

        //--[Raiju's Animation]
        //--Compute the frame.
        int tRaijuFrame = ((mFrame+0) / mFrameDenominator) % RUN_FRAMES;

        //--Sizes.
        float cRaijuTrueWid = Images.Data.mRaijuFrames->GetTrueWidth();
        //float cRaijuTrueHei = Images.Data.mRaijuFrames->GetTrueHeight();

        //--Constants.
        cLft = ((1366.0f + 150.0f) * 0.50f) - (cWidPerFrame * 0.50f * cRenderScale);
        cTop = 200.0f;
        cRgt = cLft + (cWidPerFrame * cRenderScale);
        cBot = cTop + (cHeiPerFrame * cRenderScale);
        cTxL = (tRaijuFrame + 0) * (cWidPerFrame / cRaijuTrueWid);
        cTxT = 1.0f;
        cTxR = (tRaijuFrame + 1) * (cWidPerFrame / cRaijuTrueWid);
        cTxB = 0.0f;

        //--Setup.
        Images.Data.mRaijuFrames->Bind();

        //--Render.
        glBegin(GL_QUADS);
            glTexCoord2f(cTxL, cTxT); glVertex2f(cLft, cTop);
            glTexCoord2f(cTxR, cTxT); glVertex2f(cRgt, cTop);
            glTexCoord2f(cTxR, cTxB); glVertex2f(cRgt, cBot);
            glTexCoord2f(cTxL, cTxB); glVertex2f(cLft, cBot);
        glEnd();

        //--[Loading Text]
        //--Loading text. Underlay always renders the same color.
        cLft = ((1366.0f) * 0.50f) - (Images.Data.mLoadingUnder->GetTrueWidth() * 0.50f);
        StarlightColor::SetMixer(0.0f, 0.0f, 0.0f, 1.0f);
        Images.Data.mLoadingUnder->Draw(cLft, 400);

        //--Overlay, loading percentage half.
        float cLoadWid = Images.Data.mLoadingOver->GetTrueWidth();
        float cLoadHei = Images.Data.mLoadingOver->GetTrueHeight();
        Images.Data.mLoadingOver->Bind();

        //--Compute left-side constants.
        cLft = ((1366.0f) * 0.50f) - (cLoadWid * 0.50f);
        cTop = 400.0f;
        cRgt = cLft + (cLoadWid * tBarPercent);
        cBot = cTop + (cLoadHei * 1.0f);
        cTxL = 0.0f;
        cTxT = 1.0f;
        cTxR = tBarPercent;
        cTxB = 0.0f;

        //--Render.
        StarlightColor::SetMixer(0.7f, 0.0f, 0.0f, 1.0f);
        glBegin(GL_QUADS);
            glTexCoord2f(cTxL, cTxT); glVertex2f(cLft, cTop);
            glTexCoord2f(cTxR, cTxT); glVertex2f(cRgt, cTop);
            glTexCoord2f(cTxR, cTxB); glVertex2f(cRgt, cBot);
            glTexCoord2f(cTxL, cTxB); glVertex2f(cLft, cBot);
        glEnd();
        StarlightColor::ClearMixer();

        //--Right-side constants.
        cLft = ((1366.0f) * 0.50f) - (cLoadWid * 0.50f) + (cLoadWid * tBarPercent);
        cRgt = ((1366.0f) * 0.50f) - (cLoadWid * 0.50f) + (cLoadWid * 1.0f);
        cTxL = tBarPercent;
        cTxR = 1.0f;

        //--Render.
        glBegin(GL_QUADS);
            glTexCoord2f(cTxL, cTxT); glVertex2f(cLft, cTop);
            glTexCoord2f(cTxR, cTxT); glVertex2f(cRgt, cTop);
            glTexCoord2f(cTxR, cTxB); glVertex2f(cRgt, cBot);
            glTexCoord2f(cTxL, cTxB); glVertex2f(cLft, cBot);
        glEnd();

    DisplayManager::CleanOrtho();

    //--[Allegro Version]
    //--Data is sent to the graphics card by flipping the display.
    #if defined _ALLEGRO_PROJECT_
        al_flip_display();

    //--[SDL Version]
    //--Data is sent to the graphics card by order the context to dump to the window.
    #elif defined _SDL_PROJECT_
        SDL_GL_SwapWindow(DisplayManager::Fetch()->GetWindow());

    #endif
    DebugManager::PopPrint("[LoadInterrupt] Finished normally\n");
}
void LoadInterrupt::CallStringTyrant(bool pForceRender)
{
    //--As above, but for the String Tyrant loading sequence.
    DebugManager::PushPrint(false, "[LoadInterruptST] Begin\n");
    if(!Images.mIsReady || mIsSuppressed || !mStringTyrantImages || SugarBitmap::xSuppressInterrupt)
    {
        DebugManager::PopPrint("[LoadInterruptST] Suppressed\n");
        return;
    }

    //--Increment the counter. This tracks how many items we've loaded and is used for the progress
    //  bar. The denominator mCurrentMax is a constant.
    mCurrent ++;

    //--Setup.
    float tCurrentTime = GetGameTime();

    //--Get how long it's been since last we rendered anything. This clamps the LoadInterrupt at
    //  60 fps (1.0 / 60.0 == 0.01667)
    //--pForceRender will bypass this check.
    //fprintf(stderr, "Time since last tick = %f\n", tCurrentTime - mLastUpdateCall);
    if(tCurrentTime - mLastUpdateCall < 0.01667f && !pForceRender)
    {
        DebugManager::PopPrint("[LoadInterruptST] Too Soon\n");
        return;
    }

    //--Store the time, increment the frame count.
    int cFrameDenom = 2;
    int cFrameMax = (((float)mCurrent / (float)mCurrentMax) * MARY_FRAMES) * cFrameDenom;
    mLastUpdateCall = tCurrentTime;
    mFrame ++;
    if(mFrame >= cFrameMax) mFrame = cFrameMax;

    //--Determine the rendering frame.
    int cRenderFrame = mFrame / cFrameDenom;
    if(cRenderFrame >= MARY_FRAMES)
    {
        cRenderFrame = MARY_FRAMES - 1;
        if(mIsAwaitingKeypress) mIsAwaitingKeypress = false;
    }

    //--Resolve how long the loading bar ought to be. Clipped 0 to 1.
    float tBarPercent = (float)mCurrent / ((float)(mCurrentMax) * 0.95f);
    if(tBarPercent > 1.0f) tBarPercent = 1.0f;
    if(tBarPercent < 0.0f) tBarPercent = 0.0f;

    //--Which text to use.
    SugarBitmap *rUseOver  = Images.Data.mLoadingOver;
    SugarBitmap *rUseUnder = Images.Data.mLoadingUnder;
    if(mIsAwaitingKeypress)
    {
        rUseOver  = Images.Data.mAnyKeyOver;
        rUseUnder = Images.Data.mAnyKeyUnder;
    }

    //--We have determined the screen needs an update. We cannot assume the DisplayManager is
    //  available, so we need to run these GL commands the old fashioned way.
    //--Coordinates used in this section are hard-coded screen coordinates.
    DisplayManager::LetterboxOffsets();
    DisplayManager::StdOrtho();
    glDrawBuffer(GL_BACK);

        //--[Base]
        //--Clean the screen to black.
        glClearDepth(1.0f);
        glClearColor(0.00f, 0.00f, 0.00f, 1.0f);
        glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

        //--[Mary's Frame]
        //--Compute the frame.
        SugarBitmap *rImage = (SugarBitmap *)mStringTyrantImages->GetElementBySlot(cRenderFrame);

        //--Setup.
        if(rImage) rImage->Draw();

        //--[Loading Text]
        //--Loading text. Underlay always renders the same color.
        float cLft = ((1366.0f) * 0.50f) - (rUseUnder->GetTrueWidth() * 0.50f);
        StarlightColor::SetMixer(0.0f, 0.0f, 0.0f, 1.0f);
        rUseUnder->Draw(cLft, 500);

        //--Overlay, loading percentage half.
        float cLoadWid = rUseOver->GetTrueWidth();
        float cLoadHei = rUseOver->GetTrueHeight();
        rUseOver->Bind();

        //--Compute left-side constants.
        cLft = ((1366.0f) * 0.50f) - (cLoadWid * 0.50f);
        float cTop = 500.0f;
        float cRgt = cLft + (cLoadWid * tBarPercent);
        float cBot = cTop + (cLoadHei * 1.0f);
        float cTxL = 0.0f;
        float cTxT = 1.0f;
        float cTxR = tBarPercent;
        float cTxB = 0.0f;

        //--Render.
        StarlightColor::SetMixer(0.7f, 0.0f, 0.0f, 1.0f);
        glBegin(GL_QUADS);
            glTexCoord2f(cTxL, cTxT); glVertex2f(cLft, cTop);
            glTexCoord2f(cTxR, cTxT); glVertex2f(cRgt, cTop);
            glTexCoord2f(cTxR, cTxB); glVertex2f(cRgt, cBot);
            glTexCoord2f(cTxL, cTxB); glVertex2f(cLft, cBot);
        glEnd();
        StarlightColor::ClearMixer();

        //--Right-side constants.
        cLft = ((1366.0f) * 0.50f) - (cLoadWid * 0.50f) + (cLoadWid * tBarPercent);
        cRgt = ((1366.0f) * 0.50f) - (cLoadWid * 0.50f) + (cLoadWid * 1.0f);
        cTxL = tBarPercent;
        cTxR = 1.0f;

        //--Render.
        glBegin(GL_QUADS);
            glTexCoord2f(cTxL, cTxT); glVertex2f(cLft, cTop);
            glTexCoord2f(cTxR, cTxT); glVertex2f(cRgt, cTop);
            glTexCoord2f(cTxR, cTxB); glVertex2f(cRgt, cBot);
            glTexCoord2f(cTxL, cTxB); glVertex2f(cLft, cBot);
        glEnd();

    DisplayManager::CleanOrtho();

    //--[Allegro Version]
    //--Data is sent to the graphics card by flipping the display.
    #if defined _ALLEGRO_PROJECT_
        al_flip_display();

    //--[SDL Version]
    //--Data is sent to the graphics card by order the context to dump to the window.
    #elif defined _SDL_PROJECT_
        SDL_GL_SwapWindow(DisplayManager::Fetch()->GetWindow());

    #endif
    DebugManager::PopPrint("[LoadInterruptST] Finished normally\n");
}

//======================================= Pointer Routing =========================================
//====================================== Static Functions =========================================
LoadInterrupt *LoadInterrupt::Fetch()
{
    if(!Global::Shared()->gLoadInterrupt) return NULL;
    return Global::Shared()->gLoadInterrupt;
}

//========================================= Lua Hooking ===========================================
void LoadInterrupt::HookToLuaState(lua_State *pLuaState)
{
    /* LI_BootStringTyrant(sPath)
       Switches to String Tyrant mode. Pass "NULL" to switch to normal mode. */
    lua_register(pLuaState, "LI_BootStringTyrant", &Hook_LI_BootStringTyrant);

    /* LI_WaitForKeypress()
       Load Interrupt now waits until the player presses a key, blocking timer updates. */
    lua_register(pLuaState, "LI_WaitForKeypress", &Hook_LI_WaitForKeypress);

    /* LI_Interrupt()
       Runs the interrupt code. Used so Lua can emulate load interrupt cases. */
    lua_register(pLuaState, "LI_Interrupt", &Hook_LI_Interrupt);

    /* LI_Reset(iCount)
       Resets the LoadInterrupt's values and changes the max to the given amount. */
    lua_register(pLuaState, "LI_Reset", &Hook_LI_Reset);

    /* LI_Finish()
       Completes the LoadInterrupt sequence. */
    lua_register(pLuaState, "LI_Finish", &Hook_LI_Finish);

    /* LI_GetLastInterruptCount() (1 Integer)
       Returns how many interrupts have been called since the last reset. */
    lua_register(pLuaState, "LI_GetLastInterruptCount", &Hook_LI_GetLastInterruptCount);

    /* LI_SetSuppression(bFlag)
       Suppresses or unsuppresses the load interrupt. */
    lua_register(pLuaState, "LI_SetSuppression", &Hook_LI_SetSuppression);
}

//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
int Hook_LI_BootStringTyrant(lua_State *L)
{
    //LI_BootStringTyrant(sPath)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("LI_BootStringTyrant");
    LoadInterrupt::Fetch()->InitStringTyrant(lua_tostring(L, 1));
    return 0;
}
int Hook_LI_WaitForKeypress(lua_State *L)
{
    //LI_WaitForKeypress()
    LoadInterrupt::WaitForKeypress();
    return 0;
}
int Hook_LI_Interrupt(lua_State *L)
{
    //LI_Interrupt()
    if(SugarBitmap::xInterruptCall) SugarBitmap::xInterruptCall();
    return 0;
}
int Hook_LI_Reset(lua_State *L)
{
    //LI_Reset(iCount)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("LI_Reset");
    LoadInterrupt::Fetch()->Reset(lua_tointeger(L, 1));
    return 0;
}
int Hook_LI_Finish(lua_State *L)
{
    //LI_Finish()
    LoadInterrupt::Fetch()->Finish();
    return 0;
}
int Hook_LI_GetLastInterruptCount(lua_State *L)
{
    //LI_GetLastInterruptCount()
    lua_pushinteger(L, LoadInterrupt::Fetch()->GetCallsSoFar());
    return 1;
}
int Hook_LI_SetSuppression(lua_State *L)
{
    //LI_SetSuppression(bFlag)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("LI_SetSuppression");
    LoadInterrupt::Fetch()->mIsSuppressed = lua_toboolean(L, 1);
    fprintf(stderr, "Changed flag.\n");
    return 0;
}
