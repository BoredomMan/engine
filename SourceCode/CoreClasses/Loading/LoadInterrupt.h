//--[LoadInterrupt]
//--This object is created right at program start and persists throughout. It is an 'interrupt' in
//  that it is called whenever a loading action occurs, and will render a loading screen automatically
//  with a background and a moving image. The object does not rely on anything else such as static
//  managers, so it is fairly independent.

#pragma once

//--[Includes]
#include "Definitions.h"
#include "Structures.h"

//--[Local Definitions]
#define RUN_FRAMES 10
#define MARY_FRAMES 159

//--[Class]
class LoadInterrupt
{
    private:
    //--System
    bool mIsInitted;
    bool mIsAwaitingKeypress;

    //--Loading Bar
    int mCurrent;
    int mCurrentMax;

    //--Animation Data.
    int mFrame;
    int mFrameDenominator;

    //--Graphics
    struct
    {
        bool mIsReady;
        struct
        {
            //--Animation
            SugarBitmap *mChristineFrames;
            SugarBitmap *mRaijuFrames;

            //--Loading Text
            SugarBitmap *mAnyKeyOver;
            SugarBitmap *mAnyKeyUnder;
            SugarBitmap *mLoadingOver;
            SugarBitmap *mLoadingUnder;
        }Data;
    }Images;

    //--String Tyrant Mode
    SugarLinkedList *mStringTyrantImages;

    //--Timing
    bool mForceNextUpdate;
    float mLastUpdateCall;

    protected:

    public:
    //--System
    LoadInterrupt();
    ~LoadInterrupt();

    //--Public Variables
    bool mIsSuppressed;

    //--Property Queries
    int GetCallsSoFar();
    bool IsAwaitingKeypress();

    //--Manipulators
    void Init();
    void InitStringTyrant(const char *pDatafilePath);
    void SetKeypressFlag(bool pFlag);
    static void Interrupt();
    static void InterruptStringTyrant();
    static void WaitForKeypress();

    //--Core Methods
    void Reset(int pMaxCount);
    void Call(bool pForceRender);
    void CallStringTyrant(bool pForceRender);
    void Finish();

    //--Update
    //--File I/O
    //--Drawing
    //--Pointer Routing
    //--Static Functions
    static LoadInterrupt *Fetch();

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_LI_BootStringTyrant(lua_State *L);
int Hook_LI_WaitForKeypress(lua_State *L);
int Hook_LI_Interrupt(lua_State *L);
int Hook_LI_Reset(lua_State *L);
int Hook_LI_Finish(lua_State *L);
int Hook_LI_GetLastInterruptCount(lua_State *L);
int Hook_LI_SetSuppression(lua_State *L);
