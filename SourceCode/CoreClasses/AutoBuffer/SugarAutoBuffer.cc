//--Base
#include "SugarAutoBuffer.h"

//--Classes
//--CoreClasses
//--Definitions
//--GUI
//--Libraries
//--Managers

//=========================================== System ==============================================
SugarAutoBuffer::SugarAutoBuffer()
{
    //--[SugarAutoBuffer]
    //--System
    mUse8BitForStringLengths = false;
    mIsDisabled = false;
    mBufferSize = SAB_START_SIZE;
    SetMemoryData(__FILE__, __LINE__);
    mBuffer = (uint8_t *)starmemoryalloc(sizeof(uint8_t) * mBufferSize);

    //--Cursor
    mCursor = 0;
}
SugarAutoBuffer::~SugarAutoBuffer()
{
    free(mBuffer);
}

//====================================== Property Queries =========================================
int SugarAutoBuffer::GetSize()
{
    if(mIsDisabled) return 0;
    return mBufferSize;
}
int SugarAutoBuffer::GetCursor()
{
    if(mIsDisabled) return 0;
    return mCursor;
}
uint8_t *SugarAutoBuffer::GetRawData()
{
    if(mIsDisabled) return NULL;
    return mBuffer;
}
uint8_t *SugarAutoBuffer::LiberateData()
{
    if(mIsDisabled) return NULL;

    //--Disable
    uint8_t *rOldData = mBuffer;
    mIsDisabled = true;

    //--Clear reference to the data, or it will be freed when the class is deleted!
    mBuffer = NULL;
    return rOldData;
}

//========================================= Manipulators ==========================================
void SugarAutoBuffer::Reinitialize()
{
    //--If the class has been disabled by a LiberateData() call, it can be restarted by using this
    //  function. If it hasn't, this call will do nothing.
    //--You could ostensibly create a new class to get the same effect, but it may be useful to
    //  re-use the same pointer in a while loop.
    if(!mIsDisabled) return;

    //--Allocate and reset.
    mIsDisabled = false;
    mBufferSize = SAB_START_SIZE;
    mBuffer = (uint8_t *)starmemoryalloc(sizeof(uint8_t) * mBufferSize);
    mCursor = 0;
}
void SugarAutoBuffer::Set8BitsForStringLengths(bool pFlag)
{
    mUse8BitForStringLengths = pFlag;
}
int SugarAutoBuffer::AppendVoidData(void *pPtr, size_t pSize)
{
    //--Appends a generic size. Not very safe!
    if(mIsDisabled) return 0;

    //--Length check.
    AllocateMoreSpace(pSize);

    //--Append.
    memcpy(&mBuffer[mCursor], pPtr, pSize);
    mCursor += pSize;

    //--Return the size provided.
    return pSize;
}
int SugarAutoBuffer::AppendNull()
{
    //--Appends a single null character. Useful with AppendStringWithoutNull() to assemble a very
    //  long string.
    char tNull = 0;
    return AppendVoidData(&tNull, sizeof(char));
}
int SugarAutoBuffer::AppendCharacter(char pCharacter)
{
    return AppendVoidData(&pCharacter, sizeof(char));
}
int SugarAutoBuffer::AppendInt8(int8_t pInteger)
{
    return AppendVoidData(&pInteger, sizeof(int8_t));
}
int SugarAutoBuffer::AppendUInt8(uint8_t pInteger)
{
    return AppendVoidData(&pInteger, sizeof(uint8_t));
}
int SugarAutoBuffer::AppendInt16(int16_t pInteger)
{
    return AppendVoidData(&pInteger, sizeof(int16_t));
}
int SugarAutoBuffer::AppendUInt16(uint16_t pInteger)
{
    return AppendVoidData(&pInteger, sizeof(uint16_t));
}
int SugarAutoBuffer::AppendInt32(int32_t pInteger)
{
    return AppendVoidData(&pInteger, sizeof(uint32_t));
}
int SugarAutoBuffer::AppendUInt32(uint32_t pInteger)
{
    return AppendVoidData(&pInteger, sizeof(uint32_t));
}
int SugarAutoBuffer::AppendFloat(float pValue)
{
    return AppendVoidData(&pValue, sizeof(float));
}
int SugarAutoBuffer::AppendString(const char *pString)
{
    //--Simply appends the given string to the buffer, including the trailing NULL character.
    if(!pString || mIsDisabled) return 0;

    //--Length check.
    int32_t tLen = (int32_t)strlen(pString);
    int32_t tTrueLen = (sizeof(char) * (tLen+1));
    AllocateMoreSpace(tTrueLen);

    //--Append.
    for(int i = 0; i < tLen; i ++)
    {
        mBuffer[mCursor+0] = pString[i];
        mBuffer[mCursor+1] = '\0';
        mCursor ++;
    }
    mCursor ++;

    //--Done.
    return tTrueLen;
}
int SugarAutoBuffer::AppendStringWithoutNull(const char *pString)
{
    //--Overload, assumes the below algorithm is executed allowing more than 256 characters.
    return AppendStringWithoutNull(pString, false);
}
int SugarAutoBuffer::AppendStringWithoutNull(const char *pString, bool pCantExceed256)
{
    //--Appends the string without the trailing NULL. If pCantExceed256 is true, then no more than
    //  256 characters will be written.
    if(!pString || mIsDisabled) return 0;

    //--Can't exceed 256?
    int32_t tLen = (int32_t)strlen(pString);
    if(mUse8BitForStringLengths)
    {
        if(pCantExceed256 && tLen >= 256) tLen = 256;
    }
    //--Use 65536 instead.
    else
    {
        if(pCantExceed256 && tLen >= 65536) tLen = 65536;
    }

    //--Allocate space as necessary.
    AllocateMoreSpace(tLen);

    //--Append.
    for(int i = 0; i < tLen; i ++)
    {
        mBuffer[mCursor] = pString[i];
        mCursor ++;
    }

    //--Done.
    return tLen;
}
int SugarAutoBuffer::AppendStringWithLen(const char *pString)
{
    //--Given a string, appends the length of that string as a single byte then the string itself,
    //  not including the trailing NULL. Returns how many letters were added.
    //--Note that this is only for *very short strings*. If more than 256 characters are in the
    //  string, it will lose accuracy since the string header is a single byte. If you need a
    //  very long string that needs a pre-sized length, do it manually.
    if(!pString || mIsDisabled) return 0;

    //--Append the length as the 0th letter. If it's more than 65535, scale it back!
    int32_t tLen = (int32_t)strlen(pString);
    if(mUse8BitForStringLengths)
    {
        if(tLen >= 0xFF) tLen = 0xFF;
        AppendInt8(tLen);
        return AppendStringWithoutNull(pString, true) + (sizeof(uint8_t) * 1);
    }

    //--65536 instead.
    if(tLen >= 0xFFFF) tLen = 0xFFFF;
    AppendInt16(tLen);
    return AppendStringWithoutNull(pString, true) + (sizeof(uint16_t) * 1);
}
int SugarAutoBuffer::AppendStringWithLen(const char *pString, const char *pDefault)
{
    //--Overload. If the pString is NULL, the pDefault is used instead. If pDefault is NULL, then
    //  we use a defined internal default.
    if(pString) return AppendStringWithLen(pString);
    if(pDefault) return AppendStringWithLen(pDefault);
    return AppendStringWithLen(SAB_DEFAULT_STRING);
}
void SugarAutoBuffer::RemoveBytes(int pAmount)
{
    //--Seeks the cursor back and 0's out the data.
    if(pAmount < 1) return;
    if(pAmount > (int)mCursor) pAmount = mCursor;

    //--Clear.
    for(int i = 0; i < pAmount; i ++)
    {
        mBuffer[mCursor - i - 1] = 0;
    }

    //--Move the cursor back.
    mCursor -= pAmount;
}
void SugarAutoBuffer::Seek(int pMove)
{
    //--Note: You cannot seek past the end of the buffer nor can you seek below 0.
    mCursor += pMove;
    if(mCursor < 0) mCursor = 0;
    if(mCursor >= mBufferSize) mCursor = mBufferSize - 1;
}

//========================================= Core Methods ==========================================
//===================================== Private Core Methods ======================================
void SugarAutoBuffer::AllocateMoreSpace(int pAddingSize)
{
    //--Called at the beginning of each append function, this checks the expected addition against
    //  the current buffer size. If more size is needed, more is added. If not, nothing happens.
    uint32_t tNewNeededSize = mCursor + pAddingSize;
    if(tNewNeededSize < mBufferSize) return;

    //--More space is needed!
    while(mBufferSize <= tNewNeededSize) mBufferSize += SAB_START_SIZE;

    //--Now we know how much space we need. Set it.
    mBuffer = (uint8_t *)realloc(mBuffer, sizeof(uint8_t) * mBufferSize);
}

//============================================ Update =============================================
//=========================================== File I/O ============================================
//=========================================== Drawing =============================================
//======================================= Pointer Routing =========================================
//====================================== Static Functions =========================================
//========================================= Lua Hooking ===========================================
//======================================= Command Hooking =========================================
//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
//=================================================================================================
//                                       Command Functions                                       ==
//=================================================================================================
