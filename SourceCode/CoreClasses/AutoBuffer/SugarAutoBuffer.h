//--[SugarAutoBuffer]
//--Basically a stream stored in RAM, the SugarAutoBuffer stores a buffer of generic data and a cursor.
//  Data can be added to the buffer, and it can be seeked around in. The buffer has no specific size,
//  instead increasing or decreasing size as the user adds data.
//--When/if the data is 'liberated', the class is disabled and cannot be used anymore. It should be
//  deleted immediately afterwards.

#pragma once

#include "Definitions.h"
#include "Structures.h"

//--[Local Definitions]
#define SAB_START_SIZE 256
#define SAB_DEFAULT_STRING "NOSTRING"

class SugarAutoBuffer
{
    private:
    //--System
    bool mUse8BitForStringLengths;
    bool mIsDisabled;
    uint32_t mBufferSize;
    uint8_t *mBuffer;

    //--Cursor
    uint32_t mCursor;

    protected:

    public:
    //--System
    SugarAutoBuffer();
    ~SugarAutoBuffer();

    //--Public Variables
    //--Property Queries
    int GetSize();
    int GetCursor();
    uint8_t *GetRawData();
    uint8_t *LiberateData();

    //--Manipulators
    void Reinitialize();
    void Set8BitsForStringLengths(bool pFlag);
    int AppendVoidData(void *pPtr, size_t pSize);
    int AppendNull();
    int AppendCharacter(char pCharacter);
    int AppendInt8(int8_t pInteger);
    int AppendUInt8(uint8_t pInteger);
    int AppendInt16(int16_t pInteger);
    int AppendUInt16(uint16_t pInteger);
    int AppendInt32(int32_t pInteger);
    int AppendUInt32(uint32_t pInteger);
    int AppendFloat(float pValue);
    int AppendString(const char *pString);
    int AppendStringWithoutNull(const char *pString);
    int AppendStringWithoutNull(const char *pString, bool pCantExceed255);
    int AppendStringWithLen(const char *pString);
    int AppendStringWithLen(const char *pString, const char *pDefault);
    void RemoveBytes(int pAmount);
    void Seek(int pMove);

    //--Core Methods
    private:
    //--Private Core Methods
    void AllocateMoreSpace(int pAddingSize);

    public:
    //--Update
    //--File I/O
    //--Drawing
    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
