//--Base
#include "StarlightPerlin.h"

//--Classes
//--CoreClasses
//--Definitions
//--Libraries
//--Managers

//=========================================== System ==============================================
StarlightPerlin::StarlightPerlin()
{
    //--[StarlightPerlin]
    //--System
    mIsReady = false;

    //--Storage
    mSeed = 0;
    mFrequency = 0.0f;
    mOctaves = 0;
    mLocalNoise = NULL;
}
StarlightPerlin::~StarlightPerlin()
{
    delete mLocalNoise;
}
StarlightPerlin *StarlightPerlin::xgStatic = NULL;

//====================================== Property Queries =========================================
float StarlightPerlin::Roll(float pX)
{
    if(!mIsReady) return 0.0f;
    return (float)mLocalNoise->noise(pX);
}
float StarlightPerlin::Roll(float pX, float pY)
{
    if(!mIsReady) return 0.0f;
    return (float)mLocalNoise->noise(pX, pY);
}
float StarlightPerlin::Roll(float pX, float pY, float pZ)
{
    if(!mIsReady) return 0.0f;
    return (float)mLocalNoise->noise(pX, pY, pZ);
}
float StarlightPerlin::RollClamped(float pX)
{
    if(!mIsReady) return 0.0f;
    return ((float)mLocalNoise->noise(pX) * 0.5f) + 0.5f;
}
float StarlightPerlin::RollClamped(float pX, float pY)
{
    if(!mIsReady) return 0.0f;
    return ((float)mLocalNoise->noise(pX, pY) * 0.5f) + 0.5f;
}
float StarlightPerlin::RollClamped(float pX, float pY, float pZ)
{
    if(!mIsReady) return 0.0f;
    return ((float)mLocalNoise->noise(pX, pY, pZ) * 0.5f) + 0.5f;
}

//========================================= Manipulators ==========================================
void StarlightPerlin::SetSeed(uint32_t pSeed)
{
    mSeed = pSeed;
}
void StarlightPerlin::SetFrequency(float pFrequency)
{
    mFrequency = pFrequency;
}
void StarlightPerlin::SetOctaves(int pOctaves)
{
    mOctaves = pOctaves;
}
void StarlightPerlin::Boot()
{
    mIsReady = true;
    delete mLocalNoise;
    mLocalNoise = new siv::PerlinNoise(mSeed);
}

//========================================= Core Methods ==========================================
//===================================== Private Core Methods ======================================
//============================================ Update =============================================
//=========================================== File I/O ============================================
//=========================================== Drawing =============================================
//======================================= Pointer Routing =========================================
//====================================== Static Functions =========================================
//========================================= Lua Hooking ===========================================
//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
