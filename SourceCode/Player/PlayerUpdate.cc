//--Base
#include "PlayerPony.h"

//--Classes
#include "Actor.h"
#include "PandemoniumLevel.h"

//--CoreClasses
//--Definitions
//--GUI
//--Libraries
//--Managers
#include "ControlManager.h"

//============================================ Update =============================================
void PlayerPony::Update()
{
    //--Player's update point. Since the player can only actually act during their turn, the Actor
    //  has a special flag which indicates that controls need to be paid attention to. On any
    //  other update tick, all controls are ignored.
    if(!Actor::xAreControlsEnabled) return;

    //--These commands only work if the current level is a PandemoniumLevel. They will not work
    //  if it's a VisualLevel, that handles its own behaviors.
    PandemoniumLevel *rActiveLevel = PandemoniumLevel::Fetch();
    if(!rActiveLevel || rActiveLevel->GetType() != POINTER_TYPE_PANDEMONIUMLEVEL) return;

    //--Setup.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Check if the Enter key has been pushed. This skips the turn.
    if(rControlManager->IsFirstPress("Enter"))
    {
        Actor::xInstruction = INSTRUCTION_SKIPTURN;
    }
    //--Directional keys. These quickly move the player around.
    else if(rControlManager->IsFirstPress("Up"))
    {
        Actor::xInstruction = INSTRUCTION_MOVENORTH;
    }
    else if(rControlManager->IsFirstPress("Right"))
    {
        Actor::xInstruction = INSTRUCTION_MOVEEAST;
    }
    else if(rControlManager->IsFirstPress("Down"))
    {
        Actor::xInstruction = INSTRUCTION_MOVESOUTH;
    }
    else if(rControlManager->IsFirstPress("Left"))
    {
        Actor::xInstruction = INSTRUCTION_MOVEWEST;
    }
    else if(rControlManager->IsFirstPress("UpLevel"))
    {
        Actor::xInstruction = INSTRUCTION_MOVEUP;
    }
    else if(rControlManager->IsFirstPress("DnLevel"))
    {
        Actor::xInstruction = INSTRUCTION_MOVEDOWN;
    }
    //--[Mouse Management]
    //--The player may also make use of on-screen controls. These do the same thing as the hotkeys
    //  in that they queue instructions, but may be more intuitive to some.
    else if(rControlManager->IsFirstPress("MouseLft"))
    {
        //--Store the mouse coordinates. They should be renormalized to the canvas, so a negative means
        //  that the click is off the canvas edge.
        int tDummy;
        rControlManager->GetMouseCoords(mClickBeginX, mClickBeginY, tDummy);
    }
    //--Release state.
    else if(rControlManager->IsFirstRelease("MouseLft"))
    {
        //--Check if both coordinates for press and release were over the same object. If so,
        //  send that object a click.
        //--Onscreen click handling is not done by the GUI, but instead by virtual objects within
        //  the PandemoniumLevel class.
        if(rActiveLevel)
        {
            //--Check if an instruction actually came back. It's possible this didn't click anything.
            int tCoordX, tCoordY, tCoordZ;
            rControlManager->GetMouseCoords(tCoordX, tCoordY, tCoordZ);
            int tInstruction = rActiveLevel->EmulateClick(mClickBeginX, mClickBeginY, tCoordX, tCoordY);
            if(tInstruction != INSTRUCTION_NOENTRY)
            {
                Actor::xInstruction = tInstruction;
            }
        }
    }
}
