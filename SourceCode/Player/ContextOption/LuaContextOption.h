//--[LuaContextOption]
//--Class representing a lua-based option for the context menu. Has an initializer, a target-validity
//  query, and an execution. They all fire from the same script with different codes.

#pragma once

//--[Includes]
#include "Definitions.h"
#include "Structures.h"

//--[Local Structures]
class LuaContextOption;
typedef struct
{
    Actor *rCallerPtr;
    RootEntity *rTargetPtr;
    LuaContextOption *rContextOption;
}LCOStoragePack;

//--[Local Definitions]
#define CONTEXT_INITIALIZE 0.0f
#define CONTEXT_ISLEGALTARGET 1.0f
#define CONTEXT_EXECUTION 2.0f

//--[Classes]
class LuaContextOption : public RootObject
{
    private:
    //--System
    char *mControllerScript;
    char *mLocalName;

    protected:

    public:
    //--System
    LuaContextOption();
    virtual ~LuaContextOption();

    //--Public Variables
    static bool xIsLegalTarget;
    static Actor *xrCaller;
    static RootEntity *xrTarget;

    //--Property Queries
    char *GetName();

    //--Manipulators
    void SetControlScript(const char *pScript);
    void SetName(const char *pName);

    //--Core Methods
    bool IsLegalOnTarget(Actor *pCaller, RootEntity *pTarget);
    void ExecuteOnTarget(Actor *pCaller, RootEntity *pTarget);

    private:
    //--Private Core Methods
    public:
    //--Update
    //--File I/O
    //--Drawing
    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_CO_WipePlayerCommands(lua_State *L);
int Hook_CO_RegisterCommand(lua_State *L);
int Hook_CO_MarkTargetAsLegal(lua_State *L);
int Hook_CO_PushCaller(lua_State *L);
int Hook_CO_PushTarget(lua_State *L);
int Hook_CO_SetProperty(lua_State *L);
