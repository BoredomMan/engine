//--Base
#include "PlayerPony.h"

//--Classes
//--Definitions
//--CoreClasses
#include "SugarLinkedList.h"

//--GUI
//--Libraries
//--Managers
#include "ControlManager.h"

bool PlayerPony::AreControlsLocked()
{
    return (mLockoutList->GetListSize() > 0);
}
void PlayerPony::AddToLockList(const char *pLockName)
{
    //--The mLockoutList is responsible for blocking the player's controls when the player is
    //  supposed to be unable to move.  If there is anything on the list, the player behaves
    //  as though her controls are turned off.
    //--This function adds entries to the lock list. Controls do not auto-lock, use AreControlsLocked()
    //  to check if you should be ignoring controls during an update. You may optionally allow hotkeys
    //  to keep functioning.

    //--Error check
    if(!pLockName) return;

    //--Avoid duplicates
    void *rCheck = mLockoutList->GetElementByName(pLockName);
    if(rCheck) return;

    //--Add to list
    SetMemoryData(__FILE__, __LINE__);
    void *nDummyPtr = starmemoryalloc(sizeof(char));
    mLockoutList->AddElement(pLockName, nDummyPtr);

    //--<DEBUG>
    //--Print the current contents of the locking list.
    if(false)
    {
        for(int i = 0; i < mLockoutList->GetListSize(); i ++) fprintf(stderr, " ");
        fprintf(stderr, "Lock %s added, %i locks on list\n", pLockName, mLockoutList->GetListSize());
    }
}
void PlayerPony::DelFromLockList(const char *pLockName)
{
    //--Removes entries off the lock list.

    //--Error check
    if(!pLockName) return;

    //--Pull out the entry
    int tOldSize = mLockoutList->GetListSize();
    mLockoutList->RemoveElementS(pLockName);
    int tNewSize = mLockoutList->GetListSize();

    //if(tNewSize == 0 && rNPCHost) rNPCHost->ClearAIQueue();

    //--<DEBUG>
    //--Print the current contents of the locking list.
    if(tOldSize == tNewSize) return;
    if(false)
    {
        for(int i = 0; i < mLockoutList->GetListSize(); i ++) fprintf(stderr, " ");
        fprintf(stderr, "Lock %s removed, %i locks on list\n", pLockName, mLockoutList->GetListSize());
    }
}
void PlayerPony::DelFromLockListPattern(const char *pPattern)
{
    //--Much like DelFromLockList, except passes in a pattern.  This will use strncmp to compare
    //  strings for deletion.
    if(!pPattern) return;

    unsigned int tLen = strlen(pPattern);
    void *rCheckPtr = mLockoutList->SetToHeadAndReturn();
    while(rCheckPtr)
    {
        const char *rName = mLockoutList->GetRandomPointerName();

        if(!strncmp(rName, pPattern, tLen))
        {
            mLockoutList->RemoveRandomPointerEntry();
        }
        rCheckPtr = mLockoutList->IncrementAndGetRandomPointerEntry();
    }
}
void PlayerPony::ClearLockList()
{
    mLockoutList->ClearList();
}
