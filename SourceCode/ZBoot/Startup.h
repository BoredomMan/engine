//--[Startup.h]
//--Functions that are required for program startup, initializing variables and libraries.
//  The boot sequence should be called in order from top to bottom, with the exception of the
//  Lua registration going when the LM is created, but being listed last.

#pragma once

#include "Definitions.h"

#define STANDARD_FPS 60.0f

void InitializeGlobals();
void HandleCommandLine(int pArgsTotal, char **pArgs);
bool InitializeLibraries();
bool InitializeManagers();
#if defined _SDL_PROJECT_
#elif defined _ALLEGRO_PROJECT_
void RegisterEvents(ALLEGRO_EVENT_QUEUE *EQ);
#endif
void LoadResources();
void PostProcess();
void ProgramBoot();
void RegisterLuaFunctions(lua_State *pLuaState);
void RegisterConsoleFunctions();

void ProgramFlexibleBoot();
void ProgramFlexibleResources();
