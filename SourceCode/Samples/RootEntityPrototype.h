//--[NewClass]
//--Notes

#pragma once

#include "Definitions.h"
#include "Structures.h"
#include "RootEntity.h"

class NewClass : public RootEntity
{
    private:

    protected:

    public:
    //--System
    NewClass();
    virtual ~NewClass();

    //--Public Variables
    //--Property Queries
    //--Manipulators
    //--Core Methods
    private:
    //--Private Core Methods
    public:
    //--Update
    //--File I/O
    //--Drawing
    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions

