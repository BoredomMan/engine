//--This prototype is a sample of a functional Lua handler.
/*
//--Base
#include "AdvCombatAbility.h"

//--Classes
//--CoreClasses
//--Definitions
//--GUI
//--Libraries
#include "DataLibrary.h"

//--Managers

//========================================= Lua Hooking ===========================================
void AdvCombatAbility::HookToLuaState(lua_State *pLuaState)
{
    /* AdvCombatAbility_GetProperty("Dummy") (1 Integer)
       Gets and returns the requested property in the Adventure Combat class. */
/*
    lua_register(pLuaState, "AdvCombatAbility_GetProperty", &Hook_AdvCombatAbility_GetProperty);

    /* AdvCombatAbility_SetProperty("Dummy")
       Sets the property in the Adventure Combat class. */
/*
    lua_register(pLuaState, "AdvCombatAbility_SetProperty", &Hook_AdvCombatAbility_SetProperty);
}

//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
int Hook_AdvCombatAbility_GetProperty(lua_State *L)
{
    ///--[Argument Listing]
    //--[System]
    //AdvCombatAbility_GetProperty("Dummy") (1 Integer)

    //--Argument check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("AdvCombatAbility_GetProperty");

    //--Setup
    int tReturns = 0;
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[Static]
    //--Dummy static.
    if(!strcasecmp(rSwitchType, "Static Dummy"))
    {
        lua_pushinteger(L, 0);
        return 1;
    }

    ///--[Dynamic]
    //--Type check.
    if(!DataLibrary::Fetch()->IsActiveValid(POINTER_TYPE_ADVCOMBATABILITY)) return LuaTypeError("AdvCombatAbility_GetProperty");
    AdvCombatAbility *rAbility = (AdvCombatAbility *)DataLibrary::Fetch()->rActiveObject;

    ///--[System]
    //--Dummy dynamic.
    if(!strcasecmp(rSwitchType, "Dummy") && tArgs == 1)
    {
        lua_pushinteger(L, 0);
        tReturns = 1;
    }
    //--Error case.
    else
    {
        LuaPropertyError("AdvCombatAbility_GetProperty", rSwitchType, tArgs);
    }

    return tReturns;
}
int Hook_AdvCombatAbility_SetProperty(lua_State *L)
{
    ///--[Argument Listing]
    //--[System]
    //AdvCombatAbility_SetProperty("Dummy")

    //--Argument check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("AdvCombatAbility_SetProperty");

    //--Setup
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[Static]
    //--Dummy static.
    if(!strcasecmp(rSwitchType, "Static Dummy"))
    {
        return 0;
    }

    ///--[Dynamic]
    //--Type check.
    if(!DataLibrary::Fetch()->IsActiveValid(POINTER_TYPE_ADVCOMBATABILITY)) return LuaTypeError("AdvCombatAbility_SetProperty");
    AdvCombatAbility *rAbility = (AdvCombatAbility *)DataLibrary::Fetch()->rActiveObject;

    ///--[System]
    //--Dummy dynamic.
    if(!strcasecmp(rSwitchType, "Dummy") && tArgs == 1)
    {
    }
    //--Error case.
    else
    {
        LuaPropertyError("AdvCombatAbility_SetProperty", rSwitchType, tArgs);
    }

    return 0;
}*/
