//--[RootObject]
//--Base of the Starlight reflection tree. RootObjects contain a type which should be one of the
//  types set in Definitions.h. They also contain a UniqueID which is a static counter. No two objects
//  can logically have the same ID.

#pragma once

#include "Definitions.h"

class RootObject
{
    private:

    protected:
    //--System
    int mType;
    uint32_t mUniqueID;

    public:
    //--System
    RootObject();
    virtual ~RootObject();
    static void DeleteThis(void *pPtr);

    //--Property Queries
    int GetType();
    virtual bool IsOfType(int pType);
    uint32_t GetID();

    //--Static Functions
    static void HookToLuaState(lua_State *pLuaState);
};

//--Lua Functions
int Hook_RO_GetID(lua_State *L);
int Hook_RO_GetType(lua_State *L);

