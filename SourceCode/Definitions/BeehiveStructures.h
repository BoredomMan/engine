//--[Beehive Structures]
//--Structures common to the Beehive Mode classes.

#pragma once

//--[Definitions]
#include "Definitions.h"

//--Tile Base Types
#define TILE_BASE_LAND 0
#define TILE_BASE_WATER 1
#define TILE_BASE_TYPES 2

//--Tile Overlay Types
#define TILE_OVER_NONE 0
#define TILE_OVER_MOUNTAIN 1
#define TILE_OVER_FOREST 2
#define TILE_OVER_SWAMP 3
#define TILE_OVER_DRY 4
#define TILE_OVER_DESERT 5
#define TILE_OVER_TOTAL 6

//--Definition indicating maximum pathing distance.
#define BEEHIVE_PATH_MAX 10

//--Edges
#define BEEHIVE_TILE_EDGES 6

//--Statistics. Used by drones and some world entities.
#define BE_STAT_COMBAT 0
#define BE_STAT_SURVIVAL 1
#define BE_STAT_EXTRACTION 2
#define BE_STAT_CRAFTING 3
#define BE_STAT_UPKEEP 4
#define BE_STAT_RESEARCH 5
#define BE_STAT_TOTAL 6

#define BE_MAX_LEVEL 5

//--Represents an entry when computing pathing. Entries are placed on a linked list which recursively checks
//  adjacency and spawns new pulses until it runs out of AP.
typedef struct
{
    //--Movement info
    int mXPosition;
    int mYPosition;
    int mAPLeft;

    //--Train info
    int mTrailSize;
    int mXTrail[BEEHIVE_PATH_MAX];
    int mYTrail[BEEHIVE_PATH_MAX];
}PathingPulse;

//--Represents an item required for a work order. The item's name and quantity are specified, but the
//  item itself is not resolved until runtime to make it a lot easier to handle the packages.
//--This also represents items inside structure inventories. These items do not need to have advanced
//  properties, just names.
typedef struct WorkOrderItemPack
{
    //--Variables
    char mItemName[64];
    int mItemQuantity;

    //--Functions
    void Initialize();
    void Set(const char *pName, int pQuantity);
    void SetWithNameQuantity(const char *pName);
    WorkOrderItemPack *Clone();
}WorkOrderItemPack;

//--Represents a possible output for the recipe. Contains a set of outputs and a chance for this result
//  to occur. If only one result is possible, no roll is done and that set of outputs occurs.
class SugarLinkedList;
typedef struct WorkOrderOutputPack
{
    //--Variables
    SugarLinkedList *mLocalResultList;//WorkOrderItemPack
    int mWeight;

    //--Functions
    void Initialize();
    void RegisterItem(WorkOrderItemPack *pPack);
    void SetWeight(int pWeight);
    static void DeleteThis(void *pPtr);
}WorkOrderOutputPack;

//--Represents one of the buttons that can appear during an Event Popup.
struct TwoDimensionReal;
typedef struct EventPopupButton
{
    char mResponseCode[STD_MAX_LETTERS];
    TwoDimensionReal mDimensions;
}EventPopupButton;

//--Holds an Event Popup. These are enqueued if more than one needs to fire in a turn.
typedef struct EventPopupHolder
{
    //--Members
    char *mHeader;
    char *mText;
    char *mImagePath;
    char *mResponseScriptPath;
    SugarLinkedList *mButtonListing; //EventPopupButton

    //--Functions
    void Initialize();
    static void DeleteThis(void *pPtr);
}EventPopupHolder;
