//--[Definitions]
//--Contains constants, typedefs, and class forward definitions that should be used by everything
//  else in the program.  Almost all classes include this file!

#pragma once

#if defined _ALLEGRO_PROJECT_
    #include "pch.h"
#endif
#if defined _SDL_PROJECT_
    #if defined _FMOD_AUDIO_
        #include "SDLpchFMOD.h"
    #endif
    #if defined _BASS_AUDIO_
        #include "SDLpch.h"
    #endif
#endif

//--Debug definitions
#include "DebugDefinitions.h"

//--[Root Object]
//--Almost all the entities derive from this as it is.
#include "RootObject.h"

//--[Structure Forward Declarations]
//--Needed for some arguments lists.
struct TwoDimensionReal;
struct ThreeDimensionReal;

//--[Function Pointers]
#ifndef _TypedefFunctionPtr_
#define _TypedefFunctionPtr_
class BaseEffect;
class SugarBitmap;

//--General Purpose
typedef void(*DeletionFunctionPtr)(void *);
typedef void(*LogicFnPtr)(void *);
typedef void(*UpdateFnPtr)(void *);
typedef void(*DrawFnPtr)(void *);

//--Gears and Drives
struct MainPackage;
typedef void(*EventHandlerFnPtr)(MainPackage &);
typedef void(*LogicHandlerFnPtr)(MainPackage &);
typedef void(*RenderHandlerFnPtr)(MainPackage &);

//--Class Specific
typedef SugarBitmap*(*AnimationResolveFuncPtr)(void *, int&, int&);

#endif

//--Unions
union FlexPtr
{
    char *c;
    unsigned char *uc;
    short *s;
    unsigned short *us;
    int *i;
    unsigned int *ui;
    float *f;
    double *d;
    void *v;
};
union FlexVal
{
    char c;
    unsigned char uc;
    short s;
    unsigned short us;
    int i;
    unsigned int ui;
    float f;
    double d;
};

//--[OpenGL Types]
#if defined _TARGET_OS_WINDOWS_
#  if !defined(_W64)
#    if !defined(__midl) && (defined(_X86_) || defined(_M_IX86)) && defined(_MSC_VER) && _MSC_VER >= 1300
#      define _W64 __w64
#    else
#      define _W64
#    endif
#  endif
#  if !defined(_PTRDIFF_T_DEFINED) && !defined(_PTRDIFF_T_) && !defined(__MINGW64__)
#    ifdef _WIN64
typedef __int64 ptrdiff_t;
#    else
typedef _W64 int ptrdiff_t;
#    endif
#    define _PTRDIFF_T_DEFINED
#    define _PTRDIFF_T_
#  endif
typedef char GLchar;
typedef ptrdiff_t GLsizeiptr;
#endif

//--[Pointer Type Specification]
//--[Primitives]
#define POINTER_TYPE_FAIL 0
#define POINTER_TYPE_BOOL 1
#define POINTER_TYPE_INT 2
#define POINTER_TYPE_FLOAT 3
#define POINTER_TYPE_STRING 4
#define POINTER_TYPE_UNSIGNEDINT 5

//--[CoreClasses]
#define POINTER_TYPE_CAMERA 10
#define POINTER_TYPE_ANIMATION 11
#define POINTER_TYPE_SUGARFONT 12
#define POINTER_TYPE_STARLIGHTSTRING 13

//--[Other]
#define POINTER_TYPE_CONTEXTOPTION 20
#define POINTER_TYPE_INVENTORYITEM 21
#define POINTER_TYPE_WORLDCONTAINER 22
#define POINTER_TYPE_GALLERYMENU 23
#define POINTER_TYPE_SKYBOX 24
#define POINTER_TYPE_ZONEEDITOR 25
#define POINTER_TYPE_WORLDLIGHT 26
#define POINTER_TYPE_CORRUPTERMENU 27
#define POINTER_TYPE_PERKPACK 28
#define POINTER_TYPE_MAGICPACK 29
#define POINTER_TYPE_TRANSFORMPACK 30
#define POINTER_TYPE_WORLDDIALOGUE 31
#define POINTER_TYPE_DIALOGUEACTOR 32
#define POINTER_TYPE_ADVENTUREMENU 33
#define POINTER_TYPE_ADVENTUREINVENTORY 34
#define POINTER_TYPE_ADVENTUREITEM 35
#define POINTER_TYPE_ADVENTURECOMBATENTITY 36
#define POINTER_TYPE_ADVENTURECOMBATENTITYUI 37
#define POINTER_TYPE_ADVENTURECOMBATACTIONCATEGORY 38
#define POINTER_TYPE_ADVENTURECOMBATACTION 39
#define POINTER_TYPE_ADVENTURECOMBATANIMATION 40
#define POINTER_TYPE_ADVENTURELIGHT 41
#define POINTER_TYPE_DIALOGUETOPIC 42
#define POINTER_TYPE_PAIRANORMALDIALOGUECHARACTER 43
#define POINTER_TYPE_SPINEEXPRESSION 44
#define POINTER_TYPE_PAIRANORMALDIALOGUE 44
#define POINTER_TYPE_DECKEDITOR 45
#define POINTER_TYPE_TEXTLEVOPTIONS 46
#define POINTER_TYPE_ACTORNOTICE 47
#define POINTER_TYPE_KPOPDANCEGAME 48
#define POINTER_TYPE_KPOPDANCER 49
#define POINTER_TYPE_BUILDPACKAGE 50
#define POINTER_TYPE_ADVCOMBAT 51
#define POINTER_TYPE_ADVCOMBATABILITY 52
#define POINTER_TYPE_ADVCOMBATANIMATION 53
#define POINTER_TYPE_ADVCOMBATEFFECT 54
#define POINTER_TYPE_ADVCOMBATENTITY 55
#define POINTER_TYPE_ADVCOMBATJOB 56
#define POINTER_TYPE_ALIASSTORAGE 57

//--[Classes]
//--RootEntity inheritance structure.
#define POINTER_TYPE_ENTITY_BEGIN 1000

    #define POINTER_TYPE_ROOTENTITY 1000
    #define POINTER_TYPE_PLAYER 1001
    #define POINTER_TYPE_ACTOR 1002
    #define POINTER_TYPE_TILEMAPACTOR 1003

    #define POINTER_TYPE_EFFECT_BEGIN 1100
        #define POINTER_TYPE_ROOTEFFECT 1100
    #define POINTER_TYPE_EFFECT_END 1199

#define POINTER_TYPE_ENTITY_END 1999

//--RootLevel Inheritance branch
#define POINTER_TYPE_LEVEL_BEGIN 2000

    #define POINTER_TYPE_ROOTLEVEL 2000
    #define POINTER_TYPE_PANDEMONIUMLEVEL 2001
    #define POINTER_TYPE_VISUALLEVEL 2002
    #define POINTER_TYPE_WAD_FILE 2003
    #define POINTER_TYPE_TILEDLEVEL 2004
    #define POINTER_TYPE_ADVENTURELEVEL 2005
    #define POINTER_TYPE_BEEHIVELEVEL 2006
    #define POINTER_TYPE_BLUESPHERELEVEL 2007
    #define POINTER_TYPE_TETRISLEVEL 2008
    #define POINTER_TYPE_INVADERLEVEL 2009
    #define POINTER_TYPE_ADVENTURELEVELGENERATOR 2010
    #define POINTER_TYPE_TEXTLEVEL 2011
    #define POINTER_TYPE_PAIRANORMALMENU 2012
    #define POINTER_TYPE_PAIRANORMALSETTINGSMENU 2013
    #define POINTER_TYPE_PAIRANORMALLEVEL 2014
    #define POINTER_TYPE_STRINGTYRANTTITLE 2015

#define POINTER_TYPE_LEVEL_END 2999

//--Room Inheritance Branch
#define POINTER_TYPE_ROOM_BEGIN 3000

    #define POINTER_TYPE_PANDEMONIUM_ROOM 3000
    #define POINTER_TYPE_VISUAL_ROOM 3001

#define POINTER_TYPE_ROOM_END 3999

//--Sugar Menu Inheritance Branch
#define POINTER_TYPE_MENU_BEGIN 4000
    #define POINTER_TYPE_MENU_ROOT 4000
    #define POINTER_TYPE_MENU_FLEX 4001
#define POINTER_TYPE_MENU_END 4999

//--Event Inheritance Branch
#define POINTER_TYPE_EVENT_BEGIN 5000
    #define POINTER_TYPE_EVENT_ROOT 5000
    #define POINTER_TYPE_EVENT_TIMER 5001
    #define POINTER_TYPE_EVENT_ACTOR 5002
    #define POINTER_TYPE_EVENT_CAMERA 5003
    #define POINTER_TYPE_EVENT_AUDIO 5004
#define POINTER_TYPE_EVENT_END 5999

//--[Executors]
#define POINTER_TYPE_EXECUTOR_BEGIN 20000
    #define POINTER_TYPE_EXECUTOR_ROOT 20000
#define POINTER_TYPE_EXECUTOR_END   20001

//--[Constant Definitions]
//--Directional Constants
#define DIR_UP 0
#define DIR_UPRIGHT 1
#define DIR_RIGHT 2
#define DIR_DOWNRIGHT 3
#define DIR_DOWN 4
#define DIR_DOWNLEFT 5
#define DIR_LEFT 6
#define DIR_UPLEFT 7

//--Directional Constants phrased as cardinal directions. These are NOT the same
//  as the NAV codes, which handle up and down but not diagonals.
#define DIR_NORTH DIR_UP
#define DIR_NE DIR_UPRIGHT
#define DIR_EAST DIR_RIGHT
#define DIR_SE DIR_DOWNRIGHT
#define DIR_SOUTH DIR_DOWN
#define DIR_SW DIR_DOWNLEFT
#define DIR_WEST DIR_LEFT
#define DIR_NW DIR_UPLEFT

//--Navigation Constants. Used for Classic Mode mostly.
#define NAV_NORTH 0
#define NAV_EAST 1
#define NAV_SOUTH 2
#define NAV_WEST 3
#define NAV_UP 4
#define NAV_DOWN 5
#define NAV_TOTAL 6

//--Rendering Constants

//--String Constants
#define STD_MAX_LETTERS 80
#define STD_NPC_LETTERS 32
#define STD_PATH_LEN 256

//--Circle Stuff
#define TODEGREE 57.29578f
#define TORADIAN 0.017453f

//--Time Constants
//  A tick is 1/60th of a second.  1/60 = 0.016666 repeating.
#define TICKSTOSECONDS 0.0166667f
#define SECONDSTOTICKS 60.0f
#define MILLISECONDSPERTICK 16.6667f

//--[Class Forward Definitions]
class Global;
class PlayerPony;
#include "ClassDefinitions.h"
#include "CoreClassDefinitions.h"
#include "ExecutorDefinitions.h"
#include "GearDefinitions.h"
#include "InterfaceDefinitions.h"
#include "LibraryDefinitions.h"
#include "ManagerDefinitions.h"

//--[Lua Functions]
//--These are used by virtually every Lua function in some way, and are thus stuffed into the main
//  definitions to avoid having to include it in every Lua-using class.
//--They are defined in the LuaManager's folder.
int LuaArgError(const char *pFunctionName);
int LuaArgError(const char *pFunctionName, lua_State *L);
int LuaArgError(const char *pFunctionName, const char *pSwitchType, int pArgs);
int LuaTypeError(const char *pFunctionName);
int LuaTypeError(const char *pFunctionName, lua_State *L);
int LuaPropertyError(const char *pFunctionName, const char *pSwitchType, int pArgs);
int LuaPropertyNoMatchError(const char *pFunctionName, const char *pSwitchType);

//--[Memory Functions]
//--Operations used to enable debugging of out-of-memory problems. Can be disabled with a define, in which case
//  they are simply aliases of starmemoryalloc().
extern char gxMemoryFile[256]; //Last-used malloc filename.
extern int gxMemoryLine; //Last-used malloc line.

//--Functions/Dummies.
#define USE_INTERNAL_MALLOC
#ifdef USE_INTERNAL_MALLOC
    void SetMemoryData(const char *pFile, int pLine);
    void *starmemoryalloc(size_t pSize);
#else
    #define SetMemoryData(pFile, pLine) ;
    #define starmemoryalloc(pSize) starmemoryalloc(pSize)
#endif

//--[Error Logging]
//--Logs an error to the ErrorLog.txt file. Typically used when a memory issue occurs.
extern char gxLastIssue[256];
void LogError();

//--[Common Functions]
//--These are used often enough that every class should have access to them.
//--These are found in SourceCode/Macros/Common/Definitions.cc
float GetGameTime();
float GetRandomPercent();
void ResetString(char *&sString, const char *pNewString);
char *InitializeString(const char *pFormat, ...);
int CompareSortingPackages(const void *elem1, const void *elem2);
uint32_t GetNearestSquare(uint32_t pBase);
void ClampToCircle(float &sValue);

//--[Console Handling]
//--Functions that make it easier to parse console commands.
//--These are found in SourceCode/Macros/Common/Definitions.cc
int GetTotalArguments(const char *pString);
char *GetArgumentS(const char *pString, int pNumberToGet);
float GetArgumentF(const char *pString, int pNumberToGet);
int GetArgumentI(const char *pString, int pNumberToGet);

//--[Spine Functions]
struct spAtlasPage;
#ifdef __cplusplus
extern "C" {
#endif
char  *_spUtil_readFile(const char *pPath, int *pLength);
void _spAtlasPage_createTexture(spAtlasPage *pSelf, const char *pPath);
void _spAtlasPage_disposeTexture(spAtlasPage* pSelf);
#ifdef __cplusplus
}
#endif

//--[Spatial Macros]
//--These are found in SourceCode/Macros/Common/Definitions.cc
void GetCenterPoints3DReal(ThreeDimensionReal pDimensions, float &sXCenter, float &sYCenter);
void GetCenterPoints2DReal(TwoDimensionReal pDimensions, float &sXCenter, float &sYCenter);
float GetDistanceBetween3DReal(ThreeDimensionReal pDimensionsA, ThreeDimensionReal pDimensionsB);

//--[Memory Functions]
bool VerifyStructure(void *pStructure, size_t pSize, size_t pSizePerElement);
bool VerifyStructure(void *pStructure, size_t pSize, size_t pSizePerElement, bool pHasDebug);
void Crossload(void *pStructure, size_t pSize, size_t pSizePerElement, const char *pPathPattern, const char **pNameList);
void Crossload(void *pStructure, size_t pSize, size_t pSizePerElement, const char *pPathPattern, const char **pNameList, bool pHasDebug);

//--[Text Functions]
void RenderTextToFit(SugarFont *pFont, float pX, float pY, float pMaxWid, const char *pText, ...);

//--[Program Definitions]
//--Resistances
#define ACE_RESIST_SLASH 0
#define ACE_RESIST_PIERCE 1
#define ACE_RESIST_STRIKE 2
#define ACE_RESIST_FIRE 3
#define ACE_RESIST_ICE 4
#define ACE_RESIST_LIGHTNING 5
#define ACE_RESIST_HOLY 6
#define ACE_RESIST_SHADOW 7
#define ACE_RESIST_BLEED 8
#define ACE_RESIST_BLIND 9
#define ACE_RESIST_POISON 10
#define ACE_RESIST_CORRODE 11
#define ACE_RESIST_TERRIFY 12
#define ACE_RESIST_TOTAL 13

//--[Effect Series]
//--Effects. These are in the same "category" as the other modifiers. The total value
//  is ACA_EFFECT_GRAND_TOTAL. Anything under that is a legal effect type.
#define ACA_EFFECT_NONE 0
#define ACA_EFFECT_TERRIFY 1
#define ACA_EFFECT_POISON 2
#define ACA_EFFECT_BLEED 3
#define ACA_EFFECT_CORRODE 4
#define ACA_EFFECT_BLIND 5
#define ACA_EFFECT_STUN 6
#define ACA_EFFECT_TOTAL 7

//--Stat Modifiers.
#define ACA_MODIFIER_SPEED 7
#define ACA_MODIFIER_DAMAGEPERCENT 8
#define ACA_MODIFIER_DAMAGEFIXED 9
#define ACA_MODIFIER_PROTECTION 10
#define ACA_MODIFIER_ACCURACY 11
#define ACA_MODIFIER_EVADE 12
#define ACA_MODIFIER_INITIATIVE 13
#define ACA_MODIFIER_TOTAL 14

//--Resistance Modifiers.
#define ACA_RESISTBUFF_START 14
#define ACA_RESISTBUFF_SLASH 14
#define ACA_RESISTBUFF_PIERCE 15
#define ACA_RESISTBUFF_STRIKE 16
#define ACA_RESISTBUFF_FIRE 17
#define ACA_RESISTBUFF_ICE 18
#define ACA_RESISTBUFF_LIGHTNING 19
#define ACA_RESISTBUFF_HOLY 20
#define ACA_RESISTBUFF_SHADOW 21
#define ACA_RESISTBUFF_BLEED 22
#define ACA_RESISTBUFF_BLIND 23
#define ACA_RESISTBUFF_POISON 24
#define ACA_RESISTBUFF_CORRODE 25
#define ACA_RESISTBUFF_TERRIFY 26
#define ACA_RESISTBUFF_TOTAL 27

//--Other Modifiers
#define ACA_STATCHANGE_THREAT 27
#define ACA_STATCHANGE_TOTAL 28

//--Total
#define ACA_EFFECT_GRAND_TOTAL 28

//--[String Functions]
#if defined _USE_LOCAL_STRCASECMP_
int strcasecmp(const char *pStringA, const char *pStringB);
int strncasecmp(const char *s1, const char *s2, register size_t n);

#endif
