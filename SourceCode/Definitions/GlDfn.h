//--Forward definitions for manual definition of GL functions.  This is part
//  of SugarCube's GL Wrangling.

#pragma once

//--[Includes]
#if defined _TARGET_OS_WINDOWS_
    #include <gl/gl.h>
    #include <gl/glu.h>
    #include <gl/glext.h>
#endif
#if defined _TARGET_OS_MAC_
    #include <osxgl.h>
    #include <osxglu.h>
    #include <osxglext.h>
#endif
#if defined _TARGET_OS_LINUX_
    #include "GL/gl.h"
#endif

//--[Typedefs]
#include <stddef.h>
typedef ptrdiff_t GLsizeiptr;
typedef char GLchar;

//--[Extra Code]
//--The DEFINE_PROC_TYPE macro used below is platform-independent. However, windows has a slightly
//  different API usage, so there are two versions here.

//--[Windows]
#if defined _TARGET_OS_WINDOWS_
	#define DEFINE_PROC_TYPE(type, name, args) \
		typedef type (APIENTRY * name) args;

//--[OSX]
#elif defined _TARGET_OS_MAC_
	#define DEFINE_PROC_TYPE(type, name, args) \
		typedef type (*name) args;

//--[*NIX systems]
#else
	#define DEFINE_PROC_TYPE(type, name, args) \
		typedef type (*name) args;
#endif

//--[Definitions]
//--A
DEFINE_PROC_TYPE(void, GLATTACHSHADER_FUNC, (GLuint, GLuint));
extern GLATTACHSHADER_FUNC sglAttachShader;

DEFINE_PROC_TYPE(void, GLACTIVETEXTURE_FUNC, (GLenum));
extern GLACTIVETEXTURE_FUNC sglActiveTexture;


//--B
DEFINE_PROC_TYPE(void, GLBINDBUFFER_FUNC, (GLenum, GLuint));
extern GLBINDBUFFER_FUNC sglBindBuffer;

DEFINE_PROC_TYPE(void, GLBINDFRAMEBUFFER_FUNC, (GLenum, GLuint));
extern GLBINDFRAMEBUFFER_FUNC sglBindFramebuffer;

DEFINE_PROC_TYPE(void, GLBINDVERTEXARRAY_FUNC, (GLuint));
extern GLBINDVERTEXARRAY_FUNC sglBindVertexArray;

DEFINE_PROC_TYPE(void, GLBLENDFUNCSEPARATE_FUNC, (GLenum, GLenum, GLenum, GLenum));
extern GLBLENDFUNCSEPARATE_FUNC sglBlendFuncSeparate;

DEFINE_PROC_TYPE(void, GLBUFFERDATA_FUNC, (GLenum, GLsizeiptr, const GLvoid *, GLenum));
extern GLBUFFERDATA_FUNC sglBufferData;

DEFINE_PROC_TYPE(void, GLBLENDEQUATION_FUNC, (GLenum));
extern GLBLENDEQUATION_FUNC sglBlendEquation;

//--C
DEFINE_PROC_TYPE(GLenum, GLCHECKFRAMEBUFFERSTATUS_FUNC, (GLenum));
extern GLCHECKFRAMEBUFFERSTATUS_FUNC sglCheckFramebufferStatus;

DEFINE_PROC_TYPE(void, GLCLIENTACTIVETEXTURE_FUNC, (GLenum));
extern GLCLIENTACTIVETEXTURE_FUNC sglClientActiveTexture;

DEFINE_PROC_TYPE(void, GLCOMPILESHADER_FUNC, (GLuint));
extern GLCOMPILESHADER_FUNC sglCompileShader;

DEFINE_PROC_TYPE(void, GLCOMPRESSEDTEX2D_FUNC, (GLenum, GLint, GLenum, GLsizei, GLsizei, GLint, GLsizei, const GLvoid *));
extern GLCOMPRESSEDTEX2D_FUNC sglCompressedTexImage2D;

DEFINE_PROC_TYPE(GLuint, GLCREATEPROGRAM_FUNC, (void));
extern GLCREATEPROGRAM_FUNC sglCreateProgram;

DEFINE_PROC_TYPE(GLuint, GLCREATESHADER_FUNC, (GLenum));
extern GLCREATESHADER_FUNC sglCreateShader;

//--D
DEFINE_PROC_TYPE(void, GLDELETEBUFFERS_FUNC, (GLsizei, const GLuint *));
extern GLDELETEBUFFERS_FUNC sglDeleteBuffers;

DEFINE_PROC_TYPE(void, GLDELETEPROGRAM_FUNC, (GLuint));
extern GLDELETEPROGRAM_FUNC sglDeleteProgram;

DEFINE_PROC_TYPE(void, GLDELETESHADER_FUNC, (GLuint));
extern GLDELETESHADER_FUNC sglDeleteShader;

DEFINE_PROC_TYPE(void, GLDELETEVERTEXARRAYS_FUNC, (GLsizei, const GLuint *));
extern GLDELETEVERTEXARRAYS_FUNC sglDeleteVertexArrays;

DEFINE_PROC_TYPE(void, GLDRAWBUFFERS_FUNC, (GLsizei, const GLenum *));
extern GLDRAWBUFFERS_FUNC sglDrawBuffers;

//--E
DEFINE_PROC_TYPE(void, GLENABLEVERTEXATTRIBARRAY_FUNC, (GLuint));
extern GLENABLEVERTEXATTRIBARRAY_FUNC sglEnableVertexAttribArray;

//--F
DEFINE_PROC_TYPE(void, GLFRAMEBUFFERTEXTURE_FUNC, (GLenum, GLenum, GLuint, GLint));
extern GLFRAMEBUFFERTEXTURE_FUNC sglFramebufferTexture;

//--G
DEFINE_PROC_TYPE(void, GLGENERATEMIPMAP_FUNC, (GLenum));
extern GLGENERATEMIPMAP_FUNC sglGenerateMipmap;

DEFINE_PROC_TYPE(void, GLGENBUFFERS_FUNC, (GLsizei, GLuint *));
extern GLGENBUFFERS_FUNC sglGenBuffers;

DEFINE_PROC_TYPE(void, GLGENFRAMEBUFFERS_FUNC, (GLsizei, GLuint *));
extern GLGENFRAMEBUFFERS_FUNC sglGenFramebuffers;

DEFINE_PROC_TYPE(void, GLGENVERTEXARRAYS_FUNC, (GLsizei, GLuint *));
extern GLGENVERTEXARRAYS_FUNC sglGenVertexArrays;

DEFINE_PROC_TYPE(void, GLGETSHADERIV_FUNC, (GLuint, GLenum, GLint *));
extern GLGETSHADERIV_FUNC sglGetShaderiv;

DEFINE_PROC_TYPE(void, GLGETSHADERINFOLOG_FUNC, (GLuint, GLsizei, GLsizei *, GLchar *));
extern GLGETSHADERINFOLOG_FUNC sglGetShaderInfoLog;

DEFINE_PROC_TYPE(void, GLGETPROGRAMIV_FUNC, (GLuint, GLenum, GLint *));
extern GLGETPROGRAMIV_FUNC sglGetProgramiv;

DEFINE_PROC_TYPE(void, GLGETPROGRAMINFOLOG_FUNC, (GLuint, GLsizei, GLsizei *, GLchar *));
extern GLGETPROGRAMINFOLOG_FUNC sglGetProgramInfoLog;

DEFINE_PROC_TYPE(GLint, GLGETUNIFORMLOCATION, (GLuint, const GLchar *));
extern GLGETUNIFORMLOCATION sglGetUniformLocation;

//--L
DEFINE_PROC_TYPE(void, GLLINKPROGRAM_FUNC, (GLuint));
extern GLLINKPROGRAM_FUNC sglLinkProgram;

//--M
DEFINE_PROC_TYPE(void, GLMULTITEXCOORD2F_FUNC, (GLenum, GLfloat, GLfloat));
extern GLMULTITEXCOORD2F_FUNC sglMultiTexCoord2f;

DEFINE_PROC_TYPE(void, GLMULTITEXCOORD3F_FUNC, (GLenum, GLfloat, GLfloat, GLfloat));
extern GLMULTITEXCOORD3F_FUNC sglMultiTexCoord3f;

//--S
DEFINE_PROC_TYPE(void, GLSHADERSOURCE_FUNC, (GLuint, GLsizei, const GLchar **, const GLint *));
extern GLSHADERSOURCE_FUNC sglShaderSource;

DEFINE_PROC_TYPE(void, WGLSWAPINTERVALEXT_FUNC, (GLint));
extern WGLSWAPINTERVALEXT_FUNC swglSwapIntervalEXT;

//--U
DEFINE_PROC_TYPE(void, GLUSEPROGRAM_FUNC, (GLuint));
extern GLUSEPROGRAM_FUNC sglUseProgram;

DEFINE_PROC_TYPE(void, GLUNIFORM4F, (GLint, GLfloat, GLfloat, GLfloat, GLfloat));
extern GLUNIFORM4F sglUniform4f;

DEFINE_PROC_TYPE(void, GLUNIFORM1I, (GLint, GLint));
extern GLUNIFORM1I sglUniform1i;

DEFINE_PROC_TYPE(void, GLUNIFORM1F, (GLint, GLfloat));
extern GLUNIFORM1F sglUniform1f;

//--V
DEFINE_PROC_TYPE(void, GLVERTEXATTRIBPOINTER_FUNC, (GLuint, GLint, GLenum, GLboolean, GLsizei, const GLvoid *));
extern GLVERTEXATTRIBPOINTER_FUNC sglVertexAttribPointer;

