//--Base
#include "InventorySubClass.h"

//--Classes
#include "InventoryItem.h"

//--CoreClasses
#include "SugarLinkedList.h"

//--Definitions
//--GUI
//--Libraries
//--Managers

//=========================================== System ==============================================
InventorySubClass::InventorySubClass()
{
    //--[RootObject]
    //--System

    //--[InventorySubClass]
    //--System
    //--Storage List
    mMaxInventorySize = 32;
    mStorageList = new SugarLinkedList(true);

    //--Public Variables
    mRequiresReconstitution = false;
}
InventorySubClass::~InventorySubClass()
{
    delete mStorageList;
}

//====================================== Property Queries =========================================
int InventorySubClass::GetInventorySize()
{
    return mStorageList->GetListSize();
}
int InventorySubClass::GetItemCount(const char *pItemName)
{
    //--Returns how many items in the inventory have the provided name.
    if(!pItemName) return 0;

    //--Count it.
    int tCount = 0;
    InventoryItem *rCheckPtr = (InventoryItem *)mStorageList->PushIterator();
    while(rCheckPtr)
    {
        if(!strcasecmp(rCheckPtr->GetName(), pItemName))
        {
            tCount ++;
        }
        rCheckPtr = (InventoryItem *)mStorageList->AutoIterate();
    }

    //--Done.
    return tCount;
}
int InventorySubClass::GetItemCountP(void *pPtr)
{
    //--Returns how many items in the inventory exactly match the given pointer. Used to verify if the
    //  item is still in this inventory (and to check for duplicates).
    if(!pPtr) return 0;

    //--Count it.
    int tCount = 0;
    InventoryItem *rCheckPtr = (InventoryItem *)mStorageList->PushIterator();
    while(rCheckPtr)
    {
        if(rCheckPtr == pPtr)
        {
            tCount ++;
        }
        rCheckPtr = (InventoryItem *)mStorageList->AutoIterate();
    }

    //--Done.
    return tCount;
}
InventoryItem *InventorySubClass::GetItemI(uint32_t pID)
{
    //--Error check.
    if(pID == 0) return NULL;

    //--Search.
    InventoryItem *rCheckPtr = (InventoryItem *)mStorageList->PushIterator();
    while(rCheckPtr)
    {
        if(rCheckPtr->GetID() == pID)
        {
            mStorageList->PopIterator();
            return rCheckPtr;
        }
        rCheckPtr = (InventoryItem *)mStorageList->AutoIterate();
    }

    //--Not found.
    return NULL;
}
InventoryItem *InventorySubClass::GetItemS(const char *pName)
{
    return (InventoryItem *)mStorageList->GetElementByName(pName);
}
InventoryItem *InventorySubClass::GetItemL(int pSlot)
{
    return (InventoryItem *)mStorageList->GetElementBySlot(pSlot);
}
int InventorySubClass::GetSlotOfItemS(const char *pName)
{
    return mStorageList->GetSlotOfElementByName(pName);
}
bool InventorySubClass::IsItemInList(void *pCheckPtr)
{
    return mStorageList->IsElementOnList(pCheckPtr);
}

//========================================= Manipulators ==========================================
uint8_t InventorySubClass::RegisterItem(InventoryItem *pItem)
{
    //--Registers the item (or not) and returns a code indicating success or failure. Items may
    //  be rejected if they are the wrong type or if the inventory is full.
    if(!pItem) return REJECTION_OTHER_ERROR;

    //--If the inventory is full, fail.
    if(mStorageList->GetListSize() >= mMaxInventorySize) return REJECTION_FULL;

    //--Register. Takes ownership of the item.
    mRequiresReconstitution = true;
    mStorageList->AddElementAsTail(pItem->GetName(), pItem, &RootObject::DeleteThis);
    return REJECTION_NONE;
}
InventoryItem *InventorySubClass::LiberateItemI(uint32_t pID)
{
    //--Remove and return the item with the matching unique ID.
    if(pID == 0) return NULL;
    InventoryItem *rCheckPtr = (InventoryItem *)mStorageList->PushIterator();
    while(rCheckPtr)
    {
        if(rCheckPtr->GetID() == pID)
        {
            mStorageList->PopIterator();
            mStorageList->RemoveElementP(rCheckPtr);
            mRequiresReconstitution = true;
            return rCheckPtr;
        }
        rCheckPtr = (InventoryItem *)mStorageList->AutoIterate();
    }

    //--Not found.
    return NULL;
}
InventoryItem *InventorySubClass::LiberateItemS(const char *pName)
{
    //--Remove and return the item with the matching name. First found match gets removed.
    if(!pName) return NULL;
    InventoryItem *rCheckPtr = (InventoryItem *)mStorageList->PushIterator();
    while(rCheckPtr)
    {
        if(!strcmp(pName, rCheckPtr->GetName()))
        {
            mStorageList->PopIterator();
            mStorageList->RemoveElementP(rCheckPtr);
            mRequiresReconstitution = true;
            return rCheckPtr;
        }
        rCheckPtr = (InventoryItem *)mStorageList->AutoIterate();
    }

    //--Not found.
    return NULL;
}
InventoryItem *InventorySubClass::LiberateItemL(int pSlot)
{
    //--Remove and return the item in the matching slot. May remove NULL if the slot is empty.
    if(pSlot < 0 || pSlot >= mMaxInventorySize) return NULL;
    mStorageList->SetDeallocation(false);
    InventoryItem *rItem = (InventoryItem *)mStorageList->RemoveElementI(pSlot);
    mStorageList->SetDeallocation(true);
    mRequiresReconstitution = true;
    return rItem;
}
void InventorySubClass::LiberateItemP(void *pPtr)
{
    //--Remove the item with the matching pointer. doesn't return it, unlike the other Liberate functions,
    //  because... you already have it. Duh.
    if(!pPtr) return;
    mStorageList->SetDeallocation(false);
    mStorageList->RemoveElementP(pPtr);
    mStorageList->SetDeallocation(true);
    mRequiresReconstitution = true;
}
void InventorySubClass::RemoveItemI(uint32_t pID)
{
    //--Remove and delete the item with the matching unique ID.
    if(pID == 0) return;

    //--Loop.
    InventoryItem *rCheckPtr = (InventoryItem *)mStorageList->PushIterator();
    while(rCheckPtr)
    {
        if(rCheckPtr->GetID() == pID)
        {
            mStorageList->PopIterator();
            mStorageList->RemoveElementP(rCheckPtr);
            mRequiresReconstitution = true;
            return;
        }
        rCheckPtr = (InventoryItem *)mStorageList->AutoIterate();
    }
}
void InventorySubClass::RemoveItemS(const char *pName)
{
    //--Remove and delete the item with the matching name. First found match gets removed.
    mStorageList->RemoveElementS(pName);
    mRequiresReconstitution = true;
}
void InventorySubClass::RemoveItemL(int pSlot)
{
    //--Remove and delete the item in the matching slot. May remove NULL if the slot is empty.
    mStorageList->RemoveElementI(pSlot);
    mRequiresReconstitution = true;
}

//========================================= Core Methods ==========================================
bool InventorySubClass::PurgeMarkedItems()
{
    //--Returns true if it did anything.
    bool tDidAnything = false;

    //--Loop.
    InventoryItem *rCheckPtr = (InventoryItem *)mStorageList->SetToHeadAndReturn();
    while(rCheckPtr)
    {
        if(rCheckPtr->mIsSelfDestructing || rCheckPtr->mIsConsumed)
        {
            tDidAnything = true;
            mStorageList->RemoveRandomPointerEntry();
        }
        rCheckPtr = (InventoryItem *)mStorageList->IncrementAndGetRandomPointerEntry();
    }

    //--Mark for reconstitution.
    mRequiresReconstitution |= tDidAnything;
    return tDidAnything;
}

//===================================== Private Core Methods ======================================
//============================================ Update =============================================
//=========================================== File I/O ============================================
//=========================================== Drawing =============================================
//======================================= Pointer Routing =========================================
//====================================== Static Functions =========================================
//========================================= Lua Hooking ===========================================
//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
