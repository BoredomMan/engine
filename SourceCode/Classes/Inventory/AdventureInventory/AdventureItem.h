//--[AdventureItem]
//--Represents an Item in Adventure Mode, which may be one of a consumable, key item, equipment, or overworld trap.
//  Some items can be used in multiple roles.

#pragma once

//--[Includes]
#include "Definitions.h"
#include "Structures.h"
#include "RootObject.h"
#include "AdvCombatDefStruct.h"

//--[Local Structures]
//--[Local Definitions]

//--Upgrade item types
#ifndef _ADAM_TYPES_
#define _ADAM_TYPES_
#define CRAFT_ADAMANTITE_POWDER 0
#define CRAFT_ADAMANTITE_FLAKES 1
#define CRAFT_ADAMANTITE_SHARD 2
#define CRAFT_ADAMANTITE_PIECE 3
#define CRAFT_ADAMANTITE_CHUNK 4
#define CRAFT_ADAMANTITE_ORE 5
#define CRAFT_ADAMANTITE_TOTAL 6
#endif

//--Property Line Flags
#define ADITEM_PROPERTY_NONE -1
#define ADITEM_PROPERTY_INVENTORY 0
#define ADITEM_PROPERTY_EQUIPSCREEN 1

//--Upgrade Storage
#define ADITEM_MAX_UPGRADES 12
#define ADITEM_MAX_GEMS 6

//--[Classes]
class AdventureItem : public RootObject
{
    private:
    //--System
    bool mIsAdamantite;
    bool mIsStackable;
    int mStackSize;
    uint32_t mItemUniqueID;
    char *mLocalName;
    char *mDescription;
    char *mTopNameLine;
    char *mBotNameLine;
    bool mHasOverrideQuality;
    float mOverrideQuality;

    //--Property Lines
    int mLastResolveFlag;
    int mPropertyLinesTotal;
    char **mPropertyLines;
    char mInternalType[STD_MAX_LETTERS];

    //--Value
    int mValue;

    //--Consumable
    bool mIsConsumable;

    //--Upgrading
    bool mIsUpgradeable;
    int8_t mUpgradeCodes[ADITEM_MAX_UPGRADES];

    //--Gem Slots
    int mGemSlotsTotal;
    AdventureItem *mGemSlots[ADITEM_MAX_GEMS];

    //--Key Item
    bool mIsUnique;
    bool mIsKeyItem;

    //--Equipment
    bool mIsEquipment;
    char mEquipSlot[EQP_TYPE_MAX_LETTERS];
    SugarLinkedList *mEquippableList;

    //--Equipment Boosts
    int mHealthBoost;
    int mDamageBoost;
    int mProtectionBoost;
    int mAccuracyBoost;
    int mEvasionBoost;
    int mInitiativeBoost;

    //--Combat Items
    bool mNeverRegenerateCharges;
    bool mItemRegeneratesEachBattle;
    int mCooldownRemaining;
    int mItemCooldown;
    int mCharges;
    int mChargesMax;
    AdvCombatAbility *mUseAbility;

    //--Rendering
    SugarBitmap *rIconImg;

    protected:

    public:
    //--System
    AdventureItem();
    virtual ~AdventureItem();

    //--Public Variables
    bool mGemErrorFlag;
    AdventureItem *rGemParent;
    AdvCombatEntity *rEquippingEntity;
    static int xItemUniqueIDCounter;

    //--Comparison Lines
    static char *xComparisonItemA;
    static char *xComparisonItemB;
    static int xComparisonLinesTotal;
    static char **xComparisonLines;

    //--Property Queries
    bool IsAdamantite();
    bool IsStackable();
    int GetStackSize();
    uint32_t GetItemUniqueID();
    const char *GetName();
    const char *GetTopName();
    const char *GetBotName();
    const char *GetDescription();
    int GetModifier(int pFlag);
    int GetPropertyLinesTotal();
    const char *GetPropertyLine(int pIndex);
    int GetPropertyByInventoryHeaders(int pHeader, bool pDisallowGems);
    int GetGemBoostPropertyByInventoryHeaders(int pHeader);
    int GetValue();
    bool IsUpgradeable();
    int GetUpgradesTotal();
    int8_t GetUpgradeCode(int pSlot);
    int GetGemSlots();
    bool IsUnique();
    bool IsKeyItem();
    SugarBitmap *GetIconImage();
    const char *GetItemTypeString();

    //--Manipulators
    void SetAdamantite();
    void SetStackable(bool pIsStackable);
    void SetStackSize(int pSize);
    void OverrideID(uint32_t pID);
    void SetName(const char *pName);
    void SetNameLines(const char *pTop, const char *pBot);
    void SetDescription(const char *pDescription);
    void SetValue(int pValue);
    void SetUnique(bool pFlag);
    void SetKeyItem(bool pFlag);
    void SetConsumable(bool pFlag);
    void SetUpgradeable(bool pFlag);
    void SetEquipmentFlag(bool pFlag);
    void SetIconImage(const char *pPath);
    AdventureItem *PlaceGemInSlot(int pSlot, AdventureItem *pGem);
    AdventureItem *RemoveGemFromSlot(int pSlot);
    void AddItemToProperties(AdventureItem *pItem);
    void RemoveItemFromProperties(AdventureItem *pItem);
    void SetOverrideQuality(float pQuality);

    //--Equipment Items
    bool IsEquipment();
    bool IsEquippableBy(const char *pName);
    const char *GetEquipSlot();
    void AddEquippableCharacer(const char *pName);
    void SetEquipSlot(const char *pSlotName);
    void SetGemCap(int pGemCap);
    float ComputeQuality();

    //--Equipment Boosts
    int GetHealthBoost();
    int GetHealthBoostNoGems();
    int GetDamageBoostNoGems();
    int GetProtectionBoostNoGems();
    int GetAccuracyBoostNoGems();
    int GetEvasionBoostNoGems();
    int GetInitiativeBoostNoGems();
    int GetEquipmentBonusNoGems(int pType);
    int GetHealthBoostFromGems();
    int GetDamageBoostFromGems();
    int GetProtectionBoostFromGems();
    int GetAccuracyBoostFromGems();
    int GetEvasionBoostFromGems();
    int GetInitiativeBoostFromGems();
    int GetEquipmentBonusFromGems(int pType);
    void SetHealthBoost(int pAmount);
    void SetDamageBoost(int pAmount);
    void SetProtectionBoost(int pAmount);
    void SetAccuracyBoost(int pAmount);
    void SetEvasionBoost(int pAmount);
    void SetInitiativeBoost(int pAmount);
    void SetEquipmentBonus(int pType, int pAmount);

    //--Combat Items
    bool RegeneratesChargesAfterCombat();
    bool IsCooldownReady();
    bool CanBeUsedInCombatRightNow();
    int GetCharges();
    int GetChargesMax();
    int GetCurrentCooldown();
    void SetCharges(int pCount);
    void SetMaxCharges(int pCount);
    void SetNeverRechargeFlag(bool pFlag);
    void SetRechargeEveryBattleFlag(bool pFlag);
    void SetCooldown(int pCooldown);
    void RegisterAbility(AdvCombatAbility *pAbility);
    void RefreshCharges();
    void BeginTurn();
    void ClearCooldown();
    void ConsumeCombatAction();

    //--Core Methods
    void ResolvePropertyLines(int pFlag);
    void ResolveComparisonLines(AdventureItem *pComparisonItem);
    void ResolveGemCoordinates(int pGemSlot, SugarBitmap *pGemIndicator, float &sLft, float &sTop, float &sRgt, float &sBot);
    AdventureItem *Clone();

    private:
    //--Private Core Methods
    public:
    //--Update
    //--File I/O
    //--Drawing
    //--Pointer Routing
    AdvCombatAbility *GetCombatAbility();
    AdventureItem *GetGemInSlot(int pSlot);

    //--Static Functions
    static void InitGemcutterCosts();

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_AdItem_GetProperty(lua_State *L);
int Hook_AdItem_SetProperty(lua_State *L);
