//--Base
#include "AdventureItem.h"

//--Classes
//--CoreClasses
//--Definitions
//--Libraries
//--Managers

//--[Boost Getters]
int AdventureItem::GetHealthBoost()
{
    return mHealthBoost;
}

//--[Boost Getters w/out Gems]
int AdventureItem::GetHealthBoostNoGems()
{
    int tBoost = mHealthBoost;
    for(int i = 0; i < mGemSlotsTotal; i ++)
    {
        if(mGemSlots[i]) { tBoost -= mGemSlots[i]->mHealthBoost; }
    }
    return tBoost;
}
int AdventureItem::GetDamageBoostNoGems()
{
    int tBoost = mDamageBoost;
    for(int i = 0; i < mGemSlotsTotal; i ++)
    {
        if(mGemSlots[i]) { tBoost -= mGemSlots[i]->mDamageBoost; }
    }
    return tBoost;
}
int AdventureItem::GetProtectionBoostNoGems()
{
    int tBoost = mProtectionBoost;
    for(int i = 0; i < mGemSlotsTotal; i ++)
    {
        if(mGemSlots[i]) { tBoost -= mGemSlots[i]->mProtectionBoost; }
    }
    return tBoost;
}
int AdventureItem::GetAccuracyBoostNoGems()
{
    int tBoost = mAccuracyBoost;
    for(int i = 0; i < mGemSlotsTotal; i ++)
    {
        if(mGemSlots[i]) { tBoost -= mGemSlots[i]->mAccuracyBoost; }
    }
    return tBoost;
}
int AdventureItem::GetEvasionBoostNoGems()
{
    int tBoost = mEvasionBoost;
    for(int i = 0; i < mGemSlotsTotal; i ++)
    {
        if(mGemSlots[i]) { tBoost -= mGemSlots[i]->mEvasionBoost; }
    }
    return tBoost;
}
int AdventureItem::GetInitiativeBoostNoGems()
{
    int tBoost = mInitiativeBoost;
    for(int i = 0; i < mGemSlotsTotal; i ++)
    {
        if(mGemSlots[i]) { tBoost -= mGemSlots[i]->mInitiativeBoost; }
    }
    return tBoost;
}
int AdventureItem::GetEquipmentBonusNoGems(int pType)
{
    return 0;
}

//--[Boost Getters from Gems]
//--Computes only the gem bonus to a given stat. Cumulative, doesn't care about specific gems.
int AdventureItem::GetHealthBoostFromGems()
{
    int tBoost = 0;
    for(int i = 0; i < mGemSlotsTotal; i ++)
    {
        if(mGemSlots[i]) { tBoost += mGemSlots[i]->mHealthBoost; }
    }
    return tBoost;
}
int AdventureItem::GetDamageBoostFromGems()
{
    int tBoost = 0;
    for(int i = 0; i < mGemSlotsTotal; i ++)
    {
        if(mGemSlots[i]) { tBoost += mGemSlots[i]->mDamageBoost; }
    }
    return tBoost;
}
int AdventureItem::GetProtectionBoostFromGems()
{
    int tBoost = 0;
    for(int i = 0; i < mGemSlotsTotal; i ++)
    {
        if(mGemSlots[i]) { tBoost += mGemSlots[i]->mProtectionBoost; }
    }
    return tBoost;
}
int AdventureItem::GetAccuracyBoostFromGems()
{
    int tBoost = 0;
    for(int i = 0; i < mGemSlotsTotal; i ++)
    {
        if(mGemSlots[i]) { tBoost += mGemSlots[i]->mAccuracyBoost; }
    }
    return tBoost;
}
int AdventureItem::GetEvasionBoostFromGems()
{
    int tBoost = 0;
    for(int i = 0; i < mGemSlotsTotal; i ++)
    {
        if(mGemSlots[i]) { tBoost += mGemSlots[i]->mEvasionBoost; }
    }
    return tBoost;
}
int AdventureItem::GetInitiativeBoostFromGems()
{
    int tBoost = 0;
    for(int i = 0; i < mGemSlotsTotal; i ++)
    {
        if(mGemSlots[i]) { tBoost += mGemSlots[i]->mInitiativeBoost; }
    }
    return tBoost;
}
int AdventureItem::GetEquipmentBonusFromGems(int pType)
{
    return 0;
}

//--[Boost Setters]
void AdventureItem::SetHealthBoost(int pAmount)
{
    mHealthBoost = pAmount;
}
void AdventureItem::SetDamageBoost(int pAmount)
{
    mDamageBoost = pAmount;
}
void AdventureItem::SetProtectionBoost(int pAmount)
{
    mProtectionBoost = pAmount;
}
void AdventureItem::SetAccuracyBoost(int pAmount)
{
    mAccuracyBoost = pAmount;
}
void AdventureItem::SetEvasionBoost(int pAmount)
{
    mEvasionBoost = pAmount;
}
void AdventureItem::SetInitiativeBoost(int pAmount)
{
    mInitiativeBoost = pAmount;
}
void AdventureItem::SetEquipmentBonus(int pType, int pAmount)
{
}
