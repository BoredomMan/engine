//--Base
#include "AdventureItem.h"

//--Classes
#include "AdvCombatAbility.h"
#include "AdvCombatEntity.h"
#include "AdventureInventory.h"
#include "AdventureMenu.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarLinkedList.h"

//--Definitions
//--Libraries
#include "DataLibrary.h"

//--Managers

//=========================================== System ==============================================
AdventureItem::AdventureItem()
{
    //--[RootObject]
    //--System
    mType = POINTER_TYPE_ADVENTUREITEM;

    //--[AdventureItem]
    //--System
    mIsAdamantite = false;
    mIsStackable = false;
    mStackSize = 1;
    mItemUniqueID = xItemUniqueIDCounter;
    xItemUniqueIDCounter ++;
    mLocalName = InitializeString("AdventureItem");
    mTopNameLine = InitializeString("AdventureItem");
    mBotNameLine = NULL;
    mLastResolveFlag = ADITEM_PROPERTY_NONE;
    mDescription = NULL;
    mPropertyLinesTotal = 0;
    mPropertyLines = NULL;
    mHasOverrideQuality = false;
    mOverrideQuality = 0.0f;
    mInternalType[0] = '\0';

    //--Value
    mValue = 0;

    //--Consumable
    mIsConsumable = false;

    //--Upgrading
    mIsUpgradeable = false;
    for(int i = 0; i < ADITEM_MAX_UPGRADES; i ++) mUpgradeCodes[i] = -1;

    //--Gem Slots
    mGemSlotsTotal = 0;
    memset(mGemSlots, 0, sizeof(AdventureItem *) * ADITEM_MAX_GEMS);

    //--Key Item
    mIsUnique = false;
    mIsKeyItem = false;

    //--Equipment
    mIsEquipment = false;
    strcpy(mEquipSlot, "None");
    mEquippableList = NULL;

    //--Equipment Boosts
    mHealthBoost = 0;
    mDamageBoost = 0;
    mProtectionBoost = 0;
    mAccuracyBoost = 0;
    mEvasionBoost = 0;
    mInitiativeBoost = 0;

    //--Combat Items
    mNeverRegenerateCharges = false;
    mItemRegeneratesEachBattle = false;
    mCooldownRemaining = 0;
    mItemCooldown = 0;
    mCharges = 0;
    mChargesMax = 0;
    mUseAbility = NULL;

    //--Rendering
    rIconImg = NULL;

    //--Public Variables
    mGemErrorFlag = false;
    rGemParent = NULL;
    rEquippingEntity = NULL;
}
AdventureItem::~AdventureItem()
{
    free(mLocalName);
    free(mTopNameLine);
    free(mBotNameLine);
    free(mDescription);
    delete mEquippableList;
    delete mUseAbility;
    for(int i = 0; i < mPropertyLinesTotal; i ++) free(mPropertyLines[i]);
    free(mPropertyLines);
    for(int i = 0; i < ADITEM_MAX_GEMS; i ++) delete mGemSlots[i];
}

//--[Public Statics]
//--Unique ID counter for items. All items in the game get an ID unique to them, which is used to
//  track their upgrade state. The ID values can never be 0.
int AdventureItem::xItemUniqueIDCounter = 1;

//--Comparison Lines. These lines are used when displaying comparisons between two items in the equipment
//  or shop screen. The names provided are the names of the comparing items, and the lines are not rebuilt
//  if both of the items are identical. Otherwise, building them is similar to building property lines.
char *AdventureItem::xComparisonItemA = NULL;
char *AdventureItem::xComparisonItemB = NULL;
int AdventureItem::xComparisonLinesTotal = 0;
char **AdventureItem::xComparisonLines = NULL;

//====================================== Property Queries =========================================
bool AdventureItem::IsAdamantite()
{
    return mIsAdamantite;
}
bool AdventureItem::IsStackable()
{
    return mIsStackable;
}
int AdventureItem::GetStackSize()
{
    if(!mIsStackable) return 1;
    return mStackSize;
}
uint32_t AdventureItem::GetItemUniqueID()
{
    return mItemUniqueID;
}
const char *AdventureItem::GetName()
{
    return (const char *)mLocalName;
}
const char *AdventureItem::GetTopName()
{
    return (const char *)mTopNameLine;
}
const char *AdventureItem::GetBotName()
{
    return (const char *)mBotNameLine;
}
const char *AdventureItem::GetDescription()
{
    return (const char *)mDescription;
}
int AdventureItem::GetModifier(int pFlag)
{
    //--Uses the same set as ActionEffectPacks.
    if(pFlag == ACA_EFFECT_TERRIFY) return 0;
    if(pFlag == ACA_EFFECT_POISON) return 0;
    if(pFlag == ACA_EFFECT_BLEED) return 0;
    if(pFlag == ACA_EFFECT_BLIND) return 0;
    if(pFlag == ACA_EFFECT_CORRODE) return 0;
    if(pFlag == ACA_EFFECT_STUN) return 0;
    if(pFlag == ACA_MODIFIER_DAMAGEFIXED) return mDamageBoost;
    if(pFlag == ACA_MODIFIER_PROTECTION) return mProtectionBoost;
    if(pFlag == ACA_MODIFIER_ACCURACY) return mAccuracyBoost;
    if(pFlag == ACA_MODIFIER_EVADE) return mEvasionBoost;
    if(pFlag == ACA_MODIFIER_INITIATIVE) return mInitiativeBoost;

    //--Unhandled case.
    return 0;
}
int AdventureItem::GetPropertyLinesTotal()
{
    return mPropertyLinesTotal;
}
const char *AdventureItem::GetPropertyLine(int pIndex)
{
    if(pIndex < 0 || pIndex >= mPropertyLinesTotal) return NULL;
    return mPropertyLines[pIndex];
}
int AdventureItem::GetPropertyByInventoryHeaders(int pHeader, bool pDisallowGems)
{
    return 0;
}
int AdventureItem::GetGemBoostPropertyByInventoryHeaders(int pHeader)
{
    return 0;
}
int AdventureItem::GetValue()
{
    return mValue;
}
bool AdventureItem::IsUpgradeable()
{
    return mIsUpgradeable;
}
int AdventureItem::GetUpgradesTotal()
{
    for(int i = 0; i < ADITEM_MAX_UPGRADES; i ++)
    {
        if(mUpgradeCodes[i] == -1) return i;
    }
    return ADITEM_MAX_UPGRADES;
}
int8_t AdventureItem::GetUpgradeCode(int pSlot)
{
    if(pSlot < 0 || pSlot >= ADITEM_MAX_UPGRADES) return -1;
    return mUpgradeCodes[pSlot];
}
int AdventureItem::GetGemSlots()
{
    return mGemSlotsTotal;
}
bool AdventureItem::IsUnique()
{
    return mIsUnique;
}
bool AdventureItem::IsKeyItem()
{
    return mIsKeyItem;
}
SugarBitmap *AdventureItem::GetIconImage()
{
    return rIconImg;
}
const char *AdventureItem::GetItemTypeString()
{
    return mInternalType;
}

//========================================= Manipulators ==========================================
void AdventureItem::SetAdamantite()
{
    mIsAdamantite = true;
    strcpy(mInternalType, "Crafting Item");
}
void AdventureItem::SetStackable(bool pIsStackable)
{
    mIsStackable = pIsStackable;
}
void AdventureItem::SetStackSize(int pSize)
{
    mStackSize = pSize;
    if(mStackSize < 1) mStackSize = 1;
}
void AdventureItem::OverrideID(uint32_t pID)
{
    //--Note: Should only be used when loading a save file!
    mItemUniqueID = pID;
}
void AdventureItem::SetName(const char *pName)
{
    //--Error check.
    if(!pName) return;

    //--Base name.
    ResetString(mLocalName, pName);
    //fprintf(stderr, "Setting name %s\n", mLocalName);

    //--Look for spaces.
    int tSpaceIsOn = -1;
    int tLen = (int)strlen(mLocalName);
    for(int i = 0; i < tLen-1; i ++)
    {
        //--Not a space, ignore.
        if(mLocalName[i] != ' ') continue;

        //--Found a space. Is it the second one? If so, fail. Too many spaces means the names
        //  need to be set manually.
        if(tSpaceIsOn != -1)
        {
            ResetString(mTopNameLine, pName);
            ResetString(mBotNameLine, NULL);
            //fprintf(stderr, " Discarded.\n");
            return;
        }

        //--First space we've found. Split the string here. First, clean up.
        tSpaceIsOn = i;
        ResetString(mTopNameLine, NULL);
        ResetString(mBotNameLine, NULL);

        //--Allocate space for the top line.
        SetMemoryData(__FILE__, __LINE__);
        mTopNameLine = (char *)starmemoryalloc(sizeof(char) * (i+1));
        strncpy(mTopNameLine, mLocalName, i);
        mTopNameLine[i] = '\0';

        //--Bottom line is simpler.
        ResetString(mBotNameLine, &mLocalName[i+1]);
        //fprintf(stderr, " Splitter: %s - |%s|%s|\n", mLocalName, mTopNameLine, mBotNameLine);
    }

    //--If no spaces get found, the top name is the same as the local name.
    if(tSpaceIsOn == -1)
    {
        ResetString(mTopNameLine, mLocalName);
    }
}
void AdventureItem::SetNameLines(const char *pTop, const char *pBot)
{
    if(pTop && strcasecmp(pTop, "Null"))
    {
        ResetString(mTopNameLine, pTop);
    }
    else
    {
        ResetString(mTopNameLine, NULL);
    }
    if(pBot && strcasecmp(pBot, "Null"))
    {
        ResetString(mBotNameLine, pBot);
    }
    else
    {
        ResetString(mBotNameLine, NULL);
    }
}
void AdventureItem::SetDescription(const char *pDescription)
{
    ResetString(mDescription, pDescription);
}
void AdventureItem::SetValue(int pValue)
{
    mValue = pValue;
}
void AdventureItem::SetUnique(bool pFlag)
{
    mIsUnique = pFlag;
}
void AdventureItem::SetKeyItem(bool pFlag)
{
    mIsKeyItem = pFlag;
}
void AdventureItem::SetConsumable(bool pFlag)
{
    mIsConsumable = pFlag;
}
void AdventureItem::SetUpgradeable(bool pFlag)
{
    mIsUpgradeable = pFlag;
    strcpy(mInternalType, "Gemstone");
}
void AdventureItem::SetEquipmentFlag(bool pFlag)
{
    //--Store the old flag.
    bool tOldFlag = mIsEquipment;

    //--Set the new flag.
    mIsEquipment = pFlag;

    //--Initialization case:
    if(!tOldFlag && mIsEquipment)
    {
        mEquippableList = new SugarLinkedList(false);
    }
    //--Dealloc case:
    else
    {
        delete mEquippableList;
        mEquippableList = NULL;
    }
}
void AdventureItem::SetIconImage(const char *pPath)
{
    rIconImg = (SugarBitmap *)DataLibrary::Fetch()->GetEntry(pPath);
}
AdventureItem *AdventureItem::PlaceGemInSlot(int pSlot, AdventureItem *pGem)
{
    //--Removes the existing gem from the slot and passes it back, replacing it with the new
    //  gem. Takes ownership of the new gem, and the caller takes ownership of the returned gem.
    //--If the mGemErrorFlag is set to true, something went wrong and *ownership does not change*.
    //--It is legal to pass NULL in to remove a gem. It is also legal to receive NULL back
    //  if the gem slot was empty.
    mGemErrorFlag = false;

    //--Range check.
    if(pSlot < 0 || pSlot >= ADITEM_MAX_GEMS) { mGemErrorFlag = true; return NULL; }
    if(pSlot >= mGemSlotsTotal) { mGemErrorFlag = true; return NULL; }

    //--Take the current gem in the slot out. It can be NULL.
    AdventureItem *rPreviousGem = mGemSlots[pSlot];
    mGemSlots[pSlot] = pGem;

    //--If the previous gem exists, remove its parent point.
    if(rPreviousGem)
    {
        RemoveItemFromProperties(rPreviousGem);
        rPreviousGem->rGemParent = NULL;
    }

    //--If the new gem exists, set us as its parent.
    if(mGemSlots[pSlot])
    {
        AddItemToProperties(mGemSlots[pSlot]);
        mGemSlots[pSlot]->rGemParent = this;
    }

    //--Pass back the previous gem. It can be NULL, in which case the slot was empty.
    return rPreviousGem;
}
AdventureItem *AdventureItem::RemoveGemFromSlot(int pSlot)
{
    //--Removes the gem from the slot. Just calls PlaceGemInSlot() with NULL. Error flag rules
    //  are the same as before.
    return PlaceGemInSlot(pSlot, NULL);
}
void AdventureItem::AddItemToProperties(AdventureItem *pItem)
{
    //--When an item is added to a socket, this function modifies the state of the caller to add
    //  the newly socketed item's properties together. This means the parent gains the health, damage,
    //  accuracy, evade, etc of the socketed item.
    //--When the item is removed, RemoveItemFromProperties() should be called.
    //--When upgrading, call the remove and then re-add the item.
    if(!pItem) return;

    //--Flag to mark this for property re-resolve.
    mLastResolveFlag = ADITEM_PROPERTY_NONE;

    //--Basics.
    mHealthBoost += pItem->mHealthBoost;
    mDamageBoost += pItem->mDamageBoost;
    mProtectionBoost += pItem->mProtectionBoost;
    mAccuracyBoost += pItem->mAccuracyBoost;
    mEvasionBoost += pItem->mEvasionBoost;
    mInitiativeBoost += pItem->mInitiativeBoost;
}
void AdventureItem::RemoveItemFromProperties(AdventureItem *pItem)
{
    //--Reverse of AddItemToProperties(). Used when unsocketing something.
    if(!pItem) return;

    //--Flag to mark this for property re-resolve.
    mLastResolveFlag = ADITEM_PROPERTY_NONE;

    //--Basics.
    mHealthBoost -= pItem->mHealthBoost;
    mDamageBoost -= pItem->mDamageBoost;
    mProtectionBoost -= pItem->mProtectionBoost;
    mAccuracyBoost -= pItem->mAccuracyBoost;
    mEvasionBoost -= pItem->mEvasionBoost;
    mInitiativeBoost -= pItem->mInitiativeBoost;
}
void AdventureItem::SetOverrideQuality(float pQuality)
{
    mHasOverrideQuality = pQuality;
    mOverrideQuality = pQuality;
}

//======================================== Equipment Items ========================================
//--[Property Queries]
bool AdventureItem::IsEquipment()
{
    return mIsEquipment;
}
bool AdventureItem::IsEquippableBy(const char *pName)
{
    if(!mEquippableList) return false;
    return (mEquippableList->GetElementByName(pName) != NULL);
}
const char *AdventureItem::GetEquipSlot()
{
    return mEquipSlot;
}

//--[Manipulators]
void AdventureItem::AddEquippableCharacer(const char *pName)
{
    static int xDummyVar;
    if(!pName || !mEquippableList || mEquippableList->GetElementByName(pName) != NULL) return;
    mEquippableList->AddElement(pName, &xDummyVar);
}
void AdventureItem::SetEquipSlot(const char *pSlotName)
{
    strncpy(mEquipSlot, pSlotName, sizeof(mEquipSlot));
}
void AdventureItem::SetGemCap(int pGemCap)
{
    mGemSlotsTotal = pGemCap;
    if(mGemSlotsTotal < 1) mGemSlotsTotal = 0;
    if(mGemSlotsTotal > ADITEM_MAX_GEMS) mGemSlotsTotal = ADITEM_MAX_GEMS;
}
float AdventureItem::ComputeQuality()
{
    //--Computes an abstract 'quality' value. The value is used in sorting items which otherwise
    //  can't really be fairly compared. The value itself doesn't actually mean anything in particular,
    //  and importantly, does not map onto the platina cost of the item.
    //--Many types of items return a quality of 0.0 inherently. Equipment and gems are the most common
    //  users of quality. Quality can also be manually overridden by scripts.
    if(mHasOverrideQuality) return mOverrideQuality;

    //--Setup.
    float tReturnVal = 0.0f;

    //--Equipment/Gems.
    if(mIsEquipment || mIsUpgradeable)
    {
        tReturnVal = tReturnVal + (mHealthBoost * 0.10f);
        tReturnVal = tReturnVal + (mDamageBoost * 0.33f);
        tReturnVal = tReturnVal + (mProtectionBoost * 0.33f);
        tReturnVal = tReturnVal + (mAccuracyBoost * 0.15f);
        tReturnVal = tReturnVal + (mEvasionBoost * 0.15f);
        tReturnVal = tReturnVal + (mInitiativeBoost * 0.50f);

        //-Upgrades count as 5 points in addition to whatever the stat improvements were.
        tReturnVal = tReturnVal + (GetUpgradesTotal() * 5.0f);
    }


    //--Finish up.
    return tReturnVal;
}

//========================================= Combat Items ==========================================
//--[Property Queries]
bool AdventureItem::RegeneratesChargesAfterCombat()
{
    return mItemRegeneratesEachBattle;
}
bool AdventureItem::IsCooldownReady()
{
    return (mCooldownRemaining < 1);
}
bool AdventureItem::CanBeUsedInCombatRightNow()
{
    //--Resolves whether or not the given item can be used. If it has charges it must have one charge.
    //  It must also not be on cooldown.
    if(!mUseAbility) return false;
    if(mCooldownRemaining > 0) return false;
    if(mChargesMax > 0 && mCharges < 1) return false;

    //--All checks passed.
    return true;
}
int AdventureItem::GetCharges()
{
    return mCharges;
}
int AdventureItem::GetChargesMax()
{
    return mChargesMax;
}
int AdventureItem::GetCurrentCooldown()
{
    return mCooldownRemaining;
}

//--[Manipulators]
void AdventureItem::SetCharges(int pCount)
{
    mCharges = pCount;
    if(mCharges < 0) mCharges = 0;
    if(mCharges > mChargesMax) mCharges = mChargesMax;
}
void AdventureItem::SetMaxCharges(int pCount)
{
    mChargesMax = pCount;
    if(mChargesMax < 1) mChargesMax = 1;
    mCharges = mChargesMax;
}
void AdventureItem::SetNeverRechargeFlag(bool pFlag)
{
    mNeverRegenerateCharges = pFlag;
}
void AdventureItem::SetRechargeEveryBattleFlag(bool pFlag)
{
    mItemRegeneratesEachBattle = pFlag;
}
void AdventureItem::SetCooldown(int pCooldown)
{
    mItemCooldown = pCooldown;
}
void AdventureItem::RegisterAbility(AdvCombatAbility *pAbility)
{
    //--Pass NULL to clear.
    delete mUseAbility;
    mUseAbility = pAbility;
}

//--[Core Methods]
void AdventureItem::RefreshCharges()
{
    //--Resets the charge counter on the item, but only if the item is flagged to do so. Some items
    //  never regenerate charges, most regenerate them when the party rests.
    if(mNeverRegenerateCharges) return;
    mCharges = mChargesMax;
}
void AdventureItem::BeginTurn()
{
    //--Call at the beginning of a turn. Drops cooldown.
    if(mCooldownRemaining > 0) mCooldownRemaining --;
}
void AdventureItem::ClearCooldown()
{
    mCooldownRemaining = 0;
}
void AdventureItem::ConsumeCombatAction()
{
    //--Consumes a charge, sets the cooldown, or removes the item. Depends on what the item's flags are,
    //  but this is called instead of combat manually setting values.
    if(mChargesMax > 0)
    {
        mCharges --;
        if(mCharges < 1) mCharges = 0;
    }

    //--Cooldown.
    if(mItemCooldown > 0)
    {
        mCooldownRemaining = mItemCooldown;
    }
}

//========================================= Core Methods ==========================================
void AdventureItem::ResolvePropertyLines(int pFlag)
{
    //--Creates a list of all property lines and saves them in a fixed-size array. Does nothing if
    //  the list has already been resolved. Used in the items menu and equipment menu.
    //--The provided flag switches between types of properties required. Equipment screen does not
    //  need to tell the player what equipment type it is or who can use it.
    if(mPropertyLinesTotal > 0 && pFlag == mLastResolveFlag && mLastResolveFlag != ADITEM_PROPERTY_NONE) return;

    //--Clear existing property lines, if any.
    for(int i = 0; i < mPropertyLinesTotal; i ++) free(mPropertyLines[i]);
    free(mPropertyLines);
    mPropertyLines = NULL;

    //--Store the last-used flag.
    mLastResolveFlag = pFlag;

    //--List to store the lines.
    SugarLinkedList *tLineList = new SugarLinkedList(false);

    //--Equippable case.
    if(mIsEquipment || mIsUpgradeable)
    {
        //--Equip Properties
        int mIniBoost = mInitiativeBoost;
        int mEvdBoost = mEvasionBoost;
        int mAccBoost = mAccuracyBoost;
        if(mHealthBoost != 0)     tLineList->AddElement("X", InitializeString("[IMG|HP18] +%i",     mHealthBoost));
        if(mDamageBoost != 0)     tLineList->AddElement("X", InitializeString("[IMG|Atk18] +%i",     mDamageBoost));
        if(mProtectionBoost != 0) tLineList->AddElement("X", InitializeString("[IMG|Prt18] +%i",    mProtectionBoost));
        if(mIniBoost != 0)        tLineList->AddElement("X", InitializeString("[IMG|Ini18] +%i", mIniBoost));
        if(mEvdBoost != 0)        tLineList->AddElement("X", InitializeString("[IMG|Evd18] +%i",      mEvdBoost));
        if(mAccBoost != 0)        tLineList->AddElement("X", InitializeString("[IMG|Acc18] +%i",   mAccBoost));
    }

    //--Store everything.
    mPropertyLinesTotal = tLineList->GetListSize();
    if(mPropertyLinesTotal > 0)
    {
        //--Allocate.
        int i = 0;
        SetMemoryData(__FILE__, __LINE__);
        mPropertyLines = (char **)starmemoryalloc(sizeof(char *) * mPropertyLinesTotal);

        //--Fill.
        tLineList->SetRandomPointerToHead();
        char *rString = (char *)tLineList->LiberateRandomPointerEntry();
        while(rString)
        {
            //--Store.
            mPropertyLines[i] = rString;

            //--Next.
            i ++;
            tLineList->SetRandomPointerToHead();
            rString = (char *)tLineList->LiberateRandomPointerEntry();
        }
    }

    //--Clean.
    delete tLineList;
}

//--[Worker Function]
void ConstructComparisonLine(int pStatA, int pStatB, const char *pString, SugarLinkedList *pList)
{
    //--Given two statistics, creates a comparison case for them. If the stats are non-zero then
    //  a line is added to the list. If both are zero, nothing gets added to the list.
    //--Example string: " Health: %+i"
    //--The first letter of the comparison indicates what color the string should be.
    //  + = green
    //  - = red
    //  0 = grey out
    if(!pList || !pString) return;

    //--Do nothing if both stats are zero. This way they won't appear on the list.
    if(pStatA == 0 && pStatB == 0) return;

    //--Create the string we will be storing. The first space of the string should be empty.
    char *nString = InitializeString(pString, pStatB - pStatA);

    //--If the B value is higher, it's a positive string.
    if(pStatB > pStatA)
    {
        nString[0] = '+';
    }
    //--Lower, negative.
    else if(pStatB < pStatA)
    {
        nString[0] = '-';
    }
    //--Otherwise, character zero.
    else
    {
        nString[0] = '0';
    }

    //--Now add the string to the list.
    pList->AddElement("X", nString);
}

void AdventureItem::ResolveComparisonLines(AdventureItem *pComparisonItem)
{
    //--Similar to resolving property lines, but resolves comparison lines instead. These show
    //  property differences, such as the difference in Attack Power or Protection.
    //--If the names of the comparison items are identical to the last comparison case, the
    //  lines do not get rebuilt. This prevents unnecessary execution.
    //--Only equipment items should be compared. If you need to compare usable items, then use their
    //  property lines and just display them next to each other.
    if(!pComparisonItem) return;

    //--[Static Recheck Case]
    //--Check if the static comparison copies are identical. This item is always the A item, and
    //  the check-against is the B item.
    if(xComparisonItemA && !strcmp(mLocalName, xComparisonItemA) && xComparisonItemB && !strcmp(pComparisonItem->mLocalName, xComparisonItemB))
    {
        return;
    }

    //--Not identical, so store the comparison names for next time.
    ResetString(xComparisonItemA, mLocalName);
    ResetString(xComparisonItemB, pComparisonItem->mLocalName);

    //--[Reset Memory]
    //--Deallocate.
    for(int i = 0; i < xComparisonLinesTotal; i ++) free(xComparisonLines[i]);
    free(xComparisonLines);

    //--Clear.
    xComparisonLinesTotal = 0;
    xComparisonLines = NULL;

    //--[Line Building]
    //--List to store the lines.
    SugarLinkedList *tLineList = new SugarLinkedList(false);

    //--Basic Stats
    ConstructComparisonLine(mHealthBoost,     pComparisonItem->mHealthBoost,     " Health: %+i",     tLineList);
    ConstructComparisonLine(mDamageBoost,     pComparisonItem->mDamageBoost,     " Damage: %+i",     tLineList);
    ConstructComparisonLine(mProtectionBoost, pComparisonItem->mProtectionBoost, " Protect: %+i",    tLineList);
    ConstructComparisonLine(mInitiativeBoost, pComparisonItem->mInitiativeBoost, " Initiative: %+i", tLineList);
    ConstructComparisonLine(mEvasionBoost,    pComparisonItem->mEvasionBoost,    " Evade: %+i",      tLineList);
    ConstructComparisonLine(mAccuracyBoost,   pComparisonItem->mAccuracyBoost,   " Accuracy: %+i",   tLineList);

    //--Store everything.
    xComparisonLinesTotal = tLineList->GetListSize();
    if(xComparisonLinesTotal > 0)
    {
        //--Allocate.
        int i = 0;
        SetMemoryData(__FILE__, __LINE__);
        xComparisonLines = (char **)starmemoryalloc(sizeof(char *) * xComparisonLinesTotal);

        //--Fill.
        tLineList->SetRandomPointerToHead();
        char *rString = (char *)tLineList->LiberateRandomPointerEntry();
        while(rString)
        {
            //--Store.
            xComparisonLines[i] = rString;

            //--Next.
            i ++;
            tLineList->SetRandomPointerToHead();
            rString = (char *)tLineList->LiberateRandomPointerEntry();
        }
    }

    //--Clean.
    delete tLineList;
}
void AdventureItem::ResolveGemCoordinates(int pGemSlot, SugarBitmap *pGemIndicator, float &sLft, float &sTop, float &sRgt, float &sBot)
{
    //--Returns the texture coordinates to use for the gem's indicator.
    if(!pGemIndicator) return;

    //--Default/No Gem/Error.
    if(pGemSlot < 0 || pGemSlot >= mGemSlotsTotal || !mGemSlots[pGemSlot])
    {
        sLft = 1.0f / pGemIndicator->GetWidth();
        sTop = 1.0f / pGemIndicator->GetHeight();
        sRgt = sLft + (15.0f / pGemIndicator->GetWidth());
        sBot = sTop + (15.0f / pGemIndicator->GetHeight());
    }
    //--Otherwise:
    else
    {
        sLft = 35.0f / pGemIndicator->GetWidth();
        sTop = 1.0f / pGemIndicator->GetHeight();
        sRgt = sLft + (15.0f / pGemIndicator->GetWidth());
        sBot = sTop + (15.0f / pGemIndicator->GetHeight());
    }

    //--Flip.
    sTop = 1.0f - sTop;
    sBot = 1.0f - sBot;
}
AdventureItem *AdventureItem::Clone()
{
    //--Clones the item, creating an exact duplicate.
    AdventureItem *nClone = new AdventureItem();

    //--System
    nClone->mIsAdamantite = mIsAdamantite;
    nClone->mIsStackable = mIsStackable;
    nClone->mStackSize = 1;
    ResetString(nClone->mLocalName, mLocalName);
    ResetString(nClone->mDescription, mDescription);
    ResetString(nClone->mTopNameLine, mTopNameLine);
    ResetString(nClone->mBotNameLine, mBotNameLine);

    //--Property Lines
    nClone->mLastResolveFlag = mLastResolveFlag;
    nClone->mPropertyLinesTotal = mPropertyLinesTotal;
    SetMemoryData(__FILE__, __LINE__);
    nClone->mPropertyLines = (char **)starmemoryalloc(sizeof(char *) * mPropertyLinesTotal);
    for(int i = 0; i < mPropertyLinesTotal; i ++)
    {
        nClone->mPropertyLines[i] = InitializeString(mPropertyLines[i]);
    }

    //--Value
    nClone->mValue = mValue;

    //--Consumable
    nClone->mIsConsumable = mIsConsumable;

    //--Upgrading
    nClone->mIsUpgradeable = mIsUpgradeable;
    memcpy(nClone->mUpgradeCodes, mUpgradeCodes, sizeof(int8_t) * ADITEM_MAX_UPGRADES);

    //--Gem Slots
    nClone->mGemSlotsTotal = mGemSlotsTotal;
    memset(mGemSlots, 0, sizeof(AdventureItem *) * ADITEM_MAX_GEMS);

    //--Key Item
    nClone->mIsUnique = mIsUnique;
    nClone->mIsKeyItem = mIsKeyItem;

    //--Equipment
    nClone->mIsEquipment = mIsEquipment;
    strcpy(nClone->mEquipSlot, mEquipSlot);
    if(mEquippableList)
    {
        nClone->mEquippableList = new SugarLinkedList(false);
        void *rDummyPtr = mEquippableList->PushIterator();
        while(rDummyPtr)
        {
            nClone->mEquippableList->AddElement(mEquippableList->GetIteratorName(), rDummyPtr);
            mEquippableList->AutoIterate();
        }
    }

    //--Equipment Boosts
    nClone->mHealthBoost = mHealthBoost;
    nClone->mDamageBoost = mDamageBoost;
    nClone->mProtectionBoost = mProtectionBoost;
    nClone->mAccuracyBoost = mAccuracyBoost;
    nClone->mEvasionBoost = mEvasionBoost;
    nClone->mInitiativeBoost = mInitiativeBoost;

    //--Combat Items
    nClone->mNeverRegenerateCharges = mNeverRegenerateCharges;
    nClone->mItemRegeneratesEachBattle = mItemRegeneratesEachBattle;
    nClone->mCooldownRemaining = mCooldownRemaining;
    nClone->mItemCooldown = mItemCooldown;
    nClone->mCharges = mCharges;
    nClone->mChargesMax = mChargesMax;
    mUseAbility = NULL;

    //--Rendering
    nClone->rIconImg = rIconImg;

    //--All done.
    return nClone;
}

//===================================== Private Core Methods ======================================
//============================================ Update =============================================
//=========================================== File I/O ============================================
//=========================================== Drawing =============================================
//======================================= Pointer Routing =========================================
AdvCombatAbility *AdventureItem::GetCombatAbility()
{
    return mUseAbility;
}
AdventureItem *AdventureItem::GetGemInSlot(int pSlot)
{
    //--Note: It is legal to receive NULL back on error, OR empty slot. Attempting to get a gem
    //  past the allowable gem slot always returns NULL since it's logically impossible to cram
    //  a gem in there.
    if(pSlot < 0 || pSlot >= ADITEM_MAX_GEMS) return NULL;
    return mGemSlots[pSlot];
}

//====================================== Static Functions =========================================
void AdventureItem::InitGemcutterCosts()
{
}

//========================================= Lua Hooking ===========================================
void AdventureItem::HookToLuaState(lua_State *pLuaState)
{
    /* AdItem_GetProperty("Dummy") (1 string)
       Returns the requested property from the active AdventureItem. */
    lua_register(pLuaState, "AdItem_GetProperty", &Hook_AdItem_GetProperty);

    /* //--General
       AdItem_SetProperty("Name", sName)
       AdItem_SetProperty("Is Adamantite")
       AdItem_SetProperty("Description", sDescription)
       AdItem_SetProperty("Is Consumable", bFlag)
       AdItem_SetProperty("Is Upgradeable", bFlag)
       AdItem_SetProperty("Is Key Item", bFlag)
       AdItem_SetProperty("Is Unique", bFlag)
       AdItem_SetProperty("Is Equipment", bFlag)
       AdItem_SetProperty("Rendering Image", sDLPath)

       //--Equippable
       AdItem_SetProperty("Add Equippable Character", sName)
       AdItem_SetProperty("Equip Slot", iValue)
       AdItem_SetProperty("Health Boost", iValue)
       AdItem_SetProperty("Damage Boost", iValue)
       AdItem_SetProperty("Speed Boost", iValue)
       AdItem_SetProperty("Protection Boost", iValue)
       AdItem_SetProperty("Evasion Boost", iValue)
       AdItem_SetProperty("Accuracy Boost", iValue)
       AdItem_SetProperty("Initiative Boost", iValue)
       AdItem_SetProperty("Equip Bonus", iFlag, iValue)
       AdItem_SetProperty("Gem Slots", iTotal)

       //--Combat Item
       AdItem_SetProperty("Charges", iValue)
       AdItem_SetProperty("Charges Max", iValue)
       AdItem_SetProperty("Charges Refresh on Rest", bValue)
       AdItem_SetProperty("Recharges Every Battle", bValue)
       AdItem_SetProperty("Cooldown", iTurns)
       AdItem_SetProperty("Create Action")

       Sets the requested property in the active AdventureItem. */
    lua_register(pLuaState, "AdItem_SetProperty", &Hook_AdItem_SetProperty);
}

//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
int Hook_AdItem_GetProperty(lua_State *L)
{
    //AdItem_GetProperty("Dummy") (1 string)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("AdItem_GetProperty");

    //--Active object.
    AdventureItem *rItem = (AdventureItem *)DataLibrary::Fetch()->rActiveObject;
    if(!rItem || !rItem->IsOfType(POINTER_TYPE_ADVENTUREITEM)) return LuaTypeError("AdItem_GetProperty");

    //--Switching.
    int tReturns = 0;
    const char *rSwitchType = lua_tostring(L, 1);

    //--Dummy.
    if(!strcasecmp(rSwitchType, "Dummy") && tArgs == 1)
    {
    }
    //--Error.
    else
    {
        LuaPropertyError("AdItem_GetProperty", rSwitchType, tArgs);
    }

    //--Finish up.
    return tReturns;
}
int Hook_AdItem_SetProperty(lua_State *L)
{
    //--General
    //AdItem_SetProperty("Name", sName)
    //AdItem_SetProperty("Name Lines", sTopName, sBotName)
    //AdItem_SetProperty("Is Adamantite")
    //AdItem_SetProperty("Is Stackable")
    //AdItem_SetProperty("Description", sDescription)
    //AdItem_SetProperty("Value", iValue)
    //AdItem_SetProperty("Is Consumable", bFlag)
    //AdItem_SetProperty("Is Upgradeable", bFlag)
    //AdItem_SetProperty("Is Key Item", bFlag)
    //AdItem_SetProperty("Is Unique", bFlag)
    //AdItem_SetProperty("Rendering Image", sDLPath)

    //--Equippable
    //AdItem_SetProperty("Is Equipment", bFlag)
    //AdItem_SetProperty("Add Equippable Character", sName)
    //AdItem_SetProperty("Equip Slot", iValue)
    //AdItem_SetProperty("Health Boost", iValue)
    //AdItem_SetProperty("Damage Boost", iValue)
    //AdItem_SetProperty("Speed Boost", iValue)
    //AdItem_SetProperty("Protection Boost", iValue)
    //AdItem_SetProperty("Evasion Boost", iValue)
    //AdItem_SetProperty("Accuracy Boost", iValue)
    //AdItem_SetProperty("Initiative Boost", iValue)
    //AdItem_SetProperty("Equip Bonus", iFlag, iValue)
    //AdItem_SetProperty("Gem Slots", iTotal)

    //--Combat Item
    //AdItem_SetProperty("Charges", iValue)
    //AdItem_SetProperty("Charges Max", iValue)
    //AdItem_SetProperty("Charges Dont Refresh on Rest", bValue)
    //AdItem_SetProperty("Recharges Every Battle", bValue)
    //AdItem_SetProperty("Cooldown", iTurns)
    //AdItem_SetProperty("Create Action")

    //--Arg check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("AdItem_SetProperty");

    //--Active object.
    AdventureItem *rItem = (AdventureItem *)DataLibrary::Fetch()->rActiveObject;
    if(!rItem || !rItem->IsOfType(POINTER_TYPE_ADVENTUREITEM))
    {
        return LuaTypeError("AdItem_SetProperty");
    }

    //--Switching.
    const char *rSwitchType = lua_tostring(L, 1);

    //--[General Properties]
    //--Display name of the item. Does not affect the search name!
    if(!strcasecmp(rSwitchType, "Name") && tArgs == 2)
    {
        rItem->SetName(lua_tostring(L, 2));
    }
    //--Display names of the item used for UI display.
    else if(!strcasecmp(rSwitchType, "Name Lines") && tArgs == 3)
    {
        rItem->SetNameLines(lua_tostring(L, 2), lua_tostring(L, 3));
    }
    //--Causes the item to become Adamantite. Can only exist in shops. When purchased, becomes a crafting item.
    else if(!strcasecmp(rSwitchType, "Is Adamantite") && tArgs == 1)
    {
        rItem->SetAdamantite();
    }
    //--Items can stack together.
    else if(!strcasecmp(rSwitchType, "Is Stackable") && tArgs == 1)
    {
        rItem->SetStackable(true);
    }
    //--Description of the item.
    else if(!strcasecmp(rSwitchType, "Description") && tArgs == 2)
    {
        rItem->SetDescription(lua_tostring(L, 2));
    }
    //--How much the item is worth at shops.
    else if(!strcasecmp(rSwitchType, "Value") && tArgs == 2)
    {
        rItem->SetValue(lua_tointeger(L, 2));
    }
    //--Consumable, can be equipped in combat. Will always have at least one max charge.
    else if(!strcasecmp(rSwitchType, "Is Consumable") && tArgs == 2)
    {
        rItem->SetConsumable(lua_toboolean(L, 2));
    }
    //--Upgradeable, can be improved at a gemcutter.
    else if(!strcasecmp(rSwitchType, "Is Upgradeable") && tArgs == 2)
    {
        rItem->SetUpgradeable(lua_toboolean(L, 2));
    }
    //--Key item. Cannot be discarded, does not preclude usage or equipping.
    else if(!strcasecmp(rSwitchType, "Is Key Item") && tArgs == 2)
    {
        rItem->SetKeyItem(lua_toboolean(L, 2));
    }
    //--Unique. Will not appear in shops if the player already has one.
    else if(!strcasecmp(rSwitchType, "Is Unique") && tArgs == 2)
    {
        rItem->SetUnique(lua_toboolean(L, 2));
    }
    //--Rendering image. Which icon to use when seeing this item on the UI.
    else if(!strcasecmp(rSwitchType, "Rendering Image") && tArgs == 2)
    {
        rItem->SetIconImage(lua_tostring(L, 2));
    }
    //--[Equipment Subsection]
    //--Marks this item as a piece of equipment which is equipped in a slot to provide stat changes.
    else if(!strcasecmp(rSwitchType, "Is Equipment") && tArgs == 2)
    {
        rItem->SetEquipmentFlag(lua_toboolean(L, 2));
    }
    //--Adds a character who can equip the item.
    else if(!strcasecmp(rSwitchType, "Add Equippable Character") && tArgs == 2)
    {
        rItem->AddEquippableCharacer(lua_tostring(L, 2));
    }
    //--Sets which slot the item is equipped in.
    else if(!strcasecmp(rSwitchType, "Equip Slot") && tArgs == 2)
    {
        rItem->SetEquipSlot(lua_tostring(L, 2));
    }
    //--Increases health total when equipped. Can be zero or negative.
    else if(!strcasecmp(rSwitchType, "Health Boost") && tArgs == 2)
    {
        rItem->SetHealthBoost(lua_tointeger(L, 2));
    }
    //--Sets the minimum damage increase for the damage range. Can be zero or negative!
    else if(!strcasecmp(rSwitchType, "Damage Boost") && tArgs == 2)
    {
        rItem->SetDamageBoost(lua_tointeger(L, 2));
    }
    //--Sets the protection provided by the equipment. Can apply to weapons.
    else if(!strcasecmp(rSwitchType, "Protection Boost") && tArgs == 2)
    {
        rItem->SetProtectionBoost(lua_tointeger(L, 2));
    }
    //--Sets evasion bonus for the equipment.
    else if(!strcasecmp(rSwitchType, "Evasion Boost") && tArgs == 2)
    {
        rItem->SetEvasionBoost(lua_tointeger(L, 2));
    }
    //--Sets accuracy bonus for the equipment.
    else if(!strcasecmp(rSwitchType, "Accuracy Boost") && tArgs == 2)
    {
        rItem->SetAccuracyBoost(lua_tointeger(L, 2));
    }
    //--Sets initiative bonus for the equipment.
    else if(!strcasecmp(rSwitchType, "Initiative Boost") && tArgs == 2)
    {
        rItem->SetInitiativeBoost(lua_tointeger(L, 2));
    }
    //--Sets the equipment bonus provided for equipping this item. Types is independent of slot: Armor can buff bleed damage, for example.
    //  Flags are the gciEquipBonus_XXX series.
    else if(!strcasecmp(rSwitchType, "Equip Bonus") && tArgs == 3)
    {
        rItem->SetEquipmentBonus(lua_tointeger(L, 2), lua_tointeger(L, 3));
    }
    //--Sets how many gems this item can hold.
    else if(!strcasecmp(rSwitchType, "Gem Slots") && tArgs == 2)
    {
        rItem->SetGemCap(lua_tointeger(L, 2));
    }
    //--[Combat Item]
    //--Current number of charges.
    else if(!strcasecmp(rSwitchType, "Charges") && tArgs == 2)
    {
        rItem->SetCharges(lua_tointeger(L, 2));
    }
    //--Maximum number of charges.
    else if(!strcasecmp(rSwitchType, "Charges Max") && tArgs == 2)
    {
        rItem->SetMaxCharges(lua_tointeger(L, 2));
    }
    //--If set to true, the item never refreshes charges.
    else if(!strcasecmp(rSwitchType, "Charges Dont Refresh on Rest") && tArgs == 2)
    {
        rItem->SetNeverRechargeFlag(lua_toboolean(L, 2));
    }
    //--If set to true, the item regains all charges after each combat.
    else if(!strcasecmp(rSwitchType, "Recharges Every Battle") && tArgs == 2)
    {
        rItem->SetRechargeEveryBattleFlag(lua_toboolean(L, 2));
    }
    //--Cooldown when the item is used.
    else if(!strcasecmp(rSwitchType, "Cooldown") && tArgs == 2)
    {
        rItem->SetCooldown(lua_tointeger(L, 2));
    }
    //--Pushes a new AdventureCombatAction. Pop it when done.
    else if(!strcasecmp(rSwitchType, "Create Action") && tArgs == 1)
    {
        AdvCombatAbility *nAction = new AdvCombatAbility();
        char *tItemActionName = InitializeString("%s Action", rItem->GetName());
        nAction->SetInternalName(tItemActionName);
        free(tItemActionName);
        rItem->RegisterAbility(nAction);
        DataLibrary::Fetch()->PushActiveEntity(nAction);
    }
    //--[Misc]
    //--Error.
    else
    {
        LuaPropertyError("AdItem_SetProperty", rSwitchType, tArgs);
    }

    //--Finish up.
    return 0;
}
