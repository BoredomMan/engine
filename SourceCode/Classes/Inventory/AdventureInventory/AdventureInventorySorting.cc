//--Base
#include "AdventureInventory.h"

//--Classes
#include "AdventureItem.h"

//--CoreClasses
#include "SugarLinkedList.h"

//--Definitions
//--Libraries
//--Managers

//--[Static Variables]
bool AdventureInventory::xSortListInReverse = false;

//--[Forward Declarations]
int CompareItemsByQuality(const void *pEntryA, const void *pEntryB);


//--[Master Function]
void AdventureInventory::SortListByCriteria(SugarLinkedList *pList, int pCriteriaFlag, bool pHighestFirst)
{
    //--Given an inventory list containing AdventureItem pointers (and ONLY those!), attempts to sort
    //  the list using the provided flag. The flag is one of the AINV_SORT_CRITERIA_* pattern.
    //--When sorting lists in this manner, make sure you don't have an iterator running. That will
    //  cause undefined behavior. If you know what you're doing, you can still run this with an iterator,
    //  so the function will *not* stop you.
    //--Note: When sorting a list that isn't the mItemList, the pointers are usually referenced anyway.
    //  For example, sorting the gem list won't change anything in the item list, and when ordering
    //  equipment changes, the master item list is searched by pointer.
    if(!pList || pCriteriaFlag < 0 || pCriteriaFlag >= AINV_SORT_CRITERIA_MAX) return;

    //--Static flag to quickly switch how the sort works without requiring function duplication.
    AdventureInventory::xSortListInReverse = !pHighestFirst;

    //--Setup.
    SortFnPtr rFuncPtr = NULL;

    //--Set the sorting operation using the sorting function requested. The functions are defined and explained
    //  below here.
    if(pCriteriaFlag == AINV_SORT_CRITERIA_QUALITY)
    {
        rFuncPtr = &CompareItemsByQuality;
    }

    //--Call.
    if(rFuncPtr) pList->SortListUsing(rFuncPtr);
}

//--[Sort by Quality]
int CompareItemsByQuality(const void *pEntryA, const void *pEntryB)
{
    //--Attempts to sort the entries by 'quality', which is a formula that just sort of figures out
    //  which is 'better' in vague game terms. Some items may have a lower quality than their immediate
    //  numbers may suggest because certain enemies are weak against certain damage types. However, as
    //  the game goes on, quality of items generally increases as their numbers do.
    //--Quality does NOT directly map onto value! Runestones and other combat items often have low values
    //  but are extremely useful.
    //--This is a good general-purpose sort.
    SugarLinkedListEntry **rEntryA = (SugarLinkedListEntry **)pEntryA;
    SugarLinkedListEntry **rEntryB = (SugarLinkedListEntry **)pEntryB;

    //--Get the pointer out and cast to AdventureItem.
    AdventureItem *rItemA = (AdventureItem *)((*rEntryA)->rData);
    AdventureItem *rItemB = (AdventureItem *)((*rEntryB)->rData);

    return ((rItemB->ComputeQuality() * 10000) - (rItemA->ComputeQuality() * 10000));
}
