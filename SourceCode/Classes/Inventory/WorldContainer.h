//--[WorldContainer]
//--An object that appears in the world and contains one or more items, with options to
//  be trapped. The player must spend a turn opening the container to get at its contents, which
//  are unceremoniously dumped on the floor.
//--If trapped, a trap fire script is provided by the inventory item in the trap slot. Some traps
//  require rolls, others always occur, it's up to the script.
//--The Lua work for spawning a container is done in the InventoryItem lua scripts, as the container
//  doesn't mean anything without any contents. Other than its name and traps, containers are uninteresting.
//--Once opened, the container immediately despawns.

#pragma once

//--[Includes]
#include "Definitions.h"
#include "Structures.h"
#include "RootObject.h"

//--[Local Structures]
//--[Local Definitions]
//--[Classes]
class WorldContainer : public RootObject
{
    private:
    //--System
    bool mSelfDestruct;
    char *mName;
    char *mDescription;

    //--Contents
    int mTotalContents;
    InventoryItem **mContents;

    //--Traps
    InventoryItem *mTrapItem;
    char *mExtraTrapScript;

    //--Owner
    PandemoniumRoom *rOwnerRoom;

    //--Visibility
    int mLastSetVisible;
    int mIsVisibleTimer;

    //--Private Statics
    static char *xContainerNameGeneratorPath;
    static char *xNextContainerName;
    static char *xNextContainerDescription;

    protected:

    public:
    //--System
    WorldContainer();
    virtual ~WorldContainer();

    //--Public Variables
    int mLastRenderSlot;

    //--Public Statics
    static int xOpenSoundTimer;
    static bool xTrapDespawn;
    static bool xTrapSucceeded;

    //--Property Queries
    const char *GetName();
    const char *GetDescription();
    const char *GetExtraTrapScript();
    bool IsSelfDestructing();
    int GetVisibilityTimer();

    //--Manipulators
    void SetDestruct(bool pFlag);
    void SetName(const char *pName);
    void SetDescription(const char *pDescription);
    void SetExtraTrap(const char *pTrapScript);
    void SetContentsSize(int pAmount);
    void SetContents(int pSlot, InventoryItem *pItem);
    void SetTrap(InventoryItem *pItem);
    void SetOwner(PandemoniumRoom *pOwner);
    void IncrementVisible();
    void DecrementVisible();

    //--Core Methods
    void Open();
    char *AssembleOpenString(const char *pActorName, bool pIsFirstPerson);

    private:
    //--Private Core Methods
    public:
    //--Update
    //--File I/O
    //--Drawing
    //--Pointer Routing
    //--Static Functions
    static void PlayOpenSound();
    static WorldContainer *SpawnContainerIn(const char *pRoomName, InventoryItem *pItem);
    static void SetNameGeneratorPath(const char *pPath);
    static void SetNextName(const char *pName);
    static void SetNextDescription(const char *pName);

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_Container_SetNameGeneratorPath(lua_State *L);
int Hook_Container_SetNextName(lua_State *L);
int Hook_Container_SetNextDescription(lua_State *L);
int Hook_Container_InformTrapDespawn(lua_State *L);
int Hook_Container_InformTrapFailed(lua_State *L);
int Hook_Container_GetProperty(lua_State *L);
int Hook_Container_SetProperty(lua_State *L);
