//--Base
#include "BuildPackage.h"

//--Classes
//--CoreClasses
#include "SugarFileSystem.h"
#include "SugarLinkedList.h"

//--Definitions
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "LuaManager.h"

//=========================================== System ==============================================
BuildPackage::BuildPackage()
{
    //--[RootObject]
    //--System
    mType = POINTER_TYPE_BUILDPACKAGE;

    //--[BuildPackage]
    //--System
    mCommandsType = 0;
    mCoreDirectory = InitializeString("./");
    mBuildDirectory = InitializeString("./");
    mInstructionList = new SugarLinkedList(true);

    //--[Resolve OS]
    //--Figure out which OS we should tailor commands to. This can be manually overridden to emulate
    //  running on a different OS.
    #if defined _TARGET_OS_WINDOWS_
        mCommandsType = BP_SHELL_DOS;
    #elif defined _TARGET_OS_MAC_
        mCommandsType = BP_SHELL_BASH;
    #elif defined _TARGET_OS_LINUX_
        mCommandsType = BP_SHELL_BASH;
    #endif
}
BuildPackage::~BuildPackage()
{
    free(mCoreDirectory);
    free(mBuildDirectory);
    delete mInstructionList;
}

//--[Private Statics]
//--Stores a list of all available builds. Defaults to NULL, use the fetcher function to retrieve it,
//  which will create it if it wasn't already created.
SugarLinkedList *BuildPackage::xBuildList = NULL;

//====================================== Property Queries =========================================
//========================================= Manipulators ==========================================
void BuildPackage::SetCoreDirectory(const char *pPath)
{
    if(!pPath) return;
    ResetString(mCoreDirectory, pPath);

    //--Fix.
    FixSlashes(mCoreDirectory);
}
void BuildPackage::SetBuildDirectory(const char *pPath)
{
    if(!pPath) return;
    ResetString(mBuildDirectory, pPath);

    //--Fix.
    FixSlashes(mBuildDirectory);
}
void BuildPackage::RegisterSetupBuildDirectory()
{
    SetMemoryData(__FILE__, __LINE__);
    BuildPackageInstruction *nInstruction = (BuildPackageInstruction *)starmemoryalloc(sizeof(BuildPackageInstruction));
    nInstruction->Initialize();
    nInstruction->mInstructionCode = BP_INSTRUCTION_SETUP_BUILD_DIR;
    mInstructionList->AddElementAsTail("X", nInstruction, &BuildPackageInstruction::DeleteThis);
}
void BuildPackage::RegisterCreateFolderCommand(const char *pFolderPath)
{
    if(!pFolderPath) return;
    SetMemoryData(__FILE__, __LINE__);
    BuildPackageInstruction *nInstruction = (BuildPackageInstruction *)starmemoryalloc(sizeof(BuildPackageInstruction));
    nInstruction->Initialize();
    nInstruction->mInstructionCode = BP_INSTRUCTION_CREATE_FOLDER;
    ResetString(nInstruction->mStrings[0], pFolderPath);
    mInstructionList->AddElementAsTail("X", nInstruction, &BuildPackageInstruction::DeleteThis);

    //--Fix.
    FixSlashes(nInstruction->mStrings[0]);
}
void BuildPackage::RegisterCopyFileCommand(const char *pOrigFile, const char *pDestFile)
{
    if(!pOrigFile || !pDestFile) return;
    SetMemoryData(__FILE__, __LINE__);
    BuildPackageInstruction *nInstruction = (BuildPackageInstruction *)starmemoryalloc(sizeof(BuildPackageInstruction));
    nInstruction->Initialize();
    nInstruction->mInstructionCode = BP_INSTRUCTION_COPY_FILE;
    ResetString(nInstruction->mStrings[0], pOrigFile);
    ResetString(nInstruction->mStrings[1], pDestFile);
    mInstructionList->AddElementAsTail("X", nInstruction, &BuildPackageInstruction::DeleteThis);

    //--Fix.
    FixSlashes(nInstruction->mStrings[0]);
    FixSlashes(nInstruction->mStrings[1]);
}
void BuildPackage::RegisterCopyFolderCommand(const char *pOrigFolder, const char *pDestFolder)
{
    if(!pOrigFolder || !pDestFolder) return;
    SetMemoryData(__FILE__, __LINE__);
    BuildPackageInstruction *nInstruction = (BuildPackageInstruction *)starmemoryalloc(sizeof(BuildPackageInstruction));
    nInstruction->Initialize();
    nInstruction->mInstructionCode = BP_INSTRUCTION_COPY_FOLDER;
    ResetString(nInstruction->mStrings[0], pOrigFolder);
    ResetString(nInstruction->mStrings[1], pDestFolder);
    mInstructionList->AddElementAsTail("X", nInstruction, &BuildPackageInstruction::DeleteThis);

    //--Fix.
    FixSlashes(nInstruction->mStrings[0]);
    FixSlashes(nInstruction->mStrings[1]);
}
void BuildPackage::RegisterDeleteFileCommand(const char *pFileName)
{
    if(!pFileName) return;
    SetMemoryData(__FILE__, __LINE__);
    BuildPackageInstruction *nInstruction = (BuildPackageInstruction *)starmemoryalloc(sizeof(BuildPackageInstruction));
    nInstruction->Initialize();
    nInstruction->mInstructionCode = BP_INSTRUCTION_DELETE_FILE;
    ResetString(nInstruction->mStrings[0], pFileName);
    mInstructionList->AddElementAsTail("X", nInstruction, &BuildPackageInstruction::DeleteThis);

    //--Fix.
    FixSlashes(nInstruction->mStrings[0]);
}
void BuildPackage::RegisterDeleteFolderCommand(const char *pFolderName)
{
    if(!pFolderName) return;
    SetMemoryData(__FILE__, __LINE__);
    BuildPackageInstruction *nInstruction = (BuildPackageInstruction *)starmemoryalloc(sizeof(BuildPackageInstruction));
    nInstruction->Initialize();
    nInstruction->mInstructionCode = BP_INSTRUCTION_DELETE_FOLDER;
    ResetString(nInstruction->mStrings[0], pFolderName);
    mInstructionList->AddElementAsTail("X", nInstruction, &BuildPackageInstruction::DeleteThis);

    //--Fix.
    FixSlashes(nInstruction->mStrings[0]);
}
void BuildPackage::RegisterChangeIconCommand(const char *pFilename, const char *pIconName)
{
    if(!pFilename || !pIconName) return;
    SetMemoryData(__FILE__, __LINE__);
    BuildPackageInstruction *nInstruction = (BuildPackageInstruction *)starmemoryalloc(sizeof(BuildPackageInstruction));
    nInstruction->Initialize();
    nInstruction->mInstructionCode = BP_INSTRUCTION_CHANGEICON;
    ResetString(nInstruction->mStrings[0], pFilename);
    ResetString(nInstruction->mStrings[1], pIconName);
    mInstructionList->AddElementAsTail("X", nInstruction, &BuildPackageInstruction::DeleteThis);

    //--Fix.
    FixSlashes(nInstruction->mStrings[0]);
    FixSlashes(nInstruction->mStrings[1]);
}

//========================================= Core Methods ==========================================
void BuildPackage::FixSlashes(char *pString)
{
    //--Changes forward slashes to backslashes for Windows. Should only be used on paths. Does nothing
    //  on other OS's.

    #if defined _TARGET_OS_WINDOWS_
    int tLen = (int)strlen(pString);
    for(int p = 0; p < tLen; p ++)
    {
        if(pString[p] == '/') pString[p] = '\\';
    }
    #endif
}
void BuildPackage::ExecuteCommand(BuildPackageInstruction *pCommand, bool pIsEmulation, FILE *fOutputFile)
{
    //--Executes the given command, or emulates it and writes it to a file. Emulation causes the command to be
    //  written to a file instead of executed (allowing for creation of batch/shell files, or to error-check)
    //  and is not exclusive with output. The file output can be NULL if desired, but if NULL and emulating,
    //  nothing actually happens. Duh.
    if(!pCommand) return;

    //--If emulating and no output file, do nothing.
    if(pIsEmulation && !fOutputFile) return;

    //--Commands Buffers.
    char tCommandBuffer[BP_COMMANDS_MAX][1024];
    for(int i = 0; i < BP_COMMANDS_MAX; i ++) tCommandBuffer[i][0] = '\0';

    //--Build Directory
    if(pCommand->mInstructionCode == BP_INSTRUCTION_SETUP_BUILD_DIR)
    {
        if(mCommandsType == BP_SHELL_DOS)
        {
            //--Trim the slashes off the end, if present.
            char tUseBuildDir[512];
            strcpy(tUseBuildDir, mBuildDirectory);
            int tLen = (int)strlen(tUseBuildDir);
            if(tUseBuildDir[tLen-1] == '/' || tUseBuildDir[tLen-1] == '\\') tUseBuildDir[tLen-1] = '\0';

            //--Set.
            sprintf(tCommandBuffer[0], "if exist \"%s\" move \"%s\" \"%s Backup %i\"\n", tUseBuildDir, tUseBuildDir, tUseBuildDir, (int)clock());
            sprintf(tCommandBuffer[1], "mkdir \"%s\"\n", tUseBuildDir);
        }
        else
        {
            sprintf(tCommandBuffer[0], "if test -e \"%s\"; then mv \"%s\" \"%s Backup %i\"; fi\n", mBuildDirectory, mBuildDirectory, mBuildDirectory, (int)clock());
            sprintf(tCommandBuffer[1], "mkdir \"%s\"\n", mBuildDirectory);
        }
    }
    //--Create Folder:
    else if(pCommand->mInstructionCode == BP_INSTRUCTION_CREATE_FOLDER)
    {
        if(mCommandsType == BP_SHELL_DOS)
        {
            sprintf(tCommandBuffer[0], "mkdir \"%s%s\"\n", mBuildDirectory, pCommand->mStrings[0]);
        }
        else if(mCommandsType == BP_SHELL_BASH)
        {
            sprintf(tCommandBuffer[0], "mkdir \"%s%s\"\n", mBuildDirectory, pCommand->mStrings[0]);
        }
    }
    //--Copy File:
    else if(pCommand->mInstructionCode == BP_INSTRUCTION_COPY_FILE)
    {
        if(mCommandsType == BP_SHELL_DOS)
        {
            sprintf(tCommandBuffer[0], "copy \"%s%s\" \"%s%s\"\n", mCoreDirectory, pCommand->mStrings[0], mBuildDirectory, pCommand->mStrings[1]);
        }
        else if(mCommandsType == BP_SHELL_BASH)
        {
            sprintf(tCommandBuffer[0], "cp \"%s%s\" \"%s%s\"\n", mCoreDirectory, pCommand->mStrings[0], mBuildDirectory, pCommand->mStrings[1]);
        }
    }
    //--Copy Folder:
    else if(pCommand->mInstructionCode == BP_INSTRUCTION_COPY_FOLDER)
    {
        if(mCommandsType == BP_SHELL_DOS)
        {
            sprintf(tCommandBuffer[0], "robocopy \"%s%s\" \"%s%s\" /E /NJH /NJS\n", mCoreDirectory, pCommand->mStrings[0], mBuildDirectory, pCommand->mStrings[1]);
        }
        else if(mCommandsType == BP_SHELL_BASH)
        {
            sprintf(tCommandBuffer[0], "cp -a \"%s%s\" \"%s%s\"\n", mCoreDirectory, pCommand->mStrings[0], mBuildDirectory, pCommand->mStrings[1]);
        }
    }
    //--Delete a file.
    else if(pCommand->mInstructionCode == BP_INSTRUCTION_DELETE_FILE)
    {
        if(mCommandsType == BP_SHELL_DOS)
        {
            sprintf(tCommandBuffer[0], "del \"%s%s\" /Q\n", mBuildDirectory, pCommand->mStrings[0]);
        }
        else if(mCommandsType == BP_SHELL_BASH)
        {
            sprintf(tCommandBuffer[0], "rm -f \"%s%s\"\n", mBuildDirectory, pCommand->mStrings[0]);
        }
    }
    //--Delete a folder.
    else if(pCommand->mInstructionCode == BP_INSTRUCTION_DELETE_FOLDER)
    {
        if(mCommandsType == BP_SHELL_DOS)
        {
            sprintf(tCommandBuffer[0], "rmdir \"%s%s\" /Q /S\n", mBuildDirectory, pCommand->mStrings[0]);
        }
        else if(mCommandsType == BP_SHELL_BASH)
        {
            sprintf(tCommandBuffer[0], "rm -rf \"%s%s\"\n", mBuildDirectory, pCommand->mStrings[0]);
        }
    }
    //--Change an icon.
    else if(pCommand->mInstructionCode == BP_INSTRUCTION_CHANGEICON)
    {
        if(mCommandsType == BP_SHELL_DOS)
        {
            char tResourceHackerBuf[256];
            strcpy(tResourceHackerBuf, "C:/Development/resourcehacker/ResourceHacker.exe");
            sprintf(tCommandBuffer[0], "%s -open \"%s%s\" -save \"%s%s\" -action addoverwrite -res %s%s -mask ICONGROUP,A,\n", tResourceHackerBuf, mBuildDirectory, pCommand->mStrings[0], mBuildDirectory, pCommand->mStrings[0], mCoreDirectory, pCommand->mStrings[1]);
        }
        else if(mCommandsType == BP_SHELL_BASH)
        {
            //--No icons on Unix systems.
        }
    }

    //--Write/Execute the commands.
    for(int i = 0; i < BP_COMMANDS_MAX; i ++)
    {
        //--Write the command:
        if(fOutputFile) fprintf(fOutputFile, tCommandBuffer[i]);

        //--Execute it:
        if(!pIsEmulation) system(tCommandBuffer[i]);
    }
}

//===================================== Private Core Methods ======================================
//============================================ Update =============================================
void BuildPackage::Execute(bool pIsEmulation, FILE *fOutfile)
{
    //--Executes all commands in order. Outfile can legally be NULL.
    BuildPackageInstruction *rInstruction = (BuildPackageInstruction *)mInstructionList->PushIterator();
    while(rInstruction)
    {
        ExecuteCommand(rInstruction, pIsEmulation, fOutfile);
        rInstruction = (BuildPackageInstruction *)mInstructionList->AutoIterate();
    }
}
void BuildPackage::ExecuteToPath(bool pIsEmulation, const char *pPath)
{
    //--Executes, using the file at the given path as the output.
    if(!pPath) return;

    //--File handling.
    FILE *fOutfile = fopen(pPath, "wb");
    if(!fOutfile) return;

    //--Call overload.
    Execute(pIsEmulation, fOutfile);

    //--Clean.
    fclose(fOutfile);
}

//=========================================== File I/O ============================================
//=========================================== Drawing =============================================
//======================================= Pointer Routing =========================================
//====================================== Static Functions =========================================
SugarLinkedList *BuildPackage::FetchBuildList()
{
    //--Returns the build list, or initializes it if it has not been booted yet.
    if(!xBuildList) xBuildList = new SugarLinkedList(true);
    return xBuildList;
}
void BuildPackage::Scan()
{
    //--Scans the builds directory for build files. Executes them to construct build profiles.
    SugarFileSystem *tFileSystem = new SugarFileSystem();
    tFileSystem->ScanDirectory("BuildFiles/Build Scripts");

    //--For each...
    int tTotalFiles = tFileSystem->GetTotalEntries();
    for(int i = 0; i < tTotalFiles; i ++)
    {
        //--Get info.
        FileInfo *rFileInfo = tFileSystem->GetEntry(i);
        if(!rFileInfo) continue;

        //--If it's not a .lua file, skip it.
        if(!SugarFileSystem::IsFileExtension(rFileInfo->mPath, ".lua")) continue;

        //--Execute.
        LuaManager::Fetch()->ExecuteLuaFile(rFileInfo->mPath);
    }

    //--Clean.
    delete tFileSystem;
}

//========================================= Lua Hooking ===========================================
void BuildPackage::HookToLuaState(lua_State *pLuaState)
{
    /* BP_Exists(sBuildName) (1 Boolean)
       Returns true if the build was already registered, false if not. */
    lua_register(pLuaState, "BP_Exists", &Hook_BP_Exists);

    /* BP_Create(sBuildName)
       Creates and pushes a BuildPackage. Pop it when done. */
    lua_register(pLuaState, "BP_Create", &Hook_BP_Create);

    /* BP_SetProperty("Core Directory", sPath)
       BP_SetProperty("Build Directory", sPath)
       BP_SetProperty("Register Command", "Setup Build Directory")
       BP_SetProperty("Register Command", "Create Directory", sDirectoryName)
       BP_SetProperty("Register Command", "Copy File", sCoreName, sBuildDest)
       BP_SetProperty("Register Command", "Delete File", sBuildFile)
       BP_SetProperty("Register Command", "Delete Folder", sBuildFile)
       BP_SetProperty("Register Command", "Change Icon", sBuildFile, sIconPath)
       Sets a property in the active BuildPackage. */
    lua_register(pLuaState, "BP_SetProperty", &Hook_BP_SetProperty);
}

//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
int Hook_BP_Exists(lua_State *L)
{
    //BP_Exists(sBuildName) (1 Boolean)

    //--Argument check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) { LuaArgError("BP_Exists"); lua_pushboolean(L, false); return 1; }

    //--Check.
    SugarLinkedList *rStaticBuildList = BuildPackage::FetchBuildList();
    void *rCheckPtr = rStaticBuildList->GetElementByName(lua_tostring(L, 1));
    lua_pushboolean(L, (rCheckPtr != NULL));
    return 1;
}
int Hook_BP_Create(lua_State *L)
{
    //BP_Create(sBuildName)
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    rDataLibrary->PushActiveEntity();

    //--Argument check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("BP_Create");

    //--Create.
    BuildPackage *nPackage = new BuildPackage();
    rDataLibrary->rActiveObject = nPackage;

    //--Register.
    SugarLinkedList *rStaticBuildList = BuildPackage::FetchBuildList();
    rStaticBuildList->AddElement(lua_tostring(L, 1), nPackage, &RootObject::DeleteThis);

    //--Finish.
    return 0;
}
int Hook_BP_SetProperty(lua_State *L)
{
    //BP_SetProperty("Core Directory", sPath)
    //BP_SetProperty("Build Directory", sPath)
    //BP_SetProperty("Register Command", "Setup Build Directory")
    //BP_SetProperty("Register Command", "Create Directory", sDirectoryName)
    //BP_SetProperty("Register Command", "Copy File", sCoreName, sBuildDest)
    //BP_SetProperty("Register Command", "Copy Folder", sCoreName, sBuildDest)
    //BP_SetProperty("Register Command", "Delete File", sBuildFile)
    //BP_SetProperty("Register Command", "Delete Folder", sBuildFile)
    //BP_SetProperty("Register Command", "Change Icon", sBuildFile, sIconPath)

    //--Argument check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("BP_SetProperty");

    //--Type check.
    if(!DataLibrary::Fetch()->IsActiveValid(POINTER_TYPE_BUILDPACKAGE)) return LuaTypeError("BP_SetProperty");
    BuildPackage *rBuildPackage = (BuildPackage *)DataLibrary::Fetch()->rActiveObject;

    //--Setup
    const char *rSwitchType = lua_tostring(L, 1);

    //--Sets the core directory where files are copied from.
    if(!strcasecmp("Core Directory", rSwitchType) && tArgs == 2)
    {
        rBuildPackage->SetCoreDirectory(lua_tostring(L, 2));
    }
    //--Sets the build directory where files end up after copying.
    else if(!strcasecmp("Build Directory", rSwitchType) && tArgs == 2)
    {
        rBuildPackage->SetBuildDirectory(lua_tostring(L, 2));
    }
    //--Command Registration.
    else if(!strcasecmp("Register Command", rSwitchType) && tArgs >= 2)
    {
        //--Command switchtype.
        const char *rCommandType = lua_tostring(L, 2);

        //--Sets up the build directory, archiving previous versions.
        if(!strcasecmp("Setup Build Directory", rCommandType) && tArgs == 2)
        {
            rBuildPackage->RegisterSetupBuildDirectory();
        }
        //--Creates a directory.
        else if(!strcasecmp("Create Directory", rCommandType) && tArgs == 3)
        {
            rBuildPackage->RegisterCreateFolderCommand(lua_tostring(L, 3));
        }
        //--Copies a file.
        else if(!strcasecmp("Copy File", rCommandType) && tArgs == 4)
        {
            rBuildPackage->RegisterCopyFileCommand(lua_tostring(L, 3), lua_tostring(L, 4));
        }
        //--Copies a folder.
        else if(!strcasecmp("Copy Folder", rCommandType) && tArgs == 4)
        {
            rBuildPackage->RegisterCopyFolderCommand(lua_tostring(L, 3), lua_tostring(L, 4));
        }
        //--Deletes a file from the build. Doesn't affect the core.
        else if(!strcasecmp("Delete File", rCommandType) && tArgs == 3)
        {
            rBuildPackage->RegisterDeleteFileCommand(lua_tostring(L, 3));
        }
        //--Deletes a folder from the build. Doesn't affect the core.
        else if(!strcasecmp("Delete Folder", rCommandType) && tArgs == 3)
        {
            rBuildPackage->RegisterDeleteFolderCommand(lua_tostring(L, 3));
        }
        //--Changes an icon in a build file. Doesn't affect the core. Icon path is relative to the core.
        else if(!strcasecmp("Change Icon", rCommandType) && tArgs == 4)
        {
            rBuildPackage->RegisterChangeIconCommand(lua_tostring(L, 3), lua_tostring(L, 4));
        }

        //--Error case.
        else
        {
            fprintf(stderr, "%s: Failed, cannot resolve command %s %s with %i args. %s.\n", "BP_SetProperty", rSwitchType, rCommandType, tArgs, LuaManager::Fetch()->GetCallStack(0));
            return 0;
        }
    }
    //--Error case.
    else
    {
        return LuaPropertyError("BP_SetProperty", rSwitchType, tArgs);
    }

    return 0;
}
