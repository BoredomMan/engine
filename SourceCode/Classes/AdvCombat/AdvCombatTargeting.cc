//--Base
#include "AdvCombat.h"

//--Classes
#include "AdvCombatEntity.h"

//--CoreClasses
#include "SugarLinkedList.h"

//--Definitions
#include "AdvCombatDefStruct.h"

//--Libraries
//--Managers
#include "DebugManager.h"

//===================================== Structure Functions =======================================
//--[Target Cluster]
//--Defined in AdvCombatStruct.h
//--TargetCluster is a wrapper around a SugarLinkedList that contains a set of AdvCombatEntities.
//  The list does not allow duplicates.
void TargetCluster::Initialize()
{
    mDisplayName = InitializeString("Unnamed");
    mrTargetList = new SugarLinkedList(false);
}
void TargetCluster::DeleteThis(void *pPtr)
{
    TargetCluster *rPtr = (TargetCluster *)pPtr;
    free(rPtr->mDisplayName);
    delete rPtr->mrTargetList;
    free(rPtr);
}
void TargetCluster::SetDisplayName(const char *pName)
{
    if(!pName) return;
    ResetString(mDisplayName, pName);
}
void TargetCluster::RegisterElement(void *pElement)
{
    if(mrTargetList->IsElementOnList(pElement)) return;
    mrTargetList->AddElement("X", pElement);
}
void TargetCluster::RemoveElementI(int pSlot)
{
    mrTargetList->RemoveElementI(pSlot);
}
void TargetCluster::RemoveElementP(void *pPtr)
{
    mrTargetList->RemoveElementP(pPtr);
}

//=========================================== System ==============================================
void AdvCombat::ClearTargetClusters()
{
    //--Do not remove target clusters via any means except this or Reinitialize(). Target clusters
    //  may be in use, in which case they get locked during ability execution to prevent dereference
    //  errors. Only when an ability is executing do the clusters get locked.
    if(mLockTargetClusters) return;
    mTargetClusterCounter = 0;
    mTargetClusters->ClearList();
}

//====================================== Property Queries =========================================
TargetCluster *AdvCombat::GetTargetCluster(const char *pClusterName)
{
    //--Can return NULL if the cluster doesn't exist.
    return (TargetCluster *)mTargetClusters->GetElementByName(pClusterName);
}
AdvCombatEntity *AdvCombat::GetEntityInCluster(const char *pClusterName, int pSlot)
{
    //--Can return NULL if the cluster doesn't exist, or if the entity doesn't exist.
    TargetCluster *rCluster = GetTargetCluster(pClusterName);
    if(!rCluster) return NULL;
    return (AdvCombatEntity *)rCluster->mrTargetList->GetElementBySlot(pSlot);
}

//========================================= Manipulators ==========================================
TargetCluster *AdvCombat::CreateTargetCluster(const char *pClusterName, const char *pDisplayName)
{
    //--Creates a target cluster with the given name and returns it. If the name is in error, creates
    //  a dummy cluster and barks an error.
    //--Note that the first name is the internal name, the second name is the display name the player
    //  will see. It is possible for the display name and cluster name to be identical, and it is possible
    //  for many clusters to share the same display name, but it is not possible for two clusters to
    //  use the same name. If that happens, the cluster that already has the name is returned instead.
    //--Passing NULL will return a cluster named "DUMMY CLUSTER".

    //--No cluster name or display name: Return "DUMMY CLUSTER".
    if(!pClusterName || !pDisplayName)
    {
        //--If the dummy cluster already exists, just return it.
        TargetCluster *rDummyCluster = (TargetCluster *)mTargetClusters->GetElementByName("DUMMY CLUSTER");
        if(rDummyCluster) return rDummyCluster;

        //--No dummy cluster exists yet, so create one and return that.
        SetMemoryData(__FILE__, __LINE__);
        TargetCluster *nTargetCluster = (TargetCluster *)starmemoryalloc(sizeof(TargetCluster));
        nTargetCluster->Initialize();
        nTargetCluster->SetDisplayName("DUMMY CLUSTER");
        mTargetClusters->AddElementAsTail("DUMMY CLUSTER", nTargetCluster, TargetCluster::DeleteThis);

        //--Bark a warning.
        DebugManager::ForcePrint("AdvCombat:CreateTargetCluster() - Error, cluster name was NULL.\n");
        return nTargetCluster;
    }

    //--Check if the named cluster already exists. If it does, return it.
    TargetCluster *rCheckCluster = (TargetCluster *)mTargetClusters->GetElementByName(pClusterName);
    if(rCheckCluster) return rCheckCluster;

    //--New cluster, create and return.
    mTargetClusterCounter ++;
    SetMemoryData(__FILE__, __LINE__);
    TargetCluster *nTargetCluster = (TargetCluster *)starmemoryalloc(sizeof(TargetCluster));
    nTargetCluster->Initialize();
    nTargetCluster->SetDisplayName(pDisplayName);
    mTargetClusters->AddElementAsTail(pClusterName, nTargetCluster, TargetCluster::DeleteThis);
    return nTargetCluster;
}
void AdvCombat::RegisterElementToCluster(const char *pClusterName, void *pElement)
{
    //--Worker function, adds the given element to the given cluster.
    if(!pClusterName || !pElement) return;

    //--Check if the cluster exists.
    TargetCluster *rTargetCluster = GetTargetCluster(pClusterName);
    if(!rTargetCluster) return;

    //--Add it. Duplicates are implicitly ignored.
    rTargetCluster->RegisterElement(pElement);
}
void AdvCombat::RegisterEntityToClusterByID(const char *pClusterName, uint32_t pUniqueID)
{
    //--Searches all lists and finds the entity that has the unique ID, then adds them to the named cluster.
    if(!pClusterName || !pUniqueID) return;

    //--Check if the cluster exists.
    TargetCluster *rTargetCluster = GetTargetCluster(pClusterName);
    if(!rTargetCluster) return;

    //--Scan the party roster.
    AdvCombatEntity *rCheckEntity = (AdvCombatEntity *)mPartyRoster->PushIterator();
    while(rCheckEntity)
    {
        if(rCheckEntity->GetID() == pUniqueID)
        {
            rTargetCluster->RegisterElement(rCheckEntity);
            mPartyRoster->PopIterator();
            return;
        }
        rCheckEntity = (AdvCombatEntity *)mPartyRoster->AutoIterate();
    }

    //--Scan the enemy roster.
    rCheckEntity = (AdvCombatEntity *)mEnemyRoster->PushIterator();
    while(rCheckEntity)
    {
        if(rCheckEntity->GetID() == pUniqueID)
        {
            rTargetCluster->RegisterElement(rCheckEntity);
            mEnemyRoster->PopIterator();
            return;
        }
        rCheckEntity = (AdvCombatEntity *)mEnemyRoster->AutoIterate();
    }

    //--Didn't find in either group. Report.
    DebugManager::ForcePrint("AdvCombat:RegisterEntityToClusterByID() - Error, could not find ID %i.\n", pUniqueID);
}
void AdvCombat::RemoveEntityFromClusterI(const char *pClusterName, int pSlot)
{
    //--Removes the element in the given slot from the cluster.
    if(!pClusterName) return;

    //--Check if the cluster exists.
    TargetCluster *rTargetCluster = GetTargetCluster(pClusterName);
    if(!rTargetCluster) return;

    //--Run.
    rTargetCluster->RemoveElementI(pSlot);
}
void AdvCombat::RemoveEntityFromClusterP(const char *pClusterName, void *pPtr)
{
    //--Removes the element with the given pointer from the cluster.
    if(!pClusterName || !pPtr) return;

    //--Check if the cluster exists.
    TargetCluster *rTargetCluster = GetTargetCluster(pClusterName);
    if(!rTargetCluster) return;

    //--Run.
    rTargetCluster->RemoveElementP(pPtr);
}

//======================================= Addition Macros =========================================
//--Macros to quickly add elements to clusters.
void AdvCombat::AddPartyTargetToClusterI(const char *pClusterName, int pSlot)
{
    //--Locates a target in the player's party by slot and adds it to the named cluster.
    RegisterElementToCluster(pClusterName, mrCombatParty->GetElementBySlot(pSlot));
}
void AdvCombat::AddPartyTargetToClusterS(const char *pClusterName, const char *pPartyMemberName)
{
    //--Locates a target in the player's party by slot and adds it to the named cluster.
    RegisterElementToCluster(pClusterName, mrCombatParty->GetElementByName(pPartyMemberName));
}
void AdvCombat::AddEnemyTargetToClusterI(const char *pClusterName, int pSlot)
{
    //--Locates a target in the enemy party by slot and adds it to the named cluster. Note that
    //  this does not check the enemy graveyard.
    RegisterElementToCluster(pClusterName, mrEnemyCombatParty->GetElementBySlot(pSlot));
}
void AdvCombat::AddEnemyTargetToClusterS(const char *pClusterName, const char *pEnemyName)
{
    //--Locates a target in the enemy party by slot and adds it to the named cluster. Note that
    //  this does not check the enemy graveyard.
    RegisterElementToCluster(pClusterName, mrEnemyCombatParty->GetElementByName(pEnemyName));
}

//====================================== Autobuild Macros =========================================
//--Given a caller, populates targets by common codes. This is usually called once and may create
//  many clusters by itself.
void AdvCombat::PopulateTargetsByCode(const char *pCodeName, void *pCaller)
{
    //--Error check:
    if(!pCodeName || !pCaller) return;

    //--Buffer.
    char tNewNameBuf[32];

    //--Target all enemies, each getting its own cluster. Does not target downed enemies. Each
    //  cluster uses the display name of the entity it contains.
    if(!strcasecmp(pCodeName, "Target Enemies Single"))
    {
        //--Resolve which list to use. Target whichever group the caller isn't in.
        SugarLinkedList *rUseList = mrEnemyCombatParty;
        if(rUseList->IsElementOnList(pCaller)) rUseList = mrCombatParty;

        //--For each element on the list, add them as a target.
        AdvCombatEntity *rEntity = (AdvCombatEntity *)rUseList->PushIterator();
        while(rEntity)
        {
            //--Create a name for the cluster. This is guaranteed to be unique.
            sprintf(tNewNameBuf, "AUTOCLUS %03i", mTargetClusterCounter);

            //--Create, add.
            TargetCluster *rNewCluster = CreateTargetCluster(tNewNameBuf, rEntity->GetDisplayName());
            rNewCluster->RegisterElement(rEntity);

            //--Next.
            rEntity = (AdvCombatEntity *)rUseList->AutoIterate();
        }
    }
    //--Target all allies, each getting its own cluster. Does not target downed allies. Each
    //  cluster uses the display name of the entity it contains.
    else if(!strcasecmp(pCodeName, "Target Enemies Single"))
    {
        //--Resolve which list to use. Target whichever group the caller is in.
        SugarLinkedList *rUseList = mrCombatParty;
        if(!rUseList->IsElementOnList(pCaller)) rUseList = mrEnemyCombatParty;

        //--For each element on the list, add them as a target.
        AdvCombatEntity *rEntity = (AdvCombatEntity *)rUseList->PushIterator();
        while(rEntity)
        {
            //--Create a name for the cluster. This is guaranteed to be unique.
            sprintf(tNewNameBuf, "AUTOCLUS %03i", mTargetClusterCounter);

            //--Create, add.
            TargetCluster *rNewCluster = CreateTargetCluster(tNewNameBuf, rEntity->GetDisplayName());
            rNewCluster->RegisterElement(rEntity);

            //--Next.
            rEntity = (AdvCombatEntity *)rUseList->AutoIterate();
        }
    }
    //--Error.
    else
    {
        DebugManager::ForcePrint("AdvCombat:PopulateTargetsByCode() - Error, no macro %s.\n", pCodeName);
    }
}
