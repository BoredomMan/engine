//--Base
#include "AdvCombatAnimation.h"

//--Classes
//--CoreClasses
//--Definitions
//--GUI
//--Libraries
#include "DataLibrary.h"

//--Managers

//========================================= Lua Hooking ===========================================
void AdvCombatAnimation::HookToLuaState(lua_State *pLuaState)
{
    /* AdvCombatAnimation_GetProperty("Dummy") (1 Integer)
       Gets and returns the requested property in the Adventure Combat class. */
    lua_register(pLuaState, "AdvCombatAnimation_GetProperty", &Hook_AdvCombatAnimation_GetProperty);

    /* AdvCombatAnimation_SetProperty("Dummy")
       Sets the property in the Adventure Combat class. */
    lua_register(pLuaState, "AdvCombatAnimation_SetProperty", &Hook_AdvCombatAnimation_SetProperty);
}

//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
int Hook_AdvCombatAnimation_GetProperty(lua_State *L)
{
    ///--[Argument Listing]
    //--[System]
    //AdvCombatAnimation_GetProperty("Dummy") (1 Integer)

    //--Argument check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("AdvCombatAnimation_GetProperty");

    //--Setup
    int tReturns = 0;
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[Static]
    //--Dummy static.
    if(!strcasecmp(rSwitchType, "Static Dummy"))
    {
        lua_pushinteger(L, 0);
        return 1;
    }

    ///--[Dynamic]
    //--Type check.
    if(!DataLibrary::Fetch()->IsActiveValid(POINTER_TYPE_ADVCOMBATANIMATION)) return LuaTypeError("AdvCombatAnimation_GetProperty");
    AdvCombatAnimation *rAnimation = (AdvCombatAnimation *)DataLibrary::Fetch()->rActiveObject;

    ///--[System]
    //--Dummy dynamic.
    if(!strcasecmp(rSwitchType, "Dummy") && tArgs == 1)
    {
        lua_pushinteger(L, 0);
        tReturns = 1;
    }
    //--Error case.
    else
    {
        LuaPropertyError("AdvCombatAnimation_GetProperty", rSwitchType, tArgs);
    }

    return tReturns;
}
int Hook_AdvCombatAnimation_SetProperty(lua_State *L)
{
    ///--[Argument Listing]
    //--[System]
    //AdvCombatAnimation_SetProperty("Dummy")

    //--Argument check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("AdvCombatAnimation_SetProperty");

    //--Setup
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[Static]
    //--Dummy static.
    if(!strcasecmp(rSwitchType, "Static Dummy"))
    {
        return 0;
    }

    ///--[Dynamic]
    //--Type check.
    if(!DataLibrary::Fetch()->IsActiveValid(POINTER_TYPE_ADVCOMBATANIMATION)) return LuaTypeError("AdvCombatAnimation_SetProperty");
    AdvCombatAnimation *rAnimation = (AdvCombatAnimation *)DataLibrary::Fetch()->rActiveObject;

    ///--[System]
    //--Dummy dynamic.
    if(!strcasecmp(rSwitchType, "Dummy") && tArgs == 1)
    {
    }
    //--Error case.
    else
    {
        LuaPropertyError("AdvCombatAnimation_SetProperty", rSwitchType, tArgs);
    }

    return 0;
}
