//--[AdvCombatAnimation]
//--Represents a visual animation in combat, such as a slash on an enemy or a flame from a spell.

#pragma once

//--[Includes]
#include "Definitions.h"
#include "Structures.h"

//--[Local Structures]
//--[Local Definitions]
//--[Classes]
class AdvCombatAnimation : public RootObject
{
    private:

    protected:

    public:
    //--System
    AdvCombatAnimation();
    virtual ~AdvCombatAnimation();

    //--Public Variables
    //--Property Queries
    //--Manipulators
    //--Core Methods
    private:
    //--Private Core Methods
    public:
    //--Update
    //--File I/O
    //--Drawing
    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_AdvCombatAnimation_GetProperty(lua_State *L);
int Hook_AdvCombatAnimation_SetProperty(lua_State *L);

