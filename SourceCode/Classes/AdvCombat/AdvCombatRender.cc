//--Base
#include "AdvCombat.h"

//--Classes
#include "AdvCombatAbility.h"
#include "AdvCombatDefStruct.h"
#include "AdvCombatEntity.h"
#include "AdvCombatJob.h"
#include "AdventureLevel.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarFont.h"
#include "SugarLinkedList.h"
#include "StarlightString.h"

//--Definitions
#include "EasingFunctions.h"

//--Libraries
//--Managers
#include "DisplayManager.h"

void AdvCombat::Render()
{
    ///--[Documentation and Setup]
    //--Renders the Adventure Combat interface. Unlike many other objects using the vis-timer format, Adventure Combat does not
    //  use a global mixer alpha.
    if(!Images.mIsReady || !mIsActive) return;

    //--If the world is still pulsing, don't render.
    AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
    if(rActiveLevel && rActiveLevel->IsPulsing()) return;

    ///--[Subhandlers]
    //--Subhandlers can take over rendering. First, combat is over:
    if(mCombatResolution != ADVCOMBAT_END_NONE) { RenderResolution();   return; }
    if(mIsIntroduction)                         { RenderIntroduction(); return; }

    //--Anything below this point is "Normal" combat rendering.

    ///--[Grey Backing]
    //--Flat grey backing.
    SugarBitmap::DrawFullBlack(ADVCOMBAT_STD_BACKING_OPACITY);

    //--Component rendering.
    RenderAllyBars();
    RenderTurnOrder();
    RenderNewTurn();

    //--Player's interface.
    if(mPlayerInterfaceTimer > 0)
    {
        //--Compute percent.
        float cPct = EasingFunction::QuadraticInOut(mPlayerInterfaceTimer, ADVCOMBAT_MOVE_TRANSITION_TICKS);

        //--Compute offset.
        float cMaxOffset = 300.0f;
        float cOffset = cMaxOffset * (1.0f - cPct);

        //--Translate, render.
        glTranslatef(0.0f, cOffset, 0.0f);
        RenderPlayerBar();
        glTranslatef(0.0f, -cOffset, 0.0f);
    }

    //--[Overlays]
    //--Confirmation window overlay.
    RenderConfirmation();
}
void AdvCombat::RenderAllyBars()
{
    ///--[Documentation and Setup]
    //--Renders the bars in the top left that show the HP/MP/Etc of the player's party.
    if(!Images.mIsReady) return;

    //--Positions
    float cYPos = 0.0f;
    float cYStep = 25.0f;

    //--For each party member:
    int tMaskCode = ACE_STENCIL_ALLY_PORTRAIT_START;
    AdvCombatEntity *rEntity = (AdvCombatEntity *)mrActiveParty->PushIterator();
    while(rEntity)
    {
        //--Render the backing.
        Images.Data.rAllyFrame->Draw(0.0f, cYPos);

        //--Mask.
        DisplayManager::ActivateMaskRender(tMaskCode);
        Images.Data.rAllyPortraitMask->Draw(0.0f, cYPos);

        //--Now render only on pixels the mask rendered on.
        DisplayManager::ActivateStencilRender(tMaskCode);
        TwoDimensionRealPoint cRenderCoords = rEntity->GetUIRenderPosition(ACE_UI_INDEX_COMBAT_ALLY);
        SugarBitmap *rCombatPortrait = rEntity->GetCombatPortrait();
        if(rCombatPortrait)
        {
            rCombatPortrait->Draw(0.0f + cRenderCoords.mXCenter, cYPos + cRenderCoords.mYCenter);
        }

        //--Disable stencilling.
        DisplayManager::DeactivateStencilling();

        //--Render the HP.
        int tHP = rEntity->GetHealth();
        int tHPMax = rEntity->GetStatistic(ADVCE_STATS_FINAL, STATS_HPMAX);
        Images.Data.rAllyHPFont->DrawTextArgs(175.0f, 24.0f + cYPos - 14.0f, SUGARFONT_RIGHTALIGN_X, 1.0f, "%i/%i", tHP, tHPMax);

        //--Next.
        tMaskCode ++;
        cYPos = cYPos + cYStep;
        rEntity = (AdvCombatEntity *)mrActiveParty->AutoIterate();
    }
}
void AdvCombat::RenderTurnOrder()
{
    ///--[Documentation and Setup]
    //--Renders the turn order in the top right.
    if(!Images.mIsReady) return;

    //--Render the left component of the bar at the end of its width.
    float cLftPos = VIRTUAL_CANVAS_X - (mTurnWidCurrent + Images.Data.rTurnOrderEdge->GetWidth());
    float cTopPos = 6.0f;
    Images.Data.rTurnOrderEdge->Draw(cLftPos, cTopPos);

    //--Backing bar.
    cLftPos = cLftPos + Images.Data.rTurnOrderEdge->GetWidth();
    float cRgtPos = VIRTUAL_CANVAS_X;
    float cBotPos = cTopPos + Images.Data.rTurnOrderBack->GetHeight();
    Images.Data.rTurnOrderBack->Bind();
    glBegin(GL_QUADS);
        glTexCoord2f(0.0f, 1.0f); glVertex2f(cLftPos, cTopPos);
        glTexCoord2f(1.0f, 1.0f); glVertex2f(cRgtPos, cTopPos);
        glTexCoord2f(1.0f, 0.0f); glVertex2f(cRgtPos, cBotPos);
        glTexCoord2f(0.0f, 0.0f); glVertex2f(cLftPos, cBotPos);
    glEnd();

    //--[Portraits]
    //--Render the portraits in the opposite order. The acting character is on the right side.
    float cRenderW = 48.0f;
    float cRenderX = cLftPos - 24.0f + (mrTurnOrder->GetListSize() * cRenderW) - cRenderW;
    float cRenderY = 5.0f;
    AdvCombatEntity *rTurnEntity = (AdvCombatEntity *)mrTurnOrder->PushIterator();
    while(rTurnEntity)
    {
        //--Get the turn order portrait.
        SugarBitmap *rTurnIcon = rTurnEntity->GetTurnIcon();
        if(rTurnIcon) rTurnIcon->Draw(cRenderX, cRenderY);

        //--Next.
        cRenderX = cRenderX - cRenderW;
        rTurnEntity = (AdvCombatEntity *)mrTurnOrder->AutoIterate();
    }

    //--Turn marker. Renders over the portrait.
    Images.Data.rTurnOrderCircle->Draw();
}
void AdvCombat::RenderPlayerBar()
{
    ///--[Documentation and Setup]
    //--Renders the player's turn commands bar on the bottom of the screen. This should not render unless
    //  a player-controlled character is acting.
    if(!Images.mIsReady) return;

    //--Get the active character.
    AdvCombatEntity *rActiveEntity = (AdvCombatEntity *)mrTurnOrder->GetElementBySlot(0);
    if(!rActiveEntity) return;

    //--Color Setup.
    StarlightColor cGrey = StarlightColor::MapRGBAF(0.5f, 0.5f, 0.5f, 1.0f);

    ///--[Character Portrait]
    //--Setup.
    SugarBitmap *rCharacterPortrait = rActiveEntity->GetCombatPortrait();
    TwoDimensionRealPoint cRenderPos = rActiveEntity->GetUIRenderPosition(ACE_UI_INDEX_COMBAT_MAIN);

    //--Render the inset.
    Images.Data.rMainPortraitRingBack->Draw();

    //--Switch to character masking.
    DisplayManager::ActivateMaskRender(ACE_STENCIL_ACTIVE_CHARACTER_PORTRAIT);
    Images.Data.rMainPortraitMask->Draw();
    DisplayManager::ActivateStencilRender(ACE_STENCIL_ACTIVE_CHARACTER_PORTRAIT);
    if(rCharacterPortrait) rCharacterPortrait->Draw(cRenderPos.mXCenter, cRenderPos.mYCenter);
    DisplayManager::DeactivateStencilling();

    //--Render the frame and name banner.
    Images.Data.rMainPortraitRing->Draw();
    Images.Data.rMainNameBanner->Draw();

    //--Render the character's display name.
    const char *rDisplayName = rActiveEntity->GetDisplayName();
    if(rDisplayName)
    {
        float cCenterX = 129.0f;
        float cCenterY = 665.0f;
        Images.Data.rActiveCharacterNameFont->DrawTextA(cCenterX, cCenterY, SUGARFONT_AUTOCENTER_XY, 1.0f, rDisplayName);
    }

    ///--[Health Bar]
    //--Health Bar by percentage. First, render the backing.
    Images.Data.rMainHealthBarBack->Draw();

    //--Get the HP percent and render that much of the health bar.
    float cHPPercent = rActiveEntity->GetHealthPercent();
    if(cHPPercent > 0.0f)
    {
        //--Sizes.
        float cLft = Images.Data.rMainHealthBarFill->GetXOffset();
        float cTop = Images.Data.rMainHealthBarFill->GetYOffset();
        float cRgt = cLft + (Images.Data.rMainHealthBarFill->GetWidth() * cHPPercent);
        float cBot = cTop + (Images.Data.rMainHealthBarFill->GetHeight());

        Images.Data.rMainHealthBarFill->Bind();
        glBegin(GL_QUADS);
            glTexCoord2f(      0.0f, 1.0f); glVertex2f(cLft, cTop);
            glTexCoord2f(cHPPercent, 1.0f); glVertex2f(cRgt, cTop);
            glTexCoord2f(cHPPercent, 0.0f); glVertex2f(cRgt, cBot);
            glTexCoord2f(      0.0f, 0.0f); glVertex2f(cLft, cBot);
        glEnd();
    }

    //--Render the frame.
    Images.Data.rMainHealthBarFrame->Draw();

    ///--[Ability Display]
    //--Render all the character's available ability icons and a cursor for the player to interact with.
    Images.Data.rAbilityFrameMain->Draw();

    //--Positions.
    float cAbltyLft = ADVCOMBAT_POSITION_ABILITY_X;
    float cAbltyTop = ADVCOMBAT_POSITION_ABILITY_Y;
    float cAbltyWid = ADVCOMBAT_POSITION_ABILITY_W;
    float cAbltyHei = ADVCOMBAT_POSITION_ABILITY_H;

    //--Render abilities.
    for(int x = 0; x < ACE_ABILITY_GRID_SIZE_X; x ++)
    {
        for(int y = 0; y < ACE_ABILITY_GRID_SIZE_Y; y ++)
        {
            //--Skip empty slots.
            AdvCombatAbility *rAbility = rActiveEntity->GetAbilityBySlot(x, y);
            if(!rAbility) continue;

            //--Resolve position.
            float cRenderX = cAbltyLft + (cAbltyWid * x);
            float cRenderY = cAbltyTop + (cAbltyHei * y);

            //--Get rendering images.
            SugarBitmap *rImageBack = rAbility->GetIconBack();
            SugarBitmap *rImage     = rAbility->GetIcon();

            //--If the ability cannot be used for any reason, grey it out.
            if(rAbility->IsUsableNow() == false) cGrey.SetAsMixer();

            //--Render.
            if(rImageBack) rImageBack->Draw(cRenderX, cRenderY);
            if(rImage)     rImage    ->Draw(cRenderX, cRenderY);

            //--Clean.
            if(rAbility->IsUsableNow() == false) StarlightColor::ClearMixer();
        }
    }

    //--Render the ability highlight.
    Images.Data.rAbilityHighlight->Draw(mAbilityHighlightPack.mXCur, mAbilityHighlightPack.mYCur);

    ///--[Description Display]
    //--Backing.
    Images.Data.rDescriptionWindow->Draw();

    //--Render the description of the hovered ability here.
    AdvCombatAbility *rHighlightedAbility = rActiveEntity->GetAbilityBySlot(mAbilitySelectionX, mAbilitySelectionY);
    if(rHighlightedAbility)
    {
        //--Position.
        float cTxtLft = 839.0f;
        float cTxtTop = 557.0f;

        //--Render.
        StarlightString *rDescription = rHighlightedAbility->GetDescription();
        if(rDescription)
        {
            rDescription->DrawText(cTxtLft, cTxtTop, 0, 1.0f, Images.Data.rAbilityDescriptionFont);
        }
    }
}
void AdvCombat::RenderNewTurn()
{
    ///--[Documentation and Setup]
    //--Renders the "TURN X" at the top of the screen. Does nothing if the time has expired.
    if(mTurnDisplayTimer >= ACE_TURN_DISPLAY_TICKS || !Images.mIsReady) return;

    //--Timer setup.
    int cLetterTicks = ACE_TURN_TICKS_PER_LETTER * 5;
    int cHoldTicks = cLetterTicks + ACE_TURN_TICKS_HOLD;

    //--Populate the buffer. Note that turn 0 renders as "TURN 1". We never pass "TURN 1000", it just stays there
    //  to prevent overflow. The actual turn counter continues to rise.
    int tUseTurns = mCurrentTurn+1;
    if(tUseTurns > 1000) tUseTurns = 1000;
    char tFullBuffer[10];
    sprintf(tFullBuffer, "TURN %i", tUseTurns);

    //--Positions
    float cXStart = (VIRTUAL_CANVAS_X * 0.50f) - (Images.Data.rNewTurnFont->GetTextWidth(tFullBuffer) * 0.50f);
    float cYStart = VIRTUAL_CANVAS_Y * 0.00f;
    float cYEnd   = VIRTUAL_CANVAS_Y * 0.10f;

    //--If we're in the displaying new letters part:
    if(mTurnDisplayTimer < cLetterTicks)
    {
        //--For each letter in "TURN"
        float tXPos = cXStart;
        int tUseTicks = mTurnDisplayTimer;
        for(int i = 0; i < 4; i ++)
        {
            //--Percentage drop. The letter starts at the top of the screen and moves down.
            float cPercent = EasingFunction::QuadraticOut(tUseTicks, ACE_TURN_TICKS_PER_LETTER);
            float cYPos = cYStart + ((cYEnd - cYStart) * cPercent);

            //--Render.
            StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cPercent);
            float tIncrement = Images.Data.rNewTurnFont->DrawLetter(tXPos, cYPos, 0, 1.0f, tFullBuffer[i], tFullBuffer[i+1]);
            tXPos = tXPos + tIncrement;

            //--Clean.
            StarlightColor::ClearMixer();

            //--Next. End if the letter should not display.
            tUseTicks -= ACE_TURN_TICKS_PER_LETTER;
            if(tUseTicks <= 0) break;
        }
    }
    //--If we're in the "Hold" part:
    else if(mTurnDisplayTimer < cHoldTicks)
    {
        Images.Data.rNewTurnFont->DrawTextA(cXStart, cYEnd, 0, 1.0f, tFullBuffer);
    }
    //--If we're in the "Fade" part:
    else
    {
        float cAlpha = 1.0f - EasingFunction::QuadraticOut(mTurnDisplayTimer - cHoldTicks, ACE_TURN_TICKS_FADE);
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha);
        Images.Data.rNewTurnFont->DrawTextA(cXStart, cYEnd, 0, 1.0f, tFullBuffer);
        StarlightColor::ClearMixer();
    }
}
