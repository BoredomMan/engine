//--Base
#include "AdvCombat.h"

//--Classes
#include "AdvCombatAbility.h"
#include "AdvCombatEntity.h"

//--CoreClasses
#include "SugarLinkedList.h"

//--Definitions
#include "AdvCombatDefStruct.h"

//--Libraries
//--Managers
#include "AudioManager.h"
#include "ControlManager.h"

void AdvCombat::MovePlayerHighlightTo(int pX, int pY)
{
    //--Moves the highlight position to the marked slot.
    float cXPos = ADVCOMBAT_POSITION_ABILITY_X + (pX * ADVCOMBAT_POSITION_ABILITY_W);
    float cYPos = ADVCOMBAT_POSITION_ABILITY_Y + (pY * ADVCOMBAT_POSITION_ABILITY_H);
    mAbilityHighlightPack.MoveTo(cXPos, cYPos, ADVCOMBAT_HIGHLIGHT_MOVE_TICKS);
}
void AdvCombat::HandlePlayerControls()
{
    ///--[Documentation and Setup]
    //--Handles player controls when an entity with no AI is acting.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Get the acting entity.
    AdvCombatEntity *rActingEntity = (AdvCombatEntity *)mrTurnOrder->GetElementBySlot(0);
    if(!rActingEntity) return;

    ///--[Timers]
    //--Player interface timer runs to ADVCOMBAT_MOVE_TRANSITION_TICKS. This causes it to slide up.
    if(mPlayerInterfaceTimer < ADVCOMBAT_MOVE_TRANSITION_TICKS) mPlayerInterfaceTimer ++;

    ///--[First tick]
    //--If this is the first tick that the player has gained control, we need to reposition the ability
    //  selection. It is implied, as this is the first tick, that target selection is not occurring.
    if(mActionFirstTick)
    {
        //--Flag.
        mActionFirstTick = false;

        //--Reposition the ability highlight.
        mAbilitySelectionX = 0;
        mAbilitySelectionY = 0;
        mAbilityHighlightPack.MoveTo(ADVCOMBAT_POSITION_ABILITY_X, ADVCOMBAT_POSITION_ABILITY_Y, 0);
    }

    //--[Debug]
    //--If the player presses the 9 key, they win! This is for debug.
    if(rControlManager->IsFirstPress("Key_9"))
    {
        BeginResolutionSequence(ADVCOMBAT_END_VICTORY);
    }

    ///--[Ability Selection]
    //--Selecting which ability to use this action.
    if(!mIsSelectingTargets)
    {
        //--[Activate]
        //--Pressing the activate key typically goes into target selection. Some abilities, like retreat
        //  or surrender, may bring up confirmation windows instead.
        if(rControlManager->IsFirstPress("Activate"))
        {
            //--Get the ability in question. If it doesn't exist, play the failure sound.
            AdvCombatAbility *rAbility = rActingEntity->GetAbilityBySlot(mAbilitySelectionX, mAbilitySelectionY);
            if(!rAbility)
            {
                AudioManager::Fetch()->PlaySound("Menu|Failed");
            }
            //--If the ability exists, but is unusuable, play the failure sound.
            else if(rAbility->IsUsableNow())
            {
                AudioManager::Fetch()->PlaySound("Menu|Failed");
            }
            //--If the ability opens a confirmation window without painting targets, do there here:
            else if(rAbility->ActivatesConfirmation())
            {
                ActivateConfirmationWindow();
                SetConfirmationText(rAbility->GetConfirmationText());
                SetConfirmationFunc(&AdvCombat::ConfirmExecuteAbility);
                rActiveAbility = rAbility;
                rActiveTargetCluster = NULL;
            }
            //--The ability exists and is usable, but does not need confirmation. Fire its target painting script.
            else
            {
                //--Execute the script.
                ClearTargetClusters();
                rAbility->CallTargetPainting();

                //--If the script didn't paint any targets, fail.
                if(mTargetClusters->GetListSize() < 1)
                {
                    AudioManager::Fetch()->PlaySound("Menu|Failed");
                }
                //--Script painted at least one target cluster, switch to targeting mode.
                else
                {
                    mIsSelectingTargets = true;
                    rActiveAbility = rAbility;
                    rActiveTargetCluster = NULL;
                }
            }

        }

        //--[Cancel]
        //--Pressing cancel attempts to pass the player's turn. This brings up a confirmation window.
        if(rControlManager->IsFirstPress("Cancel"))
        {
        }

        //--[Directional Keys]
        //--Directional keys change where the ability highlight is.
        bool tRepositionHighlight = false;
        if(rControlManager->IsFirstPress("Left"))
        {
            tRepositionHighlight = true;
            mAbilitySelectionX --;
            if(mAbilitySelectionX < 0) mAbilitySelectionX = ACE_ABILITY_GRID_SIZE_X - 1;
        }
        if(rControlManager->IsFirstPress("Right"))
        {
            tRepositionHighlight = true;
            mAbilitySelectionX ++;
            if(mAbilitySelectionX >= ACE_ABILITY_GRID_SIZE_X) mAbilitySelectionX = 0;
        }
        if(rControlManager->IsFirstPress("Up"))
        {
            tRepositionHighlight = true;
            mAbilitySelectionY --;
            if(mAbilitySelectionY < 0) mAbilitySelectionY = ACE_ABILITY_GRID_SIZE_Y - 1;
        }
        if(rControlManager->IsFirstPress("Down"))
        {
            tRepositionHighlight = true;
            mAbilitySelectionY ++;
            if(mAbilitySelectionY >= ACE_ABILITY_GRID_SIZE_Y) mAbilitySelectionY = 0;
        }

        //--If this flag was set, we need to reposition the highlight.
        if(tRepositionHighlight)
        {
            MovePlayerHighlightTo(mAbilitySelectionX, mAbilitySelectionY);
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
    }
    ///--[Target Selection]
    //--Selecting which target to execute the ability on.
    else
    {

    }
}
void AdvCombat::ExecuteAbility(AdvCombatAbility *pAbility, TargetCluster *pTargetCluster)
{
    //--Executes the given ability on the given target cluster. The target cluster can be NULL for some
    //  abilities, but the ability should always be non-NULL.
    fprintf(stderr, "Execute Ability %p %p\n", pAbility, pTargetCluster);
    if(!pAbility)
    {
        rActiveAbility = NULL;
        rActiveTargetCluster = NULL;
        return;
    }

    //--Ask the ability if it needs a target cluster. If it does, and the cluster is NULL, fail.
    if(pAbility->NeedsTargetCluster() && !pTargetCluster)
    {
        rActiveAbility = NULL;
        rActiveTargetCluster = NULL;
        return;
    }

    //--We have an ability and a set of targets, so execute the ability on each target in order.
    fprintf(stderr, "Call!\n");
    pAbility->CallExecution();
}
void AdvCombat::ExecuteActiveAbility()
{
    //--Executes the ability stored in the rConfirmationAbility. Typically called from outside the class
    //  by a static function after the player presses the confirmation button.
    fprintf(stderr, "Execute Active Ability %p %p\n", rActiveAbility, rActiveTargetCluster);
    ExecuteAbility(rActiveAbility, rActiveTargetCluster);
}
