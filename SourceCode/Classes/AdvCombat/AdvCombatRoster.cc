//--Base
#include "AdvCombat.h"

//--Classes
#include "AdvCombatEntity.h"

//--CoreClasses
#include "SugarLinkedList.h"

//--Definitions
//--Libraries
#include "DataLibrary.h"

//--Managers

//--[Property Queries]
int AdvCombat::GetRosterCount()
{
    return mPartyRoster->GetListSize();
}
int AdvCombat::GetActivePartyCount()
{
    return mrActiveParty->GetListSize();
}
int AdvCombat::GetCombatPartyCount()
{
    return mrCombatParty->GetListSize();
}
bool AdvCombat::DoesPartyMemberExist(const char *pInternalName)
{
    //--Used to prevent double-registrations.
    if(!pInternalName) return false;
    return (mPartyRoster->GetElementByName(pInternalName) != NULL);
}

//--[Manipulators]
void AdvCombat::SetPartyBySlot(int pSlot, const char *pPartyMemberName)
{
    //--Error check.
    if(!pPartyMemberName) return;

    //--Locate the character.
    AdvCombatEntity *rEntity = (AdvCombatEntity *)mPartyRoster->GetElementByName(pPartyMemberName);
    if(!rEntity) return;

    //--Insert.
    mrActiveParty->AddElementInSlot(pPartyMemberName, rEntity, pSlot);
}
void AdvCombat::RegisterPartyMember(const char *pInternalName, AdvCombatEntity *pEntity)
{
    //--Registers the given entity to the roster. Doesn't put it on the active party list.
    if(!pInternalName || !pEntity) return;
    mPartyRoster->AddElementAsTail(pInternalName, pEntity, &RootObject::DeleteThis);
}
void AdvCombat::PushPartyMember(const char *pInternalName)
{
    //--Pushes the named party member as the active object, or NULL if not found.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    rDataLibrary->PushActiveEntity();
    if(!pInternalName) return;

    //--Locate, push if it exists.
    rDataLibrary->rActiveObject = mPartyRoster->GetElementByName(pInternalName);
}

//--[Core Methods]
void AdvCombat::ClearParty()
{
    mrActiveParty->ClearList();
    mrCombatParty->ClearList();
}
void AdvCombat::FullRestoreParty()
{
    //--Fully restores all active party members. Usually used at rest points.
    AdvCombatEntity *rEntity = (AdvCombatEntity *)mrActiveParty->PushIterator();
    while(rEntity)
    {
        rEntity->FullRestore();
        rEntity = (AdvCombatEntity *)mrActiveParty->AutoIterate();
    }
}
