//--Base
#include "AdvCombat.h"

//--Classes
#include "TilemapActor.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarFont.h"
#include "SugarLinkedList.h"

//--Definitions
#include "AdvCombatDefStruct.h"
#include "EasingFunctions.h"

//--Libraries
//--Managers
#include "DisplayManager.h"
#include "LuaManager.h"

void AdvCombat::AwardXP(int pAmount)
{

}
void AdvCombat::AwardJP(int pAmount)
{

}
void AdvCombat::AwardPlatina(int pAmount)
{

}
void AdvCombat::BeginResolutionSequence(int pSequence)
{
    //--In all cases, set the state/timer to 0.
    mResolutionTimer = 0;
    mResolutionState = 0;
    if(pSequence < 0 || pSequence >= ADVCOMBAT_END_TOTAL) pSequence = ADVCOMBAT_END_NONE;
    mCombatResolution = pSequence;
}
void AdvCombat::UpdateResolution()
{
    //--No resolution, do nothing.
    if(mCombatResolution == ADVCOMBAT_END_NONE)
    {
        return;
    }
    //--Victory.
    else if(mCombatResolution == ADVCOMBAT_END_VICTORY)
    {
        //--Overlay. The word "VICTORY!" displays.
        if(mResolutionState == ADVCOMBAT_VICTORY_OVERLAY)
        {
            mResolutionTimer ++;
            if(mResolutionTimer >= ADVCOMBAT_VICTORY_OVERLAY_TICKS) { mResolutionState ++; mResolutionTimer = 0; }
        }
        //--Finish.
        else
        {
            //--If there is an override script, handle it here:
            if(mCurrentVictoryScript)
            {
                LuaManager::Fetch()->ExecuteLuaFile(mCurrentVictoryScript);
            }
            //--Run the standard victory script if it exists:
            else if(mStandardVictoryScript)
            {
                LuaManager::Fetch()->ExecuteLuaFile(mStandardVictoryScript);
            }

            //--Purge world references.
            WorldRefPack *rPackage = (WorldRefPack *)mWorldReferences->PushIterator();
            while(rPackage)
            {
                if(rPackage->mTurns < 1 && rPackage->rActor)
                {
                    rPackage->rActor->BeginDying();
                }
                rPackage = (WorldRefPack *)mWorldReferences->AutoIterate();
            }
            mWorldReferences->ClearList();

            //--In all cases, deactivate this object.
            Deactivate();
        }
    }
    //--Defeat/Surrender.
    else if(mCombatResolution == ADVCOMBAT_END_DEFEAT || mCombatResolution == ADVCOMBAT_END_SURRENDER)
    {
        //--Defeat doesn't care about player input and doesn't have modes.
        int cTotalTicks = (ADVCOMBAT_DEFEAT_TICKS_LETTERS + ADVCOMBAT_DEFEAT_TICKS_HOLD + ADVCOMBAT_DEFEAT_TICKS_FADEOUT);
        if(mResolutionTimer < cTotalTicks)
        {
            mResolutionTimer ++;
        }
        //--Ending.
        else
        {
            //--If this is a surrender, set this flag.
            if(mCombatResolution == ADVCOMBAT_END_SURRENDER)
            {
                mWasLastDefeatSurrender = true;
            }

            //--If there is an override script, handle it here:
            if(mCurrentDefeatScript)
            {
                LuaManager::Fetch()->ExecuteLuaFile(mCurrentDefeatScript);
            }
            //--Run the standard defeat script if it exists:
            else if(mStandardDefeatScript)
            {
                LuaManager::Fetch()->ExecuteLuaFile(mStandardDefeatScript);
            }

            //--In all cases, deactivate this object.
            Deactivate();
        }
    }
    //--Retreat.
    else if(mCombatResolution == ADVCOMBAT_END_RETREAT)
    {
        //--Retreat just counts to the end.
        if(mResolutionTimer < ADVCOMBAT_RETREAT_TICKS)
        {
            mResolutionTimer ++;
        }
        //--Ending.
        else
        {

            //--If there is an override script, handle it here:
            if(mCurrentRetreatScript)
            {
                LuaManager::Fetch()->ExecuteLuaFile(mCurrentRetreatScript);
            }
            //--Run the standard retreat script if it exists:
            else if(mStandardRetreatScript)
            {
                LuaManager::Fetch()->ExecuteLuaFile(mStandardRetreatScript);
            }

            //--In all cases, deactivate this object.
            Deactivate();
        }
    }
}
void AdvCombat::RenderResolution()
{
    //--Renders the victory handler. This is for all cases of victory: Defeat, surrender, retreat, or victory.
    //  This is basically just a routing call.
    if(mCombatResolution == ADVCOMBAT_END_NONE || !Images.mIsReady) return;

    //--Victory:
    if(mCombatResolution == ADVCOMBAT_END_VICTORY)   RenderVictory();

    //--Defeat and Surrender share functions:
    if(mCombatResolution == ADVCOMBAT_END_DEFEAT)    RenderDefeat();
    if(mCombatResolution == ADVCOMBAT_END_SURRENDER) RenderDefeat();

    //--Retreat:
    if(mCombatResolution == ADVCOMBAT_END_RETREAT) RenderRetreat();
}
void AdvCombat::RenderVictory()
{
    //--[Documentation and Setup]
    //--Victory! The player sees UI showing their party and the spoils of the fight.

    //--[Backing]
    //--Backing underlay.
    SugarBitmap::DrawFullBlack(ADVCOMBAT_STD_BACKING_OPACITY);

    //--[Victory Overlay]
    //--In the first phase, render the "VICTORY!" text.
    if(mResolutionState == ADVCOMBAT_VICTORY_OVERLAY)
    {
        //--Get the full length. We compute the left position using the full length.
        float cRenderX = (VIRTUAL_CANVAS_X * 0.50f) - (Images.Data.rVictoryFont->GetTextWidth("VICTORY!") * 0.50f);
        float cRenderY = VIRTUAL_CANVAS_Y * 0.50f;

        //--Create a buffer with the number of letters we are currently displaying.
        int tLetters = mResolutionTimer / ADVCOMBAT_VICTORY_OVERLAY_TICKS_PER_LETTER;
        if(tLetters >= 1)
        {
            char tBuffer[10];
            memset(tBuffer, 0, sizeof(char) * 10);
            strncpy(tBuffer, "VICTORY!", tLetters);
            Images.Data.rVictoryFont->DrawTextA(cRenderX, cRenderY, SUGARFONT_AUTOCENTER_Y, 1.0f, tBuffer);
        }
    }
}
void AdvCombat::RenderDefeat()
{
    //--[Documentation and Setup]
    //--Shows the defeat overlay and then fades to black.

    //--[Phase 1]
    //--Show the defeat letters.
    if(mResolutionTimer < ADVCOMBAT_DEFEAT_TICKS_LETTERS)
    {
        //--Backing is the normal blackout, advancing to fullblack.
        float cPercent = EasingFunction::QuadraticOut(mResolutionTimer, ADVCOMBAT_DEFEAT_TICKS_LETTERS);
        float cDif = (1.0f - ADVCOMBAT_STD_BACKING_OPACITY) * cPercent;
        SugarBitmap::DrawFullBlack(ADVCOMBAT_STD_BACKING_OPACITY + cDif);

        //--Render the defeat letters.
        int tLettersToRender = mResolutionTimer / ADVCOMBAT_DEFEAT_TICKS_PER_LETTER;
        if(tLettersToRender >= ADVCOMBAT_DEFEAT_FRAMES_TOTAL) tLettersToRender = ADVCOMBAT_DEFEAT_FRAMES_TOTAL;
        for(int i = 0; i < tLettersToRender; i ++)
        {
            Images.Data.rDefeatFrames[i]->Draw();
        }
    }
    //--[Phase 2]
    //--Hold on the full set.
    else if(mResolutionTimer < ADVCOMBAT_DEFEAT_TICKS_LETTERS + ADVCOMBAT_DEFEAT_TICKS_HOLD)
    {
        //--Backing is fullblack.
        SugarBitmap::DrawFullBlack(1.0f);

        //--Render the defeat letters.
        for(int i = 0; i < ADVCOMBAT_DEFEAT_FRAMES_TOTAL; i ++)
        {
            Images.Data.rDefeatFrames[i]->Draw();
        }
    }
    //--[Phase 3]
    //--Full set, black fade over.
    else
    {
        //--Backing is fullblack.
        SugarBitmap::DrawFullBlack(1.0f);

        //--Render the defeat letters.
        for(int i = 0; i < ADVCOMBAT_DEFEAT_FRAMES_TOTAL; i ++)
        {
            Images.Data.rDefeatFrames[i]->Draw();
        }

        //--Black overlay.
        int tUseTimer = mResolutionTimer - (ADVCOMBAT_DEFEAT_TICKS_LETTERS + ADVCOMBAT_DEFEAT_TICKS_HOLD);
        float cPct = EasingFunction::QuadraticOut(tUseTimer, ADVCOMBAT_DEFEAT_TICKS_FADEOUT);
        SugarBitmap::DrawFullBlack(cPct);
    }
}
void AdvCombat::RenderRetreat()
{
    //--Just a black overlay.
    float cPercent = EasingFunction::QuadraticOut(mResolutionTimer, ADVCOMBAT_RETREAT_TICKS);
    float cDif = (1.0f - ADVCOMBAT_STD_BACKING_OPACITY) * cPercent;
    SugarBitmap::DrawFullBlack(ADVCOMBAT_STD_BACKING_OPACITY + cDif);
}
