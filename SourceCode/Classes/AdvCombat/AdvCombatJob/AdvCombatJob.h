//--[AdvCombatJob]
//--Represents a character job, which is a collection of skills, stats, and passive properties.
//  Can alternately be called a "Class", but to avoid confusion is called a Job in the code.

#pragma once

//--[Includes]
#include "Definitions.h"
#include "Structures.h"

//--[Local Structures]
//--[Local Definitions]
#define ADVCJOB_CODE_CREATE 0
#define ADVCJOB_CODE_SWITCHTO 1
#define ADVCJOB_CODE_LEVEL 2
#define ADVCJOB_CODE_MASTER 3
#define ADVCJOB_CODE_COMBATSTART 4

//--[Classes]
class AdvCombatJob : public RootObject
{
    private:
    //--System
    char *mLocalName;
    char *mDisplayName;
    char *mScriptPath;

    protected:

    public:
    //--System
    AdvCombatJob();
    virtual ~AdvCombatJob();

    //--Public Variables
    //--Property Queries
    //--Manipulators
    void SetInternalName(const char *pName);
    void SetDisplayName(const char *pName);
    void SetScriptPath(const char *pPath);

    //--Core Methods
    void AssumeJob(AdvCombatEntity *pCaller);
    void CallAtCombatStart(AdvCombatEntity *pCaller);

    private:
    //--Private Core Methods
    public:
    //--Update
    //--File I/O
    //--Drawing
    //--Pointer Routing
    AdvCombatAbility *GetAbility(int pSlot);

    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_AdvCombatJob_GetProperty(lua_State *L);
int Hook_AdvCombatJob_SetProperty(lua_State *L);

