//--Base
#include "AdvCombatJob.h"

//--Classes
#include "AdvCombatEntity.h"

//--CoreClasses
#include "SugarLinkedList.h"

//--Definitions
#include "AdvCombatDefStruct.h"

//--Libraries
//--Managers
#include "LuaManager.h"

//=========================================== System ==============================================
AdvCombatJob::AdvCombatJob()
{
    //--[RootObject]
    //--System
    mType = POINTER_TYPE_ADVCOMBATJOB;

    //--[AdvCombatJob]
    //--System
    mLocalName = InitializeString("Job");
    mDisplayName = InitializeString("Job");
    mScriptPath = NULL;
}
AdvCombatJob::~AdvCombatJob()
{
    free(mLocalName);
    free(mDisplayName);
    free(mScriptPath);
}

//====================================== Property Queries =========================================
//========================================= Manipulators ==========================================
void AdvCombatJob::SetInternalName(const char *pName)
{
    if(!pName) return;
    ResetString(mLocalName, pName);
}
void AdvCombatJob::SetDisplayName(const char *pName)
{
    if(!pName) return;
    ResetString(mDisplayName, pName);
}
void AdvCombatJob::SetScriptPath(const char *pPath)
{
    ResetString(mScriptPath, pPath);
}

//========================================= Core Methods ==========================================
void AdvCombatJob::AssumeJob(AdvCombatEntity *pCaller)
{
    //--The given caller will assume this job, having their stats and graphics modified as needed.
    //  The caller is placed on the activity stack, but the job can be retrieved with lua commands.
    if(!mScriptPath || !pCaller) return;

    //--Reset the job stats.
    CombatStatistics *rJobStatGroup = pCaller->GetStatisticsGroup(ADVCE_STATS_JOB);
    rJobStatGroup->Zero();

    //--Call the lua script to do the heavy lifting.
    LuaManager::Fetch()->PushExecPop(pCaller, mScriptPath, 1, "N", (float)ADVCJOB_CODE_SWITCHTO);
}
void AdvCombatJob::CallAtCombatStart(AdvCombatEntity *pCaller)
{
    //--At combat start, after the caller has been cleared to neutral, call this to repopulate
    //  passive abilities and effects.
    if(!mScriptPath || !pCaller) return;

    //--Call the lua script to do the heavy lifting.
    LuaManager::Fetch()->PushExecPop(pCaller, mScriptPath, 1, "N", (float)ADVCJOB_CODE_COMBATSTART);
}

//===================================== Private Core Methods ======================================
//============================================ Update =============================================
//=========================================== File I/O ============================================
//=========================================== Drawing =============================================
//======================================= Pointer Routing =========================================
//====================================== Static Functions =========================================
//========================================= Lua Hooking ===========================================
//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
