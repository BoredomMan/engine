//--Base
#include "AdvCombatJob.h"

//--Classes
#include "AdvCombatAbility.h"

//--CoreClasses
//--Definitions
//--GUI
//--Libraries
#include "DataLibrary.h"

//--Managers

//========================================= Lua Hooking ===========================================
void AdvCombatJob::HookToLuaState(lua_State *pLuaState)
{
    /* AdvCombatJob_GetProperty("Dummy") (1 Integer)
       Gets and returns the requested property in the Adventure Combat class. */
    lua_register(pLuaState, "AdvCombatJob_GetProperty", &Hook_AdvCombatJob_GetProperty);

    /* AdvCombatJob_SetProperty("Dummy")
       Sets the property in the Adventure Combat class. */
    lua_register(pLuaState, "AdvCombatJob_SetProperty", &Hook_AdvCombatJob_SetProperty);
}

//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
int Hook_AdvCombatJob_GetProperty(lua_State *L)
{
    ///--[Argument Listing]
    //--[System]
    //AdvCombatJob_GetProperty("Dummy") (1 Integer)

    //--Argument check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("AdvCombatJob_GetProperty");

    //--Setup
    int tReturns = 0;
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[Static]
    //--Dummy static.
    if(!strcasecmp(rSwitchType, "Static Dummy"))
    {
        lua_pushinteger(L, 0);
        return 1;
    }

    ///--[Dynamic]
    //--Type check.
    if(!DataLibrary::Fetch()->IsActiveValid(POINTER_TYPE_ADVCOMBATJOB)) return LuaTypeError("AdvCombatJob_GetProperty");
    AdvCombatJob *rJob = (AdvCombatJob *)DataLibrary::Fetch()->rActiveObject;

    ///--[System]
    //--Dummy dynamic.
    if(!strcasecmp("Dummy", rSwitchType) && tArgs == 1)
    {
        lua_pushinteger(L, 0);
        tReturns = 1;
    }
    //--Error case.
    else
    {
        LuaPropertyError("AdvCombatJob_GetProperty", rSwitchType, tArgs);
    }

    return tReturns;
}
int Hook_AdvCombatJob_SetProperty(lua_State *L)
{
    ///--[Argument Listing]
    //--[System]
    //AdvCombatJob_SetProperty("Script Path", sPath)

    //--Argument check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("AdvCombatJob_SetProperty");

    //--Setup
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[Static]
    //--Dummy static.
    if(!strcasecmp(rSwitchType, "Static Dummy"))
    {
        return 0;
    }

    ///--[Dynamic]
    //--Type check.
    if(!DataLibrary::Fetch()->IsActiveValid(POINTER_TYPE_ADVCOMBATJOB)) return LuaTypeError("AdvCombatJob_SetProperty");
    AdvCombatJob *rJob = (AdvCombatJob *)DataLibrary::Fetch()->rActiveObject;

    ///--[System]
    //--Sets the path this job calls whenever it needs a script call.
    if(!strcasecmp("Script Path", rSwitchType) && tArgs == 2)
    {
        rJob->SetScriptPath(lua_tostring(L, 2));
    }
    //--Error case.
    else
    {
        LuaPropertyError("AdvCombatJob_SetProperty", rSwitchType, tArgs);
    }

    return 0;
}
