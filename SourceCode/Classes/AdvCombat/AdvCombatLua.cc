//--Base
#include "AdvCombat.h"

//--Classes
#include "AdvCombatEntity.h"
#include "AliasStorage.h"

//--CoreClasses
//--Definitions
//--GUI
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "LuaManager.h"

//========================================= Lua Hooking ===========================================
void AdvCombat::HookToLuaState(lua_State *pLuaState)
{
    /* AdvCombat_GetProperty("Dummy") (1 Integer)
       Gets and returns the requested property in the Adventure Combat class. */
    lua_register(pLuaState, "AdvCombat_GetProperty", &Hook_AdvCombat_GetProperty);

    /* AdvCombat_SetProperty("Dummy")
       Sets the property in the Adventure Combat class. */
    lua_register(pLuaState, "AdvCombat_SetProperty", &Hook_AdvCombat_SetProperty);
}

//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
int Hook_AdvCombat_GetProperty(lua_State *L)
{
    ///--[Argument Listing]
    //--[System]
    //--[Enemies]
    //AdvCombat_GetProperty("Generate Unique ID") (1 Integer)

    //--[Party]
    //AdvCombat_GetProperty("Does Party Member Exist", sMemberName) (1 Boolean)

    //--Argument check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("AdvCombat_GetProperty");

    //--Setup
    int tReturns = 0;
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[Static]
    //--Dummy static.
    if(!strcasecmp(rSwitchType, "Static Dummy"))
    {
        lua_pushinteger(L, 0);
        return 1;
    }

    ///--[Dynamic]
    //--Type check. Can never fail, as the Fetch() function performs lazy init.
    AdvCombat *rAdventureCombat = AdvCombat::Fetch();

    ///--[System]
    ///--[Enemies]
    //--Returns an integer that is unique for generating enemy names. Is reset each time combat reinitializes.
    //  Standard naming pattern is "Autogen|XX" where XX is the ID. Use leading zeroes to pad.
    if(!strcasecmp(rSwitchType, "Generate Unique ID") && tArgs == 1)
    {
        lua_pushinteger(L, rAdventureCombat->GetNextEnemyID());
        tReturns = 1;
    }
    ///--[Party]
    //--True if the named member exists, false if not.
    else if(!strcasecmp(rSwitchType, "Does Party Member Exist") && tArgs == 2)
    {
        lua_pushboolean(L, rAdventureCombat->DoesPartyMemberExist(lua_tostring(L, 2)));
        tReturns = 1;
    }
    //--Error case.
    else
    {
        LuaPropertyError("AdvCombat_GetProperty", rSwitchType, tArgs);
    }

    return tReturns;
}
int Hook_AdvCombat_SetProperty(lua_State *L)
{
    ///--[Argument Listing]
    //--[System]
    //AdvCombat_SetProperty("Construct")
    //AdvCombat_SetProperty("Reinitialize")
    //AdvCombat_SetProperty("Activate")
    //AdvCombat_SetProperty("World Pulse", bDontAddNearbyEnemies)
    //AdvCombat_SetProperty("Default Combat Music", sMusicName)

    //--[System Abilities]
    //AdvCombat_SetProperty("Set Retreat Properties", sScriptPath)
    //AdvCombat_SetProperty("Set Surrender Properties", sScriptPath)
    //AdvCombat_SetProperty("Order Retreat")
    //AdvCombat_SetProperty("Order Surrender")

    //--[Options]
    //AdvCombat_SetProperty("Auto Doctor Bag", bFlag)
    //AdvCombat_SetProperty("Tourist Mode", bFlag)
    //AdvCombat_SetProperty("Memory Cursor", bFlag)

    //--[Resolution Handlers]
    //AdvCombat_SetProperty("Unwinnable", bFlag)
    //AdvCombat_SetProperty("Unloseable", bFlag)
    //AdvCombat_SetProperty("Unsurrenderable", bFlag)
    //AdvCombat_SetProperty("Unretreatable", bFlag)
    //AdvCombat_SetProperty("Standard Retreat Script", sScriptPath)
    //AdvCombat_SetProperty("Standard Victory Script", sScriptPath)
    //AdvCombat_SetProperty("Standard Defeat Script", sScriptPath)
    //AdvCombat_SetProperty("Retreat Script", sScriptPath)
    //AdvCombat_SetProperty("Victory Script", sScriptPath)
    //AdvCombat_SetProperty("Defeat Script", sScriptPath)

    //--[Enemies]
    //AdvCombat_SetProperty("Register Enemy", sInternalName, iReinforcementTurns) (Pushes Activity Stack)
    //AdvCombat_SetProperty("Clear Enemy Aliases")
    //AdvCombat_SetProperty("Create Enemy Path", sPathName, sScriptPath)
    //AdvCombat_SetProperty("Create Enemy Alias", sAliasName, sPathName)

    //--[Party]
    //AdvCombat_SetProperty("Push Party Member", sInternalName) (Pushes Activity Stack)
    //AdvCombat_SetProperty("Push Party Member By Slot", iSlot) (Pushes Activity Stack)
    //AdvCombat_SetProperty("Register Party Member", sInternalName) (Pushes Activity Stack)
    //AdvCombat_SetProperty("Clear Party")
    //AdvCombat_SetProperty("Party Slot", iSlot, sPartyMemberName)
    //AdvCombat_SetProperty("Restore Party")

    //--[Targeting]
    //AdvCombat_SetProperty("Clear Target Clusters")
    //AdvCombat_SetProperty("Create Target Cluster", sClusterName, sDisplayName)
    //AdvCombat_SetProperty("Add Target To Cluster", sClusterName, iTargetUniqueID)
    //AdvCombat_SetProperty("Run Target Macro", sMacroName)

    //--Argument check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("AdvCombat_SetProperty");

    //--Setup
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[Static]
    //--Dummy static.
    if(!strcasecmp(rSwitchType, "Static Dummy"))
    {
        return 0;
    }

    ///--[Dynamic]
    //--Type check. Can never fail, as the Fetch() function performs lazy init.
    AdvCombat *rAdventureCombat = AdvCombat::Fetch();

    ///--[System]
    //--Tells the AdvCombat class it can crossload its image pointers.
    if(!strcasecmp(rSwitchType, "Construct") && tArgs == 1)
    {
        rAdventureCombat->Construct();
    }
    //--Clears the AdvCombat back to a blank state. Call this before setting up combat.
    else if(!strcasecmp(rSwitchType, "Reinitialize") && tArgs == 1)
    {
        rAdventureCombat->Reinitialize();
    }
    //--Activates the combat handler. Setup can occur before activation.
    else if(!strcasecmp(rSwitchType, "Activate") && tArgs == 1)
    {
        rAdventureCombat->Activate();
    }
    //--Causes the AdventureLevel to show the combat overlay. Optionally prevents nearby enemies from joining the battle.
    else if(!strcasecmp(rSwitchType, "World Pulse") && tArgs == 2)
    {
        rAdventureCombat->PulseWorld(lua_toboolean(L, 2));
    }
    //--Sets which song plays during combat if no override is in place.
    else if(!strcasecmp(rSwitchType, "Default Combat Music") && tArgs == 2)
    {
        rAdventureCombat->SetDefaultCombatMusic(lua_tostring(L, 2));
    }
    ///--[System Abilities]
    //--Pushes the system ability Retreat onto the activity stack, then calls the given script to set its properties.
    else if(!strcasecmp(rSwitchType, "Set Retreat Properties") && tArgs == 2)
    {
        AdvCombatAbility *rSystemRetreat = rAdventureCombat->GetSystemRetreat();
        LuaManager::Fetch()->PushExecPop(rSystemRetreat, lua_tostring(L, 2), 1, "N", (float)ACA_SCRIPT_CODE_CREATE);
    }
    //--Pushes the system ability Surrender onto the activity stack, then calls the given script to set its properties.
    else if(!strcasecmp(rSwitchType, "Set Surrender Properties") && tArgs == 2)
    {
        AdvCombatAbility *rSystemSurrender = rAdventureCombat->GetSystemSurrender();
        LuaManager::Fetch()->PushExecPop(rSystemSurrender, lua_tostring(L, 2), 1, "N", (float)ACA_SCRIPT_CODE_CREATE);
    }
    //--Causes the player to immediately retreat from the battle.
    else if(!strcasecmp(rSwitchType, "Order Retreat") && tArgs == 1)
    {
        rAdventureCombat->BeginResolutionSequence(ADVCOMBAT_END_RETREAT);
    }
    //--Causes the player to immediately lose the battle.
    else if(!strcasecmp(rSwitchType, "Order Surrender") && tArgs == 1)
    {
        rAdventureCombat->BeginResolutionSequence(ADVCOMBAT_END_DEFEAT);
    }
    ///--[Options]
    //--Automatically use the Doctor Bag after combat flag.
    else if(!strcasecmp(rSwitchType, "Auto Doctor Bag") && tArgs == 2)
    {
        rAdventureCombat->SetAutoUseDoctorBag(lua_toboolean(L, 2));
    }
    //--Toggles Tourist Mode flag.
    else if(!strcasecmp(rSwitchType, "Tourist Mode") && tArgs == 2)
    {
        rAdventureCombat->SetTouristMode(lua_toboolean(L, 2));
    }
    //--Store last used cursor position for each character in combat flag.
    else if(!strcasecmp(rSwitchType, "Memory Cursor") && tArgs == 2)
    {
        rAdventureCombat->SetMemoryCursor(lua_toboolean(L, 2));
    }
    ///--[Resolution Handlers]
    //--If true, the player cannot win the battle.
    else if(!strcasecmp(rSwitchType, "Unwinnable") && tArgs == 2)
    {
        rAdventureCombat->SetUnwinnable(lua_toboolean(L, 2));
    }
    //--If true, the player cannot lose the battle.
    else if(!strcasecmp(rSwitchType, "Unloseable") && tArgs == 2)
    {
        rAdventureCombat->SetUnloseable(lua_toboolean(L, 2));
    }
    //--If true, the player cannot surrender.
    else if(!strcasecmp(rSwitchType, "Unsurrenderable") && tArgs == 2)
    {
        rAdventureCombat->SetUnsurrenderable(lua_toboolean(L, 2));
    }
    //--If true, the player cannot retreat.
    else if(!strcasecmp(rSwitchType, "Unretreatable") && tArgs == 2)
    {
        rAdventureCombat->SetUnretreatable(lua_toboolean(L, 2));
    }
    //--Retreat script used if no override is in place.
    else if(!strcasecmp(rSwitchType, "Standard Retreat Script") && tArgs == 2)
    {
        rAdventureCombat->SetStandardRetreatScript(lua_tostring(L, 2));
    }
    //--Victory script used if no override is in place.
    else if(!strcasecmp(rSwitchType, "Standard Victory Script") && tArgs == 2)
    {
        rAdventureCombat->SetStandardVictoryScript(lua_tostring(L, 2));
    }
    //--Defeat script used if no override is in place.
    else if(!strcasecmp(rSwitchType, "Standard Defeat Script") && tArgs == 2)
    {
        rAdventureCombat->SetStandardDefeatScript(lua_tostring(L, 2));
    }
    //--Retreat script for the next battle.
    else if(!strcasecmp(rSwitchType, "Retreat Script") && tArgs == 2)
    {
        rAdventureCombat->SetRetreatScript(lua_tostring(L, 2));
    }
    //--Victory script for the next battle.
    else if(!strcasecmp(rSwitchType, "Victory Script") && tArgs == 2)
    {
        rAdventureCombat->SetVictoryScript(lua_tostring(L, 2));
    }
    //--Defeat script for the next battle.
    else if(!strcasecmp(rSwitchType, "Defeat Script") && tArgs == 2)
    {
        rAdventureCombat->SetDefeatScript(lua_tostring(L, 2));
    }
    ///--[Enemies]
    //--Creates and registers a new AdvCombatEntity as an enemy.
    else if(!strcasecmp(rSwitchType, "Register Enemy") && tArgs == 3)
    {
        AdvCombatEntity *nEntity = new AdvCombatEntity();
        nEntity->SetInternalName(lua_tostring(L, 2));
        rAdventureCombat->RegisterEnemy(lua_tostring(L, 2), nEntity, lua_tointeger(L, 3));
        DataLibrary::Fetch()->PushActiveEntity(nEntity);
    }
    //--Clears all existing enemy aliases.
    else if(!strcasecmp(rSwitchType, "Clear Enemy Aliases") && tArgs == 1)
    {
        AliasStorage *rAliasStorage = rAdventureCombat->GetEnemyAliasStorage();
        rAliasStorage->Wipe();
    }
    //--Creates a new enemy path.
    else if(!strcasecmp(rSwitchType, "Create Enemy Path") && tArgs == 3)
    {
        AliasStorage *rAliasStorage = rAdventureCombat->GetEnemyAliasStorage();
        rAliasStorage->StoreString(lua_tostring(L, 2), lua_tostring(L, 3));
    }
    //--Creates an alias to a path.
    else if(!strcasecmp(rSwitchType, "Create Enemy Alias") && tArgs == 3)
    {
        AliasStorage *rAliasStorage = rAdventureCombat->GetEnemyAliasStorage();
        rAliasStorage->RegisterAliasToString(lua_tostring(L, 2), lua_tostring(L, 3));
    }
    ///--[Party]
    //--Pushes the named party member on the activity stack, or NULL on error.
    else if(!strcasecmp(rSwitchType, "Push Party Member") && tArgs == 2)
    {
        rAdventureCombat->PushPartyMember(lua_tostring(L, 2));
    }
    //--Registers a new party member and pushes them on the activity stack.
    else if(!strcasecmp(rSwitchType, "Register Party Member") && tArgs == 2)
    {
        AdvCombatEntity *nEntity = new AdvCombatEntity();
        nEntity->SetInternalName(lua_tostring(L, 2));
        nEntity->SetDisplayName("DEFAULT");
        rAdventureCombat->RegisterPartyMember(lua_tostring(L, 2), nEntity);
        DataLibrary::Fetch()->PushActiveEntity(nEntity);
    }
    //--Removes all party members from the current party.
    else if(!strcasecmp(rSwitchType, "Clear Party") && tArgs == 1)
    {
        rAdventureCombat->ClearParty();
    }
    //--Which party member is in which slot.
    else if(!strcasecmp(rSwitchType, "Party Slot") && tArgs == 3)
    {
        rAdventureCombat->SetPartyBySlot(lua_tointeger(L, 2), lua_tostring(L, 3));
    }
    //--Heals party to full. Usually used by rest sequences.
    else if(!strcasecmp(rSwitchType, "Restore Party") && tArgs == 1)
    {
        rAdventureCombat->FullRestoreParty();
    }
    ///--[Targeting]
    //--Clears all existing target clusters.
    else if(!strcasecmp(rSwitchType, "Clear Target Clusters") && tArgs == 1)
    {
        rAdventureCombat->ClearTargetClusters();
    }
    //--Creates a new target cluster with the given name.
    else if(!strcasecmp(rSwitchType, "Create Target Cluster") && tArgs == 3)
    {
        rAdventureCombat->CreateTargetCluster(lua_tostring(L, 2), lua_tostring(L, 3));
    }
    //--Adds the entity with the matching ID to the named cluster.
    else if(!strcasecmp(rSwitchType, "Add Target To Cluster") && tArgs == 3)
    {
        rAdventureCombat->RegisterEntityToClusterByID(lua_tostring(L, 2), (uint32_t)lua_tointeger(L, 3));
    }
    //--Automatically populates targets using an in-built macro.
    else if(!strcasecmp(rSwitchType, "Run Target Macro") && tArgs == 2)
    {
        rAdventureCombat->PopulateTargetsByCode(lua_tostring(L, 2), rAdventureCombat->GetActingEntity());
    }
    ///--[Error]
    //--Error case.
    else
    {
        LuaPropertyError("AdvCombat_SetProperty", rSwitchType, tArgs);
    }

    return 0;
}
