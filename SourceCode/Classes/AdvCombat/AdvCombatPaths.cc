//--Base
#include "AdvCombat.h"

//--Classes
//--CoreClasses
//--Definitions
//--Libraries
//--Managers

void AdvCombat::SetStandardRetreatScript(const char *pPath)
{
    ResetString(mStandardRetreatScript, pPath);
}
void AdvCombat::SetStandardDefeatScript(const char *pPath)
{
    ResetString(mStandardDefeatScript, pPath);
}
void AdvCombat::SetStandardVictoryScript(const char *pPath)
{
    ResetString(mStandardVictoryScript, pPath);
}
void AdvCombat::SetRetreatScript(const char *pPath)
{
    ResetString(mCurrentRetreatScript, pPath);
}
void AdvCombat::SetDefeatScript(const char *pPath)
{
    ResetString(mCurrentDefeatScript, pPath);
}
void AdvCombat::SetVictoryScript(const char *pPath)
{
    ResetString(mCurrentVictoryScript, pPath);
}
