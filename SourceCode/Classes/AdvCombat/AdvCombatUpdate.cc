//--Base
#include "AdvCombat.h"

//--Classes
#include "AdvCombatEntity.h"
#include "AdventureLevel.h"

//--CoreClasses
#include "SugarLinkedList.h"

//--Definitions
#include "AdvCombatDefStruct.h"
#include "EasingFunctions.h"

//--Libraries
//--Managers
#include "AudioManager.h"
#include "ControlManager.h"

void AdvCombat::Update()
{
    ///--[Documentation and Setup]
    //--Handles the combat update. Combat runs a set of timers then handles controls or logic updates, depending
    //  on its internal state.

    ///--[Pulsing]
    //--If the AdventureLevel is still pulsing, this object will be active but should not do anything.
    AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
    if(rActiveLevel && rActiveLevel->IsPulsing()) return;

    ///--[Stop Non-Active Update]
    //--If the object is not active, update stops at this point.
    if(!mIsActive) return;

    ///--[Post-Reinitialize]
    //--After combat called Reinitialize(), this flag will be true. Once all enemies have registered themselves
    //  and all setup is done, this will be called.
    if(mCombatReinitialized)
    {
        //--Unset flag.
        mCombatReinitialized = false;
        fprintf(stderr, "Running post-reinitialize.\n");

        //--Player's party resets their jobs.
        AdvCombatEntity *rEntity = (AdvCombatEntity *)mrActiveParty->PushIterator();
        while(rEntity)
        {
            //--Call subroutine.
            rEntity->HandleCombatStart();

            //--Next.
            rEntity = (AdvCombatEntity *)mrActiveParty->AutoIterate();
        }
    }

    ///--[Timers]
    //--Turn display timer. Indicates what turn it is, auto-caps out and stops rendering.
    if(mTurnDisplayTimer < ACE_TURN_DISPLAY_TICKS) mTurnDisplayTimer ++;

    //--Run the turn-order length handler.
    if(mTurnBarMoveTimer < mTurnBarMoveTimerMax)
    {
        //--Increment.
        mTurnBarMoveTimer ++;

        //--Recompute size.
        float cPercent = EasingFunction::QuadraticInOut(mTurnBarMoveTimer, mTurnBarMoveTimerMax);
        mTurnWidCurrent = mTurnWidStart + ((mTurnWidTarget - mTurnWidStart) * cPercent);
    }

    //--Confirmation window timer.
    if(mIsShowingConfirmationWindow)
    {
        if(mConfirmationTimer < ADVCOMBAT_CONFIRMATION_TICKS) mConfirmationTimer ++;
    }
    //--Timer decrement.
    else
    {
        if(mConfirmationTimer > 0) mConfirmationTimer --;
    }

    //--Player's highlight cursor. Auto-clamps.
    mAbilityHighlightPack.Increment(EASING_CODE_QUADOUT);

    ///--[Subhandlers]
    //--Subhandlers take control away from the usual combat update.
    if(mCombatResolution != ADVCOMBAT_END_NONE) { UpdateResolution();   return; }
    if(mIsIntroduction)                         { UpdateIntroduction(); return; }
    if(mIsShowingConfirmationWindow)            { UpdateConfirmation(); return; }

    ///--[Turn Resolution]
    //--If no subhandlers are running, check if there is nobody acting. If the turn order list is clear, it's a new turn.
    if(mrTurnOrder->GetListSize() < 1)
    {
        //--Start a new turn. After Reinitialize() is called, the turn is -1, so the first turn is 0.
        mCurrentTurn ++;

        //--Run the turn order roller.
        RollTurnOrder();

        //--Mark the new turn length.
        RecomputeTurnBarLength();

        //--Reset the turn timer.
        BeginTurnDisplay();

        //--Mark a new action as taking place.
        mIsActionConcluding = false;
        mPlayerInterfaceTimer = 0;

        //--Mark the next action as running its first tick.
        mActionFirstTick = true;
    }

    ///--[Acting Entity]
    //--Get the acting entity.
    AdvCombatEntity *rActingEntity = (AdvCombatEntity *)mrTurnOrder->GetElementBySlot(0);
    if(!rActingEntity) return;

    ///--[Action Conclusion]
    //--When the acting entity has finished their action, the player's party slides offscreen, including the
    //  acting character if it was a party member.
    if(mIsActionConcluding)
    {
        mIsActionConcluding = false;
        mActionFirstTick = true;
        return;
    }

    ///--[AI Handling]
    //--If the entity has an AI controlling it:
    if(false)
    {

    }
    ///--[Player Handling]
    //--If there is no commanding AI, the player inputs commands here. This gets its own file.
    else
    {
        HandlePlayerControls();
    }
}
