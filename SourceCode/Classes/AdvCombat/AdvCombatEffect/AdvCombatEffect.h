//--[AdvCombatEffect]
//--An effect in combat, which can be buffs, debuffs, damage-over-times, or anything a scripter
//  can dream up.

#pragma once

//--[Includes]
#include "Definitions.h"
#include "Structures.h"
#include "RootObject.h"

//--[Local Structures]
//--[Local Definitions]
//--[Classes]
class AdvCombatEffect : public RootObject
{
    private:
    //--System
    int mEffectID;
    char *mLocalName;
    char *mDisplayName;

    //--Expiration
    bool mExpiresFromTurns;
    int mTurnsLeft;

    //--Script Handler
    int mApplicationPriority;
    char *mScriptPath;

    protected:

    public:
    //--System
    AdvCombatEffect();
    virtual ~AdvCombatEffect();

    //--Public Variables
    //--Property Queries
    //--Manipulators
    //--Core Methods
    private:
    //--Private Core Methods
    public:
    //--Update
    //--File I/O
    //--Drawing
    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions

