//--Base
#include "AdvCombatEffect.h"

//--Classes
//--CoreClasses
//--Definitions
//--Libraries
//--Managers

//=========================================== System ==============================================
AdvCombatEffect::AdvCombatEffect()
{
    //--[RootObject]
    //--System
    mType = POINTER_TYPE_ADVCOMBATEFFECT;

    //--[AdvCombatEffect]
    //--System
    mEffectID = 0;
    mLocalName = InitializeString("Effect");
    mDisplayName = InitializeString("Effect");

    //--Expiration
    mExpiresFromTurns = false;
    mTurnsLeft = 0;

    //--Script Handler
    mApplicationPriority = 0;
    mScriptPath = NULL;
}
AdvCombatEffect::~AdvCombatEffect()
{
    free(mLocalName);
    free(mDisplayName);
    free(mScriptPath);
}

//====================================== Property Queries =========================================
//========================================= Manipulators ==========================================
//========================================= Core Methods ==========================================
//===================================== Private Core Methods ======================================
//============================================ Update =============================================
//=========================================== File I/O ============================================
//=========================================== Drawing =============================================
//======================================= Pointer Routing =========================================
//====================================== Static Functions =========================================
//========================================= Lua Hooking ===========================================
//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
