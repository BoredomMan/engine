//--Base
#include "AdvCombat.h"

//--Classes
#include "AdvCombatAbility.h"
#include "AliasStorage.h"

//--CoreClasses
#include "SugarLinkedList.h"
#include "StarlightString.h"

//--Definitions
#include "AdvCombatDefStruct.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "ControlManager.h"

void AdvCombat::Initialize()
{
    //--[Documentation and Setup]
    //--In order to keep the constructor in very large classes from becoming unmanagement, this Initialize() function is called
    //  when it is created. It otherwise does all the work of the constructor.
    //--Should only be called once on allocation. If resetting the class, use Reinitialize().

    //--[RootObject]
    //--System
    mType = POINTER_TYPE_ADVCOMBAT;

    //--[AdvCombat]
    //--System
    mCombatReinitialized = false;
    mIsActive = false;
    mWasLastDefeatSurrender = false;

    //--Introduction
    mIsIntroduction = true;
    mIntroTimer = 0;

    //--Turn State
    mCurrentTurn = -1;
    mTurnDisplayTimer = ACE_TURN_DISPLAY_TICKS;
    mTurnBarMoveTimer = 1;
    mTurnBarMoveTimerMax = 1;
    mTurnWidStart = 0;
    mTurnWidCurrent = 0;
    mTurnWidTarget = 0;
    mrTurnOrder = new SugarLinkedList(false);
    rTurnDiscardImg = NULL;

    //--Action Handling
    mActionFirstTick = true;
    mIsActionConcluding = false;
    mPlayerInterfaceTimer = 0;
    mAbilitySelectionX = 0;
    mAbilitySelectionY = 0;
    mAbilityHighlightPack.Initialize();

    //--Target Selection
    mLockTargetClusters = false;
    mTargetClusterCounter = 0;
    mIsSelectingTargets = false;
    mTargetFlashTimer = 0;
    mTargetClusters = new SugarLinkedList(true);

    //--Confirmation Window
    mIsShowingConfirmationWindow = false;
    mConfirmationTimer = 0;
    mConfirmationString = new StarlightString();
    mAcceptString = new StarlightString();
    mCancelString = new StarlightString();
    rConfirmationFunction = NULL;

    //--Ability Execution
    rActiveAbility = NULL;
    rActiveTargetCluster = NULL;

    //--System Abilities
    mSysAbilityRetreat = new AdvCombatAbility();
    mSysAbilityRetreat->SetInternalName("System|Retreat");
    mSysAbilitySurrender = new AdvCombatAbility();
    mSysAbilitySurrender->SetInternalName("System|Surrender");

    //--Option Flags
    mAutoUseDoctorBag = true;
    mTouristMode = false;
    mMemoryCursor = true;

    //--Combat State Flags
    mIsUnwinnable = false;
    mIsUnloseable = false;
    mIsUnretreatable = false;
    mIsUnsurrenderable = false;
    mResolutionTimer = 0;
    mResolutionState = 0;
    mCombatResolution = ADVCOMBAT_END_NONE;

    //--Enemy Storage
    mUniqueEnemyIDCounter = 0;
    mEnemyAliases = new AliasStorage();
    mEnemyRoster = new SugarLinkedList(true);
    mrEnemyCombatParty = new SugarLinkedList(false);
    mrEnemyReinforcements = new SugarLinkedList(false);
    mWorldReferences = new SugarLinkedList(true);

    //--Party Storage
    mPartyRoster = new SugarLinkedList(true);
    mrActiveParty = new SugarLinkedList(false);
    mrCombatParty = new SugarLinkedList(false);

    //--Effect Storage
    mGlobalEffectIDs = 0;
    mGlobalEffectList = new SugarLinkedList(true);

    //--Cutscene Handlers
    mStandardRetreatScript = NULL;
    mStandardDefeatScript = NULL;
    mStandardVictoryScript = NULL;
    mCurrentRetreatScript = NULL;
    mCurrentDefeatScript = NULL;
    mCurrentVictoryScript = NULL;

    //--Audio
    mDefaultCombatMusic = InitializeString("Null");

    //--Images
    memset(&Images, 0, sizeof(Images));
}
void AdvCombat::Reinitialize()
{
    //--[Documentation and Setup]
    //--Re-initializes the class back to neutral variables, but leaves constants and referenced images in place. Used when the
    //  combat reboots between battles.

    //--[RootObject]
    //--System

    //--[AdvCombat]
    //--System
    mCombatReinitialized = true;

    //--Introduction
    mIsIntroduction = true;
    mIntroTimer = 0;
    mWasLastDefeatSurrender = false;

    //--Turn State
    mCurrentTurn = -1;
    mTurnDisplayTimer = ACE_TURN_DISPLAY_TICKS;
    mTurnBarMoveTimer = 1;
    mTurnBarMoveTimerMax = 1;
    mTurnWidStart = 0;
    mTurnWidCurrent = 0;
    mTurnWidTarget = 0;
    mrTurnOrder->ClearList();
    rTurnDiscardImg = NULL;

    //--Action Handling
    mActionFirstTick = true;
    mIsActionConcluding = false;
    mPlayerInterfaceTimer = 0;
    mAbilitySelectionX = 0;
    mAbilitySelectionY = 0;
    mAbilityHighlightPack.Initialize();

    //--Target Selection
    mLockTargetClusters = false;
    mTargetClusterCounter = 0;
    mIsSelectingTargets = false;
    mTargetFlashTimer = 0;
    mTargetClusters->ClearList();

    //--Confirmation Window
    mIsShowingConfirmationWindow = false;
    mConfirmationTimer = 0;
    rConfirmationFunction = NULL;

    //--Ability Execution
    rActiveAbility = NULL;
    rActiveTargetCluster = NULL;

    //--System Abilities
    //--Option Flags
    //--Combat State Flags
    mIsUnwinnable = false;
    mIsUnloseable = false;
    mIsUnretreatable = false;
    mIsUnsurrenderable = false;
    mResolutionTimer = 0;
    mResolutionState = 0;
    mCombatResolution = ADVCOMBAT_END_NONE;

    //--Enemy Storage
    mUniqueEnemyIDCounter = 0;
    mEnemyRoster->ClearList();
    mrEnemyCombatParty->ClearList();
    mrEnemyReinforcements->ClearList();
    mWorldReferences->ClearList();

    //--Party Storage
    mrCombatParty->ClearList();
    void *rCharacterPtr = mrActiveParty->PushIterator();
    while(rCharacterPtr)
    {
        mrCombatParty->AddElementAsTail(mrActiveParty->GetIteratorName(), rCharacterPtr);
        rCharacterPtr = mrActiveParty->AutoIterate();
    }

    //--Effect Storage
    mGlobalEffectIDs = 0;
    mGlobalEffectList->ClearList();

    //--Cutscene Handlers
    ResetString(mCurrentRetreatScript, NULL);
    ResetString(mCurrentDefeatScript, NULL);
    ResetString(mCurrentVictoryScript, NULL);

    //--Audio
    //--Images

    //--[Confirmation Strings]
    //--These strings reinitialize every time combat starts since the player may have changed controls.
    mAcceptString->SetString("[IMG0] Accept");
    mAcceptString->AllocateImages(1);
    mAcceptString->SetImageP(0, 5.0f, ControlManager::Fetch()->ResolveControlImage("Activate"));
    mAcceptString->CrossreferenceImages();

    mCancelString->SetString("[IMG0] Cancel");
    mCancelString->AllocateImages(1);
    mCancelString->SetImageP(0, 5.0f, ControlManager::Fetch()->ResolveControlImage("Cancel"));
    mCancelString->CrossreferenceImages();
}
void AdvCombat::Construct()
{
    //--[Documentation and Setup]
    //--Resolve all image references in the object, then verify them. Also sets up hardcoded positions.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();

    //--[Fonts]
    Images.Data.rDamageFont              = rDataLibrary->GetFont("Adventure Combat Damage");
    Images.Data.rVictoryFont             = rDataLibrary->GetFont("Adventure Combat Header");
    Images.Data.rAllyHPFont              = rDataLibrary->GetFont("Adventure Combat Ally Bar");
    Images.Data.rActiveCharacterNameFont = rDataLibrary->GetFont("Adventure Combat Acting Name Font");
    Images.Data.rNewTurnFont             = rDataLibrary->GetFont("Adventure Combat New Turn Font");
    Images.Data.rAbilityDescriptionFont  = rDataLibrary->GetFont("Adventure Combat Description");
    Images.Data.rConfirmationFontBig     = rDataLibrary->GetFont("Adventure Combat Confirmation Big");
    Images.Data.rConfirmationFontSmall   = rDataLibrary->GetFont("Adventure Combat Confirmation Small");

    //--[Ally Bar]
    const char *cAllyBarNames[] = {"AllyFrame", "AllyPortraitMask"};
    Crossload(&Images.Data.rAllyFrame, sizeof(SugarBitmap *) * 2, sizeof(SugarBitmap *), "Root/Images/AdventureUI/Combat/AllyBar|%s", cAllyBarNames);

    //--[Defeat Overlay]
    const char *cDefeatNames[] = {"Defeat_0", "Defeat_1", "Defeat_2", "Defeat_3", "Defeat_4", "Defeat_5"};
    Crossload(&Images.Data.rDefeatFrames[0], sizeof(SugarBitmap *) * ADVCOMBAT_DEFEAT_FRAMES_TOTAL, sizeof(SugarBitmap *), "Root/Images/AdventureUI/Combat/Defeat|%s", cDefeatNames);

    //--[Enemy Health Bar]
    const char *cEnemyHealthBarNames[] = {"EnemyHealthBarFill", "EnemyHealthBarFrame", "EnemyHealthBarUnder"};
    Crossload(&Images.Data.rEnemyHealthBarFill, sizeof(SugarBitmap *) * 3, sizeof(SugarBitmap *), "Root/Images/AdventureUI/Combat/EnemyHealthBar|%s", cEnemyHealthBarNames);

    //--[Combat Inspector]
    const char *cInspectorNames[] = {"CharacterBack", "EffectInset", "Frames"};
    Crossload(&Images.Data.rInspectorCharacterBack, sizeof(SugarBitmap *) * 3, sizeof(SugarBitmap *), "Root/Images/AdventureUI/Combat/Inspector|%s", cInspectorNames);

    //--[Player Interface]
    const char *cPlayerInterfaceNames[] = {"AbilityFrameMain", "AbilityHighlight", "Descriptionwindow", "MainHealthBarBack", "MainHealthBarFill", "MainHealthBarFrame", "MainNameBanner", "MainPortraitMask", "MainPortraitRing", "MainPortraitRingBack"};
    Crossload(&Images.Data.rAbilityFrameMain, sizeof(SugarBitmap *) * 10, sizeof(SugarBitmap *), "Root/Images/AdventureUI/Combat/PlayerInterface|%s", cPlayerInterfaceNames);

    //--[Turn Order]
    const char *cTurnOrderNames[] = {"TurnOrderBack", "TurnOrderCircle", "TurnOrderEdge"};
    Crossload(&Images.Data.rTurnOrderBack, sizeof(SugarBitmap *) * 3, sizeof(SugarBitmap *), "Root/Images/AdventureUI/Combat/TurnOrder|%s", cTurnOrderNames);

    //--[Verify]
    Images.mIsReady = VerifyStructure(&Images.Data, sizeof(Images.Data), sizeof(void *), false);
}
void AdvCombat::Disassemble()
{
    //--[Documentation and Setup]
    //--Performs the work of the destructor but does not delete the class.

    //--[AdvCombat]
    //--System
    //--Introduction
    //--Turn State
    delete mrTurnOrder;

    //--Action Handling
    //--Target Selection
    delete mTargetClusters;

    //--Confirmation Window
    delete mConfirmationString;
    delete mAcceptString;
    delete mCancelString;

    //--Option Flags
    //--Combat State Flags
    //--Enemy Storage
    delete mEnemyAliases;
    delete mEnemyRoster;
    delete mrEnemyCombatParty;
    delete mrEnemyReinforcements;
    delete mWorldReferences;

    //--Party Storage
    delete mPartyRoster;
    delete mrActiveParty;
    delete mrCombatParty;

    //--Effect Storage
    delete mGlobalEffectList;

    //--Cutscene Handlers
    free(mStandardRetreatScript);
    free(mStandardDefeatScript);
    free(mStandardVictoryScript);
    free(mCurrentRetreatScript);
    free(mCurrentDefeatScript);
    free(mCurrentVictoryScript);

    //--Audio
    free(mDefaultCombatMusic);

    //--Images
}
