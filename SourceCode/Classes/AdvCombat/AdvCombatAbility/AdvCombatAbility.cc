//--Base
#include "AdvCombatAbility.h"

//--Classes
//--CoreClasses
#include "StarlightString.h"

//--Definitions
#include "AdvCombatDefStruct.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "LuaManager.h"

//=========================================== System ==============================================
AdvCombatAbility::AdvCombatAbility()
{
    //--[RootObject]
    //--System
    mType = POINTER_TYPE_ADVCOMBATABILITY;

    //--[AdvCombatAbility]
    //--System
    mLocalName = InitializeString("Null");
    mDisplayName = InitializeString("Null");
    mScriptPath = NULL;

    //--Usage
    mIsUsableNow = false;
    mRequiresTarget = true;
    mOpensConfirmation = false;

    //--Confirmation
    mConfirmationText = NULL;

    //--Display
    mDescription = new StarlightString();
    rIconBack = NULL;
    rIcon = NULL;

    //--Storage
    rOwningEntity = NULL;
}
AdvCombatAbility::~AdvCombatAbility()
{
    free(mLocalName);
    free(mDisplayName);
    free(mScriptPath);
    free(mConfirmationText);
    delete mDescription;
}

//--[Private Statics]
//--Uses for enqueuing sound effects.
char AdvCombatAbility::xSoundEffectQueue[STD_MAX_LETTERS];
int AdvCombatAbility::xSoundEffectPriority = -1;

//====================================== Property Queries =========================================
const char *AdvCombatAbility::GetDisplayName()
{
    return mDisplayName;
}
bool AdvCombatAbility::IsUsableNow()
{
    return mIsUsableNow;
}
bool AdvCombatAbility::NeedsTargetCluster()
{
    return mRequiresTarget;
}
bool AdvCombatAbility::ActivatesConfirmation()
{
    return mOpensConfirmation;
}
char *AdvCombatAbility::GetConfirmationText()
{
    return mConfirmationText;
}
StarlightString *AdvCombatAbility::GetDescription()
{
    return mDescription;
}
SugarBitmap *AdvCombatAbility::GetIconBack()
{
    return rIconBack;
}
SugarBitmap *AdvCombatAbility::GetIcon()
{
    return rIcon;
}

//========================================= Manipulators ==========================================
void AdvCombatAbility::SetOwner(AdvCombatEntity *pOwner)
{
    rOwningEntity = pOwner;
}
void AdvCombatAbility::SetInternalName(const char *pName)
{
    if(!pName) return;
    ResetString(mLocalName, pName);
}
void AdvCombatAbility::SetDisplayName(const char *pName)
{
    if(!pName) return;
    ResetString(mDisplayName, pName);
}
void AdvCombatAbility::SetScriptPath(const char *pPath)
{
    ResetString(mScriptPath, pPath);
}
void AdvCombatAbility::SetUsable(bool pIsUsable)
{
    mIsUsableNow = pIsUsable;
}
void AdvCombatAbility::SetNeedsTarget(bool pNeedsTarget)
{
    mRequiresTarget = pNeedsTarget;
}
void AdvCombatAbility::SetActivatesConfirmation(bool pFlag, const char *pConfirmationString)
{
    mOpensConfirmation = pFlag;
    ResetString(mConfirmationText, pConfirmationString);
}
void AdvCombatAbility::SetDescription(const char *pDescription)
{
    if(!pDescription) return;
    mDescription->SetString(pDescription);
}
void AdvCombatAbility::AllocateDescriptionImages(int pImages)
{
    mDescription->AllocateImages(pImages);
}
void AdvCombatAbility::SetDescriptionImage(int pSlot, float pYOffset, const char *pDLPath)
{
    mDescription->SetImageS(pSlot, pYOffset, pDLPath);
}
void AdvCombatAbility::CrossreferenceDescriptionImages()
{
    mDescription->CrossreferenceImages();
}
void AdvCombatAbility::SetIconBack(const char *pDLPath)
{
    rIconBack = (SugarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath);
}
void AdvCombatAbility::SetIcon(const char *pDLPath)
{
    rIcon = (SugarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath);
}

//========================================= Core Methods ==========================================
void AdvCombatAbility::CallCombatStart()
{
    //--Calls the ability handler at combat start. The calling entity is available to be pushed.
    if(!mScriptPath) return;
    LuaManager::Fetch()->PushExecPop(this, mScriptPath, 1, "N", (float)ACA_SCRIPT_CODE_BEGINCOMBAT);
}
void AdvCombatAbility::CallTargetPainting()
{
    //--Calls target painting script. If no targets are painted, ability fails.
    if(!mScriptPath) return;
    LuaManager::Fetch()->PushExecPop(this, mScriptPath, 1, "N", (float)ACA_SCRIPT_CODE_PAINTTARGETS);
}
void AdvCombatAbility::CallExecution()
{
    //--Calls execution script.
    if(!mScriptPath) return;
    LuaManager::Fetch()->PushExecPop(this, mScriptPath, 1, "N", (float)ACA_SCRIPT_CODE_EXECUTE);
}

//===================================== Private Core Methods ======================================
//============================================ Update =============================================
//=========================================== File I/O ============================================
//=========================================== Drawing =============================================
//======================================= Pointer Routing =========================================
AdvCombatEntity *AdvCombatAbility::GetOwner()
{
    //--Gets the entity that owns this ability. Only populated during script calls, otherwise returns
    //  NULL. Assume this pointer is unstable.
    return rOwningEntity;
}

//====================================== Static Functions =========================================
void AdvCombatAbility::HandleCombatActionSounds()
{
    //--Static function, called at the end of the tick. Whichever sound effect had the highest priority
    //  will play. If many sound effects trigger from actions at the same time, this prevents a lot of overlay.
    //  Keeps splash-damage attacks from hurting your ears.
    if(xSoundEffectPriority == -1) return;

    //--Play the sound effect if the priority was not -1.
    AudioManager::Fetch()->PlaySound(xSoundEffectQueue);

    //--Reset the data. If no new effect is set to play, the -1 code will be ignored.
    xSoundEffectPriority = -1;
}

//========================================= Lua Hooking ===========================================
//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
