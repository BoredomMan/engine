//--[AdvCombatAbility]
//--Represents an ability to be used in combat. Is held by an AdvCombatEntity.
//--Note about mRequiresTarget:
//  Almost all abilities should require targets, even if the ability only ever targets one
//  entity. This is to allow the player to subconsciously confirm the target.
//  The exceptions are system abilities like Retreat or Surrender, which use confirmation
//  windows and thus don't need targets. Abilities default to needing targets.
//  Abilities that do not need targets *never* call the target paint script.

#pragma once

//--[Includes]
#include "Definitions.h"
#include "Structures.h"

//--[Local Structures]
//--[Local Definitions]
//--[Classes]
class AdvCombatAbility : public RootObject
{
    private:
    //--System
    char *mLocalName;
    char *mDisplayName;
    char *mScriptPath;

    //--Usage
    bool mIsUsableNow;
    bool mRequiresTarget;
    bool mOpensConfirmation;

    //--Confirmation
    char *mConfirmationText;

    //--Display
    StarlightString *mDescription;
    SugarBitmap *rIconBack;
    SugarBitmap *rIcon;

    //--Storage
    AdvCombatEntity *rOwningEntity;

    protected:

    public:
    //--System
    AdvCombatAbility();
    virtual ~AdvCombatAbility();

    //--Public Variables
    //--Private Statics
    static char xSoundEffectQueue[STD_MAX_LETTERS];
    static int xSoundEffectPriority;

    //--Property Queries
    const char *GetDisplayName();
    bool IsUsableNow();
    bool NeedsTargetCluster();
    bool ActivatesConfirmation();
    char *GetConfirmationText();
    StarlightString *GetDescription();
    SugarBitmap *GetIconBack();
    SugarBitmap *GetIcon();

    //--Manipulators
    void SetOwner(AdvCombatEntity *pOwner);
    void SetInternalName(const char *pName);
    void SetDisplayName(const char *pName);
    void SetScriptPath(const char *pPath);
    void SetUsable(bool pIsUsable);
    void SetNeedsTarget(bool pNeedsTarget);
    void SetActivatesConfirmation(bool pFlag, const char *pConfirmationString);
    void SetDescription(const char *pDescription);
    void AllocateDescriptionImages(int pImages);
    void SetDescriptionImage(int pSlot, float pYOffset, const char *pDLPath);
    void CrossreferenceDescriptionImages();
    void SetIconBack(const char *pDLPath);
    void SetIcon(const char *pDLPath);

    //--Core Methods
    void CallCombatStart();
    void CallTargetPainting();
    void CallExecution();

    private:
    //--Private Core Methods
    public:
    //--Update
    //--File I/O
    //--Drawing
    //--Pointer Routing
    AdvCombatEntity *GetOwner();

    //--Static Functions
    static void HandleCombatActionSounds();

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_AdvCombatAbility_GetProperty(lua_State *L);
int Hook_AdvCombatAbility_SetProperty(lua_State *L);

