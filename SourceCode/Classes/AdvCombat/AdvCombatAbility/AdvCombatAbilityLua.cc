//--Base
#include "AdvCombatAbility.h"

//--Classes
//--CoreClasses
//--Definitions
//--GUI
//--Libraries
#include "DataLibrary.h"

//--Managers

//========================================= Lua Hooking ===========================================
void AdvCombatAbility::HookToLuaState(lua_State *pLuaState)
{
    /* AdvCombatAbility_GetProperty("Dummy") (1 Integer)
       Gets and returns the requested property in the Adventure Combat class. */
    lua_register(pLuaState, "AdvCombatAbility_GetProperty", &Hook_AdvCombatAbility_GetProperty);

    /* AdvCombatAbility_SetProperty("Display Name", sName)
       AdvCombatAbility_SetProperty("Description", sDescription)
       AdvCombatAbility_SetProperty("Icon Back", sDLPath)
       AdvCombatAbility_SetProperty("Icon", sDLPath)
       Sets the property in the Adventure Combat class. */
    lua_register(pLuaState, "AdvCombatAbility_SetProperty", &Hook_AdvCombatAbility_SetProperty);
}

//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
int Hook_AdvCombatAbility_GetProperty(lua_State *L)
{
    ///--[Argument Listing]
    //--[System]
    //AdvCombatAbility_GetProperty("Dummy") (1 Integer)

    //--Argument check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("AdvCombatAbility_GetProperty");

    //--Setup
    int tReturns = 0;
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[Static]
    //--Dummy static.
    if(!strcasecmp(rSwitchType, "Static Dummy"))
    {
        lua_pushinteger(L, 0);
        return 1;
    }

    ///--[Dynamic]
    //--Type check.
    if(!DataLibrary::Fetch()->IsActiveValid(POINTER_TYPE_ADVCOMBATABILITY)) return LuaTypeError("AdvCombatAbility_GetProperty");
    AdvCombatAbility *rAbility = (AdvCombatAbility *)DataLibrary::Fetch()->rActiveObject;

    ///--[System]
    //--Dummy dynamic.
    if(!strcasecmp(rSwitchType, "Dummy") && tArgs == 1)
    {
        lua_pushinteger(L, 0);
        tReturns = 1;
    }
    //--Error case.
    else
    {
        LuaPropertyError("AdvCombatAbility_GetProperty", rSwitchType, tArgs);
    }

    return tReturns;
}
int Hook_AdvCombatAbility_SetProperty(lua_State *L)
{
    ///--[Argument Listing]
    //--[System]
    //AdvCombatAbility_SetProperty("Script Path", sPath)
    //AdvCombatAbility_SetProperty("Push Owner") (Pushes Activity Stack)
    //AdvCombatAbility_SetProperty("Usable", bFlag)

    //--[Special Flags]
    //AdvCombatAbility_SetProperty("Requires Target", bFlag)
    //AdvCombatAbility_SetProperty("Opens Confirmation Window", bFlag, sConfirmationText)

    //--[Display]
    //AdvCombatAbility_SetProperty("Display Name", sName)
    //AdvCombatAbility_SetProperty("Icon Back", sDLPath)
    //AdvCombatAbility_SetProperty("Icon", sDLPath)

    //--[Description]
    //AdvCombatAbility_SetProperty("Description", sDescription)
    //AdvCombatAbility_SetProperty("Allocate Description Images", iCount)
    //AdvCombatAbility_SetProperty("Description Image", iSlot, fYOffset, sDLPath)
    //AdvCombatAbility_SetProperty("Crossload Description Images")

    //--Argument check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("AdvCombatAbility_SetProperty");

    //--Setup
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[Static]
    //--Dummy
    if(!strcasecmp(rSwitchType, "Dummy") && tArgs == 1000)
    {
    }

    ///--[Dynamic]
    //--Type check.
    if(!DataLibrary::Fetch()->IsActiveValid(POINTER_TYPE_ADVCOMBATABILITY)) return LuaTypeError("AdvCombatAbility_SetProperty");
    AdvCombatAbility *rAbility = (AdvCombatAbility *)DataLibrary::Fetch()->rActiveObject;

    ///--[System]
    //--Sets the path this ability uses to perform logic.
    if(!strcasecmp(rSwitchType, "Script Path") && tArgs == 2)
    {
        rAbility->SetScriptPath(lua_tostring(L, 2));
    }
    //--Pushes the entity that owns this ability.
    else if(!strcasecmp(rSwitchType, "Push Owner") && tArgs == 1)
    {
        AdvCombatEntity *rCaller = rAbility->GetOwner();
        DataLibrary::Fetch()->PushActiveEntity(rCaller);
    }
    //--Whether or not the ability can be used right now. Often ignored by AIs.
    else if(!strcasecmp(rSwitchType, "Usable") && tArgs == 2)
    {
        rAbility->SetUsable(lua_toboolean(L, 2));
    }
    ///--[Special Flags]
    //--Whether or not the ability explicitly requires a target to execute. Only used for system abilities
    //  like Retreat/Surrender.
    else if(!strcasecmp(rSwitchType, "Requires Target") && tArgs == 2)
    {
        rAbility->SetNeedsTarget(lua_toboolean(L, 2));
    }
    //--If the ability opens a confirmation window, and what the text should be on that window.
    else if(!strcasecmp(rSwitchType, "Opens Confirmation Window") && tArgs == 3)
    {
        rAbility->SetActivatesConfirmation(lua_toboolean(L, 2), lua_tostring(L, 3));
    }
    ///--[Display]
    //--Name that appears on the UI.
    else if(!strcasecmp(rSwitchType, "Display Name") && tArgs == 2)
    {
        rAbility->SetDisplayName(lua_tostring(L, 2));
    }
    //--DLPath of the backing behind the icon.
    else if(!strcasecmp(rSwitchType, "Icon Back") && tArgs == 2)
    {
        rAbility->SetIconBack(lua_tostring(L, 2));
    }
    //--DLPath of the icon that appears in combat.
    else if(!strcasecmp(rSwitchType, "Icon") && tArgs == 2)
    {
        rAbility->SetIcon(lua_tostring(L, 2));
    }
    ///--[Description]
    //--Description lines in-combat. Supports in-stride images.
    else if(!strcasecmp(rSwitchType, "Description") && tArgs == 2)
    {
        rAbility->SetDescription(lua_tostring(L, 2));
    }
    //--Set how many images are in the description.
    else if(!strcasecmp(rSwitchType, "Allocate Description Images") && tArgs == 2)
    {
        rAbility->AllocateDescriptionImages(lua_tointeger(L, 2));
    }
    //--Sets the image in the given slot.
    else if(!strcasecmp(rSwitchType, "Description Image") && tArgs == 4)
    {
        rAbility->SetDescriptionImage(lua_tointeger(L, 2), lua_tonumber(L, 3), lua_tostring(L, 4));
    }
    //--Call once images are set and the string is parsed to set image pointers.
    else if(!strcasecmp(rSwitchType, "Crossload Description Images") && tArgs == 1)
    {
        rAbility->CrossreferenceDescriptionImages();
    }
    //--Error case.
    else
    {
        LuaPropertyError("AdvCombatAbility_SetProperty", rSwitchType, tArgs);
    }

    return 0;
}
