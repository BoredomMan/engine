//--Base
#include "AdvCombat.h"

//--Classes
#include "AdvCombatEntity.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarLinkedList.h"

//--Definitions
#include "EasingFunctions.h"

//--Libraries
//--Managers
#include "DisplayManager.h"

//--[Local Definitions]
#define ADVCOMBAT_INTRO_TICKS 15
#define ADVCOMBAT_INTRO_OFFSCREEN (VIRTUAL_CANVAS_X + 300.0f)

//--[Update]
void AdvCombat::UpdateIntroduction()
{
    //--[Documentation and Setup]
    //--Handles the introduction to combat. Presently this is just the enemies sliding in from the
    //  right side of the screen. Once completed, the game begins setting turn order.
    //--By default, the introduction is active as soon as the AdvCombat class Reinitialize() is called.

    //--[Zero Tick]
    //--Position all enemies at their offscreen start positions. Order them all to move to their ideal positions.
    if(mIntroTimer == 0)
    {
        AdvCombatEntity *rEnemy = (AdvCombatEntity *)mrEnemyCombatParty->PushIterator();
        while(rEnemy)
        {
            rEnemy->MoveToPosition(ADVCOMBAT_INTRO_OFFSCREEN, ADVCOMBAT_POSITION_STD_Y, 0);
            rEnemy->MoveToIdealPosition(ADVCOMBAT_INTRO_TICKS);
            rEnemy = (AdvCombatEntity *)mrEnemyCombatParty->AutoIterate();
        }
    }

    //--[Timer]
    if(mIntroTimer < ADVCOMBAT_INTRO_TICKS)
    {
        //--Increment.
        mIntroTimer ++;

        //--Ending case.
        if(mIntroTimer >= ADVCOMBAT_INTRO_TICKS) mIsIntroduction = false;
    }

    //--[Position Update]
    //--Order all enemies to update their positions.
    AdvCombatEntity *rEnemy = (AdvCombatEntity *)mrEnemyCombatParty->PushIterator();
    while(rEnemy)
    {
        rEnemy->UpdatePosition();
        rEnemy = (AdvCombatEntity *)mrEnemyCombatParty->AutoIterate();
    }
}

//--[Render]
void AdvCombat::RenderIntroduction()
{
    //--[Documentation and Setup]
    //--Renders the introduction. Presently this is just enemies sliding in from the right, and the UI
    //  sliding onto the field.
    if(!Images.mIsReady) return;
    float cPercent = EasingFunction::QuadraticInOut(mIntroTimer, ADVCOMBAT_INTRO_TICKS);

    //--[Backing]
    //--Flat grey backing.
    SugarBitmap::DrawFullBlack(ADVCOMBAT_STD_BACKING_OPACITY);

    //--[UI Positions]
    //--All UI objects share the offset of the largest object, which is ally bars in the top left. The maximum
    //  width is computed here.
    float cMaxWidth = Images.Data.rAllyFrame->GetXOffset() + Images.Data.rAllyFrame->GetWidth();
    float cXTranslate = cMaxWidth * (1.0f - cPercent);

    //--Render the ally frames offset to the left.
    glTranslatef(cXTranslate * -1.0f, 0.0f, 0.0f);
    RenderAllyBars();
    glTranslatef(cXTranslate *  1.0f, 0.0f, 0.0f);

    //--Now shift to the right and render the turn order nub.
    glTranslatef(cXTranslate *  1.0f, 0.0f, 0.0f);
    RenderTurnOrder();
    glTranslatef(cXTranslate * -1.0f, 0.0f, 0.0f);
}
