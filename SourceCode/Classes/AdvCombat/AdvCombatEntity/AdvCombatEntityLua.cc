//--Base
#include "AdvCombatEntity.h"

//--Classes
#include "AdvCombatAbility.h"
#include "AdvCombatJob.h"

//--CoreClasses
//--Definitions
//--GUI
//--Libraries
#include "DataLibrary.h"

//--Managers

//========================================= Lua Hooking ===========================================
void AdvCombatEntity::HookToLuaState(lua_State *pLuaState)
{
    /* AdvCombatEntity_GetProperty("Dummy") (1 Integer)
       Gets and returns the requested property in the Adventure Combat class. */
    lua_register(pLuaState, "AdvCombatEntity_GetProperty", &Hook_AdvCombatEntity_GetProperty);

    /* AdvCombatEntity_SetProperty("Dummy")
       Sets the property in the Adventure Combat class. */
    lua_register(pLuaState, "AdvCombatEntity_SetProperty", &Hook_AdvCombatEntity_SetProperty);
}

//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
int Hook_AdvCombatEntity_GetProperty(lua_State *L)
{
    ///--[Argument Listing]
    //--[System]
    //AdvCombatEntity_GetProperty("Dummy") (1 Integer)

    //--Argument check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("AdvCombatEntity_GetProperty");

    //--Setup
    int tReturns = 0;
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[Static]
    //--Dummy static.
    if(!strcasecmp(rSwitchType, "Static Dummy"))
    {
        lua_pushinteger(L, 0);
        return 1;
    }

    ///--[Dynamic]
    //--Type check.
    if(!DataLibrary::Fetch()->IsActiveValid(POINTER_TYPE_ADVCOMBATENTITY)) return LuaTypeError("AdvCombatEntity_GetProperty");
    AdvCombatEntity *rEntity = (AdvCombatEntity *)DataLibrary::Fetch()->rActiveObject;

    ///--[System]
    //--Dummy dynamic.
    if(!strcasecmp(rSwitchType, "Dummy") && tArgs == 1)
    {
        lua_pushinteger(L, 0);
        tReturns = 1;
    }
    //--Error case.
    else
    {
        LuaPropertyError("AdvCombatEntity_GetProperty", rSwitchType, tArgs);
    }

    return tReturns;
}
int Hook_AdvCombatEntity_SetProperty(lua_State *L)
{
    ///--[Argument Listing]
    //--[System]
    //AdvCombatEntity_SetProperty("Display Name", sName) //"DEFAULT" reverts to internal name

    //--[Statistics]
    //AdvCombatEntity_SetProperty("Statistic", iGrouping, iStatistic, iValue)
    //AdvCombatEntity_SetProperty("Recompute Stats")

    //--[Display]
    //AdvCombatEntity_SetProperty("Combat Portrait", sDLPath)
    //AdvCombatEntity_SetProperty("Combat Countermask", sDLPath)
    //AdvCombatEntity_SetProperty("Turn Icon", sDLPath)
    //AdvCombatEntity_SetProperty("Vendor Image", iSlot, sDLPath)
    //AdvCombatEntity_SetProperty("UI Render Position", iSlot, iXOffset, iYOffset)
    //AdvCombatEntity_SetProperty("Face Table Data", fLft, fTop, fRgt, fBot, sDLPath)

    //--[Jobs]
    //AdvCombatEntity_SetProperty("Create Job", sJobName) (Pushes Activity Stack)
    //AdvCombatEntity_SetProperty("Active Job", sJobName)

    //--[Abilities]
    //AdvCombatEntity_SetProperty("Create Ability", sAbilityName) (Pushes Activity Stack)
    //AdvCombatEntity_SetProperty("Set Ability Slot", iSlotX, iSlotY, sAbilityName)
    //AdvCombatEntity_SetProperty("Add Passive Ability", sAbilityName)

    //--Argument check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("AdvCombatEntity_SetProperty");

    //--Setup
    const char *rSwitchType = lua_tostring(L, 1);

    ///--[Static]
    //--Dummy static.
    if(!strcasecmp(rSwitchType, "Static Dummy"))
    {
        return 0;
    }

    ///--[Dynamic]
    //--Type check.
    if(!DataLibrary::Fetch()->IsActiveValid(POINTER_TYPE_ADVCOMBATENTITY)) return LuaTypeError("AdvCombatEntity_SetProperty");
    AdvCombatEntity *rEntity = (AdvCombatEntity *)DataLibrary::Fetch()->rActiveObject;

    ///--[System]
    //--Name that shows on the UI. Pass "DEFAULT" to return to the internal name.
    if(!strcasecmp(rSwitchType, "Display Name") && tArgs == 2)
    {
        rEntity->SetDisplayName(lua_tostring(L, 2));
    }
    ///--[Statistics]
    //--Sets the requested statistic.
    else if(!strcasecmp(rSwitchType, "Statistic") && tArgs == 4)
    {
        rEntity->SetStatistic(lua_tointeger(L, 2), lua_tointeger(L, 3), lua_tointeger(L, 4));
    }
    //--Calculates final statistics after modifications are made.
    else if(!strcasecmp(rSwitchType, "Recompute Stats") && tArgs == 1)
    {
        rEntity->ComputeStatistics(true);
    }
    ///--[Display]
    //--Sets the main portrait for the character. This appears in combat, on the UI, and a few other places.
    else if(!strcasecmp(rSwitchType, "Combat Portrait") && tArgs == 2)
    {
        rEntity->SetCombatPortrait(lua_tostring(L, 2));
    }
    //--Sets the optional countermask, used to prevent a character from rendering something that goes off
    //  and back on to the frame when on certain parts of the UI.
    else if(!strcasecmp(rSwitchType, "Combat Countermask") && tArgs == 2)
    {
        rEntity->SetCombatCountermask(lua_tostring(L, 2));
    }
    //--Set the turn icon that appears in combat.
    else if(!strcasecmp(rSwitchType, "Turn Icon") && tArgs == 2)
    {
        rEntity->SetTurnIcon(lua_tostring(L, 2));
    }
    //--Sets images that show during the vendor UI.
    else if(!strcasecmp(rSwitchType, "Vendor Image") && tArgs == 3)
    {
        rEntity->SetVendorImage(lua_tointeger(L, 2), lua_tostring(L, 3));
    }
    //--Render position of the combat portrait at various UI locations.
    else if(!strcasecmp(rSwitchType, "UI Render Position") && tArgs == 4)
    {
        rEntity->SetUIRenderPos(lua_tointeger(L, 2), lua_tointeger(L, 3), lua_tointeger(L, 4));
    }
    //--Set dimensions of the face table used on equipment and inventory rendering.
    else if(!strcasecmp(rSwitchType, "Face Table Data") && tArgs == 6)
    {
        rEntity->SetFaceTableData(lua_tonumber(L, 2), lua_tonumber(L, 3), lua_tonumber(L, 4), lua_tonumber(L, 5), lua_tostring(L, 6));
    }
    ///--[Jobs]
    //--Creates a new AdvCombatJob and pushes it on the activity stack.
    else if(!strcasecmp(rSwitchType, "Create Job") && tArgs == 2)
    {
        AdvCombatJob *nNewJob = new AdvCombatJob();
        nNewJob->SetInternalName(lua_tostring(L, 2));
        rEntity->RegisterJob(lua_tostring(L, 2), nNewJob);
        DataLibrary::Fetch()->PushActiveEntity(nNewJob);
    }
    //--Orders the character to change to the requested job.
    else if(!strcasecmp(rSwitchType, "Active Job") && tArgs == 2)
    {
        rEntity->SetActiveJob(lua_tostring(L, 2));
    }
    ///--[Abilities]
    //--Creates a new ability and pushes the activity stack.
    else if(!strcasecmp(rSwitchType, "Create Ability") && tArgs == 2)
    {
        AdvCombatAbility *nNewAbility = new AdvCombatAbility();
        rEntity->RegisterAbility(lua_tostring(L, 2), nNewAbility);
        DataLibrary::Fetch()->PushActiveEntity(nNewAbility);
    }
    //--Populates a slot with the named ability from the master list.
    else if(!strcasecmp(rSwitchType, "Set Ability Slot") && tArgs == 4)
    {
        rEntity->SetAbilitySlot(lua_tointeger(L, 2), lua_tointeger(L, 3), lua_tostring(L, 4));
    }
    //--Adds an ability from the master list to the passive listing.
    else if(!strcasecmp(rSwitchType, "Add Passive Ability") && tArgs == 2)
    {
        rEntity->AddPassiveAbility(lua_tostring(L, 2));
    }
    ///--[Error]
    //--Error case.
    else
    {
        LuaPropertyError("AdvCombatEntity_SetProperty", rSwitchType, tArgs);
    }

    return 0;
}
