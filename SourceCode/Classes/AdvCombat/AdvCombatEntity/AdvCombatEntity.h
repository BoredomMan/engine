//--[AdvCombatEntity]
//--Represents an entity in combat, either party member or enemy.

//--[Statistics Information]
//--The AdvCombatEntity has many sets of statistics. A description of the types follows:
//--mBaseStatistics
//  Base statistics for the character that never change. These are not influenced by levels.
//  For enemies, these are the statistics they use as enemies never change jobs or use equipment.
//--mJobStatistics
//  Stat bonuses applied from the character's current job. Can change in combat.
//--mEquipmentStatistics
//  Stat bonuses applied from the character's equipment. Changes when they change equipment. Includes
//  all gem bonuses.
//--mScriptStatistics
//  Bonuses as applied by scripts, used for plot events. Under normal circumstances, these are zeroes.
//--mPermaEffectStatistics
//  Bonuses applied by effects that are permanent in some way. For example, some jobs have passives
//  that apply on top of their base job statistics, or effects that are applied by abilities equipped.
//--mTemporaryEffectStatistics
//  Bonuses from effects that are currently applied. These expire at the end of combat.
//--mSummedStatistics
//  All stats added together. This is the set that is queried in combat.

#pragma once

//--[Includes]
#include "Definitions.h"
#include "Structures.h"
#include "AdvCombatDefStruct.h"

//--[Local Structures]
//--[Local Definitions]
//--Statistic Groupings
#define ADVCE_STATS_BASE 0
#define ADVCE_STATS_JOB 1
#define ADVCE_STATS_EQUIPMENT 2
#define ADVCE_STATS_SCRIPT 3
#define ADVCE_STATS_PERMAEFFECT 4
#define ADVCE_STATS_TEMPEFFECT 5
#define ADVCE_STATS_FINAL 6
#define ADVCE_STATS_TOTAL 7

//--Vendor Sprites
#define ADVCE_VENDOR_SPRITES_TOTAL 4

//--[Classes]
class AdvCombatEntity : public RootObject
{
    private:
    //--System
    char *mLocalName;
    char *mDisplayName;

    //--Persistent Statistics
    int mHP;

    //--Combat Position
    int mCombatMoveTimer;
    int mCombatMoveTimerMax;
    float mCombatIdealX;
    float mCombatIdealY;
    float mCombatStartX;
    float mCombatStartY;
    float mCombatCurrentX;
    float mCombatCurrentY;
    float mCombatTargetX;
    float mCombatTargetY;

    //--Turn Order
    int mTurnID;
    int mCurrentInitiative;

    //--Statistics
    CombatStatistics mBaseStatistics;
    CombatStatistics mJobStatistics;
    CombatStatistics mEquipmentStatistics;
    CombatStatistics mScriptStatistics;
    CombatStatistics mPermaEffectStatistics;
    CombatStatistics mTemporaryEffectStatistics;
    CombatStatistics mSummedStatistics;

    //--Jobs
    AdvCombatJob *rActiveJob;
    SugarLinkedList *mJobList; //AdvCombatJob *, master

    //--Reinforcement Handling
    int mReinforcementTurns;

    //--Ability Listing
    SugarLinkedList *mAbilityList; //AdvCombatAbility *, master
    AdvCombatAbility *rAbilityGrid[ACE_ABILITY_GRID_SIZE_X][ACE_ABILITY_GRID_SIZE_Y];
    SugarLinkedList *mrPassiveAbilityList; //AdvCombatAbility *, ref

    //--UI Positions
    TwoDimensionRealPoint mUIPositions[ACE_UI_INDEX_TOTAL];
    TwoDimensionReal mFaceTableDim;

    //--Images
    SugarBitmap *rCombatImage;
    SugarBitmap *rCombatCountermask;
    SugarBitmap *rTurnIcon;
    SugarBitmap *rVendorImages[ADVCE_VENDOR_SPRITES_TOTAL];
    SugarBitmap *rFaceTableImg;

    protected:

    public:
    //--System
    AdvCombatEntity();
    virtual ~AdvCombatEntity();

    //--Public Variables
    //--Property Queries
    const char *GetName();
    const char *GetDisplayName();
    int GetReinforcementTurns();
    SugarBitmap *GetFaceProperties(TwoDimensionReal &sDimensions);
    TwoDimensionRealPoint GetUIRenderPosition(int pIndex);
    bool CanActThisTurn();
    int GetCurrentInitiative();
    int ComputeTurnBucket();
    int GetTurnID();

    //--Property Queries - World Statistics
    int GetLevel();
    int GetXP();
    int GetXPToNextLevel();

    //--Manipulators
    void SetInternalName(const char *pName);
    void SetDisplayName(const char *pName);
    void SetReinforcementTurns(int pTurns);
    void RegisterJob(const char *pName, AdvCombatJob *pJob);
    void SetActiveJob(const char *pJobName);
    void SetCombatPortrait(const char *pDLPath);
    void SetCombatCountermask(const char *pDLPath);
    void SetTurnIcon(const char *pDLPath);
    void SetVendorImage(int pSlot, const char *pDLPath);
    void SetUIRenderPos(int pSlot, int pX, int pY);
    void SetFaceTableData(float pLft, float pTop, float pRgt, float pBot, const char *pPath);
    void SetIdealPosition(float pXPos, float pYPos);
    void MoveToPosition(float pXPos, float pYPos, int pTicks);
    void MoveToIdealPosition(int pTicks);
    void SetCurrentInitiative(int pValue);
    void SetTurnID(int pValue);
    void SetAbilitySlot(int pSlotX, int pSlotY, const char *pAbilityName);
    void AddPassiveAbility(const char *pAbilityName);

    //--Core Methods
    void FullRestore();
    void HandleCombatStart();

    //--Abilities
    void RegisterAbility(const char *pAbilityName, AdvCombatAbility *pAbility);

    //--Equipment
    AdventureItem *GetEquipmentBySlot(const char *pEquipSlot);
    void EquipItemToSlot(const char *pSlotName, AdventureItem *pItem);

    //--Statistics
    int GetHealth();
    float GetHealthPercent();
    int GetStatistic(int pGroupIndex, int pStatisticIndex);
    void SetStatistic(int pGroupIndex, int pStatisticIndex, int pValue);
    void ComputeStatistics(bool pRerunEffects);
    CombatStatistics *GetStatisticsGroup(int pIndex);
    CombatStatistics *GetStatisticsGroup(const char *pName);

    //--Dummy
    int GetPropertyByInventoryHeaders(int pDum, bool pDumDum);

    private:
    //--Private Core Methods
    public:
    //--Update
    void UpdatePosition();

    //--File I/O
    //--Drawing
    SugarBitmap *GetCombatPortrait();
    SugarBitmap *GetCounterMask();
    SugarBitmap *GetTurnIcon();

    //--Pointer Routing
    SugarLinkedList *GetAbilityList();
    AdvCombatAbility *GetAbilityBySlot(int pX, int pY);
    AdvCombatJob *GetActiveJob();

    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_AdvCombatEntity_GetProperty(lua_State *L);
int Hook_AdvCombatEntity_SetProperty(lua_State *L);

