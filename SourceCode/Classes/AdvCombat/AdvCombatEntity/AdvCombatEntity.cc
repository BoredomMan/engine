//--Base
#include "AdvCombatEntity.h"

//--Classes
#include "AdvCombat.h"
#include "AdvCombatAbility.h"
#include "AdvCombatJob.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarLinkedList.h"

//--Definitions
#include "EasingFunctions.h"

//--Libraries
#include "DataLibrary.h"

//--Managers

//=========================================== System ==============================================
AdvCombatEntity::AdvCombatEntity()
{
    //--[RootObject]
    //--System
    mType = POINTER_TYPE_ADVCOMBATENTITY;

    //--[AdvCombatEntity]
    //--System
    mLocalName = InitializeString("Character");
    mDisplayName = InitializeString("Character");

    //--Persistent Statistics
    mHP = 1;

    //--Combat Position
    mCombatMoveTimer = 1;
    mCombatMoveTimerMax = 1;
    mCombatIdealX = 0.0f;
    mCombatIdealY = 0.0f;
    mCombatStartX = 0.0f;
    mCombatStartY = 0.0f;
    mCombatCurrentX = 0.0f;
    mCombatCurrentY = 0.0f;
    mCombatTargetX = 0.0f;
    mCombatTargetY = 0.0f;

    //--Turn Order
    mTurnID = 0;
    mCurrentInitiative = 0;

    //--Statistics
    mBaseStatistics.Initialize();
    mJobStatistics.Initialize();
    mEquipmentStatistics.Initialize();
    mScriptStatistics.Initialize();
    mPermaEffectStatistics.Initialize();
    mTemporaryEffectStatistics.Initialize();
    mSummedStatistics.Initialize();

    //--Jobs
    rActiveJob = NULL;
    mJobList = new SugarLinkedList(true);

    //--Reinforcement Handling
    mReinforcementTurns = 0;

    //--Ability Listing
    mAbilityList = new SugarLinkedList(true);
    for(int x = 0; x < ACE_ABILITY_GRID_SIZE_X; x ++)
    {
        for(int y = 0; y < ACE_ABILITY_GRID_SIZE_Y; y ++)
        {
            rAbilityGrid[x][y] = NULL;
        }
    }
    mrPassiveAbilityList = new SugarLinkedList(false);

    //--UI Positions
    memset(&mUIPositions, 0, sizeof(TwoDimensionRealPoint) * ACE_UI_INDEX_TOTAL);
    mFaceTableDim.SetWH(0.0f, 0.0f, 1.0f, 1.0f);

    //--Images
    rCombatImage = NULL;
    rCombatCountermask = NULL;
    rTurnIcon = NULL;
    memset(rVendorImages, 0, sizeof(SugarBitmap *) * ADVCE_VENDOR_SPRITES_TOTAL);
    rFaceTableImg = NULL;
}
AdvCombatEntity::~AdvCombatEntity()
{
    free(mLocalName);
    free(mDisplayName);
    delete mJobList;
    delete mAbilityList;
    delete mrPassiveAbilityList;
}

//====================================== Property Queries =========================================
const char *AdvCombatEntity::GetName()
{
    return mLocalName;
}
const char *AdvCombatEntity::GetDisplayName()
{
    return mDisplayName;
}
int AdvCombatEntity::GetReinforcementTurns()
{
    return mReinforcementTurns;
}
SugarBitmap *AdvCombatEntity::GetFaceProperties(TwoDimensionReal &sDimensions)
{
    memcpy(&sDimensions, &mFaceTableDim, sizeof(TwoDimensionReal));
    return rFaceTableImg;
}
TwoDimensionRealPoint AdvCombatEntity::GetUIRenderPosition(int pIndex)
{
    if(pIndex < 0 || pIndex >= ACE_UI_INDEX_TOTAL)
    {
        TwoDimensionRealPoint tDummy;
        tDummy.mXCenter = 0.0f;
        tDummy.mYCenter = 0.0f;
        return tDummy;
    }
    return mUIPositions[pIndex];
}
bool AdvCombatEntity::CanActThisTurn()
{
    return true;
}
int AdvCombatEntity::GetCurrentInitiative()
{
    return mCurrentInitiative;
}
int AdvCombatEntity::ComputeTurnBucket()
{
    int tReturnVal = ACE_BUCKET_NEUTRAL;
    if(false) tReturnVal += ACE_BUCKET_ALWAYS_STRIKES_LAST;
    if(false) tReturnVal += ACE_BUCKET_SLOW;
    if(false) tReturnVal += ACE_BUCKET_FAST;
    if(false) tReturnVal += ACE_BUCKET_ALWAYS_STRIKES_FIRST;

    return tReturnVal;
}
int AdvCombatEntity::GetTurnID()
{
    return mTurnID;
}

//============================= Property Queries - World Statistics ===============================
int AdvCombatEntity::GetLevel()
{
    return 0;
}
int AdvCombatEntity::GetXP()
{
    return 1;
}
int AdvCombatEntity::GetXPToNextLevel()
{
    return 10000;
}

//--[Dummy Functions, Remove Later]
int AdvCombatEntity::GetPropertyByInventoryHeaders(int pDum, bool pDumDum)
{
    return 0;
}
//========================================= Manipulators ==========================================
void AdvCombatEntity::SetInternalName(const char *pName)
{
    ResetString(mLocalName, pName);
}
void AdvCombatEntity::SetDisplayName(const char *pName)
{
    //--If the string passed is "DEFUALT", use the local name.
    if(!strcasecmp(pName, "DEFAULT"))
    {
        ResetString(mDisplayName, mLocalName);
    }
    //--Use the provided name.
    else
    {
        ResetString(mDisplayName, pName);
    }
}
void AdvCombatEntity::SetReinforcementTurns(int pTurns)
{
    mReinforcementTurns = pTurns;
}
void AdvCombatEntity::RegisterJob(const char *pName, AdvCombatJob *pJob)
{
    if(!pName || !pJob) return;
    mJobList->AddElement(pName, pJob, &RootObject::DeleteThis);
}
void AdvCombatEntity::SetActiveJob(const char *pJobName)
{
    //--Set job.
    rActiveJob = (AdvCombatJob *)mJobList->GetElementByName(pJobName);
    if(rActiveJob) rActiveJob->AssumeJob(this);

    //--Recompute stats.
    ComputeStatistics(true);
}
void AdvCombatEntity::SetCombatPortrait(const char *pDLPath)
{
    rCombatImage = (SugarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath);
}
void AdvCombatEntity::SetCombatCountermask(const char *pDLPath)
{
    rCombatCountermask = (SugarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath);
}
void AdvCombatEntity::SetTurnIcon(const char *pDLPath)
{
    rTurnIcon = (SugarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath);
}
void AdvCombatEntity::SetVendorImage(int pSlot, const char *pDLPath)
{
    if(pSlot < 0 || pSlot >= ADVCE_VENDOR_SPRITES_TOTAL) return;
    rVendorImages[pSlot] = (SugarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath);
}
void AdvCombatEntity::SetUIRenderPos(int pSlot, int pX, int pY)
{
    if(pSlot < 0 || pSlot >= ACE_UI_INDEX_TOTAL) return;
    mUIPositions[pSlot].mXCenter = pX;
    mUIPositions[pSlot].mYCenter = pY;
}
void AdvCombatEntity::SetFaceTableData(float pLft, float pTop, float pRgt, float pBot, const char *pPath)
{
    if(!pPath) return;
    rFaceTableImg = (SugarBitmap *)DataLibrary::Fetch()->GetEntry(pPath);
    mFaceTableDim.Set(pLft, pTop, pRgt, pBot);

    //--If the image exists, recompute face table positions.
    if(!rFaceTableImg) return;
    float cWid = rFaceTableImg->GetWidth();
    float cHei = rFaceTableImg->GetHeight();
    mFaceTableDim.mLft = mFaceTableDim.mLft / cWid;
    mFaceTableDim.mTop = mFaceTableDim.mTop / cHei;
    mFaceTableDim.mRgt = mFaceTableDim.mRgt / cWid;
    mFaceTableDim.mBot = mFaceTableDim.mBot / cHei;
    mFaceTableDim.mXCenter = mFaceTableDim.mXCenter / cWid;
    mFaceTableDim.mYCenter = mFaceTableDim.mYCenter / cHei;
}
void AdvCombatEntity::SetIdealPosition(float pXPos, float pYPos)
{
    mCombatIdealX = pXPos;
    mCombatIdealY = pYPos;
}
void AdvCombatEntity::MoveToPosition(float pXPos, float pYPos, int pTicks)
{
    //--Pass 0 or lower to move to the position instantly.
    if(pTicks < 1)
    {
        mCombatMoveTimer = 1;
        mCombatMoveTimerMax = 1;
        mCombatStartX = pXPos;
        mCombatStartY = pYPos;
        mCombatCurrentX = pXPos;
        mCombatCurrentY = pYPos;
        mCombatTargetX = pXPos;
        mCombatTargetY = pYPos;
    }
    else
    {
        mCombatMoveTimer = 0;
        mCombatMoveTimerMax = pTicks;
        mCombatStartX = mCombatCurrentX;
        mCombatStartY = mCombatCurrentY;
        mCombatTargetX = pXPos;
        mCombatTargetY = pYPos;
    }
}
void AdvCombatEntity::MoveToIdealPosition(int pTicks)
{
    MoveToPosition(mCombatIdealX, mCombatIdealY, pTicks);
}
void AdvCombatEntity::SetCurrentInitiative(int pValue)
{
    mCurrentInitiative = pValue;
}
void AdvCombatEntity::SetTurnID(int pValue)
{
    mTurnID = pValue;
}
void AdvCombatEntity::SetAbilitySlot(int pSlotX, int pSlotY, const char *pAbilityName)
{
    //--Range check.
    if(pSlotX < 0 || pSlotX >= ACE_ABILITY_GRID_SIZE_X) return;
    if(pSlotY < 0 || pSlotY >= ACE_ABILITY_GRID_SIZE_Y) return;

    //--Special: If the ability is "System|Retreat" or "System|Surrender", those are acquired from the AdvCombat class.
    if(!strcasecmp(pAbilityName, "System|Retreat"))
    {
        fprintf(stderr, "Set retreat in slot %i %i\n", pSlotX, pSlotY);
        rAbilityGrid[pSlotX][pSlotY] = AdvCombat::Fetch()->GetSystemRetreat();
        return;
    }
    else if(!strcasecmp(pAbilityName, "System|Surrender"))
    {
        fprintf(stderr, "Set surrender in slot %i %i\n", pSlotX, pSlotY);
        rAbilityGrid[pSlotX][pSlotY] = AdvCombat::Fetch()->GetSystemSurrender();
        return;
    }

    //--Get the ability. If it comes back NULL, or "Null" is passed in, we can still set the slot to null
    //  to indicate it is empty.
    AdvCombatAbility *rAbility = NULL;
    if(strcasecmp(pAbilityName, "Null")) rAbility = (AdvCombatAbility *)mAbilityList->GetElementByName(pAbilityName);
    rAbilityGrid[pSlotX][pSlotY] = rAbility;
}
void AdvCombatEntity::AddPassiveAbility(const char *pAbilityName)
{
    AdvCombatAbility *rCheckPtr = (AdvCombatAbility *)mAbilityList->GetElementByName(pAbilityName);
    if(rCheckPtr) mrPassiveAbilityList->AddElementAsTail(pAbilityName, rCheckPtr);
}

//========================================= Core Methods ==========================================
void AdvCombatEntity::FullRestore()
{
    //--Restores all of an entity's HP and other stats that are restored at a rest point.
    mHP = mSummedStatistics.GetStatByIndex(STATS_HPMAX);
}
void AdvCombatEntity::HandleCombatStart()
{
    //--When combat begins, this is called for the player's party. Enemy units never call this.

    //--Reset passive abilities. These will be re-added by the AdvCombatJob later.
    mrPassiveAbilityList->ClearList();

    //--Order the active job to run its combat-begins script.
    if(rActiveJob) rActiveJob->CallAtCombatStart(this);

    //--Now iterate across all equipped abilities and call their combat start script.
    for(int x = 0; x < ACE_ABILITY_GRID_SIZE_X; x ++)
    {
        for(int y = 0; y < ACE_ABILITY_GRID_SIZE_Y; y ++)
        {
            if(!rAbilityGrid[x][y]) continue;
            rAbilityGrid[x][y]->CallCombatStart();
        }
    }
    mrPassiveAbilityList = new SugarLinkedList(false);
}

//===================================== Private Core Methods ======================================
//============================================ Update =============================================
void AdvCombatEntity::UpdatePosition()
{
    //--Updates the entity's position by the movement timer.
    if(mCombatMoveTimer >= mCombatMoveTimerMax) return;

    //--Increment timer:
    mCombatMoveTimer ++;

    //--Ending case:
    if(mCombatMoveTimer >= mCombatMoveTimerMax)
    {
        mCombatCurrentX = mCombatTargetX;
        mCombatCurrentY = mCombatTargetY;
    }
    //--Intermediate case:
    else
    {
        float cPercent = EasingFunction::QuadraticOut(mCombatMoveTimer, mCombatMoveTimerMax);
        mCombatCurrentX = mCombatStartX + ((mCombatTargetX - mCombatStartX) * cPercent);
        mCombatCurrentY = mCombatStartY + ((mCombatTargetY - mCombatStartY) * cPercent);
    }
}

//=========================================== File I/O ============================================
//=========================================== Drawing =============================================
SugarBitmap *AdvCombatEntity::GetCombatPortrait()
{
    return rCombatImage;
}
SugarBitmap *AdvCombatEntity::GetCounterMask()
{
    return rCombatCountermask;
}
SugarBitmap *AdvCombatEntity::GetTurnIcon()
{
    return rTurnIcon;
}

//======================================= Pointer Routing =========================================
SugarLinkedList *AdvCombatEntity::GetAbilityList()
{
    return mAbilityList;
}
AdvCombatAbility *AdvCombatEntity::GetAbilityBySlot(int pX, int pY)
{
    if(pX < 0 || pX >= ACE_ABILITY_GRID_SIZE_X) return NULL;
    if(pY < 0 || pY >= ACE_ABILITY_GRID_SIZE_Y) return NULL;
    return rAbilityGrid[pX][pY];
}
AdvCombatJob *AdvCombatEntity::GetActiveJob()
{
    return rActiveJob;
}

//====================================== Static Functions =========================================
//========================================= Lua Hooking ===========================================
//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
