//--Base
#include "AdvCombatEntity.h"

//--Classes
#include "AdvCombatAbility.h"

//--CoreClasses
#include "SugarLinkedList.h"

//--Definitions
//--Libraries
//--Managers

void AdvCombatEntity::RegisterAbility(const char *pAbilityName, AdvCombatAbility *pAbility)
{
    //--Registers a new ability and takes explicit ownership of it.
    if(!pAbilityName || !pAbility) return;
    pAbility->SetOwner(this);
    pAbility->SetInternalName(pAbilityName);
    mAbilityList->AddElement(pAbilityName, pAbility, &RootObject::DeleteThis);
}
