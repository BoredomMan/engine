//--Base
#include "AdvCombatEntity.h"

//--Classes
//--CoreClasses
//--Definitions
//--Libraries
//--Managers

//--[Property Queries]
int AdvCombatEntity::GetHealth()
{
    return mHP;
}
float AdvCombatEntity::GetHealthPercent()
{
    //--Range-check on max HP.
    int tHPMax = mSummedStatistics.GetStatByIndex(STATS_HPMAX);
    if(tHPMax < 1) tHPMax = 1;
    return (float)mHP / (float)tHPMax;
}
int AdvCombatEntity::GetStatistic(int pGroupIndex, int pStatisticIndex)
{
    CombatStatistics *rStatGroup = GetStatisticsGroup(pGroupIndex);
    return rStatGroup->GetStatByIndex(pStatisticIndex);
}

//--[Manipulators]
void AdvCombatEntity::SetStatistic(int pGroupIndex, int pStatisticIndex, int pValue)
{
    CombatStatistics *rStatGroup = GetStatisticsGroup(pGroupIndex);
    rStatGroup->SetStatByIndex(pStatisticIndex, pValue);
}

//--[Core Methods]
void AdvCombatEntity::ComputeStatistics(bool pRerunEffects)
{
    //--If the effects need to be rerun, zero them off and run their query handlers.
    if(pRerunEffects)
    {
        //--Set to zero.
        mTemporaryEffectStatistics.Zero();

        //--Run all effects to sum up their bonuses.
    }

    //--Clear the final effect holder.
    mSummedStatistics.Zero();

    //--Now add all the values together.
    mSummedStatistics.AddStatistics(mBaseStatistics);
    mSummedStatistics.AddStatistics(mJobStatistics);
    mSummedStatistics.AddStatistics(mEquipmentStatistics);
    mSummedStatistics.AddStatistics(mScriptStatistics);
    mSummedStatistics.AddStatistics(mPermaEffectStatistics);
    mSummedStatistics.AddStatistics(mTemporaryEffectStatistics);

    //--Clamp to make sure nothing was out of range.
    mSummedStatistics.ClampStatistics();
    fprintf(stderr, "Computed max HP to be %i\n", mSummedStatistics.GetStatByIndex(STATS_HPMAX));
}

//--[Pointer Routing]
CombatStatistics *AdvCombatEntity::GetStatisticsGroup(int pIndex)
{
    //--Get stat group by index.
    if(pIndex == ADVCE_STATS_BASE)        return &mBaseStatistics;
    if(pIndex == ADVCE_STATS_JOB)         return &mJobStatistics;
    if(pIndex == ADVCE_STATS_EQUIPMENT)   return &mEquipmentStatistics;
    if(pIndex == ADVCE_STATS_SCRIPT)      return &mScriptStatistics;
    if(pIndex == ADVCE_STATS_PERMAEFFECT) return &mPermaEffectStatistics;
    if(pIndex == ADVCE_STATS_TEMPEFFECT)  return &mTemporaryEffectStatistics;
    if(pIndex == ADVCE_STATS_FINAL)       return &mSummedStatistics;

    //--Range errors always return the base statistics.
    fprintf(stderr, "Warning: Statistics index %i is out of range.\n", pIndex);
    return &mBaseStatistics;
}
CombatStatistics *AdvCombatEntity::GetStatisticsGroup(const char *pName)
{
    //--Errors return the base statistics.
    if(!pName)
    {
        fprintf(stderr, "Warning: Null statistics group requested, returning base.\n");
        return &mBaseStatistics;
    }

    //--Groupings by string.
    if(!strcasecmp(pName, "Base"))        return &mBaseStatistics;
    if(!strcasecmp(pName, "Job"))         return &mJobStatistics;
    if(!strcasecmp(pName, "Equipment"))   return &mEquipmentStatistics;
    if(!strcasecmp(pName, "Script"))      return &mScriptStatistics;
    if(!strcasecmp(pName, "PermaEffect")) return &mPermaEffectStatistics;
    if(!strcasecmp(pName, "TempEffect"))  return &mTemporaryEffectStatistics;

    //--If not found, return base statistics.
    fprintf(stderr, "Warning: Statistics group %s not found.\n", pName);
    return &mBaseStatistics;
}
