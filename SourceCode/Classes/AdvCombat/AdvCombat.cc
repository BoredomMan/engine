//--Base
#include "AdvCombat.h"

//--Classes
//--CoreClasses
#include "SugarLinkedList.h"

//--Definitions
//--Libraries
//--Managers
#include "MapManager.h"

//=========================================== System ==============================================
AdvCombat::AdvCombat()
{
    Initialize();
}
AdvCombat::~AdvCombat()
{
    Disassemble();
}

//====================================== Property Queries =========================================
bool AdvCombat::IsActive()
{
    return mIsActive;
}
bool AdvCombat::IsStoppingWorldUpdate()
{
    return mIsActive;
}
bool AdvCombat::DoesPlayerHaveInitiative()
{
    return false;
}

//========================================= Manipulators ==========================================
void AdvCombat::Activate()
{
    mIsActive = true;
}
void AdvCombat::Deactivate()
{
    mIsActive = false;
}
void AdvCombat::SetDefaultCombatMusic(const char *pMusicName)
{
    ResetString(mDefaultCombatMusic, pMusicName);
}
void AdvCombat::SetUnloseable(bool pFlag)
{
    mIsUnloseable = pFlag;
}
void AdvCombat::SetUnwinnable(bool pFlag)
{
    mIsUnwinnable = pFlag;
}
void AdvCombat::SetUnretreatable(bool pFlag)
{
    mIsUnretreatable = pFlag;
}
void AdvCombat::SetUnsurrenderable(bool pFlag)
{
    mIsUnsurrenderable = pFlag;
}

//========================================= Core Methods ==========================================
void AdvCombat::HealFromDoctorBag(int pPartyIndex)
{

}

//===================================== Private Core Methods ======================================
//============================================ Update =============================================
//=========================================== File I/O ============================================
//=========================================== Drawing =============================================
//======================================= Pointer Routing =========================================
AdvCombatEntity *AdvCombat::GetRosterMemberI(int pIndex)
{
    return (AdvCombatEntity *)mPartyRoster->GetElementBySlot(pIndex);
}
AdvCombatEntity *AdvCombat::GetRosterMemberS(const char *pName)
{
    return (AdvCombatEntity *)mPartyRoster->GetElementByName(pName);
}
AdvCombatEntity *AdvCombat::GetActiveMemberI(int pIndex)
{
    return (AdvCombatEntity *)mrActiveParty->GetElementBySlot(pIndex);
}
AdvCombatEntity *AdvCombat::GetActiveMemberS(const char *pName)
{
    return (AdvCombatEntity *)mrActiveParty->GetElementByName(pName);
}
AdvCombatEntity *AdvCombat::GetCombatMemberI(int pIndex)
{
    return (AdvCombatEntity *)mrCombatParty->GetElementBySlot(pIndex);
}
AdvCombatEntity *AdvCombat::GetCombatMemberS(const char *pName)
{
    return (AdvCombatEntity *)mrCombatParty->GetElementByName(pName);
}
AliasStorage *AdvCombat::GetEnemyAliasStorage()
{
    return mEnemyAliases;
}
AdvCombatEntity *AdvCombat::GetActingEntity()
{
    //--Can return NULL if no entity is currently acting.
    return (AdvCombatEntity *)mrTurnOrder->GetElementBySlot(0);
}
AdvCombatAbility *AdvCombat::GetSystemRetreat()
{
    return mSysAbilityRetreat;
}
AdvCombatAbility *AdvCombat::GetSystemSurrender()
{
    return mSysAbilitySurrender;
}

//====================================== Static Functions =========================================
AdvCombat *AdvCombat::Fetch()
{
    return MapManager::Fetch()->GetAdventureCombat();
}

//========================================= Lua Hooking ===========================================
//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
