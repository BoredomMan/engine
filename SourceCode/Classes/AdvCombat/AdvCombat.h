//--[AdvCombat]
//--Adventure Mode's combat engine.

//--Party Notes:
//--The player's party is split into the Roster, the Active Party, and the Combat Party.
//--The Roster is all of the player's party members, including ones not in the party. This is
//  used when switching party members later in the game.
//--The Active Party is the characters in the player's party out of combat.
//--The Combat Party is the characters under the player's control in combat. This starts as a
//  copy of the Active Party, but enemies can change sides and the player's party can change sides.

#pragma once

//--[Includes]
#include "Definitions.h"
#include "Structures.h"
#include "DisplayManager.h"

//--[Local Structures]
//--[Local Definitions]
//--Timers
#define ADVCOMBAT_HIGHLIGHT_MOVE_TICKS 10
#define ADVCOMBAT_CONFIRMATION_TICKS 15

//--Rendering
#define ADVCOMBAT_STD_BACKING_OPACITY 0.75f

//--Combat Resolution Types
#define ADVCOMBAT_END_NONE 0
#define ADVCOMBAT_END_VICTORY 1
#define ADVCOMBAT_END_DEFEAT 2
#define ADVCOMBAT_END_RETREAT 3
#define ADVCOMBAT_END_SURRENDER 4
#define ADVCOMBAT_END_TOTAL 5

//--Victory
#define ADVCOMBAT_VICTORY_OVERLAY 0
#define ADVCOMBAT_VICTORY_OVERLAY_TICKS 42
#define ADVCOMBAT_VICTORY_OVERLAY_TICKS_PER_LETTER 4

//--Defeat
#define ADVCOMBAT_DEFEAT_FRAMES_TOTAL 6
#define ADVCOMBAT_DEFEAT_TICKS_PER_LETTER 15
#define ADVCOMBAT_DEFEAT_TICKS_LETTERS (ADVCOMBAT_DEFEAT_TICKS_PER_LETTER * 6)
#define ADVCOMBAT_DEFEAT_TICKS_HOLD 45
#define ADVCOMBAT_DEFEAT_TICKS_FADEOUT 45

//--Retreat
#define ADVCOMBAT_RETREAT_TICKS 30

//--Positions
#define ADVCOMBAT_POSITION_STD_Y (VIRTUAL_CANVAS_Y * 0.50f)
#define ADVCOMBAT_POSITION_ABILITY_X 269.0f
#define ADVCOMBAT_POSITION_ABILITY_Y 526.0f
#define ADVCOMBAT_POSITION_ABILITY_W 54.0f
#define ADVCOMBAT_POSITION_ABILITY_H 70.0f

//--Move Transition
#define ADVCOMBAT_MOVE_TRANSITION_TICKS 25

//--[Forward Declarations]
struct TargetCluster;

//--[Function Pointers]
typedef void(*ConfirmationFnPtr)();

//--[Classes]
class AdvCombat : public RootObject
{
    private:
    //--System
    bool mCombatReinitialized;
    bool mIsActive;
    bool mWasLastDefeatSurrender;

    //--Introduction
    bool mIsIntroduction;
    int mIntroTimer;

    //--Turn State
    int mCurrentTurn;
    int mTurnDisplayTimer;
    int mTurnBarMoveTimer;
    int mTurnBarMoveTimerMax;
    int mTurnWidStart;
    int mTurnWidCurrent;
    int mTurnWidTarget;
    SugarLinkedList *mrTurnOrder; //AdvCombatEntity *, ref
    SugarBitmap *rTurnDiscardImg;

    //--Action Handling
    bool mActionFirstTick;
    bool mIsActionConcluding;
    int mPlayerInterfaceTimer;
    int mAbilitySelectionX;
    int mAbilitySelectionY;
    EasingPack2D mAbilityHighlightPack;

    //--Target Selection
    bool mLockTargetClusters;
    int mTargetClusterCounter;
    bool mIsSelectingTargets;
    int mTargetFlashTimer;
    SugarLinkedList *mTargetClusters; //TargetCluster *, master

    //--Confirmation Window
    bool mIsShowingConfirmationWindow;
    int mConfirmationTimer;
    StarlightString *mConfirmationString;
    StarlightString *mAcceptString;
    StarlightString *mCancelString;
    ConfirmationFnPtr rConfirmationFunction;

    //--Ability Execution
    AdvCombatAbility *rActiveAbility;
    TargetCluster *rActiveTargetCluster;

    //--System Abilities
    AdvCombatAbility *mSysAbilityRetreat;
    AdvCombatAbility *mSysAbilitySurrender;

    //--Option Flags
    bool mAutoUseDoctorBag;
    bool mTouristMode;
    bool mMemoryCursor;

    //--Combat State Flags
    bool mIsUnwinnable;
    bool mIsUnloseable;
    bool mIsUnretreatable;
    bool mIsUnsurrenderable;
    int mResolutionTimer;
    int mResolutionState;
    int mCombatResolution;

    //--Enemy Storage
    int mUniqueEnemyIDCounter;
    AliasStorage *mEnemyAliases;
    SugarLinkedList *mEnemyRoster; //AdvCombatEntity *, master
    SugarLinkedList *mrEnemyCombatParty; //AdvCombatEntity *, ref
    SugarLinkedList *mrEnemyReinforcements; //AdvCombatEntity *, ref
    SugarLinkedList *mWorldReferences; //WorldRefPack *, master

    //--Party Storage
    SugarLinkedList *mPartyRoster; //AdvCombatEntity *, master
    SugarLinkedList *mrActiveParty; //AdvCombatEntity *, ref
    SugarLinkedList *mrCombatParty; //AdvCombatEntity *, ref

    //--Effect Storage
    int mGlobalEffectIDs;
    SugarLinkedList *mGlobalEffectList; //AdvCombatEffect *, master

    //--Cutscene Handlers
    char *mStandardRetreatScript;
    char *mStandardDefeatScript;
    char *mStandardVictoryScript;
    char *mCurrentRetreatScript;
    char *mCurrentDefeatScript;
    char *mCurrentVictoryScript;

    //--Audio
    char *mDefaultCombatMusic;

    //--Images
    struct
    {
        bool mIsReady;
        struct
        {
            //--Fonts
            SugarFont *rDamageFont;
            SugarFont *rVictoryFont;
            SugarFont *rAllyHPFont;
            SugarFont *rActiveCharacterNameFont;
            SugarFont *rNewTurnFont;
            SugarFont *rAbilityDescriptionFont;
            SugarFont *rConfirmationFontBig;
            SugarFont *rConfirmationFontSmall;

            //--Ally Bar
            SugarBitmap *rAllyFrame;
            SugarBitmap *rAllyPortraitMask;

            //--Defeat Overlay
            SugarBitmap *rDefeatFrames[ADVCOMBAT_DEFEAT_FRAMES_TOTAL];

            //--Enemy Health Bar
            SugarBitmap *rEnemyHealthBarFill;
            SugarBitmap *rEnemyHealthBarFrame;
            SugarBitmap *rEnemyHealthBarUnder;

            //--Combat Inspector
            SugarBitmap *rInspectorCharacterBack;
            SugarBitmap *rInspectorEffectInset;
            SugarBitmap *rInspectorFrames;

            //--Player Interface
            SugarBitmap *rAbilityFrameMain;
            SugarBitmap *rAbilityHighlight;
            SugarBitmap *rDescriptionWindow;
            SugarBitmap *rMainHealthBarBack;
            SugarBitmap *rMainHealthBarFill;
            SugarBitmap *rMainHealthBarFrame;
            SugarBitmap *rMainNameBanner;
            SugarBitmap *rMainPortraitMask;
            SugarBitmap *rMainPortraitRing;
            SugarBitmap *rMainPortraitRingBack;

            //--Turn Order
            SugarBitmap *rTurnOrderBack;
            SugarBitmap *rTurnOrderCircle;
            SugarBitmap *rTurnOrderEdge;
        }Data;
    }Images;

    protected:

    public:
    ///--System
    AdvCombat();
    virtual ~AdvCombat();

    //--Public Variables
    //--Property Queries
    bool IsActive();
    bool IsStoppingWorldUpdate();
    bool DoesPlayerHaveInitiative();

    //--Manipulators
    void Activate();
    void Deactivate();
    void SetDefaultCombatMusic(const char *pMusicName);
    void SetUnloseable(bool pFlag);
    void SetUnwinnable(bool pFlag);
    void SetUnretreatable(bool pFlag);
    void SetUnsurrenderable(bool pFlag);

    //--Core Methods
    void HealFromDoctorBag(int pPartyIndex);

    ///--Animations
    ///--Confirmation
    void ActivateConfirmationWindow();
    void DeactivateConfirmationWindow();
    StarlightString *GetConfirmationString();
    void SetConfirmationText(const char *pString);
    void SetConfirmationFunc(ConfirmationFnPtr pFuncPtr);
    static void ConfirmExecuteAbility();
    void UpdateConfirmation();
    void RenderConfirmation();

    ///--Construction
    void Initialize();
    void Reinitialize();
    void Construct();
    void Disassemble();

    ///--Debug
    void Debug_RestoreParty();
    void Debug_AwardXP(int pAmount);
    void Debug_AwardJP(int pAmount);
    void Debug_AwardPlatina(int pAmount);
    void Debug_AwardCrafting();

    ///--Enemy Handlers
    int GetNextEnemyID();
    void RegisterEnemy(const char *pUniqueName, AdvCombatEntity *pEntity, int pReinforcementTurns);
    void ComputeEnemyPositions();

    ///--Introduction
    void UpdateIntroduction();
    void RenderIntroduction();

    ///--Options
    bool IsAutoUseDoctorBag();
    bool IsTouristMode();
    bool IsMemoryCursor();
    void SetAutoUseDoctorBag(bool pFlag);
    void SetTouristMode(bool pFlag);
    void SetMemoryCursor(bool pFlag);

    ///--Paths
    void SetStandardRetreatScript(const char *pPath);
    void SetStandardDefeatScript(const char *pPath);
    void SetStandardVictoryScript(const char *pPath);
    void SetRetreatScript(const char *pPath);
    void SetDefeatScript(const char *pPath);
    void SetVictoryScript(const char *pPath);

    ///--Player Input
    void MovePlayerHighlightTo(int pX, int pY);
    void HandlePlayerControls();
    void ExecuteAbility(AdvCombatAbility *pAbility, TargetCluster *pTargetCluster);
    void ExecuteActiveAbility();

    ///--Reinforcements
    void RegisterWorldReference(TilemapActor *pActor, int pReinforcementTurns);

    ///--Resolution
    void AwardXP(int pAmount);
    void AwardJP(int pAmount);
    void AwardPlatina(int pAmount);
    void BeginResolutionSequence(int pSequence);
    void UpdateResolution();
    void RenderResolution();
    void RenderVictory();
    void RenderDefeat();
    void RenderRetreat();

    ///--Roster Handling
    int GetRosterCount();
    int GetActivePartyCount();
    int GetCombatPartyCount();
    bool DoesPartyMemberExist(const char *pInternalName);
    void RegisterPartyMember(const char *pInternalName, AdvCombatEntity *pEntity);
    void PushPartyMember(const char *pInternalName);
    void SetPartyBySlot(int pSlot, const char *pPartyMemberName);
    void ClearParty();
    void FullRestoreParty();

    ///--Targeting
    //--System
    void ClearTargetClusters();

    //--Property Queries
    TargetCluster *GetTargetCluster(const char *pClusterName);
    AdvCombatEntity *GetEntityInCluster(const char *pClusterName, int pSlot);

    //--Manipulators
    TargetCluster *CreateTargetCluster(const char *pClusterName, const char *pDisplayName);
    void RegisterElementToCluster(const char *pClusterName, void *pElement);
    void RegisterEntityToClusterByID(const char *pClusterName, uint32_t pUniqueID);
    void RemoveEntityFromClusterI(const char *pClusterName, int pSlot);
    void RemoveEntityFromClusterP(const char *pClusterName, void *pPtr);

    //--Addition Macros
    void AddPartyTargetToClusterI(const char *pClusterName, int pSlot);
    void AddPartyTargetToClusterS(const char *pClusterName, const char *pPartyMemberName);
    void AddEnemyTargetToClusterI(const char *pClusterName, int pSlot);
    void AddEnemyTargetToClusterS(const char *pClusterName, const char *pEnemyName);

    //--Autobuild Macros
    void PopulateTargetsByCode(const char *pCodeName, void *pCaller);

    ///--Turn Order
    void GivePlayerInitiative();
    void RollTurnOrder();
    static int SortByInitiative(const void *pEntryA, const void *pEntryB);
    void RecomputeTurnBarLength();
    void BeginTurnDisplay();

    ///--World
    void PulseWorld(bool pDontCheckEnemies);

    private:
    //--Private Core Methods
    public:
    ///--Update
    void Update();

    //--File I/O
    void WriteLoadInfo(SugarAutoBuffer *pBuffer);
    void WriteGameInfo(SugarAutoBuffer *pBuffer);
    void ReadFromFile(VirtualFile *pInfile);

    ///--Drawing
    void Render();
    void RenderAllyBars();
    void RenderTurnOrder();
    void RenderPlayerBar();
    void RenderNewTurn();

    //--Pointer Routing
    AdvCombatEntity *GetRosterMemberI(int pIndex);
    AdvCombatEntity *GetRosterMemberS(const char *pName);
    AdvCombatEntity *GetActiveMemberI(int pIndex);
    AdvCombatEntity *GetActiveMemberS(const char *pName);
    AdvCombatEntity *GetCombatMemberI(int pIndex);
    AdvCombatEntity *GetCombatMemberS(const char *pName);
    AliasStorage *GetEnemyAliasStorage();
    AdvCombatEntity *GetActingEntity();
    AdvCombatAbility *GetSystemRetreat();
    AdvCombatAbility *GetSystemSurrender();

    //--Static Functions
    static AdvCombat *Fetch();

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_AdvCombat_GetProperty(lua_State *L);
int Hook_AdvCombat_SetProperty(lua_State *L);
