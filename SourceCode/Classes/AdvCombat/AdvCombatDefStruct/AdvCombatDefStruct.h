//--[Adventure Combat: Definitions and Structures]
//--Header file used for all structures and definitions in the Adventure Combat code.
#pragma once

//--[UI Positions]
//--These are a set of lookups for offsets on the UI. A character's portrait renders on the UI
//  with masks and offsets at various locations, these are them.
#define ACE_UI_INDEX_STATUS_MAIN 0
#define ACE_UI_INDEX_STATUS_LEFT 1
#define ACE_UI_INDEX_STATUS_RIGHT 2
#define ACE_UI_INDEX_COMBAT_ALLY 3
#define ACE_UI_INDEX_COMBAT_MAIN 4
#define ACE_UI_INDEX_BASEMENU_MAIN 5
#define ACE_UI_INDEX_DOCTOR_MAIN 6
#define ACE_UI_INDEX_VICTORY_MAIN 7
#define ACE_UI_INDEX_EQUIP_MAIN 8
#define ACE_UI_INDEX_EQUIP_LEFT 9
#define ACE_UI_INDEX_EQUIP_RIGHT 10
#define ACE_UI_INDEX_TOTAL 11

//--Stencil Codes
#define ACE_STENCIL_ACTIVE_CHARACTER_PORTRAIT 1
#define ACE_STENCIL_ALLY_PORTRAIT_START 2

//--[Ability Grid]
#define ACE_ABILITY_GRID_SIZE_X 10
#define ACE_ABILITY_GRID_SIZE_Y 3

//--[Ability Script Codes]
#define ACA_SCRIPT_CODE_CREATE 0
#define ACA_SCRIPT_CODE_ASSUMEJOB 1
#define ACA_SCRIPT_CODE_BEGINCOMBAT 2
#define ACA_SCRIPT_CODE_BEGINTURN 3
#define ACA_SCRIPT_CODE_BEGINACTION 4
#define ACA_SCRIPT_CODE_POSTFREEACTION 5
#define ACA_SCRIPT_CODE_POSTACTION 6
#define ACA_SCRIPT_CODE_PAINTTARGETS 7
#define ACA_SCRIPT_CODE_EXECUTE 8
#define ACA_SCRIPT_CODE_TURNENDS 9
#define ACA_SCRIPT_CODE_COMBATENDS 10

//--[Turn Order]
//--Amount of random scatter on initiative rolls.
#define ACE_TURN_ORDER_RANGE 40

//--Turn-order buckets
#define ACE_BUCKET_ALWAYS_STRIKES_LAST -2
#define ACE_BUCKET_SLOW -1
#define ACE_BUCKET_FAST 1
#define ACE_BUCKET_ALWAYS_STRIKES_FIRST 2

//--Turn-order bucket clamps
#define ACE_BUCKET_LOWEST 0
#define ACE_BUCKET_NEUTRAL 3
#define ACE_BUCKET_HIGHEST 6
#define ACE_BUCKET_TOTAL 7

//--Turn-order display ticks
#define ACE_TURN_TICKS_PER_LETTER 10
#define ACE_TURN_TICKS_HOLD 15
#define ACE_TURN_TICKS_FADE 15
#define ACE_TURN_DISPLAY_TICKS ((ACE_TURN_TICKS_PER_LETTER * 5) + ACE_TURN_TICKS_HOLD + ACE_TURN_TICKS_FADE)

//--[Combat Statistics]
//--List of statistics.
#define STATS_HPMAX 0
#define STATS_INITIATIVE 1
#define STATS_TOTAL 2

//--Represents a set of combat stats, like HP, MP, Attack Power, etc. These are used
//  to store and compute buffs and whatnot.
//--The stats can be accessed via a list array.
//--The functions are implemented in AdvCombatStats.cc
typedef struct CombatStatistics
{
    //--Members
    int mValueList[STATS_TOTAL];

    //--Functions
    void Initialize();
    void Zero();
    int GetStatByIndex(int pIndex);
    void SetStatByIndex(int pIndex, int pValue);
    void SetHPMax(int pValue);
    void SetInitiative(int pValue);
    void AddStatistics(CombatStatistics pPackage);
    void ClampStatistics();
}CombatStatistics;

//--[World Reference]
//--Package containing a TilemapActor and a number of turns.
typedef struct WorldRefPack
{
    int mTurns;
    TilemapActor *rActor;
    void Initialize()
    {
        mTurns = 0;
        rActor = NULL;
    }
}WorldRefPack;

//--[Equipment]
//--Maximum length of equipment type name. Common names are "Weapon A" and "Armor".
#define EQP_TYPE_MAX_LETTERS 32

//--[Target Cluster]
//--Represents a group of targets. Uses a SugarLinkedList * to store them. Implemented in
//  AdvCombatTargeting.cc.
typedef struct TargetCluster
{
    //--Storage.
    char *mDisplayName;
    SugarLinkedList *mrTargetList; //AdvCombatEntity *, ref

    //--Functions.
    void Initialize();
    static void DeleteThis(void *pPtr);
    void SetDisplayName(const char *pName);
    void RegisterElement(void *pElement);
    void RemoveElementI(int pSlot);
    void RemoveElementP(void *pPtr);
}TargetCluster;
