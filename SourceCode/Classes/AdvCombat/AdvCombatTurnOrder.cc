//--Base
#include "AdvCombat.h"

//--Classes
#include "AdvCombatEntity.h"

//--CoreClasses
#include "SugarLinkedList.h"

//--Definitions
#include "AdvCombatDefStruct.h"

//--Libraries
//--Managers

void AdvCombat::GivePlayerInitiative()
{

}
void AdvCombat::RollTurnOrder()
{
    //--[Documentation and Setup]
    //--Rolls turn order. All characters roll an initiative value based on their internal value plus rnd() % 40.
    //  Once rolling is done, characters are sorted into buckets based on Always-Strikes-First/Last and Fast/Slow flags.
    //  Then, those are dumped into the turn order and we're done.
    //--Ties are broken by character ID, with party members starting at zero and counting up, then enemies.
    mrTurnOrder->ClearList();

    //--[Initiative Rolling]
    //--Regardless of bucket position, everyone needs to roll. Note that entities who are stunned or otherwise not acting
    //  still roll, they just pass their turns. KO'd party members do not roll, when revived they wait until the next turn.
    int cTotalEntities = 0;
    AdvCombatEntity *rPartyMember = (AdvCombatEntity *)mrActiveParty->PushIterator();
    while(rPartyMember)
    {
        //--Ask if the member needs to roll.
        if(rPartyMember->CanActThisTurn())
        {
            int cInitiativeRoll = rPartyMember->GetStatistic(ADVCE_STATS_FINAL, STATS_INITIATIVE) + (rand() % ACE_TURN_ORDER_RANGE);
            rPartyMember->SetCurrentInitiative(cInitiativeRoll);
            rPartyMember->SetTurnID(cTotalEntities);
            cTotalEntities ++;
        }

        //--Next.
        rPartyMember = (AdvCombatEntity *)mrActiveParty->AutoIterate();
    }

    //--Same deal but for the baddies.
    AdvCombatEntity *rEnemyMember = (AdvCombatEntity *)mrEnemyCombatParty->PushIterator();
    while(rEnemyMember)
    {
        //--Enemies always roll since KO'd enemies leave the combat party.
        int cInitiativeRoll = rEnemyMember->GetStatistic(ADVCE_STATS_FINAL, STATS_INITIATIVE) + (rand() % ACE_TURN_ORDER_RANGE);
        rEnemyMember->SetCurrentInitiative(cInitiativeRoll);
        rEnemyMember->SetTurnID(cTotalEntities);
        cTotalEntities ++;

        //--Next.
        rEnemyMember = (AdvCombatEntity *)mrEnemyCombatParty->AutoIterate();
    }

    //--[Bucketing]
    //--We create a set of lists for each possibility. There are ACE_BUCKET_TOTAL lists and we hard clamp into them.
    //  Entities compute which one they should be in based on their turn-order flags.
    SugarLinkedList *tTurnLists[ACE_BUCKET_TOTAL];
    for(int i = 0; i < ACE_BUCKET_TOTAL; i ++) tTurnLists[i] = new SugarLinkedList(false);

    //--Run across the player's party and put them in the appropriate buckets.
    rPartyMember = (AdvCombatEntity *)mrActiveParty->PushIterator();
    while(rPartyMember)
    {
        //--Skip non-acting party members.
        if(rPartyMember->CanActThisTurn())
        {
            //--Compute, clamp.
            int tTurnBucket = rPartyMember->ComputeTurnBucket();
            if(tTurnBucket < ACE_BUCKET_LOWEST)  tTurnBucket = ACE_BUCKET_LOWEST;
            if(tTurnBucket > ACE_BUCKET_HIGHEST) tTurnBucket = ACE_BUCKET_HIGHEST;

            //--Place the entity in the bucket.
            tTurnLists[tTurnBucket]->AddElement("X", rPartyMember);
        }

        //--Next.
        rPartyMember = (AdvCombatEntity *)mrActiveParty->AutoIterate();
    }

    //--Same for the baddies.
    rEnemyMember = (AdvCombatEntity *)mrEnemyCombatParty->PushIterator();
    while(rEnemyMember)
    {
        //--Compute, clamp.
        int tTurnBucket = rEnemyMember->ComputeTurnBucket();
        if(tTurnBucket < ACE_BUCKET_LOWEST)  tTurnBucket = ACE_BUCKET_LOWEST;
        if(tTurnBucket > ACE_BUCKET_HIGHEST) tTurnBucket = ACE_BUCKET_HIGHEST;

        //--Place the entity in the bucket.
        tTurnLists[tTurnBucket]->AddElement("X", rEnemyMember);

        //--Next.
        rEnemyMember = (AdvCombatEntity *)mrEnemyCombatParty->AutoIterate();
    }

    //--[Sorting]
    //--All of the buckets now sort the highest initiative to the front.
    for(int i = 0; i < ACE_BUCKET_TOTAL; i ++)
    {
        tTurnLists[i]->SortListUsing(&AdvCombat::SortByInitiative);
    }

    //--[Turn Order]
    //--At last, we can now populate the turn order list.
    for(int i = 0; i < ACE_BUCKET_TOTAL; i ++)
    {
        AdvCombatEntity *rPtr = (AdvCombatEntity *)tTurnLists[i]->PushIterator();
        while(rPtr)
        {
            mrTurnOrder->AddElementAsTail(rPtr->GetName(), rPtr);
            rPtr = (AdvCombatEntity *)tTurnLists[i]->AutoIterate();
        }
    }

    //--[Clean]
    //--Deallocate.
    for(int i = 0; i < ACE_BUCKET_TOTAL; i ++)
    {
        delete tTurnLists[i];
    }

    //--[Debug]
    if(true)
    {
        //--Header.
        fprintf(stderr, "Turn order report:\n");

        //--List elements.
        int i = 0;
        AdvCombatEntity *rCheckPtr = (AdvCombatEntity *)mrTurnOrder->PushIterator();
        while(rCheckPtr)
        {
            fprintf(stderr, " %i: %s %p - %i %i\n", i, mrTurnOrder->GetIteratorName(), rCheckPtr, rCheckPtr->ComputeTurnBucket(), rCheckPtr->GetCurrentInitiative());
            i ++;
            rCheckPtr = (AdvCombatEntity *)mrTurnOrder->AutoIterate();
        }
    }
}
int AdvCombat::SortByInitiative(const void *pEntryA, const void *pEntryB)
{
    //--Compare the two entities by their initiative. If that fails, use their turn IDs.
    SugarLinkedListEntry **rEntryA = (SugarLinkedListEntry **)pEntryA;
    SugarLinkedListEntry **rEntryB = (SugarLinkedListEntry **)pEntryB;

    //--Cast the data pointer into an AdvCombatEntity.
    AdvCombatEntity *rEntityA = (AdvCombatEntity *)(*rEntryA)->rData;
    AdvCombatEntity *rEntityB = (AdvCombatEntity *)(*rEntryB)->rData;

    //--If the initiatives are identical, compare the turn IDs which cannot be identical.
    int cInitiativeA = rEntityA->GetCurrentInitiative();
    int cInitiativeB = rEntityB->GetCurrentInitiative();
    if(cInitiativeA == cInitiativeB)
    {
        return rEntityB->GetTurnID() - rEntityA->GetTurnID();
    }

    //--Otherwise, just return the comparison.
    return cInitiativeB - cInitiativeA;
}
void AdvCombat::RecomputeTurnBarLength()
{
    //--Whenever the turn list changes length, call this function to recompute its length and move the icons on it.
    float cTurnOrderWidPerPortrait = 48.0f;

    //--Reset timers.
    mTurnBarMoveTimer = 0;
    mTurnBarMoveTimerMax = 15;

    //--Set positions.
    mTurnWidStart = mTurnWidCurrent;
    mTurnWidTarget = (cTurnOrderWidPerPortrait * (mrTurnOrder->GetListSize())) + 10.0f;
}
void AdvCombat::BeginTurnDisplay()
{
    //--Causes the turn display text to appear on screen.
    mTurnDisplayTimer = 0;
}
