//--Base
#include "KPopDanceMove.h"

//--Classes
//--CoreClasses
//--Definitions
//--Libraries
#include "DataLibrary.h"

//--Managers

//=========================================== System ==============================================
KPopDanceMove::KPopDanceMove()
{
    //--[KPopDanceMove]
    //--System
    mTotalTicks = 0;

    //--Images
    mTotalFrames = 0;
    mTicksPerFrame = NULL;
    rImages = NULL;
}
KPopDanceMove::~KPopDanceMove()
{
    free(mTicksPerFrame);
    free(rImages);
}
void KPopDanceMove::DeleteThis(void *pPtr)
{
    KPopDanceMove *rPtr = (KPopDanceMove *)pPtr;
    delete rPtr;
}

//====================================== Property Queries =========================================
int KPopDanceMove::GetFrameTicks(int pIndex)
{
    if(pIndex < 0 || pIndex >= mTotalFrames) return -1;
    return mTicksPerFrame[pIndex];
}
SugarBitmap *KPopDanceMove::GetImage(int pIndex)
{
    if(pIndex < 0 || pIndex >= mTotalFrames) return NULL;
    return rImages[pIndex];
}

//========================================= Manipulators ==========================================
void KPopDanceMove::AllocateFrames(int pTotalFrames)
{
    //--Deallocate.
    free(mTicksPerFrame);
    free(rImages);

    //--Reset.
    mTotalFrames = 0;
    mTicksPerFrame = NULL;
    rImages = NULL;
    if(pTotalFrames < 1) return;

    //--Allocate.
    mTotalFrames = pTotalFrames;
    SetMemoryData(__FILE__, __LINE__);
    mTicksPerFrame = (int *)         starmemoryalloc(sizeof(int)           * mTotalFrames);
    rImages        = (SugarBitmap **)starmemoryalloc(sizeof(SugarBitmap *) * mTotalFrames);

    //--Clear.
    memset(mTicksPerFrame, 0, sizeof(int)           * mTotalFrames);
    memset(rImages,        0, sizeof(SugarBitmap *) * mTotalFrames);
}
void KPopDanceMove::SetFrame(int pIndex, const char *pDLPath, int pTicksToPlay)
{
    if(pIndex < 0 || pIndex >= mTotalFrames || !pDLPath || pTicksToPlay < 1) return;
    mTicksPerFrame[pIndex] = pTicksToPlay;
    rImages[pIndex] = (SugarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath);
}

//========================================= Core Methods ==========================================
void KPopDanceMove::ComputeTotalFrames()
{
    mTotalTicks = 0;
    for(int i = 0; i < mTotalFrames; i ++)
    {
        mTotalTicks += mTicksPerFrame[i];
    }
}
