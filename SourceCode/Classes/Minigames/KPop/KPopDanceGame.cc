//--Base
#include "KPopDanceGame.h"

//--Classes
#include "AdventureLevel.h"
#include "KPopDancer.h"
#include "KPopDanceMove.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarFont.h"
#include "SugarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "EasingFunctions.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "AudioPackage.h"
#include "ControlManager.h"
#include "DebugManager.h"
#include "DisplayManager.h"

//--[Debug]
//#define KPOPDANCE_DEBUG
#ifdef KPOPDANCE_DEBUG
    #define VERIFY_DEFAULT true
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define VERIFY_DEFAULT false
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

//=========================================== System ==============================================
KPopDanceGame::KPopDanceGame()
{
    //--[RootObject]
    //--System
    mType = POINTER_TYPE_KPOPDANCEGAME;

    //--[KPopDanceGame]
    //--System
    mIsActive = false;
    mStartedMusic = false;
    mVisibilityTimer = 0;

    //--Combos and Score
    mComboCount = 0;
    mPerfectStreak = 0;
    mScore = 0;

    //--Score Spawn Positions
    memset(mScoreSpawns,   0, sizeof(Point2DF) * KPOP_SCORE_POS);
    memset(mScoreOccupied, 0, sizeof(bool)     * KPOP_SCORE_POS);

    //--Dance Instructions
    mBeatTimer = 0;
    mBeatScrollRate = 1366.0f / 360.0f;
    mInstructionList = new SugarLinkedList(true);

    //--Results
    mResultList = new SugarLinkedList(true);

    //--Dancers
    mDancerList = new SugarLinkedList(true);

    //--System Output
    mIsSystemOutputMode = false;
    fOutputFile = NULL;

    //--Images
    memset(&Images, 0, sizeof(Images));

    //--[Construction]
    //--Fast Access Pointers
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    bool tVerifyFlag = VERIFY_DEFAULT || DebugManager::GetDebugFlag("KPopDance");
    if(tVerifyFlag) DebugManager::ForcePrint("== KPopDanceGame - Getting Asset References ==\n");

    //--Fonts
    Images.Data.rScoreFont = rDataLibrary->GetFont("Dance Score");

    //--Arrows.
    Images.Data.rArrows[KPOP_ARROW_NON] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/KPopSprites/UI/ArrowNon");
    Images.Data.rArrows[KPOP_ARROW_TOP] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/KPopSprites/UI/ArrowTop");
    Images.Data.rArrows[KPOP_ARROW_RGT] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/KPopSprites/UI/ArrowRgt");
    Images.Data.rArrows[KPOP_ARROW_BOT] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/KPopSprites/UI/ArrowBot");
    Images.Data.rArrows[KPOP_ARROW_LFT] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/KPopSprites/UI/ArrowLft");
    Images.Data.rArrows[KPOP_ARROW_ACT] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/KPopSprites/UI/ArrowAct");
    Images.Data.rArrows[KPOP_ARROW_CAN] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/KPopSprites/UI/ArrowCan");

    //--Display
    Images.Data.rInputBox = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/KPopSprites/UI/ArrowBox");

    //--Popups
    Images.Data.rPopupEncourage[0] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/KPopSprites/UI/Encourage0");
    Images.Data.rPopupEncourage[1] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/KPopSprites/UI/Encourage1");
    Images.Data.rPopupEncourage[2] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/KPopSprites/UI/Encourage2");
    Images.Data.rPopupLate         = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/KPopSprites/UI/DiscLate");
    Images.Data.rPopupEarly        = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/KPopSprites/UI/DiscEarly");
    Images.Data.rPopupWrong        = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/KPopSprites/UI/DiscWrong");

    //--Verify.
    Images.mIsReady = VerifyStructure(&Images.Data, sizeof(Images.Data), sizeof(void *), tVerifyFlag);
    if(tVerifyFlag) DebugManager::ForcePrint("== KPopDanceGame - Finished Asset References ==\n");

    //--[Score Positions]
    //--Set positions around the player.
    float cX = VIRTUAL_CANVAS_X * 0.50f;
    float cY = KPOP_DANCER_Y;
    float cRadius = 150.0f;

    //--Positions "Alternate" between sides. This makes the spawning routine space them
    //  out a bit more even though it uses adjacency in array positions.
    float cPosHi =  22.5f;
    float cPosLo =   7.5f;
    float cBase  =   0.0f;
    float cHalf  = 180.0f;
    float cAngles[] = {cBase + -cPosLo, cHalf + cPosLo, cBase + cPosHi, cHalf + -cPosHi, cBase + -cPosHi, cHalf + cPosHi, cBase + cPosLo, cHalf + -cPosLo};
    for(int i = 0; i < KPOP_SCORE_POS; i ++)
    {
        mScoreSpawns[i].mX = cX + (cosf(cAngles[i] * TORADIAN) * cRadius);
        mScoreSpawns[i].mY = cY + (sinf(cAngles[i] * TORADIAN) * cRadius);
    }
}
KPopDanceGame::~KPopDanceGame()
{
    delete mInstructionList;
    delete mResultList;
    delete mDancerList;
    fclose(fOutputFile);
}

//====================================== Property Queries =========================================
bool KPopDanceGame::IsActive()
{
    return mIsActive;
}
bool KPopDanceGame::IsVisible()
{
    return (mVisibilityTimer > 0);
}

//========================================= Manipulators ==========================================
void KPopDanceGame::SetActive(bool pFlag)
{
    mIsActive = pFlag;
}
void KPopDanceGame::RegisterInstruction(int pTimer, int pDirection)
{
    if(pDirection < 0 || pDirection >= KPOP_ARROW_TOTAL) return;
    SetMemoryData(__FILE__, __LINE__);
    KPopInstruction *nInstruction = (KPopInstruction *)starmemoryalloc(sizeof(KPopInstruction));
    nInstruction->Initialize();
    nInstruction->mTimer = pTimer;
    nInstruction->mDirection = pDirection;
    nInstruction->rArrowImg = Images.Data.rArrows[pDirection];
    mInstructionList->AddElement("X", nInstruction, &FreeThis);
}
KPopResultPack *KPopDanceGame::RegisterResult(float pX, float pY, SugarBitmap *pImage)
{
    if(!pImage) return NULL;
    SetMemoryData(__FILE__, __LINE__);
    KPopResultPack *nPack = (KPopResultPack *)starmemoryalloc(sizeof(KPopResultPack));
    nPack->Initialize();
    nPack->mX = pX;
    nPack->mY = pY;
    nPack->rImage = pImage;
    mResultList->AddElement("X", nPack, &FreeThis);
    return nPack;
}
KPopResultPack *KPopDanceGame::RegisterResult(float pX, float pY, SugarFont *pFont, const char *pText)
{
    if(!pFont || !pText) return NULL;
    SetMemoryData(__FILE__, __LINE__);
    KPopResultPack *nPack = (KPopResultPack *)starmemoryalloc(sizeof(KPopResultPack));
    nPack->Initialize();
    nPack->mX = pX;
    nPack->mY = pY;
    nPack->rFont = pFont;
    strncpy(nPack->mBuffer, pText, sizeof(nPack->mBuffer));
    mResultList->AddElement("X", nPack, &FreeThis);
    return nPack;
}
void KPopDanceGame::ActivateSystemOutput(const char *pFilePath)
{
    //--Clear.
    mIsSystemOutputMode = false;
    fclose(fOutputFile);
    fOutputFile = NULL;
    if(!pFilePath) return;

    //--Open. If the file could not open, defaults to not being in output mode.
    fOutputFile = fopen(pFilePath, "w");
    mIsSystemOutputMode = (fOutputFile);
}

//========================================= Core Methods ==========================================
void KPopDanceGame::CreateDancer(const char *pDancerName)
{
    //--Error check.
    if(!pDancerName) return;

    //--If the dancer already exists, do nothing.
    void *rCheckPtr = mDancerList->GetElementByName(pDancerName);
    if(rCheckPtr) return;

    //--Create.
    KPopDancer *nDancer = new KPopDancer();
    mDancerList->AddElement(pDancerName, nDancer, &RootObject::DeleteThis);
}
void KPopDanceGame::CreateDanceMove(const char *pDancerName, const char *pMoveName)
{
    //--Error check.
    if(!pDancerName || !pMoveName) return;

    //--Get the Dancer.
    KPopDancer *rDancer = (KPopDancer *)mDancerList->GetElementByName(pDancerName);
    if(!rDancer) return;

    //--Create.
    KPopDanceMove *nMove = new KPopDanceMove();

    //--Set.
    rDancer->RegisterMove(pMoveName, nMove);
}
void KPopDanceGame::AllocateDanceMoveFrames(const char *pDancerName, const char *pMoveName, int pFramesTotal)
{
    //--Error check.
    if(!pDancerName || !pMoveName) return;

    //--Get the Dancer.
    KPopDancer *rDancer = (KPopDancer *)mDancerList->GetElementByName(pDancerName);
    if(!rDancer) return;

    //--Create.
    KPopDanceMove *rDanceMove = rDancer->GetDanceMove(pMoveName);
    if(!rDanceMove) return;

    //--Set.
    rDanceMove->AllocateFrames(pFramesTotal);
}
void KPopDanceGame::SetDanceMoveFrame(const char *pDancerName, const char *pMoveName, int pFrame, int pTicks, const char *pImgPath)
{
    //--Error check.
    if(!pDancerName || !pMoveName) return;

    //--Get the Dancer.
    KPopDancer *rDancer = (KPopDancer *)mDancerList->GetElementByName(pDancerName);
    if(!rDancer) return;

    //--Create.
    KPopDanceMove *rDanceMove = rDancer->GetDanceMove(pMoveName);
    if(!rDanceMove) return;

    //--Set.
    rDanceMove->SetFrame(pFrame, pImgPath, pTicks);
}
void KPopDanceGame::PositionDancers()
{
    //--Once all dancers are registered, position them.
    KPopDancer *rDancer = (KPopDancer *)mDancerList->PushIterator();
    while(rDancer)
    {
        rDancer->MoveTo(VIRTUAL_CANVAS_X * 0.50f, KPOP_DANCER_Y, -1);
        rDancer = (KPopDancer *)mDancerList->AutoIterate();
    }
}
void KPopDanceGame::GainScore(int pFlag)
{
    //--Scores points based on the incoming flag, which is from KPOP_ENCOURAGE_GOOD to KPOP_ENCOURAGE_PERFECT.
    //  If the player missed a keypress, -1 is passed in.
    if(pFlag < -1 || pFlag >= KPOP_ENCOURAGE_TOTAL) return;

    //--Handle a "Miss". This resets combo counters.
    if(pFlag == -1)
    {
        AudioManager::Fetch()->PlaySound("Dance|Fail");
        mComboCount = 0;
        mPerfectStreak = 0;
        return;
    }

    //--Past this point, the player scored a "Hit". Get the base score.
    int tScoreGain = 100;
    if(pFlag == KPOP_ENCOURAGE_GREAT) tScoreGain = 200;
    if(pFlag == KPOP_ENCOURAGE_PERFECT) tScoreGain = 500;

    //--The score gains +200 points for every hit in a "combo". Misses reset the combo.
    mComboCount ++;
    tScoreGain = tScoreGain + ((mComboCount-1) * 200);

    //--Sound effect is based on the number of combos in a row.
    if(mComboCount == 1) AudioManager::Fetch()->PlaySound("Dance|Bell0");
    else if(mComboCount == 2) AudioManager::Fetch()->PlaySound("Dance|Bell1");
    else if(mComboCount == 3) AudioManager::Fetch()->PlaySound("Dance|Bell2");
    else AudioManager::Fetch()->PlaySound("Dance|Bell3");

    //--If this is a perfect, an extra 300 points are gained per perfect in a row.
    if(pFlag == KPOP_ENCOURAGE_PERFECT)
    {
        mPerfectStreak ++;
        tScoreGain = tScoreGain + ((mPerfectStreak-1) * 300);
    }
    //--If not a perfect, reset the perfect counter.
    else
    {
        mPerfectStreak = 0;
    }

    //--Score has been computed. Add it to the player's score.
    mScore += tScoreGain;

    //--Create a text package with the score in it. It spawns in one of several positions near the player.
    //  First, roll a random position.
    int tSpawnSlot = rand() % KPOP_SCORE_POS;
    int tOrigSlot = tSpawnSlot;

    //--As long as the slot is occupied, increment. If we get back to the original, there are no
    //  open slots, so fail.
    while(mScoreOccupied[tSpawnSlot])
    {
        //--Try the next slot up. This loops back to zero if it exceeds the maximum.
        tSpawnSlot = (tSpawnSlot + 1) % KPOP_SCORE_POS;
        if(tSpawnSlot == tOrigSlot)
        {
            tSpawnSlot = -1;
            break;
        }
    }

    //--If the slot came back as -1, there were no spaces open. Fail.
    if(tSpawnSlot == -1)
    {
        return;
    }

    //--Otherwise, spawn the score!
    char tBuffer[32];
    sprintf(tBuffer, "%i", tScoreGain);
    KPopResultPack *rResultPack = RegisterResult(mScoreSpawns[tSpawnSlot].mX, mScoreSpawns[tSpawnSlot].mY, Images.Data.rScoreFont, tBuffer);

    //--Mark the slot as in use.
    if(rResultPack)
    {
        mScoreOccupied[tSpawnSlot] = true;
        rResultPack->mScoreSlot = tSpawnSlot;
    }
}

//===================================== Private Core Methods ======================================
//============================================ Update =============================================
void KPopDanceGame::Update()
{
    //--[Visibility]
    //--If active, increment the timer.
    if(mIsActive)
    {
        if(mVisibilityTimer < KPOP_VIS_TICKS) mVisibilityTimer ++;
    }
    //--If not active, decrement the timer. Stop the update after other timers have run.
    else
    {
        if(mVisibilityTimer > 0) mVisibilityTimer --;
    }

    //--[Beats]
    //--Start the music if it's not playing yet!
    if(!mStartedMusic)
    {
        mStartedMusic = true;
        AudioManager::Fetch()->PlayMusic("DanceMusic");
    }

    //--Increment the beat timer.
    mBeatTimer ++;

    //--Audio Sync. Check if the music package is not synced with the beat timer. If not, re-sync it.
    AudioPackage *rMusicPack = AudioManager::Fetch()->GetPlayingMusic();
    if(rMusicPack)
    {
        //--Compute.
        float cSecondsExpected = mBeatTimer * TICKSTOSECONDS;
        float cSecondsActual = rMusicPack->GetPosition();

        //--The allowable different is 1 tick, or 0.016667 ticks. We put a small buffer in to make sure we don't constantly resync.
        float cRange = (1.0f / 60.0f) + (0.100000f);
        if(fabsf(cSecondsActual - cSecondsExpected) >= cRange)
        {
            rMusicPack->SeekTo(cSecondsExpected);
        }
    }

    //--Run across all the beats and update the timers of the beats that have been hit.
    KPopInstruction *rInstruction = (KPopInstruction *)mInstructionList->PushIterator();
    while(rInstruction)
    {
        if(rInstruction->mDone) rInstruction->mFadeoutTimer ++;

        //--Next.
        rInstruction = (KPopInstruction *)mInstructionList->AutoIterate();
    }

    //--[Dancers]
    //--Order each dancing character to update.
    KPopDancer *rDancer = (KPopDancer *)mDancerList->PushIterator();
    while(rDancer)
    {
        rDancer->Update();
        rDancer = (KPopDancer *)mDancerList->AutoIterate();
    }

    //--[Results]
    KPopResultPack *rResultPack = (KPopResultPack *)mResultList->SetToHeadAndReturn();
    while(rResultPack)
    {
        //--Update.
        rResultPack->mTimer ++;
        if(rResultPack->mTimer >= KPOP_RESULT_TICKS)
        {
            if(rResultPack->mScoreSlot != -1)
            {
                mScoreOccupied[rResultPack->mScoreSlot] = false;
            }
            mResultList->RemoveRandomPointerEntry();
        }

        //--Next.
        rResultPack = (KPopResultPack *)mResultList->IncrementAndGetRandomPointerEntry();
    }

    //--[Stop Update]
    //--If not the active object, stop the update here.
    if(!mIsActive) return;

    //--[Player Controls]
    //--Fast-access pointers.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Escape, quits the minigame.
    if(rControlManager->IsFirstPress("Escape"))
    {
        SetActive(false);
        return;
    }

    //--[Dancer Controls]
    //--Get the zeroth dancer. This is who the player controls.
    KPopDancer *rPlayerDancer = (KPopDancer *)mDancerList->GetElementBySlot(0);
    if(!rPlayerDancer) return;

    //--Keypresses.
    int tPlayerMove = KPOP_ARROW_NON;
    bool tIsLftPress = rControlManager->IsFirstPress("Left");
    bool tIsRgtPress = rControlManager->IsFirstPress("Right");
    bool tIsTopPress = rControlManager->IsFirstPress("Up");
    bool tIsBotPress = rControlManager->IsFirstPress("Down");
    bool tIsActPress = rControlManager->IsFirstPress("Activate");
    bool tIsCanPress = rControlManager->IsFirstPress("Cancel");

    //--Arrow Keys. Causes the player to do an animation.
    if(tIsLftPress)
    {
        tPlayerMove = KPOP_ARROW_LFT;
        rPlayerDancer->SetActiveMove("Arm Left");
    }
    else if(tIsRgtPress)
    {
        tPlayerMove = KPOP_ARROW_RGT;
        rPlayerDancer->SetActiveMove("Arm Right");
    }
    else if(tIsTopPress)
    {
        tPlayerMove = KPOP_ARROW_TOP;
        rPlayerDancer->SetActiveMove("Leg Left");
    }
    else if(tIsBotPress)
    {
        tPlayerMove = KPOP_ARROW_BOT;
        rPlayerDancer->SetActiveMove("Leg Right");
    }
    else if(tIsActPress)
    {
        tPlayerMove = KPOP_ARROW_ACT;
        rPlayerDancer->SetActiveMove("Head Roll");
    }
    else if(tIsCanPress)
    {
        tPlayerMove = KPOP_ARROW_CAN;
        rPlayerDancer->SetActiveMove("Hip Thrust");
    }

    //--Player did not press a key. Stop here.
    if(tPlayerMove == KPOP_ARROW_NON) return;

    //--In system output mode, write the button pressed and the time to a file. This allows us to dynamically
    //  create timing files!
    if(mIsSystemOutputMode && fOutputFile)
    {
        //--The format is "KPop_SetProperty("Register Action", iTickPos, iRoll)\n")
        //  For human-readability, we output in a seconds-based format.
        float cSeconds = mBeatTimer * TICKSTOSECONDS;
        fprintf(fOutputFile, "KPop_SetProperty(\"Register Action\", %f * 60.0, %i)\n", cSeconds, tPlayerMove);
    }

    //--Iterate across the instructions and find the one nearest to zero.
    KPopInstruction *rUseInstruction = (KPopInstruction *)mInstructionList->PushIterator();
    while(rUseInstruction)
    {
        //--Calculate.
        int tActiveTime = rUseInstruction->mTimer - mBeatTimer;

        //--If the beat has already been hit, ignore it. When the fadeout timer hits its clamp,
        //  it will remove itself during the next rendering cycle.
        if(rUseInstruction->mDone)
        {
        }
        //--If it's less than negative KPOP_TICKS_RANGE_HALFBEAT, ignore it.
        else if(tActiveTime < -KPOP_TICKS_RANGE_HALFBEAT)
        {
        }
        //--If it's more than positive KPOP_TICKS_RANGE_HALFBEAT, ignore it.
        else if(tActiveTime > KPOP_TICKS_RANGE_HALFBEAT)
        {
        }
        //--Otherwise, we've found our beat.
        else
        {
            mInstructionList->PopIterator();
            break;
        }

        //--Next.
        rUseInstruction = (KPopInstruction *)mInstructionList->AutoIterate();
    }

    //--If the instruction is NULL, we ran out of instance.
    if(!rUseInstruction) return;

    //--[Result Spawning]
    //--Position of the beat results.
    float cXPos = VIRTUAL_CANVAS_X * 0.50f;
    float cYPos = KPOP_BEAT_CENTER_Y - 25.0f;

    //--Flag the beat as "Done".
    rUseInstruction->mDone = true;

    //--Wrong instruction!
    if(rUseInstruction->mDirection != tPlayerMove)
    {
        GainScore(-1);
        RegisterResult(cXPos, cYPos, Images.Data.rPopupWrong);
        return;
    }

    //--Check if the instruction was the right one, and how early it was.
    int tHitWindow = rUseInstruction->mTimer - mBeatTimer;

    //--Early!
    if(tHitWindow < -KPOP_TICKS_RANGE_FINE)
    {
        GainScore(-1);
        RegisterResult(cXPos, cYPos, Images.Data.rPopupEarly);
    }
    //--Good!
    else if(tHitWindow < -KPOP_TICKS_RANGE_GREAT)
    {
        GainScore(KPOP_ENCOURAGE_GOOD);
        rPlayerDancer->SetSizeBoost(KPOP_JUICE_SIZE_FINE);
        RegisterResult(cXPos, cYPos, Images.Data.rPopupEncourage[KPOP_ENCOURAGE_GOOD]);
    }
    //--Great!
    else if(tHitWindow < -KPOP_TICKS_RANGE_PERFECT)
    {
        GainScore(KPOP_ENCOURAGE_GREAT);
        rPlayerDancer->SetSizeBoost(KPOP_JUICE_SIZE_GREAT);
        RegisterResult(cXPos, cYPos, Images.Data.rPopupEncourage[KPOP_ENCOURAGE_GREAT]);
    }
    //--Perfect! Switches to positive ticks here.
    else if(tHitWindow < KPOP_TICKS_RANGE_PERFECT)
    {
        GainScore(KPOP_ENCOURAGE_PERFECT);
        rPlayerDancer->SetSizeBoost(KPOP_JUICE_SIZE_PERFECT);
        RegisterResult(cXPos, cYPos, Images.Data.rPopupEncourage[KPOP_ENCOURAGE_PERFECT]);
    }
    //--Great!
    else if(tHitWindow < KPOP_TICKS_RANGE_GREAT)
    {
        GainScore(KPOP_ENCOURAGE_GREAT);
        rPlayerDancer->SetSizeBoost(KPOP_JUICE_SIZE_GREAT);
        RegisterResult(cXPos, cYPos, Images.Data.rPopupEncourage[KPOP_ENCOURAGE_GREAT]);
    }
    //--Good!
    else if(tHitWindow < KPOP_TICKS_RANGE_FINE)
    {
        GainScore(KPOP_ENCOURAGE_GOOD);
        rPlayerDancer->SetSizeBoost(KPOP_JUICE_SIZE_FINE);
        RegisterResult(cXPos, cYPos, Images.Data.rPopupEncourage[KPOP_ENCOURAGE_GOOD]);
    }
    //--Late!
    else
    {
        GainScore(-1);
        RegisterResult(cXPos, cYPos, Images.Data.rPopupLate);
    }
}

//=========================================== File I/O ============================================
//=========================================== Drawing =============================================
void KPopDanceGame::Render()
{
    if(!Images.mIsReady) return;

    //--[Visibility]
    //--Compute the alpha we should render with.
    float cAlpha = EasingFunction::QuadraticInOut(mVisibilityTimer, KPOP_VIS_TICKS);
    if(cAlpha <= 0.0f) return;

    //--Backing
    SugarBitmap::DrawFullBlack(cAlpha * 0.90f);

    //--Set the mixer.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha);

    //--[Dancers]
    //--Render all the dancers.
    KPopDancer *rDancer = (KPopDancer *)mDancerList->PushIterator();
    while(rDancer)
    {
        rDancer->Render();
        rDancer = (KPopDancer *)mDancerList->AutoIterate();
    }

    //--[Beat Box]
    //--Renders before the beats do.
    float cXCenter = VIRTUAL_CANVAS_X * 0.50f;
    Images.Data.rInputBox->Draw(cXCenter - (Images.Data.rInputBox->GetTrueWidth() * 0.50f), KPOP_BEAT_CENTER_Y - (Images.Data.rInputBox->GetTrueHeight() * 0.50f));

    //--[Beats]
    //--Represents the buttons the player needs to press.
    KPopInstruction *rInstruction = (KPopInstruction *)mInstructionList->SetToHeadAndReturn();
    while(rInstruction)
    {
        //--Skip invalid instructions.
        if(!rInstruction->rArrowImg)
        {
            rInstruction = (KPopInstruction *)mInstructionList->IncrementAndGetRandomPointerEntry();
            continue;
        }

        //--Compute position.
        int tUseTimer = (rInstruction->mTimer - mBeatTimer);
        float cXPos = cXCenter + (tUseTimer * mBeatScrollRate);
        float cYPos = KPOP_BEAT_CENTER_Y;
        float cScale = 1.0f;
        float cScaleInv = 1.0f / cScale;

        //--If the beat is "Active" then it needs to increase in size and fade out.
        if(rInstruction->mDone)
        {
            //--Compute percent. When the beat is invisible, we can remove it.
            float cPercent = EasingFunction::QuadraticOut(rInstruction->mFadeoutTimer, KPOP_BEAT_FADE_TICKS);
            if(cPercent >= 1.0f)
            {
                mInstructionList->RemoveRandomPointerEntry();
                rInstruction = (KPopInstruction *)mInstructionList->IncrementAndGetRandomPointerEntry();
                continue;
            }

            //--Otherwise, set the scale.
            cScale = 1.0f + (KPOP_BEAT_FADE_SIZE_BOOST * cPercent);
            if(cScale <= 0.0f) cScale = 1.0f;
            cScaleInv = 1.0f / cScale;

            //--The X position locks to the position it was at when it was hit.
            cXPos = rInstruction->mDoneXPos;

            //--Set color.
            StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, (1.0f - cPercent) * cAlpha);
        }
        else
        {
            StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, 1.0f * cAlpha);
        }

        //--Offscreen to the left? Remove it!
        if(cXPos < -100.0f)
        {
            mInstructionList->RemoveRandomPointerEntry();
            rInstruction = (KPopInstruction *)mInstructionList->IncrementAndGetRandomPointerEntry();
            continue;
        }
        //--Offscreen to the right? We're done iterating.
        else if(cXPos >= VIRTUAL_CANVAS_X + 30.0f)
        {
            break;
        }
        //--Otherwise, render it.
        else if(rInstruction->rArrowImg)
        {
            //--Render.
            glTranslatef(cXPos, cYPos, 0.0f);
            glScalef(cScale, cScale, 1.0f);
            rInstruction->rArrowImg->Draw((rInstruction->rArrowImg->GetTrueWidth() * -0.50f), (rInstruction->rArrowImg->GetTrueHeight() * -0.50f));
            glScalef(cScaleInv, cScaleInv, 1.0f);
            glTranslatef(-cXPos, -cYPos, 0.0f);

            //--Store the last position we rendered at.
            rInstruction->mDoneXPos = cXPos;
        }

        //--Next.
        rInstruction = (KPopInstruction *)mInstructionList->IncrementAndGetRandomPointerEntry();
    }

    //--Clean.
    StarlightColor::ClearMixer();

    //--[Encouragement/You Failed]
    //--Compute size.
    float cScale = 3.0f;
    float cScaleInv = 1.0f / cScale;

    //--Render.
    KPopResultPack *rResultPack = (KPopResultPack *)mResultList->PushIterator();
    while(rResultPack)
    {
        //--Skip invalid packs.
        if(!rResultPack->rImage && !rResultPack->rFont)
        {
            rResultPack = (KPopResultPack *)mResultList->AutoIterate();
            continue;
        }

        //--Vis Percent.
        float cPercent = EasingFunction::QuadraticOut(rResultPack->mTimer, KPOP_RESULT_TICKS);
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, 1.0f - cPercent);

        //--Compute position.
        float cXPos = rResultPack->mX;
        float cYPos = rResultPack->mY - (30.0f * cPercent);

        //--Position.
        glTranslatef(cXPos, cYPos, 0.0f);
        glScalef(cScale, cScale, 1.0f);

        //--Image pack:
        if(rResultPack->rImage)
        {
            rResultPack->rImage->Draw(rResultPack->rImage->GetTrueWidth() * -0.50f, rResultPack->rImage->GetTrueHeight() * -0.50f);
        }
        //--Text pack:
        else if(rResultPack->rFont)
        {
            rResultPack->rFont->DrawText(0.0f, 0.0f, SUGARFONT_AUTOCENTER_XY, 1.0f, rResultPack->mBuffer);
        }

        //--Clean.
        glScalef(cScaleInv, cScaleInv, 1.0f);
        glTranslatef(-cXPos, -cYPos, 0.0f);

        //--Next.
        rResultPack = (KPopResultPack *)mResultList->AutoIterate();
    }

    //--Clean
    StarlightColor::ClearMixer();

    //--[Score]
    //--Renders the player's total score.
    Images.Data.rScoreFont->DrawText    (VIRTUAL_CANVAS_X * 0.50f, VIRTUAL_CANVAS_Y * 0.12f, SUGARFONT_AUTOCENTER_X, 3.0f, "SCORE");
    Images.Data.rScoreFont->DrawTextArgs(VIRTUAL_CANVAS_X * 0.50f, VIRTUAL_CANVAS_Y * 0.18f, SUGARFONT_AUTOCENTER_X, 3.0f, "%i", mScore);
}

//======================================= Pointer Routing =========================================
//====================================== Static Functions =========================================
KPopDanceGame *KPopDanceGame::Fetch()
{
    AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
    if(!rActiveLevel) return NULL;
    return rActiveLevel->GetDanceMinigame();
}

//========================================= Lua Hooking ===========================================
void KPopDanceGame::HookToLuaState(lua_State *pLuaState)
{
    /* KPop_GetProperty("Dummy") (1 Integer)
       Returns the requested property from the KPop Minigame. */
    lua_register(pLuaState, "KPop_GetProperty", &Hook_KPop_GetProperty);

    /* KPop_SetProperty("Set Active", bIsActive)
       KPop_SetProperty("Register Dancer", sDancerName)
       KPop_SetProperty("Register Dance Move", sDancerName, sMoveName)
       KPop_SetProperty("Set Dancer Move Frames", sDancerName, sMoveName, iFramesTotal)
       KPop_SetProperty("Set Dancer Move Frame", sDancerName, sMoveName, iFrame, iTicks, sImgPath)
       KPop_SetProperty("Activate System Output", sFilePath)
       Sets the requested property in the KPop Minigame. */
    lua_register(pLuaState, "KPop_SetProperty", &Hook_KPop_SetProperty);
}
//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
int Hook_KPop_GetProperty(lua_State *L)
{
    //KPop_GetProperty("Dummy") (1 Integer)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("KPop_GetProperty");

    //--Switching.
    int tReturns = 0;
    const char *rSwitchType = lua_tostring(L, 1);

    //--Active object.
    KPopDanceGame *rDanceGame = KPopDanceGame::Fetch();
    if(!rDanceGame) return LuaTypeError("KPop_GetProperty");

    //--Name of the current level.
    if(!strcasecmp(rSwitchType, "Dummy") && tArgs == 1)
    {
        lua_pushstring(L, "Dogs");
        tReturns = 1;
    }
    //--Error.
    else
    {
        LuaPropertyError("KPop_GetProperty", rSwitchType, tArgs);
    }

    //--Finish up.
    return tReturns;
}
int Hook_KPop_SetProperty(lua_State *L)
{
    //KPop_SetProperty("Set Active", bIsActive)
    //KPop_SetProperty("Register Dancer", sDancerName)
    //KPop_SetProperty("Register Dance Move", sDancerName, sMoveName)
    //KPop_SetProperty("Set Dancer Move Frames", sDancerName, sMoveName, iFramesTotal)
    //KPop_SetProperty("Set Dancer Move Frame", sDancerName, sMoveName, iFrame, iTicks, sImgPath)
    //KPop_SetProperty("Position Dancers")
    //KPop_SetProperty("Register Action", iTimer, iDirection)
    //KPop_SetProperty("Activate System Output", sFilePath)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("KPop_SetProperty");

    //--Switching.
    const char *rSwitchType = lua_tostring(L, 1);

    //--[Dynamic]
    //--Active object.
    KPopDanceGame *rDanceGame = KPopDanceGame::Fetch();
    if(!rDanceGame) return LuaTypeError("KPop_GetProperty");

    //--Sets activity state.
    if(!strcasecmp(rSwitchType, "Set Active") && tArgs == 2)
    {
        rDanceGame->SetActive(lua_toboolean(L, 2));
    }
    //--Creates a new dancer.
    else if(!strcasecmp(rSwitchType, "Register Dancer") && tArgs == 2)
    {
        rDanceGame->CreateDancer(lua_tostring(L, 2));
    }
    //--Creates a new dance move for the named dancer.
    else if(!strcasecmp(rSwitchType, "Register Dance Move") && tArgs == 3)
    {
        rDanceGame->CreateDanceMove(lua_tostring(L, 2), lua_tostring(L, 3));
    }
    //--Sets how many frames the named dance move has.
    else if(!strcasecmp(rSwitchType, "Set Dancer Move Frames") && tArgs == 4)
    {
        rDanceGame->AllocateDanceMoveFrames(lua_tostring(L, 2), lua_tostring(L, 3), lua_tointeger(L, 4));
    }
    //--Sets the image and number of ticks to display for the named frame.
    else if(!strcasecmp(rSwitchType, "Set Dancer Move Frame") && tArgs == 6)
    {
        rDanceGame->SetDanceMoveFrame(lua_tostring(L, 2), lua_tostring(L, 3), lua_tointeger(L, 4), lua_tointeger(L, 5), lua_tostring(L, 6));
    }
    //--Once all dancers are registered, position them on the screen.
    else if(!strcasecmp(rSwitchType, "Position Dancers") && tArgs == 1)
    {
        rDanceGame->PositionDancers();
    }
    //--Sets which action the player should do.
    else if(!strcasecmp(rSwitchType, "Register Action") && tArgs == 3)
    {
        rDanceGame->RegisterInstruction(lua_tointeger(L, 2), lua_tointeger(L, 3));
    }
    //--Causes the program to dynamically output the player's inputs to a file.
    else if(!strcasecmp(rSwitchType, "Activate System Output") && tArgs == 2)
    {
        rDanceGame->ActivateSystemOutput(lua_tostring(L, 2));
    }
    //--Error.
    else
    {
        LuaPropertyError("KPop_SetProperty", rSwitchType, tArgs);
    }

    //--Finish up.
    return 0;
}
