//--[KPopDancer]
//--Represents a dancer in the KPop minigame.

#pragma once

//--[Includes]
#include "Definitions.h"
#include "Structures.h"
#include "RootObject.h"

//--[Local Structures]
//--[Local Definitions]
//--Sizes
#define DANCER_SCALE 4.0f
#define DANCER_OFFSET_X -24.0f
#define DANCER_OFFSET_Y -24.0f

//--Size Boost
#define DANCER_SCALE_TICKS_INFLECTION 4
#define DANCER_SCALE_TICKS_MAX 12

//--[Classes]
class KPopDancer : public RootObject
{
    private:
    //--System
    //--Position
    float mX;
    float mY;

    //--Movement
    int mMoveTimer;
    int mMoveTimerMax;
    float mStrtX;
    float mStrtY;
    float mDestX;
    float mDestY;

    //--Moves
    int mCurrentMove;
    int mCurrentMoveTicks;
    int mCurrentMoveFrame;

    //--Size Boost
    int mSizeIncreaseTimer;
    float mSizeIncreaseAmount;

    //--Images
    SugarLinkedList *mDanceMovesList; //KPopDanceMove *, master

    protected:

    public:
    //--System
    KPopDancer();
    virtual ~KPopDancer();

    //--Public Variables
    //--Property Queries
    //--Manipulators
    void RegisterMove(const char *pName, KPopDanceMove *pMove);
    void SetActiveMove(const char *pName);
    void MoveTo(float pX, float pY, int pTicks);
    void SetSizeBoost(float pAmount);

    //--Core Methods
    private:
    //--Private Core Methods
    public:
    //--Update
    void Update();

    //--File I/O
    //--Drawing
    void Render();

    //--Pointer Routing
    KPopDanceMove *GetDanceMove(const char *pName);

    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions

