//--Base
#include "KPopDancer.h"

//--Classes
#include "KPopDanceMove.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarLinkedList.h"

//--Definitions
#include "EasingFunctions.h"

//--Libraries
//--Managers

//=========================================== System ==============================================
KPopDancer::KPopDancer()
{
    //--[RootObject]
    //--System
    mType = POINTER_TYPE_KPOPDANCER;

    //--[KPopDancer]
    //--System
    //--Position
    mX = 0.0f;
    mY = 0.0f;

    //--Movement
    mMoveTimer = 0;
    mMoveTimerMax = 0;
    mStrtX = 0.0f;
    mStrtY = 0.0f;
    mDestX = 0.0f;
    mDestY = 0.0f;

    //--Moves
    mCurrentMove = 0;
    mCurrentMoveTicks = 0;
    mCurrentMoveFrame = 0;

    //--Size Boost
    mSizeIncreaseTimer = DANCER_SCALE_TICKS_MAX;
    mSizeIncreaseAmount = 0.0f;

    //--Images
    mDanceMovesList = new SugarLinkedList(true);
}
KPopDancer::~KPopDancer()
{
    delete mDanceMovesList;
}

//====================================== Property Queries =========================================
//========================================= Manipulators ==========================================
void KPopDancer::RegisterMove(const char *pName, KPopDanceMove *pMove)
{
    if(!pName || !pMove) return;
    mDanceMovesList->AddElement(pName, pMove, &KPopDanceMove::DeleteThis);
}
void KPopDancer::SetActiveMove(const char *pName)
{
    //--Reset flags.
    mCurrentMoveTicks = 0;
    mCurrentMoveFrame = 0;

    //--Reset to the idle move, which is always zero.
    mCurrentMove = 0;
    if(!strcasecmp(pName, "Idle")) return;

    //--Otherwise, get the index of the move and store it.
    mCurrentMove = mDanceMovesList->GetSlotOfElementByName(pName);

    //--Error, move not found, return to idle.
    if(mCurrentMove == -1) mCurrentMove = 0;
}
void KPopDancer::MoveTo(float pX, float pY, int pTicks)
{
    //--If the tick value is zero or lower, instantly move to the position.
    if(pTicks < 1)
    {
        mMoveTimer = 1;
        mMoveTimerMax = 1;
        mX = pX;
        mY = pY;
        mStrtX = pX;
        mStrtY = pY;
        mDestX = pX;
        mDestY = pY;
    }
    //--Otherwise, set flags to move towards the location.
    else
    {
        mMoveTimer = 0;
        mMoveTimerMax = pTicks;
        mStrtX = mX;
        mStrtY = mY;
        mDestX = pX;
        mDestY = pY;
    }
}
void KPopDancer::SetSizeBoost(float pAmount)
{
    mSizeIncreaseTimer = 0;
    mSizeIncreaseAmount = pAmount;
}

//========================================= Core Methods ==========================================
//===================================== Private Core Methods ======================================
//============================================ Update =============================================
void KPopDancer::Update()
{
    //--[Moving]
    //--If the move is not complete, update positions.
    if(mMoveTimer < mMoveTimerMax)
    {
        //--Update.
        mMoveTimer ++;

        //--Recompute positions.
        float cPercent = EasingFunction::QuadraticOut(mMoveTimer, mMoveTimerMax);
        mX = mStrtX + ((mDestX - mStrtX) * cPercent);
        mY = mStrtY + ((mDestY - mStrtY) * cPercent);
    }

    //--[Size Timer]
    //--Increments if below the cap.
    if(mSizeIncreaseTimer < DANCER_SCALE_TICKS_MAX)
    {
        mSizeIncreaseTimer ++;
    }

    //--[Dancing]
    //--Get the current dance move.
    KPopDanceMove *rActiveMove = (KPopDanceMove *)mDanceMovesList->GetElementBySlot(mCurrentMove);
    if(!rActiveMove) return;

    //--Update the dance timer.
    mCurrentMoveTicks ++;

    //--Check if the current frame is complete.
    if(mCurrentMoveTicks >= rActiveMove->GetFrameTicks(mCurrentMoveFrame))
    {
        //--Increment the frame.
        mCurrentMoveTicks = 0;
        mCurrentMoveFrame ++;

        //--If the frame went over the edge, return to the idle animation.
        if(mCurrentMoveFrame >= rActiveMove->mTotalFrames)
        {
            mCurrentMoveFrame = 0;
            SetActiveMove("Idle");
        }
    }
}

//=========================================== File I/O ============================================
//=========================================== Drawing =============================================
void KPopDancer::Render()
{
    //--[Documentation and Setup]
    //--Renders the dancer at the internally computed position with the current dance move.

    //--Resolve the current dance move.
    KPopDanceMove *rActiveMove = (KPopDanceMove *)mDanceMovesList->GetElementBySlot(mCurrentMove);
    if(!rActiveMove) return;

    //--Resolve the current frame.
    SugarBitmap *rImage = rActiveMove->GetImage(mCurrentMoveFrame);
    if(!rImage) return;

    //--[Scale Computations]
    //--The dancer increases in scale for each tick on an internal timer. This makes the dancer "pop"
    //  the player gets a move correct.
    float cScaleBase  = DANCER_SCALE;

    //--We use an increase up to tick DANCER_SCALE_TICKS_INFLECTION, then a decrease to the finale.
    float cPercentIncrease = 0.0f;
    if(mSizeIncreaseTimer < DANCER_SCALE_TICKS_INFLECTION)
    {
        cPercentIncrease = EasingFunction::QuadraticOut(mSizeIncreaseTimer, DANCER_SCALE_TICKS_INFLECTION);
    }
    else
    {
        cPercentIncrease = 1.0f - EasingFunction::QuadraticIn(mSizeIncreaseTimer-DANCER_SCALE_TICKS_INFLECTION, DANCER_SCALE_TICKS_MAX-DANCER_SCALE_TICKS_INFLECTION);
    }
    float cScaleBoost = 1.0f + (cPercentIncrease * mSizeIncreaseAmount);

    //--Compute the inversions.
    if(cScaleBase  <= 0.0f) cScaleBase = 1.0f;
    if(cScaleBoost <= 0.0f) cScaleBoost = 1.0f;
    float cScaleBaseInv  = 1.0f / cScaleBase;
    float cScaleBoostInv = 1.0f / cScaleBoost;

    //--[Rendering]
    //--Scaling and Position
    glTranslatef(mX, mY, 0.0f);
    glScalef(cScaleBase, cScaleBase, 1.0f);

    //--Render the 'ghost' which goes behind the dancer to enunciate player inputs.
    if(mSizeIncreaseTimer < DANCER_SCALE_TICKS_INFLECTION)
    {
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, 0.10f);
        glScalef(cScaleBoost, cScaleBoost, 1.0f);
        rImage->Draw(DANCER_OFFSET_X, DANCER_OFFSET_Y);
        glScalef(cScaleBoostInv, cScaleBoostInv, 1.0f);
        StarlightColor::ClearMixer();
    }

    //--Render.
    rImage->Draw(DANCER_OFFSET_X, DANCER_OFFSET_Y);

    //--Clean.
    glScalef(cScaleBaseInv, cScaleBaseInv, 1.0f);
    glTranslatef(-mX, -mY, 0.0f);
}

//======================================= Pointer Routing =========================================
KPopDanceMove *KPopDancer::GetDanceMove(const char *pName)
{
    return (KPopDanceMove *)mDanceMovesList->GetElementByName(pName);
}

//====================================== Static Functions =========================================
//========================================= Lua Hooking ===========================================
//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
