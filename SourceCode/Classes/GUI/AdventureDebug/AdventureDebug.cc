//--Base
#include "AdventureDebug.h"

//--Classes
#include "AdvCombat.h"
#include "AdventureInventory.h"
#include "AdventureLevel.h"
#include "BuildPackage.h"
#include "DialogueTopic.h"
#include "ScriptHunter.h"
#include "WorldDialogue.h"

//--CoreClasses
#include "SugarFont.h"
#include "SugarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "Global.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "DisplayManager.h"
#include "LuaManager.h"

//=========================================== System ==============================================
AdventureDebug::AdventureDebug()
{
    //--[AdventureDebug]
    //--System
    mIsVisible = false;

    //--Menu Cursor
    mMenuMode = ADM_MODE_MAIN;
    mMenuCursor = 0;
    mMenuMax = ADM_OPT_MAIN_TOTAL;

    //--Script Variables Listing
    mScriptVarsTotal = 0;
    mScriptVarOffset = 0;
    mScriptVarNames = NULL;
    mScriptVarValues = NULL;

    //--Topic Listing
    mTopicsTotal = 0;
    mTopicOffset = 0;
    mrTopicNames = NULL;
    mTopicValues = NULL;

    //--Warp Listing
    mWarpListing = new SugarLinkedList(true);
    rActiveListing = NULL;
    mCursorStack = new SugarLinkedList(true);

    //--Builds Listing
    mIsEmulation = true;
    mBuildFile = ADM_BUILD_FILE_STDERR;

    //--Images
    memset(&Images, 0, sizeof(Images));
}
AdventureDebug::~AdventureDebug()
{
    AllocateScriptVariables(0);
    free(mrTopicNames);
    free(mTopicValues);
    delete mWarpListing;
    delete mCursorStack;
}

//--[Private Statics]
//--Warp Listing
int AdventureDebug::xWarpDestinationsTotal = 0;
char **AdventureDebug::xWarpDestinations = NULL;

//--Scenes Listing
int AdventureDebug::xScenesTotal = 0;
char **AdventureDebug::xSceneListing = NULL;

//--Forms Listing
int AdventureDebug::xFormsTotal = 0;
char **AdventureDebug::xFormListing = NULL;

//--[Public Statics]
//--Allows the debug menu to be brought up even if the password isn't correct.
bool AdventureDebug::xManualActivation = false;

//--When active, enemies will not chase the player at all. Walking into one still triggers combat.
bool AdventureDebug::xCamouflageMode = false;

//--List of Directories for the Script Hunter. The Script Huntr checks for syntax errors in all
//  Lua files it finds when activated. While it can only be activated from the debug menu in
//  Adventure Mode, it can be customized to search any directory requested.
SugarLinkedList *AdventureDebug::xmScriptHunterPathsList = new SugarLinkedList(true);

//====================================== Property Queries =========================================
bool AdventureDebug::IsVisible()
{
    return mIsVisible;
}

//========================================= Manipulators ==========================================
void AdventureDebug::Show()
{
    //--Debug Menu cannot be shown in "Nowhere" to prevent... so many bugs...
    AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
    if(rActiveLevel && !strcasecmp("Nowhere", rActiveLevel->GetName()))
    {
        return;
    }

    //--Flags.
    mIsVisible = true;
    SetToMain();
    AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");

    //--Attempt to resolve images.
    if(!Images.mIsReady)
    {
        DataLibrary *rDataLibrary = DataLibrary::Fetch();
        Images.Data.rMenuFont = rDataLibrary->GetFont("Adventure Debug Font");
        Images.mIsReady = VerifyStructure(&Images.Data, sizeof(Images.Data), sizeof(void *));
    }
}
void AdventureDebug::Hide()
{
    mIsVisible = false;
}
void AdventureDebug::SetVisibility(bool pFlag)
{
    if(pFlag)
        Show();
    else
        Hide();
}
void AdventureDebug::SetToMain()
{
    mMenuMode = ADM_MODE_MAIN;
    mMenuCursor = 0;
    mMenuMax = ADM_OPT_MAIN_TOTAL;
}
void AdventureDebug::SetToWarp()
{
    //--Rebuild the warp listing.
    BuildWarpListing();

    //--Flags.
    mMenuMode = ADM_MODE_WARP;
    mMenuCursor = 0;
    rActiveListing = mWarpListing;
    mMenuMax = mWarpListing->GetListSize();
    mCursorStack->ClearList();
}
void AdventureDebug::SetToScenes()
{
    mMenuMode = ADM_MODE_SCENES;
    mMenuCursor = 0;
    mMenuMax = xScenesTotal;
}
void AdventureDebug::SetToScriptVars()
{
    RebuildVariableListing();
    mMenuMode = ADM_MODE_SCRIPTVARS;
    mMenuCursor = 0;
    mScriptVarOffset = 0;
    mMenuMax = mScriptVarsTotal;
}
void AdventureDebug::SetToEnemies()
{
    mMenuMode = ADM_MODE_ENEMIES;
    mMenuCursor = 0;
    mMenuMax = ADM_OPT_ENEMIES_TOTAL;
}
void AdventureDebug::SetToParty()
{
    mMenuMode = ADM_MODE_PARTY;
    mMenuCursor = 0;
    mMenuMax = ADM_OPT_PARTY_TOTAL;
}
void AdventureDebug::SetToForms()
{
    mMenuMode = ADM_MODE_FORMS;
    mMenuCursor = 0;
    mMenuMax = xFormsTotal;
}
void AdventureDebug::SetToTopics()
{
    //--Make sure at least one topic exists.
    BuildTopicListing();
    if(!mrTopicNames) return;

    //--Flags.
    mMenuMode = ADM_MODE_TOPICS;
    mMenuCursor = 0;
    mTopicOffset = 0;
    mMenuMax = mTopicsTotal;
}
void AdventureDebug::SetToScriptHunter()
{
    mMenuMode = ADM_MODE_SCRIPTHUNTER;
    mMenuCursor = 0;
    mMenuMax = xmScriptHunterPathsList->GetListSize();
}
void AdventureDebug::SetToBuilds()
{
    BuildPackage::Scan();
    mMenuMode = ADM_MODE_BUILDS;
    mMenuCursor = 0;
    mMenuMax = BuildPackage::FetchBuildList()->GetListSize() + 2;
}

//========================================= Core Methods ==========================================
void AdventureDebug::AllocateScriptVariables(int pAmount)
{
    //--Clears and reallocates space for script variables. Pass a value less than 1 to clear.
    for(int i = 0; i < mScriptVarsTotal; i ++)
    {
        free(mScriptVarNames[i]);
        free(mScriptVarValues[i]);
    }
    free(mScriptVarNames);
    free(mScriptVarValues);

    //--Reset.
    mScriptVarsTotal = 0;
    mScriptVarNames = NULL;
    mScriptVarValues = NULL;
    if(pAmount < 1) return;

    //--Allocate space.
    mScriptVarsTotal = pAmount;
    SetMemoryData(__FILE__, __LINE__);
    mScriptVarNames = (char **)starmemoryalloc(sizeof(char *) * mScriptVarsTotal);
    SetMemoryData(__FILE__, __LINE__);
    mScriptVarValues = (char **)starmemoryalloc(sizeof(char *) * mScriptVarsTotal);
    for(int i = 0; i < mScriptVarsTotal; i ++)
    {
        mScriptVarNames[i] = NULL;
        mScriptVarValues[i] = NULL;
    }
}
void AdventureDebug::RebuildVariableListing()
{
    //--Checks the DataLibrary for all script variables and stores them. They can be modified from
    //  the debug menu directly, but use at your own risk.
    char tNameBuffer[256];
    DataLibrary *rDataLibrary = DataLibrary::Fetch();

    //--This linked-list will store every variable we're interested in. It contains references.
    SugarLinkedList *trVarList = new SugarLinkedList(false);

    //--First, we need to recurse through the variables section and count how many variables there are.
    SugarLinkedList *rSectionList = rDataLibrary->GetSection("Root/Variables/");
    if(!rSectionList)
    {
        AllocateScriptVariables(0);
        return;
    }

    //--Iterate across the catalogues.
    SugarLinkedList *rCatalogueList = (SugarLinkedList *)rSectionList->PushIterator();
    while(rCatalogueList)
    {
        //--Iterate across the headings.
        SugarLinkedList *rHeadingList = (SugarLinkedList *)rCatalogueList->PushIterator();
        while(rHeadingList)
        {
            //--Each entry in the heading is a SysVar pointer. Put it on our storage list.
            void *rVariable = rHeadingList->PushIterator();
            while(rVariable)
            {
                //--Create a name buffer.
                sprintf(tNameBuffer, "Root/%s/%s/%s/%s", "Variables", rSectionList->GetIteratorName(), rCatalogueList->GetIteratorName(), rHeadingList->GetIteratorName());
                trVarList->AddElementAsTail(tNameBuffer, rVariable);

                //--Next.
                rVariable = rHeadingList->AutoIterate();
            }

            //--Next.
            rHeadingList = (SugarLinkedList *)rCatalogueList->AutoIterate();
        }

        //--Next.
        rCatalogueList = (SugarLinkedList *)rSectionList->AutoIterate();
    }

    //--Allocate space.
    AllocateScriptVariables(trVarList->GetListSize());

    //--Store the results.
    int i = 0;
    SysVar *rVariable = (SysVar *)trVarList->PushIterator();
    while(rVariable)
    {
        //--Name is the same as the iterator's name, which is the variable's full path.
        ResetString(mScriptVarNames[i], trVarList->GetIteratorName());

        //--If this variable is a string, store that.
        if(rVariable->mAlpha && strcasecmp(rVariable->mAlpha, "NULL"))
        {
            mScriptVarValues[i] = InitializeString("S: %s", rVariable->mAlpha);
        }
        //--Otherwise, it's a number.
        else
        {
            mScriptVarValues[i] = InitializeString("N: %f", rVariable->mNumeric);
        }

        //--Next.
        i ++;
        rVariable = (SysVar *)trVarList->AutoIterate();
    }

    //--Clean.
    delete trVarList;
}
void AdventureDebug::BuildTopicListing()
{
    //--Runs through the topic listing and stores their names as reference copies.
    mTopicsTotal = 0;
    free(mrTopicNames);
    mrTopicNames = NULL;
    free(mTopicValues);
    mTopicValues = NULL;

    //--Get the topic listing.
    SugarLinkedList *rTopicList = WorldDialogue::Fetch()->GetTopicList();
    if(rTopicList->GetListSize() < 1) return;

    //--Allocate.
    mTopicsTotal = rTopicList->GetListSize();
    SetMemoryData(__FILE__, __LINE__);
    mrTopicNames = (const char **)starmemoryalloc(sizeof(char *) * mTopicsTotal);
    SetMemoryData(__FILE__, __LINE__);
    mTopicValues = (int *)starmemoryalloc(sizeof(int) * mTopicsTotal);

    //--Iterate and store the references.
    int i = 0;
    DialogueTopic *rTopic = (DialogueTopic *)rTopicList->PushIterator();
    while(rTopic)
    {
        //--Store.
        mrTopicNames[i] = rTopic->GetInternalName();
        mTopicValues[i] = rTopic->GetLevel();

        //--Next.
        i++;
        rTopic = (DialogueTopic *)rTopicList->AutoIterate();
    }
}
void AdventureDebug::BuildWarpListing()
{
    //--Build the warp linked lists from the statically-built warp information.
    delete mWarpListing;
    mWarpListing = new SugarLinkedList(true);
    for(int i = 0; i < xWarpDestinationsTotal; i ++)
    {
        //--Buffer.
        int tBufferIndex = 0;
        char tBuffer[128];

        //--Checking list.
        SugarLinkedList *rCurrentWarpList = mWarpListing;

        //--Debug:
        //fprintf(stderr, "Scanning string: %s\n", xWarpDestinations[i]);

        //--Scan the string.
        int tLen = (int)strlen(xWarpDestinations[i]);
        for(int p = 0; p < tLen; p ++)
        {
            //--If this is a forward-slash, then the preceding string is the name of a warp sublist.
            if(xWarpDestinations[i][p] == '/')
            {
                //--Error check: Don't do anything if the forward slash has nothing preceding it.
                if(tBufferIndex == 0) continue;

                //--Check if this sublist already exists on the checking list. If it does it becomes the active list.
                WarpListing *rCheckListing = (WarpListing *)rCurrentWarpList->GetElementByName(tBuffer);
                if(rCheckListing)
                {
                    //fprintf(stderr, " Entering sublist: %s\n", tBuffer);
                    rCurrentWarpList = rCheckListing->mSubList;
                }
                //--New, so initialize a new listing. This will hold a sub-listing.
                else
                {
                    //--Create and register.
                    SetMemoryData(__FILE__, __LINE__);
                    WarpListing *nNewListing = (WarpListing *)starmemoryalloc(sizeof(WarpListing));
                    nNewListing->mSubList = new SugarLinkedList(true);
                    nNewListing->mWarpTarget = NULL;
                    rCurrentWarpList->AddElement(tBuffer, nNewListing, &WarpListing::DeleteThis);

                    //--The new listing's sublist becomes the current list.
                    rCurrentWarpList = nNewListing->mSubList;
                    //fprintf(stderr, " Entering sublist after creation: %s\n", tBuffer);
                }

                //--In all cases, clear the buffer.
                tBuffer[0] = '\0';
                tBufferIndex = 0;
            }
            //--Otherwise, append it to the buffer.
            else
            {
                tBuffer[tBufferIndex+0] = xWarpDestinations[i][p];
                tBuffer[tBufferIndex+1] = '\0';
                tBufferIndex ++;
            }
        }

        //--Once the string is done scanning, whatever is in the buffer is the name. Add that to the end of the list.
        //fprintf(stderr, "  Final name of level: %s\n", tBuffer);
        if(tBufferIndex > 0)
        {
            SetMemoryData(__FILE__, __LINE__);
            WarpListing *nNewListing = (WarpListing *)starmemoryalloc(sizeof(WarpListing));
            nNewListing->mSubList = NULL;
            nNewListing->mWarpTarget = InitializeString("%s", tBuffer);
            rCurrentWarpList->AddElement(tBuffer, nNewListing, &WarpListing::DeleteThis);
        }
    }
}

//===================================== Private Core Methods ======================================
//============================================ Update =============================================
void AdventureDebug::StandardCursorUpdate()
{
    //--Moves the cursor and handles SFX if necessary.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Store the cursor position.
    int tOldCursor = mMenuCursor;

    //--Up!
    if(rControlManager->IsFirstPress("Up"))
    {
        //--Normal case:
        if(!rControlManager->IsDown("Ctrl"))
        {
            mMenuCursor --;
        }
        //--Holding down Ctrl increases by 10.
        else
        {
            mMenuCursor -= 10;
        }

        //--Clamp.
        if(mMenuCursor < 0) mMenuCursor = 0;
    }

    //--Down!
    if(rControlManager->IsFirstPress("Down"))
    {
        //--Normal case:
        if(!rControlManager->IsDown("Ctrl"))
        {
            mMenuCursor ++;
        }
        //--Holding down Ctrl increases by 10.
        else
        {
            mMenuCursor += 10;
        }

        //--Clamp.
        if(mMenuCursor >= mMenuMax) mMenuCursor = mMenuMax - 1;
        if(mMenuCursor < 0) mMenuCursor = 0;
    }

    //--Retroactive offset handling: Script variables.
    if(mMenuMode == ADM_MODE_SCRIPTVARS)
    {
        //--Setup.
        const int cMaxVarsPerPage = 57;

        //--Set.
        mScriptVarOffset = mMenuCursor - cMaxVarsPerPage/2;

        //--Clamp.
        if(mScriptVarOffset < 0) mScriptVarOffset = 0;
        if(mScriptVarOffset > mScriptVarsTotal - cMaxVarsPerPage) mScriptVarOffset = mScriptVarsTotal - cMaxVarsPerPage;
    }
    //--Retroactive offset handling: Topics.
    else if(mMenuMode == ADM_MODE_TOPICS)
    {
        //--Setup.
        const int cMaxVarsPerPage = 57;

        //--Set.
        mTopicOffset = mMenuCursor - cMaxVarsPerPage/2;

        //--Clamp.
        if(mTopicOffset < 0) mTopicOffset = 0;
        if(mTopicOffset > mTopicsTotal - cMaxVarsPerPage) mTopicOffset = mTopicsTotal - cMaxVarsPerPage;
    }

    //--If the cursor changed, handle SFX.
    if(tOldCursor != mMenuCursor)
    {
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }
}
void AdventureDebug::Update()
{
    //--Handle the cursor first.
    StandardCursorUpdate();

    //--Handle Accept/Cancel using their subroutines.
    if(mMenuMode == ADM_MODE_WARP)         { UpdateWarp();         return; }
    if(mMenuMode == ADM_MODE_SCENES)       { UpdateScenes();       return; }
    if(mMenuMode == ADM_MODE_SCRIPTVARS)   { UpdateScriptVars();   return; }
    if(mMenuMode == ADM_MODE_MAIN)         { UpdateMain();         return; }
    if(mMenuMode == ADM_MODE_ENEMIES)      { UpdateEnemies();      return; }
    if(mMenuMode == ADM_MODE_PARTY)        { UpdateParty();        return; }
    if(mMenuMode == ADM_MODE_FORMS)        { UpdateForms();        return; }
    if(mMenuMode == ADM_MODE_TOPICS)       { UpdateTopics();       return; }
    if(mMenuMode == ADM_MODE_SCRIPTHUNTER) { UpdateScriptHunter(); return; }
    if(mMenuMode == ADM_MODE_BUILDS)       { UpdateBuilds();       return; }
}
void AdventureDebug::UpdateMain()
{
    //--Main menu. Allows access to submenus and exiting this menu. Does not require an AdventureLevel to do anything.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Activate handling.
    if(rControlManager->IsFirstPress("Activate"))
    {
        //--Activate the Warp submenu.
        if(mMenuCursor == ADM_OPT_MAIN_OPEN_WARP_SUBMENU)
        {
            SetToWarp();
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        //--Activate the Scenes submenu.
        else if(mMenuCursor == ADM_OPT_MAIN_OPEN_SCENES_SUBMENU)
        {
            SetToScenes();
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        //--Activate the ScriptVars submenu.
        else if(mMenuCursor == ADM_OPT_MAIN_OPEN_SCRIPTVARS_SUBMENU)
        {
            SetToScriptVars();
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        //--Activate the Enemies submenu.
        else if(mMenuCursor == ADM_OPT_MAIN_OPEN_ENEMIES_SUBMENU)
        {
            SetToEnemies();
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        //--Activate the Party submenu.
        else if(mMenuCursor == ADM_OPT_MAIN_OPEN_PARTY_SUBMENU)
        {
            SetToParty();
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        //--Activate the Forms submenu.
        else if(mMenuCursor == ADM_OPT_MAIN_OPEN_FORM_SUBMENU)
        {
            SetToForms();
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        //--Activate the Topics submenu
        else if(mMenuCursor == ADM_OPT_MAIN_OPEN_TOPIC_SUBMENU)
        {
            SetToTopics();
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        //--Instantly clear overlays, if any exist.
        else if(mMenuCursor == ADM_OPT_MAIN_CLEAR_OVERLAY)
        {
            AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
            if(rActiveLevel)
            {
                rActiveLevel->ActivateScriptFade(1, StarlightColor::MapRGBAF(0, 0, 0, 1.0f), StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, 0.0f), 0, false);
            }
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        //--Open the Script Hunter submenu.
        else if(mMenuCursor == ADM_OPT_MAIN_OPEN_SCRIPT_HUNTER)
        {
            SetToScriptHunter();
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        //--Open the Builds submenu.
        else if(mMenuCursor == ADM_OPT_MAIN_OPEN_BUILDS)
        {
            SetToBuilds();
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        //--Close this menu.
        else
        {
            Hide();
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
    }
    //--Cancel. Closes the menu.
    else if(rControlManager->IsFirstPress("Cancel") || rControlManager->IsFirstPress("Escape"))
    {
        Hide();
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }
}
void AdventureDebug::UpdateWarp()
{
    //--Warps to various destinations. Has a variable number of entries, so each one is assumed to be the path
    //  to a level. We do not check if the level exists first! Aiee! This is debug, you dope!
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Activate handling.
    if(rControlManager->IsFirstPress("Activate"))
    {
        AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
        if(rActiveLevel && mMenuCursor >= 0 && mMenuCursor < xWarpDestinationsTotal)
        {
            //--Get the element under consideration:
            WarpListing *rWarpListing = (WarpListing *)rActiveListing->GetElementBySlot(mMenuCursor);
            if(rWarpListing)
            {
                //--Does the warp listing have a submenu? If so, make that the current menu.
                if(rWarpListing->mSubList)
                {
                    //--Store the cursor stuff on the cursor stack:
                    SetMemoryData(__FILE__, __LINE__);
                    CursorStackEntry *nStackEntry = (CursorStackEntry *)starmemoryalloc(sizeof(CursorStackEntry));
                    nStackEntry->mCursor = mMenuCursor;
                    nStackEntry->rActiveList = rActiveListing;
                    mCursorStack->AddElementAsHead("X", nStackEntry, &FreeThis);

                    //--Move to the next list.
                    mMenuCursor = 0;
                    mMenuMax = rWarpListing->mSubList->GetListSize();
                    rActiveListing = rWarpListing->mSubList;
                }
                //--Otherwise, warp to that room.
                else
                {
                    Hide();
                    rActiveLevel->Debug_TransitionTo(rWarpListing->mWarpTarget);
                }
            }
            //--Error:
            else
            {
                Hide();
            }
        }

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }
    //--Cancel. Closes the menu or goes up one list on the stack.
    else if(rControlManager->IsFirstPress("Cancel") || rControlManager->IsFirstPress("Escape"))
    {
        //--If there's anything on the cursor stack, go up one level:
        if(mCursorStack->GetListSize() > 0)
        {
            //--Verify the entry:
            CursorStackEntry *rHead = (CursorStackEntry *)mCursorStack->GetHead();
            if(rHead)
            {
                mMenuCursor = rHead->mCursor;
                mMenuMax = rHead->rActiveList->GetListSize();
                rActiveListing = rHead->rActiveList;
            }

            //--Pop.
            mCursorStack->RemoveElementI(0);
        }
        //--Otherwise, go back to the main menu:
        else
        {
            SetToMain();
            mMenuCursor = ADM_OPT_MAIN_OPEN_WARP_SUBMENU;
        }

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }
}
void AdventureDebug::UpdateScenes()
{
    //--Cutscene menu. Similar to the warp screen, has a variable argument listing. Activates cutscenes
    //  when activated.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Activate handling.
    if(rControlManager->IsFirstPress("Activate"))
    {
        AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
        if(rActiveLevel && mMenuCursor >= 0 && mMenuCursor < xScenesTotal)
        {
            Hide();
            rActiveLevel->Debug_FireCutscene(xSceneListing[mMenuCursor]);
        }
    }
    //--Cancel. Closes the menu.
    else if(rControlManager->IsFirstPress("Cancel") || rControlManager->IsFirstPress("Escape"))
    {
        SetToMain();
        mMenuCursor = ADM_OPT_MAIN_OPEN_SCENES_SUBMENU;
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }
}
void AdventureDebug::UpdateScriptVars()
{
    //--Script variable editor.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Activate handling.
    if(rControlManager->IsFirstPress("Activate"))
    {
    }
    //--Cancel. Closes the menu.
    else if(rControlManager->IsFirstPress("Cancel") || rControlManager->IsFirstPress("Escape"))
    {
        SetToMain();
        mMenuCursor = ADM_OPT_MAIN_OPEN_SCRIPTVARS_SUBMENU;
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }
}
void AdventureDebug::UpdateEnemies()
{
    //--Enemies menu. Remember that this is accessed from the field, so we can wipe out field enemies without
    //  causing dangling pointer errors.
    AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
    ControlManager *rControlManager = ControlManager::Fetch();
    if(!rActiveLevel) { SetToMain(); return; }

    //--Activate handling.
    if(rControlManager->IsFirstPress("Activate"))
    {
        //--Wipe all field enemies out.
        if(mMenuCursor == ADM_OPT_ENEMIES_WIPE_FIELD)
        {
            rActiveLevel->Debug_WipeFieldEnemies();
            AudioManager::Fetch()->PlaySound("Menu|Levelup");
        }
        //--Forcibly respawn all field enemies.
        else if(mMenuCursor == ADM_OPT_ENEMIES_RESPAWN_ALL)
        {
            rActiveLevel->Debug_RespawnFieldEnemies();
            AudioManager::Fetch()->PlaySound("Menu|Levelup");
        }
        //--Toggle Camouflage on or off.
        else if(mMenuCursor == ADM_OPT_ENEMIES_CAMOUFLAGE)
        {
            xCamouflageMode = !xCamouflageMode;
            AudioManager::Fetch()->PlaySound("Menu|Levelup");
        }
        //--Close this menu.
        else
        {
            SetToMain();
            mMenuCursor = ADM_OPT_MAIN_OPEN_ENEMIES_SUBMENU;
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
    }
    //--Cancel. Closes the menu.
    else if(rControlManager->IsFirstPress("Cancel") || rControlManager->IsFirstPress("Escape"))
    {
        SetToMain();
        mMenuCursor = ADM_OPT_MAIN_OPEN_ENEMIES_SUBMENU;
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }
}
void AdventureDebug::UpdateParty()
{
    //--Party menu. Refill health, gain XP and money and items, that sort of thing.
    AdvCombat *rAdventureCombat = AdvCombat::Fetch();
    AdventureInventory *rInventory = AdventureInventory::Fetch();
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Activate handling.
    if(rControlManager->IsFirstPress("Activate"))
    {
        //--Restore the party to full health.
        if(mMenuCursor == ADM_OPT_PARTY_FULL_RESTORE)
        {
            rAdventureCombat->Debug_RestoreParty();
            AudioManager::Fetch()->PlaySound("Menu|Levelup");
        }
        //--Give everyone in the party 200 XP.
        else if(mMenuCursor == ADM_OPT_PARTY_GIVE_EXPA)
        {
            rAdventureCombat->Debug_AwardXP(200);
            AudioManager::Fetch()->PlaySound("Menu|Levelup");
        }
        //--Give everyone in the party 2000 XP.
        else if(mMenuCursor == ADM_OPT_PARTY_GIVE_EXPB)
        {
            rAdventureCombat->Debug_AwardXP(2000);
            AudioManager::Fetch()->PlaySound("Menu|Levelup");
        }
        //--Give everyone in the party 20000 XP.
        else if(mMenuCursor == ADM_OPT_PARTY_GIVE_EXPC)
        {
            rAdventureCombat->Debug_AwardXP(20000);
            AudioManager::Fetch()->PlaySound("Menu|Levelup");
        }
        //--Give the party 1000 Platina.
        else if(mMenuCursor == ADM_OPT_PARTY_GIVE_CASH)
        {
            rInventory->Debug_AwardPlatina();
            AudioManager::Fetch()->PlaySound("Menu|Levelup");
        }
        //--Give the party 10 of each crafting material.
        else if(mMenuCursor == ADM_OPT_PARTY_GIVE_CRAFTING)
        {
            rInventory->Debug_AwardCrafting();
            AudioManager::Fetch()->PlaySound("Menu|Levelup");
        }
        //--Close this menu.
        else
        {
            SetToMain();
            mMenuCursor = ADM_OPT_MAIN_OPEN_PARTY_SUBMENU;
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
    }
    //--Cancel. Closes the menu.
    else if(rControlManager->IsFirstPress("Cancel") || rControlManager->IsFirstPress("Escape"))
    {
        SetToMain();
        mMenuCursor = ADM_OPT_MAIN_OPEN_PARTY_SUBMENU;
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }
}
void AdventureDebug::UpdateForms()
{
    //--Similar to the cutscenes, uses the static listing. Closes the menu upon execution.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Activate handling.
    if(rControlManager->IsFirstPress("Activate"))
    {
        //--Build the name.
        char tBuffer[256];
        const char *rAdventurePath = DataLibrary::GetGamePath("Root/Paths/System/Startup/sAdventurePath");
        sprintf(tBuffer, "%s/FormHandlers/%s.lua", rAdventurePath, xFormListing[mMenuCursor]);
        LuaManager::Fetch()->ExecuteLuaFile(tBuffer);

        //--Hide this menu.
        Hide();
        AudioManager::Fetch()->PlaySound("Menu|Select");
    }
    //--Cancel. Closes the menu.
    else if(rControlManager->IsFirstPress("Cancel") || rControlManager->IsFirstPress("Escape"))
    {
        SetToMain();
        mMenuCursor = ADM_OPT_MAIN_OPEN_FORM_SUBMENU;
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }
}
void AdventureDebug::UpdateTopics()
{
    //--Uses the topic listing. Dynamically sized. Left and Right modify
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Get the topic listing.
    SugarLinkedList *rTopicList = WorldDialogue::Fetch()->GetTopicList();

    //--Left and Right edit the topic values.
    if(rControlManager->IsFirstPress("Left"))
    {
        //--Edit.
        if(mTopicValues[mMenuCursor] > -1)
        {
            mTopicValues[mMenuCursor] --;
            DialogueTopic *rTopic = (DialogueTopic *)rTopicList->GetElementBySlot(mMenuCursor);
            if(rTopic)
            {
                rTopic->SetLevel(mTopicValues[mMenuCursor]);
            }
        }

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }
    else if(rControlManager->IsFirstPress("Right"))
    {
        //--Edit.
        mTopicValues[mMenuCursor] ++;
        DialogueTopic *rTopic = (DialogueTopic *)rTopicList->GetElementBySlot(mMenuCursor);
        if(rTopic)
        {
            rTopic->SetLevel(mTopicValues[mMenuCursor]);
        }

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }

    //--Activate handling.
    if(rControlManager->IsFirstPress("Activate"))
    {
    }
    //--Cancel. Closes the menu.
    else if(rControlManager->IsFirstPress("Cancel") || rControlManager->IsFirstPress("Escape"))
    {
        SetToMain();
        mMenuCursor = ADM_OPT_MAIN_OPEN_TOPIC_SUBMENU;
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }
}
void AdventureDebug::UpdateScriptHunter()
{
    //--Uses the Script Hunter listing. This is built when the game boots.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Activate handling.
    if(rControlManager->IsFirstPress("Activate"))
    {
        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");

        //--Select the entry and run the Script Hunter.
        const char *rPath = (const char *)xmScriptHunterPathsList->GetElementBySlot(mMenuCursor);
        if(!rPath) return;

        //--Build and run.
        ScriptHunter *tScriptHunter = new ScriptHunter();
        tScriptHunter->SetTargetDirectory(rPath);
        tScriptHunter->Execute();
        delete tScriptHunter;
    }
    //--Cancel. Closes the menu.
    else if(rControlManager->IsFirstPress("Cancel") || rControlManager->IsFirstPress("Escape"))
    {
        SetToMain();
        mMenuCursor = ADM_OPT_MAIN_OPEN_SCRIPT_HUNTER;
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }
}
void AdventureDebug::UpdateBuilds()
{
    //--Used for automated build construction.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Activate handling.
    if(rControlManager->IsFirstPress("Activate"))
    {
        //--If this is the emulation option:
        if(mMenuCursor == ADM_BUILD_TOGGLE_EMULATION)
        {
            mIsEmulation = !mIsEmulation;
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
        //--If this is the emulation option:
        else if(mMenuCursor == ADM_BUILD_TOGGLE_FILE)
        {
            mBuildFile = (mBuildFile + 1) % ADM_BUILD_FILE_TOTAL;
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
        //--Otherwise, it's a build. Run that.
        else
        {
            //--Get the build list and package.
            SugarLinkedList *rBuildList = BuildPackage::FetchBuildList();
            BuildPackage *rPackage = (BuildPackage *)rBuildList->GetElementBySlot(mMenuCursor - 2);
            if(!rPackage)
            {
                AudioManager::Fetch()->PlaySound("Menu|Failed");
            }
            else
            {
                //--Execute with various file outputs.
                if(mBuildFile == ADM_BUILD_FILE_NONE)
                    rPackage->Execute(mIsEmulation, NULL);
                else if(mBuildFile == ADM_BUILD_FILE_STDERR)
                    rPackage->Execute(mIsEmulation, stderr);
                else
                    rPackage->ExecuteToPath(mIsEmulation, "Build Output.txt");

                //--SFX.
                AudioManager::Fetch()->PlaySound("Menu|Save");
            }
        }
    }
    //--Cancel. Closes the menu.
    else if(rControlManager->IsFirstPress("Cancel") || rControlManager->IsFirstPress("Escape"))
    {
        SetToMain();
        mMenuCursor = ADM_OPT_MAIN_OPEN_BUILDS;
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }
}

//=========================================== File I/O ============================================
//=========================================== Drawing =============================================
void AdventureDebug::RenderOverlay()
{
    //--Renders the overlay that goes under the menu. Can be called externally.
    glDisable(GL_TEXTURE_2D);
    glColor4f(0.2f, 0.2f, 0.2f, 0.9f);

    //--Render.
    glBegin(GL_QUADS);
        glVertex2f(            0.0f,             0.0f);
        glVertex2f(VIRTUAL_CANVAS_X,             0.0f);
        glVertex2f(VIRTUAL_CANVAS_X, VIRTUAL_CANVAS_Y);
        glVertex2f(            0.0f, VIRTUAL_CANVAS_Y);
    glEnd();

    //--Clean.
    glEnable(GL_TEXTURE_2D);
    glColor3f(1.0f, 1.0f, 1.0f);
}
void AdventureDebug::Render()
{
    //--Setup.
    if(!Images.mIsReady) return;

    //--Get the font size.
    float cScale = 1.0f;
    float tFontSize = Images.Data.rMenuFont->GetTextHeight() * cScale;

    //--Backing overlay.
    RenderOverlay();

    //--Positions
    float cIndent = 4.0f;
    float cLf0 = 8.0f;
    float cLf1 = cLf0 + cIndent;
    float cRt0 = VIRTUAL_CANVAS_X * 0.666666f;
    //float cRt1 = cRt0 + cIndent;
    float cTop = 8.0f;

    //--Main menu.
    if(mMenuMode == ADM_MODE_MAIN)
    {
        Images.Data.rMenuFont->DrawText                 ( 8.0f, 8.0f + (tFontSize *  0.0f), 0, cScale, "Debug Menu");
        RenderText(ADM_OPT_MAIN_OPEN_WARP_SUBMENU,       12.0f, 8.0f + (tFontSize *  1.0f),    cScale, "Warp To A Level");
        RenderText(ADM_OPT_MAIN_OPEN_SCENES_SUBMENU,     12.0f, 8.0f + (tFontSize *  2.0f),    cScale, "Fire A Cutscene");
        RenderText(ADM_OPT_MAIN_OPEN_SCRIPTVARS_SUBMENU, 12.0f, 8.0f + (tFontSize *  3.0f),    cScale, "View Script Variables");
        RenderText(ADM_OPT_MAIN_OPEN_ENEMIES_SUBMENU,    12.0f, 8.0f + (tFontSize *  4.0f),    cScale, "Field Enemy Menu");
        RenderText(ADM_OPT_MAIN_OPEN_PARTY_SUBMENU,      12.0f, 8.0f + (tFontSize *  5.0f),    cScale, "Party Menu");
        RenderText(ADM_OPT_MAIN_OPEN_FORM_SUBMENU,       12.0f, 8.0f + (tFontSize *  6.0f),    cScale, "Change Character Form");
        RenderText(ADM_OPT_MAIN_OPEN_TOPIC_SUBMENU,      12.0f, 8.0f + (tFontSize *  7.0f),    cScale, "Edit Topics");
        RenderText(ADM_OPT_MAIN_CLEAR_OVERLAY,           12.0f, 8.0f + (tFontSize *  8.0f),    cScale, "Clear Overlay");
        RenderText(ADM_OPT_MAIN_OPEN_SCRIPT_HUNTER,      12.0f, 8.0f + (tFontSize *  9.0f),    cScale, "Script Hunter");
        RenderText(ADM_OPT_MAIN_OPEN_BUILDS,             12.0f, 8.0f + (tFontSize * 10.0f),    cScale, "Automated Builder");
        RenderText(ADM_OPT_MAIN_CLOSE_THIS_MENU,         12.0f, 8.0f + (tFontSize * 11.0f),    cScale, "Close This Menu");

        //--Top right. Render the name of the current room, assuming one exists.
        AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
        if(rActiveLevel)
        {
            Images.Data.rMenuFont->DrawText(VIRTUAL_CANVAS_X - 32, 8.0f, SUGARFONT_RIGHTALIGN_X, cScale, rActiveLevel->GetName());
        }

        //--Render the version string below that.
        GLOBAL *rGlobal = Global::Shared();
        Images.Data.rMenuFont->DrawText(VIRTUAL_CANVAS_X - 32, 8.0f + (tFontSize *  1.0f), SUGARFONT_RIGHTALIGN_X, 1.0f, rGlobal->gVersionString);
    }
    //--Warp menu.
    else if(mMenuMode == ADM_MODE_WARP)
    {
        //--Header.
        Images.Data.rMenuFont->DrawText(8.0f, 8.0f + (tFontSize * 0.0f), 0, cScale, "Select Warp Destination");

        //--Iterate across the current warp listing.
        int i = 0;
        WarpListing *rListing = (WarpListing *)rActiveListing->PushIterator();
        while(rListing)
        {
            //--Render.
            RenderText(i, 12.0f, 8.0f + (tFontSize * 1.0f) + (tFontSize * i), cScale, rActiveListing->GetIteratorName());

            //--Next.
            i ++;
            rListing = (WarpListing *)rActiveListing->AutoIterate();
        }
    }
    //--Scenes menu.
    else if(mMenuMode == ADM_MODE_SCENES)
    {
        Images.Data.rMenuFont->DrawText(8.0f, 8.0f + (tFontSize * 0.0f), 0, cScale, "Select Cutscene");
        for(int i = 0; i < xScenesTotal; i ++)
        {
            RenderText(i, 12.0f, 8.0f + (tFontSize * 1.0f) + (tFontSize * i), cScale, xSceneListing[i]);
        }
    }
    //--Script variables.
    else if(mMenuMode == ADM_MODE_SCRIPTVARS)
    {
        //--Header:
        Images.Data.rMenuFont->DrawText(8.0f, 8.0f + (tFontSize * 0.0f), 0, cScale, "Script Variable Listing");

        //--Render.
        for(int i = mScriptVarOffset; i < mScriptVarsTotal; i ++)
        {
            RenderText(i, 12.0f, 8.0f + (tFontSize * 1.0f) + (tFontSize * (i-mScriptVarOffset)), cScale, mScriptVarNames[i]);
            RenderText(i, VIRTUAL_CANVAS_X * 0.666666f, 8.0f + (tFontSize * 1.0f) + (tFontSize * (i-mScriptVarOffset)), cScale, mScriptVarValues[i]);
        }

        //--In case of no variables...
        if(mScriptVarsTotal < 1)
        {
            glColor3f(1.0f, 0.0f, 0.0f);
            Images.Data.rMenuFont->DrawText(12.0f, 8.0f + (tFontSize * 1.0f), 0, cScale, "No script variables available.");
            glColor3f(1.0f, 1.0f, 1.0f);
        }
    }
    //--Enemies.
    else if(mMenuMode == ADM_MODE_ENEMIES)
    {
        Images.Data.rMenuFont->DrawText        ( 8.0f, 8.0f + (tFontSize * 0.0f), 0, cScale, "Field Enemy Menu");
        RenderText(ADM_OPT_ENEMIES_WIPE_FIELD,  12.0f, 8.0f + (tFontSize * 1.0f),    cScale, "Wipe Field Enemies");
        RenderText(ADM_OPT_ENEMIES_RESPAWN_ALL, 12.0f, 8.0f + (tFontSize * 2.0f),    cScale, "Respawn All Enemies");
        if(!xCamouflageMode)
        {
            RenderText(ADM_OPT_ENEMIES_CAMOUFLAGE, 12.0f, 8.0f + (tFontSize * 3.0f),    cScale, "Activate Camouflage");
        }
        else
        {
            RenderText(ADM_OPT_ENEMIES_CAMOUFLAGE, 12.0f, 8.0f + (tFontSize * 3.0f),    cScale, "Deactivate Camouflage");
        }
        RenderText(ADM_OPT_ENEMIES_BACK,        12.0f, 8.0f + (tFontSize * 4.0f),    cScale, "Back");
    }
    //--Party.
    else if(mMenuMode == ADM_MODE_PARTY)
    {
        Images.Data.rMenuFont->DrawText       ( 8.0f, 8.0f + (tFontSize * 0.0f), 0, cScale, "Party Menu");
        RenderText(ADM_OPT_PARTY_FULL_RESTORE, 12.0f, 8.0f + (tFontSize * 1.0f),    cScale, "Restore Party");
        RenderText(ADM_OPT_PARTY_GIVE_EXPA,    12.0f, 8.0f + (tFontSize * 2.0f),    cScale, "Give 200 EXP");
        RenderText(ADM_OPT_PARTY_GIVE_EXPB,    12.0f, 8.0f + (tFontSize * 3.0f),    cScale, "Give 2000 EXP");
        RenderText(ADM_OPT_PARTY_GIVE_EXPC,    12.0f, 8.0f + (tFontSize * 4.0f),    cScale, "Give 20000 EXP");
        RenderText(ADM_OPT_PARTY_GIVE_CASH,    12.0f, 8.0f + (tFontSize * 5.0f),    cScale, "Give 1000 Platina");
        RenderText(ADM_OPT_PARTY_GIVE_CRAFTING,12.0f, 8.0f + (tFontSize * 6.0f),    cScale, "Give 10 Crafting");
        RenderText(ADM_OPT_PARTY_BACK,         12.0f, 8.0f + (tFontSize * 7.0f),    cScale, "Back");
    }
    //--Forms.
    else if(mMenuMode == ADM_MODE_FORMS)
    {
        Images.Data.rMenuFont->DrawText(8.0f, 8.0f + (tFontSize * 0.0f), 0, cScale, "Select Character/Form");
        for(int i = 0; i < xFormsTotal; i ++)
        {
            RenderText(i, 12.0f, 8.0f + (tFontSize * 1.0f) + (tFontSize * i), cScale, xFormListing[i]);
        }
    }
    //--Topics.
    else if(mMenuMode == ADM_MODE_TOPICS)
    {
        char tBuffer[32];
        Images.Data.rMenuFont->DrawText(8.0f, 8.0f + (tFontSize * 0.0f), 0, cScale, "Topic Listing");
        for(int i = mTopicOffset; i < mTopicsTotal; i ++)
        {
            sprintf(tBuffer, "%i", mTopicValues[i]);
            RenderText(i, 12.0f, 8.0f + (tFontSize * 1.0f) + (tFontSize * (i-mTopicOffset)), cScale, mrTopicNames[i]);
            RenderText(i, VIRTUAL_CANVAS_X * 0.666666f, 8.0f + (tFontSize * 1.0f) + (tFontSize * (i-mTopicOffset)), cScale, tBuffer);
        }
    }
    //--Script Hunter.
    else if(mMenuMode == ADM_MODE_SCRIPTHUNTER)
    {
        int i = 0;
        Images.Data.rMenuFont->DrawText(8.0f, 8.0f + (tFontSize * 0.0f), 0, cScale, "Select a Directory Tree");
        const char *rPath = (const char *)xmScriptHunterPathsList->PushIterator();
        while(rPath)
        {
            //--Render.
            RenderText(i, 12.0f, 8.0f + (tFontSize * (i+1)), cScale, rPath);

            //--Next.
            i ++;
            rPath = (const char *)xmScriptHunterPathsList->AutoIterate();
        }
    }
    //--Builds
    else if(mMenuMode == ADM_MODE_BUILDS)
    {
        //--[Header]
        //--Setup.
        int i = 0;
        float tYPosition = cTop + (tFontSize * 1.0f);

        //--Header.
        Images.Data.rMenuFont->DrawText(cLf0, cTop + (tFontSize * 0.0f), 0, cScale, "Select a Build Profile. Press Cancel to Exit.");

        //--[First Option]
        //--First option is emulation.
        RenderText(i, cLf1, tYPosition, cScale, "Emulation Mode: ");
        if(mIsEmulation)
            RenderText(i, cRt0, tYPosition, cScale, "On");
        else
            RenderText(i, cRt0, tYPosition, cScale, "Off");

        //--Increment.
        i++;
        tYPosition = tYPosition + tFontSize;

        //--[Second Option]
        //--Second option is which file to write to.
        RenderText(i, cLf1, tYPosition, cScale, "File Output: ");
        if(mBuildFile == ADM_BUILD_FILE_NONE)
            RenderText(i, cRt0, tYPosition, cScale, "Null");
        else if(mBuildFile == ADM_BUILD_FILE_STDERR)
            RenderText(i, cRt0, tYPosition, cScale, "Console");
        else
            RenderText(i, cRt0, tYPosition, cScale, "Hard Drive");

        //--Increment.
        i++;
        tYPosition = tYPosition + tFontSize;

        //--[List of Builds]
        //--Get the build list.
        SugarLinkedList *rBuildList = BuildPackage::FetchBuildList();

        //--Names of all builds detected.
        BuildPackage *rPackage = (BuildPackage *)rBuildList->PushIterator();
        while(rPackage)
        {
            //--Render.
            RenderText(i, cLf1, tYPosition, cScale, rBuildList->GetIteratorName());

            //--Next.
            i ++;
            tYPosition = tYPosition + tFontSize;
            rPackage = (BuildPackage *)rBuildList->AutoIterate();
        }
    }
}
void AdventureDebug::RenderText(int pCursorValue, float pX, float pY, float pScale, const char *pText)
{
    //--Subroutine to render text. If the text in question is the selection option, then it renders
    //  a different color so the player can see their cursor.
    if(!pText) return;
    if(pCursorValue == mMenuCursor) glColor3f(1.0f, 0.0f, 1.0f);
    Images.Data.rMenuFont->DrawText(pX, pY, 0, pScale, pText);
    if(pCursorValue == mMenuCursor) glColor3f(1.0f, 1.0f, 1.0f);
}

//======================================= Pointer Routing =========================================
//====================================== Static Functions =========================================
//--Worker function: Allocates space using shared variables. All of the below static functions do
//  the same thing so we may as well optimize this a big.
void AllocateSpaceIn(int pSlots, int &sSlotCount, char **&sArray)
{
    //--Deallocate.
    for(int i = 0; i < sSlotCount; i ++) free(sArray[i]);
    free(sArray);

    //--Reset.
    sSlotCount = 0;
    sArray = NULL;
    if(pSlots < 1) return;

    //--Allocate.
    sSlotCount = pSlots;
    SetMemoryData(__FILE__, __LINE__);
    sArray = (char **)starmemoryalloc(sizeof(char *) * sSlotCount);

    //--Defaults.
    for(int i = 0; i < sSlotCount; i ++)
    {
        sArray[i] = InitializeString("\0");
    }
}

void AdventureDebug::AllocateWarpDestinations(int pTotal)
{
    //--Allocates space for destinations to warp to. Not all maps are meant for warping, some are
    //  exclusive for cutscenes!
    AllocateSpaceIn(pTotal, xWarpDestinationsTotal, xWarpDestinations);
}
void AdventureDebug::SetWarpDestination(int pSlot, const char *pPath)
{
    //--Sets a given warp destination. Should be the name of the containing folder.
    if(pSlot < 0 || pSlot >= xWarpDestinationsTotal || !pPath) return;
    ResetString(xWarpDestinations[pSlot], pPath);
}
void AdventureDebug::AllocateSceneListings(int pTotal)
{
    //--Allocates space for cutscene handlers. Same as the warp handler above.
    AllocateSpaceIn(pTotal, xScenesTotal, xSceneListing);
}
void AdventureDebug::SetSceneListing(int pSlot, const char *pPath)
{
    //--Sets a given cutscene handler. Should be the name of the containing folder.
    if(pSlot < 0 || pSlot >= xScenesTotal || !pPath) return;
    ResetString(xSceneListing[pSlot], pPath);
}
void AdventureDebug::AllocateFormHandlers(int pTotal)
{
    //--Form handlers, which are what does the low-level changing of the species of a party member.
    //  Can be accessed from the debug menu or scripts.
    AllocateSpaceIn(pTotal, xFormsTotal, xFormListing);
}
void AdventureDebug::SetFormHandler(int pSlot, const char *pPath)
{
    //--Sets a given form handler. Typically in the format of "[Charactername]/Form_[Formname].lua".
    if(pSlot < 0 || pSlot >= xFormsTotal || !pPath) return;
    ResetString(xFormListing[pSlot], pPath);
}

//========================================= Lua Hooking ===========================================
void AdventureDebug::HookToLuaState(lua_State *pLuaState)
{
    /* ADebug_SetProperty("Warp Destinations Total", iAmount) (Static)
       ADebug_SetProperty("Warp Destination", iSlot, sPath) (Static)
       ADebug_SetProperty("Cutscenes Total", iAmount) (Static)
       ADebug_SetProperty("Cutscene Path", iSlot, sPath) (Static)
       ADebug_SetProperty("Forms Total", iAmount) (Static)
       ADebug_SetProperty("Form Path", iSlot, sPath) (Static)
       ADebug_SetProperty("Clear Script Hunter") (Static)
       ADebug_SetProperty("Add Script Hunter Path", sPath) (Static)
       Sets the requested property in the AdventureDebug menu. These are all static calls. */
    lua_register(pLuaState, "ADebug_SetProperty", &Hook_ADebug_SetProperty);
}

//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
int Hook_ADebug_SetProperty(lua_State *L)
{
    //ADebug_SetProperty("Warp Destinations Total", iAmount) (Static)
    //ADebug_SetProperty("Warp Destination", iSlot, sPath) (Static)
    //ADebug_SetProperty("Cutscenes Total", iAmount) (Static)
    //ADebug_SetProperty("Cutscene Path", iSlot, sPath) (Static)
    //ADebug_SetProperty("Forms Total", iAmount) (Static)
    //ADebug_SetProperty("Form Path", iSlot, sPath) (Static)
    //ADebug_SetProperty("Clear Script Hunter") (Static)
    //ADebug_SetProperty("Add Script Hunter Path", sPath) (Static)

    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("ADebug_SetProperty");

    //--Switching.
    const char *rSwitchType = lua_tostring(L, 1);

    //--Allocate space for the warp paths.
    if(!strcasecmp(rSwitchType, "Warp Destinations Total") && tArgs == 2)
    {
        AdventureDebug::AllocateWarpDestinations(lua_tointeger(L, 2));
    }
    //--Set a warp path.
    else if(!strcasecmp(rSwitchType, "Warp Destination") && tArgs == 3)
    {
        AdventureDebug::SetWarpDestination(lua_tointeger(L, 2), lua_tostring(L, 3));
    }
    //--Allocate space for cutscene paths.
    else if(!strcasecmp(rSwitchType, "Cutscenes Total") && tArgs == 2)
    {
        AdventureDebug::AllocateSceneListings(lua_tointeger(L, 2));
    }
    //--Set a cutscene path.
    else if(!strcasecmp(rSwitchType, "Cutscene Path") && tArgs == 3)
    {
        AdventureDebug::SetSceneListing(lua_tointeger(L, 2), lua_tostring(L, 3));
    }
    //--Allocate space for form handler paths.
    else if(!strcasecmp(rSwitchType, "Forms Total") && tArgs == 2)
    {
        AdventureDebug::AllocateFormHandlers(lua_tointeger(L, 2));
    }
    //--Set a form handler path.
    else if(!strcasecmp(rSwitchType, "Form Path") && tArgs == 3)
    {
        AdventureDebug::SetFormHandler(lua_tointeger(L, 2), lua_tostring(L, 3));
    }
    //--Clear the script hunter path list.
    else if(!strcasecmp(rSwitchType, "Clear Script Hunter") && tArgs == 1)
    {
        AdventureDebug::xmScriptHunterPathsList->ClearList();
    }
    //--Add a new path to the script hunter.
    else if(!strcasecmp(rSwitchType, "Add Script Hunter Path") && tArgs == 2)
    {
        AdventureDebug::xmScriptHunterPathsList->AddElement("X", InitializeString(lua_tostring(L, 2)), &FreeThis);
    }
    //--Error.
    else
    {
        LuaPropertyError("ADebug_SetProperty", rSwitchType, tArgs);
    }

    //--Finish up.
    return 0;
}
