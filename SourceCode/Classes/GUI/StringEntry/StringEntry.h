//--[StringEntry]
//--UI object which allows the player to input a standardized string. This object allows mouseclick
//  input, controller-style input, and typing. It also screens for special characters so it remains
//  standard ASCII!

#pragma once

//--[Includes]
#include "Definitions.h"
#include "Structures.h"

//--[Local Structures]
//--[Local Definitions]
#define SE_MAXLEN 256

#define SE_BTN_A 0
#define SE_BTN_B 1
#define SE_BTN_C 2
#define SE_BTN_D 3
#define SE_BTN_E 4
#define SE_BTN_F 5
#define SE_BTN_G 6
#define SE_BTN_H 7
#define SE_BTN_I 8
#define SE_BTN_J 9
#define SE_BTN_K 10
#define SE_BTN_L 11
#define SE_BTN_M 12
#define SE_BTN_N 13
#define SE_BTN_O 14
#define SE_BTN_P 15
#define SE_BTN_Q 16
#define SE_BTN_R 17
#define SE_BTN_S 18
#define SE_BTN_T 19
#define SE_BTN_U 20
#define SE_BTN_V 21
#define SE_BTN_W 22
#define SE_BTN_X 23
#define SE_BTN_Y 24
#define SE_BTN_Z 25
#define SE_BTN_1 26
#define SE_BTN_2 27
#define SE_BTN_3 28
#define SE_BTN_4 29
#define SE_BTN_5 30
#define SE_BTN_6 31
#define SE_BTN_7 32
#define SE_BTN_8 33
#define SE_BTN_9 34
#define SE_BTN_0 35
#define SE_BTN_SHIFT 36
#define SE_BTN_SPACE 37
#define SE_BTN_DONE 38
#define SE_BTN_BACKSPACE 39
#define SE_BTN_CAPSLOCK 40
#define SE_BTN_COMMA 41
#define SE_BTN_PERIOD 42
#define SE_BTN_SLASH 43
#define SE_BTN_MINUS 44
#define SE_BTN_EQUALS 45
#define SE_BTN_BACKSLASH 46
#define SE_BTN_GRAVE 47
#define SE_BTN_SEMICOLON 48
#define SE_BTN_QUOTE 49
#define SE_BTN_LBRACE 50
#define SE_BTN_RBRACE 51
#define SE_BTN_CANCEL 52
#define SE_BTN_TOTAL 53

//--[Classes]
class StringEntry
{
    private:
    //--System
    bool mIsComplete;
    bool mIsCancelled;
    char *mPromptString;

    //--String
    int mTimer;
    char mCurrentString[SE_MAXLEN];

    //--Inputs
    int mHighlightedButton;
    bool mIsShifted;
    bool mIsCapsLocked;
    int mPressTimers[SE_BTN_TOTAL];
    TwoDimensionReal mButtons[SE_BTN_TOTAL];
    char mBtnStrings[SE_BTN_TOTAL][16];
    int mRefireTimer;

    //--Images
    struct
    {
        bool mIsReady;
        struct
        {
            //--Fonts
            SugarFont *rHeaderFont;
            SugarFont *rMainlineFont;

            //--Images
            SugarBitmap *rRenderBase;
            SugarBitmap *rRenderPressed;
        }Data;
    }Images;

    protected:

    public:
    //--System
    StringEntry();
    ~StringEntry();

    //--Public Variables
    bool mIgnoreMouse;

    //--Property Queries
    bool IsComplete();
    bool IsCancelled();
    const char *GetString();

    //--Manipulators
    void SetCompleteFlag(bool pFlag);
    void SetPromptString(const char *pString);
    void SetString(const char *pString);

    //--Core Methods
    private:
    //--Private Core Methods
    public:
    //--Update
    void Update();

    //--File I/O
    //--Drawing
    void Render();
    void Render(float pOpacity);

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions

