//--Base
#include "PairanormalMenu.h"

//--Classes
#include "PairanormalDialogue.h"
#include "PairanormalLevel.h"
#include "ScriptHunter.h"
#include "PairanormalSettingsMenu.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarFont.h"
#include "SugarLinkedList.h"

//--Definitions
#include "Global.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "DisplayManager.h"
#include "LuaManager.h"
#include "MapManager.h"
#include "SaveManager.h"

//=========================================== System ==============================================
PairanormalMenu::PairanormalMenu()
{
    //--[RootObject]
    //--System
    mType = POINTER_TYPE_PAIRANORMALMENU;

    //--[IRenderable]
    //--System

    //--[RootLevel]
    //--System

    //--[PairanormalMenu]
    //--System
    mAllSavefilesExist = false;
    mAnySavefilesExist = false;
    mLevelSelectProgress = 0;
    mSpeedupProgress = 0;
    mSkipToTwoProgress = 0;
    mScriptHunterProgress = 0;
    mJayNoMoreProgress = 0;
    mMouseX = 0.0f;
    mMouseY = 0.0f;

    //--Mode
    mCurrentMode = PM_MODE_DISCLAIMER;
    if(Global::Shared()->gSkipPairanormalDisclaimer) mCurrentMode = PM_MODE_MAINMENU;
    Global::Shared()->gSkipPairanormalDisclaimer = true;

    //--Background
    mTitleFloatTimer = 0;
    rCurrentBackground = NULL;

    //--Buttons
    mButtonHighlight = -1;
    memset(mMainBtnDim, 0, sizeof(TwoDimensionReal) * PMBTN_TOTAL);
    memset(mGalleryBtnDim, 0, sizeof(TwoDimensionReal) * PMBTN_GAL_TOTAL);
    memset(mLoadingBtnDim, 0, sizeof(TwoDimensionReal) * PMBTN_LOAD_TOTAL);
    memset(mDisclaimerBtnDim, 0, sizeof(TwoDimensionReal) * PMBTN_DIS_TOTAL);

    //--Disclaimer
    mDisclaimerHighlight = -1;

    //--Gallery Paths
    mGalleryFadeTimer = 0;
    mCurrentGalleryPage = 0;
    mFocusOnImage = -1;
    mGalleryPathsTotal = 0;
    mGalleryPaths = NULL;
    memset(rGalleryImages, 0, sizeof(void *) * GALLERY_VISIBLE_IMAGES);

    //--Loading Screen
    mIsFadingToBlack = false;
    mFadeToBlackTimer = 0;
    mLoadFadeTimer = 0;
    mLoadPackSelected = -1;
    memset(mLoadPacks, 0, sizeof(LoadPack) * LOAD_PACKS_MAX);

    //--Images
    memset(&Images, 0, sizeof(Images));

    //--[Construction]
    //--Load image data.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();

    //--Fonts
    Images.Data.rSystemFont = (SugarFont *)rDataLibrary->GetEntry("Root/Fonts/System/TTF/Swagger20");
    Images.Data.rDisclaimerFont = (SugarFont *)rDataLibrary->GetEntry("Root/Fonts/System/TTF/Swagger40");
    Images.Data.rHeadingFont = (SugarFont *)rDataLibrary->GetEntry("Root/Fonts/System/TTF/Swagger60");

    //--Icons
    Images.Data.rIconBack = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Common/Back");
    Images.Data.rIconClose = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Common/Close");
    Images.Data.rIconSettings = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Common/Settings");

    //--Title Card.
    Images.Data.rTitleCard = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Title/TitleCard");

    //--Boxes
    Images.Data.rBoxBlack = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Title/BoxBlack");
    Images.Data.rBoxWhite = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Title/BoxWhite");

    //--Loading
    Images.Data.rLoadBacking         = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/LoadMenu/Backing");
    Images.Data.rLoadDeleteBtn       = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/LoadMenu/DeleteBtn");
    Images.Data.rLoadHeader          = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/LoadMenu/Header");
    Images.Data.rLoadPencil          = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/LoadMenu/Pencil");
    Images.Data.rLoadPlayBtn         = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/LoadMenu/PlayBtn");
    Images.Data.rLoadSelectedBacking = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/LoadMenu/SelectedBacking");

    //--Gallery
    Images.Data.rGalleryBacking = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Title/GalleryBlackBack");
    Images.Data.rImageBacking   = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Title/GalleryBack");
    Images.Data.rBtnIncrement   = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Title/GalleryRgt");
    Images.Data.rBtnDecrement   = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Title/GalleryLft");

    //--Disclaimer
    Images.Data.rDisclaimer                      = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Derived/Disclaimer");
    Images.Data.rDisclaimerBtnReadySelected      = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Derived/DisclaimerBtn|Ready|Selected");
    Images.Data.rDisclaimerBtnReadyUnselected    = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Derived/DisclaimerBtn|Ready|Unselected");
    Images.Data.rDisclaimerBtnNotReadySelected   = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Derived/DisclaimerBtn|NotReady|Selected");
    Images.Data.rDisclaimerBtnNotReadyUnselected = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Derived/DisclaimerBtn|NotReady|Unselected");

    //--Verify.
    Images.mIsReady = VerifyStructure(&Images.Data, sizeof(Images.Data), sizeof(void *));

    //--Standard background.
    rCurrentBackground = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Backgrounds/All/SchoolExterior");
    rGalleryBackground = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Backgrounds/All/SchoolExterior");

    //--Position the buttons.
    mMainBtnDim[PMBTN_CONTINUE].SetWH(82.0f, 336.0f, 452.0f, 65.0f);
    mMainBtnDim[PMBTN_NEWGAME].SetWH( 82.0f, 426.0f, 452.0f, 65.0f);
    mMainBtnDim[PMBTN_GALLERY].SetWH( 82.0f, 519.0f, 452.0f, 65.0f);
    mMainBtnDim[PMBTN_QUIT].SetWH(     2.0f,   2.0f,  30.0f, 27.0f);
    mMainBtnDim[PMBTN_SETTINGS].SetWH(41.0f,   3.0f,  25.0f, 25.0f);

    //--Gallery buttons.
    float cGalWid = 228.0f;
    float cGalHei = 158.0f;
    float cGalSpc = 25.0f;
    float cGalX0 = 315.0f;
    float cGalX1 = cGalX0 + cGalWid + cGalSpc;
    float cGalX2 = cGalX1 + cGalWid + cGalSpc;
    float cGalY0 = 130.0f;
    float cGalY1 = cGalY0 + cGalHei + cGalSpc;
    float cGalY2 = cGalY1 + cGalHei + cGalSpc;
    mGalleryBtnDim[PMBTN_GAL_BACK].SetWH(        37.0f,   2.0f,   33.0f,   28.0f);
    mGalleryBtnDim[PMBTN_GAL_SETTINGS].SetWH(    77.0f,  14.0f,   32.0f,   28.0f);
    mGalleryBtnDim[PMBTN_GAL_PICTURE_0].SetWH(  cGalX0, cGalY0, cGalWid, cGalHei);
    mGalleryBtnDim[PMBTN_GAL_PICTURE_1].SetWH(  cGalX1, cGalY0, cGalWid, cGalHei);
    mGalleryBtnDim[PMBTN_GAL_PICTURE_2].SetWH(  cGalX2, cGalY0, cGalWid, cGalHei);
    mGalleryBtnDim[PMBTN_GAL_PICTURE_3].SetWH(  cGalX0, cGalY1, cGalWid, cGalHei);
    mGalleryBtnDim[PMBTN_GAL_PICTURE_4].SetWH(  cGalX1, cGalY1, cGalWid, cGalHei);
    mGalleryBtnDim[PMBTN_GAL_PICTURE_5].SetWH(  cGalX2, cGalY1, cGalWid, cGalHei);
    mGalleryBtnDim[PMBTN_GAL_PICTURE_6].SetWH(  cGalX0, cGalY2, cGalWid, cGalHei);
    mGalleryBtnDim[PMBTN_GAL_PICTURE_7].SetWH(  cGalX1, cGalY2, cGalWid, cGalHei);
    mGalleryBtnDim[PMBTN_GAL_PICTURE_8].SetWH(  cGalX2, cGalY2, cGalWid, cGalHei);
    mGalleryBtnDim[PMBTN_GAL_PAGEDOWN].SetWH(   681.0f, 683.0f,   71.0f,   71.0f);
    mGalleryBtnDim[PMBTN_GAL_PAGEUP].SetWH(     768.0f, 683.0f,   71.0f,   71.0f);
    mGalleryBtnDim[PMBTN_GAL_CLEARFOCUS].SetWH(1038.0f, 659.0f,   54.0f,   52.0f);

    //--Loading Buttons
    mLoadingBtnDim[PMBTN_LOAD_BACK].SetWH(37.0f, 2.0f, 33.0f, 28.0f);
    mLoadingBtnDim[PMBTN_LOAD_HIGHLIGHT_0].SetWH(301.0f, 171.0f, 268.0f, 302.0f);
    mLoadingBtnDim[PMBTN_LOAD_HIGHLIGHT_1].SetWH(569.0f, 171.0f, 253.0f, 302.0f);
    mLoadingBtnDim[PMBTN_LOAD_HIGHLIGHT_2].SetWH(822.0f, 171.0f, 260.0f, 302.0f);
    mLoadingBtnDim[PMBTN_LOAD_PLAY].SetWH(417.0f, 549.0f, 393.0f, 91.0f);
    mLoadingBtnDim[PMBTN_LOAD_DELETE].SetWH(860.0f, 557.0f, 71.0f, 71.0f);
    mLoadingBtnDim[PMBTN_LOAD_MOD_0].SetWH(mLoadingBtnDim[PMBTN_LOAD_HIGHLIGHT_0].mRgt - 25.0f, 449.0f, 25.0f, 25.0f);
    mLoadingBtnDim[PMBTN_LOAD_MOD_1].SetWH(mLoadingBtnDim[PMBTN_LOAD_HIGHLIGHT_1].mRgt - 25.0f, 449.0f, 25.0f, 25.0f);
    mLoadingBtnDim[PMBTN_LOAD_MOD_2].SetWH(mLoadingBtnDim[PMBTN_LOAD_HIGHLIGHT_2].mRgt - 25.0f, 449.0f, 25.0f, 25.0f);

    //--Disclaimer buttons.
    mDisclaimerBtnDim[PMBTN_DIS_READY].SetWH(   376.0f, 597.0f, 520.0f, 136.0f);
    mDisclaimerBtnDim[PMBTN_DIS_NOTREADY].SetWH(421.0f, 504.0f, 520.0f,  93.0f);

    //--[Music]
    AudioManager::Fetch()->PlayMusic("Theme");

    //--[File Check]
    //--Check if a savefile exists.
    mAnySavefilesExist = false;
    mAllSavefilesExist = true;

    //--Path.
    const char *rPairanormalPath = DataLibrary::GetGamePath("Root/Paths/System/Startup/sPairanormalPath");

    //--Run.
    for(int i = 0; i < 3; i ++)
    {
        //--Name.
        char tSaveNameBuf[80];
        sprintf(tSaveNameBuf, "%s/../../Saves/Save%02i.slf", rPairanormalPath, i);

        //--Check for existence.
        FILE *fCheckFile = fopen(tSaveNameBuf, "rb");
        if(fCheckFile)
        {
            mAnySavefilesExist = true;
            fclose(fCheckFile);
        }
        else
        {
            mAllSavefilesExist = false;
        }
    }

    //--[Settings Menu]
    //--Instantiate the settings menu. This occurs when it is first fetched.
    PairanormalSettingsMenu::Fetch();
    PairanormalDialogue::Fetch();
}
PairanormalMenu::~PairanormalMenu()
{
    for(int i = 0; i < mGalleryPathsTotal; i ++) free(mGalleryPaths[i]);
    free(mGalleryPaths);
}
void PairanormalMenu::BootPairanormalFonts()
{
    //--When Pairanormal runs, it needs these fonts to function. This function boots the fonts and registers
    //  them to the DataLibrary. If they already exist, they are ignored.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    rDataLibrary->AddPath("Root/Fonts/System/TTF/");

    //--Determine the path needed for the files. All fonts are in Path/Datafiles/Font.ext
    const char *rPairanormalPath = DataLibrary::GetGamePath("Root/Paths/System/Startup/sPairanormalPath");

    //--Buffer setup.
    char tPathBuffer[512];
    fprintf(stderr, "Booting pairanormal fonts: %s\n", rPairanormalPath);

    //--Swagger 20
    if(!rDataLibrary->DoesEntryExist("Root/Fonts/System/TTF/Swagger20"))
    {
        SugarFont *nSwaggerFont20 = new SugarFont();
        sprintf(tPathBuffer, "%s/Datafiles/Swagger.ttf", rPairanormalPath);
        nSwaggerFont20->ConstructWith(tPathBuffer, 20, 0);
        rDataLibrary->RegisterPointer("Root/Fonts/System/TTF/Swagger20", nSwaggerFont20, &RootObject::DeleteThis);
    }

    //--Swagger 40
    if(!rDataLibrary->DoesEntryExist("Root/Fonts/System/TTF/Swagger40"))
    {
        SugarFont *nSwaggerFont40 = new SugarFont();
        sprintf(tPathBuffer, "%s/Datafiles/Swagger.ttf", rPairanormalPath);
        nSwaggerFont40->ConstructWith(tPathBuffer, 40, 0);
        rDataLibrary->RegisterPointer("Root/Fonts/System/TTF/Swagger40", nSwaggerFont40, &RootObject::DeleteThis);
    }

    //--Swagger 60
    if(!rDataLibrary->DoesEntryExist("Root/Fonts/System/TTF/Swagger60"))
    {
        SugarFont *nSwaggerFont60 = new SugarFont();
        sprintf(tPathBuffer, "%s/Datafiles/Swagger.ttf", rPairanormalPath);
        nSwaggerFont60->ConstructWith(tPathBuffer, 60, 0);
        rDataLibrary->RegisterPointer("Root/Fonts/System/TTF/Swagger60", nSwaggerFont60, &RootObject::DeleteThis);
    }

    //--OpenDyslexicMono 40.
    if(!rDataLibrary->DoesEntryExist("Root/Fonts/System/TTF/DyslexicMono40"))
    {
        SugarFont *nDyslexicMono40 = new SugarFont();
        sprintf(tPathBuffer, "%s/Datafiles/DyslexicMono.otf", rPairanormalPath);
        nDyslexicMono40->ConstructWith(tPathBuffer, 40, 0);
        rDataLibrary->RegisterPointer("Root/Fonts/System/TTF/DyslexicMono40", nDyslexicMono40, &RootObject::DeleteThis);
    }

    //--Oxygen 40.
    if(!rDataLibrary->DoesEntryExist("Root/Fonts/System/TTF/Oxygen40"))
    {
        SugarFont *nOxygen40 = new SugarFont();
        sprintf(tPathBuffer, "%s/Datafiles/Oxygen-Regular.ttf", rPairanormalPath);
        nOxygen40->ConstructWith(tPathBuffer, 40, 0);
        rDataLibrary->RegisterPointer("Root/Fonts/System/TTF/Oxygen40", nOxygen40, &RootObject::DeleteThis);
    }
}

//--[Public Statics]
//--Causes the New Game button to boot with level select if true.
bool PairanormalMenu::xIsLevelSelectEnabled = false;

//--Decreases the length of pauses and greatly increases text speed.
bool PairanormalMenu::xSpeedupFlag = false;

//--Skips directly to Chapter 2 on New Game.
bool PairanormalMenu::xSkipToChapter2 = false;

//--Causes the rename prompt to come up when a game is loaded.
bool PairanormalMenu::xImmediateRename = false;

//====================================== Property Queries =========================================
//========================================= Manipulators ==========================================
//========================================= Core Methods ==========================================
void PairanormalMenu::RecheckFileExistence()
{
    //--Checks for the presence of the save files in the standard location, which is relative to the baseline
    //  path. Multiple installs could theoretically have multiple save blocks.
    mAllSavefilesExist = true;
    mAnySavefilesExist = false;

    //--Path base.
    const char *rPairanormalPath = DataLibrary::GetGamePath("Root/Paths/System/Startup/sPairanormalPath");

    //--Run.
    for(int i = 0; i < 3; i ++)
    {
        //--Name.
        char tSaveNameBuf[80];
        sprintf(tSaveNameBuf, "%s/../../Saves/Save%02i.slf", rPairanormalPath, i);

        //--Check for existence.
        FILE *fCheckFile = fopen(tSaveNameBuf, "rb");
        if(fCheckFile)
        {
            mAnySavefilesExist = true;
            fclose(fCheckFile);
        }
        else
        {
            mAllSavefilesExist = false;
        }
    }
}

//===================================== Private Core Methods ======================================
bool PairanormalMenu::HandleCheatCode(const char *pPressedKey, int &sProgressVar, int pMaxLength, const char *pCheatCode)
{
    //--Worker function, handles typing cheat codes. Returns true if the code was entered this pass,
    //  false if not. The sProgressVar will be updated if a correct key is pressed.
    if(!pPressedKey || !pCheatCode) return false;

    //--Downshift it if it's a letter. Numbers and other characters do not need modification.
    char tCharacter = pPressedKey[0];
    if(tCharacter >= 'A' && tCharacter <= 'Z')
    {
        tCharacter = tCharacter + 'a' - 'A';
    }

    //--Now check if it matches a key we need.
    if(tCharacter == pCheatCode[sProgressVar])
    {
        //--Increment.
        sProgressVar ++;

        //--Ending case.
        if(sProgressVar >= pMaxLength-1)
        {
            AudioManager::Fetch()->PlaySound("Special|CheatTone");
            return true;
        }
    }

    //--Cheat code not entered this tick.
    return false;
}

//============================================ Update =============================================
void PairanormalMenu::Update()
{
    //--[Documentation and Setup]
    //--Handles updates and controls. This is mostly a mouse-driven UI. This update handles the
    //  main menu, with all submenus being in their own .cc files.
    ControlManager *rControlManager = ControlManager::Fetch();
    mTitleFloatTimer ++;

    //--[Special]
    //--If this global flag is set, autorun to new game.
    if(Global::Shared()->gAutorunPairanormalNewGame)
    {
        //--If there is not path to the main files, fail here.
        Global::Shared()->gAutorunPairanormalNewGame = false;
        const char *rPairanormalPath = DataLibrary::GetGamePath("Root/Paths/System/Startup/sPairanormalPath");

        //--Delete the zero save file. Fail if there's no zero file. We don't need to delete anything.
        if(mLoadPacks[0].mCharacterName[0] != '\0')
        {
            //--Build a pathname.
            char tSaveNameBuf[80];
            sprintf(tSaveNameBuf, "%s/../../Saves/Save%02i.slf", rPairanormalPath, 0);

            //--Delete it.
            remove(tSaveNameBuf);

            //--Remove the loading pack's information so it can't be selected again.
            mLoadPacks[0].mCharacterName[0] = '\0';
            mLoadPackSelected = -1;
        }

        //--Determine the first open slot. It should always be slot 0, but it's here just in case the
        //  remove command fails.
        for(int i = 0; i < 3; i ++)
        {
            //--Name.
            char tSaveNameBuf[80];
            sprintf(tSaveNameBuf, "%s/../../Saves/Save%02i.slf", rPairanormalPath, i);

            //--Check for existence.
            FILE *fCheckFile = fopen(tSaveNameBuf, "rb");
            if(!fCheckFile)
            {
                ResetString(PairanormalLevel::xActiveGamePath, tSaveNameBuf);
                break;
            }
            else
            {
                fclose(fCheckFile);
            }
        }

        //--Start a new game.
        PairanormalLevel *nNewGameLevel = new PairanormalLevel();
        MapManager::Fetch()->ReceiveLevel(nNewGameLevel);
        PairanormalLevel::xPlayTime = 0;

        //--Execute the launcher script.
        char tBuffer[256];
        sprintf(tBuffer, "%s/Chapter 1/ZLaunch.lua", rPairanormalPath);
        LuaManager::Fetch()->ExecuteLuaFile(tBuffer);

        //--This object is now unstable. Return out.
        return;
    }

    //--[Settings Menu]
    PairanormalSettingsMenu *rSettingsMenu = PairanormalSettingsMenu::Fetch();
    if(rSettingsMenu->IsOpen())
    {
        rSettingsMenu->Update();
        return;
    }
    else
    {
        rSettingsMenu->NotUpdate();
    }

    //--[Disclaimer Handler]
    if(mCurrentMode == PM_MODE_DISCLAIMER)
    {
        UpdateDisclaimer();
        return;
    }
    //--[Gallery Handler]
    else if(mCurrentMode == PM_MODE_GALLERY)
    {
        UpdateGallery();
        return;
    }
    else if(mCurrentMode == PM_MODE_LOADING)
    {
        UpdateLoadMenu();
        return;
    }

    //--[Timers]
    //--Decrement these if their update is not running.
    if(mGalleryFadeTimer > 0) mGalleryFadeTimer --;
    if(mLoadFadeTimer    > 0) mLoadFadeTimer    --;

    //--[Cheat Code Handlers]
    //--Get codes.
    int tKeyboard, tMouse, tJoy;
    rControlManager->GetKeyPressCodes(tKeyboard, tMouse, tJoy);
    const char *rPressedKeyName = rControlManager->GetNameOfKeyIndex(tKeyboard);

    //--If the player hasn't activated level select, they can do so here.
    if(!xIsLevelSelectEnabled && HandleCheatCode(rPressedKeyName, mLevelSelectProgress, LEVEL_SELECT_LEN, LEVEL_SELECT_WORD))
    {
        xIsLevelSelectEnabled = true;
    }

    //--If the player hasn't activated speed up, handle that.
    if(!xSpeedupFlag && HandleCheatCode(rPressedKeyName, mSpeedupProgress, SPEEDUP_LEN, SPEEDUP_WORD))
    {
        xSpeedupFlag = true;
    }

    //--Immediately skip to chapter 2.
    if(!xSkipToChapter2 && HandleCheatCode(rPressedKeyName, mSkipToTwoProgress, SKIPTOTWO_LEN, SKIPTOTWO_WORD))
    {
        xSkipToChapter2 = true;
    }

    //--Jay No More, my friend! Rename your character after loading a game.
    if(!xImmediateRename && HandleCheatCode(rPressedKeyName, mJayNoMoreProgress, CHANGENAME_LEN, CHANGENAME_WORD))
    {
        xImmediateRename = true;
    }

    //--Script Hunter can always be activated, it resets after each use.
    if(HandleCheatCode(rPressedKeyName, mScriptHunterProgress, SCRIPTHUNTER_LEN, SCRIPTHUNTER_WORD))
    {
        //--Get the baseline path.
        const char *rPairanormalPath = DataLibrary::GetGamePath("Root/Paths/System/Startup/sPairanormalPath");

        //--Build and execute the ScriptHunter.
        ScriptHunter *tScriptHunter = new ScriptHunter();
        tScriptHunter->SetTargetDirectory(rPairanormalPath);
        tScriptHunter->Execute();
        delete tScriptHunter;

        //--Reset progress.
        mScriptHunterProgress = 0;
    }

    //--[Mouse Position]
    //--Get mouse location.
    float tMouseZ;
    rControlManager->GetMouseCoordsF(mMouseX, mMouseY, tMouseZ);
    int tOldHighlight = mButtonHighlight;
    mButtonHighlight = -1;

    //--Check which button the mouse is over.
    for(int i = 0; i < PMBTN_TOTAL; i ++)
    {
        if(mMainBtnDim[i].IsPointWithin(mMouseX, mMouseY))
        {
            //--Skip hidden buttons.
            if(!mAnySavefilesExist && (i == PMBTN_CONTINUE || i == PMBTN_GALLERY)) continue;

            //--Set.
            mButtonHighlight = i;

            //--Play the mouse-over sound effect.
            if(tOldHighlight != mButtonHighlight)
            {
                AudioManager::Fetch()->PlaySound("UI|MouseOver");
            }
            break;
        }
    }

    //--[Mouse Clicking]
    //--Player clicks the mouse at a location on screen.
    if(rControlManager->IsFirstPress("MouseLft"))
    {
        //--If the mouse is over the Continue button...
        if(mButtonHighlight == PMBTN_CONTINUE && mAnySavefilesExist)
        {
            //--SFX.
            AudioManager::Fetch()->PlaySound("UI|Select");

            //--Change mode.
            mCurrentMode = PM_MODE_LOADING;
            OpenLoadMenu();
        }
        //--New Game button...
        else if(mButtonHighlight == PMBTN_NEWGAME)
        {
            //--SFX.
            AudioManager::Fetch()->PlaySound("UI|Select");

            //--Get the execution path.
            const char *rPairanormalPath = DataLibrary::GetGamePath("Root/Paths/System/Startup/sPairanormalPath");

            //--If no empty save slots exist, fail.
            if(mAllSavefilesExist) return;

            //--Determine the first open slot.
            for(int i = 0; i < 3; i ++)
            {
                //--Name.
                char tSaveNameBuf[80];
                sprintf(tSaveNameBuf, "%s/../../Saves/Save%02i.slf", rPairanormalPath, i);

                //--Check for existence.
                FILE *fCheckFile = fopen(tSaveNameBuf, "rb");
                if(!fCheckFile)
                {
                    ResetString(PairanormalLevel::xActiveGamePath, tSaveNameBuf);
                    break;
                }
                else
                {
                    fclose(fCheckFile);
                }
            }

            //--Start a new game.
            PairanormalLevel *nNewGameLevel = new PairanormalLevel();
            MapManager::Fetch()->ReceiveLevel(nNewGameLevel);
            PairanormalLevel::xPlayTime = 0;

            //--Execute the launcher script.
            char tBuffer[256];
            sprintf(tBuffer, "%s/Chapter 1/ZLaunch.lua", rPairanormalPath);
            LuaManager::Fetch()->ExecuteLuaFile(tBuffer);

            //--This object is now unstable. Return out.
            return;
        }
        //--Gallery button....
        else if(mButtonHighlight == PMBTN_GALLERY && mAnySavefilesExist)
        {
            //--SFX.
            AudioManager::Fetch()->PlaySound("UI|Select");

            //--Load gallery data in case it changed.
            char tSaveBuf[256];
            const char *rPairanormalPath = DataLibrary::Fetch()->GetGamePath("Root/Paths/System/Startup/sPairanormalPath");
            sprintf(tSaveBuf, "%s/../../Saves/GalleryData.slf", rPairanormalPath);
            SaveManager::Fetch()->LoadPairanormalGallery(tSaveBuf);

            //--Now run the gallery controller. This will unlock the images.
            char tGalleryBuf[256];
            sprintf(tGalleryBuf, "%s/GalleryController.lua", rPairanormalPath);
            LuaManager::Fetch()->ExecuteLuaFile(tGalleryBuf);

            //--Change mode.
            mCurrentMode = PM_MODE_GALLERY;
            RefreshImagesByPage(0);
        }
        //--Quit button....
        else if(mButtonHighlight == PMBTN_QUIT)
        {
            //--SFX.
            AudioManager::Fetch()->PlaySound("UI|Select");

            Global::Shared()->gQuit = true;
        }
        //--Settings button....
        else if(mButtonHighlight == PMBTN_SETTINGS)
        {
            //--SFX.
            AudioManager::Fetch()->PlaySound("UI|Select");

            rSettingsMenu->Open();
        }
    }
}

//=========================================== File I/O ============================================
//=========================================== Drawing =============================================
void PairanormalMenu::AddToRenderList(SugarLinkedList *pRenderList)
{
    pRenderList->AddElement("X", this);
}
void PairanormalMenu::Render()
{
    //--[Documentation and Setup]
    //--Renders the main menu, or gallery menu, depending on which is currently set.
    MapManager::xHasRenderedMenus = true;
    if(!Images.mIsReady) return;

    //--Background does not render in demo mode.
    if(Global::Shared()->gAutorunPairanormalNewGame) return;

    //--[Disclaimer Handler]
    if(mCurrentMode == PM_MODE_DISCLAIMER)
    {
        RenderDisclaimer();
        return;
    }
    //--[Gallery Handler]
    else if(mCurrentMode == PM_MODE_GALLERY)
    {
    }
    //--[Loading Handler]
    else if(mCurrentMode == PM_MODE_LOADING)
    {
    }

    //--[Major Block]
    //--Only renders if the other parts are not at full.
    if(mGalleryFadeTimer < PM_FADE_TICKS && mLoadFadeTimer < PM_FADE_TICKS)
    {
        //--[Background]
        //--For now, just render normally. Blurring happens later.
        if(rCurrentBackground)
        {
            //--Determine scale and offset.
            float cUseScale = BG_SCALE;
            float cYOffset = BG_OFFSET_Y;
            if(PairanormalLevel::xIsLowResMode)
            {
                cUseScale = cUseScale * 2.0f;
                cYOffset = cYOffset * 0.5f;
            }

            //--Render.
            glScalef(cUseScale, cUseScale, 1.0f);
            rCurrentBackground->Draw(0, cYOffset);
            glScalef(1.0f / cUseScale, 1.0f / cUseScale, 1.0f);
        }

        //--Title Card.
        float cYPosition = 105.0f + (sinf(mTitleFloatTimer * 3.1415926f / 180.0f) * 4.9f);
        Images.Data.rTitleCard->Draw(323, cYPosition);

        //--[Buttons]
        //--Constants.
        float cTxtLftInd = 26.0f;
        float cTxtTopInd = 0.0f;
        float cTxtScale = 1.0f;

        //--Continue button.
        if(mAnySavefilesExist)
        {
            if(mButtonHighlight != PMBTN_CONTINUE)
            {
                Images.Data.rBoxBlack->Draw(mMainBtnDim[PMBTN_CONTINUE].mLft, mMainBtnDim[PMBTN_CONTINUE].mTop);
                Images.Data.rHeadingFont->DrawText(mMainBtnDim[PMBTN_CONTINUE].mLft + cTxtLftInd, mMainBtnDim[PMBTN_CONTINUE].mTop + cTxtTopInd, 0, cTxtScale, "Continue");
            }
            else
            {
                Images.Data.rBoxWhite->Draw(mMainBtnDim[PMBTN_CONTINUE].mLft, mMainBtnDim[PMBTN_CONTINUE].mTop);
                StarlightColor::SetMixer(0.0f, 0.0f, 0.0f, 1.0f);
                Images.Data.rHeadingFont->DrawText(mMainBtnDim[PMBTN_CONTINUE].mLft + cTxtLftInd, mMainBtnDim[PMBTN_CONTINUE].mTop + cTxtTopInd, 0, cTxtScale, "Continue");
                StarlightColor::ClearMixer();
            }
        }

        //--New Game button.
        if(mButtonHighlight != PMBTN_NEWGAME)
        {
            if(mAllSavefilesExist) glColor4f(1.0f, 1.0f, 1.0f, 0.5f);

            Images.Data.rBoxBlack->Draw(mMainBtnDim[PMBTN_NEWGAME].mLft, mMainBtnDim[PMBTN_NEWGAME].mTop);
            Images.Data.rHeadingFont->DrawText(mMainBtnDim[PMBTN_NEWGAME].mLft + cTxtLftInd, mMainBtnDim[PMBTN_NEWGAME].mTop + cTxtTopInd, 0, cTxtScale, "New Game");
            StarlightColor::ClearMixer();
        }
        else
        {
            if(mAllSavefilesExist) glColor4f(1.0f, 1.0f, 1.0f, 0.5f);
            Images.Data.rBoxWhite->Draw(mMainBtnDim[PMBTN_NEWGAME].mLft, mMainBtnDim[PMBTN_NEWGAME].mTop);
            StarlightColor::SetMixer(0.0f, 0.0f, 0.0f, 1.0f);
            Images.Data.rHeadingFont->DrawText(mMainBtnDim[PMBTN_NEWGAME].mLft + cTxtLftInd, mMainBtnDim[PMBTN_NEWGAME].mTop + cTxtTopInd, 0, cTxtScale, "New Game");
            StarlightColor::ClearMixer();
        }

        //--Gallery button.
        if(mAnySavefilesExist)
        {
            if(mButtonHighlight != PMBTN_GALLERY)
            {
                Images.Data.rBoxBlack->Draw(mMainBtnDim[PMBTN_GALLERY].mLft, mMainBtnDim[PMBTN_GALLERY].mTop);
                Images.Data.rHeadingFont->DrawText(mMainBtnDim[PMBTN_GALLERY].mLft + cTxtLftInd, mMainBtnDim[PMBTN_GALLERY].mTop + cTxtTopInd, 0, cTxtScale, "Gallery");
            }
            else
            {
                Images.Data.rBoxWhite->Draw(mMainBtnDim[PMBTN_GALLERY].mLft, mMainBtnDim[PMBTN_GALLERY].mTop);
                StarlightColor::SetMixer(0.0f, 0.0f, 0.0f, 1.0f);
                Images.Data.rHeadingFont->DrawText(mMainBtnDim[PMBTN_GALLERY].mLft + cTxtLftInd, mMainBtnDim[PMBTN_GALLERY].mTop + cTxtTopInd, 0, cTxtScale, "Gallery");
                StarlightColor::ClearMixer();
            }
        }

        //--Exit and Settings
        Images.Data.rIconClose->Draw(0.0f, 0.0f);
        Images.Data.rIconSettings->Draw(0.0f, 0.0f);
    }

    //--[Render Fade Cases]
    if(mGalleryFadeTimer > 0) RenderGallery();
    if(mLoadFadeTimer    > 0) RenderLoadMenu();

    //--[Settings Menu]
    //--Renders over everything else.
    PairanormalSettingsMenu *rSettingsMenu = PairanormalSettingsMenu::Fetch();
    rSettingsMenu->Render();

    //--[Fade to Black]
    //--Renders a fullscreen fade to black, when... fading to black. Duh.
    if(mIsFadingToBlack)
    {
        float cAlpha = (float)mFadeToBlackTimer / (float)PM_FADE_TO_BLACK_TICKS;
        SugarBitmap::DrawRectFill(0.0f, 0.0f, VIRTUAL_CANVAS_X, VIRTUAL_CANVAS_Y, StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, cAlpha));
    }
}

//======================================= Pointer Routing =========================================
//====================================== Static Functions =========================================
PairanormalMenu *PairanormalMenu::Fetch()
{
    //--Static dummy copy.
    static PairanormalMenu *xPairanormalMenu = NULL;

    //--Check if a valid PairanormalMenu is in the MapManager.
    RootLevel *rActiveLevel = MapManager::Fetch()->GetActiveLevel();
    if(!rActiveLevel || rActiveLevel->GetType() != POINTER_TYPE_PAIRANORMALMENU)
    {
        if(!xPairanormalMenu) xPairanormalMenu = new PairanormalMenu();
        return xPairanormalMenu;
    }

    //--Valid. Return it.
    return (PairanormalMenu *)rActiveLevel;
}

//========================================= Lua Hooking ===========================================
void PairanormalMenu::HookToLuaState(lua_State *pLuaState)
{
    /* PairanormalMenu_SetProperty("Is Level Select Active") (1 Boolean) (Static)
       PairanormalMenu_SetProperty("Is Chapter 2 Skip Active") (1 Boolean) (Static)
       PairanormalMenu_SetProperty("Boot Pairanormal Fonts") (Static)
       PairanormalMenu_SetProperty("Activate") (Static)
       PairanormalMenu_SetProperty("Gallery Images Total", iCount)
       PairanormalMenu_SetProperty("Gallery Image", iSlot, sPath)
       Sets the requested property in the PairanormalMenu object. */
    lua_register(pLuaState, "PairanormalMenu_SetProperty", &Hook_PairanormalMenu_SetProperty);
}

//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
int Hook_PairanormalMenu_SetProperty(lua_State *L)
{
    //PairanormalMenu_SetProperty("Is Level Select Active") (1 Boolean) (Static)
    //PairanormalMenu_SetProperty("Is Chapter 2 Skip Active") (1 Boolean) (Static)
    //PairanormalMenu_SetProperty("Boot Pairanormal Fonts") (Static)
    //PairanormalMenu_SetProperty("Activate") (Static)
    //PairanormalMenu_SetProperty("Gallery Images Total", iCount)
    //PairanormalMenu_SetProperty("Gallery Image", iSlot, sPath)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("Menu_SetProperty");

    //--Switching.
    const char *rSwitchType = lua_tostring(L, 1);

    //--Statics.
    if(!strcasecmp(rSwitchType, "Is Level Select Active") && tArgs == 1)
    {
        lua_pushboolean(L, PairanormalMenu::xIsLevelSelectEnabled);
        return 1;
    }
    else if(!strcasecmp(rSwitchType, "Is Chapter 2 Skip Active") && tArgs == 1)
    {
        lua_pushboolean(L, PairanormalMenu::xSkipToChapter2);
        return 1;
    }
    else if(!strcasecmp(rSwitchType, "Boot Pairanormal Fonts") && tArgs == 1)
    {
        PairanormalMenu::BootPairanormalFonts();
        return 0;
    }
    else if(!strcasecmp(rSwitchType, "Activate") && tArgs == 1)
    {
        PairanormalMenu *nNewMenu = new PairanormalMenu();
        MapManager::Fetch()->ReceiveLevel(nNewMenu);
        return 0;
    }

    //--Active Object.
    PairanormalMenu *rPairanormalMenu = PairanormalMenu::Fetch();

    //--Allocate space for the gallery.
    if(!strcasecmp(rSwitchType, "Gallery Images Total") && tArgs == 2)
    {
        rPairanormalMenu->AllocateGallerySpace(lua_tointeger(L, 2));
    }
    //--Set an image in the gallery.
    else if(!strcasecmp(rSwitchType, "Gallery Image") && tArgs == 3)
    {
        rPairanormalMenu->SetGalleryImage(lua_tointeger(L, 2), lua_tostring(L, 3));
    }
    //--Error.
    else
    {
        LuaPropertyError("Menu_SetProperty", rSwitchType, tArgs);
    }

    //--Done.
    return 0;
}
