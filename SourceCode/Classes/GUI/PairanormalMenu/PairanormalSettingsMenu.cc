//--Base
#include "PairanormalSettingsMenu.h"

//--Classes
#include "PairanormalDialogue.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarFont.h"

//--Definitions
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "DisplayManager.h"
#include "MapManager.h"
#include "OptionsManager.h"

//=========================================== System ==============================================
PairanormalSettingsMenu::PairanormalSettingsMenu()
{
    //--[RootObject]
    //--System
    mType = POINTER_TYPE_PAIRANORMALSETTINGSMENU;

    //--[PairanormalSettingsMenu]
    //--System
    mFadeTimer = 0;
    mIsOpen = false;

    //--Buttons
    memset(mButtons, 0, sizeof(TwoDimensionReal) * PSM_BTN_TOTAL);

    //--Sliders
    mGrabbedSlider = -1;
    mMusicVolumeSlider = 0.50f;
    mSoundVolumeSlider = 0.75f;
    mTextSpeedSlider = 0.50f;

    //--Starting Values
    mStartingMusicVolume = 0.50f;
    mStartingSoundVolume = 0.75f;
    mStartingTextSpeed = 0.50f;
    mFlashImages = false;
    mUseSwagger = false;
    mAllowTextTicks = true;
    mShowInvestigationHints = false;

    //--Images
    memset(&Images, 0, sizeof(Images));

    //--Public Variables
    mUpdatedThisTick = false;
}
PairanormalSettingsMenu::~PairanormalSettingsMenu()
{
}
void PairanormalSettingsMenu::Construct()
{
    //--Fast-access pointers.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();

    //--Images
    Images.Data.rBacking          = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Settings/Backing");
    Images.Data.rBoxChecked       = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Settings/BoxChecked");
    Images.Data.rBoxUnchecked     = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Settings/BoxUnchecked");
    Images.Data.rHeaderBox        = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Settings/HeaderBox");
    Images.Data.rLowerBtnAccept   = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Settings/LowerBtnAccept");
    Images.Data.rLowerBtnClose    = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Settings/LowerBtnClose");
    Images.Data.rLowerBtnDefaults = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Settings/LowerBtnDefaults");
    Images.Data.rVolumeSliders    = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Settings/VolumeSliders");

    //--Fonts
    Images.Data.rMainFont  = (SugarFont *)rDataLibrary->GetEntry("Root/Fonts/System/TTF/Swagger40");
    Images.Data.rSwagger40 = (SugarFont *)rDataLibrary->GetEntry("Root/Fonts/System/TTF/Swagger40");
    Images.Data.rOxygen40  = (SugarFont *)rDataLibrary->GetEntry("Root/Fonts/System/TTF/Oxygen40");

    //--Verify.
    Images.mIsReady = VerifyStructure(&Images.Data, sizeof(Images.Data), sizeof(void *));

    //--[Button Positioning]
    mButtons[PSM_BTN_TODEFAULTS].SetWH(830.0f, 635.0f,                   71.0f, 73.0f);
    mButtons[PSM_BTN_CANCEL].SetWH(    909.0f, 635.0f,                   69.0f, 73.0f);
    mButtons[PSM_BTN_ACCEPT].SetWH(    986.0f, 635.0f,                   69.0f, 73.0f);
    mButtons[PSM_SLIDER_MUSIC].SetWH(  654.0f, 154.0f - 13.0f,          274.0f,  5.0f + 26.0f);
    mButtons[PSM_SLIDER_SOUND].SetWH(  654.0f, 154.0f - 13.0f +  60.0f, 274.0f,  5.0f + 26.0f);
    mButtons[PSM_SLIDER_TEXT].SetWH(   654.0f, 154.0f - 13.0f + 120.0f, 274.0f,  5.0f + 26.0f);
    mButtons[PSM_BTN_SWAGGER].SetWH(   331.0f, 570.0f,                   37.0f, 37.0f);
    mButtons[PSM_BTN_OXYGEN].SetWH(    331.0f, 523.0f,                   37.0f, 37.0f);
    mButtons[PSM_BTN_YESFLASH].SetWH(  766.0f, 523.0f,                   37.0f, 37.0f);
    mButtons[PSM_BTN_NOFLASH].SetWH(   766.0f, 570.0f,                   37.0f, 37.0f);

    mButtons[PSM_BTN_YESTICKS].SetWH(  331.0f, 364.0f,                   37.0f, 37.0f);
    mButtons[PSM_BTN_NOTICKS].SetWH(   331.0f, 412.0f,                   37.0f, 37.0f);
    mButtons[PSM_BTN_YESHINTS].SetWH(  766.0f, 364.0f,                   37.0f, 37.0f);
    mButtons[PSM_BTN_NOHINTS].SetWH(   766.0f, 412.0f,                   37.0f, 37.0f);
}

//--[Public Statics]
//--Static copy of the menu. Gets instantiated when it first gets fetched, or when Pairanormal is
//  booted for the first time (from the title screen).
PairanormalSettingsMenu *PairanormalSettingsMenu::xStaticMenu = NULL;

//====================================== Property Queries =========================================
bool PairanormalSettingsMenu::IsOpen()
{
    return mIsOpen;
}

//========================================= Manipulators ==========================================
//========================================= Core Methods ==========================================
void PairanormalSettingsMenu::Open()
{
    //--Set flag.
    mIsOpen = true;
    mFadeTimer = 0;

    //--Store the current values for all the options.
    mStartingMusicVolume = AudioManager::xMusicVolume;
    mStartingSoundVolume = AudioManager::xSoundVolume;
    mStartingTextSpeed = PairanormalDialogue::Fetch()->GetAppendSpeed();
    mFlashImages = OptionsManager::xAllowFlashingImages;
    mUseSwagger = PairanormalDialogue::Fetch()->IsUsingSwagger();
    mAllowTextTicks = PairanormalDialogue::xAllowVoiceTicks;
    mShowInvestigationHints = PairanormalDialogue::xShowInvestigationHints;

    //--Set the slider values.
    mMusicVolumeSlider = mStartingMusicVolume;
    mSoundVolumeSlider = mStartingSoundVolume;
    float cMax = 70.0f / 60.0f;
    float cMin = 10.0f / 60.0f;
    mTextSpeedSlider = (mStartingTextSpeed - cMin) / (cMax - cMin);
}
void PairanormalSettingsMenu::ResetToDefaults()
{
    float cMax = 70.0f / 60.0f;
    float cMin = 10.0f / 60.0f;
    mMusicVolumeSlider = 0.50f;
    mSoundVolumeSlider = 0.75f;
    AudioManager::Fetch()->ChangeMusicVolume( AudioManager::xMusicVolume - mMusicVolumeSlider );
    AudioManager::Fetch()->ChangeSoundVolume( AudioManager::xSoundVolume - mSoundVolumeSlider );
    mTextSpeedSlider = (1.0f - cMin) / (cMax - cMin);
    mFlashImages = true;
    mUseSwagger = true;
    mAllowTextTicks = true;
    mShowInvestigationHints = false;
}
void PairanormalSettingsMenu::Cancel()
{
    //--Just close the menu without changing any settings.
    mIsOpen = false;

    //--We need to set the music and sound volumes back to where they were before.
    AudioManager::Fetch()->ChangeMusicVolume( AudioManager::xMusicVolume - mMusicVolumeSlider );
    AudioManager::Fetch()->ChangeSoundVolume( AudioManager::xSoundVolume - mSoundVolumeSlider );
}
void PairanormalSettingsMenu::CloseAndSave()
{
    //--Set the values as needed. Music/Sound will already have been set.
    mIsOpen = false;
    float cMax = 70.0f / 60.0f;
    float cMin = 10.0f / 60.0f;
    PairanormalDialogue::Fetch()->SetAppendSpeed( (mTextSpeedSlider * (cMax - cMin)) + cMin );
    PairanormalDialogue::Fetch()->SetUseSwagger(mUseSwagger);
    OptionsManager::xAllowFlashingImages = mFlashImages;
    PairanormalDialogue::xAllowVoiceTicks = mAllowTextTicks;
    PairanormalDialogue::xShowInvestigationHints = mShowInvestigationHints;
}

//===================================== Private Core Methods ======================================
//============================================ Update =============================================
void PairanormalSettingsMenu::Update()
{
    //--[Documentation and Setup]
    //--Handles controls and modification of game settings. This can be controlled from a variety
    //  of places. To close this object, set mIsOpen to false, and the calling object should check
    //  that variable and not update when it's false.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--[Timer]
    if(mFadeTimer < PSM_FADE_TICKS) mFadeTimer ++;
    mUpdatedThisTick = true;

    //--[Mouse Position]
    //--Get mouse location.
    float mMouseX, mMouseY, tMouseZ;
    rControlManager->GetMouseCoordsF(mMouseX, mMouseY, tMouseZ);

    //--[Mouse Dragging]
    //--Only works if a slider is currently dragged.
    if(mGrabbedSlider == PSM_SLIDER_MUSIC)
    {
        //--Check value, clamp edges.
        float tOldMusic = mMusicVolumeSlider;
        mMusicVolumeSlider = (mMouseX - mButtons[PSM_SLIDER_MUSIC].mLft) / (float)mButtons[PSM_SLIDER_MUSIC].GetWidth();
        if(mMusicVolumeSlider < 0.0f) mMusicVolumeSlider = 0.0f;
        if(mMusicVolumeSlider > 1.0f) mMusicVolumeSlider = 1.0f;

        //--If it's different, upload that to the AudioManager.
        if(tOldMusic != mMusicVolumeSlider)
        {
            AudioManager::Fetch()->ChangeMusicVolume(mMusicVolumeSlider - tOldMusic);
        }
    }
    //--Sound.
    else if(mGrabbedSlider == PSM_SLIDER_SOUND)
    {
        //--Check value, clamp edges.
        float tOldSound = mSoundVolumeSlider;
        mSoundVolumeSlider = (mMouseX - mButtons[PSM_SLIDER_SOUND].mLft) / (float)mButtons[PSM_SLIDER_SOUND].GetWidth();
        if(mSoundVolumeSlider < 0.0f) mSoundVolumeSlider = 0.0f;
        if(mSoundVolumeSlider > 1.0f) mSoundVolumeSlider = 1.0f;

        //--If it's different, upload that to the AudioManager.
        if(tOldSound != mMusicVolumeSlider)
        {
            AudioManager::Fetch()->ChangeSoundVolume(mSoundVolumeSlider - tOldSound);
        }
    }
    //--Text speed.
    else if(mGrabbedSlider == PSM_SLIDER_TEXT)
    {
        mTextSpeedSlider = (mMouseX - mButtons[PSM_SLIDER_TEXT].mLft) / (float)mButtons[PSM_SLIDER_TEXT].GetWidth();
        if(mTextSpeedSlider < 0.0f) mTextSpeedSlider = 0.0f;
        if(mTextSpeedSlider > 1.0f) mTextSpeedSlider = 1.0f;
    }

    //--[Mouse Clicking]
    //--Player clicks the mouse at a location on screen.
    if(rControlManager->IsFirstPress("MouseLft"))
    {
        //--[Restore Defaults]
        if(mButtons[PSM_BTN_TODEFAULTS].IsPointWithin(mMouseX, mMouseY))
        {
            ResetToDefaults();
            AudioManager::Fetch()->PlaySound("UI|Select");
        }
        //--[Cancel]
        else if(mButtons[PSM_BTN_CANCEL].IsPointWithin(mMouseX, mMouseY))
        {
            Cancel();
            AudioManager::Fetch()->PlaySound("UI|Select");
        }
        //--[Accept Changes]
        else if(mButtons[PSM_BTN_ACCEPT].IsPointWithin(mMouseX, mMouseY))
        {
            CloseAndSave();
            AudioManager::Fetch()->PlaySound("UI|Select");
        }
        //--[Music Slider]
        else if(mButtons[PSM_SLIDER_MUSIC].IsPointWithin(mMouseX, mMouseY))
        {
            //--Set slider.
            mGrabbedSlider = PSM_SLIDER_MUSIC;

            //--Set values.
            float tOldMusic = mMusicVolumeSlider;
            mMusicVolumeSlider = (mMouseX - mButtons[PSM_SLIDER_MUSIC].mLft) / (float)mButtons[PSM_SLIDER_MUSIC].GetWidth();

            //--If it's different, upload that to the AudioManager.
            if(tOldMusic != mMusicVolumeSlider)
            {
                AudioManager::Fetch()->ChangeMusicVolume(mMusicVolumeSlider - tOldMusic);
            }
            AudioManager::Fetch()->PlaySound("UI|Select");
        }
        //--[Sound Slider]
        else if(mButtons[PSM_SLIDER_SOUND].IsPointWithin(mMouseX, mMouseY))
        {
            mGrabbedSlider = PSM_SLIDER_SOUND;
            mSoundVolumeSlider = (mMouseX - mButtons[PSM_SLIDER_SOUND].mLft) / (float)mButtons[PSM_SLIDER_SOUND].GetWidth();
            AudioManager::Fetch()->PlaySound("UI|Select");
        }
        //--[Text Slider]
        else if(mButtons[PSM_SLIDER_TEXT].IsPointWithin(mMouseX, mMouseY))
        {
            mGrabbedSlider = PSM_SLIDER_TEXT;
            mTextSpeedSlider = (mMouseX - mButtons[PSM_SLIDER_TEXT].mLft) / (float)mButtons[PSM_SLIDER_TEXT].GetWidth();
            AudioManager::Fetch()->PlaySound("UI|Select");
        }
        //--[Using Swagger]
        else if(mButtons[PSM_BTN_SWAGGER].IsPointWithin(mMouseX, mMouseY))
        {
            mUseSwagger = true;
            AudioManager::Fetch()->PlaySound("UI|Select");
        }
        //--[Using Oxygen]
        else if(mButtons[PSM_BTN_OXYGEN].IsPointWithin(mMouseX, mMouseY))
        {
            mUseSwagger = false;
            AudioManager::Fetch()->PlaySound("UI|Select");
        }
        //--[Allow Flashing Images]
        else if(mButtons[PSM_BTN_YESFLASH].IsPointWithin(mMouseX, mMouseY))
        {
            mFlashImages = true;
            AudioManager::Fetch()->PlaySound("UI|Select");
        }
        //--[Disallow Flashing Images]
        else if(mButtons[PSM_BTN_NOFLASH].IsPointWithin(mMouseX, mMouseY))
        {
            mFlashImages = false;
            AudioManager::Fetch()->PlaySound("UI|Select");
        }
        //--[Allow Voice SFX]
        else if(mButtons[PSM_BTN_YESTICKS].IsPointWithin(mMouseX, mMouseY))
        {
            mAllowTextTicks = true;
            AudioManager::Fetch()->PlaySound("UI|Select");
        }
        //--[Disallow Voice SFX]
        else if(mButtons[PSM_BTN_NOTICKS].IsPointWithin(mMouseX, mMouseY))
        {
            mAllowTextTicks = false;
            AudioManager::Fetch()->PlaySound("UI|Select");
        }
        //--[Allow Investigation Hints]
        else if(mButtons[PSM_BTN_YESHINTS].IsPointWithin(mMouseX, mMouseY))
        {
            mShowInvestigationHints = true;
            AudioManager::Fetch()->PlaySound("UI|Select");
        }
        //--[Disallow Investigation Hints]
        else if(mButtons[PSM_BTN_NOHINTS].IsPointWithin(mMouseX, mMouseY))
        {
            mShowInvestigationHints = false;
            AudioManager::Fetch()->PlaySound("UI|Select");
        }
    }
    //--[Mouse Releasing]
    //--Only used for sliders.
    else if(rControlManager->IsFirstRelease("MouseLft"))
    {
        mGrabbedSlider = -1;
    }
}
void PairanormalSettingsMenu::NotUpdate()
{
    if(mFadeTimer > 0) mFadeTimer --;
}

//=========================================== File I/O ============================================
//=========================================== Drawing =============================================
void PairanormalSettingsMenu::Render()
{
    //--[Documentation and Setup]
    //--Renders the settings menu. Note that there is no fixed background. Rather, the rest of the
    //  screen is greyed and the menu renders on top of it.
    if(!Images.mIsReady || mFadeTimer == 0) return;

    //--[Alpha]
    float cAlpha = (float)mFadeTimer / (float)PSM_FADE_TICKS;

    //--[Background Overlay]
    //--Render a grey box to darken the background.
    StarlightColor::SetMixer(0.0f, 0.0f, 0.0f, 0.75f * cAlpha);
    glDisable(GL_TEXTURE_2D);
    glBegin(GL_QUADS);
        glVertex2f(            0.0f,             0.0f);
        glVertex2f(VIRTUAL_CANVAS_X,             0.0f);
        glVertex2f(VIRTUAL_CANVAS_X, VIRTUAL_CANVAS_Y);
        glVertex2f(            0.0f, VIRTUAL_CANVAS_Y);
    glEnd();
    glEnable(GL_TEXTURE_2D);
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha);

    //--[Fixed Parts]
    Images.Data.rBacking->Draw();
    Images.Data.rHeaderBox->Draw();
    Images.Data.rLowerBtnAccept->Draw();
    Images.Data.rLowerBtnClose->Draw();
    Images.Data.rLowerBtnDefaults->Draw();
    Images.Data.rVolumeSliders->Draw();

    //--[Font Rendering]
    Images.Data.rMainFont->DrawText(496.0f,  60.0f, 0, 1.0f, "Settings");
    Images.Data.rMainFont->DrawText(351.0f, 136.0f, 0, 1.0f, "Music Volume");
    Images.Data.rMainFont->DrawText(351.0f, 196.0f, 0, 1.0f, "Sound Volume");
    Images.Data.rMainFont->DrawText(351.0f, 256.0f, 0, 1.0f, "Text Speed");

    //--[Voice Ticks]
    //--Constants.
    float cXIndent = 80.0f;
    float cYIndent = 12.0f;

    Images.Data.rMainFont->DrawText(336.0f, 463.0f - 161.0f, 0, 1.0f, "Voice SFX");
    if(mAllowTextTicks)
    {
        Images.Data.rBoxChecked->Draw(318, 481 + 23 - 161.0f);
        Images.Data.rOxygen40->DrawText(318.0f + cXIndent, 503.0f + cYIndent - 161.0f, 0, 1.0f, "On");

        Images.Data.rBoxUnchecked->Draw(331, 570 - 161.0f);
        Images.Data.rSwagger40->DrawText(318.0f + cXIndent, 550.0f + cYIndent - 161.0f, 0, 1.0f, "Off");
    }
    else
    {
        Images.Data.rBoxUnchecked->Draw(331, 523 - 161.0f);
        Images.Data.rOxygen40->DrawText(318.0f + cXIndent, 503.0f + cYIndent - 161.0f, 0, 1.0f, "On");

        Images.Data.rBoxChecked->Draw(318, 551 - 161.0f);
        Images.Data.rSwagger40->DrawText(318.0f + cXIndent, 550.0f + cYIndent - 161.0f, 0, 1.0f, "Off");
    }

    //--[Investigation Hints]
    Images.Data.rMainFont->DrawText(771.0f, 463.0f - 161.0f, 0, 1.0f, "Investigation Hints");
    if(mShowInvestigationHints)
    {
        Images.Data.rBoxChecked->Draw(  751,    481 + 23 - 161.0f);
        Images.Data.rMainFont->DrawText(751.0f + cXIndent, 503.0f + cYIndent - 161.0f, 0, 1.0f, "On");

        Images.Data.rBoxUnchecked->Draw(766,    570 - 161.0f);
        Images.Data.rMainFont->DrawText(751.0f + cXIndent, 550.0f + cYIndent - 161.0f, 0, 1.0f, "Off");
    }
    else
    {
        Images.Data.rBoxUnchecked->Draw(766,    523 - 161.0f);
        Images.Data.rMainFont->DrawText(751.0f + cXIndent, 503.0f + cYIndent - 161.0f, 0, 1.0f, "On");

        Images.Data.rBoxChecked->Draw(  751,    551 - 161.0f);
        Images.Data.rMainFont->DrawText(751.0f + cXIndent, 550.0f + cYIndent - 161.0f, 0, 1.0f, "Off");
    }

    //--[Font Buttons]
    //--Heading.
    Images.Data.rMainFont->DrawText(336.0f, 463.0f, 0, 1.0f, "Dialogue Font");

    //--Using Swagger font.
    if(mUseSwagger)
    {
        Images.Data.rBoxUnchecked->Draw(331, 523);
        Images.Data.rOxygen40->DrawText(318.0f + cXIndent, 503.0f + cYIndent, 0, 1.0f, "Oxygen");

        Images.Data.rBoxChecked->Draw(318, 551);
        Images.Data.rSwagger40->DrawText(318.0f + cXIndent, 550.0f + cYIndent, 0, 1.0f, "Swagger");
    }
    //--Using Oxygen font.
    else
    {
        Images.Data.rBoxChecked->Draw(318, 481 + 23);
        Images.Data.rOxygen40->DrawText(318.0f + cXIndent, 503.0f + cYIndent, 0, 1.0f, "Oxygen");

        Images.Data.rBoxUnchecked->Draw(331, 570);
        Images.Data.rSwagger40->DrawText(318.0f + cXIndent, 550.0f + cYIndent, 0, 1.0f, "Swagger");
    }

    //--[Flashing Images]
    Images.Data.rMainFont->DrawText(771.0f, 463.0f, 0, 1.0f, "Flashing Images");
    if(mFlashImages)
    {
        Images.Data.rBoxChecked->Draw(  751,    481 + 23);
        Images.Data.rMainFont->DrawText(751.0f + cXIndent, 503.0f + cYIndent, 0, 1.0f, "On");

        Images.Data.rBoxUnchecked->Draw(766,    570);
        Images.Data.rMainFont->DrawText(751.0f + cXIndent, 550.0f + cYIndent, 0, 1.0f, "Off");
    }
    else
    {
        Images.Data.rBoxUnchecked->Draw(766,    523);
        Images.Data.rMainFont->DrawText(751.0f + cXIndent, 503.0f + cYIndent, 0, 1.0f, "On");

        Images.Data.rBoxChecked->Draw(  751,    551);
        Images.Data.rMainFont->DrawText(751.0f + cXIndent, 550.0f + cYIndent, 0, 1.0f, "Off");
    }

    //--[Sliders]
    //--WOOOAAAAHHH (Sliders...)
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlpha);
    glDisable(GL_TEXTURE_2D);
    glBegin(GL_QUADS);

        //--[Music]
        //--Bar indicating how full the slider is.
        float cBarLft = mButtons[PSM_SLIDER_MUSIC].mLft;
        float cBarTop = mButtons[PSM_SLIDER_MUSIC].mTop + 13.0f;
        float cBarRgt = mButtons[PSM_SLIDER_MUSIC].mLft + (mButtons[PSM_SLIDER_MUSIC].GetWidth() * mMusicVolumeSlider);
        float cBarBot = mButtons[PSM_SLIDER_MUSIC].mTop + 13.0f + 5.0f;
        if(mMusicVolumeSlider > 0.0f)
        {
            glVertex2f(cBarLft, cBarTop);
            glVertex2f(cBarRgt, cBarTop);
            glVertex2f(cBarRgt, cBarBot);
            glVertex2f(cBarLft, cBarBot);
        }

        //--Slider itself.
        cBarLft = mButtons[PSM_SLIDER_MUSIC].mLft + (mButtons[PSM_SLIDER_MUSIC].GetWidth() * mMusicVolumeSlider) - 3.0f;
        cBarTop = mButtons[PSM_SLIDER_MUSIC].mTop;
        cBarRgt = mButtons[PSM_SLIDER_MUSIC].mLft + (mButtons[PSM_SLIDER_MUSIC].GetWidth() * mMusicVolumeSlider) + 3.0f;
        cBarBot = mButtons[PSM_SLIDER_MUSIC].mBot;
        glVertex2f(cBarLft, cBarTop);
        glVertex2f(cBarRgt, cBarTop);
        glVertex2f(cBarRgt, cBarBot);
        glVertex2f(cBarLft, cBarBot);

        //--[Sound]
        //--Same as Music, use a different spot.
        cBarLft = mButtons[PSM_SLIDER_SOUND].mLft;
        cBarTop = mButtons[PSM_SLIDER_SOUND].mTop + 13.0f;
        cBarRgt = mButtons[PSM_SLIDER_SOUND].mLft + (mButtons[PSM_SLIDER_SOUND].GetWidth() * mSoundVolumeSlider);
        cBarBot = mButtons[PSM_SLIDER_SOUND].mTop + 13.0f + 5.0f;
        if(mSoundVolumeSlider > 0.0f)
        {
            glVertex2f(cBarLft, cBarTop);
            glVertex2f(cBarRgt, cBarTop);
            glVertex2f(cBarRgt, cBarBot);
            glVertex2f(cBarLft, cBarBot);
        }

        //--Slider itself.
        cBarLft = mButtons[PSM_SLIDER_SOUND].mLft + (mButtons[PSM_SLIDER_SOUND].GetWidth() * mSoundVolumeSlider) - 3.0f;
        cBarTop = mButtons[PSM_SLIDER_SOUND].mTop;
        cBarRgt = mButtons[PSM_SLIDER_SOUND].mLft + (mButtons[PSM_SLIDER_SOUND].GetWidth() * mSoundVolumeSlider) + 3.0f;
        cBarBot = mButtons[PSM_SLIDER_SOUND].mBot;
        glVertex2f(cBarLft, cBarTop);
        glVertex2f(cBarRgt, cBarTop);
        glVertex2f(cBarRgt, cBarBot);
        glVertex2f(cBarLft, cBarBot);

        //--[Text Rate]
        //--Same as Music, use a different spot.
        cBarLft = mButtons[PSM_SLIDER_TEXT].mLft;
        cBarTop = mButtons[PSM_SLIDER_TEXT].mTop + 13.0f;
        cBarRgt = mButtons[PSM_SLIDER_TEXT].mLft + (mButtons[PSM_SLIDER_TEXT].GetWidth() * mTextSpeedSlider);
        cBarBot = mButtons[PSM_SLIDER_TEXT].mTop + 13.0f + 5.0f;
        if(mTextSpeedSlider > 0.0f)
        {
            glVertex2f(cBarLft, cBarTop);
            glVertex2f(cBarRgt, cBarTop);
            glVertex2f(cBarRgt, cBarBot);
            glVertex2f(cBarLft, cBarBot);
        }

        //--Slider itself.
        cBarLft = mButtons[PSM_SLIDER_TEXT].mLft + (mButtons[PSM_SLIDER_TEXT].GetWidth() * mTextSpeedSlider) - 3.0f;
        cBarTop = mButtons[PSM_SLIDER_TEXT].mTop;
        cBarRgt = mButtons[PSM_SLIDER_TEXT].mLft + (mButtons[PSM_SLIDER_TEXT].GetWidth() * mTextSpeedSlider) + 3.0f;
        cBarBot = mButtons[PSM_SLIDER_TEXT].mBot;
        glVertex2f(cBarLft, cBarTop);
        glVertex2f(cBarRgt, cBarTop);
        glVertex2f(cBarRgt, cBarBot);
        glVertex2f(cBarLft, cBarBot);


    glEnd();
    glEnable(GL_TEXTURE_2D);
    StarlightColor::ClearMixer();
}

//======================================= Pointer Routing =========================================
//====================================== Static Functions =========================================
PairanormalSettingsMenu *PairanormalSettingsMenu::Fetch()
{
    if(!xStaticMenu)
    {
        xStaticMenu = new PairanormalSettingsMenu();
        xStaticMenu->Construct();
    }
    return xStaticMenu;
}

//========================================= Lua Hooking ===========================================
//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
