//--[PairanormalMenu]
//--Main menu for Pairanormal.

#pragma once

//--[Includes]
#include "Definitions.h"
#include "Structures.h"
#include "RootLevel.h"

//--[Local Structures]
typedef struct
{
    SugarBitmap *rDisplayImage;
    char mCharacterName[32];
    char mPlayTime[32];
}LoadPack;

//--[Local Definitions]
//--Menu Modes
#define PM_MODE_MAINMENU 0
#define PM_MODE_GALLERY 1
#define PM_MODE_DISCLAIMER 2
#define PM_MODE_LOADING 3

//--Main Menu Buttons
#define PMBTN_CONTINUE 0
#define PMBTN_NEWGAME 1
#define PMBTN_GALLERY 2
#define PMBTN_QUIT 3
#define PMBTN_SETTINGS 4
#define PMBTN_TOTAL 5

//--Disclaimer Buttons
#define PMBTN_DIS_READY 0
#define PMBTN_DIS_NOTREADY 1
#define PMBTN_DIS_TOTAL 2

//--Gallery Buttons
#define PMBTN_GAL_BACK 0
#define PMBTN_GAL_SETTINGS 1
#define PMBTN_GAL_PICTURE_0 2
#define PMBTN_GAL_PICTURE_1 3
#define PMBTN_GAL_PICTURE_2 4
#define PMBTN_GAL_PICTURE_3 5
#define PMBTN_GAL_PICTURE_4 6
#define PMBTN_GAL_PICTURE_5 7
#define PMBTN_GAL_PICTURE_6 8
#define PMBTN_GAL_PICTURE_7 9
#define PMBTN_GAL_PICTURE_8 10
#define PMBTN_GAL_PAGEDOWN 11
#define PMBTN_GAL_PAGEUP 12
#define PMBTN_GAL_CLEARFOCUS 13
#define PMBTN_GAL_TOTAL 14
#define GALLERY_VISIBLE_IMAGES 9

//--Loading Buttons
#define PMBTN_LOAD_BACK 0
#define PMBTN_LOAD_HIGHLIGHT_0 1
#define PMBTN_LOAD_HIGHLIGHT_1 2
#define PMBTN_LOAD_HIGHLIGHT_2 3
#define PMBTN_LOAD_PLAY 4
#define PMBTN_LOAD_DELETE 5
#define PMBTN_LOAD_MOD_0 6
#define PMBTN_LOAD_MOD_1 7
#define PMBTN_LOAD_MOD_2 8
#define PMBTN_LOAD_TOTAL 9

//--Loading Constants
#define LOAD_PACKS_MAX 3

//--Fading Constants
#define PM_FADE_TICKS 15
#define PM_FADE_TO_BLACK_TICKS 45
#define PM_FADE_TO_BLACK_HOLD_TICKS 45

//--Cheat Codes
#define LEVEL_SELECT_WORD "chocolatelab"
#define LEVEL_SELECT_LEN 13
#define SPEEDUP_WORD "gottagofast"
#define SPEEDUP_LEN 12
#define SCRIPTHUNTER_WORD "findthebugs"
#define SCRIPTHUNTER_LEN 12
#define SKIPTOTWO_WORD "skiptotwo"
#define SKIPTOTWO_LEN 10
#define CHANGENAME_WORD "jaynomore"
#define CHANGENAME_LEN 10

//--[Classes]
class PairanormalMenu : public RootLevel
{
    private:
    //--System
    bool mAllSavefilesExist;
    bool mAnySavefilesExist;
    int mLevelSelectProgress;
    int mSpeedupProgress;
    int mSkipToTwoProgress;
    int mScriptHunterProgress;
    int mJayNoMoreProgress;
    float mMouseX;
    float mMouseY;

    //--Mode
    int mCurrentMode;

    //--Background
    int mTitleFloatTimer;
    SugarBitmap *rCurrentBackground;
    SugarBitmap *rGalleryBackground;

    //--Buttons
    int mButtonHighlight;
    TwoDimensionReal mMainBtnDim[PMBTN_TOTAL];
    TwoDimensionReal mGalleryBtnDim[PMBTN_GAL_TOTAL];
    TwoDimensionReal mLoadingBtnDim[PMBTN_LOAD_TOTAL];
    TwoDimensionReal mDisclaimerBtnDim[PMBTN_DIS_TOTAL];

    //--Disclaimer
    int mDisclaimerHighlight;

    //--Gallery Paths
    int mGalleryFadeTimer;
    int mCurrentGalleryPage;
    int mFocusOnImage;
    int mGalleryPathsTotal;
    char **mGalleryPaths;
    SugarBitmap *rGalleryImages[GALLERY_VISIBLE_IMAGES];

    //--Loading Screen
    bool mIsFadingToBlack;
    int mFadeToBlackTimer;
    int mLoadFadeTimer;
    int mLoadPackSelected;
    LoadPack mLoadPacks[LOAD_PACKS_MAX];

    //--Images
    struct
    {
        bool mIsReady;
        struct
        {
            //--Fonts
            SugarFont *rSystemFont;
            SugarFont *rDisclaimerFont;
            SugarFont *rHeadingFont;

            //--Icons
            SugarBitmap *rIconBack;
            SugarBitmap *rIconClose;
            SugarBitmap *rIconSettings;

            //--Title Card.
            SugarBitmap *rTitleCard;

            //--Boxes
            SugarBitmap *rBoxBlack;
            SugarBitmap *rBoxWhite;

            //--Loading
            SugarBitmap *rLoadBacking;
            SugarBitmap *rLoadDeleteBtn;
            SugarBitmap *rLoadHeader;
            SugarBitmap *rLoadPencil;
            SugarBitmap *rLoadPlayBtn;
            SugarBitmap *rLoadSelectedBacking;

            //--Gallery
            SugarBitmap *rGalleryBacking;
            SugarBitmap *rImageBacking;
            SugarBitmap *rBtnIncrement;
            SugarBitmap *rBtnDecrement;

            //--Disclaimer
            SugarBitmap *rDisclaimer;
            SugarBitmap *rDisclaimerBtnReadySelected;
            SugarBitmap *rDisclaimerBtnReadyUnselected;
            SugarBitmap *rDisclaimerBtnNotReadySelected;
            SugarBitmap *rDisclaimerBtnNotReadyUnselected;
        }Data;
    }Images;

    protected:

    public:
    //--System
    PairanormalMenu();
    virtual ~PairanormalMenu();
    static void BootPairanormalFonts();

    //--Public Variables
    static bool xIsLevelSelectEnabled;
    static bool xSpeedupFlag;
    static bool xSkipToChapter2;
    static bool xImmediateRename;

    //--Property Queries
    //--Manipulators
    //--Core Methods
    void RecheckFileExistence();

    //--Disclaimer
    void UpdateDisclaimer();
    void RenderDisclaimer();

    //--Gallery Menu
    void AllocateGallerySpace(int pSlots);
    void SetGalleryImage(int pSlot, const char *pPath);
    void RefreshImagesByPage(int pPage);
    void UpdateGallery();
    void RenderGallery();

    //--Loading Menu
    void OpenLoadMenu();
    void UpdateLoadMenu();
    void RenderLoadMenu();

    private:
    //--Private Core Methods
    bool HandleCheatCode(const char *pPressedKey, int &sProgressVar, int pMaxLength, const char *pCheatCode);

    public:
    //--Update
    virtual void Update();

    //--File I/O
    //--Drawing
    virtual void AddToRenderList(SugarLinkedList *pRenderList);
    virtual void Render();

    //--Pointer Routing
    //--Static Functions
    static PairanormalMenu *Fetch();

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_PairanormalMenu_SetProperty(lua_State *L);

