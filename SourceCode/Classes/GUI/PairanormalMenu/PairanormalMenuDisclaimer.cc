//--Base
#include "PairanormalMenu.h"

//--Classes
#include "PairanormalLevel.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarFont.h"

//--Definitions
#include "Global.h"

//--Libraries

//--Managers
#include "ControlManager.h"
#include "DisplayManager.h"

//--[Manipulators]

//--[Update]
void PairanormalMenu::UpdateDisclaimer()
{
    //--Updates the gallery controls.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--[Mouse Position]
    //--Get mouse location.
    float tMouseZ;
    rControlManager->GetMouseCoordsF(mMouseX, mMouseY, tMouseZ);

    //--[Mouse Highlighting]
    if(mDisclaimerBtnDim[PMBTN_DIS_NOTREADY].IsPointWithin(mMouseX, mMouseY))
    {
        mDisclaimerHighlight = PMBTN_DIS_NOTREADY;
    }
    else if(mDisclaimerBtnDim[PMBTN_DIS_READY].IsPointWithin(mMouseX, mMouseY))
    {
        mDisclaimerHighlight = PMBTN_DIS_READY;
    }
    else
    {
        mDisclaimerHighlight = -1;
    }

    //--[Mouse Clicking]
    //--Player clicks the mouse at a location on screen.
    if(rControlManager->IsFirstPress("MouseLft"))
    {
        //--Just use the highlight value.
        if(mDisclaimerHighlight == PMBTN_DIS_NOTREADY)
        {
            Global::Shared()->gQuit = true;
        }
        else if(mDisclaimerHighlight == PMBTN_DIS_READY)
        {
            mCurrentMode = PM_MODE_MAINMENU;
        }
        else
        {

        }
    }
}

//--[Render]
void PairanormalMenu::RenderDisclaimer()
{
    //--[Documentation and Setup]
    //--Renders the gallery.
    if(!Images.mIsReady) return;

    //--[Background]
    //--Render the background.
    if(rGalleryBackground)
    {
        //--Determine scale and offset.
        float cUseScale = BG_SCALE;
        float cYOffset = BG_OFFSET_Y;
        if(PairanormalLevel::xIsLowResMode)
        {
            cUseScale = cUseScale * 2.0f;
            cYOffset = cYOffset * 0.5f;
        }

        //--Render.
        glScalef(cUseScale, cUseScale, 1.0f);
        rCurrentBackground->Draw(0, cYOffset);
        glScalef(1.0f / cUseScale, 1.0f / cUseScale, 1.0f);
    }

    //--Darken overlay.
    StarlightColor::SetMixer(0.0f, 0.0f, 0.0f, 0.50f);
    glDisable(GL_TEXTURE_2D);
    glBegin(GL_QUADS);
        glVertex2f(            0.0f,             0.0f);
        glVertex2f(VIRTUAL_CANVAS_X,             0.0f);
        glVertex2f(VIRTUAL_CANVAS_X, VIRTUAL_CANVAS_Y);
        glVertex2f(            0.0f, VIRTUAL_CANVAS_Y);
    glEnd();
    glEnable(GL_TEXTURE_2D);
    StarlightColor::ClearMixer();

    //--[Main Disclaimer]
    //--For now just render an image.
    Images.Data.rDisclaimer->Draw();

    //--[Buttons]
    //--Not Ready.
    if(mDisclaimerHighlight != PMBTN_DIS_NOTREADY)
    {
        Images.Data.rDisclaimerBtnNotReadyUnselected->Draw();
        Images.Data.rDisclaimerFont->DrawText(668, 525, SUGARFONT_AUTOCENTER_X, 1.0f, "I'm not quite ready.");
    }
    else
    {
        Images.Data.rDisclaimerBtnNotReadySelected->Draw();
        StarlightColor::SetMixer(0.0f, 0.0f, 0.0f, 1.0f);
        Images.Data.rDisclaimerFont->DrawText(668, 525, SUGARFONT_AUTOCENTER_X, 1.0f, "I'm not quite ready.");
        StarlightColor::ClearMixer();
    }

    //--Ready.
    if(mDisclaimerHighlight != PMBTN_DIS_READY)
    {
        Images.Data.rDisclaimerBtnReadyUnselected->Draw();
        Images.Data.rDisclaimerFont->DrawText(668, 630, SUGARFONT_AUTOCENTER_X, 0.9f, "Yeah yeah, let me date my");
        Images.Data.rDisclaimerFont->DrawText(668, 670, SUGARFONT_AUTOCENTER_X, 0.9f, "favourite Youtube celebrity already!");
    }
    else
    {
        Images.Data.rDisclaimerBtnReadySelected->Draw();
        StarlightColor::SetMixer(0.0f, 0.0f, 0.0f, 1.0f);
        Images.Data.rDisclaimerFont->DrawText(668, 630, SUGARFONT_AUTOCENTER_X, 0.9f, "Yeah yeah, let me date my");
        Images.Data.rDisclaimerFont->DrawText(668, 670, SUGARFONT_AUTOCENTER_X, 0.9f, "favourite Youtube celebrity already!");
        StarlightColor::ClearMixer();
    }
}
