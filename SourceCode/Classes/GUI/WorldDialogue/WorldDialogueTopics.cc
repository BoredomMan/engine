//--Base
#include "WorldDialogue.h"

//--Classes
#include "DialogueTopic.h"
#include "VisualLevel.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarFont.h"
#include "SugarLinkedList.h"

//--Definitions
#include "EasingFunctions.h"

//--Libraries
//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "LuaManager.h"

//--[Property Queries]
bool WorldDialogue::IsTopicRead(const char *pName)
{
    //--Get. On no existence, false is returned.
    DialogueTopic *rCheckTopic = (DialogueTopic *)mTopicListing->GetElementByName(pName);
    if(!rCheckTopic) return false;

    //--Return state.
    return (rCheckTopic->GetLevel() >= 1);
}

//--[Manipulators]
void WorldDialogue::WipeTopicData()
{
    //--Clears all topic packages. Only use during startup.
    mTopicListing->ClearList();
}
void WorldDialogue::ActivateTopicsModeAfterDialogue(const char *pNPCName)
{
    //--Scripting access function. Flags topics mode to be activated after the dialogue has finished printing,
    //  instead of closing as normal. Pass NULL to disable this behavior.
    mRunTopicsAfterDialogue = false;
    if(!pNPCName || !strcasecmp(pNPCName, "Null")) return;

    //--Set.
    mRunTopicsAfterDialogue = true;
    strncpy(mRunTopicsAfterDialogueName, pNPCName, STD_NPC_LETTERS - 1);
    SetGoodbyeName("Goodbye");
}
void WorldDialogue::ActivateTopicsMode(const char *pNPCName)
{
    //--Activates Topics Mode with the given NPC as the speaker. Doesn't set portraits or the like,
    //  just sets flags and builds the topic listing.
    mIsTopicsMode = true;
    mIsMajorSequenceMode = false;
    mIsSceneMode = false;
    mTopicTimer = 0;
    mTopicCursor = 0;
    mTopicScroll = 0;
    BuildTopicListing(pNPCName);
}
void WorldDialogue::SetGoodbyeName(const char *pName)
{
    //--Changes the display name of the "Goodbye" option on the topic listing. Does *not* change which
    //  script gets executed, just the display name!
    //--Every time topics gets activated, the name reverts to "Goodbye" so it needs to be re-set each
    //  time if you want it changed.
    if(!pName || !mGoodbyeTopic) return;
    mGoodbyeTopic->SetDisplayName(pName);
}
void WorldDialogue::RegisterTopic(const char *pName, const char *pDisplayName, int pStartingLevel)
{
    //--Adds a new topic to the database. If the topic already exists, then its level is increased
    //  if the requested level is higher.
    if(!pName || !pDisplayName) return;
    DialogueTopic *rCheckTopic = (DialogueTopic *)mTopicListing->GetElementByName(pName);
    if(rCheckTopic)
    {
        if(rCheckTopic->GetLevel() < pStartingLevel) rCheckTopic->SetLevel(pStartingLevel);
        return;
    }

    //--Allocate and initialize.
    DialogueTopic *nTopic = new DialogueTopic();
    nTopic->SetName(pName);
    nTopic->SetDisplayName(pDisplayName);
    nTopic->SetLevel(pStartingLevel);

    //--Register.
    mTopicListing->AddElement(pName, nTopic, &RootObject::DeleteThis);
}
void WorldDialogue::RegisterTopicFor(const char *pTopicName, const char *pNPCName, int pStartingLevel)
{
    //--Informs the given topic that the given NPC has a response to it if the topic is at least the given level.
    if(!pTopicName || !pNPCName) return;

    //--Make sure the topic actually exists.
    DialogueTopic *rCheckTopic = (DialogueTopic *)mTopicListing->GetElementByName(pTopicName);
    if(!rCheckTopic) return;

    //--Add the NPC. It will internally fail if they were already registered.
    rCheckTopic->RegisterNPC(pNPCName, pStartingLevel);
}
void WorldDialogue::UnlockTopic(const char *pTopicName, int pMinimumLevel)
{
    //--Get the named topic. If its level is less than the provided level, increase it. This effectively unlocks
    //  the state of a topic. Remember that topics at -1 never appear on the topic listing.
    DialogueTopic *rCheckTopic = (DialogueTopic *)mTopicListing->GetElementByName(pTopicName);
    if(!rCheckTopic) return;

    //--If it's lower, upgrade it.
    if(rCheckTopic->GetLevel() < pMinimumLevel) rCheckTopic->SetLevel(pMinimumLevel);
}
void WorldDialogue::ClearTopicReadFlag(const char *pTopicName)
{
    //--If the given topic exists, mark it as unread. This is to indicate something has changed and the player
    //  can gain more/new information.
    if(!pTopicName) return;

    //--Make sure the topic actually exists.
    DialogueTopic *rCheckTopic = (DialogueTopic *)mTopicListing->GetElementByName(pTopicName);
    if(!rCheckTopic) return;

    //--Topic found. Flag it as unread.
    rCheckTopic->MarkAsUnread();
}

//--[Core Methods]
void WorldDialogue::BuildTopicListing(const char *pNPCName)
{
    //--Given an NPC's name, builds a list of all topics that that NPC has something to say about. It is possible
    //  for the topic listing to come back empty, in which case the only topic present will be "Goodbye".
    //--The current topic listing is always cleared. Pass NULL to clear it exclusively.
    mrCurrentTopicListing->ClearList();
    if(!pNPCName)
    {
        mrCurrentTopicListing->AddElement("Yes", mGoodbyeTopic);
        return;
    }

    //--Store the name of the NPC.
    strncpy(mCurrentTopicActor, pNPCName, STD_NPC_LETTERS - 1);

    //--Clear any topics with the display name of NULL. Those are topics that are from previous versions and
    //  were deprecated for whatever reason.
    DialogueTopic *rTopic = (DialogueTopic *)mTopicListing->SetToHeadAndReturn();
    while(rTopic)
    {
        if(rTopic->GetDisplayName() == NULL)
        {
            mTopicListing->RemoveRandomPointerEntry();
        }
        rTopic = (DialogueTopic *)mTopicListing->IncrementAndGetRandomPointerEntry();
    }

    //--Iterate across the topics and store all the relevant ones.
    rTopic = (DialogueTopic *)mTopicListing->PushIterator();
    while(rTopic)
    {
        //--If the level of the NPC comes back as -1, the NPC is not listed for this topic.
        if(rTopic->GetLevelOfNPC(pNPCName) == -1)
        {
        }
        //--If the level of the topic comes back as -1, then the topic has not been unlocked yet.
        else if(rTopic->GetLevel() == -1)
        {
        }
        //--NPC has something to say, add them.
        else
        {
            //--The name stores whether or not this topic has been discussed before. If dicussed before,
            //  the name is "Yes". Otherwise, it's "No".
            if(rTopic->HasDiscussedBefore(pNPCName))
            {
                mrCurrentTopicListing->AddElement("Yes", rTopic);
            }
            else
            {
                mrCurrentTopicListing->AddElement("No", rTopic);
            }
        }

        //--Next.
        rTopic = (DialogueTopic *)mTopicListing->AutoIterate();
    }

    //--The "Goodbye" topic is always added last. It always gets the "Yes" case since it's never "new".
    mrCurrentTopicListing->AddElement("Yes", mGoodbyeTopic);
}

//--[Update]
void WorldDialogue::UpdateTopicsMode()
{
    //--Timer.
    mTopicTimer ++;

    //--Setup.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Pressing up decrements the topic cursor by 2.
    if(rControlManager->IsFirstPress("Up"))
    {
        //--Move.
        if(mTopicCursor >= 2)
        {
            mTopicCursor -= 2;
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }

        //--If the cursor is close to the top of the scroll, move the scroll. Scroll moves in increments of 2.
        if(mTopicScroll > 0 && mTopicCursor < mTopicScroll + 4)
        {
            mTopicScroll -= 2;
        }
    }
    //--Down, scrolls positively by 2.
    else if(rControlManager->IsFirstPress("Down"))
    {
        //--Move.
        if(mTopicCursor < mrCurrentTopicListing->GetListSize() - 2)
        {
            mTopicCursor += 2;
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }

        //--If the cursor is close to the top of the scroll, move the scroll. Scroll moves in increments of 2.
        int tMaxTopicScroll = (mrCurrentTopicListing->GetListSize() - 10);
        if(tMaxTopicScroll > 0 && tMaxTopicScroll % 2 == 1) tMaxTopicScroll ++;
        if(mTopicScroll < tMaxTopicScroll && mTopicCursor >= mTopicScroll + 6)
        {
            mTopicScroll += 2;
        }
    }
    //--Left, if the index is odd decrements.
    else if(rControlManager->IsFirstPress("Left"))
    {
        if(mTopicCursor % 2 == 1)
        {
            mTopicCursor --;
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
    }
    //--Right, if the index is even increments.
    else if(rControlManager->IsFirstPress("Right"))
    {
        if(mTopicCursor % 2 == 0 && mTopicCursor < mrCurrentTopicListing->GetListSize() - 1)
        {
            mTopicCursor ++;
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
    }
    //--Activate will execute the given topic script.
    else if(rControlManager->IsFirstPress("Activate"))
    {
        //--Get the topic in question.
        DialogueTopic *rSelectedTopic = (DialogueTopic *)mrCurrentTopicListing->GetElementBySlot(mTopicCursor);
        if(!rSelectedTopic) return;

        //--Normal case: Execute.
        if(rSelectedTopic != mGoodbyeTopic)
        {
            //--Flags.
            mIsTopicsMode = false;

            //--Build path.
            char tPathBuffer[256];
            sprintf(tPathBuffer, "%s/%s/100 %s.lua", xTopicsDirectory, mCurrentTopicActor, rSelectedTopic->GetInternalName());

            //--Auto-update the topic.
            rSelectedTopic->SetNPCLevel(mCurrentTopicActor, rSelectedTopic->GetLevel());

            //--Run.
            Clear();
            LuaManager::Fetch()->ExecuteLuaFile(tPathBuffer, 1, "S", "Hello");

            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        //--Goodbye case: Execute the goodbye text. The dialogue closes afterwards.
        else
        {
            //--Flags.
            mIsTopicsMode = false;

            //--Build path.
            char tPathBuffer[256];
            sprintf(tPathBuffer, "%s/%s/100 %s.lua", xTopicsDirectory, mCurrentTopicActor, "Goodbye");

            //--Run.
            Clear();
            LuaManager::Fetch()->ExecuteLuaFile(tPathBuffer);

            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
    }
    //--Cancel will move the cursor to the "Goodbye" entry.
    else if(rControlManager->IsFirstPress("Cancel"))
    {
        //--Set.
        mTopicCursor = mrCurrentTopicListing->GetListSize() - 1;
        if(mTopicCursor < 0) mTopicCursor = 0;

        //--Handle the topic scroll.
        mTopicScroll = (mrCurrentTopicListing->GetListSize() - 10);
        if(mTopicScroll % 2 == 1 && mTopicScroll >= 0) mTopicScroll ++;
        if(mTopicScroll < 0) mTopicScroll = 0;

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }
}

//--[Render]
void WorldDialogue::RenderTopicsMode()
{
    //--[Documentation]
    //--Rendering routine for topics mode. Shares a lot of code with Major Sequences since portraits are expected
    //  to be shown during a topic sequence. Does not have the top dialogue box, though.
    if(!Images.mIsReady) return;

    //--[Mixing]
    float tAlphaFactor = 1.0f;
    if(mHidingTimer != -1) tAlphaFactor = 1.0f - EasingFunction::QuadraticInOut(mHidingTimer, (float)WD_HIDING_TICKS);

    //--[Backing and Widescreen]
    //--Base borders.
    float cLft = 0.0f;
    float cTop = 0.0f;
    float cRgt = VIRTUAL_CANVAS_X;
    float cBot = VIRTUAL_CANVAS_Y;

    //--Render a black border under everything else. This makes portraits pop out of the screen better.
    glColor4f(0.1f, 0.1f, 0.1f, 0.80f * tAlphaFactor);
    glDisable(GL_TEXTURE_2D);
    glBegin(GL_QUADS);
        glVertex2f(cLft, cTop);
        glVertex2f(cRgt, cTop);
        glVertex2f(cRgt, cBot);
        glVertex2f(cLft, cBot);
    glEnd();
    glEnable(GL_TEXTURE_2D);
    glColor3f(1.0f, 1.0f, 1.0f);

    //--[Backing]
    //--Backing image
    Images.Data.rNamelessBox->Draw();

    //--[Topic Listing]
    //--Constants.
    float cTextHeight = Images.Data.rDialogueFont->GetTextHeight() * mTextScale;

    //--Positioning.
    float cCursorIndent = 12.0f;
    float tCursorX =  42.0f;
    float tCursorY = 592.0f;
    glTranslatef(tCursorX, tCursorY, 0.0f);

    //--Render the topics.
    int i = 0;
    int tActualRenders = 0;
    DialogueTopic *rTopic = (DialogueTopic *)mrCurrentTopicListing->PushIterator();
    while(rTopic)
    {
        //--Value is less than the topic scroll, so don't render it.
        if(i < mTopicScroll)
        {
            i ++;
            rTopic = (DialogueTopic *)mrCurrentTopicListing->AutoIterate();
            continue;
        }

        //--Stop rendering if 10 topics render.
        if(tActualRenders >= 10)
        {
            mrCurrentTopicListing->PopIterator();
            break;
        }

        //--Compute position.
        float tPosX = ((i-mTopicScroll) % 2) * 460.0f;
        float tPosY = ((i-mTopicScroll) / 2) * (cTextHeight);

        //--If this dialogue has been seen before, nothing but its name gets rendered.
        if(mrCurrentTopicListing->GetIteratorName()[0] == 'Y')
        {

        }
        //--Otherwise, render an indicator that there's something new.
        else
        {
            glColor4f(0.9f, 0.0f, 0.9f, 1.0f * tAlphaFactor);
        }

        //--Render the topic, indented slightly for the cursor.
        Images.Data.rDialogueFont->DrawTextArgs(tPosX + cCursorIndent, tPosY, 0, mTextScale, "%s", rTopic->GetDisplayName());

        //--Next.
        i ++;
        tActualRenders ++;
        glColor4f(1.0f, 1.0f, 1.0f, 1.0f * tAlphaFactor);
        rTopic = (DialogueTopic *)mrCurrentTopicListing->AutoIterate();
    }

    //--Render the cursor.
    float cCursorWid = 5.0f;
    float cCursorHei = (cTextHeight * 0.80f) - 2.0f;
    float tTopicCursorX = ((mTopicCursor-mTopicScroll) % 2) * 460.0f;
    float tTopicCursorY = (((mTopicCursor-mTopicScroll) / 2) * (cTextHeight)) + 5.0f;

    //--Render.
    glColor4f(1.0f, 1.0f, 1.0f, 1.0f * tAlphaFactor);
    glDisable(GL_TEXTURE_2D);
    glBegin(GL_TRIANGLES);
        glVertex2f(tTopicCursorX,              tTopicCursorY);
        glVertex2f(tTopicCursorX + cCursorWid, tTopicCursorY + cCursorHei / 2.0f);
        glVertex2f(tTopicCursorX,              tTopicCursorY + cCursorHei);
    glEnd();
    glEnable(GL_TEXTURE_2D);

    //--Clean up.
    glColor3f(1.0f, 1.0f, 1.0f);
    glTranslatef(tCursorX * -1, tCursorY * -1, 0.0f);

    //--[Portraits]
    //--Render the portraits with the subroutine.
    RenderDialogueActors(1.0f);

    //--[Flash]
    RenderFlash();
}
