//--Base
#include "WorldDialogue.h"

//--Classes
#include "DialogueActor.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "EasingFunctions.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "DisplayManager.h"
#include "LuaManager.h"

//--[Manipulators]
void WorldDialogue::SetVisualNovelMode(bool pFlag)
{
    mIsVisualNovel = pFlag;
}
void WorldDialogue::SetVisualNovelBackground(const char *pPath)
{
    //--Before we do anything else, see if there's a remap available.
    const char *rUsePath = pPath;
    const char *rCheckPtr = (char *)VN.mBackgroundRemaps->GetElementByName(pPath);
    if(rCheckPtr) rUsePath = rCheckPtr;

    //--Set values.
    VN.mBackgroundTimer = 0;
    VN.rPrevBackground = VN.rCurBackground;
    VN.rCurBackground = (SugarBitmap *)DataLibrary::Fetch()->GetEntry(rUsePath);
}
void WorldDialogue::RegisterBackgroundRemap(const char *pRemapName, const char *pPath)
{
    if(!pRemapName || !pPath) return;
    VN.mBackgroundRemaps->AddElement(pRemapName, InitializeString(pPath), &FreeThis);
}

//--[Update]
void WorldDialogue::UpdateVisualNovel()
{
    //--Background timer.
    if(VN.mBackgroundTimer < WD_VN_BG_TICKS)
    {
        VN.mBackgroundTimer ++;
    }

    //--Movement. Actors can move around the screen and this update cycle handles that.
    for(int i = 0; i < WD_DIALOGUE_ACTOR_SLOTS; i ++)
    {
        //--Flag.
        bool tExecFinisher = false;

        //--If the actor is not moving, do nothing here.
        if(VN.mActorMoveFlags[i] == WD_VN_MOVETPYE_NONE)
        {
        }
        //--Teleport. Moves instantly and finishes in the same tick.
        else if(VN.mActorMoveFlags[i] == WD_VN_MOVETPYE_TELEPORT)
        {
            VN.mActorMoveFlags[i] = WD_VN_MOVETPYE_NONE;
            VN.mActorCurrents[i] = VN.mActorTargets[i];
            tExecFinisher = true;
        }
        //--Linear. Moves a fixed percentage of the distance each tick.
        else if(VN.mActorMoveFlags[i] == WD_VN_MOVETPYE_LINEAR)
        {
            //--Increment timer.
            VN.mActorTimers[i] ++;
            float tPercent = (float)VN.mActorTimers[i] / (float)VN.mActorTimersMax[i];

            //--Set position if not complete yet.
            if(tPercent < 1.0f)
            {
                VN.mActorCurrents[i] = VN.mActorStarts[i] + ((VN.mActorTargets[i] - VN.mActorStarts[i]) * tPercent);
            }
            //--Completion case.
            else
            {
                VN.mActorMoveFlags[i] = WD_VN_MOVETPYE_NONE;
                VN.mActorCurrents[i] = VN.mActorTargets[i];
                tExecFinisher = true;
            }
        }
        //--Quadratic-In. Uses an easing function.
        else if(VN.mActorMoveFlags[i] == WD_VN_MOVETPYE_QUADIN)
        {
            //--Increment timer.
            VN.mActorTimers[i] ++;
            float tPercent = EasingFunction::QuadraticIn((float)VN.mActorTimers[i], (float)VN.mActorTimersMax[i]);

            //--Set position if not complete yet.
            if(tPercent < 1.0f)
            {
                VN.mActorCurrents[i] = VN.mActorStarts[i] + ((VN.mActorTargets[i] - VN.mActorStarts[i]) * tPercent);
            }
            //--Completion case.
            else
            {
                VN.mActorMoveFlags[i] = WD_VN_MOVETPYE_NONE;
                VN.mActorCurrents[i] = VN.mActorTargets[i];
                tExecFinisher = true;
            }
        }
        //--Quadratic-Out. Uses an easing function.
        else if(VN.mActorMoveFlags[i] == WD_VN_MOVETPYE_QUADOUT)
        {
            //--Increment timer.
            VN.mActorTimers[i] ++;
            float tPercent = EasingFunction::QuadraticOut((float)VN.mActorTimers[i], (float)VN.mActorTimersMax[i]);

            //--Set position if not complete yet.
            if(tPercent < 1.0f)
            {
                VN.mActorCurrents[i] = VN.mActorStarts[i] + ((VN.mActorTargets[i] - VN.mActorStarts[i]) * tPercent);
            }
            //--Completion case.
            else
            {
                VN.mActorMoveFlags[i] = WD_VN_MOVETPYE_NONE;
                VN.mActorCurrents[i] = VN.mActorTargets[i];
                tExecFinisher = true;
            }
        }
        //--Quadratic-In-Out. Uses an easing function.
        else if(VN.mActorMoveFlags[i] == WD_VN_MOVETPYE_QUADINOUT)
        {
            //--Increment timer.
            VN.mActorTimers[i] ++;
            float tPercent = EasingFunction::QuadraticInOut((float)VN.mActorTimers[i], (float)VN.mActorTimersMax[i]);

            //--Set position if not complete yet.
            if(tPercent < 1.0f)
            {
                VN.mActorCurrents[i] = VN.mActorStarts[i] + ((VN.mActorTargets[i] - VN.mActorStarts[i]) * tPercent);
            }
            //--Completion case.
            else
            {
                VN.mActorMoveFlags[i] = WD_VN_MOVETPYE_NONE;
                VN.mActorCurrents[i] = VN.mActorTargets[i];
                tExecFinisher = true;
            }
        }

        //--Post-exec. If flagged, runs a script. Can be preset to do audio. Cleans itself up when executed.
        if(tExecFinisher)
        {
            //--Null. Do nothing.
            if(!VN.mActorPostExecs[i])
            {

            }
            //--Special case: If the first part is "SOUND|", play a sound effect.
            else if(!strncasecmp(VN.mActorPostExecs[i], "SOUND|", 6))
            {
                AudioManager::Fetch()->PlaySound(&VN.mActorPostExecs[i][6]);
                free(VN.mActorPostExecs[i]);
                VN.mActorPostExecs[i] = NULL;
            }
            //--Otherwise, this is a script. Treat it as such.
            else
            {
                //--Because the script could theoretically start another movement sequence, we need to deallocate this now.
                //  Store the script path.
                char tStoragePath[256];
                strcpy(tStoragePath, VN.mActorPostExecs[i]);

                //--Deallocate.
                free(VN.mActorPostExecs[i]);
                VN.mActorPostExecs[i] = NULL;

                //--Execute.
                LuaManager::Fetch()->ExecuteLuaFile(tStoragePath);
            }
        }
    }
}

//--[Drawing]
void WorldDialogue::RenderVisualNovelActors(float pGlobalAlpha)
{
    //--[Documentation]
    //--Renders the DialogueActors, called from the primary rendering cycle. This will only be called during
    //  a major dialogue sequence.
    //--The Visual Novel version uses different positioning rules but follows most of the same logic as the
    //  major sequence code.
    float cScale = 0.60f;
    float cXOffset = 0.0f;
    if(DisplayManager::xLowDefinitionFlag)
    {
        cXOffset = 64.0f;
        cScale = cScale * 2.0f;
    }

    //--[Backgrounds]
    //--Renders the backgrounds for Visual Novel mode.
    float tPrevPercent = 1.0f - EasingFunction::QuadraticInOut(VN.mBackgroundTimer, (float)WD_VN_BG_TICKS);
    float tCurrPercent = 1.0f - tPrevPercent;
    if(tPrevPercent > 0.0f && VN.rPrevBackground)
    {
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pGlobalAlpha);
        VN.rPrevBackground->Draw();
    }
    if(tCurrPercent > 0.0f && VN.rCurBackground)
    {
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, tCurrPercent * pGlobalAlpha);
        VN.rCurBackground->Draw();
    }

    //--[Mixing]
    float tAlphaFactor = 1.0f;
    if(mIsMajorSequenceMode) tAlphaFactor = EasingFunction::QuadraticInOut(mMajorSequenceTimer, WD_MAJOR_SEQUENCE_FADE_TICKS / 2);
    if(mHidingTimer != -1) tAlphaFactor = 1.0f - EasingFunction::QuadraticInOut(mHidingTimer, (float)WD_HIDING_TICKS);
    glColor4f(1.0f, 1.0f, 1.0f, 1.0f * tAlphaFactor);

    //--Iterate across the rendering list for back characters.
    for(int i = 0; i < WD_DIALOGUE_ACTOR_SLOTS; i ++)
    {
        //--[Error Check]
        //--If the actor is not "fulldark", skip them. We are rendering characters who are not speaking.
        if(mDialogueActors[i] && !mDialogueActors[i]->IsFullDark()) continue;

        //--Flip flag.
        uint32_t tFlipFlags = 0x00;

        //--Position.
        glTranslatef(VN.mActorCurrents[i], mDimensions.mTop, 0.0f);
        glScalef(cScale, cScale, 1.0f);

        //--[No Crossfade]
        //--When not crossfading, render the character at the listed alpha.
        if(mPreviousFadeTimers[i] >= WD_CHARACTER_CROSSFADE_TICKS || mMajorSequenceTimer < WD_MAJOR_SEQUENCE_FADE_TICKS)
        {
            if(mDialogueActors[i])
            {
                mDialogueActors[i]->RenderAtVN(cXOffset, 0.0f, pGlobalAlpha, tFlipFlags);
            }
        }
        //--[Crossfade]
        //--When crossfading, render the previous image and the current image at complementary alphas.
        else
        {
            //--Compute alphas.
            float cFadeOutAlpha = (1.0f - EasingFunction::QuadraticInOut(mPreviousFadeTimers[i], WD_CHARACTER_CROSSFADE_TICKS)) * pGlobalAlpha;
            float cFadeInAlpha = 1.0f - cFadeOutAlpha;

            //--Fade-out character. Doesn't always exist.
            if(rPreviousActors[i] && rPreviousImages[i])
            {
                rPreviousActors[i]->RenderAtVN(rPreviousImages[i], cXOffset, 0.0f, cFadeOutAlpha, tFlipFlags);
            }

            //--Fade-in character. Doesn't always exist.
            if(mDialogueActors[i])
            {
                mDialogueActors[i]->RenderAtVN(cXOffset, 0.0f, cFadeInAlpha, tFlipFlags);
            }
        }

        //--[Clean]
        glScalef(1.0f / cScale, 1.0f / cScale, 1.0f);
        glTranslatef(-VN.mActorCurrents[i], -mDimensions.mTop, 0.0f);
    }

    //--Now do it again for the front characters.
    for(int i = 0; i < WD_DIALOGUE_ACTOR_SLOTS; i ++)
    {
        //--[Error Check]
        //--Fast-access pointer.
        if(mDialogueActors[i] && mDialogueActors[i]->IsFullDark()) continue;

        //--Flip flag.
        uint32_t tFlipFlags = 0x00;

        //--Position.
        glTranslatef(VN.mActorCurrents[i], mDimensions.mTop, 0.0f);
        glScalef(cScale, cScale, 1.0f);

        //--[No Crossfade]
        //--When not crossfading, render the character at the listed alpha.
        if(mPreviousFadeTimers[i] >= WD_CHARACTER_CROSSFADE_TICKS || mMajorSequenceTimer < WD_MAJOR_SEQUENCE_FADE_TICKS)
        {
            if(mDialogueActors[i])
            {
                mDialogueActors[i]->RenderAtVN(cXOffset, 0.0f, pGlobalAlpha, tFlipFlags);
            }
        }
        //--[Crossfade]
        //--When crossfading, render the previous image and the current image at complementary alphas.
        else
        {
            //--Compute alphas.
            float cFadeOutAlpha = (1.0f - EasingFunction::QuadraticInOut(mPreviousFadeTimers[i], WD_CHARACTER_CROSSFADE_TICKS)) * pGlobalAlpha;
            float cFadeInAlpha = 1.0f - cFadeOutAlpha;

            //--Fade-out character. Doesn't always exist.
            if(rPreviousActors[i] && rPreviousImages[i])
            {
                rPreviousActors[i]->RenderAtVN(rPreviousImages[i], cXOffset, 0.0f, cFadeOutAlpha, tFlipFlags);
            }

            //--Fade-in character. Doesn't always exist.
            if(mDialogueActors[i])
            {
                mDialogueActors[i]->RenderAtVN(cXOffset, 0.0f, cFadeInAlpha, tFlipFlags);
            }
        }

        //--[Clean]
        glScalef(1.0f / cScale, 1.0f / cScale, 1.0f);
        glTranslatef(-VN.mActorCurrents[i], -mDimensions.mTop, 0.0f);
    }

    //--Clean.
    glColor3f(1.0f, 1.0f, 1.0f);
}
