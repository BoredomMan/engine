//--Base
#include "DeckEditor.h"

//--Classes
//--CoreClasses
#include "SugarBitmap.h"
#include "SugarFont.h"

//--Definitions
#include "EasingFunctions.h"
#include "HitDetection.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "DisplayManager.h"
#include "LuaManager.h"

//======================================= Local Definitions =======================================
//--Headers
#define DE_DECKSIZE_X 905.0f
#define DE_DECKSIZE_Y 595.0f

//--Sizings
#define DE_HANDPOS_TITLE_X 39.0f
#define DE_HANDPOS_TITLE_Y -17.0f
#define DE_HANDPOS_COUNTS_X 242.0f
#define DE_HANDPOS_COUNTS_Y -17.0f
#define DE_HANDPOS_BTNADD_X 260.0f
#define DE_HANDPOS_BTNADD_Y 26.0f
#define DE_HANDPOS_BTNSUB_X -15.0f
#define DE_HANDPOS_BTNSUB_Y 26.0f
#define DE_HANDPOS_CARDSTART_X 13.0f
#define DE_HANDPOS_CARDSTART_Y -3.0f
#define DE_HANDPOS_CARDTILE_X 22.0f
#define DE_HANDPOS_CARDTILE_Y 0.0f
#define DE_HANDPOS_HELP_X -3.0f
#define DE_HANDPOS_HELP_Y 77.0f

#define DR_COLUMN_0 268.0f
#define DR_COLUMN_1 180.0f
#define DR_COLUMN_2 144.0f

#define DE_HANDPOS_ROW_0 121.0f
#define DE_HANDPOS_ROW_1 255.0f
#define DE_HANDPOS_ROW_2 386.0f
#define DE_HANDPOS_ROW_3 518.0f
#define DE_HANDPOS_ROW_4 649.0f

//--General Positions
#define DE_HANDPOS_COLUMN_LFT 110.0f
#define DE_HANDPOS_COLUMN_TOP 180.0f
#define DE_HANDPOS_COLUMN_WID 311.0f
#define DE_HANDPOS_COLUMN_HEI 110.0f

//--Statistics Positions
#define DE_STATS_HEADER_X 1080.0f
#define DE_STATS_HEADER_Y  180.0f
#define DE_STATS_COLUMN_LFT 915.0f
#define DE_STATS_COLUMN_TOP 150.0f
#define DE_STATS_COLUMN_WID 160.0f
#define DE_STATS_COLUMN_HEI  50.0f
#define DE_STATS_VAL_OFFSET_X  50.0f
#define DE_STATS_VAL_OFFSET_Y  25.0f

//--Deck Statistics Positions
#define DE_DECKSTAT_HEADER_X 1080.0f
#define DE_DECKSTAT_HEADER_Y  400.0f
#define DE_DECKSTAT_COLUMN_LFT 915.0f
#define DE_DECKSTAT_COLUMN_TOP 345.0f
#define DE_DECKSTAT_COLUMN_WID 160.0f
#define DE_DECKSTAT_COLUMN_HEI  50.0f
#define DE_DECKSTAT_VAL_OFFSET_X  50.0f
#define DE_DECKSTAT_VAL_OFFSET_Y  25.0f

//--Error Codes
#define DE_ERRORPOS_X 1098.0f
#define DE_ERRORPOS_Y 642.0f
#define DE_ERROR_NONE -1
#define DE_ERROR_TOO_MANY_CARDS 0
#define DE_ERROR_TOO_FEW_CARDS 1

//=========================================== System ==============================================
DeckEditor::DeckEditor()
{
    //--[RootObject]
    //--System
    mType = POINTER_TYPE_DECKEDITOR;

    //--[DeckEditor]
    //--System
    mIsActive = false;
    mPostExecScript = NULL;

    //--Visibility
    mIsVisible = false;
    mVisibilityTimer = 0;

    //--Help Handler
    mHelpVisible = false;
    mHelpVisibleTimer = 0;
    mHelpExecPath = NULL;
    mHelpHeader = NULL;
    memset(mHelpStrings, 0, sizeof(char *) * DE_HELP_MAXLINES);

    //--Buttons and Positions
    memset(&mHands, 0, sizeof(DeckEditorHand) * DE_HAND_TOTAL);
    memset(&mButtons, 0, sizeof(DeckEditorButton) * DE_BTN_TOTAL);

    //--Deck Statistics
    mDeckSizeMin = 0;
    mDeckSizeMax = 0;

    //--Other Strings/Values
    mAttackBonus = 0;
    mDefendBonus = 0;
    mStartShieldBonus = 0;
    memset(&mSentenceLenBonus, 0, sizeof(mSentenceLenBonus));
    mBonusWater = 0;
    mBonusFire = 0;
    mBonusWind = 0;
    mBonusEarth = 0;
    mBonusLife = 0;
    mBonusDeath = 0;

    //--Images
    memset(&Images, 0, sizeof(Images));
}
DeckEditor::~DeckEditor()
{
    free(mPostExecScript);
    free(mHelpExecPath);
    free(mHelpHeader);
    for(int i = 0; i < DE_HELP_MAXLINES; i ++) free(mHelpStrings[i]);
}
void DeckEditor::Construct()
{
    //--[Documentation and Setup]
    //--Crossload image pointers from the DataLibrary.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();

    //--[Crossload]
    //--Fonts
    Images.Data.rFontHeader = rDataLibrary->GetFont("Deck Editor Header");
    Images.Data.rFontMainLg = rDataLibrary->GetFont("Deck Editor Main");
    Images.Data.rFontMainSm = rDataLibrary->GetFont("Deck Editor Small");
    Images.Data.rFontButton = rDataLibrary->GetFont("Deck Editor Button");

    //--UI Parts
    Images.Data.rBackground     = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DeckEd/Backing");
    Images.Data.rHelpInlay      = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DeckEd/HelpInlay");
    Images.Data.rButtonCancel   = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DeckEd/ButtonCancel");
    Images.Data.rButtonReset    = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DeckEd/ButtonReset");
    Images.Data.rButtonSave     = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DeckEd/ButtonSave");
    Images.Data.rButtonSmallAdd = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DeckEd/ButtonSmallAdd");
    Images.Data.rButtonSmallSub = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DeckEd/ButtonSmallSub");
    Images.Data.rButtonSmallHlp = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/AdventureUI/DeckEd/ButtonQuestion");

    //--Lookups
    Images.Data.rExitBtnLookups[DE_BTN_ACCEPT]   = Images.Data.rButtonSave;
    Images.Data.rExitBtnLookups[DE_BTN_CANCEL]   = Images.Data.rButtonCancel;
    Images.Data.rExitBtnLookups[DE_BTN_DEFAULTS] = Images.Data.rButtonReset;

    //--[Verify]
    //--Make sure none of the pointers are NULL.
    Images.mIsReady = VerifyStructure(&Images.Data, sizeof(Images.Data), sizeof(void *));
    if(!Images.mIsReady) return;

    //--[Elemental Card References]
    //--We need these from the combat UI to render what each card type is. The renderer will
    //  check if any are NULL.
    SugarBitmap *rWaterCard     = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/DollManor/Combat/Card_Water");
    SugarBitmap *rFireCard      = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/DollManor/Combat/Card_Fire");
    SugarBitmap *rWindCard      = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/DollManor/Combat/Card_Wind");
    SugarBitmap *rEarthCard     = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/DollManor/Combat/Card_Earth");
    SugarBitmap *rLifeCard      = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/DollManor/Combat/Card_Life");
    SugarBitmap *rDeathCard     = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/DollManor/Combat/Card_Death");
    SugarBitmap *rAttackCard    = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/DollManor/Combat/Card_Attack");
    SugarBitmap *rDefendCard    = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/DollManor/Combat/Card_Defend");
    SugarBitmap *rBridgeAndCard = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/DollManor/Combat/Card_And");
    SugarBitmap *rBridgeOfCard  = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/DollManor/Combat/Card_Of");

    //--[Hand Placement]
    //--There is a 'Hand' used for each type of card the player can edit. Buttons are arranged relative to the
    //  positions of the hands.
    SetHandBasicProperties(DE_HAND_WATER,     DR_COLUMN_2 + (DE_HANDPOS_COLUMN_WID * 0.0f), DE_HANDPOS_ROW_2, DE_HANDPOS_COLUMN_WID, DE_HANDPOS_COLUMN_HEI, "Water",  rWaterCard);
    SetHandBasicProperties(DE_HAND_FIRE,      DR_COLUMN_2 + (DE_HANDPOS_COLUMN_WID * 1.0f), DE_HANDPOS_ROW_2, DE_HANDPOS_COLUMN_WID, DE_HANDPOS_COLUMN_HEI, "Fire",   rFireCard);
    SetHandBasicProperties(DE_HAND_WIND,      DR_COLUMN_1 + (DE_HANDPOS_COLUMN_WID * 0.0f), DE_HANDPOS_ROW_3, DE_HANDPOS_COLUMN_WID, DE_HANDPOS_COLUMN_HEI, "Wind",   rWindCard);
    SetHandBasicProperties(DE_HAND_EARTH,     DR_COLUMN_1 + (DE_HANDPOS_COLUMN_WID * 1.0f), DE_HANDPOS_ROW_3, DE_HANDPOS_COLUMN_WID, DE_HANDPOS_COLUMN_HEI, "Earth",  rEarthCard);
    SetHandBasicProperties(DE_HAND_LIFE,      DR_COLUMN_0 + (DE_HANDPOS_COLUMN_WID * 0.0f), DE_HANDPOS_ROW_4, DE_HANDPOS_COLUMN_WID, DE_HANDPOS_COLUMN_HEI, "Life",   rLifeCard);
    SetHandBasicProperties(DE_HAND_DEATH,     DR_COLUMN_0 + (DE_HANDPOS_COLUMN_WID * 1.0f), DE_HANDPOS_ROW_4, DE_HANDPOS_COLUMN_WID, DE_HANDPOS_COLUMN_HEI, "Death",  rDeathCard);
    SetHandBasicProperties(DE_HAND_ATTACK,    DR_COLUMN_0 + (DE_HANDPOS_COLUMN_WID * 0.0f), DE_HANDPOS_ROW_0, DE_HANDPOS_COLUMN_WID, DE_HANDPOS_COLUMN_HEI, "Attack", rAttackCard);
    SetHandBasicProperties(DE_HAND_DEFEND,    DR_COLUMN_0 + (DE_HANDPOS_COLUMN_WID * 1.0f), DE_HANDPOS_ROW_0, DE_HANDPOS_COLUMN_WID, DE_HANDPOS_COLUMN_HEI, "Defend", rDefendCard);
    SetHandBasicProperties(DE_HAND_BRIDGEAND, DR_COLUMN_1 + (DE_HANDPOS_COLUMN_WID * 0.0f), DE_HANDPOS_ROW_1, DE_HANDPOS_COLUMN_WID, DE_HANDPOS_COLUMN_HEI, "And",    rBridgeAndCard);
    SetHandBasicProperties(DE_HAND_BRIDGEOF,  DR_COLUMN_1 + (DE_HANDPOS_COLUMN_WID * 1.0f), DE_HANDPOS_ROW_1, DE_HANDPOS_COLUMN_WID, DE_HANDPOS_COLUMN_HEI, "Of",     rBridgeOfCard);

    //--[Button Placement]
    //--Exiting Buttons
    SetButtonProperties(DE_BTN_ACCEPT,   1104.0f, 653.0f, 205.0f, 74.0f, "Accept");
    SetButtonProperties(DE_BTN_CANCEL,    923.0f, 675.0f, 168.0f, 56.0f, "Cancel");
    SetButtonProperties(DE_BTN_DEFAULTS,   52.0f, 672.0f, 168.0f, 56.0f, "Defaults");

    //--Add/Subtract Buttons for each Hand
    SetButtonProperties(DE_BTN_WATERADD,     mHands[DE_HAND_WATER].mPosition.mLft     + DE_HANDPOS_BTNADD_X, mHands[DE_HAND_WATER].mPosition.mTop     + DE_HANDPOS_BTNADD_Y, 24.0f, 24.0f, "+");
    SetButtonProperties(DE_BTN_WATERSUB,     mHands[DE_HAND_WATER].mPosition.mLft     + DE_HANDPOS_BTNSUB_X, mHands[DE_HAND_WATER].mPosition.mTop     + DE_HANDPOS_BTNSUB_Y, 24.0f, 24.0f, "-");
    SetButtonProperties(DE_BTN_FIREADD,      mHands[DE_HAND_FIRE].mPosition.mLft      + DE_HANDPOS_BTNADD_X, mHands[DE_HAND_FIRE].mPosition.mTop      + DE_HANDPOS_BTNADD_Y, 24.0f, 24.0f, "+");
    SetButtonProperties(DE_BTN_FIRESUB,      mHands[DE_HAND_FIRE].mPosition.mLft      + DE_HANDPOS_BTNSUB_X, mHands[DE_HAND_FIRE].mPosition.mTop      + DE_HANDPOS_BTNSUB_Y, 24.0f, 24.0f, "-");
    SetButtonProperties(DE_BTN_WINDADD,      mHands[DE_HAND_WIND].mPosition.mLft      + DE_HANDPOS_BTNADD_X, mHands[DE_HAND_WIND].mPosition.mTop      + DE_HANDPOS_BTNADD_Y, 24.0f, 24.0f, "+");
    SetButtonProperties(DE_BTN_WINDSUB,      mHands[DE_HAND_WIND].mPosition.mLft      + DE_HANDPOS_BTNSUB_X, mHands[DE_HAND_WIND].mPosition.mTop      + DE_HANDPOS_BTNSUB_Y, 24.0f, 24.0f, "-");
    SetButtonProperties(DE_BTN_EARTHADD,     mHands[DE_HAND_EARTH].mPosition.mLft     + DE_HANDPOS_BTNADD_X, mHands[DE_HAND_EARTH].mPosition.mTop     + DE_HANDPOS_BTNADD_Y, 24.0f, 24.0f, "+");
    SetButtonProperties(DE_BTN_EARTHSUB,     mHands[DE_HAND_EARTH].mPosition.mLft     + DE_HANDPOS_BTNSUB_X, mHands[DE_HAND_EARTH].mPosition.mTop     + DE_HANDPOS_BTNSUB_Y, 24.0f, 24.0f, "-");
    SetButtonProperties(DE_BTN_LIFEADD,      mHands[DE_HAND_LIFE].mPosition.mLft      + DE_HANDPOS_BTNADD_X, mHands[DE_HAND_LIFE].mPosition.mTop      + DE_HANDPOS_BTNADD_Y, 24.0f, 24.0f, "+");
    SetButtonProperties(DE_BTN_LIFESUB,      mHands[DE_HAND_LIFE].mPosition.mLft      + DE_HANDPOS_BTNSUB_X, mHands[DE_HAND_LIFE].mPosition.mTop      + DE_HANDPOS_BTNSUB_Y, 24.0f, 24.0f, "-");
    SetButtonProperties(DE_BTN_DEATHADD,     mHands[DE_HAND_DEATH].mPosition.mLft     + DE_HANDPOS_BTNADD_X, mHands[DE_HAND_DEATH].mPosition.mTop     + DE_HANDPOS_BTNADD_Y, 24.0f, 24.0f, "+");
    SetButtonProperties(DE_BTN_DEATHSUB,     mHands[DE_HAND_DEATH].mPosition.mLft     + DE_HANDPOS_BTNSUB_X, mHands[DE_HAND_DEATH].mPosition.mTop     + DE_HANDPOS_BTNSUB_Y, 24.0f, 24.0f, "-");
    SetButtonProperties(DE_BTN_ATTACKADD,    mHands[DE_HAND_ATTACK].mPosition.mLft    + DE_HANDPOS_BTNADD_X, mHands[DE_HAND_ATTACK].mPosition.mTop    + DE_HANDPOS_BTNADD_Y, 24.0f, 24.0f, "+");
    SetButtonProperties(DE_BTN_ATTACKSUB,    mHands[DE_HAND_ATTACK].mPosition.mLft    + DE_HANDPOS_BTNSUB_X, mHands[DE_HAND_ATTACK].mPosition.mTop    + DE_HANDPOS_BTNSUB_Y, 24.0f, 24.0f, "-");
    SetButtonProperties(DE_BTN_DEFENDADD,    mHands[DE_HAND_DEFEND].mPosition.mLft    + DE_HANDPOS_BTNADD_X, mHands[DE_HAND_DEFEND].mPosition.mTop    + DE_HANDPOS_BTNADD_Y, 24.0f, 24.0f, "+");
    SetButtonProperties(DE_BTN_DEFENDSUB,    mHands[DE_HAND_DEFEND].mPosition.mLft    + DE_HANDPOS_BTNSUB_X, mHands[DE_HAND_DEFEND].mPosition.mTop    + DE_HANDPOS_BTNSUB_Y, 24.0f, 24.0f, "-");
    SetButtonProperties(DE_BTN_BRIDGEANDADD, mHands[DE_HAND_BRIDGEAND].mPosition.mLft + DE_HANDPOS_BTNADD_X, mHands[DE_HAND_BRIDGEAND].mPosition.mTop + DE_HANDPOS_BTNADD_Y, 24.0f, 24.0f, "+");
    SetButtonProperties(DE_BTN_BRIDGEANDSUB, mHands[DE_HAND_BRIDGEAND].mPosition.mLft + DE_HANDPOS_BTNSUB_X, mHands[DE_HAND_BRIDGEAND].mPosition.mTop + DE_HANDPOS_BTNSUB_Y, 24.0f, 24.0f, "-");
    SetButtonProperties(DE_BTN_BRIDGEOFADD,  mHands[DE_HAND_BRIDGEOF].mPosition.mLft  + DE_HANDPOS_BTNADD_X, mHands[DE_HAND_BRIDGEOF].mPosition.mTop  + DE_HANDPOS_BTNADD_Y, 24.0f, 24.0f, "+");
    SetButtonProperties(DE_BTN_BRIDGEOFSUB,  mHands[DE_HAND_BRIDGEOF].mPosition.mLft  + DE_HANDPOS_BTNSUB_X, mHands[DE_HAND_BRIDGEOF].mPosition.mTop  + DE_HANDPOS_BTNSUB_Y, 24.0f, 24.0f, "-");

    //--Help Buttons
    SetButtonProperties(DE_BTN_HELP_DECKSIZE, 1165.0f, 594.0f, 24.0f, 24.0f, " ");
    SetButtonProperties(DE_BTN_HELP_WATER,     224, 359, 24.0f, 24.0f, " ");
    SetButtonProperties(DE_BTN_HELP_FIRE,      534, 359, 24.0f, 24.0f, " ");
    SetButtonProperties(DE_BTN_HELP_WIND,      260, 491, 24.0f, 24.0f, " ");
    SetButtonProperties(DE_BTN_HELP_EARTH,     570, 491, 24.0f, 24.0f, " ");
    SetButtonProperties(DE_BTN_HELP_LIFE,      347, 623, 24.0f, 24.0f, " ");
    SetButtonProperties(DE_BTN_HELP_DEATH,     658, 623, 24.0f, 24.0f, " ");
    SetButtonProperties(DE_BTN_HELP_ATTACK,    347,  95, 24.0f, 24.0f, " ");
    SetButtonProperties(DE_BTN_HELP_DEFEND,    658,  95, 24.0f, 24.0f, " ");
    SetButtonProperties(DE_BTN_HELP_BRIDGEAND, 260, 227, 24.0f, 24.0f, " ");
    SetButtonProperties(DE_BTN_HELP_BRIDGEOF,  570, 227, 24.0f, 24.0f, " ");
    SetButtonProperties(DE_BTN_HELP_STATS,        1133.0f, 107.0f, 24.0f, 24.0f, " ");
    SetButtonProperties(DE_BTN_HELP_ELEMENTBONUS, 1133.0f, 292.0f, 24.0f, 24.0f, " ");
}

//====================================== Property Queries =========================================
bool DeckEditor::IsActive()
{
    return mIsActive;
}
int DeckEditor::GetHandCount(int pType)
{
    if(pType < 0 || pType >= DE_HAND_TOTAL) return 0;
    return mHands[pType].mCurrent;
}
int DeckEditor::GetDeckSize()
{
    int tTotal = 0;
    for(int i = 0; i < DE_HAND_TOTAL; i ++) tTotal += mHands[i].mCurrent;
    return tTotal;
}

//========================================= Manipulators ==========================================
void DeckEditor::Activate()
{
    mIsActive = true;
    mIsVisible = true;
}
void DeckEditor::Deactivate()
{
    mIsActive = false;
    mIsVisible = false;
}
void DeckEditor::SetPostExec(const char *pPath)
{
    ResetString(mPostExecScript, pPath);
}
void DeckEditor::SetHelpExec(const char *pPath)
{
    ResetString(mHelpExecPath, pPath);
}
void DeckEditor::SetButtonProperties(int pSlot, float pLft, float pTop, float pWid, float pHei, const char *pText)
{
    //--Worker function, quickly changes button properties.
    if(pSlot < 0 || pSlot >= DE_BTN_TOTAL) return;
    if(!pText) return;
    mButtons[pSlot].mIsDisabled = false;
    mButtons[pSlot].mIsPressed = false;
    mButtons[pSlot].mDimensions.SetWH(pLft, pTop, pWid, pHei);
    strncpy(mButtons[pSlot].mText, pText, sizeof(char) * STD_MAX_LETTERS);
}
void DeckEditor::SetHandBasicProperties(int pSlot, float pLft, float pTop, float pWid, float pHei, const char *pText, SugarBitmap *pImg)
{
    //--Worker function, quickly changes basic properties of each card type.
    if(pSlot < 0 || pSlot >= DE_HAND_TOTAL) return;
    if(!pText) return;
    mHands[pSlot].mCurrent = 0;
    mHands[pSlot].mMinimum = 0;
    mHands[pSlot].mMaximum = 0;
    mHands[pSlot].mPosition.SetWH(pLft, pTop, pWid, pHei);
    strncpy(mHands[pSlot].mTitle, pText, sizeof(char) * STD_MAX_LETTERS);
    mHands[pSlot].rCardImg = pImg;
}
void DeckEditor::SetHandCounters(int pSlot, int pCurrent, int pMinimum, int pMaximum)
{
    //--Sets how many cards are min/max for the given hand slot. Does *not* do any clamping!
    if(pSlot < 0 || pSlot >= DE_HAND_TOTAL) return;
    mHands[pSlot].mCurrent = pCurrent;
    mHands[pSlot].mMinimum = pMinimum;
    mHands[pSlot].mMaximum = pMaximum;
}
void DeckEditor::SetDeckMinMaxSize(int pMinSize, int pMaxSize)
{
    mDeckSizeMin = pMinSize;
    mDeckSizeMax = pMaxSize;
}
void DeckEditor::SetBonus(int pType, int pValue)
{
    //--Routing setter for Lua files to modify the stat bonuses with a single call.
    int *rPtr = NULL;
    if(pType == DE_STAT_ATTACK)
        rPtr = &mAttackBonus;
    else if(pType == DE_STAT_DEFEND)
        rPtr = &mDefendBonus;
    else if(pType == DE_STAT_STARTSHIELD)
        rPtr = &mStartShieldBonus;
    else if(pType == DE_STAT_WATERBONUS)
        rPtr = &mBonusWater;
    else if(pType == DE_STAT_FIREBONUS)
        rPtr = &mBonusFire;
    else if(pType == DE_STAT_WINDBONUS)
        rPtr = &mBonusWind;
    else if(pType == DE_STAT_EARTHBONUS)
        rPtr = &mBonusEarth;
    else if(pType == DE_STAT_LIFEBONUS)
        rPtr = &mBonusLife;
    else if(pType == DE_STAT_DEATHBONUS)
        rPtr = &mBonusDeath;

    //--Implicit range check.
    if(!rPtr) return;
    *rPtr = pValue;
}
void DeckEditor::SetSentenceLenBonus(const char *pValue)
{
    //--The sentence length bonus is a special instance since it cannot be described in simple integers.
    strncpy(mSentenceLenBonus, pValue, sizeof(mSentenceLenBonus));
}
void DeckEditor::SetHelpHeader(const char *pText)
{
    ResetString(mHelpHeader, pText);
}
void DeckEditor::SetHelpString(int pLine, const char *pText)
{
    if(pLine < 0 || pLine >= DE_HELP_MAXLINES) return;
    ResetString(mHelpStrings[pLine], pText);
}

//========================================= Core Methods ==========================================
void DeckEditor::HandleButtonClick(int pSlot)
{
    //--[Documentation and Setup]
    //--When the indicated button is clicked, this routine handles what it actually does.
    if(pSlot < 0 || pSlot >= DE_BTN_TOTAL) return;

    //--[Exit Handlers]
    //--Accept. Marks the object as having modified the deck state.
    if(pSlot == DE_BTN_ACCEPT)
    {
        //--Error check. We won't run the accept case if an error occurs, doesn't matter which.
        bool tError = false;
        int tDeckSize = GetDeckSize();
        if(tDeckSize < mDeckSizeMin)
        {
            tError = true;
        }
        else if(tDeckSize > mDeckSizeMax)
        {
            tError = true;
        }

        //--Error was found. Stop here.
        if(tError)
        {
            AudioManager::Fetch()->PlaySound("Menu|Failed");
            return;
        }

        //--No errors, save the deck composition.
        if(mPostExecScript) LuaManager::Fetch()->ExecuteLuaFile(mPostExecScript, 1, "S", "Accept");
        AudioManager::Fetch()->PlaySound("Menu|Select");
        mIsActive = false;
        mIsVisible = false;
        return;
    }
    //--Cancel. Marks the object as having not modified the deck state.
    else if(pSlot == DE_BTN_CANCEL)
    {
        AudioManager::Fetch()->PlaySound("Menu|Select");
        mIsActive = false;
        mIsVisible = false;
        return;
    }
    //--Defaults. Orders the lua state to reset deck to default state.
    else if(pSlot == DE_BTN_DEFAULTS)
    {
        if(mPostExecScript) LuaManager::Fetch()->ExecuteLuaFile(mPostExecScript, 1, "S", "Defaults");
        AudioManager::Fetch()->PlaySound("Menu|Select");
        mIsActive = false;
        mIsVisible = false;
        return;
    }

    //--[Add and Subtract]
    //--Performed programmatically. Each hand type has two entries: Add and Subtract. These modify
    //  the hand's card count down or up to the minimum or maximum.
    int tHandType = (pSlot - DE_BTN_HAND_START) / DE_BTN_HAND_BTNS;
    if(pSlot >= DE_BTN_ADDSUB_START && pSlot <= DE_BTN_ADDSUB_END)
    {
        //--Make sure the hand is valid.
        if(tHandType < 0 || tHandType >= DE_HAND_TOTAL) return;

        //--If this is the 'add' button:
        if((pSlot - DE_BTN_HAND_START) % DE_BTN_HAND_BTNS == DE_BTN_HAND_BTN_ADD)
        {
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            mHands[tHandType].mCurrent ++;
            if(mHands[tHandType].mCurrent >= mHands[tHandType].mMaximum) mHands[tHandType].mCurrent = mHands[tHandType].mMaximum;
        }
        //--If this is the 'subtract' button:
        else if((pSlot - DE_BTN_HAND_START) % DE_BTN_HAND_BTNS == DE_BTN_HAND_BTN_SUB)
        {
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            mHands[tHandType].mCurrent --;
            if(mHands[tHandType].mCurrent <= mHands[tHandType].mMinimum) mHands[tHandType].mCurrent = mHands[tHandType].mMinimum;
        }

        //--In any case, stop the update here.
        return;
    }

    //--[Help]
    if(pSlot >= DE_BTN_HELP_START && pSlot <= DE_BTN_HELP_END)
    {
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        ActivateHelpMode(pSlot);
        return;
    }
}
void DeckEditor::ActivateHelpMode(int pButtonSlot)
{
    //--When the player clicks a help button, show the help window with the matching set of strings.
    //  The strings are generated by Lua.
    if(!mHelpExecPath) return;

    //--Flags.
    mHelpVisible = true;

    //--Clear old data back to NULL.
    ResetString(mHelpHeader, NULL);
    for(int i = 0; i < DE_HELP_MAXLINES; i ++) ResetString(mHelpStrings[i], NULL);

    //--Execute.
    LuaManager::Fetch()->ExecuteLuaFile(mHelpExecPath, 1, "N", (float)pButtonSlot);
}

//===================================== Private Core Methods ======================================
//============================================ Update =============================================
void DeckEditor::Update()
{
    //--[Visibility Handler]
    //--If the object is meant to be visible, increment the timer.
    if(mIsVisible)
    {
        if(mVisibilityTimer < DE_VIS_TICKS) mVisibilityTimer ++;
    }
    //--Decrement.
    else
    {
        if(mVisibilityTimer > 0) mVisibilityTimer --;
    }

    //--If the object is not actually active, stop the update here.
    if(!mIsActive) return;

    //--[Fast-Access Pointers]
    //--Managers.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Mouse position.
    int tMouseX, tMouseY, tMouseZ;
    rControlManager->GetMouseCoords(tMouseX, tMouseY, tMouseZ);

    //--[Help Mode]
    if(mHelpVisible)
    {
        //--Timer.
        if(mHelpVisibleTimer < DE_VIS_TICKS) mHelpVisibleTimer ++;

        //--All other controls are locked out. Only one button matters, the new Accept button.
        if(rControlManager->IsFirstPress("MouseLft")) mHelpVisible = false;
        return;
    }
    else
    {
        if(mHelpVisibleTimer > 0) mHelpVisibleTimer --;
    }

    //--[Button Press Scan]
    //--Run across all the buttons and see if any got pressed.
    if(rControlManager->IsFirstPress("MouseLft"))
    {
        //--Setup.
        int tClickedBtn = -1;

        //--Scan.
        for(int i = 0; i < DE_BTN_TOTAL; i ++)
        {
            //--Point is not within, ignore.
            if(!IsPointWithin2DReal(tMouseX, tMouseY, mButtons[i].mDimensions)) continue;

            //--Stop the update on the first clicked button.
            tClickedBtn = i;
            break;
        }

        //--Handle the click.
        HandleButtonClick(tClickedBtn);
    }
}

//=========================================== File I/O ============================================
//=========================================== Drawing =============================================
void DeckEditor::Render()
{
    //--[Visibility Check]
    //--If the object is not actually visible, we don't need to render anything.
    if(mVisibilityTimer < 1) return;

    //--Otherwise, compute opacity.
    float cOpacity = EasingFunction::QuadraticInOut(mVisibilityTimer, DE_VIS_TICKS);
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cOpacity);

    //--[Setup]
    //--Error code.
    int tErrorCode = -1;

    //--[Background]
    //--Backing card.
    Images.Data.rBackground->Draw();

    //--Header.
    //Images.Data.rFontHeader->DrawText(VIRTUAL_CANVAS_X * 0.50f, 50.0f, SUGARFONT_AUTOCENTER_X, 2.0f, "Deck Editor");

    //--[Hands]
    //--If over/under the deck size cap, change to red text.
    int tDeckSize = GetDeckSize();
    if(tDeckSize < mDeckSizeMin)
    {
        tErrorCode = DE_ERROR_TOO_FEW_CARDS;
        StarlightColor::SetMixer(1.0f, 0.0f, 0.0f, cOpacity);
    }
    else if(tDeckSize > mDeckSizeMax)
    {
        tErrorCode = DE_ERROR_TOO_MANY_CARDS;
        StarlightColor::SetMixer(1.0f, 0.0f, 0.0f, cOpacity);
    }

    //--Deck size range.
    Images.Data.rFontMainSm->DrawTextArgs(DE_DECKSIZE_X, DE_DECKSIZE_Y, 0, 1.0f, "Deck Size (Min %i Max %i)", mDeckSizeMin, mDeckSizeMax);
    Images.Data.rFontMainSm->DrawTextArgs(DE_DECKSIZE_X +  50.0f, DE_DECKSIZE_Y + 22.0f, 0, 1.0f, "%i", tDeckSize);
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cOpacity);
    StarlightColor cGreyed = StarlightColor::MapRGBAF(0.20f, 0.20f, 0.20f, cOpacity);

    //--For each hand, render its text parts, card counts, cards, and buttons.
    for(int i = 0; i < DE_HAND_TOTAL; i ++)
    {
        //--Card Heading
        if(!mHands[i].rCardImg) continue;
        //mHands[i].rCardImg->DrawScaled(mHands[i].mPosition.mLft + DE_HANDPOS_TITLE_X - cWid, mHands[i].mPosition.mTop + DE_HANDPOS_TITLE_Y - cHei, 0.50f, 0.50f, 0);

        //--Hand Heading
        Images.Data.rFontMainSm->DrawTextArgs(mHands[i].mPosition.mLft + DE_HANDPOS_TITLE_X, mHands[i].mPosition.mTop + DE_HANDPOS_TITLE_Y, SUGARFONT_AUTOCENTER_XY, 1.0f, "%s", mHands[i].mTitle);

        //--Indicate how many cards of this type:
        Images.Data.rFontMainSm->DrawTextArgs(mHands[i].mPosition.mLft + DE_HANDPOS_COUNTS_X, mHands[i].mPosition.mTop + DE_HANDPOS_COUNTS_Y, SUGARFONT_AUTOCENTER_XY, 1.0f, "%i/%i", mHands[i].mCurrent, mHands[i].mMaximum);
        //Images.Data.rFontMainSm->DrawTextArgs(mHands[i].mPosition.mLft + DE_HANDPOS_COUNTS_X + 25.0f, mHands[i].mPosition.mTop + DE_HANDPOS_COUNTS_Y, 0, 1.0f, "(Min %i, Max %i)", mHands[i].mMinimum, mHands[i].mMaximum);

        //--Associated Add/Subtract Buttons.
        for(int p = 0; p < DE_BTN_HAND_BTNS; p ++)
        {
            //--Compute the slot.
            int tBtnSlot = DE_BTN_HAND_START + (i * DE_BTN_HAND_BTNS) + p;
            if(tBtnSlot < 0 || tBtnSlot >= DE_BTN_TOTAL) continue;

            //--Render.
            if(p == DE_BTN_HAND_BTN_ADD)
                Images.Data.rButtonSmallAdd->Draw(mButtons[tBtnSlot].mDimensions.mLft, mButtons[tBtnSlot].mDimensions.mTop);
            else
                Images.Data.rButtonSmallSub->Draw(mButtons[tBtnSlot].mDimensions.mLft, mButtons[tBtnSlot].mDimensions.mTop);
        }

        //--Cards. These render right-to-left so they appear stacked.
        float cYOffset = 0.0f;
        //float cWid = mHands[i].rCardImg->GetWidth()  * 0.25f;
        //float cHei = mHands[i].rCardImg->GetHeight() * 0.25f;
        if(i == DE_HAND_BRIDGEAND || i == DE_HAND_BRIDGEOF) cYOffset = 17.0f;
        cGreyed.SetAsMixer();
        for(int p = mHands[i].mCurrent; p < mHands[i].mMaximum; p ++)
        {
            float tXPos = (mHands[i].mPosition.mLft + DE_HANDPOS_CARDSTART_X) + (DE_HANDPOS_CARDTILE_X * p);
            float tYPos = (mHands[i].mPosition.mTop + DE_HANDPOS_CARDSTART_Y) + (DE_HANDPOS_CARDTILE_Y * p) + cYOffset;
            mHands[i].rCardImg->Draw(tXPos, tYPos);
        }
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cOpacity);
        for(int p = 0; p < mHands[i].mCurrent; p ++)
        {
            float tXPos = (mHands[i].mPosition.mLft + DE_HANDPOS_CARDSTART_X) + (DE_HANDPOS_CARDTILE_X * p);
            float tYPos = (mHands[i].mPosition.mTop + DE_HANDPOS_CARDSTART_Y) + (DE_HANDPOS_CARDTILE_Y * p) + cYOffset;
            mHands[i].rCardImg->Draw(tXPos, tYPos);
        }
    }

    //--[Stat Bonus Listing]
    //--First, the headers.
    //Images.Data.rFontHeader->DrawText(DE_STATS_HEADER_X, DE_STATS_HEADER_Y, SUGARFONT_AUTOCENTER_XY, 1.0f, "Your Stats");
    Images.Data.rFontMainSm->DrawText(DE_STATS_COLUMN_LFT + (DE_STATS_COLUMN_WID * 0.0f), DE_STATS_COLUMN_TOP + (DE_STATS_COLUMN_HEI * 0.0f), 0, 1.0, "Attack Bonus");
    Images.Data.rFontMainSm->DrawText(DE_STATS_COLUMN_LFT + (DE_STATS_COLUMN_WID * 1.0f), DE_STATS_COLUMN_TOP + (DE_STATS_COLUMN_HEI * 0.0f), 0, 1.0, "Defend Bonus");
    Images.Data.rFontMainSm->DrawText(DE_STATS_COLUMN_LFT + (DE_STATS_COLUMN_WID * 0.0f), DE_STATS_COLUMN_TOP + (DE_STATS_COLUMN_HEI * 1.0f), 0, 1.0, "Starting Shields");
    Images.Data.rFontMainSm->DrawText(DE_STATS_COLUMN_LFT + (DE_STATS_COLUMN_WID * 1.0f), DE_STATS_COLUMN_TOP + (DE_STATS_COLUMN_HEI * 1.0f), 0, 1.0, "Length Bonus");

    //--Values.
    Images.Data.rFontMainSm->DrawTextArgs(DE_STATS_COLUMN_LFT + (DE_STATS_COLUMN_WID * 0.0f) + DE_STATS_VAL_OFFSET_X, DE_STATS_COLUMN_TOP + (DE_STATS_COLUMN_HEI * 0.0f) + DE_STATS_VAL_OFFSET_Y, SUGARFONT_RIGHTALIGN_X, 1.0, "%i", mAttackBonus);
    Images.Data.rFontMainSm->DrawTextArgs(DE_STATS_COLUMN_LFT + (DE_STATS_COLUMN_WID * 1.0f) + DE_STATS_VAL_OFFSET_X, DE_STATS_COLUMN_TOP + (DE_STATS_COLUMN_HEI * 0.0f) + DE_STATS_VAL_OFFSET_Y, SUGARFONT_RIGHTALIGN_X, 1.0, "%i", mDefendBonus);
    Images.Data.rFontMainSm->DrawTextArgs(DE_STATS_COLUMN_LFT + (DE_STATS_COLUMN_WID * 0.0f) + DE_STATS_VAL_OFFSET_X, DE_STATS_COLUMN_TOP + (DE_STATS_COLUMN_HEI * 1.0f) + DE_STATS_VAL_OFFSET_Y, SUGARFONT_RIGHTALIGN_X, 1.0, "%i", mStartShieldBonus);
    Images.Data.rFontMainSm->DrawText    (DE_STATS_COLUMN_LFT + (DE_STATS_COLUMN_WID * 1.0f) + DE_STATS_VAL_OFFSET_X + 75.0f, DE_STATS_COLUMN_TOP + (DE_STATS_COLUMN_HEI * 1.0f) + DE_STATS_VAL_OFFSET_Y, SUGARFONT_RIGHTALIGN_X, 1.0, mSentenceLenBonus);

    //--[Deck Statistics]
    //--Headers.
    //Images.Data.rFontHeader->DrawText(DE_DECKSTAT_HEADER_X, DE_DECKSTAT_HEADER_Y, SUGARFONT_AUTOCENTER_XY, 1.0f, "Elemental Bonuses");
    Images.Data.rFontMainSm->DrawText(DE_DECKSTAT_COLUMN_LFT + (DE_DECKSTAT_COLUMN_WID * 0.0f), DE_DECKSTAT_COLUMN_TOP + (DE_DECKSTAT_COLUMN_HEI * 0.0f), 0, 1.0, "+Water Damage");
    Images.Data.rFontMainSm->DrawText(DE_DECKSTAT_COLUMN_LFT + (DE_DECKSTAT_COLUMN_WID * 1.0f), DE_DECKSTAT_COLUMN_TOP + (DE_DECKSTAT_COLUMN_HEI * 0.0f), 0, 1.0, "+Fire Damage");
    Images.Data.rFontMainSm->DrawText(DE_DECKSTAT_COLUMN_LFT + (DE_DECKSTAT_COLUMN_WID * 0.0f), DE_DECKSTAT_COLUMN_TOP + (DE_DECKSTAT_COLUMN_HEI * 1.0f), 0, 1.0, "+Wind Damage");
    Images.Data.rFontMainSm->DrawText(DE_DECKSTAT_COLUMN_LFT + (DE_DECKSTAT_COLUMN_WID * 1.0f), DE_DECKSTAT_COLUMN_TOP + (DE_DECKSTAT_COLUMN_HEI * 1.0f), 0, 1.0, "+Earth Damage");
    Images.Data.rFontMainSm->DrawText(DE_DECKSTAT_COLUMN_LFT + (DE_DECKSTAT_COLUMN_WID * 0.0f), DE_DECKSTAT_COLUMN_TOP + (DE_DECKSTAT_COLUMN_HEI * 2.0f), 0, 1.0, "+Life Damage");
    Images.Data.rFontMainSm->DrawText(DE_DECKSTAT_COLUMN_LFT + (DE_DECKSTAT_COLUMN_WID * 1.0f), DE_DECKSTAT_COLUMN_TOP + (DE_DECKSTAT_COLUMN_HEI * 2.0f), 0, 1.0, "+Death Damage");

    //--Values.
    Images.Data.rFontMainSm->DrawTextArgs(DE_DECKSTAT_COLUMN_LFT + (DE_DECKSTAT_COLUMN_WID * 0.0f) + DE_DECKSTAT_VAL_OFFSET_X, DE_DECKSTAT_COLUMN_TOP + (DE_DECKSTAT_COLUMN_HEI * 0.0f) + DE_DECKSTAT_VAL_OFFSET_Y, SUGARFONT_RIGHTALIGN_X, 1.0, "%i", mBonusWater);
    Images.Data.rFontMainSm->DrawTextArgs(DE_DECKSTAT_COLUMN_LFT + (DE_DECKSTAT_COLUMN_WID * 1.0f) + DE_DECKSTAT_VAL_OFFSET_X, DE_DECKSTAT_COLUMN_TOP + (DE_DECKSTAT_COLUMN_HEI * 0.0f) + DE_DECKSTAT_VAL_OFFSET_Y, SUGARFONT_RIGHTALIGN_X, 1.0, "%i", mBonusFire);
    Images.Data.rFontMainSm->DrawTextArgs(DE_DECKSTAT_COLUMN_LFT + (DE_DECKSTAT_COLUMN_WID * 0.0f) + DE_DECKSTAT_VAL_OFFSET_X, DE_DECKSTAT_COLUMN_TOP + (DE_DECKSTAT_COLUMN_HEI * 1.0f) + DE_DECKSTAT_VAL_OFFSET_Y, SUGARFONT_RIGHTALIGN_X, 1.0, "%i", mBonusWind);
    Images.Data.rFontMainSm->DrawTextArgs(DE_DECKSTAT_COLUMN_LFT + (DE_DECKSTAT_COLUMN_WID * 1.0f) + DE_DECKSTAT_VAL_OFFSET_X, DE_DECKSTAT_COLUMN_TOP + (DE_DECKSTAT_COLUMN_HEI * 1.0f) + DE_DECKSTAT_VAL_OFFSET_Y, SUGARFONT_RIGHTALIGN_X, 1.0, "%i", mBonusEarth);
    Images.Data.rFontMainSm->DrawTextArgs(DE_DECKSTAT_COLUMN_LFT + (DE_DECKSTAT_COLUMN_WID * 0.0f) + DE_DECKSTAT_VAL_OFFSET_X, DE_DECKSTAT_COLUMN_TOP + (DE_DECKSTAT_COLUMN_HEI * 2.0f) + DE_DECKSTAT_VAL_OFFSET_Y, SUGARFONT_RIGHTALIGN_X, 1.0, "%i", mBonusLife);
    Images.Data.rFontMainSm->DrawTextArgs(DE_DECKSTAT_COLUMN_LFT + (DE_DECKSTAT_COLUMN_WID * 1.0f) + DE_DECKSTAT_VAL_OFFSET_X, DE_DECKSTAT_COLUMN_TOP + (DE_DECKSTAT_COLUMN_HEI * 2.0f) + DE_DECKSTAT_VAL_OFFSET_Y, SUGARFONT_RIGHTALIGN_X, 1.0, "%i", mBonusDeath);

    //--[Exit Buttons]
    //--Render each button and the text on top.
    for(int i = 0; i < DE_BTN_EXIT_TOTAL; i ++)
    {
        //--Grey out the "Accept" button if an error is found.
        if(i == DE_BTN_ACCEPT && tErrorCode != DE_ERROR_NONE) StarlightColor::SetMixer(0.5f, 0.5f, 0.5f, cOpacity);

        //--Button.
        Images.Data.rExitBtnLookups[i]->Draw();
        //Images.Data.rButtonMain->Draw(mButtons[i].mDimensions.mLft, mButtons[i].mDimensions.mTop);

        //--Text.
        //StarlightColor::SetMixer(0.0f, 0.0f, 0.0f, cOpacity);
        //Images.Data.rFontMainSm->DrawText(mButtons[i].mDimensions.mXCenter, mButtons[i].mDimensions.mYCenter - 3.0f, SUGARFONT_AUTOCENTER_XY, 1.0, mButtons[i].mText);
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cOpacity);
    }

    //--[Help Buttons]
    //--Render the help buttons as question marks.
    for(int i = DE_BTN_HELP_START; i < DE_BTN_TOTAL; i ++)
    {
        Images.Data.rButtonSmallHlp->Draw(mButtons[i].mDimensions.mLft, mButtons[i].mDimensions.mTop);
    }

    //--[Warnings]
    if(tErrorCode == DE_ERROR_TOO_FEW_CARDS)
    {
        StarlightColor::SetMixer(1.0f, 0.0f, 0.0f, cOpacity);
        Images.Data.rFontMainSm->DrawText(DE_ERRORPOS_X, DE_ERRORPOS_Y, SUGARFONT_RIGHTALIGN_X, 1.0f, "Not enough cards in deck!");
    }
    else if(tErrorCode == DE_ERROR_TOO_MANY_CARDS)
    {
        StarlightColor::SetMixer(1.0f, 0.0f, 0.0f, cOpacity);
        Images.Data.rFontMainSm->DrawText(DE_ERRORPOS_X, DE_ERRORPOS_Y, SUGARFONT_RIGHTALIGN_X, 1.0f, "Too many cards in deck!");
    }

    //--[Clean]
    //--Reset mixer.
    StarlightColor::ClearMixer();

    //--[Help Overlay]
    //--Covers the rest of the screen. Has transparency.
    if(mHelpVisibleTimer > 0)
    {
        //--Compute opacity.
        float cHelpOpacity = EasingFunction::QuadraticInOut(mHelpVisibleTimer, DE_VIS_TICKS);

        //--Underlay.
        SugarBitmap::DrawRectFill(0.0f, 0.0f, VIRTUAL_CANVAS_X, VIRTUAL_CANVAS_Y, StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, cHelpOpacity * 0.50f));
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cOpacity * cHelpOpacity);

        //--Render the backing.
        Images.Data.rHelpInlay->Draw(80, 39);

        //--Header.
        if(mHelpHeader)
        {
            Images.Data.rFontHeader->DrawText(VIRTUAL_CANVAS_X * 0.50f, 70.0f, SUGARFONT_AUTOCENTER_X, 1.0f, mHelpHeader);
        }

        //--Lines.
        float cTextSize = Images.Data.rFontMainLg->GetTextHeight();
        for(int i = 0; i < DE_HELP_MAXLINES; i ++)
        {
            //--Skip empty lines.
            if(!mHelpStrings[i]) continue;
            Images.Data.rFontMainLg->DrawText(220.0f, 170.0f + (cTextSize * i), 0, 1.0f, mHelpStrings[i]);
        }
    }

    //--[Clean]
    //--Reset mixer.
    StarlightColor::ClearMixer();
}

//======================================= Pointer Routing =========================================
//====================================== Static Functions =========================================
//========================================= Lua Hooking ===========================================
//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
