//--Base
#include "DialogueActor.h"

//--Classes
#include "WorldDialogue.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarLinkedList.h"

//--Definitions
#include "EasingFunctions.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "OptionsManager.h"

//=========================================== System ==============================================
DialogueActor::DialogueActor()
{
    //--[RootObject]
    //--System
    mType = POINTER_TYPE_DIALOGUEACTOR;

    //--[DialogueActor]
    //--System
    //--Names
    mLocalName = InitializeString("Unnamed Dialogue Actor");
    mAliasList = new SugarLinkedList(false);

    //--Images
    rActiveImage = NULL;
    mImageList = new SugarLinkedList(false);
    mImageListRev = new SugarLinkedList(false);
    mIsNotWideImageList = new SugarLinkedList(false);
    mIsNotWideImageListRev = new SugarLinkedList(false);

    //--Multi-layer emotions.
    mrAdditionalLayerList = new SugarLinkedList(false);

    //--Render State
    mSizeTimer = 0;
    mIsDarkened = false;
}
DialogueActor::~DialogueActor()
{
    delete mAliasList;
    delete mImageList;
    delete mImageListRev;
    delete mIsNotWideImageList;
    delete mIsNotWideImageListRev;
    delete mrAdditionalLayerList;
}

//====================================== Property Queries =========================================
const char *DialogueActor::GetName()
{
    return (const char *)mLocalName;
}
bool DialogueActor::IsSpeaking(const char *pSpeakerName)
{
    if(!pSpeakerName) return false;
    return (mAliasList->GetElementByName(pSpeakerName) != NULL);
}
bool DialogueActor::IsDarkened()
{
    return mIsDarkened;
}
bool DialogueActor::IsFullDark()
{
    if(mSizeTimer == 0) return true;
    return false;
}
SugarBitmap *DialogueActor::GetActiveImage()
{
    return rActiveImage;
}

//========================================= Manipulators ==========================================
void DialogueActor::SetName(const char *pName)
{
    if(!pName) return;
    ResetString(mLocalName, pName);
}
void DialogueActor::AddAlias(const char *pName)
{
    //--Fail if the alias is already on the list.
    static int xDummyInt = 1;
    if(!pName || mAliasList->GetElementByName(pName) != NULL) return;
    mAliasList->AddElement(pName, &xDummyInt);
}
void DialogueActor::RemoveAlias(const char *pName)
{
    //--Remove every instance of the alias. Nominally it'd only appear once, but...
    while(mAliasList->RemoveElementS(pName))
    {

    }
}
void DialogueActor::AddPortrait(const char *pName, const char *pDLPath, bool pIsNotWide)
{
    //--Adds a new portrait. Replaces it if that name already exists.
    if(!pName || !pDLPath) return;

    //--Check if this portrait name already exists. If so, replace it.
    void *rCheckPortrait = mImageList->GetElementByName(pName);
    if(rCheckPortrait)
    {
        mImageList->RemoveElementS(pName);
        mIsNotWideImageList->RemoveElementS(pName);
    }

    //--Static HD copies.
    static bool xTrue = true;
    static bool xFalse = false;

    //--Add a copy.
    mImageList->AddElementAsTail(pName, DataLibrary::Fetch()->GetEntry(pDLPath));
    if(pIsNotWide)
    {
        mIsNotWideImageList->AddElementAsTail(pName, &xTrue);
    }
    else
    {
        mIsNotWideImageList->AddElementAsTail(pName, &xFalse);
    }
}
void DialogueActor::AddPortraitRev(const char *pName, const char *pDLPath, bool pIsNotWide)
{
    //--Adds a new portrait for the reverse case, which appears if the character is on the right side
    //  of the dialogue. If a reverse exists, it is displayed, otherwise the normal case appears flipped.
    if(!pName || !pDLPath) return;

    //--Check if this portrait name already exists. If so, replace it.
    void *rCheckPortrait = mImageListRev->GetElementByName(pName);
    if(rCheckPortrait)
    {
        mImageListRev->RemoveElementS(pName);
        mIsNotWideImageListRev->RemoveElementS(pName);
    }

    //--Static HD copies.
    static bool xTrue = true;
    static bool xFalse = false;

    //--Add a copy.
    mImageListRev->AddElementAsTail(pName, DataLibrary::Fetch()->GetEntry(pDLPath));
    if(pIsNotWide)
    {
        mIsNotWideImageListRev->AddElementAsTail(pName, &xTrue);
    }
    else
    {
        mIsNotWideImageListRev->AddElementAsTail(pName, &xFalse);
    }
}
void DialogueActor::RemovePortrait(const char *pName)
{
    //--Remove all portraits matching the name.
    while(mImageList->RemoveElementS(pName))
    {
    }
    while(mImageListRev->RemoveElementS(pName))
    {
    }
    while(mIsNotWideImageList->RemoveElementS(pName))
    {
    }
    while(mIsNotWideImageListRev->RemoveElementS(pName))
    {
    }
}
void DialogueActor::SetActivePortrait(const char *pName)
{
    //--Locate the requested portrait and set it as the active one. Passing NULL is legal but will
    //  clear the portrait (so don't do that).
    if(!pName)
    {
        rActiveImage = NULL;
        mrAdditionalLayerList->ClearList();
    }
    //--If the first six letters are "LAYER|" then this is a layered emotion case.
    else if(!strncasecmp(pName, "LAYER|", 6))
    {
        //--Active image is NULL.
        rActiveImage = NULL;

        //--Uses a while loop as there may be a variable number of layers. Each layer is delimited by a '|' symbol.
        char tBuffer[128];
        int tCurBufLetter = 0;
        int tCurLetter = 6;
        int cLength = (int)strlen(pName);
        while(tCurLetter < cLength)
        {
            //--Copy over.
            if(pName[tCurLetter] != '|')
            {
                tBuffer[tCurBufLetter+0] = pName[tCurLetter];
                tBuffer[tCurBufLetter+1] = '\0';
                tCurBufLetter ++;
            }
            //--Bar. Add the layer.
            else
            {
                mrAdditionalLayerList->AddElement("X", mImageList->GetElementByName(tBuffer));
                tCurBufLetter = 0;
                tBuffer[0] = '\0';
            }

            //--Next.
            tCurLetter ++;
        }
    }
    //--Otherwise, this is a single-emotion case.
    else
    {
        rActiveImage = (SugarBitmap *)mImageList->GetElementByName(pName);
        mrAdditionalLayerList->ClearList();
    }
}
void DialogueActor::SetDarkenedState(bool pFlag)
{
    mIsDarkened = pFlag;
}

//========================================= Core Methods ==========================================
void DialogueActor::SetDarkeningFromName(const char *pName)
{
    //--Given a name, checks if the name is any of this DialogueActor's aliases. If it is, then
    //  this DialogueActor is speaking or otherwise should not be darkened.
    SetDarkenedState(true);
    if(!pName) return;

    //--Scan the alias list. If it exists, we're not darkened.
    void *rCheckPtr = mAliasList->GetElementByName(pName);
    SetDarkenedState(rCheckPtr == NULL);
}

//===================================== Private Core Methods ======================================
//============================================ Update =============================================
void DialogueActor::Update()
{
    //--Just handles the size modification timers.
    if(mIsDarkened)
    {
        if(mSizeTimer > 0) mSizeTimer --;
    }
    else
    {
        if(mSizeTimer < SIZE_MOD_TICKS) mSizeTimer ++;
    }
}

//=========================================== File I/O ============================================
//=========================================== Drawing =============================================
void DialogueActor::RenderAt(float pXCenter, float pYBottom, float pGlobalAlpha, uint32_t pFlags)
{
    //--Overload, uses the active image. Will auto-switch to layered rendering if required.
    RenderAt(rActiveImage, pXCenter, pYBottom, pGlobalAlpha, pFlags);
}
void DialogueActor::RenderAt(SugarBitmap *pUseImage, float pXCenter, float pYBottom, float pGlobalAlpha, uint32_t pFlags)
{
    //--Renders whatever the current image is at the provided position. Portraits always render
    //  bottom-up and are centered. If no image is provided, attempts to use layered rendering.
    if(!pUseImage)
    {
        RenderAtLayered(pXCenter, pYBottom, pGlobalAlpha, pFlags);
        return;
    }

    //--If this entity is darkened, modify the mixer.
    if(mIsDarkened)
    {
        StarlightColor::SetMixer(0.5f, 0.5f, 0.5f, pGlobalAlpha);
    }
    //--Normal color mixing.
    else
    {
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pGlobalAlpha);
    }

    //--Constants.
    float cPortraitScale = 0.50f;
    float cWidePortraitWid = 1392.0f;
    float cNarrowPortraitWid = 694.0f;
    bool tIsSmallPortraitMode = OptionsManager::Fetch()->GetOptionB("LowResAdventureMode");
    if(tIsSmallPortraitMode) pXCenter = pXCenter - 64.0f;

    //--Resolve the size.
    float cSizeAmount = SIZE_MOD_AMOUNT;
    if(tIsSmallPortraitMode) cSizeAmount = cSizeAmount * 0.50f;

    //--Modify.
    float cSizeMod = EasingFunction::QuadraticInOut(mSizeTimer, SIZE_MOD_TICKS) * cSizeAmount;
    if(cSizeMod != 0.0f) cPortraitScale = cPortraitScale + cSizeMod;

    //--Locate the position of the active image and figure out if it is "Wide" or not.
    int tSlot = mImageList->GetSlotOfElementByPtr(pUseImage);
    bool *rIsNotWideFlag = (bool *)mIsNotWideImageList->GetElementBySlot(tSlot);

    //--Not wide. This is most of the player party portraits and some often-seen NPCs.
    if(rIsNotWideFlag && (*rIsNotWideFlag))
    {
        //--Flipping case: If a flipped variant exists and the flags call for it, swap it out for
        //  the flipped variant.
        if(pFlags & SUGAR_FLIP_HORIZONTAL && tSlot != -1)
        {
            //--Check for a flipped version.
            const char *rName = mImageList->GetNameOfElementBySlot(tSlot);
            SugarBitmap *rImage = (SugarBitmap *)mImageListRev->GetElementByName(rName);

            //--If it exists, replace it.
            if(rImage) pUseImage = rImage;
        }

        //--Normal rendering:
        if(!tIsSmallPortraitMode)
        {
            float tRenderX = pXCenter - (cNarrowPortraitWid * 0.50f * cPortraitScale);
            float tRenderY = pYBottom - (pUseImage->GetTrueHeight() * cPortraitScale);
            pUseImage->DrawScaled(tRenderX, tRenderY, cPortraitScale, cPortraitScale, pFlags);
        }
        //--Small portrait rendering. The portraits are even smaller.
        else
        {
            float tRenderX = pXCenter - (cNarrowPortraitWid * 0.50f * cPortraitScale * 0.50f);
            float tRenderY = pYBottom - (pUseImage->GetTrueHeight() * cPortraitScale * 0.50f);
            if(pFlags & SUGAR_FLIP_HORIZONTAL) tRenderX = tRenderX - 600.0f;
            pUseImage->DrawScaled(tRenderX, tRenderY, cPortraitScale * 2.0f, cPortraitScale * 2.0f, pFlags);
        }
    }
    //--A "Wide" image uses a wider offset. Enemy portraits use a 1392 standard instead of a 694 standard.
    else
    {
        //--Normal rendering:
        if(!tIsSmallPortraitMode)
        {
            float tRenderX = pXCenter - (cWidePortraitWid * 0.50f * cPortraitScale);
            float tRenderY = pYBottom - (pUseImage->GetTrueHeight() * cPortraitScale);
            pUseImage->DrawScaled(tRenderX, tRenderY, cPortraitScale, cPortraitScale, pFlags);
        }
        //--Small portrait rendering. The portraits are even smaller.
        else
        {
            float tRenderX = pXCenter - (cWidePortraitWid * 0.50f * cPortraitScale * 1.00f);
            float tRenderY = pYBottom - (pUseImage->GetTrueHeight() * cPortraitScale * 0.50f);
            if(pFlags & SUGAR_FLIP_HORIZONTAL) tRenderX = tRenderX - 900.0f;
            pUseImage->DrawScaled(tRenderX, tRenderY, cPortraitScale * 2.0f, cPortraitScale * 2.0f, pFlags);
        }
    }

    //--Clean.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pGlobalAlpha);
}
void DialogueActor::RenderAtLayered(float pXCenter, float pYBottom, float pGlobalAlpha, uint32_t pFlags)
{
    //--Attempts to render assuming the portrait is a layered depiction. If the list contains no entries
    //  then doesn't render anything.
    //--Sizing for layered portraits is very specific and does not support low-res options for now.
    if(mrAdditionalLayerList->GetListSize() < 1) return;

    //--Constants. For now, layered emotions are always 700px wide.
    float cExpectedWid = 700.0f;

    //--If this entity is darkened, modify the mixer. All images use the same mixing.
    if(mIsDarkened)
    {
        StarlightColor::SetMixer(0.5f, 0.5f, 0.5f, pGlobalAlpha);
    }
    //--Normal color mixing.
    else
    {
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pGlobalAlpha);
    }

    //--Modify size. All layers use the same sizing.
    float cSizeAmount = SIZE_MOD_AMOUNT;
    float cPortraitScale = 1.00f;
    float cSizeMod = EasingFunction::QuadraticInOut(mSizeTimer, SIZE_MOD_TICKS) * cSizeAmount;
    if(cSizeMod != 0.0f) cPortraitScale = cPortraitScale + cSizeMod;

    //--Begin.
    SugarBitmap *rImage = (SugarBitmap *)mrAdditionalLayerList->PushIterator();
    while(rImage)
    {
        //--Compute positions and render.
        float tRenderX = pXCenter - (cExpectedWid * 0.50f * cPortraitScale);
        float tRenderY = pYBottom - (rImage->GetTrueHeight() * cPortraitScale);
        rImage->DrawScaled(tRenderX, tRenderY, cPortraitScale, cPortraitScale, pFlags);

        //--Next.
        rImage = (SugarBitmap *)mrAdditionalLayerList->AutoIterate();
    }

    //--Clean.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pGlobalAlpha);
}
void DialogueActor::RenderAtVN(float pXCenter, float pYBottom, float pGlobalAlpha, uint32_t pFlags)
{
    //--Overload, as below but uses the active image.
    RenderAtVN(rActiveImage, pXCenter, pYBottom, pGlobalAlpha, pFlags);
}
void DialogueActor::RenderAtVN(SugarBitmap *pUseImage, float pXCenter, float pYBottom, float pGlobalAlpha, uint32_t pFlags)
{
    //--Renders whatever the current image is at the provided position. Portraits always render
    //  bottom-up and are centered.
    //--Visual Novel portraits can use different sizing properties. These never use wide/narrow distinctions.
    if(!pUseImage) return;

    //--If this entity is darkened, modify the mixer.
    if(mIsDarkened)
    {
        StarlightColor::SetMixer(0.5f, 0.5f, 0.5f, pGlobalAlpha);
    }
    //--Normal color mixing.
    else
    {
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pGlobalAlpha);
    }

    //--Constants.
    float cPortraitScale = 1.00f;
    float cPortraitScaleInv = 1.00f;

    //--Resolve the size.
    float cSizeMod = EasingFunction::QuadraticInOut(mSizeTimer, SIZE_MOD_TICKS) * SIZE_MOD_AMOUNT;
    if(cSizeMod != 0.0f)
    {
        cPortraitScale = cPortraitScale + cSizeMod;
        cPortraitScaleInv = 1.0f / cPortraitScale;
    }

    //--Compute position.
    float tRenderX = pXCenter - (pUseImage->GetTrueWidth() * 0.50f * cPortraitScale);
    float tRenderY = pYBottom - (pUseImage->GetTrueHeight() * cPortraitScale);

    //--Render.
    glTranslatef(tRenderX, tRenderY, 0.0f);
    glScalef(cPortraitScale, cPortraitScale, 1.0f);
    pUseImage->Draw(0, 0, pFlags);
    glScalef(cPortraitScaleInv, cPortraitScaleInv, 1.0f);
    glTranslatef(-tRenderX, -tRenderY, 0.0f);

    //--Clean.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pGlobalAlpha);
}

//======================================= Pointer Routing =========================================
//====================================== Static Functions =========================================
//========================================= Lua Hooking ===========================================
void DialogueActor::HookToLuaState(lua_State *pLuaState)
{
    /* DialogueActor_Create(sName)
       Creates and pushes a new DialogueActor, also registering it to the WorldDialogue bench.
       Remember to pop it when you're done setup. */
    lua_register(pLuaState, "DialogueActor_Create", &Hook_DialogueActor_Create);

    /* DialogueActor_Exists(sName) (1 boolean)
       Returns whether or not the named dialogue actor actually exists. */
    lua_register(pLuaState, "DialogueActor_Exists", &Hook_DialogueActor_Exists);

    /* DialogueActor_Push(sName)
       Pushes the named DialogueActor onto the Activity Stack. */
    lua_register(pLuaState, "DialogueActor_Push", &Hook_DialogueActor_Push);

    /* DialogueActor_SetProperty("Add Alias", sAlias)
       DialogueActor_SetProperty("Add Emotion", sEmotionName, sDLPath)
       DialogueActor_SetProperty("Add Emotion", sEmotionName, sDLPath, bIsNotWide)
       DialogueActor_SetProperty("Add Emotion Rev", sEmotionName, sDLPath)
       DialogueActor_SetProperty("Add Emotion Rev", sEmotionName, sDLPath, bIsNotWide)
       DialogueActor_SetProperty("Remove Alias", sAlias)
       Sets the requested property in the active DialogueActor. */
    lua_register(pLuaState, "DialogueActor_SetProperty", &Hook_DialogueActor_SetProperty);
}

//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
int Hook_DialogueActor_Create(lua_State *L)
{
    //DialogueActor_Create(sName)
    int tArgs = lua_gettop(L);

    //--Push.
    DataLibrary::Fetch()->PushActiveEntity();

    //--Arg check.
    if(tArgs < 1) return LuaArgError("DialogueActor_Create");

    //--If the actor already exists, push the actor instead.
    DialogueActor *rCheckActor = WorldDialogue::Fetch()->GetActor(lua_tostring(L, 1));
    if(rCheckActor)
    {
        DataLibrary::Fetch()->rActiveObject = rCheckActor;
        return 0;
    }

    //--Create the Actor, basic setup.
    DialogueActor *nActor = new DialogueActor();
    nActor->SetName(lua_tostring(L, 1));

    //--Register it to the WorldDialogue.
    WorldDialogue::Fetch()->RegisterDialogueActor(nActor);
    DataLibrary::Fetch()->rActiveObject = nActor;
    return 0;
}
int Hook_DialogueActor_Exists(lua_State *L)
{
    //DialogueActor_Exists(sName) (1 boolean)
    int tArgs = lua_gettop(L);
    if(tArgs < 1)
    {
        LuaArgError("DialogueActor_Exists");
        lua_pushboolean(L, false);
        return 1;
    }

    //--Check and return.
    void *rTestPtr = WorldDialogue::Fetch()->GetActor(lua_tostring(L, 1));
    lua_pushboolean(L, (rTestPtr != NULL));
    return 1;
}
#include "LuaManager.h"
int Hook_DialogueActor_Push(lua_State *L)
{
    //DialogueActor_Push(sName)
    int tArgs = lua_gettop(L);

    //--Push.
    DataLibrary::Fetch()->PushActiveEntity();

    //--Arg check.
    if(tArgs < 1) return LuaArgError("DialogueActor_Push");

    //--Create the Actor, basic setup.
    DataLibrary::Fetch()->rActiveObject = WorldDialogue::Fetch()->GetActor(lua_tostring(L, 1));
    if(!DataLibrary::Fetch()->rActiveObject)
    {
        fprintf(stderr, "%s: Failed, actor %s was not found. %s\n", "DialogueActor_Push", lua_tostring(L, 1), LuaManager::Fetch()->GetCallStack(0));
    }
    return 0;
}
int Hook_DialogueActor_SetProperty(lua_State *L)
{
    //DialogueActor_SetProperty("Add Alias", sAlias)
    //DialogueActor_SetProperty("Add Emotion", sEmotionName, sDLPath)
    //DialogueActor_SetProperty("Add Emotion", sEmotionName, sDLPath, bIsNotWide)
    //DialogueActor_SetProperty("Add Emotion Rev", sEmotionName, sDLPath)
    //DialogueActor_SetProperty("Add Emotion Rev", sEmotionName, sDLPath, bIsNotWide)
    //DialogueActor_SetProperty("Remove Alias", sAlias)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("DialogueActor_SetProperty");

    //--Active object.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    DialogueActor *rDialogueActor = (DialogueActor *)rDataLibrary->rActiveObject;
    if(!rDialogueActor || !rDialogueActor->IsOfType(POINTER_TYPE_DIALOGUEACTOR)) return LuaTypeError("DialogueActor_SetProperty");

    //--Switching.
    const char *rSwitchType = lua_tostring(L, 1);

    //--Add a new alias.
    if(!strcasecmp(rSwitchType, "Add Alias") && tArgs == 2)
    {
        rDialogueActor->AddAlias(lua_tostring(L, 2));
    }
    //--Add a new emotion.
    else if(!strcasecmp(rSwitchType, "Add Emotion") && tArgs == 3)
    {
        rDialogueActor->AddPortrait(lua_tostring(L, 2), lua_tostring(L, 3), false);
    }
    //--Add a new emotion with the HD flag set.
    else if(!strcasecmp(rSwitchType, "Add Emotion") && tArgs == 4)
    {
        rDialogueActor->AddPortrait(lua_tostring(L, 2), lua_tostring(L, 3), lua_toboolean(L, 4));
    }
    //--Add a new emotion for the reverse case.
    else if(!strcasecmp(rSwitchType, "Add Emotion Rev") && tArgs == 3)
    {
        rDialogueActor->AddPortraitRev(lua_tostring(L, 2), lua_tostring(L, 3), false);
    }
    //--Add a new emotion with the HD flag set for the reverse case.
    else if(!strcasecmp(rSwitchType, "Add Emotion Rev") && tArgs == 4)
    {
        rDialogueActor->AddPortraitRev(lua_tostring(L, 2), lua_tostring(L, 3), lua_toboolean(L, 4));
    }
    //--Removes an alias.
    else if(!strcasecmp(rSwitchType, "Remove Alias") && tArgs == 2)
    {
        rDialogueActor->RemoveAlias(lua_tostring(L, 2));
    }
    //--Error.
    else
    {
        LuaPropertyError("DialogueActor_SetProperty", rSwitchType, tArgs);
    }

    //--Finish up.
    return 0;
}
