//--[DialogueActor]
//--Represents a portrait on the dialogue screen. These usually possess several emotions and render states.

#pragma once

//--[Includes]
#include "Definitions.h"
#include "Structures.h"
#include "RootObject.h"

//--[Local Structures]
//--[Local Definitions]
#define SIZE_MOD_TICKS 15.0f
#define SIZE_MOD_AMOUNT 0.05f

//--[Classes]
class DialogueActor : public RootObject
{
    private:
    //--System
    int mTicksSinceShown;

    //--Names
    char *mLocalName;
    SugarLinkedList *mAliasList; //Dummy Int, ref

    //--Images
    SugarBitmap *rActiveImage;
    SugarLinkedList *mImageList; //SugarBitmap *, ref
    SugarLinkedList *mImageListRev; //SugarBitmap *, ref
    SugarLinkedList *mIsNotWideImageList; //SugarBitmap *, ref
    SugarLinkedList *mIsNotWideImageListRev; //SugarBitmap *, ref

    //--Multi-layered emotions.
    SugarLinkedList *mrAdditionalLayerList; //SugarBitmap *, ref

    //--Render State
    int mSizeTimer;
    bool mIsDarkened;

    protected:

    public:
    //--System
    DialogueActor();
    virtual ~DialogueActor();

    //--Public Variables
    //--Property Queries
    const char *GetName();
    bool IsSpeaking(const char *pSpeakerName);
    bool IsDarkened();
    bool IsFullDark();
    SugarBitmap *GetActiveImage();

    //--Manipulators
    void SetName(const char *pName);
    void AddAlias(const char *pName);
    void RemoveAlias(const char *pName);
    void AddPortrait(const char *pName, const char *pDLPath, bool pIsNotWide);
    void AddPortraitRev(const char *pName, const char *pDLPath, bool pIsNotWide);
    void RemovePortrait(const char *pName);
    void SetActivePortrait(const char *pName);
    void SetDarkenedState(bool pFlag);

    //--Core Methods
    void SetDarkeningFromName(const char *pName);

    private:
    //--Private Core Methods
    public:
    //--Update
    void Update();

    //--File I/O
    //--Drawing
    void RenderAt(float pXCenter, float pYBottom, float pGlobalAlpha, uint32_t pFlags);
    void RenderAt(SugarBitmap *pUseImage, float pXCenter, float pYBottom, float pGlobalAlpha, uint32_t pFlags);
    void RenderAtLayered(float pXCenter, float pYBottom, float pGlobalAlpha, uint32_t pFlags);
    void RenderAtVN(float pXCenter, float pYBottom, float pGlobalAlpha, uint32_t pFlags);
    void RenderAtVN(SugarBitmap *pUseImage, float pXCenter, float pYBottom, float pGlobalAlpha, uint32_t pFlags);

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_DialogueActor_Create(lua_State *L);
int Hook_DialogueActor_Exists(lua_State *L);
int Hook_DialogueActor_Push(lua_State *L);
int Hook_DialogueActor_SetProperty(lua_State *L);
