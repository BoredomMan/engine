//--Base
#include "AdventureMenu.h"

//--Classes
#include "AdvCombat.h"
#include "AdventureLevel.h"
#include "TilemapActor.h"
#include "VisualLevel.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarFont.h"
#include "SugarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "EasingFunctions.h"
#include "GridHandler.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "LuaManager.h"

//--[Forward Declarations]
void RenderLineBounded(float pLft, float pTop, float pWidth, SugarFont *pFont, const char *pString);
void RenderTextConditionalColor(SugarFont *pFont, float pX, float pY, int pFlags, float pSize, const char *pText, bool pConditional, StarlightColor pTrueColor, StarlightColor pFalseColor);

//--[System]
void AdventureMenu::SetToReliveMode()
{
    //--[Setup]
    //--Set flags.
    mIsReliveMode = true;
    mReliveGridCurrent = 0;

    //--Clean old data.
    mReliveGrid->ClearList();

    //--[Positioning and Controls]
    //--Figure out where the grid entries go dynamically. A set of priorities is built and the grid is filled
    //  according to those priorities. The priority is set to require the smallest number of keypresses to
    //  access the largest number of entries. The grid is 3 high by default, and has padding.
    int cXSize = 5;
    int cYSize = 3;
    int cXMiddle = 2;
    int cYMiddle = 1;
    int **tGridData = GridHandler::BuildGrid(cXSize, cYSize, cXMiddle, cYMiddle, mReliveList->GetListSize());

    //--Debug.
    if(false)
    {
        fprintf(stderr, "Relive grid %ix%i:\n", cXSize-2, cYSize-2);
        for(int y = 1; y < cYSize-1+2; y ++)
        {
            for(int x = 1; x < cXSize-1+2; x ++)
            {
                fprintf(stderr, "%2i ", tGridData[x][y]);
            }
            fprintf(stderr, "\n");
        }
    }

    //--Call the subroutine to build the control list for us.
    GridHandler::BuildGridList(mReliveGrid, tGridData, cXSize, cYSize, mReliveList->GetListSize(), AM_GRID_CENT_X, AM_GRID_CENT_Y, AM_GRID_SPAC_X, AM_GRID_SPAC_Y);

    //--Clean up the grid data. This argument expects the original grid size.
    GridHandler::DeallocateGrid(tGridData, cXSize);
}

//--[Manipulators]
void AdventureMenu::ClearReliveList()
{
    mReliveList->ClearList();
}
void AdventureMenu::RegisterReliveScript(const char *pDisplayName, const char *pScriptPath, const char *pImagePath)
{
    //--Error check.
    if(!pDisplayName || !pScriptPath) return;

    //--Allocate a new relive package.
    SetMemoryData(__FILE__, __LINE__);
    RelivePackage *nPackage = (RelivePackage *)starmemoryalloc(sizeof(RelivePackage));
    nPackage->Initialize();
    strncpy(nPackage->mScriptPath, pScriptPath, STD_PATH_LEN);
    nPackage->rImage = (SugarBitmap *)DataLibrary::Fetch()->GetEntry(pImagePath);

    //--Register.
    mReliveList->AddElement(pDisplayName, nPackage, &FreeThis);
}

//--[Core Methods]
//--[Update]
void AdventureMenu::UpdateReliveMode()
{
    //--[Timers]
    //--If in relive mode, increment this timer.
    if(mIsReliveMode)
    {
        if(mReliveGridOpacityTimer < AM_CAMPFIRE_OPACITY_TICKS) mReliveGridOpacityTimer ++;
    }
    //--Decrement and stop if not.
    else
    {
        if(mReliveGridOpacityTimer > 0) mReliveGridOpacityTimer --;
        return;
    }

    //--[Setup]
    //--Handles controls for the relive menu. Handles up/down/activate/cancel.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Error check:
    if(mReliveGridCurrent < 0 || mReliveGridCurrent >= mReliveGrid->GetListSize()) mReliveGridCurrent = 0;
    GridPackage *rPackage = (GridPackage *)mReliveGrid->GetElementBySlot(mReliveGridCurrent);
    if(!rPackage)
    {
        mStartedHidingThisTick = true;
        mIsReliveMode = false;
        mCurrentCursor = AM_SAVE_RELIVE;
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        return;
    }

    //--[Directions]
    //--Directional controls are handled programmatically. If the instruction is -1, it means there is nothing
    //  at that grid position.
    int tReliveGridPrevious = mReliveGridCurrent;
    if(rControlManager->IsFirstPress("Up"))
    {
        int tTargetEntry = rPackage->mControlLookups[GRID_TOP];
        if(tTargetEntry != -1) mReliveGridCurrent = tTargetEntry;
    }
    if(rControlManager->IsFirstPress("Down"))
    {
        int tTargetEntry = rPackage->mControlLookups[GRID_BOT];
        if(tTargetEntry != -1) mReliveGridCurrent = tTargetEntry;
    }
    if(rControlManager->IsFirstPress("Left"))
    {
        int tTargetEntry = rPackage->mControlLookups[GRID_LFT];
        if(tTargetEntry != -1) mReliveGridCurrent = tTargetEntry;
    }
    if(rControlManager->IsFirstPress("Right"))
    {
        int tTargetEntry = rPackage->mControlLookups[GRID_RGT];
        if(tTargetEntry != -1) mReliveGridCurrent = tTargetEntry;
    }

    //--If the grid changed positions, play a sound.
    if(tReliveGridPrevious != mReliveGridCurrent)
    {
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }

    //--Size setting. All not-selected grid entries decrease their timers. The selected entry increases.
    GridPackage *rTimerPackage = (GridPackage *)mReliveGrid->PushIterator();
    while(rTimerPackage)
    {
        //--Increment if selected.
        if(rTimerPackage == rPackage)
        {
            if(rTimerPackage->mTimer < AM_CAMPFIRE_SIZE_TICKS) rTimerPackage->mTimer ++;
        }
        //--Decrement if deselected.
        else
        {
            if(rTimerPackage->mTimer > 0) rTimerPackage->mTimer --;
        }
        rTimerPackage = (GridPackage *)mReliveGrid->AutoIterate();
    }

    //--Relive a cutscene.
    if(rControlManager->IsFirstPress("Activate"))
    {
        //--Get the relive entry in question.
        RelivePackage *rRelivePackage = (RelivePackage *)mReliveList->GetElementBySlot(mReliveGridCurrent);
        if(!rRelivePackage) return;

        //--Execute the standard file to warp there.
        LuaManager::Fetch()->ExecuteLuaFile(rRelivePackage->mScriptPath, 1, "S", mReliveList->GetNameOfElementBySlot(mReliveGridCurrent));

        //--Close this menu.
        Hide();

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }
    //--Back to the main menu.
    else if(rControlManager->IsFirstPress("Cancel"))
    {
        mStartedHidingThisTick = true;
        mIsReliveMode = false;
        mCurrentCursor = AM_SAVE_RELIVE;
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }
}

//--[Drawing]
void AdventureMenu::RenderReliveMode()
{
    //--[Setup]
    //--Don't render at all if the opacity is at zero.
    if(mReliveGridOpacityTimer < 1) return;
    if(!Images.mIsReady) return;

    //--Opacity percent. Determines position and fading.
    float tOpacityPct = (float)mReliveGridOpacityTimer / (float)AM_CAMPFIRE_OPACITY_TICKS;

    //--[Title]
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, tOpacityPct);
    Images.CampfireMenu.rHeader->Draw(427.0f, 45.0f);
    Images.CampfireMenu.rHeadingFont->DrawText(683.0f, 57.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Relive Menu");
    StarlightColor::ClearMixer();

    //--[Icons]
    //--Sizes.
    float cIconSize = Images.CampfireMenu.rIconFrame->GetTrueWidth();

    //--Position the objects slide away from.
    float tOpacitySlidePct = EasingFunction::QuadraticOut(mReliveGridOpacityTimer, AM_CAMPFIRE_OPACITY_TICKS);
    float tCenterX = AM_GRID_CENT_X;
    float tCenterY = AM_GRID_CENT_Y;

    //--Iterate across the list.
    GridPackage *rGridPack = (GridPackage *)mReliveGrid->PushIterator();
    RelivePackage *rRelivePack = (RelivePackage *)mReliveList->PushIterator();
    while(rGridPack && rRelivePack)
    {
        //--Determine the rendering size. This also affects color blending.
        float cBoostPct = EasingFunction::QuadraticInOut(rGridPack->mTimer, AM_CAMPFIRE_SIZE_TICKS);
        float cScale = 3.0f + (cBoostPct * AM_CAMPFIRE_SIZE_BOOST);
        float cScaleInv = 1.0f / cScale;

        //--Figure out the render position.
        float tXPos = rGridPack->mXPos;
        float tYPos = rGridPack->mYPos;

        //--The actual render position is based on the opacity percent as the object slides out of the middle.
        tXPos = tCenterX + ((tXPos - tCenterX) * tOpacitySlidePct);
        tYPos = tCenterY + ((tYPos - tCenterY) * tOpacitySlidePct);

        //--Move the render position off by the scale of the object.
        tXPos = tXPos - (cIconSize * cScale * 0.50f);
        tYPos = tYPos - (cIconSize * cScale * 0.50f);

        //--Color blending.
        float cColVal = 0.75f + (0.25f * cBoostPct);
        StarlightColor::SetMixer(cColVal, cColVal, cColVal, tOpacityPct);

        //--Render.
        glTranslatef(tXPos, tYPos, 0.0f);
        glScalef(cScale, cScale, 1.0f);
        Images.CampfireMenu.rIconFrame->Draw();
        if(rRelivePack->rImage) rRelivePack->rImage->Draw(0, -3);
        glScalef(cScaleInv, cScaleInv, 1.0f);
        glTranslatef(-tXPos, -tYPos, 0.0f);

        //--Next.
        rGridPack = (GridPackage *)mReliveGrid->AutoIterate();
        rRelivePack = (RelivePackage *)mReliveList->AutoIterate();
    }

    //--After the main grid loop, re-run the loop and render the text. This is to make it appear over everything else.
    rGridPack = (GridPackage *)mReliveGrid->PushIterator();
    rRelivePack = (RelivePackage *)mReliveList->PushIterator();
    while(rGridPack && rRelivePack)
    {
        //--Determine the rendering size. This also affects color blending.
        float cBoostPct = EasingFunction::QuadraticInOut(rGridPack->mTimer, AM_CAMPFIRE_SIZE_TICKS);
        float cScale = 3.0f + (cBoostPct * AM_CAMPFIRE_SIZE_BOOST);

        //--Figure out the render position.
        float tXPos = rGridPack->mXPos;
        float tYPos = rGridPack->mYPos;

        //--The actual render position is based on the opacity percent as the object slides out of the middle.
        tXPos = tCenterX + ((tXPos - tCenterX) * tOpacitySlidePct);
        tYPos = tCenterY + ((tYPos - tCenterY) * tOpacitySlidePct);
        float tTextX = tXPos;

        //--Move the render position off by the scale of the object.
        tXPos = tXPos - (cIconSize * cScale * 0.50f);
        tYPos = tYPos - (cIconSize * cScale * 0.50f);

        //--Color blending.
        float cColVal = 0.75f + (0.25f * cBoostPct);
        StarlightColor::SetMixer(cColVal, cColVal, cColVal, tOpacityPct);

        //--Text.
        float cTxtScale = 1.0f + (cBoostPct * AM_CAMPFIRE_SIZE_BOOST);
        Images.CampfireMenu.rMainlineFont->DrawText(tTextX, tYPos - 15.0f, SUGARFONT_AUTOCENTER_X, cTxtScale, mReliveList->GetIteratorName());

        //--Next.
        rGridPack = (GridPackage *)mReliveGrid->AutoIterate();
        rRelivePack = (RelivePackage *)mReliveList->AutoIterate();
    }

    //--[Clean]
    StarlightColor::ClearMixer();
}
