//--Base
#include "AdventureMenu.h"

//--Classes
#include "AdvCombat.h"
#include "AdventureLevel.h"
#include "FlexMenu.h"
#include "VisualLevel.h"
#include "WorldDialogue.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarFont.h"

//--Definitions
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "DisplayManager.h"

//--[Manipulators]
void AdventureMenu::SetToOptionsMode()
{
    //--Mode setting.
    mCurrentMode = AM_MODE_OPTIONS;
    mOptionsCursor = 0;
    mFlexMenuIsRebinding = false;

    //--Fast-access pointers.
    AdvCombat *rAdventureCombat = AdvCombat::Fetch();

    //--Store the option values when this was opened.
    mOptionOldMusic = AudioManager::xMusicVolume;
    mOptionOldSound = AudioManager::xSoundVolume;
    mOptionOldDoctor = rAdventureCombat->IsAutoUseDoctorBag();
    mOptionOldTourist = rAdventureCombat->IsTouristMode();
    mOptionOldAmbientLight = AdventureLevel::Fetch()->GetLightBoost();
    mOptionOldMemoryCursor = rAdventureCombat->IsMemoryCursor();
}

//--[Core Methods]
void AdventureMenu::HandleOptionChange(int pCursor, bool pIsLeftPress)
{
    //--Changing the music volume:
    if(pCursor == AM_OPTIONS_MUSICVOL)
    {
        //--Decrement:
        if(pIsLeftPress)
        {
            AudioManager::Fetch()->ChangeMusicVolume(-0.05f);
        }
        //--Increment:
        else
        {
            AudioManager::Fetch()->ChangeMusicVolume(0.05f);
        }
    }
    //--Changing the sound volume:
    else if(pCursor == AM_OPTIONS_SOUNDVOL)
    {
        //--Decrement:
        if(pIsLeftPress)
        {
            AudioManager::Fetch()->ChangeSoundVolume(-0.05f);
        }
        //--Increment:
        else
        {
            AudioManager::Fetch()->ChangeSoundVolume(0.05f);
        }
    }
    //--Toggling auto-doctor bag on or off:
    else if(pCursor == AM_OPTIONS_AUTODOCTORBAG)
    {
        //--Left to false:
        if(pIsLeftPress)
        {
            AdvCombat::Fetch()->SetAutoUseDoctorBag(false);
        }
        //--Right to true:
        else
        {
            AdvCombat::Fetch()->SetAutoUseDoctorBag(true);
        }

        //--Match these values for the matching DataLibrary variable.
        SysVar *rSystemVariable = (SysVar *)DataLibrary::Fetch()->GetEntry("Root/Variables/System/Special/iAutoUseDoctorBag");
        if(rSystemVariable)
        {
            rSystemVariable->mNumeric = AdvCombat::Fetch()->IsAutoUseDoctorBag();
        }
    }
    //--Toggling auto-hasten on or off.
    else if(pCursor == AM_OPTIONS_AUTOHASTEN)
    {
        //--Left to false:
        if(pIsLeftPress)
        {
            WorldDialogue::Fetch()->SetAutoHastenFlag(false);
        }
        //--Right to true:
        else
        {
            WorldDialogue::Fetch()->SetAutoHastenFlag(true);
        }

        //--Match these values for the matching DataLibrary variable.
        SysVar *rSystemVariable = (SysVar *)DataLibrary::Fetch()->GetEntry("Root/Variables/System/Special/iAutoHastenDialogue");
        if(rSystemVariable)
        {
            rSystemVariable->mNumeric = 0.0f;
            if(WorldDialogue::Fetch()->IsAutoHastening()) rSystemVariable->mNumeric = 1.0f;
        }
    }
    //--Activate or deactivate Tourist Mode.
    else if(pCursor == AM_OPTIONS_TOURISTMODE)
    {
        //--Left to false:
        if(pIsLeftPress)
        {
            AdvCombat::Fetch()->SetTouristMode(false);
        }
        //--Right to true:
        else
        {
            AdvCombat::Fetch()->SetTouristMode(true);
        }

        //--Match these values for the matching DataLibrary variable.
        SysVar *rSystemVariable = (SysVar *)DataLibrary::Fetch()->GetEntry("Root/Variables/System/Special/iIsTouristMode");
        if(rSystemVariable)
        {
            rSystemVariable->mNumeric = AdvCombat::Fetch()->IsTouristMode();
        }
    }
    //--Ambient Light Boost
    else if(pCursor == AM_OPTIONS_AMBIENTLIGHT)
    {
        //--Base.
        float tAmbientLight = AdventureLevel::Fetch()->GetLightBoost();

        //--Decrement:
        if(pIsLeftPress)
        {
            tAmbientLight = tAmbientLight - 0.05f;
            AdventureLevel::Fetch()->SetLightBoost(tAmbientLight);
        }
        //--Increment:
        else
        {
            tAmbientLight = tAmbientLight + 0.05f;
            AdventureLevel::Fetch()->SetLightBoost(tAmbientLight);
        }
    }
    //--Activate or deactivate memory cursor.
    else if(pCursor == AM_OPTIONS_MEMORYCURSOR)
    {
        //--Left to false:
        if(pIsLeftPress)
        {
            AdvCombat::Fetch()->SetMemoryCursor(false);
        }
        //--Right to true:
        else
        {
            AdvCombat::Fetch()->SetMemoryCursor(true);
        }

        //--Match these values for the matching DataLibrary variable.
        SysVar *rSystemVariable = (SysVar *)DataLibrary::Fetch()->GetEntry("Root/Variables/System/Special/iMemoryCursor");
        if(rSystemVariable)
        {
            if(AdvCombat::Fetch()->IsMemoryCursor())
            {
                rSystemVariable->mNumeric = 1.0f;
            }
            else
            {
                rSystemVariable->mNumeric = 0.0f;
            }
        }
    }
}

//--[Main Functions]
void AdventureMenu::UpdateOptionsMode()
{
    //--Handles controls for the options menu.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--If the FlexMenu is handling control rebinding, deal with that here.
    if(mFlexMenuIsRebinding)
    {
        mLocalizedRebindMenu->Update();
        if(!mLocalizedRebindMenu->IsRebindingMode()) mFlexMenuIsRebinding = false;
        return;
    }

    //--Left!
    if(rControlManager->IsFirstPress("Left"))
    {
        //--Subroutine:
        if(mOptionsCursor < AM_OPTIONS_OKAY)
        {
            HandleOptionChange(mOptionsCursor, true);
        }
        //--Move right once.
        else
        {
            mOptionsCursor = AM_OPTIONS_OKAY;
        }

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }
    //--Right.
    if(rControlManager->IsFirstPress("Right"))
    {
        //--Subroutine:
        if(mOptionsCursor < AM_OPTIONS_OKAY)
        {
            HandleOptionChange(mOptionsCursor, false);
        }
        //--Move right once.
        else
        {
            mOptionsCursor = AM_OPTIONS_DEFAULTS;
        }

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }

    //--Up
    if(rControlManager->IsFirstPress("Up"))
    {
        //--Move up. Move twice if moving off the defaults button.
        if(mOptionsCursor > AM_OPTIONS_MUSICVOL) mOptionsCursor --;
        if(mOptionsCursor == AM_OPTIONS_OKAY) mOptionsCursor --;

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }
    //--Down
    if(rControlManager->IsFirstPress("Down"))
    {
        //--Move down if not on the okay/defaults prompts.
        if(mOptionsCursor < AM_OPTIONS_OKAY)
        {
            mOptionsCursor ++;
        }

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }

    //--Pressing Activate:
    if(rControlManager->IsFirstPress("Activate"))
    {
        //--Rebind button:
        if(mOptionsCursor == AM_OPTIONS_CONTROLREBIND)
        {
            mFlexMenuIsRebinding = true;
            mLocalizedRebindMenu->ActivateRebindingMode();
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
        //--On the "Okay" button:
        else if(mOptionsCursor == AM_OPTIONS_OKAY)
        {
            //--Back out of options mode. We don't need to do anything else.
            mCurrentMode = AM_MODE_BASE;
            mCurrentCursor = AM_MAIN_OPTIONS;
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
        //--On the "Defaults" button:
        else if(mOptionsCursor == AM_OPTIONS_DEFAULTS)
        {
            //--Reset to defaults.
            AudioManager::Fetch()->ChangeMusicVolume(0.50f - AudioManager::xMusicVolume);
            AudioManager::Fetch()->ChangeSoundVolume(0.75f - AudioManager::xSoundVolume);
            AdvCombat::Fetch()->SetAutoUseDoctorBag(true);
            AdvCombat::Fetch()->SetTouristMode(false);
            AdventureLevel::Fetch()->SetLightBoost(0.0f);
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            AdvCombat::Fetch()->SetMemoryCursor(true);
        }
    }

    //--Back to the main menu.
    if(rControlManager->IsFirstPress("Cancel"))
    {
        //--Reset everything to where it was when this was opened.
        AudioManager::Fetch()->ChangeMusicVolume(mOptionOldMusic - AudioManager::xMusicVolume);
        AudioManager::Fetch()->ChangeSoundVolume(mOptionOldSound - AudioManager::xSoundVolume);
        AdvCombat::Fetch()->SetAutoUseDoctorBag(mOptionOldDoctor);
        AdvCombat::Fetch()->SetTouristMode(mOptionOldTourist);
        AdventureLevel::Fetch()->SetLightBoost(mOptionOldAmbientLight);
        AdvCombat::Fetch()->SetMemoryCursor(mOptionOldMemoryCursor);

        //--Back out of options mode.
        mCurrentMode = AM_MODE_BASE;
        mCurrentCursor = AM_MAIN_OPTIONS;
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }
}

//--[Worker Function]
void ConditionalColor(bool pCondition)
{
    //--If the condition is false, set to a greyed out color.
    if(!pCondition)
    {
        StarlightColor::SetMixer(0.5f, 0.5f, 0.5f, 1.0f);
    }
    //--White color.
    else
    {
        StarlightColor::ClearMixer();
    }
}

void AdventureMenu::RenderOptionsMode()
{
    //--[Documentation and Setup]
    //--Renders the options menu. Pretty simple stuff.
    if(!Images.mIsReady) return;

    //--Constants
    float cXPosition = 602.0f;
    float cYIndent = 4.0f;

    //--Static parts.
    Images.OptionsUI.rMenuBack->Draw();
    Images.OptionsUI.rMenuInsert->Draw();
    Images.OptionsUI.rHeader->Draw();
    Images.OptionsUI.rBars->Draw();
    Images.OptionsUI.rSliders->Draw();
    Images.OptionsUI.rTextBoxes->Draw();
    Images.OptionsUI.rConfirmButtons->Draw();

    //--Static Text
    Images.OptionsUI.rHeadingFont->DrawText(683.0f, 147.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Settings");

    //--[Volume Options]
    //--Music Volume
    ConditionalColor(mOptionsCursor == AM_OPTIONS_MUSICVOL);
    Images.OptionsUI.rMainlineFont->DrawText(cXPosition, 232.0f + cYIndent, SUGARFONT_AUTOCENTER_X, 1.0f, "Music Volume");
    Images.OptionsUI.rSliderButtons->Draw(709.0f + (145.0f * AudioManager::xMusicVolume), 232.0f);

    //--Sound Volume
    ConditionalColor(mOptionsCursor == AM_OPTIONS_SOUNDVOL);
    Images.OptionsUI.rMainlineFont->DrawText(cXPosition, 273.0f + cYIndent, SUGARFONT_AUTOCENTER_X, 1.0f, "Sound Volume");
    Images.OptionsUI.rSliderButtons->Draw(709.0f + (145.0f * AudioManager::xSoundVolume), 273.0f);

    //--[Boolean Options]
    //--Doctor Bar
    ConditionalColor(mOptionsCursor == AM_OPTIONS_AUTODOCTORBAG);
    Images.OptionsUI.rMainlineFont->DrawText(cXPosition, 315.0f + cYIndent, SUGARFONT_AUTOCENTER_X, 1.0f, "Auto-use Doctor Bag");
    if(AdvCombat::Fetch()->IsAutoUseDoctorBag())
        Images.OptionsUI.rMainlineFont->DrawText(817.0f, 315.0f + cYIndent, SUGARFONT_AUTOCENTER_X, 1.0f, "True");
    else
        Images.OptionsUI.rMainlineFont->DrawText(817.0f, 315.0f + cYIndent, SUGARFONT_AUTOCENTER_X, 1.0f, "False");

    //--Auto-hasten dialogue.
    ConditionalColor(mOptionsCursor == AM_OPTIONS_AUTOHASTEN);
    Images.OptionsUI.rMainlineFont->DrawText(cXPosition, 360.0f + cYIndent, SUGARFONT_AUTOCENTER_X, 1.0f, "Auto-hasten Dialogue");
    if(WorldDialogue::Fetch()->IsAutoHastening())
        Images.OptionsUI.rMainlineFont->DrawText(817.0f, 360.0f + cYIndent, SUGARFONT_AUTOCENTER_X, 1.0f, "True");
    else
        Images.OptionsUI.rMainlineFont->DrawText(817.0f, 360.0f + cYIndent, SUGARFONT_AUTOCENTER_X, 1.0f, "False");

    //--Tourist Mode.
    ConditionalColor(mOptionsCursor == AM_OPTIONS_TOURISTMODE);
    Images.OptionsUI.rMainlineFont->DrawText(cXPosition, 442.0f + cYIndent, SUGARFONT_AUTOCENTER_X, 1.0f, "Tourist Mode");
    if(AdvCombat::Fetch()->IsTouristMode())
        Images.OptionsUI.rMainlineFont->DrawText(817.0f, 442.0f + cYIndent, SUGARFONT_AUTOCENTER_X, 1.0f, "True");
    else
        Images.OptionsUI.rMainlineFont->DrawText(817.0f, 442.0f + cYIndent, SUGARFONT_AUTOCENTER_X, 1.0f, "False");

    //--Ambient Lighting
    ConditionalColor(mOptionsCursor == AM_OPTIONS_AMBIENTLIGHT);
    Images.OptionsUI.rMainlineFont->DrawText(cXPosition, 485.0f + cYIndent, SUGARFONT_AUTOCENTER_X, 1.0f, "Ambient Light Boost");
    Images.OptionsUI.rSliderButtons->Draw(709.0f + (145.0f * AdventureLevel::Fetch()->GetLightBoost()), 486.0f);

    //--Combat Memory Cursor
    ConditionalColor(mOptionsCursor == AM_OPTIONS_MEMORYCURSOR);
    Images.OptionsUI.rMainlineFont->DrawText(cXPosition, 528.0f + cYIndent, SUGARFONT_AUTOCENTER_X, 1.0f, "Combat Memory Cursor");
    if(AdvCombat::Fetch()->IsMemoryCursor())
        Images.OptionsUI.rMainlineFont->DrawText(817.0f, 528.0f + cYIndent, SUGARFONT_AUTOCENTER_X, 1.0f, "True");
    else
        Images.OptionsUI.rMainlineFont->DrawText(817.0f, 528.0f + cYIndent, SUGARFONT_AUTOCENTER_X, 1.0f, "False");

    //--[Control Rebinding]
    ConditionalColor(mOptionsCursor == AM_OPTIONS_CONTROLREBIND);
    Images.OptionsUI.rMainlineFont->DrawText(cXPosition, 570.0f + cYIndent, SUGARFONT_AUTOCENTER_X, 1.0f, "Rebind Controls");

    //--[Bottom Options]
    //--Soka.
    ConditionalColor(mOptionsCursor == AM_OPTIONS_OKAY);
    Images.OptionsUI.rHeadingFont->DrawText(526.0f, 642.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Okay");

    //--Reset to Defaults.
    ConditionalColor(mOptionsCursor == AM_OPTIONS_DEFAULTS);
    Images.OptionsUI.rHeadingFont->DrawText(792.0f, 642.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Defaults");

    //--[Rebinding Mode]
    //--Render this over the menu.
    if(mFlexMenuIsRebinding)
    {
        SugarBitmap::DrawFullColor(StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, 0.5f));
        mLocalizedRebindMenu->Render();
    }
}
