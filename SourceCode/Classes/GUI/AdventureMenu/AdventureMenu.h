//--[AdventureMenu]
//--Menu used in Adventure Mode. Has sub-menus for things like status, party arrangement, equipment, and inventory.

#pragma once

//--[Includes]
#include "Definitions.h"
#include "Structures.h"
#include "RootObject.h"
#include "GridHandler.h"

//--[Local Structures]
//--Forward Declaration
#ifndef _ADAM_TYPES_
#define _ADAM_TYPES_
#define CRAFT_ADAMANTITE_POWDER 0
#define CRAFT_ADAMANTITE_FLAKES 1
#define CRAFT_ADAMANTITE_SHARD 2
#define CRAFT_ADAMANTITE_PIECE 3
#define CRAFT_ADAMANTITE_CHUNK 4
#define CRAFT_ADAMANTITE_ORE 5
#define CRAFT_ADAMANTITE_TOTAL 6
#endif

//--Structure Declarations
struct LoadingPack;

//--[Local Definitions]
//--Modes
#define AM_MODE_BASE 0
#define AM_MODE_ITEMS 1
#define AM_MODE_EQUIP 2
#define AM_MODE_STATUS 3
#define AM_MODE_MAP 4
#define AM_MODE_OPTIONS 5
#define AM_MODE_QUIT 6
#define AM_MODE_HEAL 7
#define AM_MODE_TOTAL 8

//--Main Menu codes
#define AM_MAIN_ITEMS 0
#define AM_MAIN_EQUIPMENT 1
#define AM_MAIN_HEAL 2
#define AM_MAIN_STATUS 3
#define AM_MAIN_MAP 4
#define AM_MAIN_OPTIONS 5
#define AM_MAIN_QUIT 6
#define AM_MAIN_TOTAL 7

//--Item Submenu codes
#define AM_ITEM_USE 0
#define AM_ITEM_DROP 1

//--Save Menu codes
#define AM_SAVE_REST 0
#define AM_SAVE_CHAT 1
#define AM_SAVE_SAVE 2
#define AM_SAVE_COSTUMES 3
#define AM_SAVE_SKILLBOOKS 4
#define AM_SAVE_FORM 5
#define AM_SAVE_WARP 6
#define AM_SAVE_RELIVE 7
#define AM_SAVE_PASSWORD 8
#define AM_SAVE_TOTAL 9

//--Options Menu codes
#define AM_OPTIONS_MUSICVOL 0
#define AM_OPTIONS_SOUNDVOL 1
#define AM_OPTIONS_AUTODOCTORBAG 2
#define AM_OPTIONS_AUTOHASTEN 3
#define AM_OPTIONS_TOURISTMODE 4
#define AM_OPTIONS_AMBIENTLIGHT 5
#define AM_OPTIONS_MEMORYCURSOR 6
#define AM_OPTIONS_CONTROLREBIND 7
#define AM_OPTIONS_OKAY 8
#define AM_OPTIONS_DEFAULTS 9
#define AM_OPTIONS_TOTAL 10

//--Quit Menu codes
#define AM_QUIT_CANCEL 0
#define AM_QUIT_TO_TITLE 1
#define AM_QUIT_PROGRAM 2
#define AM_QUIT_TOTAL 3

//--Sizing
#define AM_STD_INDENT 12.0f
#define AM_CHARACTER_CARD_X 192.0f
#define AM_CHARACTER_CARD_Y 192.0f
#define AM_ITEMS_PER_PAGE 16
#define AM_GEMS_PER_PAGE 24
#define AM_SHOP_ITEMS_PER_PAGE 17

//--Shop Types
#define AM_SELECT_SHOP_MODE 0
#define AM_BUY 1
#define AM_SELL 2
#define AM_PURIFY 3
#define AM_GEMCUTTER 4

//--Cursor for Mode Selection in Shops
#define AM_SHOP_SELECT_MIN 0
#define AM_SHOP_SELECT_BUY 0
#define AM_SHOP_SELECT_SELL 1
#define AM_SHOP_SELECT_PURIFY 2
#define AM_SHOP_SELECT_GEMCUTTER 3
#define AM_SHOP_SELECT_MAX 3

//--Save Selection
#define AM_SCAN_TOTAL 100
#define AM_FILES_PER_PAGE 5

//--Ability Inspector
#define AM_ABILITY_MODE_TICKS 10.0f

//--Map Zooming
#define AM_MAP_ZOOM_TICKS 15

//--Inventory Image Headers
#define AM_INV_HEADER_HP 0
#define AM_INV_HEADER_ATK 1
#define AM_INV_HEADER_PRT 2
#define AM_INV_HEADER_ACC 3
#define AM_INV_HEADER_EVD 4
#define AM_INV_HEADER_INI 5
#define AM_INV_HEADER_SLASHING 6
#define AM_INV_HEADER_PIERCING 7
#define AM_INV_HEADER_STRIKING 8
#define AM_INV_HEADER_BLEEDING 9
#define AM_INV_HEADER_POISONING 10
#define AM_INV_HEADER_CORRODING 11
#define AM_INV_HEADER_FLAMING 12
#define AM_INV_HEADER_FREEZING 13
#define AM_INV_HEADER_SHOCKING 14
#define AM_INV_HEADER_CRUSADING 15
#define AM_INV_HEADER_OBSCURING 16
#define AM_INV_HEADER_BLINDING 17
#define AM_INV_HEADER_TERRIFYING 18
#define AM_INV_HEADER_DOWNX3 19
#define AM_INV_HEADER_DOWNX2 20
#define AM_INV_HEADER_DOWNX1 21
#define AM_INV_HEADER_NEUTRAL 22
#define AM_INV_HEADER_UPX1 23
#define AM_INV_HEADER_UPX2 24
#define AM_INV_HEADER_UPX3 25
#define AM_INV_HEADER_RARROW 26
#define AM_INV_HEADER_TOTAL 27

#define AM_INV_NAME_REMAP_MAXLEN 4

//--[Advanced Map Pack]
//--Contains an image layer when using multi-layered images, as well as a chance for that
//  image to appear. This creates the flashing effect.
typedef struct AdvancedMapPack
{
    SugarBitmap *rImage;
    int mRenderChanceLo;
    int mRenderChanceHi;
}AdvancedMapPack;

//--[Costume Heading Pack]
//--Contains a character's name, image, form, and which sublist contains their costume packages.
typedef struct CostumeHeadingPack
{
    SugarBitmap *rImage;
    char mCharacterName[STD_MAX_LETTERS];
    char mFormName[STD_MAX_LETTERS];
    int mSublistIndex;
}CostumeHeadingPack;

//--[Costume Pack]
//--Contains a costume entry for a character in a given form.
typedef struct CostumePack
{
    bool mIsSelected;
    SugarBitmap *rImage;
    char mExecPath[STD_PATH_LEN];
}CostumePack;

//--[TFComparisonPack]
//--Definitions
#define AM_COMPARE_STATS 6
#define AM_COMPARE_RESISTANCES 13

//--Stores comparison data.
typedef struct TFComparisonPack
{
    //--Members
    SugarBitmap *rImages[AM_INV_HEADER_TOTAL];

    //--Functions
    void Initialize()
    {
        memset(rImages, 0, sizeof(SugarBitmap *) * AM_INV_HEADER_TOTAL);
    }
}TFComparisonPack;

//--[Grid Entry]
//--Grid Opacity Values
#define AM_CAMPFIRE_OPACITY_TICKS 10
#define AM_CAMPFIRE_SIZE_TICKS 10
#define AM_CAMPFIRE_SIZE_BOOST 1.50f

//--Grid Positioning Values
#define AM_GRID_CENT_X (VIRTUAL_CANVAS_X * 0.50f)
#define AM_GRID_CENT_Y (VIRTUAL_CANVAS_Y * 0.40f)
#define AM_GRID_SPAC_X (VIRTUAL_CANVAS_X * 0.10f)
#define AM_GRID_SPAC_Y (VIRTUAL_CANVAS_X * 0.10f)

//--[Relive Package]
//--Contains a path and an image, used for the Relive submenu.
typedef struct RelivePackage
{
    //--Members
    char mScriptPath[STD_PATH_LEN];
    SugarBitmap *rImage;

    //--Functions
    void Initialize()
    {
        memset(mScriptPath, 0, sizeof(char) * STD_PATH_LEN);
        rImage = NULL;
    }
}RelivePackage;

//--[Warp Package]
//--Constains a map destination, an image, and a set of coordinates. Used by the Warp submenu.
#define WARP_MAX_PER_REGION 32
typedef struct WarpPackage
{
    //--Members
    int mRoomsTotal;
    char mWarpNames[WARP_MAX_PER_REGION][STD_MAX_LETTERS];
    char mWarpRooms[WARP_MAX_PER_REGION][STD_MAX_LETTERS];
    float mCoordX[WARP_MAX_PER_REGION];
    float mCoordY[WARP_MAX_PER_REGION];
    SugarBitmap *rMapImage;
    SugarBitmap *rMapOverlayImage;
    char mCallBuffer[STD_MAX_LETTERS];

    //--Functions
    void Initialize()
    {
        mRoomsTotal = 0;
        memset(mWarpNames, 0, sizeof(char) * WARP_MAX_PER_REGION * STD_MAX_LETTERS);
        memset(mWarpRooms, 0, sizeof(char) * WARP_MAX_PER_REGION * STD_MAX_LETTERS);
        memset(mCoordX, 0, sizeof(float) * WARP_MAX_PER_REGION);
        memset(mCoordY, 0, sizeof(float) * WARP_MAX_PER_REGION);
        memset(mCallBuffer, 0, sizeof(char) * STD_MAX_LETTERS);
        rMapImage = NULL;
        rMapOverlayImage = NULL;
    }
}WarpPackage;

//--[Classes]
class AdventureMenu : public RootObject
{
    private:
    //--System
    bool mStartedHidingThisTick;
    int mMainOpacityTimer;
    bool mIsVisible;
    bool mIsCampfireMode;
    uint8_t mCurrentMode;

    //--Common
    int mCurrentCursor;
    float mMenuWidest;
    float mMenuWidestSecond;

    //--Main Menu
    TwoDimensionReal mMainDimensions;
    TwoDimensionReal mCraftingDimensions;
    TwoDimensionReal mPlatinaDimensions;
    TwoDimensionReal mCatalystDimensions;
    TwoDimensionReal mPartyBoardDimensions;

    //--Equipment Menu
    bool mRebuildModifyList;
    int mCharacterIndex;
    bool mIsModifying;
    int mModifySlot;
    TwoDimensionReal mEquipDimensions;
    TwoDimensionReal mEquipListDimensions;
    SugarLinkedList *mValidEquipsList;
    int mEquipMinimumRender;
    int mGemMinimumRender;
    bool mIsSocketEditingMode;
    int mSocketEditTimer;
    int mSocketEditSlot;
    int mSocketQuerySlot;

    //--Items Menu
    int mInventorySkip;
    bool mIsItemSubMenuOpen;
    int mItemSubCursor;
    TwoDimensionReal mItemDimensions;
    TwoDimensionReal mItemPropertyDimensions;

    //--[Saving / File Selection]
    //--System
    bool mIsBenchMode;
    bool mIsEditingNoteForNewSave;
    bool mIsSelectFileMode;
    int mEmptySaveSlotsTotal;
    int mSaveOpacityTimer;

    //--Selection and Navigation
    int mSelectFileCursor;
    int mSelectFileLastColumn;
    int mSelectFileOffset;
    int mHighestCursor;
    int mEditingFileCursor;
    int mArrowBobTimer;
    int mArrowOpacityTimerLft;
    int mArrowOpacityTimerRgt;

    //--Page Moving
    bool mIsPageMoving;
    bool mIsPageMovingUp;
    int mPageMoveTimer;

    //--File Information Storage
    int mCurrentSortingType;
    int mFileListingIndex[AM_SCAN_TOTAL];
    char mFileListing[AM_SCAN_TOTAL][80];
    LoadingPack *mLoadingPacks[AM_SCAN_TOTAL];
    bool mExistingFileListing[AM_SCAN_TOTAL];

    //--Entering Notes
    bool mIsEnteringNotes;
    int mNoteSlot;
    StringEntry *mStringEntryForm;

    //--Deleting a Save
    int mSaveDeletionIndex;
    int mSaveDeletionIndexOld;
    int mSaveDeletionOpacityTimer;

    //--[Forms Submenu]
    //--System
    bool mIsFormsMode;
    int mFormsPartyMember;

    //--Character Selection.
    int mFormsOpacityTimer;
    int mFormsGridCurrent;
    SugarLinkedList *mFormsPartyList;    //dummy, ref, parallel with mFormsPartyGrid
    SugarLinkedList *mFormsPartyGrid;    //GridPackage *, master
    SugarLinkedList *mFormsPartyGridImg; //SugarBitmap *, ref, parallel with mFormsGrid

    //--Form Selection.
    int mFormSubOpacityTimer;
    int mFormSubGridCurrent;
    SugarLinkedList *mFormSubGrid;         //GridPackage *, master
    SugarLinkedList *mFormSubPaths;        //char *, master, parallel with mFormSubGrid
    SugarLinkedList *mFormSubDescriptions; //char *, master, parallel with mFormSubGrid
    SugarLinkedList *mFormSubGridImg;      //SugarBitmap *, ref, parallel with mFormSubGrid
    TFComparisonPack mBasePackage;
    SugarLinkedList *mFormSubComparisons;  //TFComparisonPack *, master, parallel with mFormSubGrid

    //--Lookup Tables
    int mCompareStatIndices[AM_COMPARE_STATS];
    int mCompareResistanceIndices[AM_COMPARE_RESISTANCES];

    //--[Other Submenus]
    //--Gemcutter Menu
    bool mIsGemcutterMode;
    int mGemcutterCursor;
    int mSelectedGemCursor;
    bool mGemConfirmCutMode;
    static bool xHasBuiltIconRemaps;
    static char xUpgradeIconRemaps[AM_INV_HEADER_TOTAL][AM_INV_NAME_REMAP_MAXLEN];

    //--Status Menu
    int mStatusCursor;
    bool mIsAbilityMode;
    int mAbilityRenderMin;
    int mAbilityCursor;
    int mAbilityCursorMax;
    int mAbilityTimer;
    TwoDimensionReal mStatusBacking;

    //--Shop Menu
    bool mIsShopMode;
    bool mShopCanSell;
    bool mShopCanGemcut;
    char mShopName[STD_MAX_LETTERS];
    int mShopModeType;
    int mShopCursor;
    int mShopSkip;
    bool mIsExchanging;
    char *mShopTeardownPath;
    int mComparisonCharacter;
    int mComparisonSlot;
    int mCharacterWalkTimer;
    bool mMultipleComparisonCharacters;
    SugarLinkedList *mShopInventory;
    SugarLinkedList *mDescriptionLines;

    //--Shop Fading
    bool mIsShopFading;
    int mShopFadeMode;
    int mShopFadeTimer;

    //--Options Menu
    int mOptionsCursor;
    TwoDimensionReal mOptionsBacking;
    float mOptionOldMusic;
    float mOptionOldSound;
    bool mOptionOldDoctor;
    bool mOptionOldTourist;
    float mOptionOldAmbientLight;
    bool mOptionOldMemoryCursor;

    //--Localized FlexMenu for Rebinding
    bool mFlexMenuIsRebinding;
    FlexMenu *mLocalizedRebindMenu;

    //--Quit Menu
    int mQuitCursor;
    bool mIsTitleQuitConfirmMode;
    bool mIsFullQuitConfirmMode;
    bool mIsFadingForQuit;
    int mQuitFadeTimer;

    //--Map Mode
    bool mIsEnteringMapMode;
    bool mHasRendered;
    int mMapTimer;
    int mMapFadeTimer;
    int mMapFocusPointX;
    int mMapFocusPointY;
    float mMapZoom;
    float mMapZoomStart;
    float mMapZoomDest;
    int mMapZoomTimer;
    SugarBitmap *rActiveMap;
    SugarBitmap *rActiveMapOverlay;
    int mPlayerMapPositionX;
    int mPlayerMapPositionY;
    float mMapScrollTimerX;
    float mMapScrollTimerY;

    //--Advanced Map Mode
    int mAdvancedMapLastRoll;
    int mAdvancedMapReroll;
    float mAdvancedMapRemapX;
    float mAdvancedMapRemapY;
    float mAdvancedMapScaleX;
    float mAdvancedMapScaleY;
    SugarLinkedList *mMapLayerList; //AdvancedMapPack *, master

    //--Advanced Map Archive
    int mArchiveAdvancedMapLastRoll;
    int mArchiveAdvancedMapReroll;
    float mArchiveAdvancedMapRemapX;
    float mArchiveAdvancedMapRemapY;
    float mArchiveAdvancedMapScaleX;
    float mArchiveAdvancedMapScaleY;
    SugarLinkedList *mArchiveMapLayerList; //AdvancedMapPack *, master

    //--Campfire Grid Menu
    int mCampfireGridOpacityTimer;
    int mCampfireGridCurrent;
    GridPackage mCampfireGrid[AM_SAVE_TOTAL];

    //--[Party Chatting]
    //--System
    bool mIsChatMode;
    int mChatGridCurrent;
    int mChatGridOpacityTimer;
    SugarLinkedList *mChatGrid;    //GridPackage *, master
    SugarLinkedList *mChatGridImg; //SugarBitmap *, ref,    parallel with mChatGrid
    SugarLinkedList *mChatListing; //char *,        master, parallel with mChatGrid

    //--[Costumes]
    //--System
    bool mIsCostumesMenu;
    int mCostumeGridCurrent;
    int mCostumeGridOpacityTimer;
    SugarLinkedList *mCostumeGrid;         //GridPackage *, master
    SugarLinkedList *mCostumeGridHeaders;  //CostumeHeadingPack *, master, parallel mCostumeGrid
    SugarLinkedList *mCostumeGridSublists; //SugarLinkedList *, master, parallel with mCostumeGridHeaders -> contains CostumePack *, master.

    //--Costume Selection
    bool mIsCostumesSubMenu;
    int mCostumeCharacterIndex;
    int mCostumeCharacterCurrent;
    int mCostumeCharacterOpacityTimer;
    SugarLinkedList *mCostumeCharacterGrid; //GridPackage *, master

    //--[Skillbook Menu]
    bool mIsSkillbookMode;
    int mSkillbookCursor;
    SugarLinkedList *mSkillbookList; //char *, master

    //--[Relive Menu]
    bool mIsReliveMode;
    int mReliveGridOpacityTimer;
    int mReliveGridCurrent;
    SugarLinkedList *mReliveList; //RelivePackage *, master
    SugarLinkedList *mReliveGrid; //GridPackage *, master

    //--[Warp Menu]
    bool mIsWarpMode;
    float mWarpZoomFactor;
    int mWarpGridCurrent;
    int mWarpGridOpacityTimer;
    int mWarpGridSelectedRegion;
    int mWarpSelectedRoom;
    int mWarpRegionOpacityTimer;
    int mWarpReposTimer;
    float mWarpReposXStart;
    float mWarpReposYStart;
    float mWarpReposXFinish;
    float mWarpReposYFinish;
    SugarLinkedList *mWarpList; //WarpPackage *, master
    SugarLinkedList *mWarpGrid; //GridPackage *, master
    SugarLinkedList *mWarpGridImg; //SugarBitmap *, ref, parallel with mWarpGrid
    SugarLinkedList *mWarpRegionImages; //SugarBitmap *, ref

    //--[Campfire Password Entry]
    //--System
    bool mIsPasswordEntry;
    int mPasswordOpacity;

    //--Images
    struct
    {
        bool mIsReady;
        struct
        {
            //--General
            SugarBitmap *rBorderCard;
            SugarFont *rMenuFont;
        }Data;
        struct
        {
            //--Fonts
            SugarFont *rHeadingFont;
            SugarFont *rMainlineFont;
            SugarFont *rSmallFont;

            //--Images
            SugarBitmap *rBtnBase;
            SugarBitmap *rComboBar;
            SugarBitmap *rFooter;
            SugarBitmap *rHeader;
            SugarBitmap *rHealthBarBack;
            SugarBitmap *rHealthBarFill;
            SugarBitmap *rHealthBarFrame;
            SugarBitmap *rInventoryBacks;
            SugarBitmap *rInventoryBanners;
            SugarBitmap *rNameBar;
            SugarBitmap *rPlatinaBanner;
            SugarBitmap *rPlatinaText;
            SugarBitmap *rPortraitBack;
            SugarBitmap *rPortraitFront;
            SugarBitmap *rPortraitMask;

            //--Adamantite
            SugarBitmap *rAdamantiteImg[CRAFT_ADAMANTITE_TOTAL];
        }BaseMenu;
        struct
        {
            //--Fonts
            SugarFont *rHeadingFont;
            SugarFont *rMainlineFont;

            //--Images
            SugarBitmap *rBaseBot;
            SugarBitmap *rBaseMid;
            SugarBitmap *rBaseTop;
            SugarBitmap *rButton;
            SugarBitmap *rDescriptionBox;
            SugarBitmap *rHeader;
            SugarBitmap *rComparison;

            //--Map Pin. Used by Warp menu.
            SugarBitmap *rMapPin;
            SugarBitmap *rMapPinSelected;

            //--Icons
            SugarBitmap *rIconFrame;
            SugarBitmap *rIconFrameLarge;
            SugarBitmap *rIcons[AM_SAVE_TOTAL];
        }CampfireMenu;
        struct
        {
            //--Fonts
            SugarFont *rHeadingFont;
            SugarFont *rVSmallFont;

            //--Images
            SugarBitmap *rFrameLower;
            SugarBitmap *rFrameLowerActive;
            SugarBitmap *rFrameUpper;
            SugarBitmap *rHeader;
            SugarBitmap *rSelectorLower;
            SugarBitmap *rSelectorUpper;
        }CostumeUI;
        struct
        {
            //--Fonts
            SugarFont *rHeadingFont;
            SugarFont *rMainlineFont;
            SugarFont *rVerySmallFont;

            //--Images
            SugarBitmap *rBackButton;
            SugarBitmap *rBarFill;
            SugarBitmap *rBarFrame;
            SugarBitmap *rBarHeader;
            SugarBitmap *rHealAllBtn;
            SugarBitmap *rMenuBack;
            SugarBitmap *rMenuHeaderA;
            SugarBitmap *rMenuHeaderB;
            SugarBitmap *rMenuInset;
            SugarBitmap *rOptionsDeactivated;
            SugarBitmap *rPortraitBtn;
            SugarBitmap *rPortraitMask;
        }DoctorUI;
        struct
        {
            //--Fonts
            SugarFont *rHeadingFont;
            SugarFont *rMainlineFont;

            //--Images
            SugarBitmap *rFrames;
            SugarBitmap *rNameplate;
            SugarBitmap *rPortraitMaskA;
            SugarBitmap *rPortraitMaskB;
            SugarBitmap *rPortraitMaskC;
            SugarBitmap *rUnequipIcon;
            SugarBitmap *rSocketFrameA;
            SugarBitmap *rSocketFrameB;
            SugarBitmap *rSocketFrameC;
            SugarBitmap *rGemEmpty22px;
            SugarBitmap *rFrameScroll;
            SugarBitmap *rSocketScroll;
            SugarBitmap *rScrollbarFront;
        }EquipmentUI;
        struct
        {
            //--Fonts
            SugarFont *rHeadingFont;
            SugarFont *rMainlineFont;

            //--Images
            SugarBitmap *rArrowLft;
            SugarBitmap *rArrowRgt;
            SugarBitmap *rHeader;
            SugarBitmap *rNewFileButton;
            SugarBitmap *rGridBackingCh0;
        }FileSelectUI;
        struct
        {
            //--Fonts
            SugarFont *rHeadingFont;
            SugarFont *rMainlineFont;

            //--Images
            SugarBitmap *rBanner;
            SugarBitmap *rBase;
            SugarBitmap *rButtonBack;
            SugarBitmap *rGemSelectBack;
            SugarBitmap *rGemSelectFrame;
            SugarBitmap *rInfoBox;
            SugarBitmap *rPlatinaBanner;
        }GemcutterUI;
        struct
        {
            //--Fonts
            SugarFont *rHeadingFont;
            SugarFont *rMainlineFont;

            //--Images
            SugarBitmap *rFrames;
            SugarBitmap *rScrollbarBack;
            SugarBitmap *rScrollbarFront;

            //--Header Images
            SugarBitmap *rImageHeaders[AM_INV_HEADER_TOTAL];
        }InventoryUI;
        struct
        {
            //--Fonts
            SugarFont *rHeadingFont;
            SugarFont *rMainlineFont;

            //--Images
            SugarBitmap *rBars;
            SugarBitmap *rConfirmButtons;
            SugarBitmap *rHeader;
            SugarBitmap *rMenuBack;
            SugarBitmap *rMenuInsert;
            SugarBitmap *rSliderButtons;
            SugarBitmap *rSliders;
            SugarBitmap *rTextBoxes;
        }OptionsUI;
        struct
        {
            //--Fonts
            SugarFont *rHeadingFont;
            SugarFont *rMainlineFont;
            SugarFont *rVerySmallFont;

            //--Images
            SugarBitmap *rAbilityCursor;
            SugarBitmap *rAbilityInspector;
            SugarBitmap *rBackgroundFill;
            SugarBitmap *rBannerTop;
            SugarBitmap *rBtnBack;
            SugarBitmap *rCharInfo;
            SugarBitmap *rCharOverlayMask;
            SugarBitmap *rCharOverlayMaskLft;
            SugarBitmap *rCharOverlayMaskRgt;
            SugarBitmap *rEXPBarFill;
            SugarBitmap *rEXPBarFrame;
            SugarBitmap *rHealthBarFill;
            SugarBitmap *rHealthBarFrame;
            SugarBitmap *rHealthBarUnder;
            SugarBitmap *rInventory;
            SugarBitmap *rNavButtons;
        }StatusUI;
        struct
        {
            //--Fonts
            SugarFont *rHeadingFont;
            SugarFont *rMainlineFont;

            //--Images
            SugarBitmap *rConfirmOverlay;
            SugarBitmap *rFramesBuy;
            SugarBitmap *rFramesSell;
            SugarBitmap *rFramesPurify;
            SugarBitmap *rFramesGemcutter;
            SugarBitmap *rGemEmpty22px;
            SugarBitmap *rCompareCursor;
            SugarBitmap *rScrollbarBack;
            SugarBitmap *rScrollbarFront;
        }VendorUI;
    }Images;

    protected:

    public:
    //--System
    AdventureMenu();
    virtual ~AdventureMenu();

    //--Public Variables
    static bool xIsSkillbookMenu;
    static char *xGemcutterResolveScript;
    static SugarLinkedList *xGemAdjectiveList;
    static char *xFormResolveScript;
    static char *xFormPartyResolveScript;
    static char *xChatResolveScript;
    static char *xRestResolveScript;
    static char *xReliveResolveScript;
    static char *xCostumeResolveScript;
    static char *xWarpResolveScript;
    static char *xWarpExecuteScript;
    static int xCatalystTotal;
    static int xLastSavefileSortingType;
    static bool xHandledPassword;

    //--Text Display Remaps
    static int xTextImgRemapsTotal;
    static char **xTextImgRemap; //Name that appears in the text
    static char **xTextImgPath;  //Path that the name maps to

    //--Property Queries
    bool StartedHidingThisTick();
    bool IsVisible();
    bool NeedsToBlockControls();
    bool NeedsToRender();

    //--Manipulators
    void Show();
    void Hide();
    void SetSaveMode(bool pFlag);
    void SetBenchMode(bool pFlag);

    //--Core Methods
    //--Chat Menu
    void ActivateChatMenu();
    void DeactivateChatMenu();
    void AddChatEntity(const char *pName, const char *pPath, const char *pImgPath);
    void UpdateChatMenu();
    void RenderChatMenu();

    //--Costumes
    void ActivateCostumesMenu();
    void DeactivateCostumesMenu();
    void AddCostumeHeading(const char *pInternalName, const char *pCharName, const char *pFormName, const char *pImagePath);
    void AddCostumeEntry(const char *pHeadingName, const char *pCostumeName, const char *pImagePath, const char *pScriptPath);
    void SetActiveCostumeEntry(const char *pHeadingName, const char *pCostumeName);
    void ActivateCostumeSubMenu(int pIndex);
    void UpdateCostumeMenu();
    void UpdateCostumeMenuSub();
    void RenderCostumeMenu();
    void RenderCostumeMenuSub();

    //--Equipment Menu
    void SetToEquipMenu();
    void BuildSlotList(int pCharacterSlot, const char *pSlotType);
    void UpdateEquipMenu();
    void RenderEquipMenu();
    void RenderEquipmentDescription(AdventureItem *pDescriptionItem);
    void RenderEquipmentComparison(AdventureItem *pDescriptionItem, AdventureItem *pComparisonItem);
    void RenderGemComparison(AdventureItem *pDescriptionGem, AdventureItem *pComparisonGem);

    //--Equipment Socketing
    void ActivateSocketMode();
    void UpdateSocketMode();
    void RenderSocketMode();

    //--File Selection
    void ScanExistingFiles();
    int GetArraySlotFromLookups(int pLookup);
    void SortFilesBySlot();
    void SortFilesBySlotRev();
    void SortFilesByDate();
    void SortFilesByDateRev();
    void UpdateFileSelection();
    void RenderFileSelection();
    void RenderFileBlock(int pIndex, float pLft, float pTop, bool pIsHighlighted, float pOpacityPct);

    //--Forms Menu
    void ActivateFormsMenu();
    void ActivateFormsSubMenu(int pPartyIndex);
    void DeactivateFormsMenu();
    int GetIconIndex(const char *pName);
    void RegisterFormPartyMember(const char *pCharacterName, const char *pImgPath);
    void RegisterBaseStats(const char *pString);
    void RegisterForm(const char *pFormName, const char *pFormPath, const char *pDescription, const char *pImgPath);
    void SetInfoFromString(TFComparisonPack *pPackage, const char *pString);
    void UpdateFormsMenu();
    void UpdateFormsSubMenu();
    void RenderFormsMenu();
    void RenderFormsSubMenu();

    //--Healing Menu
    void SetToHealMode();
    void UpdateHealMode();
    void RenderHealMode();

    //--Items Menu
    void SetToItemsMenu();
    void UpdateItemsMenu();
    void RenderItemsMenu();
    void RenderItemDescription(AdventureItem *pDescriptionItem);

    //--Gemcutter Mode
    void InitializeGemcutterMode();
    static void RegisterGemAdjective(const char *pAdjectiveName, SugarLinkedList *pUpgradeList);
    void UpdateGemcutterMode();
    void RenderGemcutterMode(bool pIsActiveMode, float pColorMix);

    //--Status Menu
    void UpdateStatusMenu();
    void RenderStatusMenu();
    void RenderAbilityInspector(AdvCombatEntity *pCharacter);

    //--Main Menu
    void SetToMainMenu();
    void UpdateMainMenu();
    void RenderMainMenu();

    //--Map Mode
    void InitializeMapMode();
    void SetMapInfo(const char *pDLPath, const char *pDLPathOverlay, int pPlayerX, int pPlayerY);
    void RepositionMap(int pX, int pY);
    void AddAdvancedLayer(const char *pDLPath, int pRenderLo, int pRenderHi);
    void SetAdvancedRemaps(float pX, float pY, float pScaleX, float pScaleY);
    void ArchiveAdvancedMap();
    void UnarchiveAdvancedMap();
    void UpdateMapMode();
    void RenderMapMode();

    //--Options Mode
    void SetToOptionsMode();
    void HandleOptionChange(int pCursor, bool pIsLeftPress);
    void UpdateOptionsMode();
    void RenderOptionsMode();

    //--Password Mode
    void SetToPasswordMode();
    void UpdatePasswordMode();
    void RenderPasswordMode();

    //--Quit Menu
    void SetToQuitMenu();
    void UpdateQuitMenu();
    void RenderQuitMenu();

    //--Relive Menu
    void SetToReliveMode();
    void ClearReliveList();
    void RegisterReliveScript(const char *pDisplayName, const char *pScriptPath, const char *pImagePath);
    void UpdateReliveMode();
    void RenderReliveMode();

    //--Campfire Mode
    void InitializeCampfireVariables();
    void BuildCampfirePositions();
    void ExecuteRest();
    void UpdateCampfireMenu();
    void RenderCampfireMenu();
    float RenderCampfireMenuBacking(int pMaxOptions, bool pRenderDescriptionBox);

    //--Shops Mode
    void InitializeShopMode(const char *pShopName, const char *pPath, const char *pTeardownPath);
    void SetSellFlag(bool pFlag);
    void RegisterItem(const char *pName);
    void RegisterItemUnlimited(const char *pName);
    void AssembleDescriptionLines(const char *pDescriptionLine);
    char *AssembleInventoryString();
    void UpdateShopMenu();
    void RenderShopMenu();

    //--Shop Subfile: Buy
    void UpdateShopBuy();
    void RenderShopBuy(bool pIsActiveMode, float pColorMix);
    void RenderShopBuyProperties(AdventureItem *pItem, int pComparisonCharacter, float pColorMix);

    //--Shop Subfile: Sell
    void UpdateShopSell();
    void RenderShopSell(bool pIsActiveMode, float pColorMix);

    //--Skillbooks Mode
    void SetToSkillbookMode();
    void RegisterSkillbookEntry(const char *pDisplayName, const char *pScriptPath);
    void OpenSkillbooksTo(const char *pCharacterName, const char *pScriptPath);
    void UpdateSkillbookMode();
    void RenderSkillbookMode();

    //--Warp Menu
    void SetToWarpMode();
    void RegisterWarpRegionImage(const char *pName, const char *pImagePath);
    void ClearWarpRegionImages();
    void RegisterWarpDestination(const char *pRegionName, const char *pDisplayName, const char *pActualName, const char *pImgName, const char *pOverlayName, float pImgX, float pImgY);
    void ClampWarpFocusPoint(SugarBitmap *pImage, float &sXPos, float &sYPos);
    void UpdateWarpMode();
    void RenderWarpMode();
    void UpdateWarpRegionMode();
    void RenderWarpRegionMode();

    private:
    //--Private Core Methods
    public:
    //--Update
    void Update();

    //--File I/O
    //--Drawing
    void Render();
    void RenderCursor(float pLft, float pTop);
    static void RenderBox(float pLft, float pTop, float pRgt, float pBot);

    //--Pointer Routing
    SugarLinkedList *GetGemAdjectiveList();

    //--Static Functions
    static AdventureMenu *Fetch();

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_AM_GetProperty(lua_State *L);
int Hook_AM_SetProperty(lua_State *L);
int Hook_AM_SetMapInfo(lua_State *L);
int Hook_AM_SetFormProperty(lua_State *L);
int Hook_AM_SetShopProperty(lua_State *L);
int Hook_AM_GetShopInventory(lua_State *L);
int Hook_AM_SetGemcutterProperty(lua_State *L);
