//--Base
#include "AdventureMenu.h"

//--Classes
#include "AdvCombat.h"
#include "AdvCombatEntity.h"
#include "VisualLevel.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarFont.h"
#include "SugarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "EasingFunctions.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "LuaManager.h"

//--[Forward Declarations]
void RenderLineBounded(float pLft, float pTop, float pWidth, SugarFont *pFont, const char *pString);
void RenderTextConditionalColor(SugarFont *pFont, float pX, float pY, int pFlags, float pSize, const char *pText, bool pConditional, StarlightColor pTrueColor, StarlightColor pFalseColor);

//--[System]
void AdventureMenu::ActivateFormsMenu()
{
    //--[Documentation and Setup]
    //--Called from the Save Menu. Wipes forms information. This won't do anything if the static
    //  forms path was never set!
    if(!xFormResolveScript || !xFormPartyResolveScript) return;

    //--System
    mIsFormsMode = true;
    mFormsPartyMember = -1;

    //--Character Selection.
    mFormsGridCurrent = 0;
    mFormsPartyList->ClearList();
    mFormsPartyGrid->ClearList();
    mFormsPartyGridImg->ClearList();

    //--Form Selection.
    mFormSubGridCurrent = 0;
    mFormSubGrid->ClearList();
    mFormSubPaths->ClearList();
    mFormSubDescriptions->ClearList();
    mFormSubGridImg->ClearList();
    mFormSubComparisons->ClearList();

    //--Now call the party resolver script.
    LuaManager::Fetch()->ExecuteLuaFile(xFormPartyResolveScript);

    //--[Error Check]
    //--No forms resolved.
    if(mFormsPartyList->GetListSize() < 1)
    {
        AudioManager::Fetch()->PlaySound("Menu|Failed");
        mIsFormsMode = false;
    }

    //--[Positioning and Controls]
    //--Figure out where the grid entries go dynamically. A set of priorities is built and the grid is filled
    //  according to those priorities. The priority is set to require the smallest number of keypresses to
    //  access the largest number of entries. The grid is 3 high by default, and has padding.
    int cXSize = 7;
    int cYSize = 1;
    int cXMiddle = 3;
    int cYMiddle = 1;
    int **tGridData = GridHandler::BuildGrid(cXSize, cYSize, cXMiddle, cYMiddle, mFormsPartyList->GetListSize());

    //--Call the subroutine to build the control list for us.
    GridHandler::BuildGridList(mFormsPartyGrid, tGridData, cXSize, cYSize, mFormsPartyList->GetListSize(), AM_GRID_CENT_X, AM_GRID_CENT_Y, AM_GRID_SPAC_X, AM_GRID_SPAC_Y);

    //--Clean up the grid data. This argument expects the original grid size.
    GridHandler::DeallocateGrid(tGridData, cXSize);
}
void AdventureMenu::ActivateFormsSubMenu(int pPartyIndex)
{
    //--When a party member is selected, builds a list of forms for that member. Can fail. The party index refers to
    //  the name of the character in the follower group, which may or may not be a party member in the combat listing.
    void *rCheckPtr = mFormsPartyList->GetElementBySlot(pPartyIndex);
    if(!rCheckPtr)
    {
        AudioManager::Fetch()->PlaySound("Menu|Failed");
        return;
    }

    //--Get the name associated with the slot.
    const char *rName = mFormsPartyList->GetNameOfElementBySlot(pPartyIndex);

    //--Clear lists off.
    mFormSubGrid->ClearList();
    mFormSubPaths->ClearList();
    mFormSubDescriptions->ClearList();
    mFormSubGridImg->ClearList();
    mFormSubComparisons->ClearList();

    //--Run the script. This will populate the forms that character can change to.
    LuaManager::Fetch()->ExecuteLuaFile(xFormResolveScript, 1, "S", rName);

    //--If the character cannot change to any forms, fail here.
    if(mFormSubPaths->GetListSize() < 1)
    {
        AudioManager::Fetch()->PlaySound("Menu|Failed");
        return;
    }

    //--If we got this far, at least one form is available to transform to. Set this as the active character.
    mFormsPartyMember = pPartyIndex;

    //--Construct a TF grid.
    int cXSize = 7;
    int cYSize = 3;
    int cXMiddle = 3;
    int cYMiddle = 1;
    int **tGridData = GridHandler::BuildGrid(cXSize, cYSize, cXMiddle, cYMiddle, mFormSubPaths->GetListSize());
    GridHandler::BuildGridList(mFormSubGrid, tGridData, cXSize, cYSize, mFormSubPaths->GetListSize(), AM_GRID_CENT_X, AM_GRID_CENT_Y, AM_GRID_SPAC_X, AM_GRID_SPAC_Y);
    GridHandler::DeallocateGrid(tGridData, cXSize);

    //--SFX.
    AudioManager::Fetch()->PlaySound("Menu|Select");

    //--For each description chunk, build a comparison package.
    char *rDescription = (char *)mFormSubDescriptions->PushIterator();
    while(rDescription)
    {
        //--Construct a comparison package.
        TFComparisonPack *nPackage = (TFComparisonPack *)starmemoryalloc(sizeof(TFComparisonPack));
        nPackage->Initialize();

        //--Run the parser on the description.
        SetInfoFromString(nPackage, rDescription);

        //--Register the pack.
        mFormSubComparisons->AddElementAsTail("X", nPackage, &FreeThis);

        //--Next.
        rDescription = (char *)mFormSubDescriptions->AutoIterate();
    }
}
void AdventureMenu::DeactivateFormsMenu()
{
    //--Clears the forms list and returns to the main menu. Note that activating a form does not trigger this.
    mIsFormsMode = false;
}

//--[Property Queries]
int AdventureMenu::GetIconIndex(const char *pName)
{
    for(int i = 0; i < AM_INV_HEADER_TOTAL; i ++)
    {
        if(!strncasecmp(xUpgradeIconRemaps[i], pName, AM_INV_NAME_REMAP_MAXLEN-1))
        {
            return i;
        }
    }
    return -1;
}

//--[Manipulators]
void AdventureMenu::RegisterFormPartyMember(const char *pCharacterName, const char *pImgPath)
{
    //--Adds a character to the listing. Some characters may be listed but unable to transform, others may not be listed
    //  because their transformation is based on someone else's.
    if(!pCharacterName || !pImgPath) return;

    //--Dummy value. Never actually used, don't worry about it going out of scope.
    int tDummyValue = 100;
    mFormsPartyList->AddElementAsTail(pCharacterName, &tDummyValue);
    mFormsPartyGridImg->AddElementAsTail("X", DataLibrary::Fetch()->GetEntry(pImgPath));
}
void AdventureMenu::RegisterBaseStats(const char *pString)
{
    //--Sets the base stats that the other forms compare against.
    if(!pString) return;
    SetInfoFromString(&mBasePackage, pString);
}
void AdventureMenu::RegisterForm(const char *pFormName, const char *pFormPath, const char *pDescription, const char *pImgPath)
{
    //--Adds a form to the listing. Called from the Lua.
    if(!pFormName || !pFormPath || !pDescription || !pImgPath) return;
    mFormSubPaths->AddElementAsTail(pFormName, InitializeString(pFormPath), &FreeThis);
    mFormSubDescriptions->AddElementAsTail(pFormName, InitializeString(pDescription), &FreeThis);
    mFormSubGridImg->AddElementAsTail("X", DataLibrary::Fetch()->GetEntry(pImgPath));
}
void AdventureMenu::SetInfoFromString(TFComparisonPack *pPackage, const char *pString)
{
    //--Given a string of form "ICN:ICN ICN:ICN ...", turns the string into a set of image indexes.
    if(!pPackage || !pString) return;

    //--Parse the description. The icons are "ICN:ICN " formatted into 8 character blocks.
    int i = 0;
    int tLen = (int)strlen(pString);
    while(i < tLen)
    {
        //--First 3-character block is the stat icon.
        int tStatIndex = GetIconIndex(&pString[i+0]);

        //--Second is the icon to display.
        int tDisplayIndex = GetIconIndex(&pString[i+4]);


        //--If either of these come back as -1, stop.
        if(tStatIndex != -1 || tDisplayIndex != -1)
        {
            pPackage->rImages[tStatIndex] = Images.InventoryUI.rImageHeaders[tDisplayIndex];
        }

        //--Move up.
        i = i + 8;
    }
}

//--[Update]
void AdventureMenu::UpdateFormsMenu()
{
    //--[Timers]
    //--If in chat mode, increment this timer. Chat listing must also have been created.
    if(mIsFormsMode)
    {
        if(mFormsPartyMember == -1)
        {
            if(mFormsOpacityTimer < AM_CAMPFIRE_OPACITY_TICKS) mFormsOpacityTimer ++;
            if(mFormSubOpacityTimer > 0) mFormSubOpacityTimer --;
        }
        else
        {
            if(mFormsOpacityTimer > 0) mFormsOpacityTimer --;
            if(mFormSubOpacityTimer < AM_CAMPFIRE_OPACITY_TICKS) mFormSubOpacityTimer ++;
            return;
        }
    }
    //--Decrement and stop if not.
    else
    {
        if(mFormsOpacityTimer > 0) mFormsOpacityTimer --;
        if(mFormSubOpacityTimer > 0) mFormSubOpacityTimer --;
        return;
    }

    //--[Setup]
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Error check:
    if(mFormsGridCurrent < 0 || mFormsGridCurrent >= mFormsPartyGrid->GetListSize()) mFormsGridCurrent = 0;
    GridPackage *rPackage = (GridPackage *)mFormsPartyGrid->GetElementBySlot(mFormsGridCurrent);
    if(!rPackage)
    {
        DeactivateFormsMenu();
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        return;
    }

    //--[Directions]
    //--Directional controls are handled programmatically. If the instruction is -1, it means there is nothing
    //  at that grid position.
    int tGridPrevious = mFormsGridCurrent;
    if(rControlManager->IsFirstPress("Up"))
    {
        int tTargetEntry = rPackage->mControlLookups[GRID_TOP];
        if(tTargetEntry != -1) mFormsGridCurrent = tTargetEntry;
    }
    if(rControlManager->IsFirstPress("Down"))
    {
        int tTargetEntry = rPackage->mControlLookups[GRID_BOT];
        if(tTargetEntry != -1) mFormsGridCurrent = tTargetEntry;
    }
    if(rControlManager->IsFirstPress("Left"))
    {
        int tTargetEntry = rPackage->mControlLookups[GRID_LFT];
        if(tTargetEntry != -1) mFormsGridCurrent = tTargetEntry;
    }
    if(rControlManager->IsFirstPress("Right"))
    {
        int tTargetEntry = rPackage->mControlLookups[GRID_RGT];
        if(tTargetEntry != -1) mFormsGridCurrent = tTargetEntry;
    }

    //--If the grid changed positions, play a sound.
    if(tGridPrevious != mFormsGridCurrent)
    {
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }

    //--Size setting. All not-selected grid entries decrease their timers. The selected entry increases.
    GridPackage *rTimerPackage = (GridPackage *)mFormsPartyGrid->PushIterator();
    while(rTimerPackage)
    {
        //--Increment if selected.
        if(rTimerPackage == rPackage)
        {
            if(rTimerPackage->mTimer < AM_CAMPFIRE_SIZE_TICKS) rTimerPackage->mTimer ++;
        }
        //--Decrement if deselected.
        else
        {
            if(rTimerPackage->mTimer > 0) rTimerPackage->mTimer --;
        }
        rTimerPackage = (GridPackage *)mFormsPartyGrid->AutoIterate();
    }

    //--Activate, talk to the specified character. Subroutine handles SFX.
    if(rControlManager->IsFirstPress("Activate"))
    {
        ActivateFormsSubMenu(mFormsGridCurrent);
        mStartedHidingThisTick = true;
    }
    //--Cancel, previous menu.
    else if(rControlManager->IsFirstPress("Cancel") && !mStartedHidingThisTick)
    {
        DeactivateFormsMenu();
        mStartedHidingThisTick = true;
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }
}
void AdventureMenu::UpdateFormsSubMenu()
{
    //--[Documentation]
    //--The submenu for Forms mode is the menu where the player selects which form the character in question
    //  should transform to. The party member should be -1 if this mode is not active.
    //--UpdateFormsMenu() is always called right before this routine, and will handle the timers for us.
    if(!mIsFormsMode || mFormsPartyMember == -1) return;

    //--[Setup]
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Error check:
    if(mFormSubGridCurrent < 0 || mFormSubGridCurrent >= mFormSubGrid->GetListSize()) mFormSubGridCurrent = 0;
    GridPackage *rPackage = (GridPackage *)mFormSubGrid->GetElementBySlot(mFormSubGridCurrent);
    if(!rPackage)
    {
        DeactivateFormsMenu();
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        return;
    }

    //--[Directions]
    //--Directional controls are handled programmatically. If the instruction is -1, it means there is nothing
    //  at that grid position.
    int tGridPrevious = mFormSubGridCurrent;
    if(rControlManager->IsFirstPress("Up"))
    {
        int tTargetEntry = rPackage->mControlLookups[GRID_TOP];
        if(tTargetEntry != -1) mFormSubGridCurrent = tTargetEntry;
    }
    if(rControlManager->IsFirstPress("Down"))
    {
        int tTargetEntry = rPackage->mControlLookups[GRID_BOT];
        if(tTargetEntry != -1) mFormSubGridCurrent = tTargetEntry;
    }
    if(rControlManager->IsFirstPress("Left"))
    {
        int tTargetEntry = rPackage->mControlLookups[GRID_LFT];
        if(tTargetEntry != -1) mFormSubGridCurrent = tTargetEntry;
    }
    if(rControlManager->IsFirstPress("Right"))
    {
        int tTargetEntry = rPackage->mControlLookups[GRID_RGT];
        if(tTargetEntry != -1) mFormSubGridCurrent = tTargetEntry;
    }

    //--If the grid changed positions, play a sound.
    if(tGridPrevious != mFormSubGridCurrent)
    {
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }

    //--Size setting. All not-selected grid entries decrease their timers. The selected entry increases.
    GridPackage *rTimerPackage = (GridPackage *)mFormSubGrid->PushIterator();
    while(rTimerPackage)
    {
        //--Increment if selected.
        if(rTimerPackage == rPackage)
        {
            if(rTimerPackage->mTimer < AM_CAMPFIRE_SIZE_TICKS) rTimerPackage->mTimer ++;
        }
        //--Decrement if deselected.
        else
        {
            if(rTimerPackage->mTimer > 0) rTimerPackage->mTimer --;
        }
        rTimerPackage = (GridPackage *)mFormSubGrid->AutoIterate();
    }

    //--Activate, talk to the specified character.
    if(rControlManager->IsFirstPress("Activate") && !mStartedHidingThisTick)
    {
        //--Get the script path.
        const char *rScriptPath = (const char *)mFormSubPaths->GetElementBySlot(mFormSubGridCurrent);

        //--Run the script.
        LuaManager::Fetch()->ExecuteLuaFile(rScriptPath);

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|Select");

        //--Hide this menu. Must be called after everything else since it clears the form list.
        DeactivateFormsMenu();
        Hide();
        mFormsPartyMember = -1;
        mStartedHidingThisTick = true;
    }
    //--Cancel, previous menu.
    else if(rControlManager->IsFirstPress("Cancel"))
    {
        mFormsPartyMember = -1;
        mStartedHidingThisTick = true;
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }
}
void AdventureMenu::RenderFormsMenu()
{
    //--[Documentation]
    //--Renders the forms menu. This will always consist of at least the party listing, and may also consist of
    //  a list of forms for that character. Characters who cannot change forms are greyed out but still listed.
    if(!Images.mIsReady) return;

    //--[Setup]
    //--Don't render at all if the opacity is at zero.
    if(mFormsOpacityTimer < 1) return;

    //--Opacity percent. Determines position and fading.
    float tOpacityPct = (float)mFormsOpacityTimer / (float)AM_CAMPFIRE_OPACITY_TICKS;

    //--[Title]
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, tOpacityPct);
    Images.CampfireMenu.rHeader->Draw(427.0f, 45.0f);
    Images.CampfireMenu.rHeadingFont->DrawText(683.0f, 57.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Select a Character");
    StarlightColor::ClearMixer();

    //--[Icons]
    //--Sizes.
    float cIconSize = Images.CampfireMenu.rIconFrame->GetTrueWidth();

    //--Position the objects slide away from.
    float tOpacitySlidePct = EasingFunction::QuadraticOut(mFormsOpacityTimer, AM_CAMPFIRE_OPACITY_TICKS);
    float tCenterX = AM_GRID_CENT_X;
    float tCenterY = AM_GRID_CENT_Y;

    //--Iterate across the list.
    GridPackage *rGridPack = (GridPackage *)mFormsPartyGrid->PushIterator();
    SugarBitmap *rImage    = (SugarBitmap *)mFormsPartyGridImg->PushIterator();
    while(rGridPack && rImage)
    {
        //--Determine the rendering size. This also affects color blending.
        float cBoostPct = EasingFunction::QuadraticInOut(rGridPack->mTimer, AM_CAMPFIRE_SIZE_TICKS);
        float cScale = 3.0f + (cBoostPct * AM_CAMPFIRE_SIZE_BOOST);
        float cScaleInv = 1.0f / cScale;

        //--Figure out the render position.
        float tXPos = rGridPack->mXPos;
        float tYPos = rGridPack->mYPos;

        //--The actual render position is based on the opacity percent as the object slides out of the middle.
        tXPos = tCenterX + ((tXPos - tCenterX) * tOpacitySlidePct);
        tYPos = tCenterY + ((tYPos - tCenterY) * tOpacitySlidePct);

        //--Move the render position off by the scale of the object.
        tXPos = tXPos - (cIconSize * cScale * 0.50f);
        tYPos = tYPos - (cIconSize * cScale * 0.50f);

        //--Color blending.
        float cColVal = 0.75f + (0.25f * cBoostPct);
        StarlightColor::SetMixer(cColVal, cColVal, cColVal, tOpacityPct);

        //--Render.
        glTranslatef(tXPos, tYPos, 0.0f);
        glScalef(cScale, cScale, 1.0f);
        Images.CampfireMenu.rIconFrame->Draw();
        rImage->Draw(0, -3);
        glScalef(cScaleInv, cScaleInv, 1.0f);
        glTranslatef(-tXPos, -tYPos, 0.0f);

        //--Next.
        rGridPack = (GridPackage *)mFormsPartyGrid->AutoIterate();
        rImage    = (SugarBitmap *)mFormsPartyGridImg->AutoIterate();
    }

    //--Iterate across the list again, this time just rendering the names.
    rGridPack = (GridPackage *)mFormsPartyGrid->PushIterator();
    void *rListingPtr = mFormsPartyList->PushIterator();
    while(rGridPack && rListingPtr)
    {
        //--Determine the rendering size. This also affects color blending.
        float cBoostPct = EasingFunction::QuadraticInOut(rGridPack->mTimer, AM_CAMPFIRE_SIZE_TICKS);
        float cScale = 3.0f + (cBoostPct * AM_CAMPFIRE_SIZE_BOOST);

        //--Figure out the render position.
        float tXPos = rGridPack->mXPos;
        float tYPos = rGridPack->mYPos;

        //--The actual render position is based on the opacity percent as the object slides out of the middle.
        tXPos = tCenterX + ((tXPos - tCenterX) * tOpacitySlidePct);
        tYPos = tCenterY + ((tYPos - tCenterY) * tOpacitySlidePct);
        float tTextX = tXPos;

        //--Move the render position off by the scale of the object.
        tXPos = tXPos - (cIconSize * cScale * 0.50f);
        tYPos = tYPos - (cIconSize * cScale * 0.50f);

        //--Color blending.
        float cColVal = 0.75f + (0.25f * cBoostPct);
        StarlightColor::SetMixer(cColVal, cColVal, cColVal, tOpacityPct);

        //--Render.
        float cTxtScale = 1.0f + (cBoostPct * (AM_CAMPFIRE_SIZE_BOOST * 0.33f));
        Images.CampfireMenu.rMainlineFont->DrawText(tTextX, tYPos - 15.0f, SUGARFONT_AUTOCENTER_X, cTxtScale, mFormsPartyList->GetIteratorName());

        //--Next.
        rGridPack = (GridPackage *)mFormsPartyGrid->AutoIterate();
        rListingPtr = mFormsPartyList->AutoIterate();
    }
}
void AdventureMenu::RenderFormsSubMenu()
{
    //--[Documentation]
    //--Renders the transformation selection for a given character. Appears when the player is selecting which form to switch to.
    if(!Images.mIsReady) return;

    //--[Setup]
    //--Don't render at all if the opacity is at zero.
    if(mFormSubOpacityTimer < 1) return;

    //--Opacity percent. Determines position and fading.
    float tOpacityPct = (float)mFormSubOpacityTimer / (float)AM_CAMPFIRE_OPACITY_TICKS;

    //--[Title]
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, tOpacityPct);
    Images.CampfireMenu.rHeader->Draw(427.0f, 45.0f);
    Images.CampfireMenu.rHeadingFont->DrawText(683.0f, 57.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Select a Form");
    StarlightColor::ClearMixer();

    //--[Icons]
    //--Sizes.
    float cIconSize = Images.CampfireMenu.rIconFrame->GetTrueWidth();

    //--Position the objects slide away from.
    float tOpacitySlidePct = EasingFunction::QuadraticOut(mFormSubOpacityTimer, AM_CAMPFIRE_OPACITY_TICKS);
    float tCenterX = AM_GRID_CENT_X;
    float tCenterY = AM_GRID_CENT_Y;

    //--Iterate across the list.
    GridPackage *rGridPack = (GridPackage *)mFormSubGrid->PushIterator();
    SugarBitmap *rImage    = (SugarBitmap *)mFormSubGridImg->PushIterator();
    while(rGridPack && rImage)
    {
        //--Determine the rendering size. This also affects color blending.
        float cBoostPct = EasingFunction::QuadraticInOut(rGridPack->mTimer, AM_CAMPFIRE_SIZE_TICKS);
        float cScale = 3.0f + (cBoostPct * AM_CAMPFIRE_SIZE_BOOST);
        float cScaleInv = 1.0f / cScale;

        //--Figure out the render position.
        float tXPos = rGridPack->mXPos;
        float tYPos = rGridPack->mYPos;

        //--The actual render position is based on the opacity percent as the object slides out of the middle.
        tXPos = tCenterX + ((tXPos - tCenterX) * tOpacitySlidePct);
        tYPos = tCenterY + ((tYPos - tCenterY) * tOpacitySlidePct);

        //--Move the render position off by the scale of the object.
        tXPos = tXPos - (cIconSize * cScale * 0.50f);
        tYPos = tYPos - (cIconSize * cScale * 0.50f);

        //--Color blending.
        float cColVal = 0.75f + (0.25f * cBoostPct);
        StarlightColor::SetMixer(cColVal, cColVal, cColVal, tOpacityPct);

        //--Render.
        glTranslatef(tXPos, tYPos, 0.0f);
        glScalef(cScale, cScale, 1.0f);
        Images.CampfireMenu.rIconFrame->Draw();
        rImage->Draw(0, -3);
        glScalef(cScaleInv, cScaleInv, 1.0f);
        glTranslatef(-tXPos, -tYPos, 0.0f);

        //--Next.
        rGridPack = (GridPackage *)mFormSubGrid->AutoIterate();
        rImage    = (SugarBitmap *)mFormSubGridImg->AutoIterate();
    }

    //--Iterate across the list again, this time just rendering the names.
    rGridPack = (GridPackage *)mFormSubGrid->PushIterator();
    void *rListingPtr = mFormSubPaths->PushIterator();
    while(rGridPack && rListingPtr)
    {
        //--Determine the rendering size. This also affects color blending.
        float cBoostPct = EasingFunction::QuadraticInOut(rGridPack->mTimer, AM_CAMPFIRE_SIZE_TICKS);
        float cScale = 3.0f + (cBoostPct * AM_CAMPFIRE_SIZE_BOOST);

        //--Figure out the render position.
        float tXPos = rGridPack->mXPos;
        float tYPos = rGridPack->mYPos;

        //--The actual render position is based on the opacity percent as the object slides out of the middle.
        tXPos = tCenterX + ((tXPos - tCenterX) * tOpacitySlidePct);
        tYPos = tCenterY + ((tYPos - tCenterY) * tOpacitySlidePct);
        float tTextX = tXPos;

        //--Move the render position off by the scale of the object.
        tXPos = tXPos - (cIconSize * cScale * 0.50f);
        tYPos = tYPos - (cIconSize * cScale * 0.50f);

        //--Color blending.
        float cColVal = 0.75f + (0.25f * cBoostPct);
        StarlightColor::SetMixer(cColVal, cColVal, cColVal, tOpacityPct);

        //--Render.
        float cTxtScale = 1.0f + (cBoostPct * (AM_CAMPFIRE_SIZE_BOOST * 0.33f));
        Images.CampfireMenu.rMainlineFont->DrawText(tTextX, tYPos - 15.0f, SUGARFONT_AUTOCENTER_X, cTxtScale, mFormSubPaths->GetIteratorName());

        //--Next.
        rGridPack = (GridPackage *)mFormSubGrid->AutoIterate();
        rListingPtr = mFormSubPaths->AutoIterate();
    }

    //--[Comparison Handler]
    //--Backing.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, tOpacityPct);
    float cOffset = ((1.0f - tOpacitySlidePct) * 300.0f);
    float cPosX = 0.0f + cOffset;
    Images.CampfireMenu.rComparison->Draw(cPosX, 0.0f);

    //--Get the comparison package currently in use.
    TFComparisonPack *rComparisonPack = (TFComparisonPack *)mFormSubComparisons->GetElementBySlot(mFormSubGridCurrent);
    if(!rComparisonPack) return;

    //--Positions.
    float tLXPos = 1188.0f + cOffset;
    float tBXPos = 1208.0f + cOffset;
    float tRXPos = 1220.0f + cOffset;
    float tCXPos = 1234.0f + cOffset;
    float tYSpc =   21.0f;

    //--Render the icons.
    Images.CampfireMenu.rHeadingFont->DrawText(1228.0f + cOffset, 100.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Stats");
    for(int i = 0; i < AM_COMPARE_STATS; i ++)
    {
        //--Check slot.
        int tSlot = mCompareStatIndices[i];
        if(tSlot < 0 || tSlot >= AM_INV_HEADER_TOTAL) continue;

        //--Compute position and render.
        float tYPos =  138.0f;
        Images.InventoryUI.rImageHeaders[tSlot]->Draw(tLXPos, tYPos + (tYSpc * i));

        //--If both packages use the "neutral" value, don't render anything.
        if(!mBasePackage.rImages[tSlot] && !rComparisonPack->rImages[tSlot]) continue;

        //--Render the icon for the base statistics.
        if(mBasePackage.rImages[tSlot])
        {
            mBasePackage.rImages[tSlot]->Draw(tBXPos, tYPos + (tYSpc * i));
        }
        //--Render the "neutral" indicator.
        else
        {
            Images.InventoryUI.rImageHeaders[AM_INV_HEADER_NEUTRAL]->Draw(tBXPos, tYPos + (tYSpc * i));
        }

        //--Render the "right-arrow" indicator.
        Images.InventoryUI.rImageHeaders[AM_INV_HEADER_RARROW]->Draw(tRXPos, tYPos + (tYSpc * i));

        //--Render the icon for the comparison statistic.
        if(rComparisonPack->rImages[tSlot])
        {
            rComparisonPack->rImages[tSlot]->Draw(tCXPos, tYPos + (tYSpc * i));
        }
        //--Render the "neutral" indicator.
        else
        {
            Images.InventoryUI.rImageHeaders[AM_INV_HEADER_NEUTRAL]->Draw(tCXPos, tYPos + (tYSpc * i));
        }
    }
    Images.CampfireMenu.rHeadingFont->DrawText(1228.0f + cOffset, 291.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Resistances");
    for(int i = 0; i < AM_COMPARE_RESISTANCES; i ++)
    {
        //--Check slot.
        int tSlot = mCompareResistanceIndices[i];
        if(tSlot < 0 || tSlot >= AM_INV_HEADER_TOTAL) continue;

        //--Compute position and render.
        float tYPos =  329.0f;
        Images.InventoryUI.rImageHeaders[tSlot]->Draw(tLXPos, tYPos + (tYSpc * i));

        //--If both packages use the "neutral" value, don't render anything.
        if(!mBasePackage.rImages[tSlot] && !rComparisonPack->rImages[tSlot]) continue;

        //--Render the icon for the base resistances.
        if(mBasePackage.rImages[tSlot])
        {
            mBasePackage.rImages[tSlot]->Draw(tBXPos, tYPos + (tYSpc * i));
        }
        //--Render the "neutral" indicator.
        else
        {
            Images.InventoryUI.rImageHeaders[AM_INV_HEADER_NEUTRAL]->Draw(tBXPos, tYPos + (tYSpc * i));
        }

        //--Render the "right-arrow" indicator.
        Images.InventoryUI.rImageHeaders[AM_INV_HEADER_RARROW]->Draw(tRXPos, tYPos + (tYSpc * i));

        //--Render the icon for the comparison statistic.
        if(rComparisonPack->rImages[tSlot])
        {
            rComparisonPack->rImages[tSlot]->Draw(tCXPos, tYPos + (tYSpc * i));
        }
        //--Render the "neutral" indicator.
        else
        {
            Images.InventoryUI.rImageHeaders[AM_INV_HEADER_NEUTRAL]->Draw(tCXPos, tYPos + (tYSpc * i));
        }
    }
}
