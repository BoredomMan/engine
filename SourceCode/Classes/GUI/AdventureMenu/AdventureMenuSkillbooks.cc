//--Base
#include "AdventureMenu.h"

//--Classes
#include "AdvCombat.h"
#include "AdvCombatEntity.h"
#include "AdventureLevel.h"
#include "TilemapActor.h"
#include "VisualLevel.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarFont.h"
#include "SugarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"

//--Libraries
//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "LuaManager.h"

//--[Forward Declarations]
void RenderLineBounded(float pLft, float pTop, float pWidth, SugarFont *pFont, const char *pString);
void RenderTextConditionalColor(SugarFont *pFont, float pX, float pY, int pFlags, float pSize, const char *pText, bool pConditional, StarlightColor pTrueColor, StarlightColor pFalseColor);

//--[System]
void AdventureMenu::SetToSkillbookMode()
{
}

//--[Manipulators]
void AdventureMenu::RegisterSkillbookEntry(const char *pDisplayName, const char *pScriptPath)
{
}

//--[Core Methods]
void AdventureMenu::OpenSkillbooksTo(const char *pCharacterName, const char *pScriptPath)
{
}

//--[Update]
void AdventureMenu::UpdateSkillbookMode()
{
}

//--[Drawing]
void AdventureMenu::RenderSkillbookMode()
{
}
