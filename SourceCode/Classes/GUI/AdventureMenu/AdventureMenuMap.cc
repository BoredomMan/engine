//--Base
#include "AdventureMenu.h"

//--Classes
#include "AdvCombat.h"
#include "AdventureLevel.h"
#include "TilemapActor.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarFont.h"
#include "SugarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "EasingFunctions.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "DisplayManager.h"

//--[Local Definitions]
//--Timers.
#define FADE_TICKS_OUT 10
#define FADE_TICKS_HOLD 15
#define FADE_TICKS_IN 10

//--Scroll speed.
#define SCROLL_SPEED_BASE 3
#define SCROLL_SPEED_MAX 15
#define SCROLL_SPEED_RAMP 15

#define SCROLL_UP 1.0f
#define SCROLL_DOWN 1.5f

//--[System]
void AdventureMenu::InitializeMapMode()
{
    //--If the map is not set, we can't enter map mode (duh).
    if(!rActiveMap && mMapLayerList->GetListSize() < 1) return;

    //--Set values necessary for map rendering to begin.
    mIsEnteringMapMode = true;
    mMapFadeTimer = 0;
    mCurrentMode = AM_MODE_MAP;

    //--Randomize the map timer.
    mMapTimer = rand() % 60;

    //--Recenter on the player's location.
    RepositionMap(mPlayerMapPositionX, mPlayerMapPositionY);
    mMapScrollTimerX = 0.0f;
    mMapScrollTimerY = 0.0f;

    //--Default zoom is always 1.0f.
    mMapZoom = 0.5f;
    mMapZoomStart = 0.5f;
    mMapZoomDest = 0.5f;
    mMapZoomTimer = AM_MAP_ZOOM_TICKS;

    //--When using map layers, reposition on the player's current position in the room, since it changes.
    if(mMapLayerList->GetListSize() > 0)
    {
        TilemapActor *rActor = AdventureLevel::Fetch()->LocatePlayerActor();//Can't fail because the level owns the menu!
        float tPlayerX = mAdvancedMapRemapX + (rActor->GetWorldX() * mAdvancedMapScaleX);
        float tPlayerY = mAdvancedMapRemapY + (rActor->GetWorldY() * mAdvancedMapScaleY);
        RepositionMap((tPlayerX - (VIRTUAL_CANVAS_X / 2)) * -1, (tPlayerY - (VIRTUAL_CANVAS_Y / 2)) * -1);
    }
}

//--[Manipulators]
void AdventureMenu::SetMapInfo(const char *pDLPath, const char *pDLPathOverlay, int pPlayerX, int pPlayerY)
{
    //--If NULL is passed, clear the map info.
    if(!pDLPath) { rActiveMap = NULL; return; }

    //--If "Null" or any derivation is passed, clear the map info.
    if(!strcasecmp(pDLPath, "Null")) { rActiveMap = NULL; return; }

    //--Otherwise, attempt to set the map.
    rActiveMap = (SugarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath);
    mPlayerMapPositionX = pPlayerX;
    mPlayerMapPositionY = pPlayerY;

    //--Overlay can be NULL.
    if(!pDLPathOverlay || !strcasecmp(pDLPathOverlay, "Null"))
    {
        rActiveMapOverlay = NULL;
    }
    //--Otherwise, set the overlay.
    else
    {
        rActiveMapOverlay = (SugarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPathOverlay);
    }
}

//--[Core Methods]
void AdventureMenu::RepositionMap(int pX, int pY)
{
    //--Repositions the map and clamps the edges so we don't go over. Remember that the offsets are
    //  negatives, so negative X scrolls right.
    mMapFocusPointX = pX;
    mMapFocusPointY = pY;

    //--Can't do any clamps if there's no map!
    SugarBitmap *rUseBitmap = rActiveMap;
    if(!rUseBitmap)
    {
        AdvancedMapPack *rZeroPack = (AdvancedMapPack *)mMapLayerList->GetElementBySlot(0);
        if(rZeroPack) rUseBitmap = rZeroPack->rImage;
    }

    //--The zero pack and the base map didn't exist, so fail here.
    if(!rUseBitmap) return;

    //--Determine the edges of possibility. The focus point can be anywhere within 300 pixels of the map edge. After
    //  that, we clamp to prevent too much offscreening.
    //--Since not all maps occupy the whole space, we use the bitmap's offsets for this.
    float cFactor = 1.0f / mMapZoom;
    int tLftClamp = 0.0f + (VIRTUAL_CANVAS_X * cFactor * 0.50f);
    int tTopClamp = 0.0f + (VIRTUAL_CANVAS_Y * cFactor * 0.50f);
    int tRgtClamp = rUseBitmap->GetTrueWidth() - (VIRTUAL_CANVAS_X * cFactor * 0.50f);
    int tBotClamp = rUseBitmap->GetTrueHeight() - (VIRTUAL_CANVAS_Y * cFactor * 0.50f);
    if(mMapFocusPointX < tLftClamp) mMapFocusPointX = tLftClamp;
    if(mMapFocusPointY < tTopClamp) mMapFocusPointY = tTopClamp;
    if(mMapFocusPointX > tRgtClamp) mMapFocusPointX = tRgtClamp;
    if(mMapFocusPointY > tBotClamp) mMapFocusPointY = tBotClamp;
}
void AdventureMenu::AddAdvancedLayer(const char *pDLPath, int pRenderLo, int pRenderHi)
{
    if(!pDLPath) return;
    SetMemoryData(__FILE__, __LINE__);
    AdvancedMapPack *nPack = (AdvancedMapPack *)starmemoryalloc(sizeof(AdvancedMapPack));
    nPack->rImage = (SugarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath);
    nPack->mRenderChanceLo = pRenderLo;
    nPack->mRenderChanceHi = pRenderHi;
    mMapLayerList->AddElement("X", nPack, &FreeThis);
}
void AdventureMenu::SetAdvancedRemaps(float pX, float pY, float pScaleX, float pScaleY)
{
    mAdvancedMapRemapX = pX;
    mAdvancedMapRemapY = pY;
    mAdvancedMapScaleX = pScaleX;
    mAdvancedMapScaleY = pScaleY;
}
void AdventureMenu::ArchiveAdvancedMap()
{
    //--Places all map properties into a set of archive variables. This is used when the warp menu is
    //  changing map properties and we want to keep the original properties.
    //--The warp menu will call UnarchiveAdvancedMap() when it is done.
    mArchiveAdvancedMapLastRoll = mAdvancedMapLastRoll;
    mArchiveAdvancedMapReroll = mAdvancedMapReroll;
    mArchiveAdvancedMapRemapX = mAdvancedMapRemapX;
    mArchiveAdvancedMapRemapY = mAdvancedMapRemapY;
    mArchiveAdvancedMapScaleX = mAdvancedMapScaleX;
    mArchiveAdvancedMapScaleY = mAdvancedMapScaleY;
    mArchiveMapLayerList = mMapLayerList;

    //--Create a new map layer list.
    mMapLayerList = new SugarLinkedList(true);
}
void AdventureMenu::UnarchiveAdvancedMap()
{
    //--Clear the new list.
    delete mMapLayerList;

    //--Unarchives the advanced map properties when you're done with them.
    mAdvancedMapLastRoll = mArchiveAdvancedMapLastRoll;
    mAdvancedMapReroll = mArchiveAdvancedMapReroll;
    mAdvancedMapRemapX = mArchiveAdvancedMapRemapX;
    mAdvancedMapRemapY = mArchiveAdvancedMapRemapY;
    mAdvancedMapScaleX = mArchiveAdvancedMapScaleX;
    mAdvancedMapScaleY = mArchiveAdvancedMapScaleY;
    mMapLayerList = mArchiveMapLayerList;

    //--Reset the archive values to neutrals.
    mArchiveAdvancedMapLastRoll = 0;
    mArchiveAdvancedMapReroll = 15;
    mArchiveAdvancedMapRemapX = 0.0f;
    mArchiveAdvancedMapRemapY = 0.0f;
    mArchiveAdvancedMapScaleX = 1.0f;
    mArchiveAdvancedMapScaleY = 1.0f;
    mArchiveMapLayerList = NULL;
}

//--[Update]
void AdventureMenu::UpdateMapMode()
{
    //--[General Timers]
    //--These always run.
    mMapTimer ++;

    //--[Advanced Map Reroll]
    if(mAdvancedMapReroll > 0)
    {
        mAdvancedMapReroll --;
    }
    else
    {
        mAdvancedMapReroll = 1 + (rand() % 2);
        mAdvancedMapLastRoll = rand() % 1000;
    }

    //--[Fade Handlers]
    //--Updates the fade timer and stops the control handlers from doing anything.
    if(mMapFadeTimer > -1)
    {
        //--Timer.
        mMapFadeTimer ++;

        //--Ending case.
        if(mMapFadeTimer > FADE_TICKS_OUT + FADE_TICKS_HOLD + FADE_TICKS_IN)
        {
            mMapFadeTimer = -1;
            if(mIsEnteringMapMode == false)
            {
                SetToMainMenu();
                mCurrentCursor = AM_MAIN_MAP;
            }
        }
        RepositionMap(mMapFocusPointX, mMapFocusPointY);

        //--In all cases, stop the update here.
        return;
    }

    //--[Control Handlers]
    //--Setup.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Pressing cancel exits map mode.
    if(rControlManager->IsFirstPress("Cancel"))
    {
        mIsEnteringMapMode = false;
        mMapFadeTimer = 0;
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }

    //--Movement setup.
    int tMapModX = 0;
    int tMapModY = 0;
    bool tPressedAnyKeyX = false;
    bool tPressedAnyKeyY = false;

    //--Horizontal calculation.
    if(mMapScrollTimerX > 0)
    {
        float tBoostX = mMapScrollTimerX / (float)SCROLL_SPEED_RAMP;
        if(tBoostX > 1.0f) tBoostX =  1.0f;
        tMapModX = SCROLL_SPEED_BASE + ((SCROLL_SPEED_MAX - SCROLL_SPEED_BASE) * (tBoostX));
    }
    else if(mMapScrollTimerX < 0)
    {
        float tBoostX = mMapScrollTimerX / (float)SCROLL_SPEED_RAMP * -1.0f;
        if(tBoostX > 1.0f) tBoostX = 1.0f;
        tMapModX = -SCROLL_SPEED_BASE - ((SCROLL_SPEED_MAX - SCROLL_SPEED_BASE) * (tBoostX));
    }

    //--Vertical calculation.
    if(mMapScrollTimerY > 0)
    {
        float tBoostY = mMapScrollTimerY / (float)SCROLL_SPEED_RAMP;
        if(tBoostY > 1.0f) tBoostY =  1.0f;
        tMapModY = SCROLL_SPEED_BASE + ((SCROLL_SPEED_MAX - SCROLL_SPEED_BASE) * (tBoostY));
    }
    else if(mMapScrollTimerY < 0)
    {
        float tBoostY = mMapScrollTimerY / (float)SCROLL_SPEED_RAMP * -1.0f;
        if(tBoostY > 1.0f) tBoostY =  1.0f;
        tMapModY = -SCROLL_SPEED_BASE - ((SCROLL_SPEED_MAX - SCROLL_SPEED_BASE) * (tBoostY));
    }

    //--Cancel the speed if no rendering occurred. This is to deal with FPS hiccups.
    if(!mHasRendered) { tMapModX = 0; tMapModY = 0; }
    mHasRendered = false;

    //--Up.
    if(rControlManager->IsDown("Up"))
    {
        mMapScrollTimerY = mMapScrollTimerY - (SCROLL_UP / mMapZoom);
        tPressedAnyKeyY = true;
    }
    //--Down!
    if(rControlManager->IsDown("Down"))
    {
        mMapScrollTimerY = mMapScrollTimerY + (SCROLL_UP / mMapZoom);
        tPressedAnyKeyY = true;
    }
    //--Left!
    if(rControlManager->IsDown("Left"))
    {
        mMapScrollTimerX = mMapScrollTimerX - (SCROLL_UP / mMapZoom);
        tPressedAnyKeyX = true;
    }
    //--RIGHT!
    if(rControlManager->IsDown("Right"))
    {
        mMapScrollTimerX = mMapScrollTimerX + (SCROLL_UP / mMapZoom);
        tPressedAnyKeyX = true;
    }

    //--Zoom Handler.
    if(rControlManager->IsFirstPress("Activate"))
    {
        //--Common.
        mMapZoomStart = mMapZoom;
        mMapZoomTimer = 0;

        //--If the destination is 1.0f, switch it to 0.5f.
        if(mMapZoomDest == 1.0f)
        {
            mMapZoomDest = 0.5f;
        }
        //--Otherwise, 1.0f.
        else
        {
            mMapZoomDest = 1.0f;
        }
    }
    //--When not pressing the activate key, edit the zoom.
    else if(mMapZoomTimer < AM_MAP_ZOOM_TICKS)
    {
        //--Timer.
        mMapZoomTimer ++;

        //--Percentage completion, use an easing function.
        mMapZoom = mMapZoomStart + ((mMapZoomDest - mMapZoomStart) * EasingFunction::QuadraticInOut(mMapZoomTimer, AM_MAP_ZOOM_TICKS));
    }

    //--Reposition the map.
    if(mMapScrollTimerX < -SCROLL_SPEED_RAMP) mMapScrollTimerX = -SCROLL_SPEED_RAMP;
    if(mMapScrollTimerY < -SCROLL_SPEED_RAMP) mMapScrollTimerY = -SCROLL_SPEED_RAMP;
    if(mMapScrollTimerX >  SCROLL_SPEED_RAMP) mMapScrollTimerX =  SCROLL_SPEED_RAMP;
    if(mMapScrollTimerY >  SCROLL_SPEED_RAMP) mMapScrollTimerY =  SCROLL_SPEED_RAMP;
    RepositionMap(mMapFocusPointX + tMapModX, mMapFocusPointY + tMapModY);

    //--If no key was pressed, reset the scroll speed.
    if(!tPressedAnyKeyX)
    {
        if(mMapScrollTimerX > 0) mMapScrollTimerX = mMapScrollTimerX - SCROLL_DOWN;
        if(mMapScrollTimerX < 0) mMapScrollTimerX = mMapScrollTimerX + SCROLL_DOWN;
        if(mMapScrollTimerX >= -SCROLL_DOWN && mMapScrollTimerX <= SCROLL_DOWN) mMapScrollTimerX = 0.0f;
    }
    if(!tPressedAnyKeyY)
    {
        if(mMapScrollTimerY > 0) mMapScrollTimerY = mMapScrollTimerY - SCROLL_DOWN;
        if(mMapScrollTimerY < 0) mMapScrollTimerY = mMapScrollTimerY + SCROLL_DOWN;
        if(mMapScrollTimerY >= -SCROLL_DOWN && mMapScrollTimerY <= SCROLL_DOWN) mMapScrollTimerY = 0.0f;
    }
}

//--[Drawing]
void AdventureMenu::RenderMapMode()
{
    //--Flags.
    mHasRendered = true;

    //--Fade Handlers:
    if(mMapFadeTimer > -1)
    {
        //--When fading to the map...
        if(mIsEnteringMapMode)
        {
            //--If in the fade-out position.
            if(mMapFadeTimer < FADE_TICKS_OUT)
            {
                //--Render the conventional main menu with an overlay.
                RenderMainMenu();

                //--Overlay.
                SugarBitmap::DrawFullBlack((float)mMapFadeTimer / (float)FADE_TICKS_OUT);

                //--End the rendering cycle.
                return;
            }
            //--Hold: Render a black box across the screen.
            else if(mMapFadeTimer < FADE_TICKS_OUT + FADE_TICKS_HOLD)
            {
                //--Fullblack.
                SugarBitmap::DrawFullBlack(1.0f);

                //--End the rendering cycle.
                return;
            }
        }
        //--When fading from the map...
        else
        {
            //--Out: Do nothing.
            if(mMapFadeTimer < FADE_TICKS_OUT)
            {
            }
            //--Hold: Render full black.
            else if(mMapFadeTimer < FADE_TICKS_OUT + FADE_TICKS_HOLD)
            {
                SugarBitmap::DrawFullBlack(1.0f);
                return;
            }
            //--Render a fade-in sequence over the main menu.
            if(mMapFadeTimer >= FADE_TICKS_OUT + FADE_TICKS_HOLD)
            {
                //--Menu render.
                RenderMainMenu();

                //--Overlay.
                int tUseTimer = mMapFadeTimer - FADE_TICKS_OUT - FADE_TICKS_HOLD;
                SugarBitmap::DrawFullBlack(1.0f - (tUseTimer / (float)FADE_TICKS_IN));
                return;
            }
        }
    }

    //--Advanced maps always have a fullblack that is 100% opaque:
    if(mMapLayerList->GetListSize() > 0)
    {
        SugarBitmap::DrawFullBlack(1.0f);
    }
    //--Otherwise, 50% opacity on exposed parts.
    else
    {
        SugarBitmap::DrawFullBlack(0.5f);
    }

    //--Map Scaling
    if(mMapZoom != 1.0f)
    {
        glTranslatef(VIRTUAL_CANVAS_X * 0.5f, VIRTUAL_CANVAS_Y * 0.5f, 0.0f);
        glScalef(mMapZoom, mMapZoom, 1.0f);
        glTranslatef(VIRTUAL_CANVAS_X * -0.5f, VIRTUAL_CANVAS_Y * -0.5f, 0.0f);
    }

    //--Compute the rendering position of the map.
    float tMapOffsetX = (mMapFocusPointX - (VIRTUAL_CANVAS_X * 0.5f)) * -1.0f;
    float tMapOffsetY = (mMapFocusPointY - (VIRTUAL_CANVAS_Y * 0.5f)) * -1.0f;

    //--Advanced map renderer.
    if(mMapLayerList->GetListSize() > 0)
    {
        //--Iterate.
        AdvancedMapPack *rPack = (AdvancedMapPack *)mMapLayerList->PushIterator();
        while(rPack)
        {
            //--Render if the roll is within range.
            if(mAdvancedMapLastRoll >= rPack->mRenderChanceLo && mAdvancedMapLastRoll <= rPack->mRenderChanceHi && rPack->rImage)
            {
                rPack->rImage->Draw(tMapOffsetX, tMapOffsetY);
            }

            //--Next.
            rPack = (AdvancedMapPack *)mMapLayerList->AutoIterate();
        }

        //--Player's level position is needed.
        TilemapActor *rActor = AdventureLevel::Fetch()->LocatePlayerActor();//Can't fail because the level owns the menu!
        if(rActor)
        {
            float tPlayerX = mAdvancedMapRemapX + (rActor->GetWorldX() * mAdvancedMapScaleX) + tMapOffsetX;
            float tPlayerY = mAdvancedMapRemapY + (rActor->GetWorldY() * mAdvancedMapScaleY) + tMapOffsetY;
            //fprintf(stderr, "Position: %f %f\n", tPlayerX, tPlayerY);
            glDisable(GL_TEXTURE_2D);
            glBegin(GL_QUADS);
                glVertex2f(tPlayerX - 5.0f, tPlayerY - 5.0f);
                glVertex2f(tPlayerX + 5.0f, tPlayerY - 5.0f);
                glVertex2f(tPlayerX + 5.0f, tPlayerY + 5.0f);
                glVertex2f(tPlayerX - 5.0f, tPlayerY + 5.0f);
            glEnd();
            glEnable(GL_TEXTURE_2D);
        }

        //--Flags.
        mHasRendered = true;
    }
    //--Active map.
    else if(rActiveMap)
    {
        //--Render the map at the requested offsets.
        rActiveMap->Draw(tMapOffsetX, tMapOffsetY);

        //--Render the player actor to indicate where the player is. This occurs at the same spot regardless of the map's scrolling.
        TilemapActor *rActor = AdventureLevel::Fetch()->LocatePlayerActor();//Can't fail because the level owns the menu!
        if(rActor)
        {
            //--Get the walking image for this actor.
            SugarBitmap *rIdleImage = rActor->GetMapImage((mMapTimer / 2));
            if(rIdleImage)
            {
                //--Setup.
                float cScale = 3.0f;

                //--Resolve position.
                float tX = mPlayerMapPositionX + tMapOffsetX - (rIdleImage->GetTrueWidth()  / 2.0f * cScale);
                float tY = mPlayerMapPositionY + tMapOffsetY - (rIdleImage->GetTrueHeight() / 2.0f * cScale);
                if(DisplayManager::xLowDefinitionFlag)
                {
                    tX = tX * 0.5f;
                    tY = tY * 0.5f;
                }

                //--Translate to position.
                glTranslatef(tX, tY, 0.0f);

                //--Render scaled down.
                glScalef(cScale, cScale, 1.0f);
                rIdleImage->Draw();
                glScalef(1.0f / cScale, 1.0f / cScale, 1.0f);

                //--Clean.
                glTranslatef(-tX, -tY, 0.0f);
            }
        }

        //--Render party members around the character.
        int i = 0;
        rActor = AdventureLevel::Fetch()->LocateFollowingActor(i);
        while(rActor)
        {
            //--Get the walking image for this actor.
            SugarBitmap *rIdleImage = rActor->GetMapImage((mMapTimer / 2) + 20);
            if(rIdleImage)
            {
                //--Setup.
                float cScale = 3.0f;

                //--Resolve position.
                float tX = mPlayerMapPositionX + tMapOffsetX - (rIdleImage->GetTrueWidth()  / 2.0f * cScale) + ((i+1) * 16.0f * cScale);
                float tY = mPlayerMapPositionY + tMapOffsetY - (rIdleImage->GetTrueHeight() / 2.0f * cScale);
                if(DisplayManager::xLowDefinitionFlag)
                {
                    tX = tX * 0.5f;
                    tY = tY * 0.5f;
                }

                //--Translate to position.
                glTranslatef(tX, tY, 0.0f);

                //--Render scaled down.
                glScalef(cScale, cScale, 1.0f);
                rIdleImage->Draw();
                glScalef(1.0f / cScale, 1.0f / cScale, 1.0f);

                //--Clean.
                glTranslatef(-tX, -tY, 0.0f);
            }

            //--Next actor.
            i ++;
            rActor = AdventureLevel::Fetch()->LocateFollowingActor(i);
        }
    }

    //--Map Scaling
    if(mMapZoom != 1.0f)
    {
        glTranslatef(VIRTUAL_CANVAS_X * 0.5f, VIRTUAL_CANVAS_Y * 0.5f, 0.0f);
        glScalef(1.0f / mMapZoom, 1.0f / mMapZoom, 1.0f);
        glTranslatef(VIRTUAL_CANVAS_X * -0.5f, VIRTUAL_CANVAS_Y * -0.5f, 0.0f);
    }

    //--Render the map overlay. This has no offsets.
    if(rActiveMapOverlay) rActiveMapOverlay->Draw();

    //--UI Rendering.
    float cTxtHei = Images.CampfireMenu.rMainlineFont->GetTextHeight();
    Images.CampfireMenu.rMainlineFont->DrawText(0.0f, VIRTUAL_CANVAS_Y - (cTxtHei + 15.0f), 0, 1.0f, "Press 'Activate' key to Zoom");

    //--If fading, we may need to render over the map.
    if(mMapFadeTimer > -1)
    {
        //--When fading to the map...
        if(mIsEnteringMapMode)
        {
            //--Render a fade-in sequence over the map.
            if(mMapFadeTimer >= FADE_TICKS_OUT + FADE_TICKS_HOLD)
            {
                int tUseTimer = mMapFadeTimer - FADE_TICKS_OUT - FADE_TICKS_HOLD;
                SugarBitmap::DrawFullBlack(1.0f - (tUseTimer / (float)FADE_TICKS_IN));
            }
        }
        //--When fading from the map...
        else
        {
            //--Render a fade-out sequence over the map.
            if(mMapFadeTimer < FADE_TICKS_OUT)
            {
                SugarBitmap::DrawFullBlack(mMapFadeTimer / (float)FADE_TICKS_OUT);
            }
        }
    }
}
