//--Base
#include "AdventureMenu.h"

//--Classes
#include "AdvCombat.h"
#include "AdvCombatEntity.h"
#include "AdventureInventory.h"
#include "AdventureItem.h"
#include "VisualLevel.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarFont.h"
#include "SugarLinkedList.h"

//--Definitions
#include "EasingFunctions.h"

//--Libraries
//--Managers
#include "AudioManager.h"
#include "ControlManager.h"

//--[Local Definitions]
#define SOCKET_SLIDE_TICKS 15.0f
#define SOCKET_REPLACE_PER_PAGE 10

//--[Forward Declarations]
void RenderEquipmentImage(float pX, float pY, float pWidth, AdvCombatEntity *pEntity, SugarBitmap *pGemIndicator, int pEquipmentSlot, SugarFont *pRenderFont);

//--[Manipulators]
void AdventureMenu::ActivateSocketMode()
{
    //--Flags.
    mIsSocketEditingMode = true;
    mSocketEditSlot = 0;
    mSocketQuerySlot = -1;
    mGemMinimumRender = 0;

    //--Build a list of unequipped gems to swap to.
    AdventureInventory::Fetch()->BuildGemListNoEquip();
}

//--[Update]
void AdventureMenu::UpdateSocketMode()
{
    //--[Documentation and Setup]
    //--Updates socket mode, which allows the player to select valid sockets on their equipment and place gems into them.
    //  This mode can only be activated on an equipped item which has gem slots.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Get the item being edited.
    AdvCombatEntity *rEquipEntity = AdvCombat::Fetch()->GetActiveMemberI(mCharacterIndex);
    if(!rEquipEntity) return;
    AdventureItem *rSelectedItem = rEquipEntity->GetEquipmentBySlot("Dummy");
    if(!rSelectedItem) return;

    //--[Selecting Gem Slots]
    if(mSocketQuerySlot == -1)
    {
        //--[Directional Keys]
        //--Setup.
        int tSlotsAvailable = rSelectedItem->GetGemSlots();

        //--Up. Decrement slot by 1 if it's odd.
        if(rControlManager->IsFirstPress("Up"))
        {
            mSocketEditSlot --;
            if(mSocketEditSlot < 0) mSocketEditSlot = 0;
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
        //--Down. Increment slot by 1 if it's even.
        if(rControlManager->IsFirstPress("Down"))
        {
            mSocketEditSlot ++;
            if(mSocketEditSlot >= tSlotsAvailable) mSocketEditSlot = tSlotsAvailable - 1;
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }

        //--[Action Keys]
        //--Activate will either start swapping gems out, or fail if the gem slot is out of range.
        if(rControlManager->IsFirstPress("Activate"))
        {
            //--Get the piece of equipment in question.
            //if(mCurrentCursor >= 0 && mCurrentCursor < ADITEM_EQUIP_SLOTS)
            {
                //--Check if the gem slot in question is usable.
                if(rSelectedItem->GetGemSlots() < mSocketEditSlot || mSocketEditSlot < 0)
                {
                    AudioManager::Fetch()->PlaySound("Menu|Failed");
                    return;
                }
                //--It's usable, set the next cursor to zero to activate gem selection mode.
                else
                {
                    mSocketQuerySlot = 0;
                    mGemMinimumRender = 0;
                    AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
                }
            }
        }
        //--Cancel returns to normal equipment mode.
        else if(rControlManager->IsFirstPress("Cancel"))
        {
            mIsSocketEditingMode = false;
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
    }
    //--[Gem Selection Mode]
    //--Pick which gem you want to shove into this slot.
    else
    {
        //--Pointers.
        SugarLinkedList *rGemList = AdventureInventory::Fetch()->GetGemList();

        //--[Directional Keys]
        //--Up. Decrement slots by 1.
        if(rControlManager->IsFirstPress("Up"))
        {
            if(mSocketQuerySlot > 0)
            {
                mSocketQuerySlot --;
                if(mSocketQuerySlot < mGemMinimumRender + 4) mGemMinimumRender --;
                if(mGemMinimumRender < 0) mGemMinimumRender = 0;
                AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            }
        }
        //--Down. Increment slots by 1.
        if(rControlManager->IsFirstPress("Down"))
        {
            if(mSocketQuerySlot < rGemList->GetListSize() -1)
            {
                //--Move socket and clamp.
                mSocketQuerySlot ++;
                if(mSocketQuerySlot > mGemMinimumRender + 4) mGemMinimumRender ++;

                //--Check if we've exceeded the max minimum (what a confusing sentence).
                if(mGemMinimumRender > rGemList->GetListSize() - SOCKET_REPLACE_PER_PAGE) mGemMinimumRender --;

                //--SFX.
                AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            }
        }

        //--[Action Keys]
        //--Activate will swap the requested gem for the gem in the slot.
        if(rControlManager->IsFirstPress("Activate"))
        {
            //--If the query cursor is zero, this is the unequip action. Dump the gem into the inventory.
            if(mSocketQuerySlot == 0)
            {
                AdventureItem *rReceivedGem = rSelectedItem->RemoveGemFromSlot(mSocketEditSlot);
                if(rReceivedGem && !rSelectedItem->mGemErrorFlag) AdventureInventory::Fetch()->RegisterItem(rReceivedGem);
            }
            //--Otherwise, this is a swap action.
            else
            {
                //--The owning entity briefly unequips the item in question.
                /*
                int tCurrentItemSlot = rEquipEntity->GetSlotOfEquipment(rSelectedItem);
                if(tCurrentItemSlot == ACE_EQUIP_WEAPONA) rEquipEntity->EquipWeapon(NULL);
                if(tCurrentItemSlot == ACE_EQUIP_ARMOR) rEquipEntity->EquipArmor(NULL);
                if(tCurrentItemSlot == ACE_EQUIP_ACCESSORYA) rEquipEntity->EquipAccessoryA(NULL);
                if(tCurrentItemSlot == ACE_EQUIP_ACCESSORYB) rEquipEntity->EquipAccessoryB(NULL);
                if(tCurrentItemSlot == ACE_EQUIP_ITEMA) rEquipEntity->EquipItemA(NULL);
                if(tCurrentItemSlot == ACE_EQUIP_ITEMB) rEquipEntity->EquipItemB(NULL);*/

                //--Swap the gems in and out.
                AdventureItem *rNewGem = (AdventureItem *)rGemList->GetElementBySlot(mSocketQuerySlot);
                AdventureItem *rReceivedGem = rSelectedItem->PlaceGemInSlot(mSocketEditSlot, rNewGem);

                //--Register/Unregister the gems as necessary.
                if(!rSelectedItem->mGemErrorFlag)
                {
                    AdventureInventory::Fetch()->RegisterItem(rReceivedGem);
                    AdventureInventory::Fetch()->LiberateItemP(rNewGem);
                }

                //--Re-equip the item. This recomputes the bonuses.
                /*
                if(tCurrentItemSlot == ACE_EQUIP_WEAPONA) rEquipEntity->EquipWeapon(rSelectedItem);
                if(tCurrentItemSlot == ACE_EQUIP_ARMOR) rEquipEntity->EquipArmor(rSelectedItem);
                if(tCurrentItemSlot == ACE_EQUIP_ACCESSORYA) rEquipEntity->EquipAccessoryA(rSelectedItem);
                if(tCurrentItemSlot == ACE_EQUIP_ACCESSORYB) rEquipEntity->EquipAccessoryB(rSelectedItem);
                if(tCurrentItemSlot == ACE_EQUIP_ITEMA) rEquipEntity->EquipItemA(rSelectedItem);
                if(tCurrentItemSlot == ACE_EQUIP_ITEMB) rEquipEntity->EquipItemB(rSelectedItem);*/

                //--In all cases, inventory deregisters the item in question.
                AdventureInventory::Fetch()->LiberateItemP(rSelectedItem);
            }

            //--In either case, clear the query slot.
            mSocketQuerySlot = -1;
            AdventureInventory::Fetch()->BuildGemListNoEquip();
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
        //--Cancel returns to slot selection mode.
        else if(rControlManager->IsFirstPress("Cancel"))
        {
            mSocketQuerySlot = -1;
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
    }
}

//--[Render]
void AdventureMenu::RenderSocketMode()
{
    //--[Documentation and Setup]
    //--Renders over top of the rest of the equipment UI. Fades it out slightly to indicate it's different.
    float cSocketSlidePercent = 1.0f - (EasingFunction::QuadraticInOut(mSocketEditTimer, SOCKET_SLIDE_TICKS));
    SugarBitmap::DrawFullBlack(0.75f * (1.0f - cSocketSlidePercent));

    //--Fast-access pointers.
    SugarLinkedList *rGemList = AdventureInventory::Fetch()->GetGemList();

    //--Constants.
    float cDistOffset = 500.0f;

    //--Get the owning character and the equipped item.
    AdvCombatEntity *rEquipEntity = AdvCombat::Fetch()->GetActiveMemberI(mCharacterIndex);
    if(!rEquipEntity) return;
    AdventureItem *rSelectedItem = rEquipEntity->GetEquipmentBySlot("Dummy");
    if(!rSelectedItem) return;

    //--[Equipment Display]
    //--This part of the display shows the currently equipped item and the gems in it. It also shows the
    //  statistics of the item in question.
    float cYOffset = cDistOffset * cSocketSlidePercent * -1.0f;
    glTranslatef(0.0f, cYOffset, 0.0f);
    Images.EquipmentUI.rSocketFrameA->Draw();

    //--Header.
    float cEqpHeaderX = 683.0f;
    float cEqpHeaderY =  59.0f;
    Images.EquipmentUI.rHeadingFont->DrawText(cEqpHeaderX, cEqpHeaderY, SUGARFONT_AUTOCENTER_X, 1.0f, "Equipped Gems");

    //--Name of item, icon.
    float cEqpIconX = 352.0f;
    float cEqpIconY = 120.0f;
    float cEqpNameX = cEqpIconX + 25.0f;
    SugarBitmap *rIcon = rSelectedItem->GetIconImage();
    if(rIcon) rIcon->Draw(cEqpIconX, cEqpIconY);
    Images.EquipmentUI.rMainlineFont->DrawText(cEqpNameX, cEqpIconY+1.0f, 0, 1.0f, rSelectedItem->GetName());

    //--Constants for gem listing.
    float cGemIconX = cEqpIconX + 10.0f;
    float cGemNameX = cGemIconX + 25.0f;
    float cGemRenderY = cEqpIconY + 25.0f;
    float cGemRenderH = 25.0f;

    //--Gem Listing, with icons.
    AdventureItem *rHighlightedGem = NULL;
    int tGemSlots = rSelectedItem->GetGemSlots();
    for(int i = 0; i < tGemSlots; i ++)
    {
        //--Get the gem in the slot.
        AdventureItem *rGemInSlot = rSelectedItem->GetGemInSlot(i);

        //--If this slot is selected:
        bool tShowHighlight = false;
        if(mSocketEditSlot == i)
        {
            tShowHighlight = true;
            rHighlightedGem = rGemInSlot;
        }

        //--If the gem exists, render its icon and name.
        if(rGemInSlot)
        {
            //--Icon.
            SugarBitmap *rGemIcon = rGemInSlot->GetIconImage();
            Images.EquipmentUI.rGemEmpty22px->Draw(cGemIconX, cGemRenderY + (i * cGemRenderH));
            if(rGemIcon) rGemIcon->Draw(cGemIconX, cGemRenderY + (i * cGemRenderH));

            //--Name.
            if(tShowHighlight) StarlightColor::SetMixer(1.0f, 0.0f, 0.0f, 1.0f);
            Images.EquipmentUI.rMainlineFont->DrawText(cGemNameX, cGemRenderY + (i * cGemRenderH) + 1.0f, 0, 1.0f, rGemInSlot->GetName());
        }
        //--Gem slot is empty.
        else
        {
            //--Empty slot.
            Images.EquipmentUI.rGemEmpty22px->Draw(cGemIconX, cGemRenderY + (i * cGemRenderH));

            //--Name.
            if(tShowHighlight) StarlightColor::SetMixer(1.0f, 0.0f, 0.0f, 1.0f);
            Images.EquipmentUI.rMainlineFont->DrawText(cGemNameX, cGemRenderY + (i * cGemRenderH) + 1.0f, 0, 1.0f, "(Empty Slot)");
        }

        //--Clean.
        StarlightColor::ClearMixer();
    }

    //--Subdivision line. Splits description from gem list.
    SugarBitmap::DrawRectFill(334.0f, 298.0f, 334.0f + 698.0f, 300.0f, StarlightColor::MapRGBAF(0.6f, 0.6f, 0.6f, 1.0f));

    //--If there is a highlighted gem, render its description.
    if(rHighlightedGem)
    {
        RenderEquipmentDescription(rHighlightedGem);
    }

    //--Clean.
    glTranslatef(0.0f, -cYOffset, 0.0f);

    //--[Replacement Display]
    //--Shows the gems which can validly replace the current gem.
    cYOffset = cDistOffset * cSocketSlidePercent;
    glTranslatef(0.0f, cYOffset, 0.0f);
    Images.EquipmentUI.rSocketFrameC->Draw();

    //--Header.
    float cRepHeaderX =  47.0f;
    float cRepHeaderY = 439.0f;
    Images.EquipmentUI.rHeadingFont->DrawText(cRepHeaderX, cRepHeaderY, 0, 1.0f, "Available Gems");

    //--Variables.
    float cInvIconX = cRepHeaderX + 10.0f;
    float cInvNameX = cInvIconX + 25.0f;
    float tCurrentY = cRepHeaderY + 45.0f;
    float cInvSpaceY = 25.0f;

    //--Upgrade indicator.
    float cRepUpgX = 350.0f;
    Images.EquipmentUI.rMainlineFont->DrawText(cRepUpgX, cRepHeaderY + 12.0f, 0, 1.0f, "Upgrades");

    //--Property Headers. Similar to the inventory's version.
    float cRepPropertiesX = 500.0f;
    float cRepPropertiesW =  25.0f;
    float cImgHeadersX[AM_INV_HEADER_TOTAL];
    for(int i = 0; i < AM_INV_HEADER_TOTAL; i ++)
    {
        cImgHeadersX[i] = cRepPropertiesX + (i * cRepPropertiesW);
        Images.InventoryUI.rImageHeaders[i]->Draw(cImgHeadersX[i], cRepHeaderY + 15.0f);
    }

    //--Render differentiation backers.
    for(int i = 1; i < SOCKET_REPLACE_PER_PAGE; i += 2)
    {
        SugarBitmap::DrawRectFill(37, cRepHeaderY + 45.0f + (cInvSpaceY * i) - 1.0f, 1034, cRepHeaderY + 45.0f + (cInvSpaceY * i) + cInvSpaceY - 1.0f, StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, 1.0f));
    }

    //--Render dividers.
    SugarBitmap::DrawRectFill(cImgHeadersX[0] - 5.0f, 435.0f, cImgHeadersX[0] - 3.0f, 751.0f, StarlightColor::MapRGBAF(0.3f, 0.3f, 0.3f, 1.0f));
    for(int p = 0; p < AM_INV_HEADER_TOTAL - 1; p ++)
    {
        SugarBitmap::DrawRectFill(cImgHeadersX[p+1] - 5.0f, 435.0f, cImgHeadersX[p+1] - 3.0f, 751.0f, StarlightColor::MapRGBAF(0.3f, 0.3f, 0.3f, 1.0f));
    }

    //--List of replacement gems.
    int tInvSlot = 0;
    int tRendersLeft = SOCKET_REPLACE_PER_PAGE;
    AdventureItem *rComparisonGem = NULL;
    AdventureItem *rCheckGem = (AdventureItem *)rGemList->PushIterator();
    while(rCheckGem)
    {
        //--Don't render items under the minimum.
        if(tInvSlot < mGemMinimumRender)
        {
            tInvSlot ++;
            rCheckGem = (AdventureItem *)rGemList->AutoIterate();
            continue;
        }

        //--Highlighting.
        bool tIsHighlighted = false;
        if(tInvSlot == mSocketQuerySlot)
        {
            tIsHighlighted = true;
            rComparisonGem = rCheckGem;
        }

        //--Very special: This is the Unequip option.
        if(rCheckGem == AdventureInventory::xrDummyUnequipItem)
        {
            Images.EquipmentUI.rUnequipIcon->Draw(cInvIconX, tCurrentY);
            if(tIsHighlighted) StarlightColor::SetMixer(1.0f, 0.0f, 0.0f, 1.0f);
            Images.EquipmentUI.rMainlineFont->DrawText(cInvNameX, tCurrentY + 1.0f, 0, 1.0f, "Remove Gem");
        }
        //--Normal case.
        else
        {
            //--Icon, no frame.
            SugarBitmap *rGemIcon = rCheckGem->GetIconImage();
            if(rGemIcon) rGemIcon->Draw(cInvIconX, tCurrentY);

            //--Name.
            if(tIsHighlighted) StarlightColor::SetMixer(1.0f, 0.0f, 0.0f, 1.0f);
            Images.EquipmentUI.rMainlineFont->DrawText(cInvNameX, tCurrentY + 1.0f, 0, 1.0f, rCheckGem->GetName());
        }
        StarlightColor::ClearMixer();

        //--If this is not the unequip item, render the gem's properties.
        if(rCheckGem != AdventureInventory::xrDummyUnequipItem)
        {
            //--How many times it was upgraded.
            int tUpgrades = rCheckGem->GetUpgradesTotal();
            Images.StatusUI.rMainlineFont->DrawTextArgs(400.0f, tCurrentY - 1.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "%i", tUpgrades);

            //--Statistics.
            for(int p = 0; p < AM_INV_HEADER_TOTAL; p ++)
            {
                int cBonus = rCheckGem->GetPropertyByInventoryHeaders(p, false);
                if(cBonus != 0)
                {
                    Images.StatusUI.rVerySmallFont->DrawTextArgs(cImgHeadersX[p] + 9.0f, tCurrentY + 4.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "%i", cBonus);
                }
            }
        }

        //--Stop rendering when the maximum number of gems is displayed.
        tRendersLeft --;
        if(tRendersLeft < 1)
        {
            rGemList->PopIterator();
            break;
        }

        //--Move the Y position down.
        tCurrentY = tCurrentY + cInvSpaceY;

        //--Next.
        tInvSlot ++;
        rCheckGem = (AdventureItem *)rGemList->AutoIterate();
    }

    //--Scrollbar. Only renders if there are enough gems on the list to warrant it.
    if(rGemList->GetListSize() > SOCKET_REPLACE_PER_PAGE)
    {
        //--Binding.
        Images.EquipmentUI.rScrollbarFront->Bind();

        //--Scrollbar front. Stretches dynamically.
        float cScrollTop = 463.0f;
        float cScrollHei = 259.0f;

        //--Determine what percentage of the gem list is presently represented.
        int tMaxOffset = rGemList->GetListSize();
        float cPctTop = (mGemMinimumRender                          ) / (float)tMaxOffset;
        float cPctBot = (mGemMinimumRender + SOCKET_REPLACE_PER_PAGE) / (float)tMaxOffset;

        //--Positions and Constants.
        float cLft = 1034.0f;
        float cRgt = cLft + Images.InventoryUI.rScrollbarFront->GetWidth();
        float cEdg = 6.0f / (float)Images.InventoryUI.rScrollbarFront->GetHeight();

        //--Compute where the bar should be.
        float cTopTop = cScrollTop + (cPctTop * cScrollHei);
        float cTopBot = cTopTop + 6.0f;
        float cBotBot = cScrollTop + (cPctBot * cScrollHei);
        float cBotTop = cBotBot - 6.0f;

        glBegin(GL_QUADS);
            //--Render the top of the bar.
            glTexCoord2f(0.0f, 0.0f); glVertex2f(cLft, cTopTop);
            glTexCoord2f(1.0f, 0.0f); glVertex2f(cRgt, cTopTop);
            glTexCoord2f(1.0f, cEdg); glVertex2f(cRgt, cTopBot);
            glTexCoord2f(0.0f, cEdg); glVertex2f(cLft, cTopBot);

            //--Render the bottom of the bar.
            glTexCoord2f(0.0f, 1.0f - cEdg); glVertex2f(cLft, cBotTop);
            glTexCoord2f(1.0f, 1.0f - cEdg); glVertex2f(cRgt, cBotTop);
            glTexCoord2f(1.0f, 1.0f);        glVertex2f(cRgt, cBotBot);
            glTexCoord2f(0.0f, 1.0f);        glVertex2f(cLft, cBotBot);

            //--Render the middle of the bar.
            glTexCoord2f(0.0f, cEdg);        glVertex2f(cLft, cTopBot);
            glTexCoord2f(1.0f, cEdg);        glVertex2f(cRgt, cTopBot);
            glTexCoord2f(1.0f, 1.0f - cEdg); glVertex2f(cRgt, cBotTop);
            glTexCoord2f(0.0f, 1.0f - cEdg); glVertex2f(cLft, cBotTop);
        glEnd();

        //--Fixed backing, which actually goes in front of the scrollbar.
        Images.EquipmentUI.rSocketScroll->Draw();
    }

    //--Clean.
    glTranslatef(0.0f, -cYOffset, 0.0f);

    //--[Properties Display]
    //--Shows the properties of the gem and, if applicable, the properties of its replacement.
    float cXOffset = cDistOffset * cSocketSlidePercent;
    glTranslatef(cXOffset, 0.0f, 0.0f);
    Images.EquipmentUI.rSocketFrameB->Draw();

    //--Subroutine handles this.
    RenderGemComparison(rHighlightedGem, rComparisonGem);

    //--Clean.
    glTranslatef(-cXOffset, 0.0f, 0.0f);
}
void AdventureMenu::RenderGemComparison(AdventureItem *pDescriptionGem, AdventureItem *pComparisonGem)
{
    //--[Documentation and Setup]
    //--Renders a comparison listing between the two provided items. Basically identical to RenderEquipmentComparison()
    //  except, since these are gems, does not include the gems column.
    if(!Images.mIsReady) return;

    //--Constants
    float cBaseValueX = 1160.0f;
    float cArrowX = cBaseValueX + 45.0f;
    float cCompareValueX = cArrowX + 45.0f;

    //--[Header]
    Images.EquipmentUI.rHeadingFont->DrawText(1211,           65.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Properties");
    Images.StatusUI.rVerySmallFont->DrawText(cBaseValueX,    105.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Base");
    if(pComparisonGem) Images.StatusUI.rVerySmallFont->DrawText(cCompareValueX, 105.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "New");

    //--[Icons]
    //--Render the statistic icons on the right side of the screen. Also store their Y positions.
    float cImgHeadersStartY = 130.0f;
    float cImgHeadersSpaceY = 20.0f;
    float cImgHeadersX = 1107.0f;
    float cImgHeadersY[AM_INV_HEADER_TOTAL];
    for(int i = 0; i < AM_INV_HEADER_TOTAL; i ++)
    {
        cImgHeadersY[i] = cImgHeadersStartY + (i * cImgHeadersSpaceY);
        Images.InventoryUI.rImageHeaders[i]->Draw(cImgHeadersX, cImgHeadersStartY + (i * cImgHeadersSpaceY));
    }

    //--[No Comparison]
    //--Just render the statistics of the provided item.
    if(pDescriptionGem && !pComparisonGem)
    {
        //--Iterate.
        for(int i = 0; i < AM_INV_HEADER_TOTAL; i ++)
        {
            //--Render the bonus.
            int cBonus = pDescriptionGem->GetPropertyByInventoryHeaders(i, true);
            if(cBonus != 0)
            {
                Images.StatusUI.rVerySmallFont->DrawTextArgs(cBaseValueX, cImgHeadersY[i], SUGARFONT_AUTOCENTER_X, 1.0f, "%i", cBonus);
            }
        }
    }
    //--[No Description]
    //--The description item is unequipped. All its stats are zeroes.
    else if(!pDescriptionGem && pComparisonGem)
    {
        //--Iterate.
        for(int i = 0; i < AM_INV_HEADER_TOTAL; i ++)
        {
            //--Get properties from both. Description item is always zero.
            int cBonusDesc = 0;
            int cBonusComp = pComparisonGem->GetPropertyByInventoryHeaders(i, true);

            //--If both are zero, don't render them.
            if(cBonusComp == 0 && cBonusDesc == 0) continue;

            //--Render the bonus, even if it's zero.
            Images.StatusUI.rVerySmallFont->DrawTextArgs(cBaseValueX, cImgHeadersY[i], SUGARFONT_AUTOCENTER_X, 1.0f, "%i", cBonusDesc);

            //--Render an arrow.
            Images.StatusUI.rVerySmallFont->DrawTextArgs(cArrowX, cImgHeadersY[i], 0, 1.0f, "->");

            //--Render the comparison value. Select color based on whether it's higher or lower.
            if(cBonusComp > cBonusDesc)
            {
                StarlightColor::SetMixer(0.0f, 1.0f, 0.0f, 1.0f);
            }
            else if(cBonusComp == cBonusDesc)
            {
                StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, 1.0f);
            }
            else
            {
                StarlightColor::SetMixer(1.0f, 0.0f, 0.0f, 1.0f);
            }
            Images.StatusUI.rVerySmallFont->DrawTextArgs(cCompareValueX, cImgHeadersY[i], SUGARFONT_AUTOCENTER_X, 1.0f, "%i", cBonusComp);
            StarlightColor::ClearMixer();
        }
    }
    //--[Unequip]
    //--Renders a comparison against unequipping the gem. This shows zeroes for all stats on the comparison gem.
    else if(pDescriptionGem && pComparisonGem && pComparisonGem == AdventureInventory::xrDummyUnequipItem)
    {
        //--Iterate.
        for(int i = 0; i < AM_INV_HEADER_TOTAL; i ++)
        {
            //--Get properties from both:
            int cBonusDesc = pDescriptionGem->GetPropertyByInventoryHeaders(i, true);
            int cBonusComp = 0;

            //--If all are zero, don't render them.
            if(cBonusComp == 0 && cBonusDesc == 0) continue;

            //--Render the bonus, even if it's zero.
            Images.StatusUI.rVerySmallFont->DrawTextArgs(cBaseValueX, cImgHeadersY[i], SUGARFONT_AUTOCENTER_X, 1.0f, "%i", cBonusDesc);

            //--Render an arrow.
            Images.StatusUI.rVerySmallFont->DrawTextArgs(cArrowX, cImgHeadersY[i], 0, 1.0f, "->");

            //--Render the comparison value. Select color based on whether it's higher or lower.
            if(cBonusComp > cBonusDesc)
            {
                StarlightColor::SetMixer(0.0f, 1.0f, 0.0f, 1.0f);
            }
            else if(cBonusComp == cBonusDesc)
            {
                StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, 1.0f);
            }
            else
            {
                StarlightColor::SetMixer(1.0f, 0.0f, 0.0f, 1.0f);
            }
            Images.StatusUI.rVerySmallFont->DrawTextArgs(cCompareValueX, cImgHeadersY[i], SUGARFONT_AUTOCENTER_X, 1.0f, "%i", cBonusComp);
            StarlightColor::ClearMixer();
        }
    }
    //--[Comparison]
    //--Render the statistics of the provided item, modified to show how the comparison item is. Stat improvements
    //  are listed in green, neutrals are white, otherwise red.
    else if(pDescriptionGem && pComparisonGem)
    {
        //--Iterate.
        for(int i = 0; i < AM_INV_HEADER_TOTAL; i ++)
        {
            //--Get properties from both:
            int cBonusDesc = pDescriptionGem->GetPropertyByInventoryHeaders(i, true);
            int cBonusComp = pComparisonGem->GetPropertyByInventoryHeaders(i, true);

            //--If all are zero, don't render them.
            if(cBonusComp == 0 && cBonusDesc == 0) continue;

            //--Render the bonus, even if it's zero.
            Images.StatusUI.rVerySmallFont->DrawTextArgs(cBaseValueX, cImgHeadersY[i], SUGARFONT_AUTOCENTER_X, 1.0f, "%i", cBonusDesc);

            //--Render an arrow.
            Images.StatusUI.rVerySmallFont->DrawTextArgs(cArrowX, cImgHeadersY[i], 0, 1.0f, "->");

            //--Render the comparison value. Select color based on whether it's higher or lower.
            if(cBonusComp > cBonusDesc)
            {
                StarlightColor::SetMixer(0.0f, 1.0f, 0.0f, 1.0f);
            }
            else if(cBonusComp == cBonusDesc)
            {
                StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, 1.0f);
            }
            else
            {
                StarlightColor::SetMixer(1.0f, 0.0f, 0.0f, 1.0f);
            }
            Images.StatusUI.rVerySmallFont->DrawTextArgs(cCompareValueX, cImgHeadersY[i], SUGARFONT_AUTOCENTER_X, 1.0f, "%i", cBonusComp);
            StarlightColor::ClearMixer();
        }
    }
}
