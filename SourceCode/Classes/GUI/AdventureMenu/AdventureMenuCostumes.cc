//--Base
#include "AdventureMenu.h"

//--Classes
//--CoreClasses
#include "SugarBitmap.h"
#include "SugarFont.h"
#include "SugarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "EasingFunctions.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "DebugManager.h"
#include "DisplayManager.h"
#include "LuaManager.h"

//--[System]
void AdventureMenu::ActivateCostumesMenu()
{
    //--[Setup]
    //--System
    if(!AdventureMenu::xCostumeResolveScript) return;
    mIsCostumesMenu = false;
    mIsCostumesSubMenu = false;
    mCostumeGridCurrent = 0;
    mCostumeGrid->ClearList();
    mCostumeGridHeaders->ClearList();
    mCostumeGridSublists->ClearList();

    //--Costume Selection
    mCostumeCharacterIndex = -1;
    mCostumeCharacterGrid->ClearList();

    //--Execute the costume resolver. This will build a list of costumes we can use.
    LuaManager::Fetch()->ExecuteLuaFile(AdventureMenu::xCostumeResolveScript);

    //--If no costumes got built somehow, stop here.
    if(mCostumeGridHeaders->GetListSize() < 1)
    {
        AudioManager::Fetch()->PlaySound("Menu|Failed");
        return;
    }

    //--SFX indicating success.
    AudioManager::Fetch()->PlaySound("Menu|Select");

    //--[Positioning and Controls]
    //--Figure out where the grid entries go dynamically. A set of priorities is built and the grid is filled
    //  according to those priorities. The priority is set to require the smallest number of keypresses to
    //  access the largest number of entries. The grid is 3 high by default, and has padding.
    int cXSize = 7;
    int cYSize = 1;
    int cXMiddle = 3;
    int cYMiddle = 0;
    int **tGridData = GridHandler::BuildGrid(cXSize, cYSize, cXMiddle, cYMiddle, mCostumeGridHeaders->GetListSize());

    //--Call the subroutine to build the control list for us.
    GridHandler::BuildGridList(mCostumeGrid, tGridData, cXSize, cYSize, mCostumeGridHeaders->GetListSize(), AM_GRID_CENT_X, AM_GRID_CENT_Y, AM_GRID_SPAC_X, AM_GRID_SPAC_Y);

    //--Clean up the grid data. This argument expects the original grid size.
    GridHandler::DeallocateGrid(tGridData, cXSize);

    //--[Final Flags]
    mIsCostumesMenu = true;
}
void AdventureMenu::DeactivateCostumesMenu()
{
    mIsCostumesMenu = false;
    mCostumeGrid->ClearList();
    mCostumeGridHeaders->ClearList();
    mCostumeGridSublists->ClearList();
    mCostumeCharacterGrid->ClearList();
}

//--[Manipulators]
void AdventureMenu::AddCostumeHeading(const char *pInternalName, const char *pCharName, const char *pFormName, const char *pImagePath)
{
    //--Adds a new heading to the costume menu. Headings are things like "Mei Alraune" and "Christine Golem", describing
    //  a character and an associated form or class. This can include characters who do not join the party.
    //--The heading automatically creates a new linked-list and associates itself with that list. This new list
    //  stores all the available costumes in that heading.
    if(!pInternalName || !pCharName || !pFormName || !pImagePath) return;

    //--Create the new package.
    SetMemoryData(__FILE__, __LINE__);
    CostumeHeadingPack *nPack = (CostumeHeadingPack *)starmemoryalloc(sizeof(CostumeHeadingPack));
    nPack->rImage = (SugarBitmap *)DataLibrary::Fetch()->GetEntry(pImagePath);
    strncpy(nPack->mCharacterName, pCharName, STD_MAX_LETTERS);
    strncpy(nPack->mFormName, pFormName, STD_MAX_LETTERS);

    //--Create a new parallel linked-list to store its entries.
    nPack->mSublistIndex = mCostumeGridSublists->GetListSize();
    SugarLinkedList *nAssociatedSublist = new SugarLinkedList(true);
    mCostumeGridSublists->AddElementAsTail("X", nAssociatedSublist, &SugarLinkedList::DeleteThis);

    //--Register to the main list.
    mCostumeGridHeaders->AddElementAsTail(pInternalName, nPack, &FreeThis);
}
void AdventureMenu::AddCostumeEntry(const char *pHeadingName, const char *pCostumeName, const char *pImagePath, const char *pScriptPath)
{
    //--Adds a new costume to the named heading. This contains an image to act as a preview, as well as a script to run
    //  if the player selects that costume.
    if(!pHeadingName || !pCostumeName || !pImagePath || !pScriptPath) return;

    //--Make sure the heading exists.
    CostumeHeadingPack *rCheckPack = (CostumeHeadingPack *)mCostumeGridHeaders->GetElementByName(pHeadingName);
    if(!rCheckPack) return;

    //--Make sure the associated sublist exists.
    SugarLinkedList *rAssociatedSublist = (SugarLinkedList *)mCostumeGridSublists->GetElementBySlot(rCheckPack->mSublistIndex);
    if(!rAssociatedSublist) return;

    //--Create the new package, register it.
    SetMemoryData(__FILE__, __LINE__);
    CostumePack *nPack = (CostumePack *)starmemoryalloc(sizeof(CostumePack));
    nPack->mIsSelected = false;
    strncpy(nPack->mExecPath, pScriptPath, STD_PATH_LEN);
    nPack->rImage = (SugarBitmap *)DataLibrary::Fetch()->GetEntry(pImagePath);
    rAssociatedSublist->AddElement(pCostumeName, nPack, &FreeThis);
}
void AdventureMenu::SetActiveCostumeEntry(const char *pHeadingName, const char *pCostumeName)
{
    //--Sets which costume is "active" in the heading. This costume will do nothing when it is selected, and appears
    //  different on the menu to indicate it is the active costume.
    //--Only one costume will be active at a time, all others will not. It's also possible for no costumes to be active.
    if(!pHeadingName || !pCostumeName) return;

    //--Make sure the heading exists.
    CostumeHeadingPack *rCheckPack = (CostumeHeadingPack *)mCostumeGridHeaders->GetElementByName(pHeadingName);
    if(!rCheckPack) return;

    //--Make sure the associated sublist exists.
    SugarLinkedList *rAssociatedSublist = (SugarLinkedList *)mCostumeGridSublists->GetElementBySlot(rCheckPack->mSublistIndex);
    if(!rAssociatedSublist) return;

    //--Run across the list and set activity.
    CostumePack *rPack = (CostumePack *)rAssociatedSublist->PushIterator();
    while(rPack)
    {
        rPack->mIsSelected = (!strcasecmp(rAssociatedSublist->GetIteratorName(), pCostumeName));
        rPack = (CostumePack *)rAssociatedSublist->AutoIterate();
    }
}

//--[Core Methods]
void AdventureMenu::ActivateCostumeSubMenu(int pIndex)
{
    //--When a character is selected, creates the grid entries for their costume selection.
    mIsCostumesSubMenu = false;
    mCostumeCharacterIndex = -1;
    mCostumeCharacterCurrent = 0;
    if(pIndex < 0 || pIndex >= mCostumeGridHeaders->GetListSize()) return;

    //--Get the sublist.
    SugarLinkedList *rSublist = (SugarLinkedList *)mCostumeGridSublists->GetElementBySlot(pIndex);
    if(!rSublist) return;

    //--Clear.
    mCostumeCharacterGrid->ClearList();

    //--Figure out where the grid entries go dynamically. A set of priorities is built and the grid is filled
    //  according to those priorities. The priority is set to require the smallest number of keypresses to
    //  access the largest number of entries. The grid is 3 high by default, and has padding.
    int cXSize = 7;
    int cYSize = 3;
    int cXMiddle = 3;
    int cYMiddle = 1;
    int **tGridData = GridHandler::BuildGrid(cXSize, cYSize, cXMiddle, cYMiddle, rSublist->GetListSize());

    //--Call the subroutine to build the control list for us.
    GridHandler::BuildGridList(mCostumeCharacterGrid, tGridData, cXSize, cYSize, rSublist->GetListSize(), AM_GRID_CENT_X, AM_GRID_CENT_Y, AM_GRID_SPAC_X, AM_GRID_SPAC_Y);

    //--Clean up the grid data. This argument expects the original grid size.
    GridHandler::DeallocateGrid(tGridData, cXSize);

    //--[Final Flags]
    mIsCostumesSubMenu = true;
    mCostumeCharacterIndex = pIndex;
}

//--[Update]
void AdventureMenu::UpdateCostumeMenu()
{
    //--[Timers]
    //--If in costumes mode, increment this timer.
    if(mIsCostumesMenu)
    {
        //--Selecting a character:
        if(mIsCostumesSubMenu == false)
        {
            if(mCostumeGridOpacityTimer < AM_CAMPFIRE_OPACITY_TICKS) mCostumeGridOpacityTimer ++;
            if(mCostumeCharacterOpacityTimer > 0) mCostumeCharacterOpacityTimer --;
        }
        //--Selecting a costume:
        else
        {
            if(mCostumeCharacterOpacityTimer < AM_CAMPFIRE_OPACITY_TICKS) mCostumeCharacterOpacityTimer ++;
            if(mCostumeGridOpacityTimer > 0) mCostumeGridOpacityTimer --;

            //--Run the update for the sub-selection and stop.
            UpdateCostumeMenuSub();
            return;
        }
    }
    //--Decrement and stop if not.
    else
    {
        if(mCostumeGridOpacityTimer > 0) mCostumeGridOpacityTimer --;
        if(mCostumeCharacterOpacityTimer > 0) mCostumeCharacterOpacityTimer --;
        return;
    }

    //--[Setup]
    //--Handles controls for the relive menu. Handles up/down/activate/cancel.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Error check:
    if(mCostumeGridCurrent < 0 || mCostumeGridCurrent >= mCostumeGrid->GetListSize()) mCostumeGridCurrent = 0;
    GridPackage *rPackage = (GridPackage *)mCostumeGrid->GetElementBySlot(mCostumeGridCurrent);
    if(!rPackage)
    {
        mStartedHidingThisTick = true;
        mIsCostumesMenu = false;
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        return;
    }

    //--[Directions]
    //--Directional controls are handled programmatically. If the instruction is -1, it means there is nothing
    //  at that grid position.
    int tGridPrevious = mCostumeGridCurrent;
    if(rControlManager->IsFirstPress("Up"))
    {
        int tTargetEntry = rPackage->mControlLookups[GRID_TOP];
        if(tTargetEntry != -1) mCostumeGridCurrent = tTargetEntry;
    }
    if(rControlManager->IsFirstPress("Down"))
    {
        int tTargetEntry = rPackage->mControlLookups[GRID_BOT];
        if(tTargetEntry != -1) mCostumeGridCurrent = tTargetEntry;
    }
    if(rControlManager->IsFirstPress("Left"))
    {
        int tTargetEntry = rPackage->mControlLookups[GRID_LFT];
        if(tTargetEntry != -1) mCostumeGridCurrent = tTargetEntry;
    }
    if(rControlManager->IsFirstPress("Right"))
    {
        int tTargetEntry = rPackage->mControlLookups[GRID_RGT];
        if(tTargetEntry != -1) mCostumeGridCurrent = tTargetEntry;
    }

    //--If the grid changed positions, play a sound.
    if(tGridPrevious != mCostumeGridCurrent)
    {
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }

    //--Size setting. All not-selected grid entries decrease their timers. The selected entry increases.
    GridPackage *rTimerPackage = (GridPackage *)mCostumeGrid->PushIterator();
    while(rTimerPackage)
    {
        //--Increment if selected.
        if(rTimerPackage == rPackage)
        {
            if(rTimerPackage->mTimer < AM_CAMPFIRE_SIZE_TICKS) rTimerPackage->mTimer ++;
        }
        //--Decrement if deselected.
        else
        {
            if(rTimerPackage->mTimer > 0) rTimerPackage->mTimer --;
        }
        rTimerPackage = (GridPackage *)mCostumeGrid->AutoIterate();
    }

    //--Relive a cutscene.
    if(rControlManager->IsFirstPress("Activate"))
    {
        mStartedHidingThisTick = true;
        ActivateCostumeSubMenu(mCostumeGridCurrent);
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }
    //--Back to the main menu.
    else if(rControlManager->IsFirstPress("Cancel"))
    {
        mStartedHidingThisTick = true;
        mIsCostumesMenu = false;
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }


    /*

    //--[Docuementation and Setup]
    //--Handles controls input for costumes mode. If the mode is not active, runs timers to cause it to gracefully leave the screen.
    if(!mIsCostumesMenu) return;

    //--Setup.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Up!
    if(rControlManager->IsFirstPress("Up"))
    {
    }
    //--Down!
    if(rControlManager->IsFirstPress("Down"))
    {
    }
    //--Left!
    if(rControlManager->IsFirstPress("Left"))
    {
        //--Top menu:
        if(mCostumeCursorLower == -1)
        {
            mCostumeCursorUpper --;
            if(mCostumeCursorUpper < 0) mCostumeCursorUpper = 0;
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
        //--Bottom menu:
        else
        {
            mCostumeCursorLower --;
            if(mCostumeCursorLower < 0) mCostumeCursorLower = 0;
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
    }
    //--Right!
    if(rControlManager->IsFirstPress("Right"))
    {
        //--Top menu:
        if(mCostumeCursorLower == -1)
        {
            mCostumeCursorUpper ++;
            if(mCostumeCursorUpper >= mCostumeHeadingList->GetListSize()) mCostumeCursorUpper = mCostumeHeadingList->GetListSize() - 1;
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
        //--Bottom menu:
        else
        {
            //--List must exist.
            SugarLinkedList *rSublist = (SugarLinkedList *)mCostumeSublistList->GetElementBySlot(mCostumeCursorUpper);
            if(!rSublist) return;

            //--Increment, clamp.
            mCostumeCursorLower ++;
            if(mCostumeCursorLower >= rSublist->GetListSize()) mCostumeCursorLower = rSublist->GetListSize() - 1;

            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
    }

    //--Activate. Either selects a heading, or activates a costume.
    if(rControlManager->IsFirstPress("Activate"))
    {
        //--Activate the heading.
        if(mCostumeCursorLower == -1)
        {
            mCostumeCursorLower = 0;
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        //--Run the costume script.
        else
        {
            //--List must exist.
            SugarLinkedList *rSublist = (SugarLinkedList *)mCostumeSublistList->GetElementBySlot(mCostumeCursorUpper);
            if(!rSublist) return;

            //--Get package.
            CostumePack *rPackage = (CostumePack *)rSublist->GetElementBySlot(mCostumeCursorLower);
            if(!rPackage) return;

            //--Build path.
            char tPath[STD_PATH_LEN];
            const char *rGamePath = DataLibrary::GetGamePath("Root/Paths/System/Startup/sAdventurePath");
            sprintf(tPath, "%s/%s", rGamePath, rPackage->mExecPath);

            //--Run it.
            LuaManager::Fetch()->ExecuteLuaFile(tPath);

            //--SFX.
            AudioManager::Fetch()->PlaySound("World|TakeItem");
        }
    }
    //--Cancel. Exits costume select, or closes the submenu.
    else if(rControlManager->IsFirstPress("Cancel"))
    {
        //--Top menu: Exit this menu.
        if(mCostumeCursorLower == -1)
        {
            mIsCostumesMenu = false;
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        //--Bottom menu: Back to top.
        else
        {
            mCostumeCursorLower = -1;
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
    }*/
}
void AdventureMenu::UpdateCostumeMenuSub()
{
    //--[Documentation]
    //--Handles controls for the costume submenu. Timers are handled by the caller.

    //--[Setup]
    //--Fast-access pointers.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Error check:
    if(mCostumeCharacterCurrent < 0 || mCostumeCharacterCurrent >= mCostumeCharacterGrid->GetListSize()) mCostumeCharacterCurrent = 0;
    GridPackage *rPackage = (GridPackage *)mCostumeCharacterGrid->GetElementBySlot(mCostumeCharacterCurrent);
    if(!rPackage)
    {
        mStartedHidingThisTick = true;
        mIsCostumesMenu = false;
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        return;
    }

    //--[Directions]
    //--Directional controls are handled programmatically. If the instruction is -1, it means there is nothing
    //  at that grid position.
    int tGridPrevious = mCostumeCharacterCurrent;
    if(rControlManager->IsFirstPress("Up"))
    {
        int tTargetEntry = rPackage->mControlLookups[GRID_TOP];
        if(tTargetEntry != -1) mCostumeCharacterCurrent = tTargetEntry;
    }
    if(rControlManager->IsFirstPress("Down"))
    {
        int tTargetEntry = rPackage->mControlLookups[GRID_BOT];
        if(tTargetEntry != -1) mCostumeCharacterCurrent = tTargetEntry;
    }
    if(rControlManager->IsFirstPress("Left"))
    {
        int tTargetEntry = rPackage->mControlLookups[GRID_LFT];
        if(tTargetEntry != -1) mCostumeCharacterCurrent = tTargetEntry;
    }
    if(rControlManager->IsFirstPress("Right"))
    {
        int tTargetEntry = rPackage->mControlLookups[GRID_RGT];
        if(tTargetEntry != -1) mCostumeCharacterCurrent = tTargetEntry;
    }

    //--If the grid changed positions, play a sound.
    if(tGridPrevious != mCostumeCharacterCurrent)
    {
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }

    //--Size setting. All not-selected grid entries decrease their timers. The selected entry increases.
    GridPackage *rTimerPackage = (GridPackage *)mCostumeCharacterGrid->PushIterator();
    while(rTimerPackage)
    {
        //--Increment if selected.
        if(rTimerPackage == rPackage)
        {
            if(rTimerPackage->mTimer < AM_CAMPFIRE_SIZE_TICKS) rTimerPackage->mTimer ++;
        }
        //--Decrement if deselected.
        else
        {
            if(rTimerPackage->mTimer > 0) rTimerPackage->mTimer --;
        }
        rTimerPackage = (GridPackage *)mCostumeCharacterGrid->AutoIterate();
    }

    //--Relive a cutscene.
    if(rControlManager->IsFirstPress("Activate"))
    {
        //--List must exist.
        SugarLinkedList *rSublist = (SugarLinkedList *)mCostumeGridSublists->GetElementBySlot(mCostumeCharacterIndex);
        if(!rSublist) return;

        //--Get package.
        CostumePack *rPackage = (CostumePack *)rSublist->GetElementBySlot(mCostumeCharacterCurrent);
        if(!rPackage) return;

        //--Build path.
        char tPath[STD_PATH_LEN];
        const char *rGamePath = DataLibrary::GetGamePath("Root/Paths/System/Startup/sAdventurePath");
        sprintf(tPath, "%s/%s", rGamePath, rPackage->mExecPath);

        //--Run it.
        LuaManager::Fetch()->ExecuteLuaFile(tPath);

        //--SFX.
        AudioManager::Fetch()->PlaySound("World|TakeItem");
    }
    //--Back to the main menu.
    else if(rControlManager->IsFirstPress("Cancel"))
    {
        mIsCostumesSubMenu = false;
        mStartedHidingThisTick = true;
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }
}

//--[Render]
void AdventureMenu::RenderCostumeMenu()
{
    //--[Setup]
    //--Don't render at all if the opacity is at zero.
    if(!Images.mIsReady) return;
    if(mCostumeGridOpacityTimer < 1) return;

    //--Opacity percent. Determines position and fading.
    float tOpacityPct = (float)mCostumeGridOpacityTimer / (float)AM_CAMPFIRE_OPACITY_TICKS;

    //--[Title]
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, tOpacityPct);
    Images.CampfireMenu.rHeader->Draw(427.0f, 45.0f);
    Images.CampfireMenu.rHeadingFont->DrawText(683.0f, 57.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Select a Character");
    StarlightColor::ClearMixer();

    //--[Icons]
    //--Sizes.
    float cIconSize = Images.CampfireMenu.rIconFrame->GetTrueWidth();

    //--Position the objects slide away from.
    float tOpacitySlidePct = EasingFunction::QuadraticOut(mCostumeGridOpacityTimer, AM_CAMPFIRE_OPACITY_TICKS);
    float tCenterX = AM_GRID_CENT_X;
    float tCenterY = AM_GRID_CENT_Y;

    //--Iterate across the list.
    GridPackage *rGridPack = (GridPackage *)mCostumeGrid->PushIterator();
    CostumeHeadingPack *rHeadingPack = (CostumeHeadingPack *)mCostumeGridHeaders->PushIterator();
    while(rGridPack && rHeadingPack)
    {
        //--Determine the rendering size. This also affects color blending.
        float cBoostPct = EasingFunction::QuadraticInOut(rGridPack->mTimer, AM_CAMPFIRE_SIZE_TICKS);
        float cScale = 3.0f + (cBoostPct * AM_CAMPFIRE_SIZE_BOOST);
        float cScaleInv = 1.0f / cScale;

        //--Figure out the render position.
        float tXPos = rGridPack->mXPos;
        float tYPos = rGridPack->mYPos;

        //--The actual render position is based on the opacity percent as the object slides out of the middle.
        tXPos = tCenterX + ((tXPos - tCenterX) * tOpacitySlidePct);
        tYPos = tCenterY + ((tYPos - tCenterY) * tOpacitySlidePct);

        //--Move the render position off by the scale of the object.
        tXPos = tXPos - (cIconSize * cScale * 0.50f);
        tYPos = tYPos - (cIconSize * cScale * 0.50f);

        //--Color blending.
        float cColVal = 0.75f + (0.25f * cBoostPct);
        StarlightColor::SetMixer(cColVal, cColVal, cColVal, tOpacityPct);

        //--Render.
        glTranslatef(tXPos, tYPos, 0.0f);
        glScalef(cScale, cScale, 1.0f);
        Images.CampfireMenu.rIconFrame->Draw();
        if(rHeadingPack->rImage) rHeadingPack->rImage->Draw(0, -3);
        glScalef(cScaleInv, cScaleInv, 1.0f);
        glTranslatef(-tXPos, -tYPos, 0.0f);

        //--Next.
        rGridPack = (GridPackage *)mCostumeGrid->AutoIterate();
        rHeadingPack = (CostumeHeadingPack *)mCostumeGridHeaders->AutoIterate();
    }

    //--After the main grid loop, re-run the loop and render the text. This is to make it appear over everything else.
    rGridPack = (GridPackage *)mCostumeGrid->PushIterator();
    rHeadingPack = (CostumeHeadingPack *)mCostumeGridHeaders->PushIterator();
    while(rGridPack && rHeadingPack)
    {
        //--Determine the rendering size. This also affects color blending.
        float cBoostPct = EasingFunction::QuadraticInOut(rGridPack->mTimer, AM_CAMPFIRE_SIZE_TICKS);
        float cScale = 3.0f + (cBoostPct * AM_CAMPFIRE_SIZE_BOOST);

        //--Figure out the render position.
        float tXPos  = rGridPack->mXPos;
        float tYPos  = rGridPack->mYPos;

        //--The actual render position is based on the opacity percent as the object slides out of the middle.
        tXPos = tCenterX + ((tXPos - tCenterX) * tOpacitySlidePct);
        tYPos = tCenterY + ((tYPos - tCenterY) * tOpacitySlidePct);
        float tTextX = tXPos;
        float tYPosL = tYPos;

        //--Move the render position off by the scale of the object.
        tXPos  = tXPos  - (cIconSize * cScale * 0.50f);
        tYPos  = tYPos  - (cIconSize * cScale * 0.50f);
        tYPosL = tYPosL + (cIconSize * cScale * 0.50f) - cIconSize;

        //--Color blending.
        float cColVal = 0.75f + (0.25f * cBoostPct);
        StarlightColor::SetMixer(cColVal, cColVal, cColVal, tOpacityPct);

        //--Text.
        float cTxtScale = 1.0f + (cBoostPct * (AM_CAMPFIRE_SIZE_BOOST * 0.33f));
        Images.CampfireMenu.rMainlineFont->DrawText(tTextX, tYPos  - 15.0f, SUGARFONT_AUTOCENTER_X, cTxtScale, rHeadingPack->mCharacterName);
        Images.CampfireMenu.rMainlineFont->DrawText(tTextX, tYPosL + 10.0f, SUGARFONT_AUTOCENTER_X, cTxtScale, rHeadingPack->mFormName);

        //--Next.
        rGridPack = (GridPackage *)mCostumeGrid->AutoIterate();
        rHeadingPack = (CostumeHeadingPack *)mCostumeGridHeaders->AutoIterate();
    }

    //--[Clean]
    StarlightColor::ClearMixer();


    /*
    //--[Documenation and Setup]
    //--Render the costumes menu, assuming it's active.
    if(!Images.mIsReady) return;
    if(!mIsCostumesMenu) return;

    //--Constants.
    float cXSpacing = 150.0f;
    float cSpriteScale = 3.0f;
    float cSpriteScaleInv = 1.0f / cSpriteScale;
    float cSpriteOffX = 617.0f + 18.0f;
    float cSpriteOffY = 123.0f;
    float cSpriteOffLowerX = 617.0f + 18.0f;
    float cSpriteOffLowerY = 393.0f;
    float cNameOffX = VIRTUAL_CANVAS_X * 0.50f;
    float cNameOffY = 217.0f;
    float cFormOffX = VIRTUAL_CANVAS_X * 0.50f;
    float cFormOffY = cNameOffY + 15.0f;
    float cNameOffLowerX = VIRTUAL_CANVAS_X * 0.50f;
    float cNameOffLowerY = 487.0f;

    //--Alpha handler when this menu is fading out.
    //TODO

    //--[Header]
    //--Shows the UI title.
    Images.CostumeUI.rHeader->Draw();
    Images.CostumeUI.rHeadingFont->DrawText(VIRTUAL_CANVAS_X * 0.50f, 13.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Costume Selection");

    //--[Top: Character/Form Selection]
    //--For each heading, render a frame, sprite, and names.
    float tXOffset = mCostumeCursorUpper * -1.0f * cXSpacing;

    //--Iterate.
    CostumeHeadingPack *rHeadingPack = (CostumeHeadingPack *)mCostumeHeadingList->PushIterator();
    while(rHeadingPack)
    {
        //--Frame.
        Images.CostumeUI.rFrameUpper->Draw(tXOffset, 0.0f);

        //--Draw the associated image, if it exists, at 3x scale.
        if(rHeadingPack->rImage)
        {
            glTranslatef(cSpriteOffX + tXOffset, cSpriteOffY, 0.0f);
            glScalef(cSpriteScale, cSpriteScale, 1.0f);
            rHeadingPack->rImage->Draw();
            glScalef(cSpriteScaleInv, cSpriteScaleInv, 1.0f);
            glTranslatef(-(cSpriteOffX + tXOffset), -cSpriteOffY, 0.0f);
        }

        //--Render the character name and form name.
        Images.CostumeUI.rVSmallFont->DrawText(cNameOffX + tXOffset, cNameOffY, SUGARFONT_AUTOCENTER_X, 1.0f, rHeadingPack->mCharacterName);
        Images.CostumeUI.rVSmallFont->DrawText(cFormOffX + tXOffset, cFormOffY, SUGARFONT_AUTOCENTER_X, 1.0f, rHeadingPack->mFormName);

        //--Next.
        tXOffset = tXOffset + cXSpacing;
        rHeadingPack = (CostumeHeadingPack *)mCostumeHeadingList->AutoIterate();
    }

    //--[Bottom: Costume Selection]
    //--If the player has not yet started on the bottom selection, stop here.
    if(mCostumeCursorLower == -1) return;

    //--Make sure the associated sublist exists.
    SugarLinkedList *rAssociatedSublist = (SugarLinkedList *)mCostumeSublistList->GetElementBySlot(mCostumeCursorUpper);
    if(!rAssociatedSublist) return;

    //--Reset heading variables.
    tXOffset = mCostumeCursorLower * -1.0f * cXSpacing;

    //--For each costume, render the associated sprite and name.
    CostumePack *rCostumePack = (CostumePack *)rAssociatedSublist->PushIterator();
    while(rCostumePack)
    {
        //--Frame selected.
        if(rCostumePack->mIsSelected)
        {
            Images.CostumeUI.rFrameLowerActive->Draw(tXOffset, 0.0f);
        }
        //--Frame not-selected.
        else
        {
            Images.CostumeUI.rFrameLower->Draw(tXOffset, 0.0f);
        }

        //--Draw the associated image, if it exists, at 3x scale.
        if(rCostumePack->rImage)
        {
            glTranslatef(cSpriteOffLowerX + tXOffset, cSpriteOffLowerY, 0.0f);
            glScalef(cSpriteScale, cSpriteScale, 1.0f);
            rCostumePack->rImage->Draw();
            glScalef(cSpriteScaleInv, cSpriteScaleInv, 1.0f);
            glTranslatef(-(cSpriteOffLowerX + tXOffset), -cSpriteOffLowerY, 0.0f);
        }

        //--Draw the name of the package.
        Images.CostumeUI.rVSmallFont->DrawText(cNameOffLowerX + tXOffset, cNameOffLowerY, SUGARFONT_AUTOCENTER_X, 1.0f, rAssociatedSublist->GetIteratorName());

        //--Next.
        tXOffset = tXOffset + cXSpacing;
        rCostumePack = (CostumePack *)rAssociatedSublist->AutoIterate();
    }
    */
}
void AdventureMenu::RenderCostumeMenuSub()
{
    //--[Setup]
    //--Don't render at all if the opacity is at zero.
    if(!Images.mIsReady) return;
    if(mCostumeCharacterOpacityTimer < 1) return;

    //--Opacity percent. Determines position and fading.
    float tOpacityPct = (float)mCostumeCharacterOpacityTimer / (float)AM_CAMPFIRE_OPACITY_TICKS;

    //--[Title]
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, tOpacityPct);
    Images.CampfireMenu.rHeader->Draw(427.0f, 45.0f);
    Images.CampfireMenu.rHeadingFont->DrawText(683.0f, 57.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Select a Character");
    StarlightColor::ClearMixer();

    //--[Fast-Access Pointers]
    //--Get the sublist.
    SugarLinkedList *rSublist = (SugarLinkedList *)mCostumeGridSublists->GetElementBySlot(mCostumeCharacterIndex);
    if(!rSublist) return;

    //--[Icons]
    //--Sizes.
    float cIconSize = Images.CampfireMenu.rIconFrame->GetTrueWidth();

    //--Position the objects slide away from.
    float tOpacitySlidePct = EasingFunction::QuadraticOut(mCostumeCharacterOpacityTimer, AM_CAMPFIRE_OPACITY_TICKS);
    float tCenterX = AM_GRID_CENT_X;
    float tCenterY = AM_GRID_CENT_Y;

    //--Iterate across the list.
    GridPackage *rGridPack = (GridPackage *)mCostumeCharacterGrid->PushIterator();
    CostumePack *rCostumePack = (CostumePack *)rSublist->PushIterator();
    while(rGridPack && rCostumePack)
    {
        //--Determine the rendering size. This also affects color blending.
        float cBoostPct = EasingFunction::QuadraticInOut(rGridPack->mTimer, AM_CAMPFIRE_SIZE_TICKS);
        float cScale = 3.0f + (cBoostPct * AM_CAMPFIRE_SIZE_BOOST);
        float cScaleInv = 1.0f / cScale;

        //--Figure out the render position.
        float tXPos = rGridPack->mXPos;
        float tYPos = rGridPack->mYPos;

        //--The actual render position is based on the opacity percent as the object slides out of the middle.
        tXPos = tCenterX + ((tXPos - tCenterX) * tOpacitySlidePct);
        tYPos = tCenterY + ((tYPos - tCenterY) * tOpacitySlidePct);

        //--Move the render position off by the scale of the object.
        tXPos = tXPos - (cIconSize * cScale * 0.50f);
        tYPos = tYPos - (cIconSize * cScale * 0.50f);

        //--Color blending.
        float cColVal = 0.75f + (0.25f * cBoostPct);
        StarlightColor::SetMixer(cColVal, cColVal, cColVal, tOpacityPct);

        //--Render.
        glTranslatef(tXPos, tYPos, 0.0f);
        glScalef(cScale, cScale, 1.0f);
        Images.CampfireMenu.rIconFrame->Draw();
        if(rCostumePack->rImage) rCostumePack->rImage->Draw(0, -3);
        glScalef(cScaleInv, cScaleInv, 1.0f);
        glTranslatef(-tXPos, -tYPos, 0.0f);

        //--Next.
        rGridPack = (GridPackage *)mCostumeCharacterGrid->AutoIterate();
        rCostumePack = (CostumePack *)rSublist->AutoIterate();
    }

    //--After the main grid loop, re-run the loop and render the text. This is to make it appear over everything else.
    rGridPack = (GridPackage *)mCostumeCharacterGrid->PushIterator();
    rCostumePack = (CostumePack *)rSublist->PushIterator();
    while(rGridPack && rCostumePack)
    {
        //--Determine the rendering size. This also affects color blending.
        float cBoostPct = EasingFunction::QuadraticInOut(rGridPack->mTimer, AM_CAMPFIRE_SIZE_TICKS);
        float cScale = 3.0f + (cBoostPct * AM_CAMPFIRE_SIZE_BOOST);

        //--Figure out the render position.
        float tXPos = rGridPack->mXPos;
        float tYPos = rGridPack->mYPos;

        //--The actual render position is based on the opacity percent as the object slides out of the middle.
        tXPos = tCenterX + ((tXPos - tCenterX) * tOpacitySlidePct);
        tYPos = tCenterY + ((tYPos - tCenterY) * tOpacitySlidePct);
        float tTextX = tXPos;

        //--Move the render position off by the scale of the object.
        tXPos = tXPos - (cIconSize * cScale * 0.50f);
        tYPos = tYPos - (cIconSize * cScale * 0.50f);

        //--Color blending.
        float cColVal = 0.75f + (0.25f * cBoostPct);
        StarlightColor::SetMixer(cColVal, cColVal, cColVal, tOpacityPct);

        //--Text.
        float cTxtScale = 1.0f + (cBoostPct * (AM_CAMPFIRE_SIZE_BOOST * 0.33f));
        Images.CampfireMenu.rMainlineFont->DrawText(tTextX, tYPos - 15.0f, SUGARFONT_AUTOCENTER_X, cTxtScale, rSublist->GetIteratorName());

        //--Next.
        rGridPack = (GridPackage *)mCostumeCharacterGrid->AutoIterate();
        rCostumePack = (CostumePack *)rSublist->AutoIterate();
    }

    //--[Clean]
    StarlightColor::ClearMixer();
}
