//--Base
#include "AdventureMenu.h"

//--Classes
#include "AdventureDebug.h"
#include "AdventureLevel.h"
#include "StringEntry.h"
#include "RunningMinigame.h"

//--CoreClasses
//--Definitions
//--Libraries
//--Managers
#include "AudioManager.h"
#include "LuaManager.h"

//--[System]
void AdventureMenu::SetToPasswordMode()
{
    //--Activates password mode. Clears the string entry form.
    mIsPasswordEntry = true;
    mStringEntryForm->SetCompleteFlag(false);
    mStringEntryForm->SetPromptString("Enter Password");
    mStringEntryForm->SetString(".");
}

//--[Update]
void AdventureMenu::UpdatePasswordMode()
{
    //--[Timers]
    //--If in password mode, increment this timer.
    if(mIsPasswordEntry)
    {
        if(mPasswordOpacity < AM_CAMPFIRE_OPACITY_TICKS) mPasswordOpacity ++;
    }
    //--Decrement and stop if not.
    else
    {
        if(mPasswordOpacity > 0) mPasswordOpacity --;
        return;
    }

    //--Run the update.
    mStringEntryForm->Update();

    //--Check for completion.
    if(mStringEntryForm->IsComplete())
    {
        //--Flag.
        mIsPasswordEntry = false;
        xHandledPassword = false;

        //--Get the string entered. It must be at least one character long.
        const char *rEnteredString = mStringEntryForm->GetString();
        if(strlen(rEnteredString) < 1) return;

        //--First, debug-mode is hard coded.
        if(!strcasecmp(rEnteredString, "Demantis Mode"))
        {
            AdventureDebug::xManualActivation = true;
            AudioManager::Fetch()->PlaySound("Menu|Save");
            return;
        }
        //--Play the running minigame!
        else if(!strcasecmp(rEnteredString, "Leg It, 55!"))
        {
            Hide();
            AudioManager::Fetch()->PlaySound("Menu|Save");
            RunningMinigame *nMinigame = new RunningMinigame();
            nMinigame->Construct();
            nMinigame->AssemblePlayerImages();
            nMinigame->GenerateLevel();
            nMinigame->Activate();
            nMinigame->SetHoldingPattern(true);
            AdventureLevel::Fetch()->ReceiveRunningMinigame(nMinigame);
            return;
        }

        //--Next, send the instruction to the costume handler. It will handle any unlocking.
        if(xCostumeResolveScript)
        {
            //--Execute.
            LuaManager::Fetch()->ExecuteLuaFile(xCostumeResolveScript, 1, "S", rEnteredString);

            //--If this flag is flipped on, the file handled the password.
            if(xHandledPassword)
            {
                AudioManager::Fetch()->PlaySound("Menu|Save");
                return;
            }
        }

        //--If we got this far, the checks failed and nothing worked.
        AudioManager::Fetch()->PlaySound("Menu|Failed");
    }
    else if(mStringEntryForm->IsCancelled())
    {
        mIsPasswordEntry = false;
    }
}

//--[Render]
void AdventureMenu::RenderPasswordMode()
{
    //--[Setup]
    //--Don't render at all if the opacity is at zero.
    if(mPasswordOpacity < 1) return;
    if(!Images.mIsReady) return;

    //--Opacity percent. Determines position and fading.
    float tOpacityPct = (float)mPasswordOpacity / (float)AM_CAMPFIRE_OPACITY_TICKS;

    //--Render password entry.
    mStringEntryForm->Render(tOpacityPct);
}
