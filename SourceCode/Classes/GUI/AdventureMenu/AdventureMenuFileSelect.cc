//--Base
#include "AdventureMenu.h"

//--Classes
#include "AdvCombat.h"
#include "StringEntry.h"
#include "VisualLevel.h"

//--CoreClasses
#include "FlexMenu.h"
#include "SugarBitmap.h"
#include "SugarFont.h"
#include "SugarLinkedList.h"
#include "VirtualFile.h"

//--Definitions
#include "EasingFunctions.h"
#include "HitDetection.h"
#include "Subdivide.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "SaveManager.h"

//--[Forward Declarations]
void RenderLineBounded(float pLft, float pTop, float pWidth, SugarFont *pFont, const char *pString);
void RenderTextConditionalColor(SugarFont *pFont, float pX, float pY, int pFlags, float pSize, const char *pText, bool pConditional, StarlightColor pTrueColor, StarlightColor pFalseColor);

//--[Local Definitions]
//--Save Grid Size
#define GRID_SIZE_X 5
#define GRID_SIZE_Y 5

//--Timers
#define AM_SAVE_PAGE_TICKS 10
#define AM_ARROW_BOB_CYCLE 120
#define AM_ARROW_BOB_RANGE 3.0f
#define AM_ARROW_OPACITY_TICKS 15
#define AM_DELETION_OPACITY_TICKS 10

//--Sorting Types
#define AM_SORT_SLOT 0
#define AM_SORT_SLOTREV 1
#define AM_SORT_DATEREV 2
#define AM_SORT_DATE 3
#define AM_SORT_TOTAL 4

//--[System]
void AdventureMenu::ScanExistingFiles()
{
    //--Scans across the file listing on the hard drive, checking off all the ones that do exist so they can be
    //  displayed in order. May be slow, this are a lot of hard-drive accesses.
    char tFileBuf[80];

    //--Clear data.
    mEmptySaveSlotsTotal = AM_SCAN_TOTAL;
    mSelectFileCursor = -1;
    mSelectFileOffset = 0;
    mHighestCursor = 0;
    mEditingFileCursor = -1;
    for(int i = 0; i < AM_SCAN_TOTAL; i ++) free(mLoadingPacks[i]);
    memset(mExistingFileListing, 0, sizeof(bool)          * AM_SCAN_TOTAL);
    memset(mLoadingPacks,        0, sizeof(LoadingPack *) * AM_SCAN_TOTAL);

    //--Create a temporary FlexMenu. We need it to resolve images for us.
    FlexMenu *tMenu = new FlexMenu();
    tMenu->Construct();

    //--Game Directory:
    const char *rAdventureDir = DataLibrary::Fetch()->GetGamePath("Root/Paths/System/Startup/sAdventurePath");

    //--Begin scanning. Info is placed in the slots in order from 0 to AM_SCAN_TOTAL, but everything else
    //  will use mFileListingIndex[] to sort the results.
    for(int i = 0; i < AM_SCAN_TOTAL; i ++)
    {
        //--Assemble filename.
        sprintf(tFileBuf, "%s/../../Saves/File%02i.slf", rAdventureDir, i);

        //--Check for existence.
        FILE *fInfile = fopen(tFileBuf, "rb");

        //--Flag.
        mExistingFileListing[i] = (fInfile != NULL);
        if(mExistingFileListing[i])
        {
            //--Reduce the empty saves count.
            mEmptySaveSlotsTotal --;

            //--Close the file.
            fclose(fInfile);

            //--Save the filename.
            sprintf(mFileListing[mHighestCursor], "File%02i.slf", i);

            //--Store the file info.
            mLoadingPacks[mHighestCursor] = SaveManager::Fetch()->GetSaveInfo(tFileBuf);
            tMenu->CrossloadSaveImages(mLoadingPacks[mHighestCursor]);

            //--Indicate this is selectable.
            mHighestCursor ++;
        }
        //--Empty slots appear as blank.
        else
        {
            mHighestCursor ++;
        }
    }

    //--Clean.
    delete tMenu;
}

//--[Property Queries]
int AdventureMenu::GetArraySlotFromLookups(int pLookup)
{
    //--Given a position in the visual lookups, returns the actual array position of the entry in question, or -1 on error.
    //  The files run from File00.slf to File99.slf, but the lookups change positions around to make sorting easier.
    //--The passed-in position is where we are on the grid, which goes from 0x0 to 4x4, or 0 to 24 internally.

    //--Get the grid position and add the page to it.
    int tCurrent = pLookup + mSelectFileOffset;
    if(tCurrent < 0 || tCurrent >= AM_SCAN_TOTAL) return -1;

    //--Check the index. If it's out of range, standardize it to -1.
    int tReturn = mFileListingIndex[tCurrent];
    if(tReturn < 0 || tReturn >= AM_SCAN_TOTAL) return -1;

    //--Now pass back the actual index.
    return tReturn;
}

//--[Sort Function]
//--Forward Declarations.
int QuicksortPartition(uint64_t *&sArray, int *&sIndexes, int pLo, int pHi);

//--Custom quicksort for the by-date functions.
void Quicksort(uint64_t *&sArray, int *&sIndexes, int pLo, int pHi)
{
    //--Do nothing if the indices are misaligned.
    if(pLo < pHi)
    {
        //--Get the partitioning index.
        int tPI = QuicksortPartition(sArray, sIndexes, pLo, pHi);

        //--Sort the 'halves'.
        Quicksort(sArray, sIndexes, pLo, tPI - 1); // Before tPI
        Quicksort(sArray, sIndexes, tPI + 1, pHi); // After tPI
    }
}

//--Partitioning Algorithm.
int QuicksortPartition(uint64_t *&sArray, int *&sIndexes, int pLo, int pHi)
{
    //--Pivot. Value placed at the rightmost end.
    uint64_t tPivot = sArray[pHi];

    //--Index of the smaller element, represented by i.
    uint64_t tTempVal;
    int tTempIndex;
    int i = (pLo - 1);

    //--Iterate across the array section.
    for(int j = pLo; j <= pHi - 1; j ++)
    {
        //--If the current value is smaller than the pivot, move the small index and swap it.
        if(sArray[j] < tPivot)
        {
            //--Move the smaller index.
            i ++;

            //--Swap the actual values.
            tTempVal = sArray[i];
            sArray[i] = sArray[j];
            sArray[j] = tTempVal;

            //--Also swap the index.
            tTempIndex = sIndexes[i];
            sIndexes[i] = sIndexes[j];
            sIndexes[j] = tTempIndex;
        }
    }

    //--Final swap.
    tTempVal = sArray[i+1];
    sArray[i+1] = sArray[pHi];
    sArray[pHi] = tTempVal;
    tTempIndex = sIndexes[i+1];
    sIndexes[i+1] = sIndexes[pHi];
    sIndexes[pHi] = tTempIndex;

    //--Pass back the lower element's index plus 1.
    return (i + 1);
}

//--[Core Methods]
void AdventureMenu::SortFilesBySlot()
{
    //--Sorts files by slot. This is just 0-99, no tricks at all!
    for(int i = 0; i < AM_SCAN_TOTAL; i ++) mFileListingIndex[i] = i;
}
void AdventureMenu::SortFilesBySlotRev()
{
    //--Sort by reverse slot. 99-0, nothing special.
    for(int i = 0; i < AM_SCAN_TOTAL; i ++) mFileListingIndex[i] = AM_SCAN_TOTAL-i-1;
}
void AdventureMenu::SortFilesByDate()
{
    //--Sorts files by date. More recent is given higher priority. All slots that are unoccupied
    //  are placed in order after all occupied slots.
    int tUnoccupiedSlotsTotal = 0;
    int tUnoccupiedSlots[AM_SCAN_TOTAL];
    int tOccupiedSlotsTotal = 0;
    uint64_t tLoadPackDateValues[AM_SCAN_TOTAL];

    //--Parallel array, stores the actual index of the file.
    int tActualIndexes[AM_SCAN_TOTAL];
    for(int i = 0; i < AM_SCAN_TOTAL; i ++) tActualIndexes[i] = -1;

    //--Variables.
    int tMonths = 0;
    int tDays = 0;
    int tYears = 0;
    int tHours = 0;
    int tMinutes = 0;
    int tSeconds = 0;
    uint64_t tTimeScore = 0;

    //--Scan.
    for(int i = 0; i < AM_SCAN_TOTAL; i ++)
    {
        //--If the slot is unoccupied, store its index.
        if(!mLoadingPacks[i])
        {
            tUnoccupiedSlots[tUnoccupiedSlotsTotal] = i;
            tUnoccupiedSlotsTotal ++;
            continue;
        }

        //--Slot is occupied, get its date. Convert the date into a number of seconds.
        LoadingPack *rPack = mLoadingPacks[i];
        //fprintf(stderr, "Slot %i: %s\n", i, rPack->mTimestamp);

        //--The format is MM/DD/YY for the first part. Compute how many days we have.
        tMonths = (((rPack->mTimestamp[0] - '0') * 10) + (rPack->mTimestamp[1] - '0'));
        tDays   = (((rPack->mTimestamp[3] - '0') * 10) + (rPack->mTimestamp[4] - '0'));
        tYears  = (((rPack->mTimestamp[6] - '0') * 10) + (rPack->mTimestamp[7] - '0'));
        //fprintf(stderr, " D M Y: %i %i %i\n", tDays, tMonths, tYears);

        //--Convert this to a "time" score. It's not exact, it just has to be relatively correct.
        tTimeScore = (tYears * 366) + (tMonths * 31) + tDays;
        tTimeScore = tTimeScore * (24 * 60 * 60); //Multiply by number of seconds per day.

        //--The second block is HH:MM:SS.
        tHours   = (((rPack->mTimestamp[ 9] - '0') * 10) + (rPack->mTimestamp[10] - '0'));
        tMinutes = (((rPack->mTimestamp[12] - '0') * 10) + (rPack->mTimestamp[13] - '0'));
        tSeconds = (((rPack->mTimestamp[15] - '0') * 10) + (rPack->mTimestamp[16] - '0'));
        //fprintf(stderr, " H M S: %i %i %i\n", tHours, tMinutes, tSeconds);

        //--Add the number of seconds to get our final time score.
        tTimeScore = tTimeScore + (tHours * 60 * 60) + (tMinutes * 60) + tSeconds;

        //--Final time score.
        //fprintf(stderr, " Time score: %i\n", tTimeScore);

        //--Move the counter up.
        tLoadPackDateValues[tOccupiedSlotsTotal] = tTimeScore;
        tActualIndexes[tOccupiedSlotsTotal] = i;
        tOccupiedSlotsTotal ++;
    }

    //--Put zeroes in all slots that didn't have an entry.
    for(int i = tOccupiedSlotsTotal; i < AM_SCAN_TOTAL; i ++)
    {
        tLoadPackDateValues[i] = 0;
        tActualIndexes[i] = -1;
    }

    //--Sort the occupied slots only.
    uint64_t *rArray = (uint64_t *)tLoadPackDateValues;
    int *rIndexes = (int *)tActualIndexes;
    Quicksort(rArray, rIndexes, 0, tOccupiedSlotsTotal-1);

    //--Give the lowest values to all remaining slots, and populate them with the unoccupied values, in order.
    int tSlotVal = 0;
    for(int i = tOccupiedSlotsTotal; i < AM_SCAN_TOTAL; i ++)
    {
        tLoadPackDateValues[i] = tSlotVal;
        tActualIndexes[i] = tUnoccupiedSlots[tSlotVal];
        tSlotVal ++;
    }

    //--Copy everything into the file index.
    for(int i = 0; i < AM_SCAN_TOTAL; i ++) mFileListingIndex[i] = tActualIndexes[i];
}
void AdventureMenu::SortFilesByDateRev()
{
    //--Sorts files by date, with the most recent being first. Basically just runs SortByDate() and then flips the array.
    SortFilesByDate();

    //--Scan across the array and find the first empty slot.
    int tUsedSlots = 0;
    for(int i = 0; i < AM_SCAN_TOTAL; i ++)
    {
        //--Error check just in case.
        int tFinal = mFileListingIndex[i];
        if(tFinal < 0 || tFinal >= AM_SCAN_TOTAL) continue;

        //--Mark this slot as occupied.
        tUsedSlots = i;

        //--Check this file in the actual lookups. If the file doesn't exist, stop here.
        if(!mLoadingPacks[tFinal])
        {
            tUsedSlots --;
            break;
        }
    }

    //--Now flip the positions in the array.
    for(int i = 0; i < tUsedSlots/2; i ++)
    {
        Swap(mFileListingIndex[i], mFileListingIndex[tUsedSlots-i]);
    }
}

//--[Update]
void AdventureMenu::UpdateFileSelection()
{
    //--[Timers]
    //--If in file-select mode, increment this timer.
    if(mIsSelectFileMode)
    {
        if(mSaveOpacityTimer < AM_CAMPFIRE_OPACITY_TICKS) mSaveOpacityTimer ++;
    }
    //--Decrement and stop if not.
    else
    {
        if(mSaveOpacityTimer > 0) mSaveOpacityTimer --;
        return;
    }

    //--Always run this timer.
    mArrowBobTimer = (mArrowBobTimer + 1) % AM_ARROW_BOB_CYCLE;

    //--Opacity timers for the arrows.
    if(mSelectFileOffset > 0)
    {
        if(mArrowOpacityTimerLft < AM_ARROW_OPACITY_TICKS) mArrowOpacityTimerLft ++;
    }
    else
    {
        if(mArrowOpacityTimerLft > 0) mArrowOpacityTimerLft --;
    }
    if(mSelectFileOffset < (AM_SCAN_TOTAL - (GRID_SIZE_X * GRID_SIZE_Y)))
    {
        if(mArrowOpacityTimerRgt < AM_ARROW_OPACITY_TICKS) mArrowOpacityTimerRgt ++;
    }
    else
    {
        if(mArrowOpacityTimerRgt > 0) mArrowOpacityTimerRgt --;
    }

    //--[Page Moving]
    //--When the page is scrolling, the player cannot do anything else.
    if(mIsPageMoving)
    {
        mPageMoveTimer ++;
        if(mPageMoveTimer >= AM_SAVE_PAGE_TICKS)
        {
            mIsPageMoving = false;
            if(mIsPageMovingUp)
            {
                mSelectFileOffset += (GRID_SIZE_X * GRID_SIZE_Y);
            }
            else
            {
                mSelectFileOffset -= (GRID_SIZE_X * GRID_SIZE_Y);
            }
        }
        else
        {
            return;
        }
    }

    //--[Setup]
    //--Handles controls for the save menu.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--[Note Entry]
    //--Accept the player's keypresses to edit the save note.
    if(mIsEnteringNotes)
    {
        //--Run the update.
        mStringEntryForm->Update();

        //--Check for completion.
        if(mStringEntryForm->IsComplete())
        {
            //--Flag.
            mIsEnteringNotes = false;

            //--Get the string entered. It must be at least one character long.
            const char *rEnteredString = mStringEntryForm->GetString();
            if(strlen(rEnteredString) < 1) return;

            //--If this is a new saving case:
            if(mEditingFileCursor != -1)
            {
                //--Set the current savegame name.
                SaveManager::Fetch()->SetSavegameName(rEnteredString);

                //--Game Directory:
                const char *rAdventureDir = DataLibrary::Fetch()->GetGamePath("Root/Paths/System/Startup/sAdventurePath");

                //--New savegame.
                if(mIsEditingNoteForNewSave)
                {
                    //--Set the current savegame name.
                    SaveManager::Fetch()->SetSavegameName(rEnteredString);

                    //--Write to the file.
                    char tFileBuf[80];
                    sprintf(tFileBuf, "%s/../../Saves/File%02i.slf", rAdventureDir, mEditingFileCursor);
                    SaveManager::Fetch()->SaveAdventureFile(tFileBuf);

                    //--Decrement the count of available save slots.
                    mEmptySaveSlotsTotal --;
                }
                //--Saving over an old save with a new note.
                else
                {
                    //--Write to the file. Remember the files are listed backwards.
                    char tFileBuf[80];
                    sprintf(tFileBuf, "%s/../../Saves/File%02i.slf", rAdventureDir, mEditingFileCursor);
                    SaveManager::Fetch()->SaveAdventureFile(tFileBuf);
                }

                //--SFX.
                AudioManager::Fetch()->PlaySound("Menu|Save");

                //--Exit this mode.
                mIsSelectFileMode = false;
                Hide();
            }
            //--If the file edit cursor is -1, then we're just saving over the note for that pack.
            //  This code cannot create new saves, just edit existing ones.
            else
            {
                //--Get the loading pack in question.
                LoadingPack *rPack = mLoadingPacks[mNoteSlot];

                //--If the loading pack exists, write over it:
                if(!rPack) return;

                //--Modify the string.
                strcpy(rPack->mFileName, rEnteredString);

                //--Load the file into a VirtualFile.
                VirtualFile *tVFile = new VirtualFile(rPack->mFilePath, true);

                //--Open the save file back up.
                FILE *fOutfile = fopen(rPack->mFilePath, "wb");
                if(!fOutfile) { delete tVFile; return; }

                //--Seek past the header and the loadinfo.
                tVFile->Seek(strlen("STARv101LOADINFO_"));

                //--Seek past this string.
                char *tDeleteString = tVFile->ReadLenString();
                free(tDeleteString);

                //--Write the header again.
                fwrite("STARv101LOADINFO_", sizeof(char), strlen("STARv101LOADINFO_"), fOutfile);

                //--Write the new string.
                uint16_t tLen = strlen(rEnteredString);
                fwrite(&tLen, sizeof(uint16_t), 1, fOutfile);
                fwrite(rEnteredString, sizeof(char), tLen, fOutfile);

                //--Now write everything else from the file back into this one.
                int tCursor = tVFile->GetCurPosition();
                int tLength = tVFile->GetLength();
                uint8_t *rDataBuf = (uint8_t *)tVFile->GetData();
                fwrite(&rDataBuf[tCursor+1], sizeof(uint8_t), tLength - tCursor, fOutfile);

                //--Clean up.
                delete tVFile;
                fclose(fOutfile);

                //--SFX.
                AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            }
        }
        else if(mStringEntryForm->IsCancelled())
        {
            mIsEnteringNotes = false;
            mEditingFileCursor = -1;
        }
        return;
    }

    //--[Sort Changing]
    //--Changes how the files are sorted. Doesn't change the current cursor position. Locks out the rest of the update
    //  to prevent the player accidentally pressing a key on the wrong file.
    if(rControlManager->IsFirstPress("F2"))
    {
        //--Cycle the sort value up by 1.
        mCurrentSortingType = (mCurrentSortingType + 1) % AM_SORT_TOTAL;

        //--Actually sort things.
        if(mCurrentSortingType == AM_SORT_SLOT)
        {
            SortFilesBySlot();
        }
        else if(mCurrentSortingType == AM_SORT_SLOTREV)
        {
            SortFilesBySlotRev();
        }
        else if(mCurrentSortingType == AM_SORT_DATEREV)
        {
            SortFilesByDateRev();
        }
        else if(mCurrentSortingType == AM_SORT_DATE)
        {
            SortFilesByDate();
        }

        //--Block the rest of the update.
        return;
    }

    //--[Deleting]
    //--When active, blocks the update.
    if(mSaveDeletionIndex != -1)
    {
        //--Increment the timer.
        if(mSaveDeletionOpacityTimer < AM_DELETION_OPACITY_TICKS) mSaveDeletionOpacityTimer ++;

        //--Stop updating if the timer is less than 3. This prevents accidents.
        if(mSaveDeletionOpacityTimer < 3) return;

        //--Activate will delete the file.
        if(rControlManager->IsFirstPress("Activate"))
        {
            //--Figure out which slot we're on.
            int tFileSlot = GetArraySlotFromLookups(mSaveDeletionIndex);
            if(tFileSlot == -1)
            {
                mSaveDeletionIndexOld = mSaveDeletionIndex;
                mSaveDeletionIndex = -1;
                return;
            }

            //--Resolve the path.
            const char *rAdventureDir = DataLibrary::Fetch()->GetGamePath("Root/Paths/System/Startup/sAdventurePath");
            char tFileBuf[STD_PATH_LEN];
            sprintf(tFileBuf, "%s/../../Saves/File%02i.slf", rAdventureDir, tFileSlot);
            fprintf(stderr, "File Buf: %s\n", tFileBuf);

            //--Delete the file.
            int tDeletedFile = remove(tFileBuf);

            //--File deleted correctly:
            if(!tDeletedFile)
            {
                free(mLoadingPacks[tFileSlot]);
                mLoadingPacks[tFileSlot] = NULL;
            }
            //--Error:
            else
            {
                fprintf(stderr, "Error, unable to delete file %s. Reasons unknown.\n", tFileBuf);
            }

            //--In all cases, exit out of deletion mode.
            mSaveDeletionIndexOld = mSaveDeletionIndex;
            mSaveDeletionIndex = -1;
            AudioManager::Fetch()->PlaySound("World|TranqShot");
        }
        //--Cancel returns to normal.
        else if(rControlManager->IsFirstPress("Cancel"))
        {
            mSaveDeletionIndexOld = mSaveDeletionIndex;
            mSaveDeletionIndex = -1;
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }

        //--Block the rest of the update.
        return;
    }
    //--Not deleting, decrement the timer.
    else
    {
        //--Timer.
        if(mSaveDeletionOpacityTimer > 0)
        {
            mSaveDeletionOpacityTimer --;
            if(mSaveDeletionOpacityTimer == 0) mSaveDeletionIndexOld = -1;
        }

        //--See if we want to activate deletion.
        if(rControlManager->IsFirstPress("Delete"))
        {
            //--Check the file index.
            int tFileSlot = GetArraySlotFromLookups(mSelectFileCursor);

            //--On the "New File" cursor, do nothing:
            if(mSelectFileCursor == -1)
            {
                AudioManager::Fetch()->PlaySound("Menu|Failed");
            }
            //--If the file doesn't actually exist, you can't delete it, stupid.
            else if(tFileSlot == -1)
            {
                AudioManager::Fetch()->PlaySound("Menu|Failed");
            }
            //--Otherwise, set the deletion index.
            else
            {
                mSaveDeletionIndex = mSelectFileCursor;
                AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            }
        }
    }

    //--[Directional Controls]
    //--Holding down either control key toggles this flag. When on, scroll by page.
    bool tIsCtrlDown = rControlManager->IsDown("Ctrl");

    //--Up.
    if(rControlManager->IsFirstPress("Up"))
    {
        //--When on "New File", do nothing.
        if(mSelectFileCursor == -1)
        {
        }
        //--In the first row, move to "New File".
        else if(mSelectFileCursor < GRID_SIZE_X)
        {
            mSelectFileLastColumn = mSelectFileCursor;
            mSelectFileCursor = -1;
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
        //--Otherwise, decrement by GRID_SIZE_X.
        else
        {
            mSelectFileCursor -= GRID_SIZE_X;
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
    }
    //--Down.
    if(rControlManager->IsFirstPress("Down"))
    {
        //--When on "New File", go to the mSelectFileLastColumn'th entry.
        if(mSelectFileCursor == -1)
        {
            mSelectFileCursor = mSelectFileLastColumn;
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
        //--In the first four rows, move to "New File".
        else if(mSelectFileCursor < GRID_SIZE_X * (GRID_SIZE_Y-1))
        {
            mSelectFileCursor += GRID_SIZE_X;
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
        //--Otherwise, do nothing.
        else
        {
        }
    }
    //--Left.
    if(rControlManager->IsFirstPress("Left"))
    {
        //--When on "New File", do nothing.
        if(mSelectFileCursor == -1)
        {
        }
        //--In the leftmost column, move down one page. This always occurs when Ctrl is down.
        else if(mSelectFileCursor % GRID_SIZE_X == 0 || tIsCtrlDown)
        {
            //--Page is already at 0, so do nothing.
            if(mSelectFileOffset < (GRID_SIZE_X * GRID_SIZE_Y))
            {

            }
            //--Move a page down.
            else
            {
                //--Move the cursor to the rightmost column. Doesn't occur when Ctrl is down.
                if(!tIsCtrlDown) mSelectFileCursor += (GRID_SIZE_X - 1);

                //--Flags.
                mIsPageMoving = true;
                mIsPageMovingUp = false;
                mPageMoveTimer = 0;
                AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
                return;
            }
        }
        //--Otherwise, decrement by 1.
        else
        {
            mSelectFileCursor --;
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
    }
    //--Right.
    if(rControlManager->IsFirstPress("Right"))
    {
        //--When on "New File", do nothing.
        if(mSelectFileCursor == -1)
        {
        }
        //--In the rightmost column, move up one page. This always occurs when Ctrl is down.
        else if(mSelectFileCursor % GRID_SIZE_X == GRID_SIZE_X-1 || tIsCtrlDown)
        {
            //--Page is already at max, so do nothing.
            if(mSelectFileOffset + (GRID_SIZE_X * GRID_SIZE_Y) >= AM_SCAN_TOTAL)
            {

            }
            //--Move a page up.
            else
            {
                //--Move the cursor to the leftmost column. Doesn't occur when Ctrl is down.
                if(!tIsCtrlDown) mSelectFileCursor -= (GRID_SIZE_X - 1);

                //--Flags.
                mIsPageMoving = true;
                mIsPageMovingUp = true;
                mPageMoveTimer = 0;
                AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
                return;
            }
        }
        //--Otherwise, increment by 1.
        else
        {
            mSelectFileCursor ++;
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
    }

    //--Activate, brings up the note entry form. Completing the form will save out a file.
    if(rControlManager->IsFirstPress("Activate"))
    {
        //--If the cursor is on -1, then create a new save file. Page doesn't matter, nor does the sorting order.
        if(mSelectFileCursor == -1)
        {
            //--Scan along the list of available spots. First open one gets saved to.
            for(int i = 0; i < AM_SCAN_TOTAL; i ++)
            {
                //--If this spot is occupied, ignore it.
                if(mExistingFileListing[i]) continue;

                //--Mark this for saving and activate note entry.
                mIsEditingNoteForNewSave = true;
                mIsEnteringNotes = true;
                mNoteSlot = i;
                mEditingFileCursor = i;

                //--Upload info.
                mStringEntryForm->SetCompleteFlag(false);
                mStringEntryForm->SetPromptString("Enter a note for this save file.");
                mStringEntryForm->SetString("New Save Game");
                break;
            }
        }
        //--Save over an existing file, sets flags.
        else
        {
            //--Figure out which slot we're on.
            int tFileSlot = GetArraySlotFromLookups(mSelectFileCursor);
            if(tFileSlot == -1) return;

            //--Flags.
            mIsEditingNoteForNewSave = false;
            mIsEnteringNotes = true;
            mNoteSlot = tFileSlot;
            mEditingFileCursor = tFileSlot;

            //--Upload info.
            mStringEntryForm->SetCompleteFlag(false);
            mStringEntryForm->SetPromptString("Enter a note for this save file.");
            mStringEntryForm->SetString(mLoadingPacks[mEditingFileCursor]->mFileName);
        }

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|Select");
    }
    //--Back to previous menu.
    else if(rControlManager->IsFirstPress("Cancel"))
    {
        mStartedHidingThisTick = true;
        mIsSelectFileMode = false;
        mCurrentCursor = AM_SAVE_SAVE;
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }

    //--Press F1 to edit notes on a file.
    if(rControlManager->IsFirstPress("F1"))
    {
        //--The given file may be different from the grid position based on page and sorting. Verify.
        int tFileSlot = GetArraySlotFromLookups(mSelectFileCursor);
        if(tFileSlot == -1)
        {
            AudioManager::Fetch()->PlaySound("Menu|Failed");
        }
        //--Make sure there's actually a file in the given slot.
        else if(!mLoadingPacks[tFileSlot])
        {
            AudioManager::Fetch()->PlaySound("Menu|Failed");
        }
        //--Checks passed.
        else
        {
            //--Verify.
            mEditingFileCursor = -1;
            mNoteSlot = tFileSlot;
            if(mNoteSlot >= 0 && mLoadingPacks[mNoteSlot])
            {
                //--Flags.
                mIsEnteringNotes = true;

                //--Upload info.
                mStringEntryForm->SetCompleteFlag(false);
                mStringEntryForm->SetPromptString("Enter a note for this save file.");
                mStringEntryForm->SetString(mLoadingPacks[mNoteSlot]->mFileName);
            }
        }
    }
}

//--[Rendering]
void AdventureMenu::RenderFileSelection()
{
    //--[Setup]
    //--Don't render at all if the opacity is at zero.
    if(mSaveOpacityTimer < 1) return;
    if(!Images.mIsReady) return;

    //--Opacity percent. Determines position and fading.
    float tOpacityPct = (float)mSaveOpacityTimer / (float)AM_CAMPFIRE_OPACITY_TICKS;

    //--[Title]
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, tOpacityPct);
    Images.CampfireMenu.rHeader->Draw(427.0f, 19.0f);
    Images.CampfireMenu.rHeadingFont->DrawText(683.0f, 31.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Select a Save");

    //--[New Save Button]
    //--Appears at the top of the UI regardless of page.
    float cColor = 1.0f;
    if(mSelectFileCursor != -1) cColor = 0.5f;
    StarlightColor::SetMixer(cColor, cColor, cColor, tOpacityPct);

    //--Backing and Text.
    Images.FileSelectUI.rNewFileButton->Draw();
    if(mEmptySaveSlotsTotal > 0)
    {
        Images.CampfireMenu.rHeadingFont->DrawText(683.0f, 105.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Create New Savefile");
    }
    //--No save slots open!
    else
    {
        Images.CampfireMenu.rHeadingFont->DrawText(683.0f, 105.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "No Empty Save Slots Available");
    }

    //--[Body]
    //--Sizes.
    float cSpaceX = 256.0f;
    float cSpaceY = 113.0f;
    float cStartX = (VIRTUAL_CANVAS_X * 0.50f) - (236.0f * 2.5f) - (20.0f * 2.0f);
    float cStartY = (VIRTUAL_CANVAS_Y * 0.30f) - 50.0f;

    //--Variables.
    int tXPos = 0;
    int tYPos = 0;

    //--When moving pages, run a translation action.
    float cTranslateX = 0.0f;
    if(mIsPageMoving)
    {
        //--Compute the offset required.
        float cTranslatePct = EasingFunction::QuadraticInOut(mPageMoveTimer, AM_SAVE_PAGE_TICKS);
        cTranslateX = VIRTUAL_CANVAS_X * cTranslatePct * -1.0f;
        if(!mIsPageMovingUp)
        {
            cTranslateX = cTranslateX * -1.0f;
        }

        //--Run a translation action.
        glTranslatef(cTranslateX, 0.0f, 0.0f);
    }

    //--[Normal Handling]
    //--Render all the blocks on the page.
    if(mSaveDeletionIndex == -1 && mSaveDeletionIndexOld == -1)
    {
        //--Render blocks for each save file that exists, counting up.
        for(int i = 0; i < GRID_SIZE_X * GRID_SIZE_Y; i ++)
        {
            //--Positions.
            float cX = cStartX + (tXPos * cSpaceX);
            float cY = cStartY + (tYPos * cSpaceY);

            //--Subroutine.
            RenderFileBlock(i, cX, cY, mSelectFileCursor == i, tOpacityPct);

            //--Increment position cursors.
            tXPos ++;
            if(tXPos >= GRID_SIZE_X)
            {
                tXPos = 0;
                tYPos ++;
                if(tYPos >= GRID_SIZE_Y) break;
            }
        }
    }
    //--Render the blocks in the back, fading out. The block up for deletion moves to the screen's center.
    else if(mSaveDeletionIndex != -1)
    {
        //--Modify the opacity.
        float tDeletionOpacity = EasingFunction::QuadraticInOut(mSaveDeletionOpacityTimer, AM_DELETION_OPACITY_TICKS);
        float tModOpacity = 1.0f - (tOpacityPct * tDeletionOpacity);

        //--Render blocks for each save file that exists, counting up.
        for(int i = 0; i < GRID_SIZE_X * GRID_SIZE_Y; i ++)
        {
            //--Normal blocks:
            if(mSaveDeletionIndex != i)
            {
                //--Positions.
                float cX = cStartX + (tXPos * cSpaceX);
                float cY = cStartY + (tYPos * cSpaceY);

                //--Subroutine.
                RenderFileBlock(i, cX, cY, mSelectFileCursor == i, tModOpacity);
            }
            //--Deletion block:
            else
            {
                //--Positions. This is where the block starts rendering.
                float cX = cStartX + (tXPos * cSpaceX);
                float cY = cStartY + (tYPos * cSpaceY);
                float cDestX = cStartX + (2.0f * cSpaceX);
                float cDestY = cStartY + (1.0f * cSpaceY);

                //--Move it towards the end position.
                float tRenderX = cX + ((cDestX - cX) * tDeletionOpacity);
                float tRenderY = cY + ((cDestY - cY) * tDeletionOpacity);

                //--Subroutine.
                RenderFileBlock(i, tRenderX, tRenderY, mSelectFileCursor == i, tOpacityPct);
            }

            //--Increment position cursors.
            tXPos ++;
            if(tXPos >= GRID_SIZE_X)
            {
                tXPos = 0;
                tYPos ++;
                if(tYPos >= GRID_SIZE_Y) break;
            }
        }

        //--Onscreen text.
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, tDeletionOpacity);
        Images.FileSelectUI.rMainlineFont->DrawText(VIRTUAL_CANVAS_X * 0.50f, VIRTUAL_CANVAS_Y * 0.55f +  0.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Really Delete This File?");
        StarlightColor::ClearMixer();
    }
    //--Save deletion index is -1, but the old index is not. The block scrolls back to its original position.
    else
    {
        //--Modify the opacity.
        float tDeletionOpacity = EasingFunction::QuadraticInOut(mSaveDeletionOpacityTimer, AM_DELETION_OPACITY_TICKS);
        float tModOpacity = 1.0f - (tOpacityPct * tDeletionOpacity);

        //--Render blocks for each save file that exists, counting up.
        for(int i = 0; i < GRID_SIZE_X * GRID_SIZE_Y; i ++)
        {
            //--Normal blocks:
            if(mSaveDeletionIndex != i)
            {
                //--Positions.
                float cX = cStartX + (tXPos * cSpaceX);
                float cY = cStartY + (tYPos * cSpaceY);

                //--Subroutine.
                RenderFileBlock(i, cX, cY, mSelectFileCursor == i, tModOpacity);
            }
            //--Deletion block:
            else
            {
                //--Positions. This is where the block starts rendering.
                float cX = cStartX + (tXPos * cSpaceX);
                float cY = cStartY + (tYPos * cSpaceY);
                float cDestX = cStartX + (2.0f * cSpaceX);
                float cDestY = cStartY + (1.0f * cSpaceY);

                //--Move it towards the end position.
                float tRenderX = cX + ((cDestX - cX) * tDeletionOpacity);
                float tRenderY = cY + ((cDestY - cY) * tDeletionOpacity);

                //--Subroutine.
                RenderFileBlock(i, tRenderX, tRenderY, mSelectFileCursor == i, tOpacityPct);
            }

            //--Increment position cursors.
            tXPos ++;
            if(tXPos >= GRID_SIZE_X)
            {
                tXPos = 0;
                tYPos ++;
                if(tYPos >= GRID_SIZE_Y) break;
            }
        }

        //--Onscreen text.
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, tDeletionOpacity);
        Images.FileSelectUI.rMainlineFont->DrawText(VIRTUAL_CANVAS_X * 0.50f, VIRTUAL_CANVAS_Y * 0.55f +  0.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Really Delete This File?");
        StarlightColor::ClearMixer();
    }

    //--[Page Swap Rendering]
    //--When changing pages, render an additional set of blocks and offset them.
    if(mIsPageMoving)
    {
        //--Reset these variables.
        tXPos = 0;
        tYPos = 0;
        float cXOffset = VIRTUAL_CANVAS_X;

        //--Figure out the index offset.
        int tIndexOffset = (GRID_SIZE_X * GRID_SIZE_Y);
        if(!mIsPageMovingUp)
        {
            tIndexOffset = tIndexOffset * -1;
            cXOffset = cXOffset * -1.0f;
        }

        //--Render with an index offset.
        for(int i = 0; i < GRID_SIZE_X * GRID_SIZE_Y; i ++)
        {
            //--Positions.
            float cX = cStartX + (tXPos * cSpaceX);
            float cY = cStartY + (tYPos * cSpaceY);

            //--Subroutine.
            RenderFileBlock(i+tIndexOffset, cX + cXOffset, cY, mSelectFileCursor == i, tOpacityPct);

            //--Increment position cursors.
            tXPos ++;
            if(tXPos >= GRID_SIZE_X)
            {
                tXPos = 0;
                tYPos ++;
                if(tYPos >= GRID_SIZE_Y) break;
            }
        }

        //--Clean up.
        glTranslatef(-cTranslateX, 0.0f, 0.0f);
    }

    //--[Arrows]
    //--Compute opacities.
    float tArrowLftOpacity = mArrowOpacityTimerLft / (float)AM_ARROW_OPACITY_TICKS;
    float tArrowRgtOpacity = mArrowOpacityTimerRgt / (float)AM_ARROW_OPACITY_TICKS;

    //--Compute bobbing offset.
    float cBobPct = mArrowBobTimer / (float)AM_ARROW_BOB_CYCLE;
    float cBobX = sinf(cBobPct * 3.1415926f * 2.0f) * AM_ARROW_BOB_RANGE;

    //--Render left.
    if(tArrowLftOpacity > 0.0f)
    {
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, tOpacityPct * tArrowLftOpacity);
        Images.FileSelectUI.rArrowLft->Draw(cBobX, 0.0f);
    }
    if(tArrowRgtOpacity > 0.0f)
    {
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, tOpacityPct * tArrowRgtOpacity);
        Images.FileSelectUI.rArrowRgt->Draw(-cBobX, 0.0f);
    }

    //--[Clean]
    StarlightColor::ClearMixer();

    //--[Entering Notes]
    //--Notes appear over everything else.
    if(mIsEnteringNotes)
    {
        mStringEntryForm->Render();
        return;
    }

    //--[Instructions]
    //--Basics.
    Images.FileSelectUI.rMainlineFont->DrawText(10.0f, 10.0f, 0, 1.0f, "F1: Edit Note");
    Images.FileSelectUI.rMainlineFont->DrawText(10.0f, 30.0f, 0, 1.0f, "F2: Cycle Sorting Types");

    //--Indicate which kind of sort is active.
    if(mCurrentSortingType == AM_SORT_SLOT)
    {
        Images.FileSelectUI.rMainlineFont->DrawText(10.0f, 50.0f, 0, 1.0f, "Current Sort: Lowest Slot First");
    }
    else if(mCurrentSortingType == AM_SORT_SLOTREV)
    {
        Images.FileSelectUI.rMainlineFont->DrawText(10.0f, 50.0f, 0, 1.0f, "Current Sort: Highest Slot First");
    }
    else if(mCurrentSortingType == AM_SORT_DATE)
    {
        Images.FileSelectUI.rMainlineFont->DrawText(10.0f, 50.0f, 0, 1.0f, "Current Sort: Oldest Save First");
    }
    else if(mCurrentSortingType == AM_SORT_DATEREV)
    {
        Images.FileSelectUI.rMainlineFont->DrawText(10.0f, 50.0f, 0, 1.0f, "Current Sort: Newest Save First");
    }

    //--Right side. Hold Ctrl down.
    Images.FileSelectUI.rMainlineFont->DrawText(VIRTUAL_CANVAS_X - 300.0f, 10.0f, 0, 1.0f, "Hold Ctrl: Scroll By Page");
    Images.FileSelectUI.rMainlineFont->DrawText(VIRTUAL_CANVAS_X - 300.0f, 30.0f, 0, 1.0f, "Del: Delete Selected File");
}
void AdventureMenu::RenderFileBlock(int pIndex, float pLft, float pTop, bool pIsHighlighted, float pOpacityPct)
{
    //--[Documentation and Setup]
    //--Given an index, renders the file information for that load information pack. Note that this is the RAW file
    //  position in the original array, NOT the lookup index. Depending on the sorting, the lookup index may not
    //  be the same as the array position.
    //--Note: This may modify the color mixer. Clear the mixer when you are done.
    int tFileSlot = GetArraySlotFromLookups(pIndex);
    if(tFileSlot < 0 || tFileSlot >= AM_SCAN_TOTAL) return;

    //--Fast-access pointer.
    SugarFont *rUseFont = Images.StatusUI.rVerySmallFont;

    //--Color.
    float cColor = 1.0f;
    if(!pIsHighlighted) cColor = 0.5f;
    StarlightColor::SetMixer(cColor, cColor, cColor, pOpacityPct);

    //--[Base]
    //--Background.
    Images.FileSelectUI.rGridBackingCh0->Draw(pLft, pTop);

    //--[System]
    //--Empty spots now get represented as blank cards with "Empty Slot" on them.
    if(!mLoadingPacks[tFileSlot])
    {
        //--Empty slot indicator.
        rUseFont->DrawTextArgs(pLft + 118.0f, pTop + 54.0f, SUGARFONT_AUTOCENTER_XY, 1.0f, "Empty Slot");

        //--File number if this slot were to be occupied.
        rUseFont->DrawTextArgs(pLft + 5.0f, pTop + 3.0f, 0, 1.0f, "File%02i.slf", tFileSlot);
        return;
    }

    //--[Text]
    //--File number.
    rUseFont->DrawTextArgs(pLft + 5.0f, pTop + 3.0f, 0, 1.0f, mLoadingPacks[tFileSlot]->mFilePathShort);

    //--File note. First, figure out if it needs to be a two-line now.
    char tTimestampA[32];
    char tTimestampB[32];
    float tNoteLen = rUseFont->GetTextWidth(mLoadingPacks[tFileSlot]->mFileName) * 1.0f;
    if(tNoteLen > 220.0f)
    {
        //--Setup.
        int tCharsParsed = 0;
        int tCharsParsedTotal = 0;
        float tCurY = pTop + 75.0f;
        int tStringLen = (int)strlen(mLoadingPacks[tFileSlot]->mFileName);

        //--Loop until the whole note has been parsed out.
        while(tCharsParsedTotal < tStringLen)
        {
            //--Run the subdivide.
            char *tDescriptionLine = Subdivide::SubdivideString(tCharsParsed, &mLoadingPacks[tFileSlot]->mFileName[tCharsParsedTotal], -1, 220.0f, rUseFont, 1.0f);
            rUseFont->DrawText(pLft + 5.0f, tCurY, 0, 1.0f, tDescriptionLine);
            tCurY = tCurY + 13.0f;

            //--Move to the next line.
            tCharsParsedTotal += tCharsParsed;

            //--Clean up.
            free(tDescriptionLine);
        }
    }
    //--One-line.
    else
    {
        rUseFont->DrawText(pLft + 5.0f, pTop + 88.0f, 0, 1.0f, mLoadingPacks[tFileSlot]->mFileName);
    }

    //--Split the timestamp into two parts.
    int tLen = (int)strlen(mLoadingPacks[tFileSlot]->mTimestamp);
    for(int p = 0; p < tLen; p ++)
    {
        if(mLoadingPacks[tFileSlot]->mTimestamp[p] == ' ')
        {
            strcpy(tTimestampB, &mLoadingPacks[tFileSlot]->mTimestamp[p+1]);
            break;
        }
        else
        {
            tTimestampA[p+0] = mLoadingPacks[tFileSlot]->mTimestamp[p];
            tTimestampA[p+1] = '\0';
        }
    }

    //--Timestamp.
    float tTimestampLen = rUseFont->GetTextWidth(tTimestampA) * 1.0f;
    rUseFont->DrawText(pLft + 233.0f - tTimestampLen, pTop + 3.0f, 0, 1.0f, tTimestampA);
    tTimestampLen = rUseFont->GetTextWidth(tTimestampB) * 1.0f;
    rUseFont->DrawText(pLft + 233.0f - tTimestampLen, pTop + 23.0f, 0, 1.0f, tTimestampB);

    //--[Party]
    //--Render all party members. Some might not exist.
    for(int p = 0; p < 4; p ++)
    {
        //--Skip empty slots.
        if(!mLoadingPacks[tFileSlot]->rRenderImg[p]) continue;

        //--Positions.
        float cRenderX = pLft - 12.0f + (p * 32.0f);
        float cRenderY = pTop +  9.0f;

        //--Render.
        glTranslatef(cRenderX, cRenderY, 0.0f);
        glScalef(2.0f, 2.0f, 1.0f);
        mLoadingPacks[tFileSlot]->rRenderImg[p]->Draw();
        glScalef(0.5f, 0.5f, 1.0f);
        glTranslatef(-cRenderX, -cRenderY, 0.0f);
    }
}
