//--Base
#include "AdventureMenu.h"

//--Classes
#include "AdvCombat.h"
#include "AdvCombatEntity.h"
#include "AdventureItem.h"
#include "AdventureInventory.h"
#include "AdventureLevel.h"
#include "VisualLevel.h"

//--CoreClasses
#include "SugarAutoBuffer.h"
#include "SugarBitmap.h"
#include "SugarFont.h"
#include "SugarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "Subdivide.h"

//--Libraries
//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "DisplayManager.h"
#include "LuaManager.h"

//--[Shop Fading]
#define FADE_MODE_NONE 0
#define FADE_INITIAL_OUT 1
#define FADE_INITIAL_HOLD 2
#define FADE_INITIAL_IN 3
#define FADE_FINAL_OUT 4
#define FADE_FINAL_HOLD 5
#define FADE_FINAL_IN 6

#define FADE_TICKS_OUT 15
#define FADE_TICKS_HOLD 30
#define FADE_TICKS_IN 15

//--[System]
//--[Manipulators]
//--[Core Methods]
//--[Update]
void AdventureMenu::UpdateShopSell()
{
    //--[Setup]
    //--Quick-access pointers.
    ControlManager *rControlManager = ControlManager::Fetch();
    AdventureInventory *rInventory = AdventureInventory::Fetch();
    SugarLinkedList *rItemList = rInventory->GetItemList();
    if(!rItemList) return;

    //--Storage.
    int tOldCursor = mShopCursor;

    //--Always run this timer.
    mCharacterWalkTimer ++;

    //--[Exchange Mode]
    //--In exchanging mode, prompt the player.
    if(mIsExchanging)
    {
        //--If the player pushes "Activate", they will buy the item.
        if(rControlManager->IsFirstPress("Activate"))
        {
            //--Remove the item from the player's inventory.
            AdventureItem *rSoldItem = (AdventureItem *)rItemList->GetElementBySlot(mShopCursor);
            rInventory->LiberateItemP(rSoldItem);

            //--Register it to the shop's inventory.
            mShopInventory->AddElementAsTail("X", rSoldItem, &RootObject::DeleteThis);

            //--Increment the player's cash.
            rInventory->SetPlatina(rInventory->GetPlatina() + rSoldItem->GetValue());

            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|BuyOrSell");

            //--No items left to sell, cancel out of sell mode.
            if(rItemList->GetListSize() < 1)
            {
                mShopCursor = 1;
                mShopModeType = AM_SELECT_SHOP_MODE;
            }
            //--Recheck the cursor.
            else if(mShopCursor >= rItemList->GetListSize())
            {
                mShopCursor = rItemList->GetListSize() - 1;
            }

            //--All cases end the exchange.
            mIsExchanging = false;
        }
    }
    //--Selecting items to sell.
    else
    {
        //--Down increases the cursor.
        if(rControlManager->IsFirstPress("Down"))
        {
            //--Increment if in range.
            mShopCursor ++;
            if(mShopCursor >= rInventory->GetItemCount()) mShopCursor = rItemList->GetListSize() - 1;
            if(mShopCursor < 0) mShopCursor = 0;
        }
        //--Up decreases the cursor.
        else if(rControlManager->IsFirstPress("Up"))
        {
            //--Increment if in range.
            mShopCursor --;
            if(mShopCursor < 0) mShopCursor = 0;
        }

        //--Get the item currently highlighted.
        SugarLinkedList *rItemList = rInventory->GetItemList();
        AdventureItem *rDescriptionItem = (AdventureItem *)rItemList->GetElementBySlot(mShopCursor);

        //--Left moves the comparison cursor down one.
        if(rControlManager->IsFirstPress("Left"))
        {
            //--Get the item under consideration.
            if(!rDescriptionItem) return;

            //--Setup.
            AdvCombat *rAdventureCombat = AdvCombat::Fetch();
            int cRosterSize = rAdventureCombat->GetActivePartyCount();

            //--Attempt to decrement by one until a valid character is found.
            for(int i = 0; i < cRosterSize - 1; i ++)
            {
                //--Compute slot.
                int tSlot = (mComparisonCharacter - i - 1) % cRosterSize;

                //--Get the character.
                AdvCombatEntity *rCharacter = rAdventureCombat->GetActiveMemberI(tSlot);
                if(!rCharacter) continue;

                //--If it's usable by this character, stop here.
                if(rDescriptionItem->IsEquippableBy(rCharacter->GetName()))
                {
                    AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
                    mComparisonCharacter = tSlot;
                    return;
                }
            }
        }
        //--Right moves the comparison cursor up one.
        else if(rControlManager->IsFirstPress("Right"))
        {
            //--Get the item under consideration.
            if(!rDescriptionItem) return;

            //--Setup.
            AdvCombat *rAdventureCombat = AdvCombat::Fetch();
            int cRosterSize = rAdventureCombat->GetActivePartyCount();

            //--Attempt to increment the cursor until a valid character is found.
            for(int i = 0; i < cRosterSize - 1; i ++)
            {
                //--Compute slot.
                int tSlot = (mComparisonCharacter + i + 1) % cRosterSize;

                //--Get the character.
                AdvCombatEntity *rCharacter = rAdventureCombat->GetActiveMemberI(tSlot);
                if(!rCharacter) continue;

                //--If it's usable by this character, stop here.
                if(rDescriptionItem->IsEquippableBy(rCharacter->GetName()))
                {
                    AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
                    mComparisonCharacter = tSlot;
                    return;
                }
            }
        }

        //--Activate will sell the item.
        if(rControlManager->IsFirstPress("Activate"))
        {
            //--Check the item.
            SugarLinkedList *rItemList = rInventory->GetItemList();
            AdventureItem *rSellingItem = (AdventureItem *)rItemList->GetElementBySlot(mShopCursor);

            //--If the item is a key-item, don't allow them to sell it.
            if(rSellingItem && !rSellingItem->IsKeyItem())
            {
                mIsExchanging = true;
                AudioManager::Fetch()->PlaySound("Menu|Select");
            }
            //--Otherwise, play a sound effect to indicate failure.
            else
            {
                AudioManager::Fetch()->PlaySound("Menu|Failed");
            }
        }
    }

    //--SFX, and check the comparison cursor if the item changed.
    if(tOldCursor != mShopCursor)
    {
        //--[SFX]
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");

        //--[Recentering]
        //--On a decrement:
        if(mShopCursor < tOldCursor)
        {
            //--Get where the cursor is on the page.
            int tPosition = mShopCursor - mShopSkip;
            if(tPosition < 2) mShopSkip = mShopCursor - 2;

            //--Clamp.
            if(mShopSkip < 0) mShopSkip = 0;
        }
        //--On an increment:
        else if(mShopCursor > tOldCursor)
        {
            //--Get where the cursor is on the page.
            int tPosition = mShopCursor - mShopSkip;
            if(tPosition > AM_SHOP_ITEMS_PER_PAGE - 3) mShopSkip = mShopCursor - (AM_SHOP_ITEMS_PER_PAGE - 3);

            //--Clamp.
            if(mShopSkip > rItemList->GetListSize() - AM_SHOP_ITEMS_PER_PAGE) mShopSkip = rItemList->GetListSize() - AM_SHOP_ITEMS_PER_PAGE;
        }

        //--[Rechecking Comparison Character]
        //--Flag.
        int tPreviousComparison = mComparisonCharacter;
        mMultipleComparisonCharacters = false;

        //--Get the item under consideration.
        SugarLinkedList *rItemList = rInventory->GetItemList();
        AdventureItem *rDescriptionItem = (AdventureItem *)rItemList->GetElementBySlot(mShopCursor);
        if(!rDescriptionItem) return;

        //--If the item is not a piece of equipment, we can't compare it to anything.
        if(!rDescriptionItem->IsEquipment()) return;

        //--Otherwise, start at character zero and run through the list.
        int tTotalCharacters = 0;
        /*
        for(int i = 0; i < AC_PARTY_MAX; i ++)
        {
            //--Get the character.
            AdventureCombatEntity *rCharacter = AdventureCombatUI::Fetch()->GetPartyMember(i);
            if(!rCharacter) continue;

            //--If it's usable by this character, stop here, flag.
            if(rDescriptionItem->IsEquippableBy(rCharacter->GetName()))
            {
                //--Flag. The first character found becomes the comparison character.
                if(tTotalCharacters == 0) mComparisonCharacter = i;

                //--The previous character, even if not the first, becomes the comparison character. This way
                //  the UI does not switch comparison characters if still valid.
                if(tPreviousComparison == i) mComparisonCharacter = i;

                //--Track the total number of comparable characters.
                tTotalCharacters ++;
            }
        }
        */

        //--If the total usable characters is more than 1, set this flag.
        if(tTotalCharacters > 1) mMultipleComparisonCharacters = true;
    }
}

//--[Drawing]
void AdventureMenu::RenderShopSell(bool pIsActiveMode, float pColorMix)
{
    //--[Documentation and Setup]
    //--Renders the player's inventory and the values of items therein.
    if(!Images.mIsReady) return;

    //--Fast-access pointers.
    SugarLinkedList *rItemList = AdventureInventory::Fetch()->GetItemList();
    if(!rItemList) return;

    //--[Shop Inventory Listing]
    //--Similar to how the inventory renders information as a table. Item properties are not listed, though.
    float cHeaderY = 124.0f;
    float cIconX  = 30.0f;
    float cIconW  = 22.0f;
    float cFaceX = cIconX + cIconW;
    float cFaceW = 32.0f;
    float cNameX  = cFaceX + cFaceW;
    float cValueX = 470.0f;
    float cValueXRight = 542.0f;
    float cGemsX = 555.0f;

    //--Text headers.
    StarlightColor::SetMixer(pColorMix, pColorMix, pColorMix, 1.0f);
    Images.EquipmentUI.rHeadingFont->DrawText(cIconX,  cHeaderY, 0, 1.0f, "Name");
    Images.EquipmentUI.rHeadingFont->DrawText(cValueX, cHeaderY, 0, 1.0f, "Cost");
    Images.EquipmentUI.rHeadingFont->DrawText(cGemsX,  cHeaderY, 0, 1.0f, "Gems");

    //--Y positioning
    float cTop = cHeaderY + 44.0f;
    float tCurrentY = cTop;
    float cSpcY = 23.0f;

    //--Variables.
    int tRendersLeft = AM_SHOP_ITEMS_PER_PAGE;
    int tSkipsLeft = mShopSkip;

    //--[Rendering Loop]
    int tCurrentItem = 0;
    AdventureItem *rItem = (AdventureItem *)rItemList->PushIterator();
    while(rItem)
    {
        //--If this item should not render, skip it.
        if(tSkipsLeft > 0)
        {
            tCurrentItem ++;
            tSkipsLeft --;
            rItem = (AdventureItem *)rItemList->AutoIterate();
            continue;
        }

        //--[Backing]
        if(tCurrentItem % 2 == 1)
        {
            SugarBitmap::DrawRectFill(15, tCurrentY, 728, tCurrentY + cSpcY, StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, 1.0f));
            StarlightColor::SetMixer(pColorMix, pColorMix, pColorMix, 1.0f);
        }

        //--[Item Icon]
        //--Render the item's icon here. It's a 22px entry.
        SugarBitmap *rItemIcon = rItem->GetIconImage();
        if(rItemIcon) rItemIcon->Draw(cIconX, tCurrentY);

        //--[Cursor Handling]
        //--If this is the selected item, render the cursor over it. If the mode is inactive, don't render this.
        //  Inactive mode is used when the player is hovering on Buy mode, but hasn't entered it yet.
        if(tCurrentItem == mShopCursor && pIsActiveMode)
        {
            //--Render the item description.
            RenderItemDescription(rItem);

            //--Render the properties and comparisons.
            RenderShopBuyProperties(rItem, mComparisonCharacter, pColorMix);

            //--Tint the color.
            StarlightColor::SetMixer(1.0f, 0.0f, 0.0f, 1.0f);
        }
        //--Not selected or is inactive mode.
        else
        {
            StarlightColor::SetMixer(pColorMix, pColorMix, pColorMix, 1.0f);
        }

        //--[Item Name]
        //--Name.
        float cNameW = Images.EquipmentUI.rMainlineFont->GetTextWidth(rItem->GetName());
        Images.EquipmentUI.rMainlineFont->DrawText(cNameX, tCurrentY, 0, 1.0f, rItem->GetName());

        //--Render stock if the item is stackable. This goes to the right of the name.
        if(rItem->IsStackable() && rItem->GetStackSize() > 1)
        {
            //--Small black backing box.
            char tString[32];
            sprintf(tString, "x%i", rItem->GetStackSize());
            float cLft = cNameX + cNameW;
            float cTop = tCurrentY + 5.0f;

            //--Color, render, clean.
            StarlightColor::SetMixer(0.0f, 1.0f * pColorMix, 0.0f, 1.0f);
            Images.StatusUI.rVerySmallFont->DrawTextArgs(cLft, cTop, 0, 1.0f, "x%i", rItem->GetStackSize());
            StarlightColor::SetMixer(pColorMix, pColorMix, pColorMix, 1.0f);
        }

        //--[Item Cost]
        //--Right-align.
        char tBuffer[32];
        sprintf(tBuffer, "%i", rItem->GetValue());

        //--Resolve position and render.
        float cXPos = cValueXRight - Images.EquipmentUI.rMainlineFont->GetTextWidth(tBuffer);
        Images.EquipmentUI.rMainlineFont->DrawTextArgs(cXPos, tCurrentY, 0, 1.0f, tBuffer);
        StarlightColor::SetMixer(pColorMix, pColorMix, pColorMix, 1.0f);

        //--[Gem Slots]
        //--Items which have gem slots render them here. They are always empty in the shop UI.
        float cGemW = 23.0f;
        int tGemSlots = rItem->GetGemSlots();
        for(int i = 0; i < tGemSlots; i ++)
        {
            Images.VendorUI.rGemEmpty22px->Draw(cGemsX + (i * cGemW), tCurrentY);
        }

        //--If the renders have run out, stop here.
        tRendersLeft --;
        if(tRendersLeft < 1)
        {
            rItemList->PopIterator();
            break;
        }

        //--Next.
        tCurrentItem ++;
        tCurrentY = tCurrentY + cSpcY;
        rItem = (AdventureItem *)rItemList->AutoIterate();
    }

    //--[Scrollbar]
    //--Render a scrollbar to indicate how much of the inventory is represented.
    if(rItemList->GetListSize() > AM_SHOP_ITEMS_PER_PAGE)
    {
        //--Binding.
        Images.VendorUI.rScrollbarFront->Bind();

        //--Scrollbar front. Stretches dynamically.
        float cScrollTop = 158.0f;
        float cScrollHei = 366.0f;

        //--Determine what percentage of the inventory is presently represented.
        int tMaxOffset = rItemList->GetListSize();
        float cPctTop = (mShopSkip                         ) / (float)tMaxOffset;
        float cPctBot = (mShopSkip + AM_SHOP_ITEMS_PER_PAGE) / (float)tMaxOffset;

        //--Positions and Constants.
        float cLft = 699.0f;
        float cRgt = cLft + Images.InventoryUI.rScrollbarFront->GetWidth();
        float cEdg = 6.0f / (float)Images.InventoryUI.rScrollbarFront->GetHeight();

        //--Compute where the bar should be.
        float cTopTop = cScrollTop + (cPctTop * cScrollHei);
        float cTopBot = cTopTop + 6.0f;
        float cBotBot = cScrollTop + (cPctBot * cScrollHei);
        float cBotTop = cBotBot - 6.0f;

        glBegin(GL_QUADS);
            //--Render the top of the bar.
            glTexCoord2f(0.0f, 0.0f); glVertex2f(cLft, cTopTop);
            glTexCoord2f(1.0f, 0.0f); glVertex2f(cRgt, cTopTop);
            glTexCoord2f(1.0f, cEdg); glVertex2f(cRgt, cTopBot);
            glTexCoord2f(0.0f, cEdg); glVertex2f(cLft, cTopBot);

            //--Render the bottom of the bar.
            glTexCoord2f(0.0f, 1.0f - cEdg); glVertex2f(cLft, cBotTop);
            glTexCoord2f(1.0f, 1.0f - cEdg); glVertex2f(cRgt, cBotTop);
            glTexCoord2f(1.0f, 1.0f);        glVertex2f(cRgt, cBotBot);
            glTexCoord2f(0.0f, 1.0f);        glVertex2f(cLft, cBotBot);

            //--Render the middle of the bar.
            glTexCoord2f(0.0f, cEdg);        glVertex2f(cLft, cTopBot);
            glTexCoord2f(1.0f, cEdg);        glVertex2f(cRgt, cTopBot);
            glTexCoord2f(1.0f, 1.0f - cEdg); glVertex2f(cRgt, cBotTop);
            glTexCoord2f(0.0f, 1.0f - cEdg); glVertex2f(cLft, cBotTop);
        glEnd();


        //--Backing. Already positioned.
        Images.VendorUI.rScrollbarBack->Draw();
    }

    //--In exchange mode, render the confirmation dialogue.
    if(mIsExchanging)
    {
        //--Get the item and verify it.
        AdventureItem *rSoldItem = (AdventureItem *)rItemList->GetElementBySlot(mShopCursor);
        if(!rSoldItem) return;

        //--Black out.
        SugarBitmap::DrawFullColor(StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, 0.75f));

        //--Overlay.
        Images.VendorUI.rConfirmOverlay->Draw();

        //--Buffer text.
        char tBuffer[128];
        sprintf(tBuffer, "Sell %s for %i Platina?", rSoldItem->GetName(), rSoldItem->GetValue());

        //--Check the length.
        float tTextLength = Images.VendorUI.rHeadingFont->GetTextWidth(tBuffer);

        //--Width is not too long. Render normally.
        if(tTextLength < 500.0f)
        {
            Images.VendorUI.rHeadingFont->DrawText(683.0f, 210.0f, SUGARFONT_AUTOCENTER_X, 1.0f, tBuffer);
        }
        //--Too long, split it into two lines.
        else
        {
            Images.VendorUI.rHeadingFont->DrawTextArgs(683.0f, 210.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Sell %s", rSoldItem->GetName());
            Images.VendorUI.rHeadingFont->DrawTextArgs(683.0f, 245.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "for %i platina?", rSoldItem->GetValue());
        }

        //--Other text.
        Images.VendorUI.rMainlineFont->DrawTextArgs(683.0f, 345.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Z to confirm");
        Images.VendorUI.rMainlineFont->DrawTextArgs(683.0f, 365.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "X to cancel");
    }
}
