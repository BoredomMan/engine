//--Base
#include "AdventureMenu.h"

//--Classes
#include "AdvCombat.h"
#include "AdvCombatEntity.h"
#include "AdventureInventory.h"
#include "AdventureItem.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarFont.h"
#include "SugarLinkedList.h"

//--Definitions
#include "Subdivide.h"

//--Libraries
//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "DisplayManager.h"
#include "LuaManager.h"

//--[Shop Fading]
//--Exactly the same as the regular shop.
#define FADE_MODE_NONE 0
#define FADE_INITIAL_OUT 1
#define FADE_INITIAL_HOLD 2
#define FADE_INITIAL_IN 3
#define FADE_FINAL_OUT 4
#define FADE_FINAL_HOLD 5
#define FADE_FINAL_IN 6

#define FADE_TICKS_OUT 15
#define FADE_TICKS_HOLD 30
#define FADE_TICKS_IN 15

//--[Rendering Statics]
//--[Forward Declarations]
void RenderAndAdvance(float pLft, float &sYCursor, SugarFont *pFont, const char *pFormat, ...);

//--[System]
//--[Manipulators]
void AdventureMenu::RegisterGemAdjective(const char *pAdjectiveName, SugarLinkedList *pUpgradeList)
{
    //--Registers a gem adjective, with the upgrade mapping provided being its properties if purchased.
    //  The list must exist, and the list must a set of dummy pointers. The name of each dummy pointer
    //  is the upgrade property expected.
    if(!pAdjectiveName || !pUpgradeList || pUpgradeList->GetListSize() < 1) return;

    //--We do not check if the individual upgrades on the list are valid, the rendering function does that.
    xGemAdjectiveList->AddElementAsTail(pAdjectiveName, pUpgradeList, &SugarLinkedList::DeleteThis);
}

//--[Update]
void AdventureMenu::UpdateGemcutterMode()
{
    //--[Setup]
    //--Quick-access pointers.
    ControlManager *rControlManager = ControlManager::Fetch();
    AdventureInventory *rInventory = AdventureInventory::Fetch();
    SugarLinkedList *rItemList = rInventory->GetGemList();

    //--Storage.
    int tOldCursor = mGemcutterCursor;

    //--[Selecting a Gem]
    //--When selecting a gem to improve, these controls are used.
    if(mSelectedGemCursor == -1)
    {
        //--[Directional Navigation Keys]
        //--Up, decrement by one row.
        if(rControlManager->IsFirstPress("Up"))
        {
            //--Decrement.
            mGemcutterCursor --;
            if(mGemcutterCursor < 1) mGemcutterCursor = 0;

            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
        //--Down, increment by one row.
        else if(rControlManager->IsFirstPress("Down"))
        {
            //--Increment.
            mGemcutterCursor ++;

            //--Range check the cursor on the last page, if needed.
            if(mGemcutterCursor >= rItemList->GetListSize()) mGemcutterCursor --;

            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
        //--Left, decrement by one. Moves page if applicable.
        else if(rControlManager->IsFirstPress("Left"))
        {
        }
        //--Right, increment by one. Moves page if applicable.
        else if(rControlManager->IsFirstPress("Right"))
        {
        }

        //--[Action Keys]
        //--Activate, enters upgrade mode for the gem in question.
        if(rControlManager->IsFirstPress("Activate"))
        {
            //--Get the item in question.
            AdventureItem *rCheckItem = (AdventureItem *)rItemList->GetElementBySlot(mGemcutterCursor);
            if(!rCheckItem) return;

            //--Make sure the upgrade costs have been built.
            AdventureItem::InitGemcutterCosts();

            //--Get the upgrade level.
            int tCurrentUpgrade = rCheckItem->GetUpgradesTotal();
            if(tCurrentUpgrade < 0 || tCurrentUpgrade >= ADITEM_MAX_UPGRADES)
            {
                AudioManager::Fetch()->PlaySound("Menu|Failed");
                return;
            }

            //--If the item can be upgraded, switch to upgrade mode:
            if(rCheckItem->IsUpgradeable())
            {
                //--Get the name of the item. Run it against the upgrade script.
                const char *rItemName = rCheckItem->GetName();
                if(!rItemName || !AdventureMenu::xGemcutterResolveScript) return;

                //--Get how many upgrades exist. If the gem is maxed out, don't allow upgrading.
                float cUpgrades = (float)rCheckItem->GetUpgradesTotal();
                if(cUpgrades >= 12.0f)
                {
                    AudioManager::Fetch()->PlaySound("Menu|Failed");
                    return;
                }

                //--Run resolver script.
                xGemAdjectiveList->ClearList();
                LuaManager::Fetch()->ExecuteLuaFile(AdventureMenu::xGemcutterResolveScript, 1, "S", rItemName);

                //--No upgrades? Fail!
                if(xGemAdjectiveList->GetListSize() < 1)
                {
                    AudioManager::Fetch()->PlaySound("Menu|Failed");
                    return;
                }

                //--Switch modes.
                mSelectedGemCursor = mGemcutterCursor;
                mGemcutterCursor = 0;
                tOldCursor = mGemcutterCursor;

                //--SFX.
                AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            }
            //--Fail, play a beep.
            else
            {
                AudioManager::Fetch()->PlaySound("Menu|Failed");
            }
        }

        //--SFX, and check the comparison cursor if the item changed.
        if(tOldCursor != mGemcutterCursor)
        {
            //--[Recentering]
            //--On a decrement:
            if(mGemcutterCursor < tOldCursor)
            {
                //--Get where the cursor is on the page.
                int tPosition = mGemcutterCursor - mShopSkip;
                if(tPosition < 2) mShopSkip = mGemcutterCursor - 2;

                //--Clamp.
                if(mShopSkip < 0) mShopSkip = 0;
            }
            //--On an increment:
            else if(mGemcutterCursor > tOldCursor)
            {
                //--Get where the cursor is on the page.
                int tPosition = mGemcutterCursor - mShopSkip;
                if(tPosition > AM_SHOP_ITEMS_PER_PAGE - 3) mShopSkip = mGemcutterCursor - (AM_SHOP_ITEMS_PER_PAGE - 3);

                //--Clamp.
                if(mShopSkip > rItemList->GetListSize() - AM_SHOP_ITEMS_PER_PAGE) mShopSkip = rItemList->GetListSize() - AM_SHOP_ITEMS_PER_PAGE;
            }
        }
    }
    //--[Upgrade Selection]
    //--Handles picking the upgrade and applying it, or cancelling out.
    else
    {
        //--[Directional Navigation Keys]
        //--Up, decrement by one row.
        if(rControlManager->IsFirstPress("Up"))
        {
            //--Clamp.
            if(mGemcutterCursor > 0)
            {
                mGemcutterCursor --;
                AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            }
        }
        //--Down, increment by one row.
        else if(rControlManager->IsFirstPress("Down"))
        {
            //--Clamp.
            if(mGemcutterCursor < xGemAdjectiveList->GetListSize() - 1)
            {
                mGemcutterCursor ++;
                AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            }
        }

        //--[Action Keys]
        //--Activate, shows the confirm cut prompt.
        if(rControlManager->IsFirstPress("Activate"))
        {
            //--Gem in question.
            AdventureItem *rGem = (AdventureItem *)rItemList->GetElementBySlot(mSelectedGemCursor);
            if(!rGem) return;

            //--Switch back to gem selection.
            mGemcutterCursor = mSelectedGemCursor;
            mSelectedGemCursor = -1;
        }
        //--Cancel, returns to gem selection.
        else if(rControlManager->IsFirstPress("Cancel"))
        {
            mGemcutterCursor = mSelectedGemCursor;
            mSelectedGemCursor = -1;
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
    }
}

//--[Rendering]
void AdventureMenu::RenderGemcutterMode(bool pIsActiveMode, float pColorMix)
{
    //--[Documentation and Setup]
    //--Renders the gemcutting menu. The frames are already rendered by the calling function, this just renders the text and icons.
    if(!Images.mIsReady) return;

    //--Fast-access pointers.
    AdventureInventory *rInventory = AdventureInventory::Fetch();
    SugarLinkedList *rItemList = rInventory->GetGemList();

    //--[No Gemcut Mode]
    //--If this flag is not set, then this vendor cannot cut gems.
    if(!mShopCanGemcut)
    {
        Images.EquipmentUI.rHeadingFont->DrawText(19.0f, 124.0f, 0, 1.0f, "This vendor cannot modify gems.");
        return;
    }

    //--[Gem Inventory]
    //--Render the player's gems.
    float cHeaderY = 124.0f;
    float cIconX  = 30.0f;
    float cIconW  = 22.0f;
    float cFaceX = cIconX + cIconW;
    float cFaceW = 32.0f;
    float cNameX  = cIconX + cIconW + cFaceW + 2.0f;
    float cUpgradesX = 370.0f;

    //--Text headers.
    StarlightColor::SetMixer(pColorMix, pColorMix, pColorMix, 1.0f);
    Images.EquipmentUI.rHeadingFont->DrawText(cIconX,     cHeaderY, 0, 1.0f, "Name");
    Images.EquipmentUI.rHeadingFont->DrawText(cUpgradesX, cHeaderY, 0, 1.0f, "Rank");

    //--Y positioning
    float cTop = cHeaderY + 44.0f;
    float tCurrentY = cTop;
    float cSpcY = 23.0f;

    //--Variables.
    int tRendersLeft = AM_SHOP_ITEMS_PER_PAGE;
    int tSkipsLeft = mShopSkip;

    //--Cursor.
    int tCursor = mGemcutterCursor;
    if(mSelectedGemCursor != -1) tCursor = mSelectedGemCursor;

    //--[Rendering Loop]
    int tCurrentItem = 0;
    AdventureItem *rHighlightedGem = NULL;
    AdventureItem *rItem = (AdventureItem *)rItemList->PushIterator();
    while(rItem)
    {
        //--If this item should not render, skip it.
        if(tSkipsLeft > 0)
        {
            tCurrentItem ++;
            tSkipsLeft --;
            rItem = (AdventureItem *)rItemList->AutoIterate();
            continue;
        }

        //--[Backing]
        if(tCurrentItem % 2 == 1)
        {
            SugarBitmap::DrawRectFill(15, tCurrentY, 728, tCurrentY + cSpcY, StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, 1.0f));
            StarlightColor::SetMixer(pColorMix, pColorMix, pColorMix, 1.0f);
        }

        //--[Item Icon]
        //--Render the item's icon here. It's a 22px entry.
        SugarBitmap *rItemIcon = rItem->GetIconImage();
        if(rItemIcon) rItemIcon->Draw(cIconX, tCurrentY);

        //--[Owner's Face Indicator]
        //--Get the item's name and its owner's name. It's possible for the owner's name to be NULL
        //  if the item isn't equipped.
        const char *rOwnerName = rItemList->GetIteratorName();

        //--Item has no owner:
        if(!rOwnerName || !strcasecmp(rOwnerName, "Null"))
        {
        }
        //--Item has an owner:
        else
        {
            //--Fast-access pointers.
            AdvCombat *rAdventureCombat = AdvCombat::Fetch();

            //--Variables.
            const char *rRenderOwnerName = rOwnerName;
            SugarBitmap *rFaceImg = NULL;
            TwoDimensionReal tFaceDim;

            //--Resolve the owner's display name. It's usually the same, but not always (Ex: Chris, Christine).
            AdvCombatEntity *rEntity = rAdventureCombat->GetRosterMemberS(rRenderOwnerName);
            if(rEntity)
            {
                rFaceImg = rEntity->GetFaceProperties(tFaceDim);
            }

            //--Render the character's face.
            if(rFaceImg)
            {
                tFaceDim.mLft = tFaceDim.mLft / rFaceImg->GetWidth();
                tFaceDim.mTop = 1.0f - (tFaceDim.mTop / rFaceImg->GetHeight());
                tFaceDim.mRgt = tFaceDim.mRgt / rFaceImg->GetWidth();
                tFaceDim.mBot = 1.0f - (tFaceDim.mBot / rFaceImg->GetHeight());
                rFaceImg->Bind();
                glBegin(GL_QUADS);
                    glTexCoord2f(tFaceDim.mLft, tFaceDim.mTop); glVertex2f(cFaceX,          tCurrentY);
                    glTexCoord2f(tFaceDim.mRgt, tFaceDim.mTop); glVertex2f(cFaceX + cFaceW, tCurrentY);
                    glTexCoord2f(tFaceDim.mRgt, tFaceDim.mBot); glVertex2f(cFaceX + cFaceW, tCurrentY + cFaceW);
                    glTexCoord2f(tFaceDim.mLft, tFaceDim.mBot); glVertex2f(cFaceX,          tCurrentY + cFaceW);
                glEnd();
            }
        }

        //--[Upgrade Determination]
        //--Set a flag if the gem can't be upgraded.
        bool tCanBeUpgraded = false;
        int tCurrentUpgrade = rItem->GetUpgradesTotal();
        float cUpgrades = (float)rItem->GetUpgradesTotal();

        //--Get how many upgrades exist. If the gem is maxed out, don't allow upgrading.
        if(cUpgrades >= 12.0f)
        {
        }
        //--Not maxed out, check if we have the adamantite.
        else
        {
        }

        //--[Cursor Handling]
        //--If this is the selected item, render the cursor over it. If the mode is inactive, don't render this.
        //  Inactive mode is used when the player is hovering on Buy mode, but hasn't entered it yet.
        if(tCurrentItem == tCursor && pIsActiveMode)
        {
            //--Render the item description if not upgrading the gem.
            if(mSelectedGemCursor == -1) RenderItemDescription(rItem);

            //--Store.
            rHighlightedGem = rItem;

            //--Tint the color.
            if(tCanBeUpgraded)
                StarlightColor::SetMixer(1.0f, 0.0f, 0.0f, 1.0f);
            else
                StarlightColor::SetMixer(0.5f, 0.0f, 0.0f, 1.0f);
        }
        //--Not selected or is inactive mode.
        else
        {
            if(tCanBeUpgraded)
                StarlightColor::SetMixer(pColorMix, pColorMix, pColorMix, 1.0f);
            else
                StarlightColor::SetMixer(pColorMix * 0.50f, pColorMix * 0.50f, pColorMix * 0.50f, 1.0f);
        }

        //--[Item Name]
        //--Name.
        Images.EquipmentUI.rMainlineFont->DrawText(cNameX, tCurrentY, 0, 1.0f, rItem->GetName());

        //--[Upgrade Count]
        //--Right-align.
        char tBuffer[32];
        sprintf(tBuffer, "%i", rItem->GetUpgradesTotal());

        //--Resolve position and render.
        Images.EquipmentUI.rMainlineFont->DrawTextArgs(cUpgradesX + 44.0f, tCurrentY, SUGARFONT_AUTOCENTER_X, 1.0f, tBuffer);
        StarlightColor::SetMixer(pColorMix, pColorMix, pColorMix, 1.0f);

        //--If the renders have run out, stop here.
        tRendersLeft --;
        if(tRendersLeft < 1)
        {
            rItemList->PopIterator();
            break;
        }

        //--Next.
        tCurrentItem ++;
        tCurrentY = tCurrentY + cSpcY;
        rItem = (AdventureItem *)rItemList->AutoIterate();
    }

    //--[Scrollbar]
    //--Render a scrollbar to indicate how much of the inventory is represented.
    if(rItemList->GetListSize() > AM_SHOP_ITEMS_PER_PAGE)
    {
        //--Binding.
        Images.VendorUI.rScrollbarFront->Bind();

        //--Scrollbar front. Stretches dynamically.
        float cScrollTop = 158.0f;
        float cScrollHei = 366.0f;

        //--Determine what percentage of the inventory is presently represented.
        int tMaxOffset = rItemList->GetListSize();
        float cPctTop = (mShopSkip                         ) / (float)tMaxOffset;
        float cPctBot = (mShopSkip + AM_SHOP_ITEMS_PER_PAGE) / (float)tMaxOffset;

        //--Positions and Constants.
        float cLft = 699.0f;
        float cRgt = cLft + Images.InventoryUI.rScrollbarFront->GetWidth();
        float cEdg = 6.0f / (float)Images.InventoryUI.rScrollbarFront->GetHeight();

        //--Compute where the bar should be.
        float cTopTop = cScrollTop + (cPctTop * cScrollHei);
        float cTopBot = cTopTop + 6.0f;
        float cBotBot = cScrollTop + (cPctBot * cScrollHei);
        float cBotTop = cBotBot - 6.0f;

        glBegin(GL_QUADS);
            //--Render the top of the bar.
            glTexCoord2f(0.0f, 0.0f); glVertex2f(cLft, cTopTop);
            glTexCoord2f(1.0f, 0.0f); glVertex2f(cRgt, cTopTop);
            glTexCoord2f(1.0f, cEdg); glVertex2f(cRgt, cTopBot);
            glTexCoord2f(0.0f, cEdg); glVertex2f(cLft, cTopBot);

            //--Render the bottom of the bar.
            glTexCoord2f(0.0f, 1.0f - cEdg); glVertex2f(cLft, cBotTop);
            glTexCoord2f(1.0f, 1.0f - cEdg); glVertex2f(cRgt, cBotTop);
            glTexCoord2f(1.0f, 1.0f);        glVertex2f(cRgt, cBotBot);
            glTexCoord2f(0.0f, 1.0f);        glVertex2f(cLft, cBotBot);

            //--Render the middle of the bar.
            glTexCoord2f(0.0f, cEdg);        glVertex2f(cLft, cTopBot);
            glTexCoord2f(1.0f, cEdg);        glVertex2f(cRgt, cTopBot);
            glTexCoord2f(1.0f, 1.0f - cEdg); glVertex2f(cRgt, cBotTop);
            glTexCoord2f(0.0f, 1.0f - cEdg); glVertex2f(cLft, cBotTop);
        glEnd();

        //--Backing. Already positioned.
        Images.VendorUI.rScrollbarBack->Draw();
    }

    //--[Ingredients Listing]
    //--Heading.
    Images.EquipmentUI.rHeadingFont->DrawTextArgs(762.0f, 125.0f, 0, 1.0f, "Adamantite");

    //--Constants.
    float cAdmIconX  = 762.0f;
    float cAdmX      = cAdmIconX + 23.0f;
    float cAdmR      = cAdmX + 150.0f;
    float cAdmY      = 160.0f;
    float cAdmSpc    =  23.0f;
    uint32_t cFlagsL = 0;
    uint32_t cFlagsR = SUGARFONT_RIGHTALIGN_X;
    Images.EquipmentUI.rMainlineFont->DrawTextArgs(cAdmX, cAdmY + (cAdmSpc * 0.00f), cFlagsL, 1.0f, "Powder");
    Images.EquipmentUI.rMainlineFont->DrawTextArgs(cAdmR, cAdmY + (cAdmSpc * 0.00f), cFlagsR, 1.0f, "%3i", rInventory->GetCraftingCount(CRAFT_ADAMANTITE_POWDER));
    Images.EquipmentUI.rMainlineFont->DrawTextArgs(cAdmX, cAdmY + (cAdmSpc * 1.00f), cFlagsL, 1.0f, "Flakes");
    Images.EquipmentUI.rMainlineFont->DrawTextArgs(cAdmR, cAdmY + (cAdmSpc * 1.00f), cFlagsR, 1.0f, "%3i", rInventory->GetCraftingCount(CRAFT_ADAMANTITE_FLAKES));
    Images.EquipmentUI.rMainlineFont->DrawTextArgs(cAdmX, cAdmY + (cAdmSpc * 2.00f), cFlagsL, 1.0f, "Shards");
    Images.EquipmentUI.rMainlineFont->DrawTextArgs(cAdmR, cAdmY + (cAdmSpc * 2.00f), cFlagsR, 1.0f, "%3i", rInventory->GetCraftingCount(CRAFT_ADAMANTITE_SHARD));
    Images.EquipmentUI.rMainlineFont->DrawTextArgs(cAdmX, cAdmY + (cAdmSpc * 3.00f), cFlagsL, 1.0f, "Pieces");
    Images.EquipmentUI.rMainlineFont->DrawTextArgs(cAdmR, cAdmY + (cAdmSpc * 3.00f), cFlagsR, 1.0f, "%3i", rInventory->GetCraftingCount(CRAFT_ADAMANTITE_PIECE));
    Images.EquipmentUI.rMainlineFont->DrawTextArgs(cAdmX, cAdmY + (cAdmSpc * 4.00f), cFlagsL, 1.0f, "Chunks");
    Images.EquipmentUI.rMainlineFont->DrawTextArgs(cAdmR, cAdmY + (cAdmSpc * 4.00f), cFlagsR, 1.0f, "%3i", rInventory->GetCraftingCount(CRAFT_ADAMANTITE_CHUNK));
    Images.EquipmentUI.rMainlineFont->DrawTextArgs(cAdmX, cAdmY + (cAdmSpc * 5.00f), cFlagsL, 1.0f, "Ore");
    Images.EquipmentUI.rMainlineFont->DrawTextArgs(cAdmR, cAdmY + (cAdmSpc * 5.00f), cFlagsR, 1.0f, "%3i", rInventory->GetCraftingCount(CRAFT_ADAMANTITE_ORE));

    //--Cost display. This uses whichever gem is currently highlighted.
    float cCostX = 1000.0f;
    Images.EquipmentUI.rHeadingFont->DrawTextArgs(cCostX, 125.0f, 0, 1.0f, "Cost");
    if(rHighlightedGem)
    {
        StarlightColor::ClearMixer();
    }

    //--[Upgrade Selection]
    //--When a gem is selected for upgrading, render the upgrades available below the adamantite counts.
    if(mSelectedGemCursor != -1)
    {
        //--Get the gem in question.
        SugarLinkedList *rItemList = AdventureInventory::Fetch()->GetGemList();
        AdventureItem *rGem = (AdventureItem *)rItemList->GetElementBySlot(mSelectedGemCursor);
        if(!rGem) return;

        //--Header.
        float cCutHeaderX = 762.0f;
        float cCutHeaderIconX = 870.0f;
        float cCutHeaderNameX = cCutHeaderIconX + 23.0f;
        float cCutHeaderY = 320.0f;
        Images.EquipmentUI.rMainlineFont->DrawText(cCutHeaderX, cCutHeaderY, 0, 1.0f, "Upgrade");
        SugarBitmap *rGemIcon = rGem->GetIconImage();
        if(rGemIcon) rGemIcon->Draw(cCutHeaderIconX, cCutHeaderY);
        Images.EquipmentUI.rMainlineFont->DrawText(cCutHeaderNameX, cCutHeaderY, 0, 1.0f, rGem->GetName());

        //--Listing of Adjectives
        float cAdjNameX = cCutHeaderX + 8.0f;
        float cAdjY = 350.0f;
        float cAdjSpcY = 23.0f;
        int tAdjectivesTotal = xGemAdjectiveList->GetListSize();
        for(int i = 0; i < tAdjectivesTotal; i ++)
        {
            //--Get the adjective's name.
            const char *rAdjectiveName = xGemAdjectiveList->GetNameOfElementBySlot(i);
            if(!rAdjectiveName) return;

            //--Grey out if unselected.
            StarlightColor::ConditionalMixer(mGemcutterCursor == i, StarlightColor::MapRGBAF(1.0f, 1.0f, 1.0f, 1.0f), StarlightColor::MapRGBAF(0.5f, 0.5f, 0.5f, 1.0f));

            //--Render the name.
            Images.EquipmentUI.rMainlineFont->DrawText(cAdjNameX, cAdjY + (cAdjSpcY * i), 0, 1.0f, rAdjectiveName);

            //--Property rendering. Skip the zeroth property, that's the upgrade code.
            SugarLinkedList *rSublist = (SugarLinkedList *)xGemAdjectiveList->GetElementBySlot(i);
            if(!rSublist) return;
            rSublist->PushIterator();

            //--Position setup.
            float tPosX = 900.0f;
            float cSpcX =  45.0f;

            //--Iterate across the properties and render them.
            void *rDummy = rSublist->AutoIterate();
            while(rDummy)
            {
                //--The actual upgrade code is the name of the entry.
                const char *rUpgradeCode = rSublist->GetIteratorName();

                //--Get the value to apply.
                int tApplyValue = atoi(&rUpgradeCode[4]);

                //--Determine which icon represents the upgrade.
                SugarBitmap *rImage = NULL;
                for(int p = 0; p < AM_INV_HEADER_TOTAL; p ++)
                {
                    if(!strncasecmp(rUpgradeCode, xUpgradeIconRemaps[p], 3))
                    {
                        rImage = Images.InventoryUI.rImageHeaders[p];
                        break;
                    }
                }

                //--If the icon resolved, render it.
                if(rImage) rImage->Draw(tPosX, cAdjY + (cAdjSpcY * i) + 3.0f);
                Images.StatusUI.rVerySmallFont->DrawTextArgs(tPosX + 23.0f, cAdjY + (cAdjSpcY * i) + 5.0f, 0, 1.0f, "%i", tApplyValue);

                //--Check if we're over the edge.
                tPosX = tPosX + cSpcX;

                //--Next.
                rDummy = rSublist->AutoIterate();
            }
        }
    }
}
