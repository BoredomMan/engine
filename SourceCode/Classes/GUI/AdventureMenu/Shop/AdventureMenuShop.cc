//--Base
#include "AdventureMenu.h"

//--Classes
#include "AdvCombat.h"
#include "AdvCombatEntity.h"
#include "AdventureItem.h"
#include "AdventureInventory.h"
#include "AdventureLevel.h"
#include "VisualLevel.h"

//--CoreClasses
#include "SugarAutoBuffer.h"
#include "SugarBitmap.h"
#include "SugarFont.h"
#include "SugarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "Subdivide.h"

//--Libraries
//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "DisplayManager.h"
#include "LuaManager.h"

//--[Shop Fading]
#define FADE_MODE_NONE 0
#define FADE_INITIAL_OUT 1
#define FADE_INITIAL_HOLD 2
#define FADE_INITIAL_IN 3
#define FADE_FINAL_OUT 4
#define FADE_FINAL_HOLD 5
#define FADE_FINAL_IN 6

#define FADE_TICKS_OUT 15
#define FADE_TICKS_HOLD 30
#define FADE_TICKS_IN 15

//--[System]
void AdventureMenu::InitializeShopMode(const char *pShopName, const char *pPath, const char *pTeardownPath)
{
    //--[Documentation and Setup]
    //--Boots shopping mode, using the provided path to get information about the items. Note that the path
    //  is executed like any other script, so it could fire a cutscene instead of specifying shop information.
    //--If the script is NULL, the shop information is cleared and shop mode is deactivated.
    mIsShopMode = false;
    mShopCanSell = false;
    mShopCanGemcut = false;
    mShopName[0] = '\0';
    mShopModeType = AM_SELECT_SHOP_MODE;
    mShopCursor = 0;
    mIsExchanging = false;
    ResetString(mShopTeardownPath, NULL);
    mShopInventory->ClearList();
    mCharacterWalkTimer = 0;
    if(!pShopName || !pPath || !pTeardownPath) return;

    //--Set the shop name.
    strncpy(mShopName, pShopName, STD_MAX_LETTERS - 1);
    ResetString(mShopTeardownPath, pTeardownPath);

    //--Run the provided script.
    LuaManager::Fetch()->ExecuteLuaFile(pPath);

    //--If at least one item is up for sale (even if its stock is zero), or the shop will allow the player to sell,
    //  activate shop mode.
    if(mShopInventory->GetListSize() > 0 || mShopCanSell)
    {
        //--Flag.
        mIsShopMode = true;

        //--Fading values.
        mShopFadeMode = FADE_INITIAL_OUT;
        mShopFadeTimer = 0;
    }
}
void AdventureMenu::InitializeGemcutterMode()
{
    //--When fired, allows the shop to cut gems.
    mShopCanGemcut = true;
    xGemAdjectiveList->ClearList();

    //--Rebuild the gems list.
    AdventureInventory::Fetch()->BuildGemList();

    //--Make sure the costs have been initialized.
    AdventureItem::InitGemcutterCosts();
}

//--[Manipulators]
void AdventureMenu::SetSellFlag(bool pFlag)
{
    mShopCanSell = pFlag;
}
void AdventureMenu::RegisterItem(const char *pName)
{
    //--Error check.
    AdventureInventory *rInventory = AdventureInventory::Fetch();
    if(!pName || !AdventureLevel::xItemListPath) return;

    //--Run the item list code to create the item in question.
    rInventory->rLastReggedItem = NULL;
    rInventory->mBlockStackingOnce = true;
    LuaManager::Fetch()->ExecuteLuaFile(AdventureLevel::xItemListPath, 2, "S", pName, "N", 1.0f);
    rInventory->mBlockStackingOnce = false;

    //--Unregister the item that was last created and add it to our local list instead.
    AdventureItem *rLastItem = (AdventureItem *)rInventory->rLastReggedItem;
    if(!rLastItem) return;
    rInventory->LiberateItemP(rLastItem);

    //--If the item is flagged as unique, and the player already has one (in inventory or equipped)
    //  then fail to add the item.
    if(rLastItem->IsUnique())
    {
        //--Check for duplicates.
        int tOwnedCount = rInventory->GetCountOf(rLastItem->GetName());
        int tEquipCount = rInventory->IsItemEquipped(rLastItem->GetName());
        if(tOwnedCount > 0 || tEquipCount > 0)
        {
            delete rLastItem;
            return;
        }
    }

    //--All checks passed, register.
    mShopInventory->AddElementAsTail("LIMITED", rLastItem, &RootObject::DeleteThis);
}
void AdventureMenu::RegisterItemUnlimited(const char *pName)
{
    //--Registers an item in unlimited quantity. When the player buys the item, it will not be removed from the
    //  inventory list. This is usually used for items like crafting ingredients.
    AdventureInventory *rInventory = AdventureInventory::Fetch();
    if(!pName || !AdventureLevel::xItemListPath) return;

    //--Run the item list code to create the item in question.
    rInventory->rLastReggedItem = NULL;
    rInventory->mBlockStackingOnce = true;
    LuaManager::Fetch()->ExecuteLuaFile(AdventureLevel::xItemListPath, 2, "S", pName, "N", 1.0f);
    rInventory->mBlockStackingOnce = false;

    //--Unregister the item that was last created and add it to our local list instead.
    AdventureItem *rLastItem = (AdventureItem *)rInventory->rLastReggedItem;
    rInventory->LiberateItemP(rLastItem);
    if(!rLastItem) return;

    //--If the item is flagged as unique, and the player already has one (in inventory or equipped)
    //  then fail to add the item.
    if(rLastItem->IsUnique())
    {
        //--Check for duplicates.
        int tOwnedCount = rInventory->GetCountOf(rLastItem->GetName());
        int tEquipCount = rInventory->IsItemEquipped(rLastItem->GetName());
        if(tOwnedCount > 0 || tEquipCount > 0)
        {
            delete rLastItem;
            return;
        }
    }

    //--All checks passed, register.
    mShopInventory->AddElementAsTail("UNLIMITED", rLastItem, &RootObject::DeleteThis);
}

//--[Core Methods]
void AdventureMenu::AssembleDescriptionLines(const char *pDescriptionLine)
{
    //--Splits the rendering strings into smaller strings so they don't run over the edge of the description box.
    mDescriptionLines->ClearList();
    if(!pDescriptionLine) return;

    //--Setup.
    int tCharsParsedTotal = 0;
    int tStringLen = (int)strlen(pDescriptionLine);

    //--Loop until the whole description has been parsed out.
    while(tCharsParsedTotal < tStringLen)
    {
        //--Run the subdivide.
        int tCharsParsed = 0;
        char *nDescriptionLine = Subdivide::SubdivideString(tCharsParsed, &pDescriptionLine[tCharsParsedTotal], -1, VIRTUAL_CANVAS_X * 0.90f, Images.Data.rMenuFont, 1.0f);
        mDescriptionLines->AddElementAsTail("X", nDescriptionLine, &FreeThis);

        //--Move to the next line.
        tCharsParsedTotal += tCharsParsed;
    }
}
char *AdventureMenu::AssembleInventoryString()
{
    //--Takes the current inventory contents and places them in a string. The string contains the names of each item, plus a dividing '|'.
    //  This is used to save the state of items between usages of each shop.
    SugarAutoBuffer *tAutoBuffer = new SugarAutoBuffer();

    //--Iterate.
    AdventureItem *rItem = (AdventureItem *)mShopInventory->PushIterator();
    while(rItem)
    {
        tAutoBuffer->AppendStringWithoutNull(rItem->GetName());
        tAutoBuffer->AppendCharacter('|');
        rItem = (AdventureItem *)mShopInventory->AutoIterate();
    }

    //--Liberate the resulting string.
    tAutoBuffer->AppendCharacter(0);
    uint8_t *nData = tAutoBuffer->LiberateData();

    //--Finish up.
    delete tAutoBuffer;
    return (char *)nData;
}

//--[Update]
void AdventureMenu::UpdateShopMenu()
{
    //--[Fade Controllers]
    if(mShopFadeMode != FADE_MODE_NONE)
    {
        //--Initial fade out.
        if(mShopFadeMode == FADE_INITIAL_OUT || mShopFadeMode == FADE_FINAL_OUT)
        {
            mShopFadeTimer ++;
            if(mShopFadeTimer > FADE_TICKS_OUT)
            {
                mShopFadeMode ++;
                mShopFadeTimer = 0;
            }
        }
        //--Holding.
        else if(mShopFadeMode == FADE_INITIAL_HOLD || mShopFadeMode == FADE_FINAL_HOLD)
        {
            mShopFadeTimer ++;
            if(mShopFadeTimer > FADE_TICKS_HOLD)
            {
                mShopFadeMode ++;
                mShopFadeTimer = 0;
            }
        }
        //--Fade back in.
        else if(mShopFadeMode == FADE_INITIAL_IN || mShopFadeMode == FADE_FINAL_IN)
        {
            mShopFadeTimer ++;
            if(mShopFadeTimer > FADE_TICKS_IN)
            {
                //--For exiting the menu, disable shop mode now.
                if(mShopFadeMode == FADE_FINAL_IN)
                {
                    //--Teardown script.
                    if(mShopTeardownPath) LuaManager::Fetch()->ExecuteLuaFile(mShopTeardownPath);

                    //--Stock.
                    Hide();
                }

                //--Stock.
                mShopFadeMode = FADE_MODE_NONE;
                mShopFadeTimer = 0;
            }
        }
        return;
    }

    //--[Setup]
    //--Quick-access pointers.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Storage.
    bool tChangedCursor = false;
    //int tOldCursor = mShopCursor;

    //--[Selecting Buy/Sell]
    //--Selecting of buy and sell.
    if(mShopModeType == AM_SELECT_SHOP_MODE)
    {
        //--Pressing left decrements the cursor.
        if(rControlManager->IsFirstPress("Left"))
        {
            if(mShopCursor > AM_SHOP_SELECT_MIN)
            {
                tChangedCursor = true;
                mShopSkip = 0;
                mShopCursor --;
                AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            }
        }
        //--Pressing right increments the cursor.
        else if(rControlManager->IsFirstPress("Right"))
        {
            if(mShopCursor < AM_SHOP_SELECT_MAX )
            {
                tChangedCursor = true;
                mShopSkip = 0;
                mShopCursor ++;
                AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            }
        }

        //--If the cursor changed, and ended on the gemcutter tab, rebuild the gem list.
        if(tChangedCursor && mShopCursor == AM_SHOP_SELECT_GEMCUTTER)
        {
            AdventureInventory::Fetch()->BuildGemList();
        }

        //--Pressing Activate will enter whatever mode is currently selected.
        if(rControlManager->IsFirstPress("Activate"))
        {
            //--Buying mode.
            if(mShopCursor == AM_SHOP_SELECT_BUY)
            {
                //--Flags.
                mShopModeType = AM_BUY;
                AudioManager::Fetch()->PlaySound("Menu|Select");

                //--Get the 0th element. Assemble the description for it.
                //AdventureItem *rZerothItem = (AdventureItem *)mShopInventory->GetElementBySlot(0);
                //AssembleDescriptionLines(rZerothItem->GetDescription());
            }
            //--Selling mode.
            else if(mShopCursor == AM_SHOP_SELECT_SELL)
            {
                //--Flags.
                mShopCursor = 0;
                mShopModeType = AM_SELL;
                AudioManager::Fetch()->PlaySound("Menu|Select");

                //--Assemble the description for the 0th item.
                //SugarLinkedList *rItemList = AdventureInventory::Fetch()->GetItemList();
                //AdventureItem *rZerothItem = (AdventureItem *)rItemList->GetElementBySlot(0);
                //AssembleDescriptionLines(rZerothItem->GetDescription());
            }
            //--Purify mode. Not in this prototype.
            else if(mShopCursor == AM_SHOP_SELECT_PURIFY)
            {

            }
            //--Gemcutter mode. Not in this prototype.
            else if(mShopCursor == AM_SHOP_SELECT_GEMCUTTER)
            {
                //--Play a fail sound if the shop can't gemcut.
                if(!mShopCanGemcut)
                {
                    AudioManager::Fetch()->PlaySound("Menu|Failed");
                }
                //--Normal case:
                else
                {
                    mShopCursor = 0;
                    mShopModeType = AM_GEMCUTTER;
                    AudioManager::Fetch()->PlaySound("Menu|Select");
                }
            }
            return;
        }
        //--Pressing Cancel will back out of the shop mode.
        else if(rControlManager->IsFirstPress("Cancel"))
        {
            //--Activate fade mode.
            mShopFadeMode = FADE_FINAL_OUT;
            mShopFadeTimer = 0;
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            return;
        }
    }
    //--[Sub Handlers]
    else
    {
        //--Press cancel either stops exchanges or returns to mode selection.
        if(rControlManager->IsFirstPress("Cancel"))
        {
            //--Cancel is ignored if the gemcutter UI is currently in an advanced selection:
            if(mShopModeType == AM_GEMCUTTER && mSelectedGemCursor != -1)
            {

            }
            //--Normal case:
            else
            {
                //--Cancel the exchange.
                if(mIsExchanging)
                {
                    mIsExchanging = false;
                    AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
                    return;
                }
                //--Return to mode selection.
                else
                {
                    //--Buy handler.
                    if(mShopModeType == AM_BUY)
                    {
                        mShopCursor = AM_SHOP_SELECT_BUY;
                    }
                    //--Sell Handler.
                    else if(mShopModeType == AM_SELL)
                    {
                        mShopCursor = AM_SHOP_SELECT_SELL;
                    }
                    //--Purify Handler.
                    else if(mShopModeType == AM_PURIFY)
                    {
                        mShopCursor = AM_SHOP_SELECT_PURIFY;
                    }
                    //--Gemcutter Handler.
                    else if(mShopModeType == AM_GEMCUTTER)
                    {
                        mShopCursor = AM_SHOP_SELECT_GEMCUTTER;
                    }

                    //--Clear outstanding description lines.
                    mShopModeType = AM_SELECT_SHOP_MODE;
                    AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
                    return;
                }
            }
        }

        //--Run the buy update if in that mode.
        if(mShopModeType == AM_BUY)
        {
            UpdateShopBuy();
        }
        //--Sell Handler.
        else if(mShopModeType == AM_SELL)
        {
            UpdateShopSell();
        }
        //--Purify Handler.
        else if(mShopModeType == AM_PURIFY)
        {
        }
        //--Gemcutter Handler.
        else if(mShopModeType == AM_GEMCUTTER)
        {
            UpdateGemcutterMode();
        }
    }
}

//--[Drawing]
void AdventureMenu::RenderShopMenu()
{
    //--[Documentation]
    //--Render the shop menu.
    if(!Images.mIsReady) return;

    //--[Fade Handling]
    //--These fades stop rendering of the menu entirely.
    if(mShopFadeMode == FADE_INITIAL_HOLD || mShopFadeMode == FADE_FINAL_HOLD)
    {
        SugarBitmap::DrawFullBlack(1.0f);
        return;
    }
    //--This is a fade out from the normal map, menu does not render.
    else if(mShopFadeMode == FADE_INITIAL_OUT)
    {
        SugarBitmap::DrawFullBlack(mShopFadeTimer / (float)FADE_TICKS_OUT);
        return;
    }
    //--This is a fade from the shop to the normal map.
    else if(mShopFadeMode == FADE_FINAL_IN)
    {
        SugarBitmap::DrawFullBlack(1.0f - (mShopFadeTimer / (float)FADE_TICKS_OUT));
        return;
    }

    //--[Setup]
    //--Fast-access pointers.
    AdventureInventory *rInventory = AdventureInventory::Fetch();

    //--[Static Parts]
    //--Shop has a background behind it.
    SugarBitmap::DrawFullColor(StarlightColor::MapRGBAF(0.0f, 0.0f, 0.35f, 1.0f));

    //--[Render Splitting]
    //--Figure out which subsection should be rendering, and whether or not it is selected.
    int tBuySellCursor = mShopCursor;
    if(mShopModeType == AM_BUY)       tBuySellCursor = AM_SHOP_SELECT_BUY;
    if(mShopModeType == AM_SELL)      tBuySellCursor = AM_SHOP_SELECT_SELL;
    if(mShopModeType == AM_PURIFY)    tBuySellCursor = AM_SHOP_SELECT_PURIFY;
    if(mShopModeType == AM_GEMCUTTER) tBuySellCursor = AM_SHOP_SELECT_GEMCUTTER;

    //--Buy is currently highlighted.
    if(tBuySellCursor == AM_SHOP_SELECT_BUY)
    {
        //--Frame set.
        Images.VendorUI.rFramesBuy->Draw();

        //--Render the headers as whited/greyed based on state.
        Images.VendorUI.rHeadingFont->DrawText( 77.0f, 81.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Buy");
        StarlightColor::cxGrey.SetAsMixer();
        Images.VendorUI.rHeadingFont->DrawText(205.0f, 81.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Sell");
        Images.VendorUI.rHeadingFont->DrawText(336.0f, 81.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Purify");
        Images.VendorUI.rHeadingFont->DrawText(465.0f, 81.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Gems");
        StarlightColor::ClearMixer();

        //--Render the buy mode, but grey it out because we haven't actually activated the mode.
        if(mShopModeType == AM_SELECT_SHOP_MODE)
        {
            RenderShopBuy(false, 0.5f);
        }
        //--Render everything at full brightness.
        else
        {
            RenderShopBuy(true, 1.0f);
        }
        StarlightColor::ClearMixer();
    }
    //--Sell is currently highlighted.
    else if(tBuySellCursor == AM_SHOP_SELECT_SELL)
    {
        //--Frame set.
        Images.VendorUI.rFramesSell->Draw();

        //--Render the headers as whited/greyed based on state.
        StarlightColor::cxGrey.SetAsMixer();
        Images.VendorUI.rHeadingFont->DrawText( 77.0f, 81.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Buy");
        StarlightColor::ClearMixer();
        Images.VendorUI.rHeadingFont->DrawText(205.0f, 81.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Sell");
        StarlightColor::cxGrey.SetAsMixer();
        Images.VendorUI.rHeadingFont->DrawText(336.0f, 81.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Purify");
        Images.VendorUI.rHeadingFont->DrawText(465.0f, 81.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Gems");
        StarlightColor::ClearMixer();

        //--Render the sell mode, but grey it out because we haven't actually activated the mode.
        if(mShopModeType == AM_SELECT_SHOP_MODE)
        {
            RenderShopSell(false, 0.5f);
        }
        //--Render everything at full brightness.
        else
        {
            RenderShopSell(true, 1.0f);
        }
        StarlightColor::ClearMixer();
    }
    //--Purify is currently highlighted.
    else if(tBuySellCursor == AM_SHOP_SELECT_PURIFY)
    {
        //--Frame set.
        Images.VendorUI.rFramesPurify->Draw();

        //--Render the headers as whited/greyed based on state.
        StarlightColor::cxGrey.SetAsMixer();
        Images.VendorUI.rHeadingFont->DrawText( 77.0f, 81.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Buy");
        Images.VendorUI.rHeadingFont->DrawText(205.0f, 81.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Sell");
        StarlightColor::ClearMixer();
        Images.VendorUI.rHeadingFont->DrawText(336.0f, 81.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Purify");
        StarlightColor::cxGrey.SetAsMixer();
        Images.VendorUI.rHeadingFont->DrawText(465.0f, 81.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Gems");
        StarlightColor::ClearMixer();
    }
    //--Gemcutter is currently highlighted.
    else if(tBuySellCursor == AM_SHOP_SELECT_GEMCUTTER)
    {
        //--Frame set.
        Images.VendorUI.rFramesGemcutter->Draw();

        //--Render the headers as whited/greyed based on state.
        StarlightColor::cxGrey.SetAsMixer();
        Images.VendorUI.rHeadingFont->DrawText( 77.0f, 81.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Buy");
        Images.VendorUI.rHeadingFont->DrawText(205.0f, 81.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Sell");
        Images.VendorUI.rHeadingFont->DrawText(336.0f, 81.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Purify");
        StarlightColor::ClearMixer();
        Images.VendorUI.rHeadingFont->DrawText(465.0f, 81.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Gems");

        //--Render the gemcutter mode, but grey it out because we haven't actually activated the mode.
        if(mShopModeType == AM_SELECT_SHOP_MODE)
        {
            RenderGemcutterMode(false, 0.5f);
        }
        //--Render everything at full brightness.
        else
        {
            RenderGemcutterMode(true, 1.0f);
        }
        StarlightColor::ClearMixer();
    }

    //--[Common]
    //--Shows the name of the shop.
    Images.VendorUI.rHeadingFont->DrawText(29.0f, 19.0f, 0, 1.0f, mShopName);

    //--Platina.
    Images.VendorUI.rHeadingFont->DrawTextArgs(902.0f, 19.0f, 0, 1.0f, "Platina: %i", rInventory->GetPlatina());

    //--[Fade Handling]
    //--Fading in to the shop menu.
    if(mShopFadeMode == FADE_INITIAL_IN)
    {
        SugarBitmap::DrawFullBlack(1.0f - (mShopFadeTimer / (float)FADE_TICKS_OUT));
    }
    //--This is a fade from the shop to the normal map.
    else if(mShopFadeMode == FADE_FINAL_OUT)
    {
        SugarBitmap::DrawFullBlack(mShopFadeTimer / (float)FADE_TICKS_OUT);
    }
}
