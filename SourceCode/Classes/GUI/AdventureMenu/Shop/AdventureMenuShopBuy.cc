//--Base
#include "AdventureMenu.h"

//--Classes
#include "AdvCombat.h"
#include "AdvCombatEntity.h"
#include "AdventureItem.h"
#include "AdventureInventory.h"
#include "AdventureLevel.h"
#include "VisualLevel.h"

//--CoreClasses
#include "SugarAutoBuffer.h"
#include "SugarBitmap.h"
#include "SugarFont.h"
#include "SugarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "Subdivide.h"

//--Libraries
//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "DisplayManager.h"
#include "LuaManager.h"

//--[Shop Fading]
#define FADE_MODE_NONE 0
#define FADE_INITIAL_OUT 1
#define FADE_INITIAL_HOLD 2
#define FADE_INITIAL_IN 3
#define FADE_FINAL_OUT 4
#define FADE_FINAL_HOLD 5
#define FADE_FINAL_IN 6

#define FADE_TICKS_OUT 15
#define FADE_TICKS_HOLD 30
#define FADE_TICKS_IN 15

//--[System]
//--[Manipulators]
//--[Core Methods]
//--[Update]
void AdventureMenu::UpdateShopBuy()
{
    //--[Setup]
    //--Quick-access pointers.
    ControlManager *rControlManager = ControlManager::Fetch();
    AdventureInventory *rInventory = AdventureInventory::Fetch();

    //--Storage.
    int tOldCursor = mShopCursor;

    //--Always run this timer.
    mCharacterWalkTimer ++;

    //--[Exchange Mode]
    //--In exchanging mode, prompt the player.
    if(mIsExchanging)
    {
        //--If the player pushes "Activate", they will buy the item.
        if(rControlManager->IsFirstPress("Activate"))
        {
            //--Check the item's name. If it's UNLIMITED then we don't remove it from the shop.
            bool tRemovedEntry = false;
            AdventureItem *rPurchaseItem = (AdventureItem *)mShopInventory->GetElementBySlot(mShopCursor);
            if(!rPurchaseItem)
            {
            }
            else
            {
                //--Get the entry's name. If it's "UNLIMITED", the item is not consumed when purchased.
                const char *rEntryName = mShopInventory->GetNameOfElementBySlot(mShopCursor);
                if(rEntryName && !strcasecmp(rEntryName, "UNLIMITED"))
                {
                }
                //--Otherwise, remove the item.
                else
                {
                    tRemovedEntry = true;
                    mShopInventory->SetRandomPointerToThis(rPurchaseItem);
                    mShopInventory->LiberateRandomPointerEntry();
                }

                //--Put the item in the player's inventory.
                if(!rPurchaseItem->IsAdamantite())
                {
                    //--If the item was flagged as unlimited, we need to clone it before we register it.
                    if(!tRemovedEntry)
                    {
                        //--Run the item list code to create the item in question. This gives us a fresh copy.
                        rInventory->rLastReggedItem = NULL;
                        LuaManager::Fetch()->ExecuteLuaFile(AdventureLevel::xItemListPath, 2, "S", rPurchaseItem->GetName(), "N", 1.0f);
                    }
                    //--Item was not unlimited, register it as normal.
                    else
                    {
                        rInventory->RegisterItem(rPurchaseItem);
                    }
                }
                //--Adamantite. Has a special registering algorithm. If the item was flagged as "UNLIMITED" then we won't deallocate it.
                else
                {
                    rInventory->RegisterAdamantite(rPurchaseItem, tRemovedEntry);
                }

                //--Decrement the player's cash.
                rInventory->SetPlatina(rInventory->GetPlatina() - rPurchaseItem->GetValue());

                //--SFX.
                AudioManager::Fetch()->PlaySound("Menu|BuyOrSell");

                //--No items left to buy, cancel out of buy mode.
                if(mShopInventory->GetListSize() < 1)
                {
                    mShopCursor = 0;
                    mShopModeType = AM_SELECT_SHOP_MODE;
                }
                //--Recheck the cursor.
                else if(mShopCursor >= mShopInventory->GetListSize() && tRemovedEntry)
                {
                    mShopCursor = mShopInventory->GetListSize() - 1;
                }

                //--All cases end the exchange.
                mIsExchanging = false;
            }
        }
    }
    //--Selecting which item to buy.
    else
    {
        //--Down increases the cursor.
        if(rControlManager->IsFirstPress("Down"))
        {
            //--Increment if in range.
            mShopCursor ++;
            if(mShopCursor >= mShopInventory->GetListSize()) mShopCursor = mShopInventory->GetListSize() - 1;
            if(mShopCursor < 0) mShopCursor = 0;
        }
        //--Up decreases the cursor.
        else if(rControlManager->IsFirstPress("Up"))
        {
            //--Increment if in range.
            mShopCursor --;
            if(mShopCursor < 0) mShopCursor = 0;
        }

        //--Get the item currently highlighted.
        AdventureItem *rDescriptionItem = (AdventureItem *)mShopInventory->GetElementBySlot(mShopCursor);

        //--Left moves the comparison cursor down one.
        if(rControlManager->IsFirstPress("Left"))
        {
            //--Get the item under consideration.
            if(!rDescriptionItem) return;

            //--Attempt to decrement by one until a valid character is found.
            /*
            for(int i = 0; i < AC_PARTY_MAX - 1; i ++)
            {
                //--Compute slot.
                int tSlot = (mComparisonCharacter - i - 1) % AC_PARTY_MAX;

                //--Get the character.
                AdventureCombatEntity *rCharacter = AdventureCombatUI::Fetch()->GetPartyMember(tSlot);
                if(!rCharacter) continue;

                //--If it's usable by this character, stop here.
                if(rDescriptionItem->IsEquippableBy(rCharacter->GetName()))
                {
                    AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
                    mComparisonCharacter = tSlot;
                    return;
                }
            }*/
        }
        //--Right moves the comparison cursor up one.
        else if(rControlManager->IsFirstPress("Right"))
        {
            //--Get the item under consideration.
            if(!rDescriptionItem) return;

            //--Attempt to increment the cursor until a valid character is found.
            /*
            for(int i = 0; i < AC_PARTY_MAX - 1; i ++)
            {
                //--Compute slot.
                int tSlot = (mComparisonCharacter + i + 1) % AC_PARTY_MAX;

                //--Get the character.
                AdventureCombatEntity *rCharacter = AdventureCombatUI::Fetch()->GetPartyMember(tSlot);
                if(!rCharacter) continue;

                //--If it's usable by this character, stop here.
                if(rDescriptionItem->IsEquippableBy(rCharacter->GetName()))
                {
                    AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
                    mComparisonCharacter = tSlot;
                    return;
                }
            }*/
        }

        //--Activate will purchase the item.
        if(rControlManager->IsFirstPress("Activate"))
        {
            //--Check the item.
            AdventureItem *rBuyingItem = (AdventureItem *)mShopInventory->GetElementBySlot(mShopCursor);

            //--If the player can afford the item, add it to their inventory.
            if(rBuyingItem && rBuyingItem->GetValue() <= rInventory->GetPlatina())
            {
                mIsExchanging = true;
                AudioManager::Fetch()->PlaySound("Menu|Select");
            }
            //--Otherwise, play a sound effect to indicate failure.
            else
            {
                AudioManager::Fetch()->PlaySound("Menu|Failed");
            }
        }
    }

    //--If the item changes, recheck the comparison character case. Also play sound effects.
    if(tOldCursor != mShopCursor)
    {
        //--[SFX]
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");

        //--[Recentering]
        //--On a decrement:
        if(mShopCursor < tOldCursor)
        {
            //--Get where the cursor is on the page.
            int tPosition = mShopCursor - mShopSkip;
            if(tPosition < 2) mShopSkip = mShopCursor - 2;

            //--Clamp.
            if(mShopSkip < 0) mShopSkip = 0;
        }
        //--On an increment:
        else if(mShopCursor > tOldCursor)
        {
            //--Get where the cursor is on the page.
            int tPosition = mShopCursor - mShopSkip;
            if(tPosition > AM_SHOP_ITEMS_PER_PAGE - 3) mShopSkip = mShopCursor - (AM_SHOP_ITEMS_PER_PAGE - 3);

            //--Clamp.
            if(mShopSkip > mShopInventory->GetListSize() - AM_SHOP_ITEMS_PER_PAGE) mShopSkip = mShopInventory->GetListSize() - AM_SHOP_ITEMS_PER_PAGE;
        }

        //--[Rechecking Comparison Character]
        //--Flag.
        int tPreviousComparison = mComparisonCharacter;
        mMultipleComparisonCharacters = false;

        //--Get the item under consideration.
        AdventureItem *rDescriptionItem = (AdventureItem *)mShopInventory->GetElementBySlot(mShopCursor);
        if(!rDescriptionItem) return;

        //--If the item is not a piece of equipment, we can't compare it to anything.
        if(!rDescriptionItem->IsEquipment()) return;

        //--Otherwise, start at character zero and run through the list.
        int tTotalCharacters = 0;
        /*
        for(int i = 0; i < AC_PARTY_MAX; i ++)
        {
            //--Get the character.
            AdventureCombatEntity *rCharacter = AdventureCombatUI::Fetch()->GetPartyMember(i);
            if(!rCharacter) continue;

            //--If it's usable by this character, stop here, flag.
            if(rDescriptionItem->IsEquippableBy(rCharacter->GetName()))
            {
                //--Flag. The first character found becomes the comparison character.
                if(tTotalCharacters == 0) mComparisonCharacter = i;

                //--The previous character, even if not the first, becomes the comparison character. This way
                //  the UI does not switch comparison characters if still valid.
                if(tPreviousComparison == i) mComparisonCharacter = i;

                //--Track the total number of comparable characters.
                tTotalCharacters ++;
            }
        }*/

        //--If the total usable characters is more than 1, set this flag.
        if(tTotalCharacters > 1) mMultipleComparisonCharacters = true;
    }
}

//--[Drawing]
void AdventureMenu::RenderShopBuy(bool pIsActiveMode, float pColorMix)
{
    //--[Documentation]
    //--Render the buying portion of the submenu. The static backing and text are already rendered, we just need to
    //  render the item icons and descriptions of the highlighted item if applicable.
    if(!Images.mIsReady) return;

    //--[Shop Inventory Listing]
    //--Similar to how the inventory renders information as a table. Item properties are not listed, though.
    float cHeaderY = 124.0f;
    float cIconX  = 30.0f;
    float cIconW  = 22.0f;
    float cFaceX = cIconX + cIconW;
    float cFaceW = 32.0f;
    float cNameX  = cFaceX + cFaceW;
    float cValueX = 470.0f;
    float cValueXRight = 542.0f;
    float cGemsX = 555.0f;

    //--Text headers.
    StarlightColor::SetMixer(pColorMix, pColorMix, pColorMix, 1.0f);
    Images.EquipmentUI.rHeadingFont->DrawText(cIconX,  cHeaderY, 0, 1.0f, "Name");
    Images.EquipmentUI.rHeadingFont->DrawText(cValueX, cHeaderY, 0, 1.0f, "Cost");
    Images.EquipmentUI.rHeadingFont->DrawText(cGemsX,  cHeaderY, 0, 1.0f, "Gems");

    //--Y positioning
    float cTop = cHeaderY + 44.0f;
    float tCurrentY = cTop;
    float cSpcY = 23.0f;

    //--Variables.
    int tRendersLeft = AM_SHOP_ITEMS_PER_PAGE;
    int tSkipsLeft = mShopSkip;

    //--[Rendering Loop]
    int tCurrentItem = 0;
    AdventureItem *rItem = (AdventureItem *)mShopInventory->PushIterator();
    while(rItem)
    {
        //--If this item should not render, skip it.
        if(tSkipsLeft > 0)
        {
            tCurrentItem ++;
            tSkipsLeft --;
            rItem = (AdventureItem *)mShopInventory->AutoIterate();
            continue;
        }

        //--[Backing]
        if(tCurrentItem % 2 == 1)
        {
            SugarBitmap::DrawRectFill(15, tCurrentY, 728, tCurrentY + cSpcY, StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, 1.0f));
            StarlightColor::SetMixer(pColorMix, pColorMix, pColorMix, 1.0f);
        }

        //--[Item Icon]
        //--Render the item's icon here. It's a 22px entry.
        SugarBitmap *rItemIcon = rItem->GetIconImage();
        if(rItemIcon) rItemIcon->Draw(cIconX, tCurrentY);

        //--[Cursor Handling]
        //--If this is the selected item, render the cursor over it. If the mode is inactive, don't render this.
        //  Inactive mode is used when the player is hovering on Buy mode, but hasn't entered it yet.
        if(tCurrentItem == mShopCursor && pIsActiveMode)
        {
            //--Render the item description.
            RenderItemDescription(rItem);

            //--Render the properties and comparisons.
            RenderShopBuyProperties(rItem, mComparisonCharacter, pColorMix);

            //--Player cannot afford the item, so grey it out.
            if(rItem->GetValue() > AdventureInventory::Fetch()->GetPlatina())
            {
                StarlightColor::SetMixer(1.0f * pColorMix, 0.0f * pColorMix, 0.0f * pColorMix, 1.0f);
            }
            //--Regular mixing.
            else
            {
                StarlightColor::SetMixer(pColorMix, pColorMix, pColorMix, 1.0f);
            }

            //--Tint the color.
            StarlightColor::SetMixer(1.0f, 0.0f, 0.0f, 1.0f);
        }
        //--Not selected or is inactive mode.
        else
        {
            //--Player cannot afford the item, so grey it out.
            if(rItem->GetValue() > AdventureInventory::Fetch()->GetPlatina())
            {
                StarlightColor::SetMixer(0.5f * pColorMix, 0.5f * pColorMix, 0.5f * pColorMix, 1.0f);
            }
            //--Regular mixing.
            else
            {
                StarlightColor::SetMixer(pColorMix, pColorMix, pColorMix, 1.0f);
            }
        }

        //--[Item Name]
        //--Name.
        Images.EquipmentUI.rMainlineFont->DrawText(cNameX, tCurrentY, 0, 1.0f, rItem->GetName());

        //--[Item Cost]
        //--Right-align.
        char tBuffer[32];
        sprintf(tBuffer, "%i", rItem->GetValue());

        //--Resolve position and render.
        float cXPos = cValueXRight - Images.EquipmentUI.rMainlineFont->GetTextWidth(tBuffer);
        Images.EquipmentUI.rMainlineFont->DrawTextArgs(cXPos, tCurrentY, 0, 1.0f, tBuffer);
        StarlightColor::SetMixer(pColorMix, pColorMix, pColorMix, 1.0f);

        //--[Gem Slots]
        //--Items which have gem slots render them here. They are always empty in the shop UI.
        float cGemW = 23.0f;
        int tGemSlots = rItem->GetGemSlots();
        for(int i = 0; i < tGemSlots; i ++)
        {
            Images.VendorUI.rGemEmpty22px->Draw(cGemsX + (i * cGemW), tCurrentY);
        }

        //--If the renders have run out, stop here.
        tRendersLeft --;
        if(tRendersLeft < 1)
        {
            mShopInventory->PopIterator();
            break;
        }

        //--Next.
        tCurrentItem ++;
        tCurrentY = tCurrentY + cSpcY;
        rItem = (AdventureItem *)mShopInventory->AutoIterate();
    }

    //--[Scrollbar]
    //--Render a scrollbar to indicate how much of the inventory is represented.
    if(mShopInventory->GetListSize() > AM_SHOP_ITEMS_PER_PAGE)
    {
        //--Binding.
        Images.VendorUI.rScrollbarFront->Bind();

        //--Scrollbar front. Stretches dynamically.
        float cScrollTop = 158.0f;
        float cScrollHei = 366.0f;

        //--Determine what percentage of the inventory is presently represented.
        int tMaxOffset = mShopInventory->GetListSize();
        float cPctTop = (mShopSkip                         ) / (float)tMaxOffset;
        float cPctBot = (mShopSkip + AM_SHOP_ITEMS_PER_PAGE) / (float)tMaxOffset;

        //--Positions and Constants.
        float cLft = 699.0f;
        float cRgt = cLft + Images.InventoryUI.rScrollbarFront->GetWidth();
        float cEdg = 6.0f / (float)Images.InventoryUI.rScrollbarFront->GetHeight();

        //--Compute where the bar should be.
        float cTopTop = cScrollTop + (cPctTop * cScrollHei);
        float cTopBot = cTopTop + 6.0f;
        float cBotBot = cScrollTop + (cPctBot * cScrollHei);
        float cBotTop = cBotBot - 6.0f;

        glBegin(GL_QUADS);
            //--Render the top of the bar.
            glTexCoord2f(0.0f, 0.0f); glVertex2f(cLft, cTopTop);
            glTexCoord2f(1.0f, 0.0f); glVertex2f(cRgt, cTopTop);
            glTexCoord2f(1.0f, cEdg); glVertex2f(cRgt, cTopBot);
            glTexCoord2f(0.0f, cEdg); glVertex2f(cLft, cTopBot);

            //--Render the bottom of the bar.
            glTexCoord2f(0.0f, 1.0f - cEdg); glVertex2f(cLft, cBotTop);
            glTexCoord2f(1.0f, 1.0f - cEdg); glVertex2f(cRgt, cBotTop);
            glTexCoord2f(1.0f, 1.0f);        glVertex2f(cRgt, cBotBot);
            glTexCoord2f(0.0f, 1.0f);        glVertex2f(cLft, cBotBot);

            //--Render the middle of the bar.
            glTexCoord2f(0.0f, cEdg);        glVertex2f(cLft, cTopBot);
            glTexCoord2f(1.0f, cEdg);        glVertex2f(cRgt, cTopBot);
            glTexCoord2f(1.0f, 1.0f - cEdg); glVertex2f(cRgt, cBotTop);
            glTexCoord2f(0.0f, 1.0f - cEdg); glVertex2f(cLft, cBotTop);
        glEnd();


        //--Backing. Already positioned.
        Images.VendorUI.rScrollbarBack->Draw();
    }

    //--[Exchange Dialogue]
    //--In exchange mode, render the confirmation dialogue.
    if(mIsExchanging)
    {
        //--Get the item and verify it.
        AdventureItem *rPurchaseItem = (AdventureItem *)mShopInventory->GetElementBySlot(mShopCursor);
        if(!rPurchaseItem) return;

        //--Black out.
        SugarBitmap::DrawFullColor(StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, 0.75f));

        //--Overlay.
        Images.VendorUI.rConfirmOverlay->Draw();

        //--Buffer text.
        char tBuffer[128];
        sprintf(tBuffer, "Purchase %s for %i platina?", rPurchaseItem->GetName(), rPurchaseItem->GetValue());

        //--Check the length.
        float tTextLength = Images.VendorUI.rHeadingFont->GetTextWidth(tBuffer);

        //--Width is not too long. Render normally.
        if(tTextLength < 500.0f)
        {
            Images.VendorUI.rHeadingFont->DrawText(683.0f, 210.0f, SUGARFONT_AUTOCENTER_X, 1.0f, tBuffer);
        }
        //--Too long, split it into two lines.
        else
        {
            Images.VendorUI.rHeadingFont->DrawTextArgs(683.0f, 210.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Purchase %s", rPurchaseItem->GetName());
            Images.VendorUI.rHeadingFont->DrawTextArgs(683.0f, 245.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "for %i Platina?", rPurchaseItem->GetValue());
        }

        //--Other text.
        Images.VendorUI.rMainlineFont->DrawTextArgs(683.0f, 345.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Z to confirm");
        Images.VendorUI.rMainlineFont->DrawTextArgs(683.0f, 365.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "X to cancel");
    }
}

void AdventureMenu::RenderShopBuyProperties(AdventureItem *pItem, int pComparisonCharacter, float pColorMix)
{
    //--[Documentation and Setup]
    //--Renders the properties of the item on the right side of the screen. For items that can be equipped, we also
    //  show which of the characters in the party can equip that item and comparisons with what they have equipped.
    //--Not all items can be easily compared, but we do our best!
    //--It is assumed that the Buy menu is active when this is taking place. If it is not active, no item should be selected.
    if(!pItem) return;

    //--Fast-access pointers.
    AdvCombat *rAdventureCombat = AdvCombat::Fetch();

    //--[Basic Information]
    //--Render how many of the item we already have, if applicable.
    int tCurrentStock = AdventureInventory::Fetch()->GetCountOf(pItem->GetName());
    Images.EquipmentUI.rMainlineFont->DrawTextArgs(761.0f, 130.0f, 0, 1.0f, "You have: %i", tCurrentStock);

    //--What type the item is:
    const char *rTypeBuffer = pItem->GetItemTypeString();
    Images.EquipmentUI.rMainlineFont->DrawTextArgs(761.0f, 150.0f, 0, 1.0f, "Type: %s", rTypeBuffer);

    //--[Equipment Comparison]
    //--If this is a piece of equipment, we need to show who can equip it and what properties it has.
    if(pItem->IsEquipment())
    {
        //--Header:
        Images.EquipmentUI.rMainlineFont->DrawTextArgs(761.0f, 170.0f, 0, 1.0f, "Usable:");

        //--Render instructions
        Images.StatusUI.rVerySmallFont->DrawText(755.0f, 306.0f, "<- -> Change Character");

        //--Get the slot.
        const char *rEquipSlot = pItem->GetEquipSlot();

        //--Go through the party members and show their sprites. If they can equip the item, the sprite is highlighted.
        AdvCombatEntity *rCompareEntity = NULL;
        /*
        for(int i = 0; i < AC_PARTY_MAX; i ++)
        {
            //--Skip empty slots.
            AdventureCombatEntity *rEntity = rCombatUI->GetPartyMember(i);
            if(!rEntity) continue;

            //--Rendering variables.
            float tX = 735.0f + (50 * i);
            float tY = 190.0f;
            float cScale = 3.0f;

            //--Can the character equip it? If not, show their zero-frame and grey them out.
            if(!pItem->IsEquippableBy(rEntity->GetName()))
            {
                SugarBitmap *rVendorFrame = rEntity->GetVendorSprite(0);
                if(rVendorFrame)
                {
                    StarlightColor::cxGrey.SetAsMixer();
                    glTranslatef(tX, tY, 0.0f);
                    glScalef(cScale, cScale, 1.0f);
                    rVendorFrame->Draw();
                    glScalef(1.0f / cScale, 1.0f / cScale, 1.0f);
                    glTranslatef(-tX, -tY, 0.0f);
                    StarlightColor::ClearMixer();
                }
            }
            //--Character can equip it, so render the comparison. We animate them walking to draw
            //  extra attention to them.
            else
            {
                int tFrame = (mCharacterWalkTimer / (8 * 4)) % 4;
                SugarBitmap *rVendorFrame = rEntity->GetVendorSprite(tFrame);
                if(rVendorFrame)
                {
                    glTranslatef(tX, tY, 0.0f);
                    glScalef(cScale, cScale, 1.0f);
                    rVendorFrame->Draw();
                    glScalef(1.0f / cScale, 1.0f / cScale, 1.0f);
                    glTranslatef(-tX, -tY, 0.0f);
                }

                //--If this is the comparison character and they can equip it, store them.
                if(i == pComparisonCharacter) rCompareEntity = rEntity;
            }

            //--Render the comparison cursor under the entity in question.
            if(i == pComparisonCharacter)
            {
                Images.VendorUI.rCompareCursor->Draw(tX + 32.0f, tY + 99.0f);
            }
        }*/

        //--Render the properties display. This is similar to the gemcutting UI. Note that two columns are shown for
        //  comparison, as items and accessories have two equip slots.
        float cBaseValueX = 987.0f;
        float cBaseNameCX = cBaseValueX + (120.0f * 0.50f);
        float cArrowX = cBaseValueX + 110.0f;
        float cCompareValueAX = cArrowX + 10.0f;
        float cCompareValueACX = cCompareValueAX + (120.0f * 0.50f);
        float cCompareValueBX = cArrowX + 10.0f + 120.0f;
        float cCompareValueBCX = cCompareValueBX + (120.0f * 0.50f);

        //--[Header]
        //--Base.
        Images.EquipmentUI.rMainlineFont->DrawText(1147, 123.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Comparison");

        //--Item's icon/name.
        SugarBitmap *rItemIcon = pItem->GetIconImage();
        if(rItemIcon) rItemIcon->Draw(987, 160.0f - 7.0f);
        const char *rTopName = pItem->GetTopName();
        const char *rBotName = pItem->GetBotName();
        if(rTopName && !rBotName)
        {
            Images.StatusUI.rVerySmallFont->DrawTextArgs(cBaseNameCX, 160.0f, SUGARFONT_AUTOCENTER_X, 1.0f, rTopName);
        }
        else if(rTopName && rBotName)
        {
            Images.StatusUI.rVerySmallFont->DrawTextArgs(cBaseNameCX, 147.0f, SUGARFONT_AUTOCENTER_X, 1.0f, rTopName);
            Images.StatusUI.rVerySmallFont->DrawTextArgs(cBaseNameCX, 160.0f, SUGARFONT_AUTOCENTER_X, 1.0f, rBotName);
        }

        //--[Icons]
        //--Render the statistic icons on the right side of the screen. Also store their Y positions.
        float cImgHeadersStartY = 180.0f;
        float cImgHeadersSpaceY = 20.0f;
        float cImgHeadersX = 960.0f;
        float cImgHeadersY[AM_INV_HEADER_TOTAL];
        for(int i = 0; i < AM_INV_HEADER_TOTAL; i ++)
        {
            cImgHeadersY[i] = cImgHeadersStartY + (i * cImgHeadersSpaceY);
            Images.InventoryUI.rImageHeaders[i]->Draw(cImgHeadersX, cImgHeadersStartY + (i * cImgHeadersSpaceY));
        }

        //--Get the comparison items.
        AdventureItem *rCompareA = NULL;
        AdventureItem *rCompareB = NULL;
        if(rCompareEntity)
        {
            /*
            if(tEquipSlot == ADITEM_EQUIP_WEAPON)
            {
                rCompareA = rCompareEntity->GetWeapon();
            }
            else if(tEquipSlot == ADITEM_EQUIP_ARMOR)
            {
                rCompareA = rCompareEntity->GetArmor();
            }
            else if(tEquipSlot == ADITEM_EQUIP_ACCESSORY)
            {
                rCompareA = rCompareEntity->GetAccessoryA();
                rCompareB = rCompareEntity->GetAccessoryB();
            }
            else if(tEquipSlot == ADITEM_EQUIP_ITEM)
            {
                rCompareA = rCompareEntity->GetItemA();
                rCompareB = rCompareEntity->GetItemB();
            }*/
        }

        //--Render which items are compared to what.
        /*
        if(tEquipSlot == ADITEM_EQUIP_WEAPON || tEquipSlot == ADITEM_EQUIP_ARMOR)
        {
            //--Has an item.
            if(rCompareA)
            {
                SugarBitmap *rItemIcon = rCompareA->GetIconImage();
                if(rItemIcon) rItemIcon->Draw(cCompareValueAX, 160.0f - 7.0f);
                const char *rTopName = rCompareA->GetTopName();
                const char *rBotName = rCompareA->GetBotName();
                if(rTopName && !rBotName)
                {
                    Images.StatusUI.rVerySmallFont->DrawTextArgs(cCompareValueACX, 160.0f, SUGARFONT_AUTOCENTER_X, 1.0f, rTopName);
                }
                else if(rTopName && rBotName)
                {
                    Images.StatusUI.rVerySmallFont->DrawTextArgs(cCompareValueACX, 147.0f, SUGARFONT_AUTOCENTER_X, 1.0f, rTopName);
                    Images.StatusUI.rVerySmallFont->DrawTextArgs(cCompareValueACX, 160.0f, SUGARFONT_AUTOCENTER_X, 1.0f, rBotName);
                }
            }
            //--Unequipped.
            else
            {
                Images.StatusUI.rVerySmallFont->DrawText(cCompareValueACX, 160.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "(Unequipped)");
            }
        }
        else
        {
            //--A-case.
            if(rCompareA)
            {
                SugarBitmap *rItemIcon = rCompareA->GetIconImage();
                if(rItemIcon) rItemIcon->Draw(cCompareValueAX, 160.0f - 7.0f);
                const char *rTopName = rCompareA->GetTopName();
                const char *rBotName = rCompareA->GetBotName();
                if(rTopName && !rBotName)
                {
                    Images.StatusUI.rVerySmallFont->DrawTextArgs(cCompareValueACX, 160.0f, SUGARFONT_AUTOCENTER_X, 1.0f, rTopName);
                }
                else if(rTopName && rBotName)
                {
                    Images.StatusUI.rVerySmallFont->DrawTextArgs(cCompareValueACX, 147.0f, SUGARFONT_AUTOCENTER_X, 1.0f, rTopName);
                    Images.StatusUI.rVerySmallFont->DrawTextArgs(cCompareValueACX, 160.0f, SUGARFONT_AUTOCENTER_X, 1.0f, rBotName);
                }
            }
            //--Unequipped.
            else
            {
                Images.StatusUI.rVerySmallFont->DrawText(cCompareValueACX, 160.0f, 0, 1.0f, "(None)");
            }

            //--B-case.
            if(rCompareB)
            {
                SugarBitmap *rItemIcon = rCompareB->GetIconImage();
                if(rItemIcon) rItemIcon->Draw(cCompareValueBX, 160.0f - 7.0f);
                const char *rTopName = rCompareB->GetTopName();
                const char *rBotName = rCompareB->GetBotName();
                if(rTopName && !rBotName)
                {
                    Images.StatusUI.rVerySmallFont->DrawTextArgs(cCompareValueBCX, 160.0f, SUGARFONT_AUTOCENTER_X, 1.0f, rTopName);
                }
                else if(rTopName && rBotName)
                {
                    Images.StatusUI.rVerySmallFont->DrawTextArgs(cCompareValueBCX, 147.0f, SUGARFONT_AUTOCENTER_X, 1.0f, rTopName);
                    Images.StatusUI.rVerySmallFont->DrawTextArgs(cCompareValueBCX, 160.0f, SUGARFONT_AUTOCENTER_X, 1.0f, rBotName);
                }
            }
            //--Unequipped.
            else
            {
                Images.StatusUI.rVerySmallFont->DrawText(cCompareValueBCX, 160.0f, 0, 1.0f, "(None)");
            }
        }*/

        //--Colors.
        StarlightColor cColIncrease;
        cColIncrease.SetRGBAF(0.0f, 1.0f, 0.0f, 1.0f);
        StarlightColor cColDecrease;
        cColDecrease.SetRGBAF(1.0f, 0.0f, 0.0f, 1.0f);

        //--Minor worthless optimization: Switch for-loops now instead of later! Slight speed increase! Waste of code!
        if(!rCompareA && !rCompareB)
        {
            for(int i = 0; i < AM_INV_HEADER_TOTAL; i ++)
            {
                int cBonus = pItem->GetPropertyByInventoryHeaders(i, true);
                if(cBonus != 0)
                {
                    Images.StatusUI.rVerySmallFont->DrawTextArgs(cBaseNameCX, cImgHeadersY[i], SUGARFONT_AUTOCENTER_X, 1.0f, "%i", cBonus);
                }
            }
        }
        //--Comparison A:
        else if(rCompareA && !rCompareB)
        {
            for(int i = 0; i < AM_INV_HEADER_TOTAL; i ++)
            {
                int cBonusBase = pItem->GetPropertyByInventoryHeaders(i, true);
                int cBonusA    = rCompareA->GetPropertyByInventoryHeaders(i, true);
                if(cBonusBase != 0 || cBonusA != 0)
                {
                    if(cBonusBase > cBonusA) cColIncrease.SetAsMixer();
                    else if(cBonusBase < cBonusA) cColDecrease.SetAsMixer();
                    Images.StatusUI.rVerySmallFont->DrawTextArgs(cBaseNameCX,     cImgHeadersY[i], SUGARFONT_AUTOCENTER_X, 1.0f, "%i", cBonusBase);
                    StarlightColor::ClearMixer();
                    Images.StatusUI.rVerySmallFont->DrawText    (cArrowX,         cImgHeadersY[i],                      0, 1.0f, "<-");
                    Images.StatusUI.rVerySmallFont->DrawTextArgs(cCompareValueACX, cImgHeadersY[i], SUGARFONT_AUTOCENTER_X, 1.0f, "%i", cBonusA);
                }
            }
        }
        //--Comparison B:
        else if(!rCompareA && rCompareB)
        {
            for(int i = 0; i < AM_INV_HEADER_TOTAL; i ++)
            {
                int cBonusBase = pItem->GetPropertyByInventoryHeaders(i, true);
                int cBonusB    = rCompareB->GetPropertyByInventoryHeaders(i, true);
                if(cBonusBase != 0 || cBonusB != 0)
                {
                    if(cBonusBase > cBonusB) cColIncrease.SetAsMixer();
                    else if(cBonusBase < cBonusB) cColDecrease.SetAsMixer();
                    Images.StatusUI.rVerySmallFont->DrawTextArgs(cBaseNameCX,     cImgHeadersY[i], SUGARFONT_AUTOCENTER_X, 1.0f, "%i", cBonusBase);
                    StarlightColor::ClearMixer();
                    Images.StatusUI.rVerySmallFont->DrawText    (cArrowX,         cImgHeadersY[i],                      0, 1.0f, "<-");
                    Images.StatusUI.rVerySmallFont->DrawTextArgs(cCompareValueBCX, cImgHeadersY[i], SUGARFONT_AUTOCENTER_X, 1.0f, "%i", cBonusB);
                }
            }
        }
        //--Compare A and B:
        else if(rCompareA && rCompareB)
        {
            for(int i = 0; i < AM_INV_HEADER_TOTAL; i ++)
            {
                int cBonusBase = pItem->GetPropertyByInventoryHeaders(i, true);
                int cBonusA    = rCompareA->GetPropertyByInventoryHeaders(i, true);
                int cBonusB    = rCompareB->GetPropertyByInventoryHeaders(i, true);
                if(cBonusBase != 0 || cBonusA != 0 || cBonusB != 0)
                {
                    Images.StatusUI.rVerySmallFont->DrawTextArgs(cBaseNameCX,     cImgHeadersY[i], SUGARFONT_AUTOCENTER_X, 1.0f, "%i", cBonusBase);
                    Images.StatusUI.rVerySmallFont->DrawText    (cArrowX,         cImgHeadersY[i],                      0, 1.0f, "<-");
                    Images.StatusUI.rVerySmallFont->DrawTextArgs(cCompareValueACX, cImgHeadersY[i], SUGARFONT_AUTOCENTER_X, 1.0f, "%i", cBonusA);
                    Images.StatusUI.rVerySmallFont->DrawTextArgs(cCompareValueBCX, cImgHeadersY[i], SUGARFONT_AUTOCENTER_X, 1.0f, "%i", cBonusB);
                }
            }
        }
    }
}
