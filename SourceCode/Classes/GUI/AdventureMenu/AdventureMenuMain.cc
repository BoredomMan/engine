//--Base
#include "AdventureMenu.h"

//--Classes
#include "AdvCombat.h"
#include "AdvCombatEntity.h"
#include "AdventureInventory.h"
#include "VisualLevel.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarFont.h"
#include "SugarLinkedList.h"

//--Definitions
#include "Global.h"

//--Libraries
//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "OptionsManager.h"

//--[Local Definitions]
int ComputeAbilityCursorMax(SugarLinkedList *pAbilityList);

//--[Manipulators]
void AdventureMenu::SetToMainMenu()
{
    mCurrentMode = AM_MODE_BASE;
    mCurrentCursor = AM_MAIN_ITEMS;
}

//--[Update]
void AdventureMenu::UpdateMainMenu()
{
    //--Standard update for the cursor. Right and Left controls are enabled. Wrapping is disabled.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Up.
    if(rControlManager->IsFirstPress("Up"))
    {
        if(mCurrentCursor >= 1)
        {
            mCurrentCursor -= 1;
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
    }
    //--Down!
    if(rControlManager->IsFirstPress("Down"))
    {
        if(mCurrentCursor < AM_MAIN_TOTAL - 1)
        {
            mCurrentCursor += 1;
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
    }

    //--Activate, enter the given submenu.
    if(rControlManager->IsFirstPress("Activate"))
    {
        if(mCurrentCursor == AM_MAIN_ITEMS)
        {
            SetToItemsMenu();
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        else if(mCurrentCursor == AM_MAIN_EQUIPMENT)
        {
            SetToEquipMenu();
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        else if(mCurrentCursor == AM_MAIN_STATUS)
        {
            if(AdvCombat::Fetch()->GetActivePartyCount() > 0)
            {
                //--Recompute the max ability cursor.
                AdvCombatEntity *rActiveEntity = AdvCombat::Fetch()->GetActiveMemberI(0);
                if(!rActiveEntity) return;
                mAbilityCursorMax = ComputeAbilityCursorMax(rActiveEntity->GetAbilityList());

                //--Set mode.
                mStatusCursor = 0;
                mCurrentMode = AM_MODE_STATUS;
                AudioManager::Fetch()->PlaySound("Menu|Select");
            }
        }
        else if(mCurrentCursor == AM_MAIN_HEAL)
        {
            SetToHealMode();
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        else if(mCurrentCursor == AM_MAIN_MAP)
        {
            //--No map? Fail.
            if(!rActiveMap && mMapLayerList->GetListSize() < 1)
            {
                AudioManager::Fetch()->PlaySound("Menu|Failed");
            }
            //--Activate map mode.
            else
            {
                InitializeMapMode();
                AudioManager::Fetch()->PlaySound("Menu|Select");
            }
        }
        else if(mCurrentCursor == AM_MAIN_OPTIONS)
        {
            SetToOptionsMode();
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        else if(mCurrentCursor == AM_MAIN_QUIT)
        {
            SetToQuitMenu();
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
    }
    //--Cancel, hides this object.
    else if(rControlManager->IsFirstPress("Cancel"))
    {
        Hide();
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }
}

//--[Worker Function]
//--Renders buttons on the left side. They stick out if they are selected.
void RenderMainMenuButton(SugarBitmap *pBtnImg, SugarFont *pRenderFont, int pSlot, bool pIsExtended, bool pIsGreyed, const char *pText)
{
    //--Error check.
    if(!pBtnImg || !pRenderFont || !pText) return;

    //--Constants.
    float cBtnTextX = 2.0f;
    float cBtnTextY = 31.0f;
    float cBtnStepY = 67.0f;
    float cBtnIndent = 31.0f;

    //--Color.
    if(pIsGreyed) StarlightColor::SetMixer(0.5f, 0.5f, 0.5f, 1.0f);

    //--Get text width.
    float cMaxWid = 137.0f;
    float cXScale = 1.0f;
    float cTextWid = pRenderFont->GetTextWidth(pText);
    if(cTextWid > cMaxWid)
    {
        cXScale = cMaxWid / cTextWid;
    }
    else
    {
        cBtnTextX = 138.0f - cTextWid;
    }

    //--Compute positions.
    float cBtnX = -cBtnIndent;
    float cTxtX = cBtnTextX;
    float cTxtY = cBtnTextY + (cBtnStepY * pSlot);
    if(pIsExtended)
    {
        cBtnX = cBtnX + cBtnIndent;
        cTxtX = cTxtX + cBtnIndent;
    }

    //--Render the button.
    pBtnImg->Draw(cBtnX, cBtnStepY * pSlot);

    //--Text rendering.
    glTranslatef(cTxtX, cTxtY, 0.0f);
    glScalef(cXScale, 1.0f, 1.0f);
    pRenderFont->DrawText(0.0f, 0.0f, 0, 1.0f, pText);
    glScalef(1.0f / cXScale, 1.0f, 1.0f);
    glTranslatef(-cTxtX, -cTxtY, 0.0f);

    //--Clean.
    StarlightColor::ClearMixer();
}

//--[Drawing]
void AdventureMenu::RenderMainMenu()
{
    //--[Documentation and Setup]
    //--This menu allows the player to access the submenus. It shows cash, crafting items, catalysts, and party status.
    if(!Images.mIsReady) return;

    //--Constants
    float cCenterX = VIRTUAL_CANVAS_X * 0.50f;

    //--Fast-access pointers.
    AdventureInventory *rInventory = AdventureInventory::Fetch();

    //--Text sizes.
    float cMainlineTextHeight = Images.BaseMenu.rMainlineFont->GetTextHeight();

    //--Rendering flag sets.
    uint32_t cFlagsL = 0;
    uint32_t cFlagsR = SUGARFONT_RIGHTALIGN_X;

    //--[Static Parts]
    //--Always render in the same position.
    Images.BaseMenu.rFooter->Draw();
    Images.BaseMenu.rHeader->Draw();
    Images.BaseMenu.rInventoryBacks->Draw();
    Images.BaseMenu.rInventoryBanners->Draw();
    Images.BaseMenu.rPlatinaBanner->Draw();

    //--Static Heading
    Images.BaseMenu.rHeadingFont->DrawTextArgs(cCenterX, 37.0f, SUGARFONT_AUTOCENTER_XY, 1.0f, "Menu");

    //--[Platina Display]
    //--Render how much cash the player has.
    float cPlatinaPosX = Images.BaseMenu.rPlatinaText->GetXOffset();
    float cPlatinaPosY = Images.BaseMenu.rPlatinaText->GetYOffset();
    Images.BaseMenu.rHeadingFont->DrawTextArgs(cPlatinaPosX, cPlatinaPosY, 0, 1.0f, "Platina: %i", rInventory->GetPlatina());

    //--[Adamantite Display]
    //--Show the adamantite counts.
    float cAdamantiteX = 1146.0f;
    float cAdamantiteT = 1166.0f;
    float cAdamantiteR = 1356.0f;
    float cAdamantiteY =  150.0f;
    float cAdamantiteSpacingY = cMainlineTextHeight + 2.0f;
    for(int i = 0; i < CRAFT_ADAMANTITE_TOTAL; i ++)
    {
        Images.BaseMenu.rAdamantiteImg[i]->Draw(cAdamantiteX, cAdamantiteY + (cAdamantiteSpacingY * i) + 3.0f);
    }
    Images.BaseMenu.rHeadingFont->DrawTextArgs(1251.0f, 104.0f, SUGARFONT_AUTOCENTER_XY, 1.0f, "Adamantite");
    Images.BaseMenu.rMainlineFont->DrawTextArgs(cAdamantiteT, cAdamantiteY + (cAdamantiteSpacingY * 0.00f), cFlagsL, 1.0f, "Powder");
    Images.BaseMenu.rMainlineFont->DrawTextArgs(cAdamantiteR, cAdamantiteY + (cAdamantiteSpacingY * 0.00f), cFlagsR, 1.0f, "%3i", rInventory->GetCraftingCount(CRAFT_ADAMANTITE_POWDER));
    Images.BaseMenu.rMainlineFont->DrawTextArgs(cAdamantiteT, cAdamantiteY + (cAdamantiteSpacingY * 1.00f), cFlagsL, 1.0f, "Flakes");
    Images.BaseMenu.rMainlineFont->DrawTextArgs(cAdamantiteR, cAdamantiteY + (cAdamantiteSpacingY * 1.00f), cFlagsR, 1.0f, "%3i", rInventory->GetCraftingCount(CRAFT_ADAMANTITE_FLAKES));
    Images.BaseMenu.rMainlineFont->DrawTextArgs(cAdamantiteT, cAdamantiteY + (cAdamantiteSpacingY * 2.00f), cFlagsL, 1.0f, "Shards");
    Images.BaseMenu.rMainlineFont->DrawTextArgs(cAdamantiteR, cAdamantiteY + (cAdamantiteSpacingY * 2.00f), cFlagsR, 1.0f, "%3i", rInventory->GetCraftingCount(CRAFT_ADAMANTITE_SHARD));
    Images.BaseMenu.rMainlineFont->DrawTextArgs(cAdamantiteT, cAdamantiteY + (cAdamantiteSpacingY * 3.00f), cFlagsL, 1.0f, "Pieces");
    Images.BaseMenu.rMainlineFont->DrawTextArgs(cAdamantiteR, cAdamantiteY + (cAdamantiteSpacingY * 3.00f), cFlagsR, 1.0f, "%3i", rInventory->GetCraftingCount(CRAFT_ADAMANTITE_PIECE));
    Images.BaseMenu.rMainlineFont->DrawTextArgs(cAdamantiteT, cAdamantiteY + (cAdamantiteSpacingY * 4.00f), cFlagsL, 1.0f, "Chunks");
    Images.BaseMenu.rMainlineFont->DrawTextArgs(cAdamantiteR, cAdamantiteY + (cAdamantiteSpacingY * 4.00f), cFlagsR, 1.0f, "%3i", rInventory->GetCraftingCount(CRAFT_ADAMANTITE_CHUNK));
    Images.BaseMenu.rMainlineFont->DrawTextArgs(cAdamantiteT, cAdamantiteY + (cAdamantiteSpacingY * 5.00f), cFlagsL, 1.0f, "Ore");
    Images.BaseMenu.rMainlineFont->DrawTextArgs(cAdamantiteR, cAdamantiteY + (cAdamantiteSpacingY * 5.00f), cFlagsR, 1.0f, "%3i", rInventory->GetCraftingCount(CRAFT_ADAMANTITE_ORE));

    //--[Catalyst Display]
    //--Show the catalyst counts.
    float cCatalystX = 1146.0f;
    float cCatalystR = 1216.0f;
    float cCatalystY = 420.0f;
    float cCatalystSpacingY = cMainlineTextHeight + 2.0f;
    Images.BaseMenu.rHeadingFont->DrawText(1251.0f, 377.0f, SUGARFONT_AUTOCENTER_XY, 1.0f, "Catalysts");
    Images.BaseMenu.rMainlineFont->DrawTextArgs(cCatalystX, cCatalystY + (cCatalystSpacingY * 0.00f), cFlagsL, 1.0f, "HP:");
    Images.BaseMenu.rMainlineFont->DrawTextArgs(cCatalystR, cCatalystY + (cCatalystSpacingY * 0.00f), cFlagsR, 1.0f, "%i", rInventory->GetCatalystCount(CATALYST_HEALTH));
    Images.BaseMenu.rMainlineFont->DrawTextArgs(cCatalystX, cCatalystY + (cCatalystSpacingY * 1.00f), cFlagsL, 1.0f, "Atk:");
    Images.BaseMenu.rMainlineFont->DrawTextArgs(cCatalystR, cCatalystY + (cCatalystSpacingY * 1.00f), cFlagsR, 1.0f, "%i", rInventory->GetCatalystCount(CATALYST_ATTACK));
    Images.BaseMenu.rMainlineFont->DrawTextArgs(cCatalystX, cCatalystY + (cCatalystSpacingY * 2.00f), cFlagsL, 1.0f, "Ini:");
    Images.BaseMenu.rMainlineFont->DrawTextArgs(cCatalystR, cCatalystY + (cCatalystSpacingY * 2.00f), cFlagsR, 1.0f, "%i", rInventory->GetCatalystCount(CATALYST_INITIATIVE));
    Images.BaseMenu.rMainlineFont->DrawTextArgs(cCatalystX, cCatalystY + (cCatalystSpacingY * 3.00f), cFlagsL, 1.0f, "Acc:");
    Images.BaseMenu.rMainlineFont->DrawTextArgs(cCatalystR, cCatalystY + (cCatalystSpacingY * 3.00f), cFlagsR, 1.0f, "%i", rInventory->GetCatalystCount(CATALYST_ACCURACY));
    Images.BaseMenu.rMainlineFont->DrawTextArgs(cCatalystX, cCatalystY + (cCatalystSpacingY * 4.00f), cFlagsL, 1.0f, "Evd:");
    Images.BaseMenu.rMainlineFont->DrawTextArgs(cCatalystR, cCatalystY + (cCatalystSpacingY * 4.00f), cFlagsR, 1.0f, "%i", rInventory->GetCatalystCount(CATALYST_DODGE));
    Images.BaseMenu.rMainlineFont->DrawTextArgs(cCatalystX, cCatalystY + (cCatalystSpacingY * 5.00f), cFlagsL, 1.0f, "Skl:");
    Images.BaseMenu.rMainlineFont->DrawTextArgs(cCatalystR, cCatalystY + (cCatalystSpacingY * 5.00f), cFlagsR, 1.0f, "%i", rInventory->GetCatalystCount(CATALYST_SKILL));

    //--Catalyst Collection Total:
    int tCatalystTotal = rInventory->GetCatalystCount(CATALYST_HEALTH) + rInventory->GetCatalystCount(CATALYST_ATTACK) + rInventory->GetCatalystCount(CATALYST_INITIATIVE) + rInventory->GetCatalystCount(CATALYST_ACCURACY) + rInventory->GetCatalystCount(CATALYST_DODGE) + rInventory->GetCatalystCount(CATALYST_SKILL);
    Images.BaseMenu.rMainlineFont->DrawTextArgs(cCatalystX, cCatalystY + (cCatalystSpacingY * 7.00f), cFlagsL, 1.0f, "Collected: %i / %i", tCatalystTotal, xCatalystTotal);

    //--Catalyst bonuses.
    float cBonusOff = 64.0f;
    if(rInventory->ComputeCatalystBonus(CATALYST_HEALTH) > 0)
    {
        Images.BaseMenu.rMainlineFont->DrawTextArgs(cCatalystR + cBonusOff, cCatalystY + (cCatalystSpacingY * 0.00f), 0, 1.0f, "+%i HP", rInventory->ComputeCatalystBonus(CATALYST_HEALTH));
    }
    if(rInventory->ComputeCatalystBonus(CATALYST_ATTACK) > 0)
    {
        Images.BaseMenu.rMainlineFont->DrawTextArgs(cCatalystR + cBonusOff, cCatalystY + (cCatalystSpacingY * 1.00f), 0, 1.0f, "+%i Atk", rInventory->ComputeCatalystBonus(CATALYST_ATTACK));
    }
    if(rInventory->ComputeCatalystBonus(CATALYST_INITIATIVE) > 0)
    {
        Images.BaseMenu.rMainlineFont->DrawTextArgs(cCatalystR + cBonusOff, cCatalystY + (cCatalystSpacingY * 2.00f), 0, 1.0f, "+%i Ini", rInventory->ComputeCatalystBonus(CATALYST_INITIATIVE));
    }
    if(rInventory->ComputeCatalystBonus(CATALYST_ACCURACY) > 0)
    {
        Images.BaseMenu.rMainlineFont->DrawTextArgs(cCatalystR + cBonusOff, cCatalystY + (cCatalystSpacingY * 3.00f), 0, 1.0f, "+%i Acc", rInventory->ComputeCatalystBonus(CATALYST_ACCURACY));
    }
    if(rInventory->ComputeCatalystBonus(CATALYST_DODGE) > 0)
    {
        Images.BaseMenu.rMainlineFont->DrawTextArgs(cCatalystR + cBonusOff, cCatalystY + (cCatalystSpacingY * 4.00f), 0, 1.0f, "+%i Evd", rInventory->ComputeCatalystBonus(CATALYST_DODGE));
    }
    if(rInventory->ComputeCatalystBonus(CATALYST_SKILL) > 0)
    {
        Images.BaseMenu.rMainlineFont->DrawTextArgs(cCatalystR + cBonusOff, cCatalystY + (cCatalystSpacingY * 5.00f), 0, 1.0f, "+%i Skl", rInventory->ComputeCatalystBonus(CATALYST_SKILL));
    }

    //--[Buttons]
    //--These slide out to indicate what the player has selected. A subroutine does the rendering based on the menu state.
    bool tMapIsGreyed = (rActiveMap == NULL) && (mMapLayerList->GetListSize() < 1);
    RenderMainMenuButton(Images.BaseMenu.rBtnBase, Images.BaseMenu.rHeadingFont, 0, (mCurrentCursor == 0), false,        "Inventory");
    RenderMainMenuButton(Images.BaseMenu.rBtnBase, Images.BaseMenu.rHeadingFont, 1, (mCurrentCursor == 1), false,        "Equipment");
    RenderMainMenuButton(Images.BaseMenu.rBtnBase, Images.BaseMenu.rHeadingFont, 2, (mCurrentCursor == 2), false,        "Doctor Bag");
    RenderMainMenuButton(Images.BaseMenu.rBtnBase, Images.BaseMenu.rHeadingFont, 3, (mCurrentCursor == 3), false,        "Status");
    RenderMainMenuButton(Images.BaseMenu.rBtnBase, Images.BaseMenu.rHeadingFont, 4, (mCurrentCursor == 4), tMapIsGreyed, "Map");
    RenderMainMenuButton(Images.BaseMenu.rBtnBase, Images.BaseMenu.rHeadingFont, 5, (mCurrentCursor == 5), false,        "Options");
    RenderMainMenuButton(Images.BaseMenu.rBtnBase, Images.BaseMenu.rHeadingFont, 6, (mCurrentCursor == 6), false,        "End Game");

    //--[Party Portraits]
    //--Fast-access pointers.
    AdvCombat *rAdventureCombat = AdvCombat::Fetch();

    //--Options.
    bool tIsSmallPortraitMode = OptionsManager::Fetch()->GetOptionB("LowResAdventureMode");

    //--Variables.
    float cXOffset = 192.0f * -3.0f;
    int cActivePartySize = rAdventureCombat->GetActivePartyCount();

    //--Iterate.
    for(int i = 0; i < cActivePartySize; i ++)
    {
        //--Get the character in question.
        AdvCombatEntity *rActiveEntity = rAdventureCombat->GetActiveMemberI(i);
        if(!rActiveEntity) continue;

        //--Constants
        float cYOffset = -45.0f;

        //--Backing.
        Images.BaseMenu.rPortraitBack->Draw(cXOffset, 0.0f);
        Images.BaseMenu.rComboBar->Draw(cXOffset, 0.0f);

        //--Activate stencils and render a mask.
        glEnable(GL_STENCIL_TEST);
        glColorMask(false, false, false, false);
        glDepthMask(false);
        glStencilMask(0xFF);
        glStencilFunc(GL_ALWAYS, i+1, 0xFF);
        glStencilOp(GL_REPLACE, GL_REPLACE, GL_REPLACE);
        Images.BaseMenu.rPortraitMask->Draw(cXOffset, 0.0f);

        //--Countermask, if it exists, stops rendering on these pixels. Not all entities have one.
        SugarBitmap *rCounterMask = rActiveEntity->GetCounterMask();
        TwoDimensionRealPoint cMainRenderCoords = rActiveEntity->GetUIRenderPosition(ACE_UI_INDEX_BASEMENU_MAIN);
        if(rCounterMask)
        {
            glStencilFunc(GL_ALWAYS, 0, 0xFF);
            rCounterMask->Draw(cMainRenderCoords.mXCenter + cXOffset, cMainRenderCoords.mYCenter + cYOffset);
        }

        //--GL Setup for rendering.
        glColorMask(true, true, true, true);
        glDepthMask(true);
        glStencilMask(0xFF);
        glStencilFunc(GL_EQUAL, i+1, 0xFF);
        glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);

        //--Current character, always exists.
        SugarBitmap *rFieldPortrait = rActiveEntity->GetCombatPortrait();
        if(rFieldPortrait)
        {
            //--Normal rendering:
            if(!tIsSmallPortraitMode)
            {
                rFieldPortrait->Draw(cMainRenderCoords.mXCenter + cXOffset, cMainRenderCoords.mYCenter + cYOffset);
            }
            //--Scaled rendering.
            else
            {
                rFieldPortrait->DrawScaled(cMainRenderCoords.mXCenter + cXOffset, cMainRenderCoords.mYCenter + cYOffset, LOWRES_SCALEINV, LOWRES_SCALEINV);
            }
        }

        //--Clean up.
        glDisable(GL_STENCIL_TEST);

        //--Front of the portrait.
        Images.BaseMenu.rNameBar->Draw(cXOffset, 0.0f);
        Images.BaseMenu.rHeadingFont->DrawText(670.0f + cXOffset, 692.0f, SUGARFONT_AUTOCENTER_XY, 1.0f, rActiveEntity->GetDisplayName());

        //--Health Bar
        Images.BaseMenu.rHealthBarBack->Draw(cXOffset, 0.0f);
        float tHPPercent = rActiveEntity->GetHealthPercent();
        if(tHPPercent > 0.0f)
        {
            //--Rendering setup.
            float cLft = Images.BaseMenu.rHealthBarFill->GetXOffset() + cXOffset;
            float cTop = Images.BaseMenu.rHealthBarFill->GetYOffset();
            float cRgt = Images.BaseMenu.rHealthBarFill->GetXOffset() + (Images.BaseMenu.rHealthBarFill->GetWidth() * tHPPercent) + cXOffset;
            float cBot = Images.BaseMenu.rHealthBarFill->GetYOffset() +  Images.BaseMenu.rHealthBarFill->GetHeight();
            float cTxL = 0.0f;
            float cTxT = 0.0f;
            float cTxR = tHPPercent;
            float cTxB = 1.0f;

            //--Flip since OpenGL is upside-down.
            cTxT = 1.0f - cTxT;
            cTxB = 1.0f - cTxB;

            //--Render.
            Images.BaseMenu.rHealthBarFill->Bind();
            glBegin(GL_QUADS);
                glTexCoord2f(cTxL, cTxT); glVertex2f(cLft, cTop);
                glTexCoord2f(cTxR, cTxT); glVertex2f(cRgt, cTop);
                glTexCoord2f(cTxR, cTxB); glVertex2f(cRgt, cBot);
                glTexCoord2f(cTxL, cTxB); glVertex2f(cLft, cBot);
            glEnd();
        }
        Images.BaseMenu.rHealthBarFrame->Draw(cXOffset, 0.0f);

        //--Render the HP values.
        int tHP = rActiveEntity->GetHealth();
        int tHPMax = rActiveEntity->GetStatistic(ADVCE_STATS_FINAL, STATS_HPMAX);
        Images.BaseMenu.rMainlineFont->DrawTextArgs(672.0f + cXOffset, 749.0f, SUGARFONT_AUTOCENTER_XY, 1.0f, "%i / %i", tHP, tHPMax);

        //--Character's level.
        Images.BaseMenu.rMainlineFont->DrawTextArgs(603.0f + cXOffset, 721.0f, SUGARFONT_AUTOCENTER_XY, 1.0f, "%i", rActiveEntity->GetLevel() + 1);

        //--Move the X offset over.
        cXOffset = cXOffset + 192.0f;
    }

    //--If quit mode is active, render that over the menu.
    if(mCurrentMode == AM_MODE_QUIT)
    {
        RenderQuitMenu();
    }

    //--Render the version number in the top left.
    GLOBAL *rGlobal = Global::Shared();
    if(rGlobal->gBitmapFont)
    {
        rGlobal->gBitmapFont->DrawText(0.0f, 0.0f, 0, 1.0f, rGlobal->gVersionString);
    }
}

void PrintLetters(const char *pString)
{
    if(pString[0] == '\0') return;
    fprintf(stderr, "%c", pString[0]);
    PrintLetters(&pString[1]);
}
