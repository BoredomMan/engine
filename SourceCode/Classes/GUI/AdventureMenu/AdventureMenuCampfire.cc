//--Base
#include "AdventureMenu.h"

//--Classes
#include "AdvCombat.h"
#include "AdventureInventory.h"
#include "AdventureLevel.h"
#include "VisualLevel.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarFont.h"
#include "SugarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "EasingFunctions.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "SaveManager.h"

//--[Forward Declarations]
void RenderLineBounded(float pLft, float pTop, float pWidth, SugarFont *pFont, const char *pString);
void RenderTextConditionalColor(SugarFont *pFont, float pX, float pY, int pFlags, float pSize, const char *pText, bool pConditional, StarlightColor pTrueColor, StarlightColor pFalseColor);

//--[System]
void AdventureMenu::InitializeCampfireVariables()
{
    //--Called whenever campfire mode is activated, sets values to their initial non-constructor versions.
    //  This is mostly just opacity timers.
    mCampfireGridOpacityTimer = 0;
    mCampfireGridCurrent = AM_SAVE_REST;
}
void AdventureMenu::BuildCampfirePositions()
{
    //--Grid Auto-Builder. 7x7 grid that automatically positions the buttons relative to one another.
    int cGridVals[7][7];
    for(int x = 0; x < 7; x ++)
    {
        for(int y = 0; y < 7; y ++)
        {
            cGridVals[x][y] = -1;
        }
    }

    //--Set the positions. 3x3 is the middle position.
    cGridVals[3][3] = AM_SAVE_REST;
    cGridVals[3][2] = AM_SAVE_CHAT;
    cGridVals[2][3] = AM_SAVE_FORM;
    cGridVals[4][3] = AM_SAVE_WARP;
    cGridVals[3][4] = AM_SAVE_SAVE;

    cGridVals[2][2] = AM_SAVE_SKILLBOOKS;
    cGridVals[4][2] = AM_SAVE_RELIVE;
    cGridVals[2][4] = AM_SAVE_COSTUMES;
    cGridVals[4][4] = AM_SAVE_PASSWORD;

    //--Now loop through the grid and set everything accordingly. Note that the very edges
    //  are skipped, because they are "buffer" entries.
    for(int x = 1; x < 6; x ++)
    {
        for(int y = 1; y < 6; y ++)
        {
            //--Skip -1 instances.
            if(cGridVals[x][y] == -1) continue;

            //--Get the entry.
            int i = cGridVals[x][y];

            //--Set which controls go where.
            mCampfireGrid[i].mControlLookups[GRID_LFT] = cGridVals[x-1][y+0];
            mCampfireGrid[i].mControlLookups[GRID_TOP] = cGridVals[x+0][y-1];
            mCampfireGrid[i].mControlLookups[GRID_RGT] = cGridVals[x+1][y+0];
            mCampfireGrid[i].mControlLookups[GRID_BOT] = cGridVals[x+0][y+1];

            //--Set the grid position.
            mCampfireGrid[i].mXPos = AM_GRID_CENT_X + ((x-3) * AM_GRID_SPAC_X);
            mCampfireGrid[i].mYPos = AM_GRID_CENT_Y + ((y-3) * AM_GRID_SPAC_Y);
        }
    }
}

//--[Core Methods]
#include "EntityManager.h"
#include "TilemapActor.h"
void AdventureMenu::ExecuteRest()
{
    //--Causes the player's party to be restored to full, all enemies to respawn, and the destroyed enemy
    //  list to be cleared.
    AdventureLevel *rActiveLevel = AdventureLevel::Fetch();

    //--Clear all entities marked as enemies.
    SugarLinkedList *rEntityList = EntityManager::Fetch()->GetEntityList();
    RootEntity *rCheckEntity = (RootEntity *)rEntityList->SetToHeadAndReturn();
    while(rCheckEntity)
    {
        //--Entity must be of the right type.
        if(rCheckEntity->IsOfType(POINTER_TYPE_TILEMAPACTOR))
        {
            TilemapActor *rActor = (TilemapActor *)rCheckEntity;
            if(rActor->IsEnemy("Any"))
            {
                rEntityList->RemoveRandomPointerEntry();
            }
        }
        rCheckEntity = (RootEntity *)rEntityList->IncrementAndGetRandomPointerEntry();
    }

    //--Clear, respawn.
    rActiveLevel->WipeDestroyedEnemies();
    rActiveLevel->SpawnEnemies();
    rActiveLevel->PulseIgnore(AdventureLevel::xLastIgnorePulse);

    //--Restore party.
    AdvCombat::Fetch()->FullRestoreParty();

    //--Inventory resets all charges.
    AdventureInventory *rInventory = AdventureInventory::Fetch();
    rInventory->ResetCharges();
    rInventory->SetDoctorBagCharges(rInventory->GetDoctorBagChargesMax());

    //--Go through the DataLibrary's special section and reset the variables within to 0.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    SugarLinkedList *rHeading = rDataLibrary->GetHeading("Root/Variables/Chapter1/RestReset/");
    if(rHeading)
    {
        //--Iterate.
        SysVar *rVariable = (SysVar *)rHeading->PushIterator();
        while(rVariable)
        {
            rVariable->mNumeric = 0.0f;
            rVariable = (SysVar *)rHeading->AutoIterate();
        }
    }
}
void AdventureMenu::UpdateCampfireMenu()
{
    //--[Sub-Handlers]
    //--These modes always update, and have internal checks to block updates. They need to update
    //  in order to run timers when they are closed.
    UpdateFormsMenu();
    UpdateFormsSubMenu();
    UpdateReliveMode();
    UpdateWarpMode();
    UpdateFileSelection();
    UpdateChatMenu();
    UpdateCostumeMenu();
    UpdatePasswordMode();

    //--Other submenus that lockout updates directly.
    bool tIsNotMainObject = false;
    if(mIsFormsMode)      { tIsNotMainObject = true; }
    if(mIsSelectFileMode) { tIsNotMainObject = true; }
    if(mIsChatMode)       { tIsNotMainObject = true; }
    if(mIsReliveMode)     { tIsNotMainObject = true; }
    if(mIsWarpMode)       { tIsNotMainObject = true; }
    if(mIsCostumesMenu)   { tIsNotMainObject = true; }
    if(mIsPasswordEntry)  { tIsNotMainObject = true; }
    if(mIsSkillbookMode)  { UpdateSkillbookMode(); tIsNotMainObject = true; }

    //--[Major Handler]
    //--If this mode is not the main handler, it sets timers and stops here.
    if(tIsNotMainObject || !mIsVisible)
    {
        if(mCampfireGridOpacityTimer > 0) mCampfireGridOpacityTimer --;
        return;
    }
    //--Object is the main handler, increment the timer.
    else
    {
        if(mCampfireGridOpacityTimer < AM_CAMPFIRE_OPACITY_TICKS) mCampfireGridOpacityTimer ++;
    }

    //--Setup.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Error check:
    if(mCampfireGridCurrent < 0 || mCampfireGridCurrent >= AM_SAVE_TOTAL) mCampfireGridCurrent = 0;

    //--Directional controls are handled programmatically. If the instruction is -1, it means there is nothing
    //  at that grid position.
    int tCampfireGridPrevious = mCampfireGridCurrent;
    if(rControlManager->IsFirstPress("Up"))
    {
        int tTargetEntry = mCampfireGrid[mCampfireGridCurrent].mControlLookups[GRID_TOP];
        if(tTargetEntry != -1) mCampfireGridCurrent = tTargetEntry;
    }
    if(rControlManager->IsFirstPress("Down"))
    {
        int tTargetEntry = mCampfireGrid[mCampfireGridCurrent].mControlLookups[GRID_BOT];
        if(tTargetEntry != -1) mCampfireGridCurrent = tTargetEntry;
    }
    if(rControlManager->IsFirstPress("Left"))
    {
        int tTargetEntry = mCampfireGrid[mCampfireGridCurrent].mControlLookups[GRID_LFT];
        if(tTargetEntry != -1) mCampfireGridCurrent = tTargetEntry;
    }
    if(rControlManager->IsFirstPress("Right"))
    {
        int tTargetEntry = mCampfireGrid[mCampfireGridCurrent].mControlLookups[GRID_RGT];
        if(tTargetEntry != -1) mCampfireGridCurrent = tTargetEntry;
    }

    //--If the campfire grid position changed, play sounds and handle the size changer.
    if(tCampfireGridPrevious != mCampfireGridCurrent)
    {
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }

    //--Size setting. All not-selected grid entries decrease their timers. The selected entry increases.
    for(int i = 0; i < AM_SAVE_TOTAL; i ++)
    {
        if(i != mCampfireGridCurrent)
        {
            mCampfireGrid[i].mTimer --;
            if(mCampfireGrid[i].mTimer < 1) mCampfireGrid[i].mTimer = 0;
        }
        else
        {
            mCampfireGrid[i].mTimer ++;
            if(mCampfireGrid[i].mTimer >= AM_CAMPFIRE_SIZE_TICKS) mCampfireGrid[i].mTimer = AM_CAMPFIRE_SIZE_TICKS;
        }
    }

    //--Activate, enter the given submenu.
    if(rControlManager->IsFirstPress("Activate"))
    {
        //--Rest. Restores HP and resets all enemies.
        if(mCampfireGridCurrent == AM_SAVE_REST)
        {
            AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
            if(rActiveLevel) rActiveLevel->BeginRestSequence();
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        //--Chat. Allows the player to speak to their party members.
        else if(mCampfireGridCurrent == AM_SAVE_CHAT)
        {
            //--Activate.
            if(mChatListing && mChatListing->GetListSize() > 0)
            {
                ActivateChatMenu();
                AudioManager::Fetch()->PlaySound("Menu|Select");
            }
            //--Error, cannot activate.
            else
            {
                AudioManager::Fetch()->PlaySound("Menu|Failed");
            }
        }
        //--Save, opens a list of files and asks you to save over one.
        else if(mCampfireGridCurrent == AM_SAVE_SAVE)
        {
            //--Activate the submenu.
            ScanExistingFiles();
            mIsSelectFileMode = true;

            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        //--Costumes. Switches character costumes.
        else if(mCampfireGridCurrent == AM_SAVE_COSTUMES)
        {
            //--Activate the submenu.
            ActivateCostumesMenu();

            //--SFX handled by submenu since it can fail.
        }
        //--Skillbooks. Re-read skillbooks that have previously been read.
        else if(mCampfireGridCurrent == AM_SAVE_SKILLBOOKS)
        {
            //--Activate the submenu.
            SetToSkillbookMode();

            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        //--Allows the player to change form on characters that allow it.
        else if(mCampfireGridCurrent == AM_SAVE_FORM)
        {
            ActivateFormsMenu();
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        //--Password Entry System. Allows unlocking of special codes, including debug menu, cheats, and costumes.
        else if(mCampfireGridCurrent == AM_SAVE_PASSWORD)
        {
            SetToPasswordMode();
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
        //--Relive cutscenes that already happened.
        else if(mCampfireGridCurrent == AM_SAVE_RELIVE)
        {
            //--Nothing to relive? Fail.
            if(mReliveList->GetListSize() < 1)
            {
                AudioManager::Fetch()->PlaySound("Menu|Failed");
            }
            //--Activate the mode.
            else
            {
                SetToReliveMode();
                AudioManager::Fetch()->PlaySound("Menu|Select");
            }
        }
        //--Warp to campfires you've already visited.
        else if(mCampfireGridCurrent == AM_SAVE_WARP)
        {
            //--Nothing to warp to? Fail.
            if(mWarpList->GetListSize() < 1 || mIsBenchMode)
            {
                AudioManager::Fetch()->PlaySound("Menu|Failed");
            }
            //--Activate the mode.
            else
            {
                SetToWarpMode();
                AudioManager::Fetch()->PlaySound("Menu|Select");
            }
        }
    }
    //--Cancel, hides this object.
    else if(rControlManager->IsFirstPress("Cancel") && !mStartedHidingThisTick)
    {
        Hide();
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }
}

//--[Rendering]
void AdventureMenu::RenderCampfireMenu()
{
    //--[Setup]
    //--Don't render if the images didn't resolve.
    if(!Images.mIsReady) return;

    //--Opacity percent. Determines position and fading.
    float tOpacityPct = (float)mCampfireGridOpacityTimer / (float)AM_CAMPFIRE_OPACITY_TICKS;

    //--[Main Rendering]
    //--Object does not render if its opacity is zero. Sub-objects may still render.
    if(mCampfireGridOpacityTimer > 0)
    {
        //--[Title]
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, tOpacityPct);
        Images.CampfireMenu.rHeader->Draw(427.0f, 45.0f);
        Images.CampfireMenu.rHeadingFont->DrawText(683.0f, 57.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Campfire Menu");
        StarlightColor::ClearMixer();

        //--[Icons]
        //--Sizes.
        float cIconSize = Images.CampfireMenu.rIconFrame->GetTrueWidth();

        //--Position the objects slide away from.
        float tOpacitySlidePct = EasingFunction::QuadraticOut(mCampfireGridOpacityTimer, AM_CAMPFIRE_OPACITY_TICKS);
        float tCenterX = AM_GRID_CENT_X;
        float tCenterY = AM_GRID_CENT_Y;

        //--Render icons at triple-size. Base size is 32.0f.
        for(int i = 0; i < AM_SAVE_TOTAL; i ++)
        {
            //--Determine the rendering size. This also affects color blending.
            float cBoostPct = EasingFunction::QuadraticInOut(mCampfireGrid[i].mTimer, AM_CAMPFIRE_SIZE_TICKS);
            float cScale = 3.0f + (cBoostPct * AM_CAMPFIRE_SIZE_BOOST);
            float cScaleInv = 1.0f / cScale;

            //--Figure out the render position.
            float tXPos = mCampfireGrid[i].mXPos;
            float tYPos = mCampfireGrid[i].mYPos;

            //--The actual render position is based on the opacity percent as the object slides out of the middle.
            tXPos = tCenterX + ((tXPos - tCenterX) * tOpacitySlidePct);
            tYPos = tCenterY + ((tYPos - tCenterY) * tOpacitySlidePct);

            //--Move the render position off by the scale of the object.
            tXPos = tXPos - (cIconSize * cScale * 0.50f);
            tYPos = tYPos - (cIconSize * cScale * 0.50f);

            //--Color blending.
            float cColVal = 0.75f + (0.25f * cBoostPct);
            StarlightColor::SetMixer(cColVal, cColVal, cColVal, tOpacityPct);

            //--Render.
            glTranslatef(tXPos, tYPos, 0.0f);
            glScalef(cScale, cScale, 1.0f);
            Images.CampfireMenu.rIconFrame->Draw();
            Images.CampfireMenu.rIcons[i]->Draw();
            glScalef(cScaleInv, cScaleInv, 1.0f);
            glTranslatef(-tXPos, -tYPos, 0.0f);
        }
        StarlightColor::ClearMixer();

        //--[Info Box]
        //--Backing.
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, tOpacityPct);
        Images.CampfireMenu.rDescriptionBox->Draw();

        //--Sizes.
        float cBotHeaderX = 683.0f;
        float cBotHeaderY = 532.0f;
        float cDescriptionLft = 316.0f;
        float cDescriptionTop = 575.0f;
        float cDescriptionWid = 746.0f;

        //--Rest Description.
        if(mCampfireGridCurrent == AM_SAVE_REST)
        {
            Images.CampfireMenu.rHeadingFont->DrawText(cBotHeaderX, cBotHeaderY, SUGARFONT_AUTOCENTER_X, 1.0f, "Rest");
            RenderLineBounded(cDescriptionLft, cDescriptionTop, cDescriptionWid, Images.CampfireMenu.rMainlineFont, "Rest for a while. Restores all HP and refills your Doctor Bag, but respawns all enemies.");
        }
        //--Chat Description.
        else if(mCampfireGridCurrent == AM_SAVE_CHAT)
        {
            Images.CampfireMenu.rHeadingFont->DrawText(cBotHeaderX, cBotHeaderY, SUGARFONT_AUTOCENTER_X, 1.0f, "Chat");
            RenderLineBounded(cDescriptionLft, cDescriptionTop, cDescriptionWid, Images.CampfireMenu.rMainlineFont, "Chat with your party members. Swap stories, get hints, and review objectives.");
        }
        //--Save Description.
        else if(mCampfireGridCurrent == AM_SAVE_SAVE)
        {
            Images.CampfireMenu.rHeadingFont->DrawText(cBotHeaderX, cBotHeaderY, SUGARFONT_AUTOCENTER_X, 1.0f, "Save Game");
            RenderLineBounded(cDescriptionLft, cDescriptionTop, cDescriptionWid, Images.CampfireMenu.rMainlineFont, "Save your game.");
        }
        //--Save Description.
        else if(mCampfireGridCurrent == AM_SAVE_COSTUMES)
        {
            Images.CampfireMenu.rHeadingFont->DrawText(cBotHeaderX, cBotHeaderY, SUGARFONT_AUTOCENTER_X, 1.0f, "Costumes");
            RenderLineBounded(cDescriptionLft, cDescriptionTop, cDescriptionWid, Images.CampfireMenu.rMainlineFont, "Change costumes worn by some characters. Entirely cosmetic.");
        }
        //--Skillbooks Description.
        else if(mCampfireGridCurrent == AM_SAVE_SKILLBOOKS)
        {
            Images.CampfireMenu.rHeadingFont->DrawText(cBotHeaderX, cBotHeaderY, SUGARFONT_AUTOCENTER_X, 1.0f, "Review Skillbooks");
            RenderLineBounded(cDescriptionLft, cDescriptionTop, cDescriptionWid, Images.CampfireMenu.rMainlineFont, "Check learned abilities and learn new ones if you've previously found their skillbook.");
        }
        //--Form Description.
        else if(mCampfireGridCurrent == AM_SAVE_FORM)
        {
            Images.CampfireMenu.rHeadingFont->DrawText(cBotHeaderX, cBotHeaderY, SUGARFONT_AUTOCENTER_X, 1.0f, "Transform");
            RenderLineBounded(cDescriptionLft, cDescriptionTop, cDescriptionWid, Images.CampfireMenu.rMainlineFont, "Transform your characters to gain new abilities or access new areas.");
        }
        //--Warp Description.
        else if(mCampfireGridCurrent == AM_SAVE_WARP)
        {
            Images.CampfireMenu.rHeadingFont->DrawText(cBotHeaderX, cBotHeaderY, SUGARFONT_AUTOCENTER_X, 1.0f, "Warp");
            RenderLineBounded(cDescriptionLft, cDescriptionTop, cDescriptionWid, Images.CampfireMenu.rMainlineFont, "Warp to any campfire you've previously visited.");
        }
        //--Relive Description.
        else if(mCampfireGridCurrent == AM_SAVE_RELIVE)
        {
            Images.CampfireMenu.rHeadingFont->DrawText(cBotHeaderX, cBotHeaderY, SUGARFONT_AUTOCENTER_X, 1.0f, "Relive");
            RenderLineBounded(cDescriptionLft, cDescriptionTop, cDescriptionWid, Images.CampfireMenu.rMainlineFont, "Relive select cutscenes.");
        }
        //--Password Description.
        else if(mCampfireGridCurrent == AM_SAVE_PASSWORD)
        {
            Images.CampfireMenu.rHeadingFont->DrawText(cBotHeaderX, cBotHeaderY, SUGARFONT_AUTOCENTER_X, 1.0f, "Enter Password");
            RenderLineBounded(cDescriptionLft, cDescriptionTop, cDescriptionWid, Images.CampfireMenu.rMainlineFont, "Enter a password to unlock cheats, costumes, or debug options.");
        }
        StarlightColor::ClearMixer();
    }

    //--[Submenus]
    //--If a submenu is active, render it.
    RenderFormsMenu();
    RenderFormsSubMenu();
    RenderFileSelection();
    RenderChatMenu();
    RenderReliveMode();
    RenderWarpMode();
    RenderCostumeMenu();
    RenderCostumeMenuSub();
    RenderPasswordMode();
    if(mIsSkillbookMode)  { RenderSkillbookMode(); }
}
float AdventureMenu::RenderCampfireMenuBacking(int pMaxOptions, bool pRenderDescriptionBox)
{
    //--[Documentation]
    //--Renders the standard backing of the Campfire Menu's assets with the listed number of option buttons available.
    //  After that, the caller can fill in the buttons, header, and descriptions.
    //--Returns the Y position where button title rendering should begin.
    if(!Images.mIsReady) return 0.0f;

    //--Clamp. Enough space for one option is always rendered no matter what.
    if(pMaxOptions < 1) pMaxOptions = 1;

    //--[Static Parts]
    //--Determine how large the backing box is. It is variable-height based on how many options are needed.
    float cYStart = 122.0f;
    float cHeightPerButton = 52.0f;
    float cHeightToFirstButton = 42.0f;

    //--Coordinates of the middle box.
    float cLft = 400.0f;
    float cTop = cYStart + Images.CampfireMenu.rBaseTop->GetHeight();
    float cRgt = cLft + 566.0f;
    float cBot = cTop + (cHeightPerButton * pMaxOptions);

    //--Render the backing top.
    glTranslatef(0.0f, -32.0f, 0.0f);
    Images.CampfireMenu.rBaseTop->Draw(400.0f, cYStart);
    Images.CampfireMenu.rHeader->Draw(427.0f, 92.0f);

    //--Render the backing middle, stretched between these two points.
    Images.CampfireMenu.rBaseMid->Bind();
    glBegin(GL_QUADS);
        glTexCoord2f(0.0f, 1.0f); glVertex2f(cLft, cTop);
        glTexCoord2f(1.0f, 1.0f); glVertex2f(cRgt, cTop);
        glTexCoord2f(1.0f, 0.0f); glVertex2f(cRgt, cBot);
        glTexCoord2f(0.0f, 0.0f); glVertex2f(cLft, cBot);
    glEnd();

    //--Render the backing bottom.
    Images.CampfireMenu.rBaseBot->Draw(400.0f, cBot);

    //--Render the buttons.
    for(int i = 0; i < pMaxOptions; i ++)
    {
        Images.CampfireMenu.rButton->Draw(457.0f, cYStart + cHeightToFirstButton + (i * cHeightPerButton));
    }

    //--Render the description box.
    glTranslatef(0.0f, 32.0f, 0.0f);
    if(pRenderDescriptionBox) Images.CampfireMenu.rDescriptionBox->Draw();

    //--Return the Y value where button rendering should begin.
    return cYStart + cHeightToFirstButton + 5.0f;
}

//--[Worker Functions]
void RenderTextConditionalColor(SugarFont *pFont, float pX, float pY, int pFlags, float pSize, const char *pText, bool pConditional, StarlightColor pTrueColor, StarlightColor pFalseColor)
{
    if(!pFont || !pText) return;
    StarlightColor::ConditionalMixer(pConditional, pTrueColor, pFalseColor);
    pFont->DrawText(pX, pY, pFlags, pSize, pText);
}
