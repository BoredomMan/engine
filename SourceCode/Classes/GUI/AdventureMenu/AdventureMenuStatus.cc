//--Base
#include "AdventureMenu.h"

//--Classes
#include "AdvCombat.h"
#include "AdvCombatAbility.h"
#include "AdvCombatEntity.h"
#include "AdventureItem.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarFont.h"
#include "SugarLinkedList.h"

//--Definitions
#include "EasingFunctions.h"
#include "Subdivide.h"

//--Libraries
//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "DisplayManager.h"
#include "OptionsManager.h"

//--Constants
#define AM_ABILITIES_PER_PAGE 11

//--Forward Declarations
void RenderTextToFit(SugarFont *pFont, float pX, float pY, float pMaxWid, const char *pText, ...);
void RenderAndAdvance(float pLft, float &sYCursor, SugarFont *pFont, const char *pFormat, ...);

//--[Worker Functions]
int ComputeAbilityCursorMax(SugarLinkedList *pAbilityList)
{
    //--Iterate across the given ability list and return the number of abilities on it.
    if(!pAbilityList) return 0;
    return pAbilityList->GetListSize();
}

//--[Main Functions]
void AdventureMenu::UpdateStatusMenu()
{
    //--Handles controls for the status menu. Basically just press cancel to go back, left and right to change characters.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Timers.
    if(mIsAbilityMode)
    {
        if(mAbilityTimer < AM_ABILITY_MODE_TICKS) mAbilityTimer ++;
    }
    else
    {
        if(mAbilityTimer > 0) mAbilityTimer --;
    }

    //--Left!
    if(rControlManager->IsFirstPress("Left"))
    {
        //--Decrement.
        mStatusCursor --;

        //--Wrap.
        int tCharactersMax = AdvCombat::Fetch()->GetActivePartyCount();
        if(mStatusCursor < 0) mStatusCursor = tCharactersMax - 1;

        //--Recompute the max ability cursor.
        AdvCombatEntity *rActiveEntity = AdvCombat::Fetch()->GetActiveMemberI(mStatusCursor);
        if(!rActiveEntity) return;
        mAbilityCursorMax = ComputeAbilityCursorMax(rActiveEntity->GetAbilityList());

        //--Move cursor if needed.
        if(mAbilityCursor >= mAbilityCursorMax) mAbilityCursor = mAbilityCursorMax;

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }
    //--RIGHT!
    if(rControlManager->IsFirstPress("Right"))
    {
        //--Increment.
        mStatusCursor ++;

        //--Wrap.
        int tCharactersMax = AdvCombat::Fetch()->GetActivePartyCount();
        if(mStatusCursor >= tCharactersMax) mStatusCursor = mStatusCursor % tCharactersMax;

        //--Recompute the max ability cursor.
        AdvCombatEntity *rActiveEntity = AdvCombat::Fetch()->GetActiveMemberI(mStatusCursor);
        if(!rActiveEntity) return;
        mAbilityCursorMax = ComputeAbilityCursorMax(rActiveEntity->GetAbilityList());

        //--Move cursor if needed.
        if(mAbilityCursor >= mAbilityCursorMax) mAbilityCursor = mAbilityCursorMax;

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }

    //--Pressing down activates ability mode if it's not active.
    if(rControlManager->IsFirstPress("Down"))
    {
        //--Activate ability mode.
        if(!mIsAbilityMode)
        {
            mIsAbilityMode = true;
            mAbilityCursor = 0;
            mAbilityRenderMin = 0;
        }
        //--Move ability cursor down.
        else
        {
            //--Move cursor.
            mAbilityCursor ++;

            //--Clamp.
            if(mAbilityCursor >= mAbilityCursorMax) mAbilityCursor --;

            //--Check the minimum case.
            if(mAbilityCursor > mAbilityRenderMin + 7)
            {
                //--Increment.
                mAbilityRenderMin ++;

                //--Clamp.
                if(mAbilityRenderMin + AM_ABILITIES_PER_PAGE > mAbilityCursorMax) mAbilityRenderMin --;
            }
        }

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }

    //--Pressing up deactivates ability mode if it's active.
    if(rControlManager->IsFirstPress("Up"))
    {
        //--Not in ability mode, do nothing.
        if(!mIsAbilityMode)
        {
        }
        //--In ability mode, move the cursor.
        else
        {
            //--If the cursor is already zero, cancel ability mode.
            if(mAbilityCursor == 0)
            {
                mIsAbilityMode = false;
            }
            //--Move the cursor up.
            else
            {
                //--Move.
                mAbilityCursor --;

                //--Check the minimum case.
                if(mAbilityCursor < mAbilityRenderMin + 2) mAbilityRenderMin --;
            }

            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
    }

    //--Back to the main menu.
    if(rControlManager->IsFirstPress("Cancel"))
    {
        //--Cancel ability mode.
        if(mIsAbilityMode)
        {
            mIsAbilityMode = false;
        }
        //--Otherwise, exit.
        else
        {
            mCurrentMode = AM_MODE_BASE;
            mCurrentCursor = AM_MAIN_STATUS;
        }

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }
}

//--[Worker Function: RenderEquipmentImage]
//--Renders the image associated with an equipment slot for a character.
void RenderEquipmentImage(float pX, float pY, float pWidth, AdvCombatEntity *pEntity, SugarBitmap *pGemIndicator, const char *pEquipmentSlot, SugarFont *pRenderFont)
{
    //--Arg check.
    if(!pEntity || !pRenderFont || !pEquipmentSlot) return;

    //--Check if the entity has an item in this slot.
    AdventureItem *rCheckItem = pEntity->GetEquipmentBySlot(pEquipmentSlot);
    if(!rCheckItem) return;

    //--Pass to subroutine.
    //RenderItemImage(pX,pY, pWidth, rCheckItem, pGemIndicator, pRenderFont);
}

//--[Drawing Function]
void AdventureMenu::RenderStatusMenu()
{
    //--[Documentation]
    //--Verify images.
    if(!Images.mIsReady) return;

    //--Get the character in question.
    AdvCombatEntity *rActiveEntity = AdvCombat::Fetch()->GetActiveMemberI(mStatusCursor);
    if(!rActiveEntity) return;

    //--[Static Backing]
    //--Background. Mostly transparent.
    SugarBitmap::DrawFullColor(StarlightColor::MapRGBAI(0, 0, 0, 64));

    //--Render static pieces.
    Images.StatusUI.rBackgroundFill->Draw();
    Images.StatusUI.rBannerTop->Draw();

    //--[Player Character Render]
    //--Render the player character in question. First, activate stenciling and render a mask.
    glEnable(GL_STENCIL_TEST);
    glColorMask(false, false, false, false);
    glDepthMask(false);
    glStencilMask(0xFF);
    glStencilFunc(GL_ALWAYS, 1, 0xFF);
    glStencilOp(GL_REPLACE, GL_REPLACE, GL_REPLACE);
    Images.StatusUI.rCharOverlayMask->Draw();

    //--These overlays render on the 2, to prevent accidental overlay with the main image.
    glStencilFunc(GL_ALWAYS, 2, 0xFF);
    Images.StatusUI.rCharOverlayMaskLft->Draw();
    Images.StatusUI.rCharOverlayMaskRgt->Draw();

    //--GL Setup for rendering.
    glColorMask(true, true, true, true);
    glDepthMask(true);
    glStencilMask(0xFF);
    glStencilFunc(GL_EQUAL, 1, 0xFF);
    glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);

    //--Current character, always exists.
    SugarBitmap *rFieldPortrait = rActiveEntity->GetCombatPortrait();
    TwoDimensionRealPoint cMainRenderCoords = rActiveEntity->GetUIRenderPosition(ACE_UI_INDEX_STATUS_MAIN);
    bool tIsSmallPortraitMode = OptionsManager::Fetch()->GetOptionB("LowResAdventureMode");
    if(rFieldPortrait)
    {
        //--Normal rendering:
        if(!tIsSmallPortraitMode)
        {
            rFieldPortrait->Draw(cMainRenderCoords.mXCenter, cMainRenderCoords.mYCenter);
        }
        //--Scaled rendering.
        else
        {
            rFieldPortrait->DrawScaled(cMainRenderCoords.mXCenter, cMainRenderCoords.mYCenter, LOWRES_SCALEINV, LOWRES_SCALEINV);
        }
    }

    //--Draw the left character, if it exists. If the cursor is at 0, no character renders.
    AdvCombatEntity *rLftEntity = AdvCombat::Fetch()->GetActiveMemberI(mStatusCursor-1);
    if(rLftEntity)
    {
        //--Setup.
        glStencilFunc(GL_EQUAL, 2, 0xFF);
        rFieldPortrait = rLftEntity->GetCombatPortrait();
        cMainRenderCoords = rLftEntity->GetUIRenderPosition(ACE_UI_INDEX_STATUS_LEFT);

        //--Normal rendering:
        if(!tIsSmallPortraitMode)
        {
            if(rFieldPortrait) rFieldPortrait->Draw(cMainRenderCoords.mXCenter, cMainRenderCoords.mYCenter);
        }
        //--Scaled rendering.
        else
        {
            if(rFieldPortrait) rFieldPortrait->DrawScaled(cMainRenderCoords.mXCenter, cMainRenderCoords.mYCenter, LOWRES_SCALEINV, LOWRES_SCALEINV);
        }
    }

    //--Draw the right character, if it exists.
    AdvCombatEntity *rRgtEntity = AdvCombat::Fetch()->GetActiveMemberI(mStatusCursor+1);
    if(rRgtEntity)
    {
        //--Setup.
        glStencilFunc(GL_EQUAL, 2, 0xFF);
        rFieldPortrait = rRgtEntity->GetCombatPortrait();
        cMainRenderCoords = rRgtEntity->GetUIRenderPosition(ACE_UI_INDEX_STATUS_RIGHT);

        //--Normal rendering:
        if(!tIsSmallPortraitMode)
        {
            if(rFieldPortrait) rFieldPortrait->Draw(cMainRenderCoords.mXCenter, cMainRenderCoords.mYCenter);
        }
        //--Scaled rendering.
        else
        {
            if(rFieldPortrait) rFieldPortrait->DrawScaled(cMainRenderCoords.mXCenter, cMainRenderCoords.mYCenter, LOWRES_SCALEINV, LOWRES_SCALEINV);
        }
    }

    //--Clean up.
    glDisable(GL_STENCIL_TEST);

    //--[Equipment Icons]
    //--Render equipment icons, based on the player's inventory.
    float cIndL = 8.0f;
    float cIndT = 3.0f;
    float cWidth = 125.0f;

    //--[Static Fronting]
    //--Parts that go over dynamic UI pieces.
    Images.StatusUI.rCharInfo->Draw();
    //Images.StatusUI.rInventory->Draw();
    Images.StatusUI.rNavButtons->Draw();
    Images.StatusUI.rEXPBarFrame->Draw();
    Images.StatusUI.rHealthBarFrame->Draw();

    //--[Text Rendering]
    //--Indicate there are abilities to be had by pressing down.
    float cOff = -4.0f;
    Images.StatusUI.rHeadingFont->DrawText(102.0f,  22.5f + cOff, SUGARFONT_AUTOCENTER_X | SUGARFONT_AUTOCENTER_Y, 1.0f, "Back");
    Images.StatusUI.rHeadingFont->DrawText(387.0f, 115.0f + cOff, SUGARFONT_AUTOCENTER_X | SUGARFONT_AUTOCENTER_Y, 1.0f, "Status");
    Images.StatusUI.rHeadingFont->DrawText(677.0f, 243.0f + cOff, SUGARFONT_AUTOCENTER_X | SUGARFONT_AUTOCENTER_Y, 1.0f, "Equipment");
    Images.StatusUI.rHeadingFont->DrawText(979.0f, 159.0f + cOff, SUGARFONT_AUTOCENTER_X | SUGARFONT_AUTOCENTER_Y, 1.0f, "Resistances");
    Images.StatusUI.rHeadingFont->DrawText(711.0f, 752.0f + cOff, SUGARFONT_AUTOCENTER_X | SUGARFONT_AUTOCENTER_Y, 1.0f, "Abilities");

    //--Character properties.
    float cHeiPerLine = 26.0f;

    //--Health Bar.
    Images.StatusUI.rHealthBarUnder->Draw();
    float tHPPercent = rActiveEntity->GetHealthPercent();
    if(tHPPercent > 0.0f)
    {
        //--Rendering setup.
        float cLft = Images.StatusUI.rHealthBarFill->GetXOffset();
        float cTop = Images.StatusUI.rHealthBarFill->GetYOffset();
        float cRgt = Images.StatusUI.rHealthBarFill->GetXOffset() + (Images.StatusUI.rHealthBarFill->GetWidth() * tHPPercent);
        float cBot = Images.StatusUI.rHealthBarFill->GetYOffset() + Images.StatusUI.rHealthBarFill->GetHeight();
        float cTxL = 0.0f;
        float cTxT = 0.0f;
        float cTxR = tHPPercent;
        float cTxB = 1.0f;

        //--Flip since OpenGL is upside-down.
        cTxT = 1.0f - cTxT;
        cTxB = 1.0f - cTxB;

        //--Render.
        Images.StatusUI.rHealthBarFill->Bind();
        glBegin(GL_QUADS);
            glTexCoord2f(cTxL, cTxT); glVertex2f(cLft, cTop);
            glTexCoord2f(cTxR, cTxT); glVertex2f(cRgt, cTop);
            glTexCoord2f(cTxR, cTxB); glVertex2f(cRgt, cBot);
            glTexCoord2f(cTxL, cTxB); glVertex2f(cLft, cBot);
        glEnd();
    }
    Images.StatusUI.rHealthBarFrame->Draw();

    //--Next level. Can change to "MAX!" if the character is at the max level.
    int tNextLevel = rActiveEntity->GetXPToNextLevel();
    if(tNextLevel > 0)
    {
        //--Text.
        Images.StatusUI.rMainlineFont->DrawTextArgs(586.0f, 176.0f, 0, 1.0f, "Exp: %i / %i", rActiveEntity->GetXP(), tNextLevel);

        //--We need to use manual rendering to indicate the bar's fill percentage.
        float tFillPercent = (float)rActiveEntity->GetXP() / (float)tNextLevel;
        float cLft = Images.StatusUI.rEXPBarFill->GetXOffset();
        float cTop = Images.StatusUI.rEXPBarFill->GetYOffset();
        float cRgt = cLft + (Images.StatusUI.rEXPBarFill->GetWidth() * tFillPercent);
        float cBot = Images.StatusUI.rEXPBarFill->GetYOffset() + (1.0f * Images.StatusUI.rEXPBarFill->GetHeight());
        float cTxL = 0.0f;
        float cTxT = 0.0f;
        float cTxR = tFillPercent;
        float cTxB = 1.0f;

        //--Flip the Y values of the textures since OpenGL is upside-down.
        cTxT = 1.0f - cTxT;
        cTxB = 1.0f - cTxB;

        //--Render.
        Images.StatusUI.rEXPBarFill->Bind();
        glBegin(GL_QUADS);
            glTexCoord2f(cTxL, cTxT); glVertex2f(cLft, cTop);
            glTexCoord2f(cTxR, cTxT); glVertex2f(cRgt, cTop);
            glTexCoord2f(cTxR, cTxB); glVertex2f(cRgt, cBot);
            glTexCoord2f(cTxL, cTxB); glVertex2f(cLft, cBot);
        glEnd();

        //--Render the overlay.
        Images.StatusUI.rEXPBarFrame->Draw();

        //--Current level, XP needed to next.
        Images.StatusUI.rHeadingFont-> DrawTextArgs(545.0f, 148.0f, SUGARFONT_AUTOCENTER_X | SUGARFONT_AUTOCENTER_Y, 1.0f, "%i", rActiveEntity->GetLevel()+1);
    }
    else
    {
        //--Text.
        Images.StatusUI.rMainlineFont->DrawTextArgs(586.0f, 176.0f, 0, 1.0f, "Exp: Maxed");

        //--Bars. Render at 100% fill.
        Images.StatusUI.rEXPBarFill->Draw();
        Images.StatusUI.rEXPBarFrame->Draw();

        //--Just show the current level.
        Images.StatusUI.rHeadingFont-> DrawTextArgs(545.0f, 148.0f, SUGARFONT_AUTOCENTER_X | SUGARFONT_AUTOCENTER_Y, 1.0f, "%i", rActiveEntity->GetLevel()+1);
    }

    //--[Resistances]
    //--Constants.
    float cTxtLft = 850.0f;
    float cTxtTop = 186.0f;
    float cIndentRes = 248.0f;
    float cTxtHei = Images.StatusUI.rMainlineFont->GetTextHeight();

    //--Render the left column.
    Images.StatusUI.rMainlineFont->DrawText(cTxtLft, cTxtTop + (cTxtHei *  0.0f), 0, 1.0f, "Slashing");
    Images.StatusUI.rMainlineFont->DrawText(cTxtLft, cTxtTop + (cTxtHei *  1.0f), 0, 1.0f, "Piercing");
    Images.StatusUI.rMainlineFont->DrawText(cTxtLft, cTxtTop + (cTxtHei *  2.0f), 0, 1.0f, "Striking");
    Images.StatusUI.rMainlineFont->DrawText(cTxtLft, cTxtTop + (cTxtHei *  3.0f), 0, 1.0f, "Flaming");
    Images.StatusUI.rMainlineFont->DrawText(cTxtLft, cTxtTop + (cTxtHei *  4.0f), 0, 1.0f, "Freezing");
    Images.StatusUI.rMainlineFont->DrawText(cTxtLft, cTxtTop + (cTxtHei *  5.0f), 0, 1.0f, "Shocking");
    Images.StatusUI.rMainlineFont->DrawText(cTxtLft, cTxtTop + (cTxtHei *  6.0f), 0, 1.0f, "Crusading");
    Images.StatusUI.rMainlineFont->DrawText(cTxtLft, cTxtTop + (cTxtHei *  7.0f), 0, 1.0f, "Obscuring");
    Images.StatusUI.rMainlineFont->DrawText(cTxtLft, cTxtTop + (cTxtHei *  8.0f), 0, 1.0f, "Bleeding");
    Images.StatusUI.rMainlineFont->DrawText(cTxtLft, cTxtTop + (cTxtHei *  9.0f), 0, 1.0f, "Blinding");
    Images.StatusUI.rMainlineFont->DrawText(cTxtLft, cTxtTop + (cTxtHei * 10.0f), 0, 1.0f, "Poisoning");
    Images.StatusUI.rMainlineFont->DrawText(cTxtLft, cTxtTop + (cTxtHei * 11.0f), 0, 1.0f, "Corroding");
    Images.StatusUI.rMainlineFont->DrawText(cTxtLft, cTxtTop + (cTxtHei * 12.0f), 0, 1.0f, "Terrifying");

    //--Render the right column which is the actual resistance values.
    for(int i = 0; i < ACE_RESIST_TOTAL; i ++)
    {
        //--Compute the resistance.
        float tResist = rActiveEntity->GetStatistic(ADVCE_STATS_FINAL, 0);

        //--Neutral: No damage bonus or penalty.
        if(tResist == 1.0f)
        {
            glColor3f(1.0f, 1.0f, 1.0f);
            Images.StatusUI.rMainlineFont->DrawTextArgs(cTxtLft + cIndentRes, cTxtTop + (cTxtHei * i), SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", (int)(tResist * 100.0f));
        }
        //--Immune: Takes no damage from this type.
        else if(tResist == 0.0f)
        {
            glColor3f(0.0f, 0.0f, 1.0f);
            Images.StatusUI.rMainlineFont->DrawText(cTxtLft + cIndentRes, cTxtTop + (cTxtHei * i), SUGARFONT_RIGHTALIGN_X, 1.0f, "Immune");
        }
        //--Heavily Resistant: 75% less damage or better.
        else if(tResist <= 0.25f)
        {
            glColor3f(0.6f, 0.0f, 1.0f);
            Images.StatusUI.rMainlineFont->DrawTextArgs(cTxtLft + cIndentRes, cTxtTop + (cTxtHei * i), SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", (int)(tResist * 100.0f));
        }
        //--Very Resistant: 50% less damage or better.
        else if(tResist <= 0.50f)
        {
            glColor3f(0.8f, 0.0f, 1.0f);
            Images.StatusUI.rMainlineFont->DrawTextArgs(cTxtLft + cIndentRes, cTxtTop + (cTxtHei * i), SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", (int)(tResist * 100.0f));
        }
        //--Resistant: 25% less damage or better.
        else if(tResist <= 0.75f)
        {
            glColor3f(1.0f, 0.0f, 1.0f);
            Images.StatusUI.rMainlineFont->DrawTextArgs(cTxtLft + cIndentRes, cTxtTop + (cTxtHei * i), SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", (int)(tResist * 100.0f));
        }
        //--Weak: +25% damage or less.
        else if(tResist <= 1.25f)
        {
            glColor3f(1.0f, 0.0f, 0.7f);
            Images.StatusUI.rMainlineFont->DrawTextArgs(cTxtLft + cIndentRes, cTxtTop + (cTxtHei * i), SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", (int)(tResist * 100.0f));
        }
        //--Vulnerable: +50% damage or less.
        else if(tResist <= 1.50f)
        {
            glColor3f(1.0f, 0.0f, 0.3f);
            Images.StatusUI.rMainlineFont->DrawTextArgs(cTxtLft + cIndentRes, cTxtTop + (cTxtHei * i), SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", (int)(tResist * 100.0f));
        }
        //--Bane: +75% damage or worse.
        else
        {
            glColor3f(1.0f, 0.0f, 0.0f);
            Images.StatusUI.rMainlineFont->DrawTextArgs(cTxtLft + cIndentRes, cTxtTop + (cTxtHei * i), SUGARFONT_RIGHTALIGN_X, 1.0f, "%i", (int)(tResist * 100.0f));
        }

        //--Clean.
        glColor3f(1.0f, 1.0f, 1.0f);
    }

    //--If the ability inspector is open, render that.
    if(mAbilityTimer > 0)
    {
        //--Compute offset.
        float cYOffset = (1.0f - EasingFunction::QuadraticOut(mAbilityTimer, AM_ABILITY_MODE_TICKS)) * CANY;
        glTranslatef(0.0f, cYOffset, 0.0f);
        RenderAbilityInspector(rActiveEntity);
        glTranslatef(0.0f, -cYOffset, 0.0f);
    }
}
void AdventureMenu::RenderAbilityInspector(AdvCombatEntity *pCharacter)
{
    //--Renders a listing of abilities for the provided character and the description of the highlighted ability.
    if(!pCharacter || !Images.mIsReady) return;

    //--Backing.
    Images.StatusUI.rAbilityInspector->Draw();

    //--Header Text.
    Images.StatusUI.rHeadingFont->DrawText(359.0f, 42.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Abilities");

    //--Constants.
    float cAbilityLft = 251.0f;
    float cAbilityTop = 100.0f;
    float cAbilityInd =  10.0f;
    float cAbilitySpY =  55.0f;
    float cNameRgt = 471.0f;

    //--Variables.
    int i = 0;
    float tCursorY = cAbilityTop;
    float cCursorRenderX = 0.0f;
    float cCursorRenderY = 0.0f;
    int tRendersLeft = AM_ABILITIES_PER_PAGE;
    /*
    AdventureCombatActionCategory *rHighlightedAbility = NULL;

    //--Run down the character's ability list and render the abilities.
    SugarLinkedList *rAbilityBaseList = pCharacter->GetAbilityList();
    AdventureCombatActionCategory *rCategory = (AdventureCombatActionCategory *)rAbilityBaseList->PushIterator();
    while(rCategory)
    {
        //--If i is less than the render minimum, skip rendering.
        if(i < mAbilityRenderMin)
        {
        }
        //--Render normally.
        else
        {
            //--Render the ability backing and icon.
            SugarBitmap *rBackingImg = rCategory->GetBackingImg();
            if(rBackingImg) rBackingImg->Draw(cAbilityLft, tCursorY);

            //--Primary Display
            SugarBitmap *rDisplayImg = rCategory->GetDisplayImg();
            if(rDisplayImg) rDisplayImg->Draw(cAbilityLft, tCursorY);

            //--Combo Point Indicator.
            SugarBitmap *rComboImg = rCategory->GetComboImg();
            if(rComboImg) rComboImg->Draw(cAbilityLft, tCursorY);

            //--Name of the ability.
            float cNameLft = cAbilityLft + 55.0f;
            float cWid = (cNameRgt) - (cNameLft);
            RenderTextToFit(Images.StatusUI.rHeadingFont, cNameLft + (cWid * 0.5f), tCursorY + 24.0f, cWid, rCategory->GetName());

            //--Store cursor rendering position.
            if(mAbilityCursor == i)
            {
                rHighlightedAbility = rCategory;
                cCursorRenderX = cAbilityLft;
                cCursorRenderY = tCursorY;
            }

            //--Move cursor down.
            tRendersLeft --;
            tCursorY = tCursorY + cAbilitySpY;
        }

        //--Next slot.
        i ++;

        //--If this category is a category with sublists, render those as well.
        if(rCategory->GetType() == POINTER_TYPE_ADVENTURECOMBATACTIONCATEGORY)
        {
            //--List.
            SugarLinkedList *rSublist = rCategory->GetSublist();
            AdventureCombatActionCategory *rSubCategory = (AdventureCombatActionCategory *)rSublist->PushIterator();
            while(rSubCategory)
            {
                //--If i is less than the render minimum, skip rendering.
                if(i < mAbilityRenderMin)
                {
                }
                //--Render normally.
                else
                {
                    //--Render the ability backing and icon.
                    SugarBitmap *rBackingImg = rSubCategory->GetBackingImg();
                    if(rBackingImg) rBackingImg->Draw(cAbilityLft + cAbilityInd, tCursorY);

                    //--Primary Display
                    SugarBitmap *rDisplayImg = rSubCategory->GetDisplayImg();
                    if(rDisplayImg) rDisplayImg->Draw(cAbilityLft + cAbilityInd, tCursorY);

                    //--Combo Point Indicator.
                    SugarBitmap *rComboImg = rSubCategory->GetComboImg();
                    if(rComboImg) rComboImg->Draw(cAbilityLft + cAbilityInd, tCursorY);

                    //--Name of the ability.
                    float cNameLft = cAbilityLft + 55.0f + cAbilityInd;
                    float cWid = (cNameRgt) - (cNameLft);
                    RenderTextToFit(Images.StatusUI.rHeadingFont, cNameLft + (cWid * 0.5f), tCursorY + 24.0f, cWid, rSubCategory->GetName());

                    //--Store cursor rendering position.
                    if(mAbilityCursor == i)
                    {
                        rHighlightedAbility = rSubCategory;
                        cCursorRenderX = cAbilityLft + cAbilityInd;
                        cCursorRenderY = tCursorY;
                    }

                    //--Move cursor down.
                    tRendersLeft --;
                    tCursorY = tCursorY + cAbilitySpY;
                }

                //--Stop rendering if over the cap.
                if(tRendersLeft < 1)
                {
                    rSublist->PopIterator();
                    break;
                }

                //--Next slot.
                i ++;

                //--Next category.
                rSubCategory = (AdventureCombatActionCategory *)rSublist->AutoIterate();
            }
        }

        //--Stop rendering if over the cap.
        if(tRendersLeft < 1)
        {
            rAbilityBaseList->PopIterator();
            break;
        }

        //--Next category.
        rCategory = (AdventureCombatActionCategory *)rAbilityBaseList->AutoIterate();
    }
    */

    //--Render the cursor over the ability.
    Images.StatusUI.rAbilityCursor->Draw(cCursorRenderX, cCursorRenderY);

    //--[Ability Description]
    //--Render information about whichever ability is highlighted.
    /*if(!rHighlightedAbility) return;

    //--Name of the ability.
    Images.StatusUI.rHeadingFont->DrawText(846.0f, 42.0f, SUGARFONT_AUTOCENTER_X, 1.0f, rHighlightedAbility->GetName());

    //--Constants.
    float cXPosition = 559.0f;
    float cWidth = 572.0f;

    //--Variables.
    float tYPosition = 112.0f;

    //--Lines auto-parse out to meet the length of the description box.
    int tCharsParsed = 0;
    int tCharsParsedTotal = 0;
    const char *rDescription = rHighlightedAbility->GetExtendedDescription();
    if(rDescription)
    {
        //--Setup.
        int tStringLen = (int)strlen(rDescription);

        //--Loop until the whole description has been parsed out.
        while(tCharsParsedTotal < tStringLen)
        {
            //--Run the subdivide.
            char *tDescriptionLine = Subdivide::SubdivideString(tCharsParsed, &rDescription[tCharsParsedTotal], -1, cWidth, Images.StatusUI.rMainlineFont, 1.0f);
            RenderAndAdvance(cXPosition, tYPosition, Images.EquipmentUI.rMainlineFont, tDescriptionLine);

            //--Move to the next line.
            tCharsParsedTotal += tCharsParsed;

            //--Clean up.
            free(tDescriptionLine);
        }
    }*/
}
