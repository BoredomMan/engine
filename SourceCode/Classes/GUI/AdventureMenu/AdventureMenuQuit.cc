//--Base
#include "AdventureMenu.h"

//--Classes
#include "VisualLevel.h"

//--CoreClasses
#include "SugarFont.h"

//--Definitions
#include "Global.h"

//--Libraries
//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "DisplayManager.h"
#include "MapManager.h"

#define FADE_TICKS 30

void AdventureMenu::SetToQuitMenu()
{
    mQuitCursor = 0;
    mCurrentMode = AM_MODE_QUIT;
    mIsTitleQuitConfirmMode = false;
    mIsFullQuitConfirmMode = false;
    mIsFadingForQuit = false;
}
void AdventureMenu::UpdateQuitMenu()
{
    //--Standard update for the cursor. Highly limited selection on the quit menu, as expected.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Fading.
    if(mIsFadingForQuit)
    {
        //--Timer.
        mQuitFadeTimer ++;

        //--Ending case.
        if(mQuitFadeTimer >= FADE_TICKS)
        {
            //--Ending: Quit to the title screen.
            if(mIsTitleQuitConfirmMode)
            {
                MapManager::Fetch()->mBackToTitle = true;
            }
            //--Ending: Quit the program.
            else
            {
                Global::Shared()->gQuit = true;
            }
        }
    }
    //--Normal:
    else if(!mIsTitleQuitConfirmMode && !mIsFullQuitConfirmMode)
    {
        //--Up...
        if(rControlManager->IsFirstPress("Up"))
        {
            if(mQuitCursor > 0)
            {
                mQuitCursor --;
                AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            }
        }
        //--Down...
        else if(rControlManager->IsFirstPress("Down"))
        {
            if(mQuitCursor < AM_QUIT_TOTAL - 1)
            {
                mQuitCursor ++;
                AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            }
        }

        //--Pressing Activate...
        if(rControlManager->IsFirstPress("Activate"))
        {
            //--On the cancel...
            if(mQuitCursor == AM_QUIT_CANCEL)
            {
                mCurrentMode = AM_MODE_BASE;
            }
            //--Quit to Title: Confirm.
            else if(mQuitCursor == AM_QUIT_TO_TITLE)
            {
                mQuitCursor = 0;
                mIsTitleQuitConfirmMode = true;
            }
            //--Quit Program: Confirm.
            else if(mQuitCursor == AM_QUIT_PROGRAM)
            {
                mQuitCursor = 0;
                mIsFullQuitConfirmMode = true;
            }

            //--All cases trigger SFX.
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }

        //--Cancel, back to the main menu.
        if(rControlManager->IsFirstPress("Cancel"))
        {
            mCurrentMode = AM_MODE_BASE;
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
    }
    //--Confirm quit to title:
    else if(mIsTitleQuitConfirmMode)
    {
        //--Left
        if(rControlManager->IsFirstPress("Left"))
        {
            if(mQuitCursor > 0)
            {
                mQuitCursor = 0;
                AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            }
        }
        //--Right...
        else if(rControlManager->IsFirstPress("Right"))
        {
            if(mQuitCursor < 1)
            {
                mQuitCursor = 1;
                AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            }
        }

        //--Activate...
        if(rControlManager->IsFirstPress("Activate"))
        {
            //--Left decision is "No".
            if(mQuitCursor == 0)
            {
                mIsTitleQuitConfirmMode = false;
                mQuitCursor = AM_QUIT_TO_TITLE;
            }
            //--Right decision is "Yes".
            else
            {
                mIsFadingForQuit = true;
                mQuitFadeTimer = 0;
            }

            //--All cases trigger SFX.
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }

        //--Cancel, back to the quit menu.
        if(rControlManager->IsFirstPress("Cancel"))
        {
            mIsTitleQuitConfirmMode = false;
            mQuitCursor = AM_QUIT_TO_TITLE;
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
    }
    //--Confirm quit program:
    else if(mIsFullQuitConfirmMode)
    {
        //--Left
        if(rControlManager->IsFirstPress("Left"))
        {
            if(mQuitCursor > 0)
            {
                mQuitCursor = 0;
                AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            }
        }
        //--Right...
        else if(rControlManager->IsFirstPress("Right"))
        {
            if(mQuitCursor < 1)
            {
                mQuitCursor = 1;
                AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            }
        }

        //--Activate...
        if(rControlManager->IsFirstPress("Activate"))
        {
            //--Left decision is "No".
            if(mQuitCursor == 0)
            {
                mIsFullQuitConfirmMode = false;
                mQuitCursor = AM_QUIT_PROGRAM;
            }
            //--Right decision is "Yes".
            else
            {
                mIsFadingForQuit = true;
                mQuitFadeTimer = 0;
            }

            //--All cases trigger SFX.
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }

        //--Cancel, back to the quit menu.
        if(rControlManager->IsFirstPress("Cancel"))
        {
            mIsFullQuitConfirmMode = false;
            mQuitCursor = AM_QUIT_PROGRAM;
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
    }
}
void AdventureMenu::RenderQuitMenu()
{
    //--Renders the quitting submenu. This renders over the base menu.
    if(!Images.mIsReady) return;

    //--Rendering Constants
    float cTextHeight = Images.Data.rMenuFont->GetTextHeight();

    //--Normal mode:
    if(!mIsTitleQuitConfirmMode && !mIsFullQuitConfirmMode)
    {
        //--Render a box in the middle of the screen.
        TwoDimensionReal tBox;
        tBox.SetWH(VIRTUAL_CANVAS_X * 0.41f, VIRTUAL_CANVAS_Y * 0.35f, VIRTUAL_CANVAS_X * 0.18f, (cTextHeight * 3.0f) + (AM_STD_INDENT * 2.0f));
        VisualLevel::RenderBorderCardOver(Images.Data.rBorderCard, tBox, 0xFFFF);

        //--Render options.
        Images.Data.rMenuFont->DrawText(tBox.mLft + AM_STD_INDENT, tBox.mTop + AM_STD_INDENT + (cTextHeight * 0.00f), 0, 1.0f, " Cancel");
        Images.Data.rMenuFont->DrawText(tBox.mLft + AM_STD_INDENT, tBox.mTop + AM_STD_INDENT + (cTextHeight * 1.00f), 0, 1.0f, " Quit To Title");
        Images.Data.rMenuFont->DrawText(tBox.mLft + AM_STD_INDENT, tBox.mTop + AM_STD_INDENT + (cTextHeight * 2.00f), 0, 1.0f, " Quit Program");

        //--Cursor.
        RenderCursor(tBox.mLft + AM_STD_INDENT, tBox.mTop + AM_STD_INDENT + (mQuitCursor * cTextHeight) + 2.0f + 7);
    }
    //--Quitting back to title.
    else if(mIsTitleQuitConfirmMode)
    {
        //--Render a box in the middle of the screen.
        TwoDimensionReal tBox;
        tBox.SetWH(VIRTUAL_CANVAS_X * 0.375f, VIRTUAL_CANVAS_Y * 0.35f, VIRTUAL_CANVAS_X * 0.25f, (cTextHeight * 2.0f) + (AM_STD_INDENT * 2.0f));
        VisualLevel::RenderBorderCardOver(Images.Data.rBorderCard, tBox, 0xFFFF);

        //--Header.
        Images.Data.rMenuFont->DrawText(tBox.mLft + AM_STD_INDENT,          tBox.mTop + AM_STD_INDENT + (cTextHeight * 0.00f), 0, 1.0f, "Return to title screen?");
        Images.Data.rMenuFont->DrawText(tBox.mLft + AM_STD_INDENT,          tBox.mTop + AM_STD_INDENT + (cTextHeight * 1.00f), 0, 1.0f, " No");
        Images.Data.rMenuFont->DrawText(tBox.mLft + AM_STD_INDENT + 128.0f, tBox.mTop + AM_STD_INDENT + (cTextHeight * 1.00f), 0, 1.0f, " Yes");

        //--Cursor.
        if(mQuitCursor == 0)
        {
            RenderCursor(tBox.mLft + AM_STD_INDENT, tBox.mTop + AM_STD_INDENT + (cTextHeight * 1.00f) + 2.0f + 7);
        }
        //--No.
        else
        {
            RenderCursor(tBox.mLft + AM_STD_INDENT + 128.0f, tBox.mTop + AM_STD_INDENT + (cTextHeight * 1.00f) + 2.0f + 7);
        }
    }
    //--Quitting the program entirely.
    else if(mIsFullQuitConfirmMode)
    {
        //--Render a box in the middle of the screen.
        TwoDimensionReal tBox;
        tBox.SetWH(VIRTUAL_CANVAS_X * 0.39f, VIRTUAL_CANVAS_Y * 0.35f, VIRTUAL_CANVAS_X * 0.22f, (cTextHeight * 2.0f) + (AM_STD_INDENT * 2.0f));
        VisualLevel::RenderBorderCardOver(Images.Data.rBorderCard, tBox, 0xFFFF);

        //--Header.
        Images.Data.rMenuFont->DrawText(tBox.mLft + AM_STD_INDENT,          tBox.mTop + AM_STD_INDENT + (cTextHeight * 0.00f), 0, 1.0f, "Quit the program?");
        Images.Data.rMenuFont->DrawText(tBox.mLft + AM_STD_INDENT,          tBox.mTop + AM_STD_INDENT + (cTextHeight * 1.00f), 0, 1.0f, " No");
        Images.Data.rMenuFont->DrawText(tBox.mLft + AM_STD_INDENT + 128.0f, tBox.mTop + AM_STD_INDENT + (cTextHeight * 1.00f), 0, 1.0f, " Yes");

        //--Cursor.
        if(mQuitCursor == 0)
        {
            RenderCursor(tBox.mLft + AM_STD_INDENT, tBox.mTop + AM_STD_INDENT + (cTextHeight * 1.00f) + 2.0f + 7);
        }
        //--No.
        else
        {
            RenderCursor(tBox.mLft + AM_STD_INDENT + 128.0f, tBox.mTop + AM_STD_INDENT + (cTextHeight * 1.00f) + 2.0f + 7);
        }
    }

    //--If fading out...
    if(mIsFadingForQuit)
    {
        //--Compute alpha.
        float tAlpha = mQuitFadeTimer / (float)FADE_TICKS;

        //--Setup.
        glDisable(GL_TEXTURE_2D);
        glColor4f(0.0f, 0.0f, 0.0f, tAlpha);

        //--Render.
        glBegin(GL_QUADS);
            glVertex2f(            0.0f,             0.0f);
            glVertex2f(VIRTUAL_CANVAS_X,             0.0f);
            glVertex2f(VIRTUAL_CANVAS_X, VIRTUAL_CANVAS_Y);
            glVertex2f(            0.0f, VIRTUAL_CANVAS_Y);
        glEnd();

        //--Clean.
        glEnable(GL_TEXTURE_2D);
        glColor3f(1.0f, 1.0f, 1.0f);
    }
}
