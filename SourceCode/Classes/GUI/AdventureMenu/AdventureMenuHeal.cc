//--Base
#include "AdventureMenu.h"

//--Classes
#include "AdvCombat.h"
#include "AdvCombatEntity.h"
#include "AdventureLevel.h"
#include "AdventureInventory.h"
#include "TilemapActor.h"
#include "VisualLevel.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarFont.h"
#include "SugarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"

//--Libraries
//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "LuaManager.h"
#include "OptionsManager.h"

//--[System]
void AdventureMenu::SetToHealMode()
{
    //--Flags.
    mCurrentMode = AM_MODE_HEAL;
    mCurrentCursor = 0;
}

//--[Manipulators]
//--[Core Methods]
//--[Update]
void AdventureMenu::UpdateHealMode()
{
    //--Handles controls for the warp menu. Handles up/down/activate/cancel.
    ControlManager *rControlManager = ControlManager::Fetch();
    AdvCombat *rAdventureCombat = AdvCombat::Fetch();

    //--Up!
    if(rControlManager->IsFirstPress("Up") && !rControlManager->IsFirstPress("Down"))
    {
        //--Decrement.
        if(mCurrentCursor > 0)
        {
            mCurrentCursor = 0;
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
    }

    //--Down!
    if(rControlManager->IsFirstPress("Down") && !rControlManager->IsFirstPress("Up"))
    {
        if(mCurrentCursor == 0)
        {
            mCurrentCursor = 1;
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
    }

    //--Left.
    if(rControlManager->IsFirstPress("Left") && !rControlManager->IsFirstPress("Right"))
    {
        //--Does nothing if the cursor is at 0.
        if(mCurrentCursor <= 1) return;

        //--Decrement.
        mCurrentCursor --;
        if(mCurrentCursor < 1) mCurrentCursor = 1;

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }

    //--Right.
    if(rControlManager->IsFirstPress("Right") && !rControlManager->IsFirstPress("Left"))
    {
        //--Does nothing if the cursor is at max.
        if(mCurrentCursor >= rAdventureCombat->GetActivePartyCount()) return;

        //--Increment.
        mCurrentCursor ++;
        if(mCurrentCursor >= rAdventureCombat->GetActivePartyCount()+1) mCurrentCursor --;

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }

    //--Heal.
    if(rControlManager->IsFirstPress("Activate") && xWarpExecuteScript)
    {
        //--Send the instruction.
        rAdventureCombat->HealFromDoctorBag(mCurrentCursor - 1);

        //--SFX.
        AudioManager::Fetch()->PlaySound("Combat|DoctorBag");
    }
    //--Back to the main menu.
    else if(rControlManager->IsFirstPress("Cancel"))
    {
        mCurrentMode = AM_MODE_BASE;
        mCurrentCursor = AM_MAIN_HEAL;
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }
}

//--[Drawing]
void RenderMainMenuButton(SugarBitmap *pBtnImg, SugarFont *pRenderFont, int pSlot, bool pIsExtended, bool pIsGreyed, const char *pText);
void RenderTextToFit(SugarFont *pFont, float pX, float pY, float pMaxWid, const char *pText, ...);
void AdventureMenu::RenderHealMode()
{
    //--[Documentation and Setup]
    //--Rendering. Renders the list of warp destinations and the cursor.
    if(!Images.mIsReady) return;
    AdvCombat *rAdventureCombat = AdvCombat::Fetch();

    //--[Main Menu]
    int tOldCursor = mCurrentCursor;
    mCurrentCursor = AM_MAIN_HEAL;
    RenderMainMenu();
    mCurrentCursor = tOldCursor;

    //--Darken the main menu.
    SugarBitmap::DrawFullColor(StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, 0.80f));

    //--[Static Parts]
    //Images.DoctorUI.rOptionsDeactivated->Draw();
    Images.DoctorUI.rMenuBack->Draw();
    Images.DoctorUI.rMenuHeaderA->Draw();
    Images.DoctorUI.rMenuHeaderB->Draw();
    Images.DoctorUI.rHeadingFont->DrawText(685.0f, 146.0f, SUGARFONT_AUTOCENTER_XY, 1.0f, "Heal");

    //--Button text.
    //RenderMainMenuButton(Images.BaseMenu.rBtnBase, Images.BaseMenu.rHeadingFont, 2, true,  false, "Doctor Bag");

    //--[Current Doctor Bag Charges]
    //--Bar fill constants.
    int tChargesCur = AdventureInventory::Fetch()->GetDoctorBagCharges();
    int tChargesMax = AdventureInventory::Fetch()->GetDoctorBagChargesMax();
    float tEfficiency = AdventureInventory::Fetch()->GetDoctorBagPotency();
    float tPercent = (float)tChargesCur / (float)tChargesMax;
    if(tEfficiency < 1.0f) tEfficiency = 1.0f;

    //--Render the bar, the frames over it.
    Images.DoctorUI.rBarHeader->Draw();
    SugarBitmap::DrawHorizontalPercent(Images.DoctorUI.rBarFill, tPercent);
    Images.DoctorUI.rBarFrame->Draw();

    //--Render the text values.
    Images.DoctorUI.rMainlineFont->DrawTextArgs(685.0f, 373.0f, SUGARFONT_AUTOCENTER_XY, 1.0f, "Doctor Bag: %i / %i", tChargesCur, tChargesMax);
    Images.DoctorUI.rMainlineFont->DrawTextArgs(685.0f, 443.0f, SUGARFONT_AUTOCENTER_XY, 1.0f, "Potency: %i%%", (int)((tEfficiency+0.001f) * 100.0f));

    //--[Buttons]
    //--Heal All.
    if(mCurrentCursor != 0) StarlightColor::SetMixer(0.5f, 0.5f, 0.5f, 1.0f);
    Images.DoctorUI.rHealAllBtn->Draw();
    Images.DoctorUI.rHeadingFont->DrawText(685.0f, 203.0f, SUGARFONT_AUTOCENTER_XY, 1.0f, "Heal All");
    StarlightColor::ClearMixer();

    //--Constants
    float cXOffset   =   0.0f;
    float cYOffset   =   0.0f;
    float cIncrement = 116.0f;

    //--For each character:
    for(int i = 0; i < rAdventureCombat->GetActivePartyCount(); i ++)
    {
        //--Check character.
        AdvCombatEntity *rSlotEntity = rAdventureCombat->GetActiveMemberI(i);
        if(!rSlotEntity) return;

        //--Darken.
        if(mCurrentCursor != i+1 && mCurrentCursor != 0) StarlightColor::SetMixer(0.5f, 0.5f, 0.5f, 1.0f);

        //--Portrait backing.
        Images.DoctorUI.rPortraitBtn->Draw(cXOffset, cYOffset);

        //--Turn on stencils to render the mask.
        glEnable(GL_STENCIL_TEST);
        glColorMask(false, false, false, false);
        glDepthMask(false);
        glStencilMask(0xFF);
        glStencilFunc(GL_ALWAYS, i+1+5, 0xFF);
        glStencilOp(GL_REPLACE, GL_REPLACE, GL_REPLACE);
        Images.DoctorUI.rPortraitMask->Draw(cXOffset, cYOffset);

        //--Turn on stencil-controlled rendering and render the combat portrait. It renders
        //  at 50% size.
        SugarBitmap *rFieldPortrait = rSlotEntity->GetCombatPortrait();
        if(rFieldPortrait)
        {
            //--Positioning.
            TwoDimensionRealPoint tPoint = rSlotEntity->GetUIRenderPosition(ACE_UI_INDEX_DOCTOR_MAIN);
            glTranslatef(tPoint.mXCenter + cXOffset, tPoint.mYCenter + cYOffset, 0.0f);
            glScalef(0.5f, 0.5f, 1.0f);

            //--If there's a countermask, render it.
            SugarBitmap *rCounterMask = rSlotEntity->GetCounterMask();
            if(rCounterMask)
            {
                glStencilFunc(GL_ALWAYS, 0, 0xFF);
                rCounterMask->Draw();
            }

            //--Render main portrait.
            glColorMask(true, true, true, true);
            glDepthMask(true);
            glStencilMask(0xFF);
            glStencilFunc(GL_EQUAL, i+1+5, 0xFF);
            glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);

            //--Variables.
            bool tIsSmallPortraitMode = OptionsManager::Fetch()->GetOptionB("LowResAdventureMode");

            //--Normal rendering.
            if(!tIsSmallPortraitMode)
            {
                rFieldPortrait->Draw();
            }
            //--Small portrait.
            else
            {
                rFieldPortrait->DrawScaled(0.0f, 0.0f, LOWRES_SCALEINV, LOWRES_SCALEINV);
            }

            //--Clean.
            glScalef(2.0f, 2.0f, 1.0f);
            glTranslatef((tPoint.mXCenter + cXOffset) * -1.0f, (tPoint.mYCenter + cYOffset) * -1.0f, 0.0f);
        }

        //--Turn stencils off.
        glDisable(GL_STENCIL_TEST);

        //--Render the HP value for this character.
        int tHealthCur = rSlotEntity->GetHealth();
        int tHealthMax = rSlotEntity->GetStatistic(ADVCE_STATS_FINAL, STATS_HPMAX);
        RenderTextToFit(Images.DoctorUI.rMainlineFont, 515.0f + cXOffset, 332.0f + cYOffset, 77.0f, "%i / %i", tHealthCur, tHealthMax);

        //--Next.
        cXOffset = cXOffset + cIncrement;
        StarlightColor::ClearMixer();
    }
}
