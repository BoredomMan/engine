//--Base
#include "AdventureMenu.h"

//--Classes
#include "AdvCombat.h"
#include "AdvCombatEntity.h"
#include "AdventureInventory.h"
#include "AdventureItem.h"
#include "VisualLevel.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarFont.h"
#include "SugarLinkedList.h"

//--Definitions
//--Libraries
//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "OptionsManager.h"

//--[Local Definitions]
#define SOCKET_SLIDE_TICKS 15.0f
#define AMEQ_MAX_ITEMS_PER_PAGE 10

//--[Forward Declarations]
void RenderEquipmentImage(float pX, float pY, float pWidth, AdvCombatEntity *pEntity, SugarBitmap *pGemIndicator, int pEquipmentSlot, SugarFont *pRenderFont);

//--[Manipulators]
void AdventureMenu::SetToEquipMenu()
{
    mRebuildModifyList = true;
    mEquipMinimumRender = 0;
    mCurrentMode = AM_MODE_EQUIP;
    mCurrentCursor = 0;
    mCharacterIndex = 0;
    mIsModifying = false;
    mModifySlot = 0;
}
void AdventureMenu::BuildSlotList(int pCharacterSlot, const char *pSlotType)
{
    //--Given a character slot and an equipment slot, creates a list of all items the player currently
    //  has which can be equipped in that slot.
    mValidEquipsList->ClearList();

    //--Get the character.
    AdvCombatEntity *rEquipEntity = AdvCombat::Fetch()->GetActiveMemberI(pCharacterSlot);
    if(!rEquipEntity) return;

    //--Pass the list to the AdventureInventory which will do the heavy lifting.
    AdventureInventory::Fetch()->BuildEquippableList(rEquipEntity, pSlotType, mValidEquipsList);
}

//--[Update]
void AdventureMenu::UpdateEquipMenu()
{
    //--[Documentation and Setup]
    //--Standard update for the cursor. Right and Left controls are enabled. Wrapping is disabled.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--In Socket Mode, let that handle the update.
    if(mIsSocketEditingMode)
    {
        if(mSocketEditTimer < SOCKET_SLIDE_TICKS) mSocketEditTimer ++;
        UpdateSocketMode();
        return;
    }
    //--Decrement.
    else
    {
        if(mSocketEditTimer > 0) mSocketEditTimer --;
    }

    //--Storage.
    int tOldCursor = mCurrentCursor;

    //--[Directional Keys]
    //--Up. Decrement slot by 1.
    if(rControlManager->IsFirstPress("Up"))
    {
        //--Currently-equipped list:
        if(!mIsModifying)
        {
            mCurrentCursor --;
            if(mCurrentCursor < 0) mCurrentCursor = 0;
        }
        //--Replace list.
        else
        {
            mCurrentCursor --;
            if(mCurrentCursor < 1) mCurrentCursor = 0;
            if(mCurrentCursor < mEquipMinimumRender + 4) mEquipMinimumRender --;
            if(mEquipMinimumRender < 0) mEquipMinimumRender = 0;
        }

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }
    //--Down. Increment slot by 1 if it's even. Does nothing when changing equipment.
    if(rControlManager->IsFirstPress("Down"))
    {
        //--Currently-equipped list:
        if(!mIsModifying)
        {
            mCurrentCursor ++;
            if(mCurrentCursor >= 10) mCurrentCursor = 10 - 1;
        }
        //--Replace list:
        else
        {
            if(mCurrentCursor < mValidEquipsList->GetListSize() - 1)
            {
                mCurrentCursor ++;
                if(mCurrentCursor > mEquipMinimumRender + 4) mEquipMinimumRender ++;
            }
        }

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }
    //--Left. Changes character.
    if(rControlManager->IsFirstPress("Left"))
    {
        //--Currently-equipped list:
        if(!mIsModifying)
        {
            //--At least one character to the left.
            if(mCharacterIndex > 0)
            {
                mCharacterIndex --;
                mRebuildModifyList = true;
            }
        }
        //--Replacing. Does nothing.
        else
        {
        }

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }
    //--Right. Changes character.
    if(rControlManager->IsFirstPress("Right"))
    {
        //--Currently-equipped list:
        if(!mIsModifying)
        {
            //--Check if there's a character above this one.
            AdvCombatEntity *rEquipEntity = AdvCombat::Fetch()->GetActiveMemberI(mCharacterIndex+1);
            if(rEquipEntity)
            {
                mCharacterIndex ++;
                mRebuildModifyList = true;
            }
        }
        //--Replacing. Does nothing.
        else
        {
        }

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
    }

    //--[Slot List]
    //--Rebuild the slot list if flagged, or if the cursor changed.
    if((mRebuildModifyList || mCurrentCursor != tOldCursor) && !mIsModifying)
    {
        //--Flag.
        mRebuildModifyList = false;

        //--Run it.
        BuildSlotList(mCharacterIndex, "Dummy");
    }

    //--[Activate and Cancel]
    //--Activate, enter the given submenu.
    if(rControlManager->IsFirstPress("Activate"))
    {
        //--Browsing mode, activates switching mode.
        if(!mIsModifying)
        {
            //--If there's nothing on the current slot list, just fail.
            if(mValidEquipsList->GetListSize() < 1)
            {
                AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
            }
            //--Otherwise, set flags.
            else
            {
                //--Flags.
                mIsModifying = true;
                mCurrentCursor = 0;

                //--SFX.
                AudioManager::Fetch()->PlaySound("Menu|Select");

                //--Figure out how much equipment space we need.
                float cTextHeight = Images.Data.rMenuFont->GetTextHeight();
                int cUseEntries = mValidEquipsList->GetListSize()+1;
                if(cUseEntries > 5) cUseEntries = 5;
                mEquipListDimensions.SetWH(mEquipDimensions.mLft, mEquipDimensions.mBot + 4.0f, mEquipDimensions.GetWidth(), (AM_STD_INDENT * 2.0f) + (cUseEntries * cTextHeight));
            }
        }
        //--In swapping mode, swaps the piece of equipment with the selected one.
        else
        {
            //--Get the character/item, order the equip.
            AdvCombatEntity *rEquipEntity = AdvCombat::Fetch()->GetActiveMemberI(mCharacterIndex);
            AdventureItem *rEquipItem = (AdventureItem *)mValidEquipsList->GetElementBySlot(mCurrentCursor);
            if(rEquipEntity)
            {
                //--If the swapping item in question happens to be the gem editor entry, open that instead.
                if(rEquipItem == AdventureInventory::xrDummyGemsItem)
                {
                    //--Get the piece of equipment. Check that it exists and has gem slots. It should not be logically
                    //  possible for this entry to exist if it doesn't, but hey you know. Bugs.
                    AdventureItem *rSelectedItem = rEquipEntity->GetEquipmentBySlot("Dummy");
                    if(!rSelectedItem || rSelectedItem->GetGemSlots() < 1)
                    {
                        AudioManager::Fetch()->PlaySound("Menu|Failed");
                    }
                    //--Activate socket mode.
                    else
                    {
                        //--SFX.
                        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");

                        //--Flags.
                        ActivateSocketMode();
                        return;
                    }
                }
                //--Otherwise, equip the item. Note that the xrDummyUnequip item has special code to unequip.
                else
                {
                    //--Cases.
                    rEquipEntity->EquipItemToSlot("Weapon A", rEquipItem);

                    //--In all cases, inventory deregisters the item in question. The entity takes ownership.
                    AdventureInventory::Fetch()->LiberateItemP(rEquipItem);

                    //--Check the last registered item, which will be the item unequipped. If it exists, remove any gems from it.
                    AdventureItem *rLastReggedItem = (AdventureItem *)AdventureInventory::Fetch()->rLastReggedItem;
                    if(rLastReggedItem)
                    {
                        for(int i = 0; i < ADITEM_MAX_GEMS; i ++)
                        {
                            AdventureItem *rPrevGem = rLastReggedItem->RemoveGemFromSlot(i);
                            if(rPrevGem)
                            {
                                AdventureInventory::Fetch()->RegisterItem(rPrevGem);
                            }
                        }
                    }
                }

                //--SFX.
                AudioManager::Fetch()->PlaySound("Menu|Select");
            }

            //--Flags.
            mIsModifying = false;
            mCurrentCursor = mModifySlot;
            mRebuildModifyList = true;

            //--Clear the equipment list.
            mValidEquipsList->ClearList();
        }
    }
    //--Cancel, hides this object.
    else if(rControlManager->IsFirstPress("Cancel"))
    {
        //--Browsing mode, go back to the main menu.
        if(!mIsModifying)
        {
            //--Flags.
            SetToMainMenu();
            mCurrentCursor = AM_MAIN_EQUIPMENT;

            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
        //--Swapping mode, cancels the swap.
        else
        {
            //--Flags.
            mIsModifying = false;
            mCurrentCursor = mModifySlot;

            //--SFX.
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");
        }
    }

    //--[Slot List]
    //--Second chance for rebuilding, happens if the mode changed.
    if(mRebuildModifyList)
    {
        //--Flag.
        mRebuildModifyList = false;

        //--Run it.
        BuildSlotList(mCharacterIndex, "Dummy");
    }
}

//--[Drawing]
void AdventureMenu::RenderEquipMenu()
{
    //--[Documentation and Setup]
    //--Render the equipment menu for whichever character is currently active.
    if(!Images.mIsReady) return;

    //--Get the character in question.
    AdvCombatEntity *rEquipEntity = AdvCombat::Fetch()->GetActiveMemberI(mCharacterIndex);
    if(!rEquipEntity) return;

    //--[Backing]
    Images.EquipmentUI.rFrames->Draw();

    //--[Character Portrait]
    //--Render the player character in question. First, activate stenciling and render a mask.
    glEnable(GL_STENCIL_TEST);
    glColorMask(false, false, false, false);
    glDepthMask(false);
    glStencilMask(0xFF);
    glStencilFunc(GL_ALWAYS, 1, 0xFF);
    glStencilOp(GL_REPLACE, GL_REPLACE, GL_REPLACE);
    Images.EquipmentUI.rPortraitMaskA->Draw();

    //--These overlays render on the 2, to prevent accidental overlay with the main image.
    glStencilFunc(GL_ALWAYS, 2, 0xFF);
    Images.EquipmentUI.rPortraitMaskB->Draw();
    Images.EquipmentUI.rPortraitMaskC->Draw();

    //--GL Setup for rendering.
    glColorMask(true, true, true, true);
    glDepthMask(true);
    glStencilMask(0xFF);
    glStencilFunc(GL_EQUAL, 1, 0xFF);
    glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);

    //--Current character, always exists.
    bool tIsSmallPortraitMode = OptionsManager::Fetch()->GetOptionB("LowResAdventureMode");
    SugarBitmap *rFieldPortrait = rEquipEntity->GetCombatPortrait();
    TwoDimensionRealPoint cMainRenderCoords = rEquipEntity->GetUIRenderPosition(ACE_UI_INDEX_EQUIP_MAIN);
    if(rFieldPortrait)
    {
        //--Normal rendering.
        if(!tIsSmallPortraitMode)
        {
            rFieldPortrait->Draw(cMainRenderCoords.mXCenter, cMainRenderCoords.mYCenter);
        }
        //--Small portrait.
        else
        {
            rFieldPortrait->DrawScaled(cMainRenderCoords.mXCenter, cMainRenderCoords.mYCenter, LOWRES_SCALEINV, LOWRES_SCALEINV);
        }
    }

    //--Draw the left character, if it exists. If the cursor is at 0, no character renders.
    AdvCombatEntity *rLftEntity = AdvCombat::Fetch()->GetActiveMemberI(mCharacterIndex-1);
    if(rLftEntity)
    {
        glStencilFunc(GL_EQUAL, 2, 0xFF);
        rFieldPortrait = rLftEntity->GetCombatPortrait();
        cMainRenderCoords = rLftEntity->GetUIRenderPosition(ACE_UI_INDEX_EQUIP_LEFT);
        if(rFieldPortrait)
        {
            //--Normal rendering.
            if(!tIsSmallPortraitMode)
            {
                rFieldPortrait->Draw(cMainRenderCoords.mXCenter, cMainRenderCoords.mYCenter);
            }
            //--Small portrait.
            else
            {
                rFieldPortrait->DrawScaled(cMainRenderCoords.mXCenter, cMainRenderCoords.mYCenter, LOWRES_SCALEINV, LOWRES_SCALEINV);
            }
        }
    }

    //--Draw the right character, if it exists.
    AdvCombatEntity *rRgtEntity = AdvCombat::Fetch()->GetActiveMemberI(mCharacterIndex+1);
    if(rRgtEntity)
    {
        glStencilFunc(GL_EQUAL, 2, 0xFF);
        rFieldPortrait = rRgtEntity->GetCombatPortrait();
        cMainRenderCoords = rRgtEntity->GetUIRenderPosition(ACE_UI_INDEX_EQUIP_RIGHT);
        if(rFieldPortrait)
        {
            //--Normal rendering.
            if(!tIsSmallPortraitMode)
            {
                rFieldPortrait->Draw(cMainRenderCoords.mXCenter, cMainRenderCoords.mYCenter);
            }
            //--Small portrait.
            else
            {
                rFieldPortrait->DrawScaled(cMainRenderCoords.mXCenter, cMainRenderCoords.mYCenter, LOWRES_SCALEINV, LOWRES_SCALEINV);
            }
        }
    }

    //--Clean up.
    glDisable(GL_STENCIL_TEST);

    //--[Static Parts]
    //--Static part rendering
    Images.EquipmentUI.rNameplate->Draw();

    //--Static text rendering.
    Images.StatusUI.rHeadingFont->DrawText(684.0f, 18.0f, SUGARFONT_AUTOCENTER_X, 2.0f, "Equipment");

    //--[Character Box]
    //--Character's name. Renders after and over the portrait.
    Images.EquipmentUI.rNameplate->Draw();
    Images.StatusUI.rHeadingFont->DrawText(153.0f, 360.0f, SUGARFONT_AUTOCENTER_X, 1.0f, rEquipEntity->GetDisplayName());

    //--Backing.
    SugarBitmap::DrawRectFill(197.0f, 230.0f, 197.0f + 75.0f, 230.0f + 125.0f, StarlightColor::cxBlack);

    //--Character's statistics. Borrows from the Inventory's list.
    float cStatIconX = 198.0f;
    float cStatTextX = 270.0f;
    float cStatYCur = 231.0f;
    float cStatYSpc = 20.0f;
    for(int i = 0; i < AM_INV_HEADER_INI + 1; i ++)
    {
        //--Render icon.
        Images.InventoryUI.rImageHeaders[i]->Draw(cStatIconX, cStatYCur);

        //--Render text. Right-align it.
        char tBuffer[32];
        int tStat = rEquipEntity->GetPropertyByInventoryHeaders(i, false);
        sprintf(tBuffer, "%i", tStat);
        int tWidth = Images.EquipmentUI.rMainlineFont->GetTextWidth(tBuffer);
        Images.EquipmentUI.rMainlineFont->DrawText(cStatTextX - tWidth, cStatYCur - 4.0f, 0, 1.0f, tBuffer);

        //--Move cursor.
        cStatYCur += cStatYSpc;
    }

    //--[Current Equipment]
    //--Resolve cursor.
    int tTopRenderCursor = mCurrentCursor;
    if(mIsModifying) tTopRenderCursor = mModifySlot;

    //--Constants.
    float cHeaderY = 100.0f;
    float cEquipYTop = 140.0f;
    float cEquipYSpc = 25.0f;
    float cIconX = 357.0f;
    float cItemNameX = cIconX + 23.0f + 3.0f;
    float cGemsX = 688.0f;

    //--Storage.
    AdventureItem *rDescriptionItem = NULL;

    //--Headers.
    Images.EquipmentUI.rHeadingFont->DrawText(cIconX, cHeaderY, 0, 1.0f, "Name");
    Images.EquipmentUI.rHeadingFont->DrawText(cGemsX, cHeaderY, 0, 1.0f, "Gems");

    //--Subdivision line. Splits description from item list.
    SugarBitmap::DrawRectFill(334.0f, 298.0f, 334.0f + 698.0f, 300.0f, StarlightColor::MapRGBAF(0.6f, 0.6f, 0.6f, 1.0f));

    //--Render the six equipment items for this character. Some slots may be empty.
    /*
    for(int i = 0; i < ACE_EQUIP_TOTAL; i ++)
    {
        //--Compute position.
        float tCurrentY = cEquipYTop + (i * cEquipYSpc);

        //--Get the equipment in the slot. Render it if it exists.
        AdventureItem *rCheckItem = rEquipEntity->GetEquipmentBySlot(i);
        if(rCheckItem)
        {
            //--Get the item's icon.
            SugarBitmap *rItemIcon = rCheckItem->GetIconImage();
            if(rItemIcon) rItemIcon->Draw(cIconX, tCurrentY);

            //--If selected, render the item's name in red as a cursor.
            if(i == tTopRenderCursor)
            {
                //--Render the equipment description.
                RenderEquipmentDescription(rCheckItem);
                rDescriptionItem = rCheckItem;

                //--Set color.
                StarlightColor::SetMixer(1.0f, 0.0f, 0.0f, 1.0f);
            }

            //--Name of the item.
            Images.EquipmentUI.rMainlineFont->DrawText(cItemNameX, tCurrentY + 1.0f, 0, 1.0f, rCheckItem->GetName());
            StarlightColor::ClearMixer();

            //--Render gem states. Borrows the inventory's UI parts.
            int tGemSlots = rCheckItem->GetGemSlots();
            if(tGemSlots > 0)
            {
                //--Render each gem indicator from the same sheet.
                for(int p = 0; p < tGemSlots; p ++)
                {
                    //--Render the base.
                    Images.VendorUI.rGemEmpty22px->Draw(cGemsX + (22.0f * p), tCurrentY);

                    //--Render the gem, i fit exists.
                    AdventureItem *rGemInSlot = rCheckItem->GetGemInSlot(p);
                    if(rGemInSlot)
                    {
                        SugarBitmap *rGemIcon = rGemInSlot->GetIconImage();
                        if(rGemIcon) rGemIcon->Draw(cGemsX + (22.0f * p), tCurrentY);
                    }
                }
            }
        }
        //--Item does not exist. Render a greyed-out version of the item's slot name.
        else
        {
            //--Buffer.
            char tBuffer[32];
            if(i == ACE_EQUIP_WEAPON)
            {
                strcpy(tBuffer, "(Weapon)");
            }
            else if(i == ACE_EQUIP_ARMOR)
            {
                strcpy(tBuffer, "(Armor)");
            }
            else if(i == ACE_EQUIP_ACCESSORYA || i == ACE_EQUIP_ACCESSORYB)
            {
                strcpy(tBuffer, "(Accessory)");
            }
            else if(i == ACE_EQUIP_ITEMA || i == ACE_EQUIP_ITEMB)
            {
                strcpy(tBuffer, "(Item)");
            }
            else
            {
                strcpy(tBuffer, "(Unhandled)");
            }

            //--If selected, render the item's name in red as a cursor.
            if(i == tTopRenderCursor)
            {
                StarlightColor::SetMixer(1.0f, 0.0f, 0.0f, 1.0f);
            }
            //--Otherwise, grey it out.
            else
            {
                StarlightColor::SetMixer(0.5f, 0.5f, 0.5f, 1.0f);
            }
            Images.EquipmentUI.rMainlineFont->DrawText(cItemNameX, tCurrentY + 1.0f, 0, 1.0f, tBuffer);
            StarlightColor::ClearMixer();
        }
    }*/

    //--[Alternates]
    //--Setup
    float cReplaceHeaderY = 449.0f;
    float cReplaceIconX = 54.0f;
    float cReplaceNameX = cReplaceIconX + 23.0f;
    float cReplaceGemsX = cReplaceIconX + 355.0f;

    //--Headers
    Images.EquipmentUI.rHeadingFont->DrawText(cReplaceIconX, cReplaceHeaderY, 0, 1.0f, "Available");
    Images.EquipmentUI.rHeadingFont->DrawText(cReplaceGemsX, cReplaceHeaderY, 0, 1.0f, "Gems");

    //--Positions.
    float tCurrentY = cReplaceHeaderY + 35.0f;
    float cReplaceSpaceY = 25.0f;

    //--Render all the items that can replace the item highlighted.
    AdventureItem *rComparisonItem = NULL;
    int tItemSlot = 0;
    int tRendersLeft = AMEQ_MAX_ITEMS_PER_PAGE;
    AdventureItem *rItem = (AdventureItem *)mValidEquipsList->PushIterator();
    while(rItem)
    {
        //--Don't render items under the minimum.
        if(tItemSlot < mEquipMinimumRender)
        {
            tItemSlot ++;
            rItem = (AdventureItem *)mValidEquipsList->AutoIterate();
            continue;
        }

        //--Check if this item is selected.
        bool tIsSelected = false;
        if(tItemSlot == mCurrentCursor && mIsModifying)
        {
            if(mModifySlot >= 0 && mModifySlot < 10)
            {
                //--Mark selection cursor so it renders red.
                tIsSelected = true;

                //--If the item is the unequip case:
                if(rItem == AdventureInventory::xrDummyUnequipItem)
                {
                }
                //--If the item was the gem-change case:
                else if(rItem == AdventureInventory::xrDummyGemsItem)
                {
                }
                //--All other items, mark for selection.
                else
                {
                    rComparisonItem = rItem;
                }
            }
        }

        //--Very special: This is the Unequip option.
        if(rItem == AdventureInventory::xrDummyUnequipItem)
        {
            //--System Unequip icon.
            Images.EquipmentUI.rUnequipIcon->Draw(cReplaceIconX, tCurrentY);

            //--Constants string.
            if(tIsSelected) StarlightColor::SetMixer(1.0f, 0.0f, 0.0f, 1.0f);
            Images.EquipmentUI.rMainlineFont->DrawText(cReplaceNameX, tCurrentY + 0.0f, 0, 1.0f, "Unequip");
        }
        //--Mostly special: This is the change gems options.
        else if(rItem == AdventureInventory::xrDummyGemsItem)
        {
            //--System Unequip icon.
            Images.EquipmentUI.rUnequipIcon->Draw(cReplaceIconX, tCurrentY);

            //--Constants string.
            if(tIsSelected) StarlightColor::SetMixer(1.0f, 0.0f, 0.0f, 1.0f);
            Images.EquipmentUI.rMainlineFont->DrawText(cReplaceNameX, tCurrentY + 0.0f, 0, 1.0f, "Socket Gems");
        }
        //--Normal case. Render the item's icon/name.
        else
        {
            //--Icon.
            SugarBitmap *rItemIcon = rItem->GetIconImage();
            if(rItemIcon) rItemIcon->Draw(cReplaceIconX, tCurrentY);

            //--Name.
            if(tIsSelected) StarlightColor::SetMixer(1.0f, 0.0f, 0.0f, 1.0f);
            Images.EquipmentUI.rMainlineFont->DrawText(cReplaceNameX, tCurrentY + 0.0f, 0, 1.0f, rItem->GetName());

            //--Gem indicators.
            for(int i = 0; i < rItem->GetGemSlots(); i ++)
            {
                Images.VendorUI.rGemEmpty22px->Draw(411.0f + (22.0f * i), tCurrentY);
            }
        }
        StarlightColor::ClearMixer();

        //--Stop rendering after 8 options have been rendered.
        tRendersLeft --;
        if(tRendersLeft < 1)
        {
            mValidEquipsList->PopIterator();
            break;
        }

        //--Move rendering downwards.
        tCurrentY = tCurrentY + cReplaceSpaceY;

        //--Next.
        tItemSlot ++;
        rItem = (AdventureItem *)mValidEquipsList->AutoIterate();
    }

    //--Scrollbar. Only renders if there are enough gems on the list to warrant it.
    if(mValidEquipsList->GetListSize() > AMEQ_MAX_ITEMS_PER_PAGE)
    {
        //--Binding.
        Images.EquipmentUI.rScrollbarFront->Bind();

        //--Scrollbar front. Stretches dynamically.
        float cScrollTop = 463.0f;
        float cScrollHei = 259.0f;

        //--Determine what percentage of the gem list is presently represented.
        int tMaxOffset = mValidEquipsList->GetListSize();
        float cPctTop = (mGemMinimumRender                          ) / (float)tMaxOffset;
        float cPctBot = (mGemMinimumRender + AMEQ_MAX_ITEMS_PER_PAGE) / (float)tMaxOffset;

        //--Positions and Constants.
        float cLft = 1034.0f;
        float cRgt = cLft + Images.InventoryUI.rScrollbarFront->GetWidth();
        float cEdg = 6.0f / (float)Images.InventoryUI.rScrollbarFront->GetHeight();

        //--Compute where the bar should be.
        float cTopTop = cScrollTop + (cPctTop * cScrollHei);
        float cTopBot = cTopTop + 6.0f;
        float cBotBot = cScrollTop + (cPctBot * cScrollHei);
        float cBotTop = cBotBot - 6.0f;

        glBegin(GL_QUADS);
            //--Render the top of the bar.
            glTexCoord2f(0.0f, 0.0f); glVertex2f(cLft, cTopTop);
            glTexCoord2f(1.0f, 0.0f); glVertex2f(cRgt, cTopTop);
            glTexCoord2f(1.0f, cEdg); glVertex2f(cRgt, cTopBot);
            glTexCoord2f(0.0f, cEdg); glVertex2f(cLft, cTopBot);

            //--Render the bottom of the bar.
            glTexCoord2f(0.0f, 1.0f - cEdg); glVertex2f(cLft, cBotTop);
            glTexCoord2f(1.0f, 1.0f - cEdg); glVertex2f(cRgt, cBotTop);
            glTexCoord2f(1.0f, 1.0f);        glVertex2f(cRgt, cBotBot);
            glTexCoord2f(0.0f, 1.0f);        glVertex2f(cLft, cBotBot);

            //--Render the middle of the bar.
            glTexCoord2f(0.0f, cEdg);        glVertex2f(cLft, cTopBot);
            glTexCoord2f(1.0f, cEdg);        glVertex2f(cRgt, cTopBot);
            glTexCoord2f(1.0f, 1.0f - cEdg); glVertex2f(cRgt, cBotTop);
            glTexCoord2f(0.0f, 1.0f - cEdg); glVertex2f(cLft, cBotTop);
        glEnd();

        //--Fixed backing, which actually goes in front of the scrollbar.
        Images.EquipmentUI.rFrameScroll->Draw();
    }

    //--[Equipment Comparison]
    //--After we've resolved the comparison items, render their properties. If the comparison item is not selected,
    //  then the base properties of the currently selected item render.
    RenderEquipmentComparison(rDescriptionItem, rComparisonItem);

    //--[Socket Mode]
    //--In Socket Mode, render that over everything else.
    if(mIsSocketEditingMode || mSocketEditTimer > 0) { RenderSocketMode(); return; }
}

//--[Forward Declarations]
#include "Subdivide.h"
void RenderAndAdvance(float pLft, float &sYCursor, SugarFont *pFont, const char *pFormat, ...);

//--Functions
void AdventureMenu::RenderEquipmentDescription(AdventureItem *pDescriptionItem)
{
    //--[Documentation and Setup]
    //--Renders information about the piece of equipment in question. This is made for the Equipment menu.
    if(!Images.mIsReady) return;
    if(!pDescriptionItem) return;

    //--Render only the description. Properties are handled elsewhere.
    float cXPosition = 353.0f;
    float cWidth = 669.0f;

    //--Variables.
    float tYPosition = 303.0f;

    //--Lines auto-parse out to meet the length of the description box.
    int tCharsParsed = 0;
    int tCharsParsedTotal = 0;
    const char *rDescription = pDescriptionItem->GetDescription();
    if(rDescription)
    {
        //--Setup.
        int tStringLen = (int)strlen(rDescription);

        //--Loop until the whole description has been parsed out.
        while(tCharsParsedTotal < tStringLen)
        {
            //--Run the subdivide.
            char *tDescriptionLine = Subdivide::SubdivideString(tCharsParsed, &rDescription[tCharsParsedTotal], -1, cWidth, Images.EquipmentUI.rMainlineFont, 1.0f);
            RenderAndAdvance(cXPosition, tYPosition, Images.EquipmentUI.rMainlineFont, tDescriptionLine);

            //--Move to the next line.
            tCharsParsedTotal += tCharsParsed;

            //--Clean up.
            free(tDescriptionLine);
        }
    }
}
void AdventureMenu::RenderEquipmentComparison(AdventureItem *pDescriptionItem, AdventureItem *pComparisonItem)
{
    //--[Documentation and Setup]
    //--Renders a comparison listing between the original item and the selected one. If the comparison item does not
    //  exist, only the description item's information renders.
    if(!Images.mIsReady) return;

    //--Constants
    float cBaseValueX = 1160.0f;
    float cGemValueX = cBaseValueX + 45.0f;
    float cArrowX = cGemValueX + 45.0f;
    float cCompareValueX = cArrowX + 45.0f;

    //--[Header]
    Images.EquipmentUI.rHeadingFont->DrawText(1211,           65.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Properties");
    Images.StatusUI.rVerySmallFont->DrawText(cBaseValueX,    105.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Base");
    Images.StatusUI.rVerySmallFont->DrawText(cGemValueX,     105.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "Gems");
    if(pComparisonItem) Images.StatusUI.rVerySmallFont->DrawText(cCompareValueX, 105.0f, SUGARFONT_AUTOCENTER_X, 1.0f, "New");

    //--[Icons]
    //--Render the statistic icons on the right side of the screen. Also store their Y positions.
    float cImgHeadersStartY = 130.0f;
    float cImgHeadersSpaceY = 20.0f;
    float cImgHeadersX = 1107.0f;
    float cImgHeadersY[AM_INV_HEADER_TOTAL];
    for(int i = 0; i < AM_INV_HEADER_TOTAL; i ++)
    {
        cImgHeadersY[i] = cImgHeadersStartY + (i * cImgHeadersSpaceY);
        Images.InventoryUI.rImageHeaders[i]->Draw(cImgHeadersX, cImgHeadersStartY + (i * cImgHeadersSpaceY));
    }

    //--[No Comparison]
    //--Just render the statistics of the provided item.
    if(pDescriptionItem && !pComparisonItem)
    {
        //--Iterate.
        for(int i = 0; i < AM_INV_HEADER_TOTAL; i ++)
        {
            //--Render the bonus.
            int cBonus = pDescriptionItem->GetPropertyByInventoryHeaders(i, true);
            int cGemBonus = pDescriptionItem->GetGemBoostPropertyByInventoryHeaders(i);
            if(cBonus != 0 || cGemBonus != 0)
            {
                Images.StatusUI.rVerySmallFont->DrawTextArgs(cBaseValueX, cImgHeadersY[i], SUGARFONT_AUTOCENTER_X, 1.0f, "%i", cBonus);
            }

            //--If the gem bonus is nonzero, render it.
            if(cGemBonus != 0)
            {
                Images.StatusUI.rVerySmallFont->DrawTextArgs(cGemValueX,  cImgHeadersY[i], SUGARFONT_AUTOCENTER_X, 1.0f, "(%i)", cGemBonus);
            }
        }
    }
    //--[No Description]
    //--The description item is unequipped. All its stats are zeroes.
    else if(!pDescriptionItem && pComparisonItem)
    {
        //--Iterate.
        for(int i = 0; i < AM_INV_HEADER_TOTAL; i ++)
        {
            //--Get properties from both. Description item is always zero.
            int cBonusDesc = 0;
            int cBonusComp = pComparisonItem->GetPropertyByInventoryHeaders(i, true);

            //--If both are zero, don't render them.
            if(cBonusComp == 0 && cBonusDesc == 0) continue;

            //--Render the bonus, even if it's zero.
            Images.StatusUI.rVerySmallFont->DrawTextArgs(cBaseValueX, cImgHeadersY[i], SUGARFONT_AUTOCENTER_X, 1.0f, "%i", cBonusDesc);

            //--Render an arrow.
            Images.StatusUI.rVerySmallFont->DrawTextArgs(cArrowX, cImgHeadersY[i], 0, 1.0f, "->");

            //--Render the comparison value. Select color based on whether it's higher or lower.
            if(cBonusComp > cBonusDesc)
            {
                StarlightColor::SetMixer(0.0f, 1.0f, 0.0f, 1.0f);
            }
            else if(cBonusComp == cBonusDesc)
            {
                StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, 1.0f);
            }
            else
            {
                StarlightColor::SetMixer(1.0f, 0.0f, 0.0f, 1.0f);
            }
            Images.StatusUI.rVerySmallFont->DrawTextArgs(cCompareValueX, cImgHeadersY[i], SUGARFONT_AUTOCENTER_X, 1.0f, "%i", cBonusComp);
            StarlightColor::ClearMixer();
        }
    }
    //--[Comparison]
    //--Render the statistics of the provided item, modified to show how the comparison item is. Stat improvements
    //  are listed in green, neutrals are white, otherwise red.
    else if(pDescriptionItem && pComparisonItem)
    {
        //--Iterate.
        for(int i = 0; i < AM_INV_HEADER_TOTAL; i ++)
        {
            //--Get properties from both:
            int cBonusDesc = pDescriptionItem->GetPropertyByInventoryHeaders(i, true);
            int cGemBonus  = pDescriptionItem->GetGemBoostPropertyByInventoryHeaders(i);
            int cBonusComp = pComparisonItem->GetPropertyByInventoryHeaders(i, true);

            //--If all are zero, don't render them.
            if(cBonusComp == 0 && cBonusDesc == 0 && cGemBonus == 0) continue;

            //--Render the bonus, even if it's zero.
            Images.StatusUI.rVerySmallFont->DrawTextArgs(cBaseValueX, cImgHeadersY[i], SUGARFONT_AUTOCENTER_X, 1.0f, "%i", cBonusDesc);
            if(cGemBonus != 0) Images.StatusUI.rVerySmallFont->DrawTextArgs(cGemValueX,  cImgHeadersY[i], SUGARFONT_AUTOCENTER_X, 1.0f, "(%i)", cGemBonus);

            //--Render an arrow.
            Images.StatusUI.rVerySmallFont->DrawTextArgs(cArrowX, cImgHeadersY[i], 0, 1.0f, "->");

            //--Render the comparison value. Select color based on whether it's higher or lower.
            if(cBonusComp > cBonusDesc)
            {
                StarlightColor::SetMixer(0.0f, 1.0f, 0.0f, 1.0f);
            }
            else if(cBonusComp == cBonusDesc)
            {
                StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, 1.0f);
            }
            else
            {
                StarlightColor::SetMixer(1.0f, 0.0f, 0.0f, 1.0f);
            }
            Images.StatusUI.rVerySmallFont->DrawTextArgs(cCompareValueX, cImgHeadersY[i], SUGARFONT_AUTOCENTER_X, 1.0f, "%i", cBonusComp);
            StarlightColor::ClearMixer();
        }
    }
}
