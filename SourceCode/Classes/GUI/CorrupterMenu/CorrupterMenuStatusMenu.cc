//--Base
#include "CorrupterMenu.h"

//--Classes
#include "Actor.h"
#include "PerkPack.h"
#include "VisualLevel.h"

//--CoreClasses
#include "DataList.h"
#include "SugarBitmap.h"
#include "SugarFont.h"
#include "SugarLinkedList.h"

//--Definitions
#include "HitDetection.h"
#include "Subdivide.h"

//--GUI
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "DisplayManager.h"
#include "EntityManager.h"

//=========================================== System ==============================================
//====================================== Property Queries =========================================
bool CorrupterMenu::IsStatusMode()
{
    return (mCurrentMode == CM_MODE_STATUS);
}

//========================================= Manipulators ==========================================
void CorrupterMenu::SetToStatusMode()
{
    //--Flags.
    mIsUnlockingPerk = false;
    mCurrentMode = CM_MODE_STATUS;
    FillStatusDescription(NULL);
}

//========================================= Core Methods ==========================================
void CorrupterMenu::FillStatusDescription(PerkPack *pPerkPack)
{
    //--Sets the description lines on the perk confirmation window. Clears the lines if NULL is passed.
    memset(mPerkUnlockLines, 0, sizeof(char) * CM_PERK_UNLOCK_LINES * CM_PERK_UNLOCK_CHARS);
    if(!pPerkPack) return;

    //--Parse along the status description lines.
    const char *rDescription = pPerkPack->GetDescription();
    if(!Images.Data.rUIFont || !rDescription) return;

    //--Specify how wide the window is.
    float cLength = mConfirmBody.GetWidth() - 15.0f;

    //--Setup.
    int tDescriptionLen = (int)strlen(rDescription);
    int tCursor = 0;
    int tRunningCursor = 0;
    int tCurrentLine = 0;

    //--Cut up.
    while(tRunningCursor < tDescriptionLen)
    {
        //--Get the string.
        char *tString = Subdivide::SubdivideString(tCursor, &rDescription[tRunningCursor], CM_PERK_UNLOCK_CHARS-1, cLength, Images.Data.rUIFont, 1.0f);

        //--Copy the line over.
        strcpy(mPerkUnlockLines[tCurrentLine], tString);

        //--Advance the cursor so the next line gets put up.
        tRunningCursor += tCursor;
        if(tCurrentLine < CM_PERK_UNLOCK_LINES - 1) tCurrentLine ++;

        //--Clean.
        free(tString);
    }
}

//===================================== Private Core Methods ======================================
//============================================ Update =============================================
void CorrupterMenu::UpdateStatusMode()
{
    //--Setup.
    ControlManager *rControlManager = ControlManager::Fetch();
    int tMouseX, tMouseY, tMouseZ;
    rControlManager->GetMouseCoords(tMouseX, tMouseY, tMouseZ);

    //--Under normal circumstances, we're handling perk selection.
    if(!mIsUnlockingPerk)
    {
        //--Old hightlight.
        int tOldHighlight = mPerkHighlightedBtn;

        //--Out of range, ignore.
        if(tMouseY < (cPerkStartY + CM_BTN_TEXT_SIZE) || tMouseX < cPerkUnlockStartX || tMouseX >= cPerkUnlockStartX + cPerkBtnWid)
        {
            mPerkHighlightedBtn = -1;
        }
        //--Resolve which perk button is highlighted.
        else
        {
            mPerkHighlightedBtn = (tMouseY - (cPerkStartY + CM_BTN_TEXT_SIZE)) / cPerkBtnHei;
        }

        //--Change case.
        if(mPerkHighlightedBtn != tOldHighlight && mPerkHighlightedBtn != -1 && mPerkHighlightedBtn < PerkPack::xPerkList->GetListSize())
            AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");

        //--Left-click, if a perk was highlighted then bring up the unlock screen.
        if(rControlManager->IsFirstPress("MouseLft"))
        {
            //--Pack has to exist.
            PerkPack *rPerkPack = (PerkPack *)PerkPack::xPerkList->GetElementBySlot(mPerkHighlightedBtn);
            if(rPerkPack)
            {
                //--Flag.
                mIsUnlockingPerk = true;

                //--Subroutine will fill description lines.
                FillStatusDescription(rPerkPack);

                //--SFX.
                AudioManager::Fetch()->PlaySound("Menu|Select");
            }
        }

        //--Right-click, attempt to unlock the perk immediately.
        if(rControlManager->IsFirstPress("MouseRgt"))
        {
            //--Pack has to exist.
            PerkPack *rPerkPack = (PerkPack *)PerkPack::xPerkList->GetElementBySlot(mPerkHighlightedBtn);
            if(rPerkPack && rPerkPack->GetCost() <= xPlayerMP && rPerkPack->GetUnlockCount() < rPerkPack->GetUnlocksMax())
            {
                //--Unlock, set flags.
                rPerkPack->Unlock();

                //--Subtract MP.
                xPlayerMP = xPlayerMP - rPerkPack->GetCost();

                //--SFX.
                AudioManager::Fetch()->PlaySound("Menu|Select");
            }
        }
    }
    //--We're on a heads-up screen informing us about the perk we're attempting to unlock.
    else
    {
        //--Must be a left-click.
        if(rControlManager->IsFirstPress("MouseLft"))
        {
            //--Check the reference perk. It must exist.
            PerkPack *rPerkPack = (PerkPack *)PerkPack::xPerkList->GetElementBySlot(mPerkHighlightedBtn);

            //--Is over the yes button?
            if(IsPointWithin2DReal(tMouseX, tMouseY, mConfirmYesBtn) && rPerkPack && rPerkPack->GetCost() <= xPlayerMP)
            {
                //--Unlock, set flags.
                mIsUnlockingPerk = false;
                rPerkPack->Unlock();

                //--Subtract MP.
                xPlayerMP = xPlayerMP - rPerkPack->GetCost();

                //--SFX.
                AudioManager::Fetch()->PlaySound("Menu|Select");
            }
            //--Cancel.
            else if(IsPointWithin2DReal(tMouseX, tMouseY, mConfirmCancelBtn))
            {
                //--Flag.
                mIsUnlockingPerk = false;

                //--SFX.
                AudioManager::Fetch()->PlaySound("Menu|Select");
            }
        }
    }
}

//=========================================== File I/O ============================================
//=========================================== Drawing =============================================
void CorrupterMenu::RenderStatusMode()
{
    //--[Documentation]
    //--Renders the player's current form and perks. Also allows selection of perks.
    if(!Images.mIsReady) return;

    //--[Player Data]
    //--Get the player's information. If the player cannot be located, fail.
    Actor *rPlayer = EntityManager::Fetch()->GetLastPlayerEntity();
    if(!rPlayer) return;

    //--Get their last portrait.
    char *rTrueFormPath = (char *)rPlayer->GetDataList()->FetchDataEntry("sTrueFormPath");
    SugarBitmap *rRenderPortrait = (SugarBitmap *)DataLibrary::Fetch()->GetEntry(rTrueFormPath);
    if(rRenderPortrait)
    {
        //--Render.
        rRenderPortrait->Draw(CM_HEADER_INDENT, mHeaderMinionsBtn.mBot + CM_HEADER_INDENT);

        //--Render text, should be centered.
        float tXRender = CM_HEADER_INDENT + (rRenderPortrait->GetTrueWidth() * 0.5f) - (Images.Data.rUIFont->GetTextWidth("Your true form") * 0.5f);
        Images.Data.rUIFont->DrawText(tXRender, mHeaderMinionsBtn.mBot + CM_HEADER_INDENT + rRenderPortrait->GetTrueHeight(), 0, 1.0f, "Your true form");
    }

    //--HP/MP and combat statistics.
    const CombatStats cStats = rPlayer->GetCombatStatistics();
    float tXPos = mHeaderSummonBtn.mRgt;
    float tYCursor = mHeaderStatusBtn.mBot + CM_BTN_INDENT;
    Images.Data.rUIFont->DrawTextArgs(tXPos, tYCursor + (CM_BTN_TEXT_SIZE * 0.0f), 0, 1.0f, "Statistics:");
    Images.Data.rUIFont->DrawTextArgs(tXPos, tYCursor + (CM_BTN_TEXT_SIZE * 1.0f), 0, 1.0f, "HP: %i/%i", cStats.mHP, cStats.mHPMax);
    Images.Data.rUIFont->DrawTextArgs(tXPos, tYCursor + (CM_BTN_TEXT_SIZE * 2.0f), 0, 1.0f, "WP: %i/%i", cStats.mWillPower, cStats.mWillPowerMax);
    Images.Data.rUIFont->DrawTextArgs(tXPos, tYCursor + (CM_BTN_TEXT_SIZE * 3.0f), 0, 1.0f, "Attack: %i", cStats.mAttackPower);
    Images.Data.rUIFont->DrawTextArgs(tXPos, tYCursor + (CM_BTN_TEXT_SIZE * 4.0f), 0, 1.0f, "Accuracy: %i", cStats.mAccuracy);
    Images.Data.rUIFont->DrawTextArgs(tXPos, tYCursor + (CM_BTN_TEXT_SIZE * 5.0f), 0, 1.0f, "Defense: %i", cStats.mDefensePower);

    //--[Perks]
    //--Setup.
    const float cBtnIndent = CM_BTN_INDENT / 2.0f;
    tYCursor = cPerkStartY;

    //--Header.
    Images.Data.rUIFont->DrawTextArgs(cPerkStartX, tYCursor, 0, 1.0f, "Perk Listing:");
    tYCursor = tYCursor + CM_BTN_TEXT_SIZE;

    //--Render each perk.
    int i = 0;
    //TwoDimensionReal tButtonDim;
    PerkPack *rPerk = (PerkPack *)PerkPack::xPerkList->PushIterator();
    while(rPerk)
    {
        //--Render a button to unlock the perk.
        /*
        if(rPerk->GetUnlockCount() < rPerk->GetUnlocksMax())
        {
            //--Set the position.
            tButtonDim.SetWH(cPerkUnlockStartX, tYCursor, cPerkBtnWid, cPerkBtnHei);

            //--If we can afford it, it says "Unlock".
            if(rPerk->GetCost() <= xPlayerMP)
            {
                VisualLevel::RenderTextButton(Images.Data.rBorderCard, Images.Data.rUIFont, tButtonDim, cBtnIndent * 2.0f, "Unlock");
            }
            //--Too expensive, show "View" instead.
            else
            {
                VisualLevel::RenderTextButton(Images.Data.rBorderCard, Images.Data.rUIFont, tButtonDim, cBtnIndent * 2.0f, "View");
            }
        }*/

        //--Coloring.
        if(i == mPerkHighlightedBtn)
        {
            mHighlightCol.SetAsMixer();
        }
        //--Normal white text.
        else
        {
            mNormalTextCol.SetAsMixer();
        }

        //--Render the name of the perk and how many times it has been unlocked.
        if(rPerk->GetUnlocksMax() == 1)
            Images.Data.rUIFont->DrawTextArgs(cPerkStartX, tYCursor + (cBtnIndent*2.0f), 0, 1.0f, "%s (%i)", rPerk->GetName(), rPerk->GetUnlockCount());
        else
            Images.Data.rUIFont->DrawTextArgs(cPerkStartX, tYCursor + (cBtnIndent*2.0f), 0, 1.0f, "%s (%i/%i)", rPerk->GetName(), rPerk->GetUnlockCount(), rPerk->GetUnlocksMax());

        //--Next.
        i ++;
        tYCursor = tYCursor + cPerkBtnHei;
        rPerk = (PerkPack *)PerkPack::xPerkList->AutoIterate();
    }

    //--Clean.
    mNormalTextCol.SetAsMixer();

    //--[Perk Unlock Box]
    //--Box that appears when unlocking a perk. Shows its cost, description, and buttons to accept or cancel. Uses the same dimensions
    //  as the summoning dialog box.
    if(mIsUnlockingPerk)
    {
        //--Make sure the perk exists.
        PerkPack *rPerkPack = (PerkPack *)PerkPack::xPerkList->GetElementBySlot(mPerkHighlightedBtn);
        if(!rPerkPack) return;

        //--Too expensive!
        if(rPerkPack->GetCost() > xPlayerMP) glColor3f(1.0f, 0.5f, 0.5f);

        //--Base card and buttons.
        VisualLevel::RenderBorderCardOver(Images.Data.rBorderCard, mConfirmBody.mLft, mConfirmBody.mTop, mConfirmBody.mRgt, mConfirmBody.mBot, 0x01FF);

        //--Normal unlocking case.
        if(rPerkPack->GetCost() <= xPlayerMP && rPerkPack->GetUnlockCount() < rPerkPack->GetUnlocksMax())
        {
            VisualLevel::RenderTextButton(Images.Data.rBorderCard, Images.Data.rUIFont, mConfirmCancelBtn, CM_BTN_INDENT, "Cancel");
            VisualLevel::RenderTextButton(Images.Data.rBorderCard, Images.Data.rUIFont, mConfirmYesBtn, CM_BTN_INDENT, "Confirm");
        }
        //--Cancel button renders red if the player doesn't have the MP to unlock. Confirm button doesn't render.
        else
        {
            glColor3f(1.0f, 0.5f, 0.5f);
            VisualLevel::RenderTextButton(Images.Data.rBorderCard, Images.Data.rUIFont, mConfirmCancelBtn, CM_BTN_INDENT, "Cancel");
        }

        //--Name and cost.
        if(rPerkPack->GetCost() > xPlayerMP) glColor3f(1.0f, 0.5f, 0.5f);
        Images.Data.rUIFont->DrawTextArgs(mConfirmBody.mLft + CM_BTN_INDENT, mConfirmBody.mTop + CM_BTN_INDENT, 0, 1.5f, "%s (Cost: %i)", rPerkPack->GetName(), rPerkPack->GetCost());

        //--Description lines.
        for(int i = 0; i < CM_PERK_UNLOCK_LINES; i ++)
        {
            Images.Data.rUIFont->DrawText(mConfirmBody.mLft + CM_BTN_INDENT, mConfirmBody.mTop + (CM_BTN_INDENT * 2.5f) + (CM_BTN_TEXT_SIZE * (i + 1)), 0, 1.0f, mPerkUnlockLines[i]);
        }
    }
}

//======================================= Pointer Routing =========================================
//====================================== Static Functions =========================================
//========================================= Lua Hooking ===========================================
//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
