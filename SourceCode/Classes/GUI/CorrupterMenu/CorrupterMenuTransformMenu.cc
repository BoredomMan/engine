//--Base
#include "CorrupterMenu.h"

//--Classes
#include "TransformPack.h"
#include "VisualLevel.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarFont.h"
#include "SugarLinkedList.h"

//--Definitions
#include "HitDetection.h"
#include "Subdivide.h"

//--GUI
//--Libraries
//--Managers
#include "AudioManager.h"
#include "ControlManager.h"

//--The Transform Menu allows the player to modify which form their transformation ability routes to.
//  Most of the forms are available from the word go, but a few require an object to be placed in the world first.
//  Switching transformation targets costs no time and no MP.

//--[Property Queries]
bool CorrupterMenu::IsTransformMode()
{
    return mCurrentMode == CM_MODE_TRANSFORM;
}

//--[Manipulators]
void CorrupterMenu::SetToTransformMode()
{
    //--Flags.
    mCurrentMode = CM_MODE_TRANSFORM;
    mHighlightedTransformIndex = -1;
}

//--[Core Methods]
void CorrupterMenu::FillTransformDescription(TransformPack *pTransformationPack)
{
    //--Given a TransformPack, populates description info. Borrows the perk description lines since they can never be
    //  used a the same time by two different tabs.
    //--Pass NULL to clear the description lines.
    memset(mPerkUnlockLines, 0, sizeof(char) * CM_PERK_UNLOCK_LINES * CM_PERK_UNLOCK_CHARS);
    if(!pTransformationPack) return;

    //--Parse along the status description lines.
    const char *rDescription = pTransformationPack->GetDescription();
    if(!Images.Data.rUIFont || !rDescription) return;

    //--Specify how wide the window is.
    float cLength = cDescriptionListingWid;

    //--Setup.
    int tDescriptionLen = (int)strlen(rDescription);
    int tCursor = 0;
    int tRunningCursor = 0;
    int tCurrentLine = 0;

    //--Cut up.
    while(tRunningCursor < tDescriptionLen)
    {
        //--Get the string.
        char *tString = Subdivide::SubdivideString(tCursor, &rDescription[tRunningCursor], CM_PERK_UNLOCK_CHARS-1, cLength, Images.Data.rUIFont, 1.0f);

        //--Copy the line over.
        strcpy(mPerkUnlockLines[tCurrentLine], tString);

        //--Advance the cursor so the next line gets put up.
        tRunningCursor += tCursor;
        if(tCurrentLine < CM_PERK_UNLOCK_LINES - 1) tCurrentLine ++;

        //--Clean.
        free(tString);
    }
}

//--[Update]
void CorrupterMenu::UpdateTransformMode()
{
    //--Update for Magic Mode. Cast spells!
    ControlManager *rControlManager = ControlManager::Fetch();
    int tMouseX, tMouseY, tMouseZ;
    rControlManager->GetMouseCoords(tMouseX, tMouseY, tMouseZ);

    //--Store the previous highlight.
    int tPreviousHighlight = mHighlightedTransformIndex;

    //--Left click.
    if(rControlManager->IsFirstPress("MouseLft"))
    {
        //--Highlight the magic spell that matches. The spell only changes if the mouse's X position
        //  is within range, as the player may want to move to the description area.
        if(tMouseX >= mTransformListingDim.mLft && tMouseX <= mTransformListingDim.mRgt)
        {
            mHighlightedTransformIndex = (tMouseY - (mTransformListingDim.mTop + mTransformListingDim.GetHeight())) / CM_BTN_TEXT_SIZE;
            if(tMouseY < (mTransformListingDim.mTop + mTransformListingDim.GetHeight())) mHighlightedTransformIndex = -1;
        }
        //--If the user clicks the "Activate" button, the transformation activates.
        else if(IsPointWithin2DReal(tMouseX, tMouseY, mTransformActivateBtn))
        {
            //--The associated transformation must exist.
            TransformPack *rTransformation = (TransformPack *)TransformPack::xTransformationList->GetElementBySlot(mHighlightedTransformIndex);
            if(rTransformation)
            {
                SetVisibility(false);
                rTransformation->Execute();
                AudioManager::Fetch()->PlaySound("Menu|Select");
            }
        }
    }

    //--Right-click will run the transformation immediately.
    if(rControlManager->IsFirstPress("MouseRgt") && tMouseX >= mTransformListingDim.mLft && tMouseX <= mTransformListingDim.mRgt)
    {
        //--Set the slot.
        mHighlightedTransformIndex = (tMouseY - (mTransformListingDim.mTop + mTransformListingDim.GetHeight())) / CM_BTN_TEXT_SIZE;
        if(tMouseY < (mTransformListingDim.mTop + mTransformListingDim.GetHeight())) mHighlightedTransformIndex = -1;

        //--Get the spell. Cast it if possible. Spells cast via this menu implicitly target the player.
        TransformPack *rTransformation = (TransformPack *)TransformPack::xTransformationList->GetElementBySlot(mHighlightedTransformIndex);
        if(rTransformation)
        {
            SetVisibility(false);
            rTransformation->Execute();
            AudioManager::Fetch()->PlaySound("Menu|Select");
        }
    }

    //--If the highlight changed, reset the description.
    if(tPreviousHighlight != mHighlightedTransformIndex && mHighlightedTransformIndex >= 0)
    {
        //--Description.
        TransformPack *rTransformPack = (TransformPack *)TransformPack::xTransformationList->GetElementBySlot(mHighlightedTransformIndex);
        FillTransformDescription(rTransformPack);

        //--SFX.
        AudioManager::Fetch()->PlaySound("Menu|ChangeHighlight");

        //--Place the Activate button.
        if(rTransformPack)
        {
            //--Figure out where the description ends.
            float tYCursor = mTransformDescriptionDim.mTop + (CM_BTN_TEXT_SIZE * 1.5f);
            for(int i = 0; i < CM_PERK_UNLOCK_LINES; i ++)
            {
                if(mPerkUnlockLines[i][0] == '\0') break;
                tYCursor = tYCursor + mTransformDescriptionDim.GetHeight();
            }

            //--Place the cast button slightly below that.
            tYCursor = tYCursor + (CM_BTN_TEXT_SIZE * 0.5f);
            mTransformActivateBtn.SetWH(mTransformDescriptionDim.mLft, tYCursor, Images.Data.rUIFont->GetTextWidth("Activate") + (CM_BTN_INDENT * 2.0f), CM_BTN_TEXT_SIZE + (CM_BTN_INDENT * 2.0f));
        }
        //--Hide the cast button.
        else
        {
            mTransformActivateBtn.SetWH(-100.0f, -100.0f, 1.0f, 1.0f);
        }
    }
}

//--[Drawing]
void CorrupterMenu::RenderTransformMode()
{
    //--Renders a list of the transformations available to the player and details about them. Allows the player
    //  to select transformations to switch to them.
    if(!Images.mIsReady) return;

    //--Sizing.
    float cIncrement = mTransformListingDim.GetHeight();

    //--Heading.
    float tYCursor = mTransformListingDim.mTop;
    Images.Data.rUIFont->DrawText(mTransformListingDim.mLft, tYCursor, 0, 1.0f, "Available Transformations");
    tYCursor = tYCursor + cIncrement;

    //--List of transformations.
    int i = 0;
    TransformPack *rTransformPack = (TransformPack *)TransformPack::xTransformationList->PushIterator();
    while(rTransformPack)
    {
        //--If this spell is selected, render a highlight.
        if(mHighlightedTransformIndex == i)
        {
            mHighlightCol.SetAsMixer();
        }
        //--Normal case.
        else
        {
            mNormalTextCol.SetAsMixer();
        }

        //--Render the name.
        Images.Data.rUIFont->DrawText(mTransformListingDim.mLft + CM_BTN_INDENT, tYCursor, 0, 1.0f, rTransformPack->GetName());

        //--Next.
        i ++;
        tYCursor = tYCursor + cIncrement;
        rTransformPack = (TransformPack *)TransformPack::xTransformationList->AutoIterate();
    }

    //--Clean.
    StarlightColor::ClearMixer();

    //--If no package is selected, show some instruction to the player.
    rTransformPack = (TransformPack *)TransformPack::xTransformationList->GetElementBySlot(mHighlightedTransformIndex);
    if(!rTransformPack)
    {
        Images.Data.rUIFont->DrawText(mTransformDescriptionDim.mLft, mTransformDescriptionDim.mTop + (cIncrement * 0.0f), 0, 1.0f, "Left-click a transformation to view its information.");
        Images.Data.rUIFont->DrawText(mTransformDescriptionDim.mLft, mTransformDescriptionDim.mTop + (cIncrement * 1.0f), 0, 1.0f, "Right-click a transformation to activate it.");
    }
    //--If a package is selected, render it.
    else
    {
        //--Render the associated image, if it exists.
        SugarBitmap *rRenderImg = rTransformPack->GetDisplayImage();
        if(rRenderImg)
        {
            //--Centering.
            float tRenderX = mTransformPortraitDim.mXCenter - (rRenderImg->GetWidth()  * 0.5f);
            float tRenderY = mTransformPortraitDim.mYCenter - (rRenderImg->GetHeight() * 0.5f);

            //--Render.
            rRenderImg->Draw(tRenderX, tRenderY);
        }

        //--Setup.
        float tYCursor = mTransformDescriptionDim.mTop;
        float cIncrement = mTransformDescriptionDim.GetHeight();

        //--Spell Information. Never changes formats.
        Images.Data.rUIFont->DrawTextArgs(mTransformDescriptionDim.mLft, tYCursor + (cIncrement * 0.0f), 0, 1.0f, "Transformation: %s", rTransformPack->GetName());
        tYCursor = tYCursor + (cIncrement * 1.5f);

        //--Render each description line. These should be set by FillSpellDescription().
        for(int i = 0; i < CM_PERK_UNLOCK_LINES; i ++)
        {
            Images.Data.rUIFont->DrawText(mTransformDescriptionDim.mLft, tYCursor, 0, 1.0f, mPerkUnlockLines[i]);
            tYCursor = tYCursor + cIncrement;
        }

        //--Render the button that lets the user activate this transformation.
        VisualLevel::RenderTextButton(Images.Data.rBorderCard, Images.Data.rUIFont, mTransformActivateBtn, CM_BTN_INDENT, "Activate");
    }
}
