//--[StringTyrantTitleStructures]
//--Structure packages used by the StringTyrantTitle class.

#pragma once

//--[Includes]
#include "Definitions.h"
#include "Structures.h"
#include "RootLevel.h"
#include "SugarFont.h"

//--Function Pointers
typedef void (*STTMenuClickHandler)(StringTyrantTitle *, int, bool, bool);

//--[STTMenuOptionPack]
//--Represents an option for the options menu. Usually an integer or a float, strings
//  are represented by integer lookups.
#define STT_OPTION_MAX_LINES 3
typedef struct STTMenuOptionPack
{
    //--Members
    int mType;
    FlexVal mValue;
    FlexVal mValueDefault;
    const char *rAssociatedString;

    //--Scrollbar Case
    bool mHasScrollbar;
    FlexVal mScrollbarLo;
    FlexVal mScrollbarHi;

    //--Optional Text
    char mOptionalText[STT_OPTION_MAX_LINES][128];

    //--Functions
    void Initialize()
    {
        //--Members
        mType = POINTER_TYPE_FAIL;
        mValue.i = 0;
        mValueDefault.i = 0;
        rAssociatedString = NULL;

        //--Scrollbar Case
        mHasScrollbar = false;
        mScrollbarLo.i = 0;
        mScrollbarHi.i = 1;

        //--Optional Text
        for(int i = 0; i < STT_OPTION_MAX_LINES; i ++)
        {
            memset(mOptionalText[i], 0, 128);
        }
    }
}STTMenuOptionPack;

//--[STTMenuPack]
//--A package representing a submenu on the title screen. Each menu contains a set of options and has
//  a handler call associated with it. Clicking the option executes the handler with that option's index
//  passed in, except options which use scrollbars which can use those to set their values.
typedef struct STTMenuPack
{
    //--Members
    bool mIsVisible;
    int mVisibilityTimer;
    int mEntries;
    char **mEntryText;
    STTMenuOptionPack *mOptionPacks;
    STTMenuClickHandler rMenuClickHandler;

    //--[System]
    void Initialize()
    {
        mIsVisible = false;
        mVisibilityTimer = 0;
        mEntries = 0;
        mEntryText = NULL;
        mOptionPacks = NULL;
        rMenuClickHandler = NULL;
    }

    //--[Property Queries]
    //--[Manipulators]
    void AllocateEntries(int pTotal)
    {
        //--Deallocate.
        for(int i = 0; i < mEntries; i ++)
        {
            free(mEntryText[i]);
        }

        //--Reset.
        mEntries = 0;
        free(mEntryText);
        free(mOptionPacks);
        mEntryText = NULL;
        mOptionPacks = NULL;
        if(pTotal < 1) return;

        //--Allocate.
        mEntries = pTotal;
        mEntryText = (char **)malloc(sizeof(char *) * mEntries);
        memset(mEntryText, 0, sizeof(char *) * mEntries);
        mOptionPacks = (STTMenuOptionPack *)malloc(sizeof(STTMenuOptionPack) * mEntries);
        for(int i = 0; i < mEntries; i ++)
        {
            mOptionPacks[i].Initialize();
        }
    }
    void SetClickHandler(STTMenuClickHandler pClickHandler)
    {
        rMenuClickHandler = pClickHandler;
    }
    void SetEntryText(int pSlot, const char *pText)
    {
        if(pSlot < 0 || pSlot >= mEntries) return;
        ResetString(mEntryText[pSlot], pText);
    }
    void SetOptionType(int pSlot, int pType)
    {
        if(pSlot < 0 || pSlot >= mEntries) return;
        mOptionPacks[pSlot].mType = pType;
    }
    void SetOptionValsI(int pSlot, int pVal, int pDefault)
    {
        if(pSlot < 0 || pSlot >= mEntries) return;
        mOptionPacks[pSlot].mValue.i = pVal;
        mOptionPacks[pSlot].mValueDefault.i = pDefault;
    }
    void SetOptionValsF(int pSlot, float pVal, float pDefault)
    {
        if(pSlot < 0 || pSlot >= mEntries) return;
        mOptionPacks[pSlot].mValue.f = pVal;
        mOptionPacks[pSlot].mValueDefault.f = pDefault;
    }
    void SetOptionText(int pSlot, int pLine, const char *pText)
    {
        if(pSlot < 0 || pSlot >= mEntries) return;
        if(pLine < 0 || pLine >= STT_OPTION_MAX_LINES) return;
        strncpy(mOptionPacks[pSlot].mOptionalText[pLine], pText, STD_MAX_LETTERS-1);
    }
    void SetOptionStringOverride(int pSlot, const char *pRef)
    {
        if(pSlot < 0 || pSlot >= mEntries) return;
        mOptionPacks[pSlot].rAssociatedString = pRef;
    }
    static void DeleteThis(void *pPtr)
    {
        STTMenuPack *rPtr = (STTMenuPack *)pPtr;
        for(int i = 0; i < rPtr->mEntries; i ++)
        {
            free(rPtr->mEntryText[i]);
        }
        free(rPtr->mEntryText);
        free(rPtr->mOptionPacks);
        free(rPtr);
    }
}STTMenuPack;
