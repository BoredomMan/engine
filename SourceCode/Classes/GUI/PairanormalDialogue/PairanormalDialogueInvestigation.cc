//--Base
#include "PairanormalDialogue.h"

//--Classes
#include "PairanormalDialogueCharacter.h"
#include "PairanormalLevel.h"
#include "PairanormalMenu.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarFont.h"
#include "SugarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "EasingFunctions.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "DisplayManager.h"
#include "LuaManager.h"

//--[Local Definitions]
#define MAX_HOVER_TICKS 25.0f
#define SWAP_TICKS 15
#define FOCUS_TICKS 15

//--[System]
void PairanormalDialogue::ConstructInvestigation()
{
    //--Called after images are loaded, builds position data for parts of Investigation Mode.
    mInvestigationButtons[DIABTN_INV_SWITCHMODES].SetWH(24.0f, 622.0f, 49.0f, 52.0f);
    mInvestigationButtons[DIABTN_INV_GOHOME].SetWH(    940.0f, 629.0f, 57.0f, 45.0f);
    mInvestigationButtons[DIABTN_INV_ARROWL].SetWH(     65.0f, 720.0f, 21.0f, 32.0f);
    mInvestigationButtons[DIABTN_INV_ARROWR].SetWH(    937.0f, 720.0f, 21.0f, 32.0f);
    mInvestigationButtons[DIABTN_INV_ITEM0].SetWH(      99.0f, 687.0f, 84.0f, 73.0f);
    mInvestigationButtons[DIABTN_INV_ITEM1].SetWH(     192.0f, 687.0f, 84.0f, 73.0f);
    mInvestigationButtons[DIABTN_INV_ITEM2].SetWH(     285.0f, 687.0f, 84.0f, 73.0f);
    mInvestigationButtons[DIABTN_INV_ITEM3].SetWH(     378.0f, 687.0f, 84.0f, 73.0f);
    mInvestigationButtons[DIABTN_INV_ITEM4].SetWH(     470.0f, 687.0f, 84.0f, 73.0f);
    mInvestigationButtons[DIABTN_INV_ITEM5].SetWH(     563.0f, 687.0f, 84.0f, 73.0f);
    mInvestigationButtons[DIABTN_INV_ITEM6].SetWH(     653.0f, 687.0f, 84.0f, 73.0f);
    mInvestigationButtons[DIABTN_INV_ITEM7].SetWH(     746.0f, 687.0f, 84.0f, 73.0f);
    mInvestigationButtons[DIABTN_INV_ITEM8].SetWH(     839.0f, 687.0f, 84.0f, 73.0f);

    //--Presentation.
    mPresentationButtons[DIABTN_PRS_CHAR0].SetWH( 76.0f, 169.0f, 277.0f, 577.0f);
    mPresentationButtons[DIABTN_PRS_CHAR1].SetWH(354.0f, 169.0f, 277.0f, 577.0f);
    mPresentationButtons[DIABTN_PRS_CHAR2].SetWH(632.0f, 169.0f, 277.0f, 577.0f);
    mPresentationButtons[DIABTN_PRS_SCROLLLFT].SetWH(  0.0f, 169.0f,  76.0f, 450.0f);
    mPresentationButtons[DIABTN_PRS_SCROLLRGT].SetWH(909.0f, 169.0f, 115.0f, 450.0f);
    mPresentationButtons[DIABTN_PRS_LATER].SetWH(460.0f, 400.0f, 444.0f, 75.0f);
    mPresentationButtons[DIABTN_PRS_CHAT].SetWH( 460.0f, 520.0f, 444.0f, 75.0f);
    mPresentNameBox.SetWH(484.0f, 132.0f, 289.0f,  65.0f);
    mPresentInfoBox.SetWH(454.0f, 172.0f, 475.0f, 196.0f);
}

//--[Property Queries]
int PairanormalDialogue::GetInvestigationTime()
{
    return mInvestigationTimeLeft;
}
int PairanormalDialogue::GetInvestigationTimeMax()
{
    return mInvestigationTimeMax;
}

//--[Manipulators]
void PairanormalDialogue::SetInvestigationTime(int pCurrentTime)
{
    mInvestigationTimeLeft = pCurrentTime;
    if(mInvestigationTimeLeft > mInvestigationTimeMax) mInvestigationTimeLeft = mInvestigationTimeMax;
}
void PairanormalDialogue::SetInvestigationTimeMax(int pMaxTime)
{
    mInvestigationTimeMax = pMaxTime;
    if(mInvestigationTimeMax == 0) mInvestigationTimeMax = 1;
    if(mInvestigationTimeMax < -1) mInvestigationTimeMax = 1;
}
void PairanormalDialogue::ActivateInvestigationMode()
{
    mIsInvestigationMode = true;
    mInventoryOffset = 0;
    HideMajorScene();

    //--Set all characters to neutral.
    PairanormalDialogueCharacter *rCharacter = (PairanormalDialogueCharacter *)mCharacterTalkList->PushIterator();
    while(rCharacter)
    {
        rCharacter->SetEmotion("Neutral");
        rCharacter = (PairanormalDialogueCharacter *)mCharacterTalkList->AutoIterate();
    }
}
void PairanormalDialogue::DeactivateInvestigationMode()
{
    mIsInvestigationMode = false;
}
void PairanormalDialogue::ActivateTalkingMode()
{
    //--Flag.
    mIsPresentationMode = true;
}
void PairanormalDialogue::DeactivateTalkingMode()
{
    mIsPresentationMode = false;
    mSpeakingToCharacter = -1;
}
void PairanormalDialogue::ClearInvestigationData()
{
    rShowDiscoveredObject = NULL;
    mInvestigationObjectList->ClearList();
    mDiscoveredObjectList->ClearList();
    ResetString(mGoHomeScript, "No String");
    ResetString(mGoHomeArg, "No String");
}
void PairanormalDialogue::ClearInvestigationCharacterData()
{
    mCharacterTalkList->ClearList();
}
void PairanormalDialogue::SetGoHomeStrings(const char *pScript, const char *pArg)
{
    if(!pScript || !pArg) return;
    ResetString(mGoHomeScript, pScript);
    ResetString(mGoHomeArg, pArg);
}
void PairanormalDialogue::ClearDiscoveredObjectPtr()
{
    //--The last discovered object displays during conversation. Use this to stop displaying it.
    rShowDiscoveredObject = NULL;
}
void PairanormalDialogue::AddInvestigationObject(const char *pName, TwoDimensionReal pDimensions, const char *pIconPath, const char *pScriptPath, const char *pScriptArg)
{
    //--Overload of below that assumes the icon uses 0,0 to 1,1 for its UV coordinates.
    AddInvestigationObject(pName, pDimensions, pIconPath, pScriptPath, pScriptArg, 0.0f, 0.0f, 1.0f, 1.0f);
}
void PairanormalDialogue::AddInvestigationObject(const char *pName, TwoDimensionReal pDimensions, const char *pIconPath, const char *pScriptPath, const char *pScriptArg, float pX1, float pY1, float pX2, float pY2)
{
    //--Adds an object to the investigation list. Clicking one of these areas will run the matching script path.
    //  The majority of investigation icons are part of the background which is then zoomed in. The X/Y values are tuned by background sizes.
    if(!pName || !pIconPath || !pScriptArg || !pScriptPath) return;

    //--Duplication check. Fail if one already exists with the same name.
    if(mInvestigationObjectList->GetElementByName(pName)) return;

    //--Fast-access pointers.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();

    //--Create the object.
    InvestigationPack *nPack = (InvestigationPack *)malloc(sizeof(InvestigationPack));
    nPack->rIconImg = (SugarBitmap *)rDataLibrary->GetEntry(pIconPath);
    nPack->mIconU1 = pX1 / 2560.0f;
    nPack->mIconV1 = pY1 / 1920.0f;
    nPack->mIconU2 = pX2 / 2560.0f;
    nPack->mIconV2 = pY2 / 1920.0f;
    memcpy(&nPack->mExaminePosition, &pDimensions, sizeof(TwoDimensionReal));
    strcpy(nPack->mScriptPath, pScriptPath);
    strcpy(nPack->mScriptArg, pScriptArg);

    //--Dimension positions are halved to match screen coordinates.
    nPack->mExaminePosition.mLft = nPack->mExaminePosition.mLft * BG_SCALE;
    nPack->mExaminePosition.mTop = (nPack->mExaminePosition.mTop - 0.0f) * BG_SCALE;
    nPack->mExaminePosition.mRgt = nPack->mExaminePosition.mRgt * BG_SCALE;
    nPack->mExaminePosition.mBot = (nPack->mExaminePosition.mBot - 0.0f) * BG_SCALE;

    //--Register.
    mInvestigationObjectList->AddElement(pName, nPack, &FreeThis);
}
void PairanormalDialogue::AddDiscoveredObject(const char *pName, const char *pIconPath, const char *pScriptPath, const char *pScriptArg)
{
    //--Overload that assumes 0,0 to 1,1 for the UV coordinates.
    AddDiscoveredObject(pName, pIconPath, pScriptPath, pScriptArg, 0.0f, 0.0f, 1.0f, 1.0f);
}
void PairanormalDialogue::AddDiscoveredObject(const char *pName, const char *pIconPath, const char *pScriptPath, const char *pScriptArg, float pX1, float pY1, float pX2, float pY2)
{
    //--Adds an object to the discovered list, which is displayed at the bottom of the screen. Discovered
    //  objects can be shown to characters during dialogue or re-examined at will.
    if(!pName || !pIconPath || !pScriptArg || !pScriptPath) return;

    //--Duplication check. Fail if one already exists with the same name, just set it as the active object.
    void *rCheckPtr = mDiscoveredObjectList->GetElementByName(pName);
    if(rCheckPtr)
    {
        rShowDiscoveredObject = (InvestigationPack *)rCheckPtr;
        return;
    }

    //--Fast-access pointers.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();

    //--Create the object.
    InvestigationPack *nPack = (InvestigationPack *)malloc(sizeof(InvestigationPack));
    nPack->rIconImg = (SugarBitmap *)rDataLibrary->GetEntry(pIconPath);
    nPack->mIconU1 = pX1 / 2560.0f;
    nPack->mIconV1 = 1.0f - (pY2 / 1920.0f);
    nPack->mIconU2 = pX2 / 2560.0f;
    nPack->mIconV2 = 1.0f - (pY1 / 1920.0f);
    strcpy(nPack->mScriptPath, pScriptPath);
    strcpy(nPack->mScriptArg, pScriptArg);

    //--Register.
    rShowDiscoveredObject = nPack;
    mDiscoveredObjectList->AddElement(pName, nPack, &FreeThis);
}
void PairanormalDialogue::RegisterTalkableCharacter(const char *pName, const char *pScriptPath)
{
    //--Adds the character named to the list of talkable characters during investigation mode. Each
    //  character has a dialogue path which is called when they are shown items or chatted at.
    if(!pName || !pScriptPath) return;

    //--If the character is already listed, just modify their script path.
    PairanormalDialogueCharacter *rCheckCharacter = (PairanormalDialogueCharacter *)mCharacterTalkList->GetElementByName(pName);
    if(rCheckCharacter)
    {
        rCheckCharacter->SetDialoguePath(pScriptPath);
        return;
    }

    //--Otherwise, find the character in the global list.
    PairanormalDialogueCharacter *rGlobalCharacter = (PairanormalDialogueCharacter *)mCharactersList->GetElementByName(pName);
    if(!rGlobalCharacter) return;

    //--Character sets themselves to neutral.
    rGlobalCharacter->SetEmotion("Neutral");

    //--Add a reference copy. Set the character's script path.
    mCharacterTalkList->AddElement(pName, rGlobalCharacter);
    rGlobalCharacter->SetDialoguePath(pScriptPath);
}

//--[Update]
void PairanormalDialogue::UpdateInvestigationMode()
{
    //--[Documentation and Setup]
    //--When in investigation mode, the player can click the background to investigate the stuff on it.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Get the mouse positions.
    float tMouseX, tMouseY, tMouseZ;
    rControlManager->GetMouseCoordsF(tMouseX, tMouseY, tMouseZ);

    //--[Swap Timers]
    if(mIsPresentationMode)
    {
        if(mCharacterTalkSwapTimer < SWAP_TICKS) mCharacterTalkSwapTimer ++;
    }
    else
    {
        if(mCharacterTalkSwapTimer > 0) mCharacterTalkSwapTimer --;
    }

    //--[Focus Timers]
    if(mSpeakingToCharacter == -1)
    {
        if(mCharacterFocusTimer < FOCUS_TICKS)
        {
            mCharacterFocusTimer ++;
        }
        else
        {
            rSlidingCharacter = NULL;
        }
    }
    else
    {
        if(mCharacterFocusTimer > 0) mCharacterFocusTimer --;
    }

    //--[Character Update]
    //--The characters the player can talk to are animated.
    PairanormalDialogueCharacter *rCharacter = (PairanormalDialogueCharacter *)mCharacterTalkList->PushIterator();
    while(rCharacter)
    {
        rCharacter->Update(true);
        rCharacter = (PairanormalDialogueCharacter *)mCharacterTalkList->AutoIterate();
    }

    //--[Mouse Over]
    //--If the mouse is over the bottom part of the screen, increment the scroll timer.
    if(!mIsPresentationMode)
    {
        //--Increment.
        if(tMouseY >= 624.0f)
        {
            if(mInvestigationBottomScrollTimer < MAX_HOVER_TICKS) mInvestigationBottomScrollTimer ++;
        }
        //--Not over it, decrement.
        else
        {
            if(mInvestigationBottomScrollTimer > 0) mInvestigationBottomScrollTimer --;
        }

        //--If in the middle area of the screen, we can move left and right.
        if(tMouseY >= 128.0f && tMouseY < 624.0f)
        {
            //--Over the left edge of the screen. Modify the screen scroll offset.
            if(tMouseX < 64.0f)
            {
                //--Compute percent.
                if(mBGScrollTimer < 15) mBGScrollTimer ++;
                float tPercent = EasingFunction::QuadraticIn(mBGScrollTimer, 15.0f);

                //--Set offset.
                float tCurrentOffset = PairanormalLevel::Fetch()->GetBackgroundOffset();
                PairanormalLevel::Fetch()->SetBackgroundOffset(tCurrentOffset + (10.0f * 1.0f * tPercent));
            }
            //--Right edge.
            else if(tMouseX >= VIRTUAL_CANVAS_X - 64.0f)
            {
                //--Compute percent.
                if(mBGScrollTimer < 15) mBGScrollTimer ++;
                float tPercent = EasingFunction::QuadraticIn(mBGScrollTimer, 15.0f);

                //--Set offset.
                float tCurrentOffset = PairanormalLevel::Fetch()->GetBackgroundOffset();
                PairanormalLevel::Fetch()->SetBackgroundOffset(tCurrentOffset + (10.0f * -1.0f * tPercent));
            }
            //--Neither edge. Reset.
            else
            {
                mBGScrollTimer = 0;
            }
        }
        else
        {
            mBGScrollTimer = 0;
        }
    }
    //--Max the timer when in character mode. Also check mouse position for character highlighting.
    else
    {
        //--Timer max.
        if(mInvestigationBottomScrollTimer < MAX_HOVER_TICKS) mInvestigationBottomScrollTimer ++;

        //--When selecting characters but not speaking to them:
        if(mSpeakingToCharacter == -1)
        {
            //--Character highlighting. -1 means nobody is selected, -2 means scroll left, -3 is scroll right.
            mSelectedCharacter = -1;
            if(mPresentationButtons[DIABTN_PRS_CHAR0].IsPointWithin(tMouseX, tMouseY))
            {
                //--If there is exactly one character, they render in the middle instead. Handle that.
                if(mCharacterTalkList->GetListSize() == 1)
                {
                    mSelectedCharacter = -1;
                }
                //--Normal case.
                else
                {
                    mSelectedCharacter = 0;
                }
            }
            else if(mPresentationButtons[DIABTN_PRS_CHAR1].IsPointWithin(tMouseX, tMouseY))
            {
                //--If there is exactly one character, they render in the middle instead. Handle that.
                if(mCharacterTalkList->GetListSize() == 1)
                {
                    mSelectedCharacter = 0;
                }
                //--Normal case.
                else
                {
                    mSelectedCharacter = 1;
                }
            }
            else if(mPresentationButtons[DIABTN_PRS_CHAR2].IsPointWithin(tMouseX, tMouseY))
            {
                mSelectedCharacter = 2;
            }
            //--Scroll left if available.
            else if(mPresentationButtons[DIABTN_PRS_SCROLLLFT].IsPointWithin(tMouseX, tMouseY))
            {
                mSelectedCharacter = -2;
            }
            //--Scroll right if available.
            else if(mPresentationButtons[DIABTN_PRS_SCROLLRGT].IsPointWithin(tMouseX, tMouseY))
            {
                mSelectedCharacter = -3;
            }
        }
    }

    //--[Mouse Click]
    //--Player decided to examine something, or click an inventory icon.
    if(rControlManager->IsFirstPress("MouseLft"))
    {
        //--[Fixed UI Functions]
        //--Switches to presentation mode.
        if(mInvestigationButtons[DIABTN_INV_SWITCHMODES].IsPointWithin(tMouseX, tMouseY))
        {
            if(!mIsPresentationMode)
                ActivateTalkingMode();
            else
                DeactivateTalkingMode();

            //--SFX.
            AudioManager::Fetch()->PlaySound("UI|Select");
            return;
        }
        //--Go Home button.
        else if(mInvestigationButtons[DIABTN_INV_GOHOME].IsPointWithin(tMouseX, tMouseY))
        {
            //--SFX.
            AudioManager::Fetch()->PlaySound("UI|Select");

            //--Execution.
            LuaManager::Fetch()->ExecuteLuaFile(mGoHomeScript, 1, "S", mGoHomeArg);
            return;
        }
        //--Left arrow. Decrements inventory offset.
        else if(mInvestigationButtons[DIABTN_INV_ARROWL].IsPointWithin(tMouseX, tMouseY))
        {
            //--SFX.
            AudioManager::Fetch()->PlaySound("UI|Select");

            //--Flags.
            mInventoryOffset --;
            if(mInventoryOffset < 0) mInventoryOffset = 0;
            return;
        }
        //--Right arrow. Increments inventory offset.
        else if(mInvestigationButtons[DIABTN_INV_ARROWR].IsPointWithin(tMouseX, tMouseY))
        {
            //--SFX.
            AudioManager::Fetch()->PlaySound("UI|Select");

            //--Flags.
            int tMaxOffset = (mDiscoveredObjectList->GetListSize() - 9);
            mInventoryOffset ++;
            if(mInventoryOffset > tMaxOffset) mInventoryOffset = tMaxOffset;
            if(mInventoryOffset < 0) mInventoryOffset = 0;
            return;
        }

        //--[Investigation]
        //--Check background objects.
        if(!mIsPresentationMode)
        {
            //--Modify the mouse coordinates to background coordinates.
            float cXOffset = -PairanormalLevel::Fetch()->GetBackgroundOffset() * BG_SCALE;
            float cYOffset = -BG_OFFSET_Y * BG_SCALE;
            float cMouseX = (tMouseX) + cXOffset;
            float cMouseY = (tMouseY) + cYOffset;

            //--Scan through the objects in the background. If any are hit, activate them.
            InvestigationPack *rClickedPack = NULL;
            InvestigationPack *rPack = (InvestigationPack *)mInvestigationObjectList->PushIterator();
            while(rPack)
            {
                //--Hit check.
                if(rPack->mExaminePosition.IsPointWithin(cMouseX, cMouseY))
                {
                    rClickedPack = rPack;
                    mInvestigationObjectList->PopIterator();
                    break;
                }

                //--Next.
                rPack = (InvestigationPack *)mInvestigationObjectList->AutoIterate();
            }

            //--If there was a hit, run that pack. We don't run the pack during iteration because the
            //  script could change the list contents.
            if(rClickedPack)
            {
                //--SFX.
                AudioManager::Fetch()->PlaySound("UI|Select");

                //--Execution.
                LuaManager::Fetch()->ExecuteLuaFile(rClickedPack->mScriptPath, 1, "S", rClickedPack->mScriptArg);
                return;
            }
        }
        //--[Characters]
        //--Talk to characters.
        else if(mIsPresentationMode && mSpeakingToCharacter == -1)
        {
            //--Default.
            mSpeakingToCharacter = -1;

            //--Nobody is selected.
            if(mSelectedCharacter == -1)
            {
            }
            //--Scroll left.
            else if(mSelectedCharacter == -2)
            {
                if(mPresentationOffset > 0)
                {
                    mPresentationOffset --;
                    AudioManager::Fetch()->PlaySound("UI|Select");
                }
                return;
            }
            //--Scroll right.
            else if(mSelectedCharacter == -3)
            {
                if(mPresentationOffset < mCharacterTalkList->GetListSize() - 3)
                {
                    mPresentationOffset ++;
                    AudioManager::Fetch()->PlaySound("UI|Select");
                }

                return;
            }
            //--Otherwise, resolve which character to speak to.
            else if(mSelectedCharacter != -1)
            {
                //--SFX.
                AudioManager::Fetch()->PlaySound("UI|Select");

                //--Execution.
                PairanormalDialogueCharacter *rSpeakCharacter = (PairanormalDialogueCharacter *)mCharacterTalkList->GetElementBySlot(mSelectedCharacter + mPresentationOffset);
                if(rSpeakCharacter)
                {
                    mSpeakingToCharacter = mSelectedCharacter + mPresentationOffset;
                    rSlidingCharacter = rSpeakCharacter;
                }
                return;
            }
        }
        //--[Speaking]
        //--Allows chatting, presentation of items, or cancelling out.
        else if(mIsPresentationMode && mSpeakingToCharacter != -1)
        {
            //--Cancels this.
            if(mPresentationButtons[DIABTN_PRS_LATER].IsPointWithin(tMouseX, tMouseY))
            {
                //--SFX.
                AudioManager::Fetch()->PlaySound("UI|Select");

                //--Execution.
                mSpeakingToCharacter = -1;
                return;
            }
            //--Activates the chat script.
            else if(mPresentationButtons[DIABTN_PRS_CHAT].IsPointWithin(tMouseX, tMouseY))
            {
                //--SFX.
                AudioManager::Fetch()->PlaySound("UI|Select");

                //--Execution.
                PairanormalDialogueCharacter *rSpeakCharacter = (PairanormalDialogueCharacter *)mCharacterTalkList->GetElementBySlot(mSelectedCharacter + mPresentationOffset);
                if(rSpeakCharacter)
                {
                    LuaManager::Fetch()->ExecuteLuaFile(rSpeakCharacter->GetDialoguePath(), 1, "S", "Chat");
                    return;
                }
            }
        }

        //--[Inventory]
        //--Check the discovered objects.
        int tBypass = 0;
        int tSlot = 0;
        InvestigationPack *rClickedPack = NULL;
        InvestigationPack *rPack = (InvestigationPack *)mDiscoveredObjectList->PushIterator();
        while(rPack)
        {
            //--Skip packs that are beneath the offset count.
            if(tBypass < mInventoryOffset)
            {
                tBypass ++;
                rPack = (InvestigationPack *)mDiscoveredObjectList->AutoIterate();
                continue;
            }

            //--Hit check.
            if(mInvestigationButtons[DIABTN_INV_ITEM0 + tSlot].IsPointWithin(tMouseX, tMouseY))
            {
                rClickedPack = rPack;
                mDiscoveredObjectList->PopIterator();
                break;
            }

            //--Next.
            tSlot ++;
            rPack = (InvestigationPack *)mDiscoveredObjectList->AutoIterate();
        }

        //--Player clicked a discovered object.
        if(rClickedPack)
        {
            //--SFX.
            AudioManager::Fetch()->PlaySound("UI|Select");

            //--When not presenting, run the object's examination file.
            if(!mIsPresentationMode)
            {
                LuaManager::Fetch()->ExecuteLuaFile(rClickedPack->mScriptPath, 1, "S", rClickedPack->mScriptArg);
                return;
            }
            //--Activates the chat script for the character with the object as the argument.
            else
            {
                PairanormalDialogueCharacter *rSpeakCharacter = (PairanormalDialogueCharacter *)mCharacterTalkList->GetElementBySlot(mSelectedCharacter + mPresentationOffset);
                if(rSpeakCharacter)
                {
                    LuaManager::Fetch()->ExecuteLuaFile(rSpeakCharacter->GetDialoguePath(), 1, "S", rClickedPack->mScriptArg);
                    return;
                }
            }
        }
    }
}

//--[Rendering]
void PairanormalDialogue::RenderInvestigationMode()
{
    //--[Documentation and Setup]
    //--Renders the UI used during investigation mode.

    //--[Characters]
    //--Render during presentation.
    if(mCharacterTalkSwapTimer > 0 && mSpeakingToCharacter == -1)
    {
        //--Alpha computation.
        float cAlpha = EasingFunction::QuadraticInOut(mCharacterTalkSwapTimer, SWAP_TICKS);
        float cFocus = EasingFunction::QuadraticInOut(mCharacterFocusTimer, FOCUS_TICKS);
        if(mCharacterFocusTimer < FOCUS_TICKS)
        {
            cAlpha = cFocus;
        }

        //--Render the 0th character if it exists.
        PairanormalDialogueCharacter *rZeroCharacter = (PairanormalDialogueCharacter *)mCharacterTalkList->GetElementBySlot(mPresentationOffset + 0);
        if(rZeroCharacter)
        {
            //--Highlighting.
            float cUsealpha = cAlpha;
            if(rZeroCharacter == rSlidingCharacter) cUsealpha = 1.0f;
            StarlightColor::SetMixer(1.0f * cUsealpha, 1.0f * cUsealpha, 1.0f * cUsealpha, cUsealpha);
            if(mSelectedCharacter != 0) StarlightColor::SetMixer(0.5f * cUsealpha, 0.5f * cUsealpha, 0.5f * cUsealpha, cUsealpha);

            //--If this is the only character visible, render at the 1 position instead.
            int cUseBtn = DIABTN_PRS_CHAR0;
            if(mCharacterTalkList->GetListSize() == 1) cUseBtn = DIABTN_PRS_CHAR1;

            //--Store.
            if(mCharacterFocusTimer == FOCUS_TICKS)
            {
                rZeroCharacter->RenderAtCenter(mPresentationButtons[cUseBtn].mXCenter, VIRTUAL_CANVAS_Y - 50.0f);
                rZeroCharacter->mPresentRenderX = mPresentationButtons[cUseBtn].mXCenter;
                rZeroCharacter->mPresentRenderY = VIRTUAL_CANVAS_Y - 50.0f;
            }
            //--Otherwise, change position.
            else
            {
                float cX = rZeroCharacter->mPresentRenderX + (mPresentationButtons[cUseBtn].mXCenter - rZeroCharacter->mPresentRenderX) * cFocus;
                rZeroCharacter->RenderAtCenter(cX, VIRTUAL_CANVAS_Y - 50.0f);
            }

            //--Clean.
            StarlightColor::ClearMixer();
        }

        //--Render the 1th character.
        PairanormalDialogueCharacter *rOnethCharacter = (PairanormalDialogueCharacter *)mCharacterTalkList->GetElementBySlot(mPresentationOffset + 1);
        if(rOnethCharacter)
        {
            //--Highlighting.
            float cUsealpha = cAlpha;
            if(rOnethCharacter == rSlidingCharacter) cUsealpha = 1.0f;
            StarlightColor::SetMixer(1.0f * cUsealpha, 1.0f * cUsealpha, 1.0f * cUsealpha, cUsealpha);
            if(mSelectedCharacter != 1) StarlightColor::SetMixer(0.5f * cUsealpha, 0.5f * cUsealpha, 0.5f * cUsealpha, cUsealpha);

            //--Store.
            if(mCharacterFocusTimer == FOCUS_TICKS)
            {
                rOnethCharacter->RenderAtCenter(mPresentationButtons[DIABTN_PRS_CHAR1].mXCenter, VIRTUAL_CANVAS_Y - 50.0f);
                rOnethCharacter->mPresentRenderX = mPresentationButtons[DIABTN_PRS_CHAR1].mXCenter;
                rOnethCharacter->mPresentRenderY = VIRTUAL_CANVAS_Y - 50.0f;
            }
            //--Otherwise, change position.
            else
            {
                float cX = rOnethCharacter->mPresentRenderX + (mPresentationButtons[DIABTN_PRS_CHAR1].mXCenter - rOnethCharacter->mPresentRenderX) * cFocus;
                rOnethCharacter->RenderAtCenter(cX, VIRTUAL_CANVAS_Y - 50.0f);
            }
            StarlightColor::ClearMixer();
        }

        //--Render the 2nd character.
        PairanormalDialogueCharacter *rSecondCharacter = (PairanormalDialogueCharacter *)mCharacterTalkList->GetElementBySlot(mPresentationOffset + 2);
        if(rSecondCharacter)
        {
            //--Highlighting.
            float cUsealpha = cAlpha;
            if(rSecondCharacter == rSlidingCharacter) cUsealpha = 1.0f;
            StarlightColor::SetMixer(1.0f * cUsealpha, 1.0f * cUsealpha, 1.0f * cUsealpha, cUsealpha);
            if(mSelectedCharacter != 2) StarlightColor::SetMixer(0.5f * cUsealpha, 0.5f * cUsealpha, 0.5f * cUsealpha, cUsealpha);

            //--Store.
            if(mCharacterFocusTimer == FOCUS_TICKS)
            {
                rSecondCharacter->RenderAtCenter(mPresentationButtons[DIABTN_PRS_CHAR2].mXCenter, VIRTUAL_CANVAS_Y - 50.0f);
                rSecondCharacter->mPresentRenderX = mPresentationButtons[DIABTN_PRS_CHAR2].mXCenter;
                rSecondCharacter->mPresentRenderY = VIRTUAL_CANVAS_Y - 50.0f;
            }
            //--Otherwise, change position.
            else
            {
                float cX = rSecondCharacter->mPresentRenderX + (mPresentationButtons[DIABTN_PRS_CHAR2].mXCenter - rSecondCharacter->mPresentRenderX) * cFocus;
                rSecondCharacter->RenderAtCenter(cX, VIRTUAL_CANVAS_Y - 50.0f);
            }
            StarlightColor::ClearMixer();
        }

        //--Render some arrows to indicate other characters can be talked to.
        if(mPresentationOffset > 0) Images.Investigation.rMoreCharsLft->Draw();
        if(mPresentationOffset < mCharacterTalkList->GetListSize() - 3) Images.Investigation.rMoreCharsRgt->Draw();
    }
    //--Presenting something to the character in question.
    else if(mIsPresentationMode && mSpeakingToCharacter != -1)
    {
        //--Compute the position of this character.
        float cSlidePercent = 1.0f - EasingFunction::QuadraticInOut(mCharacterFocusTimer, FOCUS_TICKS);

        //--Get the characters.
        PairanormalDialogueCharacter *rSpeakCharacter = (PairanormalDialogueCharacter *)mCharacterTalkList->GetElementBySlot(mSelectedCharacter + mPresentationOffset);
        PairanormalDialogueCharacter *rZeroCharacter = (PairanormalDialogueCharacter *)mCharacterTalkList->GetElementBySlot(mPresentationOffset + 0);
        PairanormalDialogueCharacter *rOnethCharacter = (PairanormalDialogueCharacter *)mCharacterTalkList->GetElementBySlot(mPresentationOffset + 1);
        PairanormalDialogueCharacter *rSecondCharacter = (PairanormalDialogueCharacter *)mCharacterTalkList->GetElementBySlot(mPresentationOffset + 2);

        //--Set mixing color.
        StarlightColor::SetMixer(1.0f - cSlidePercent, 1.0f - cSlidePercent, 1.0f - cSlidePercent, 1.0f - cSlidePercent);

        //--Render all the characters who aren't the speaking character.
        if(rZeroCharacter && rZeroCharacter != rSpeakCharacter)
        {
            rZeroCharacter->RenderAtCenter(rZeroCharacter->mPresentRenderX, rZeroCharacter->mPresentRenderY);
        }
        if(rOnethCharacter && rOnethCharacter != rSpeakCharacter)
        {
            rOnethCharacter->RenderAtCenter(rOnethCharacter->mPresentRenderX, rOnethCharacter->mPresentRenderY);
        }
        if(rSecondCharacter && rSecondCharacter != rSpeakCharacter)
        {
            rSecondCharacter->RenderAtCenter(rSecondCharacter->mPresentRenderX, rSecondCharacter->mPresentRenderY);
        }
        StarlightColor::ClearMixer();

        //--Store the character's name and render them to the side.
        char tNameBuf[32];
        if(rSpeakCharacter)
        {
            //--Position.
            float cIdealX = 272.0f;
            float cX = rSpeakCharacter->mPresentRenderX + ((cIdealX - rSpeakCharacter->mPresentRenderX) * cSlidePercent);

            //--Store the name.
            strcpy(tNameBuf, mCharacterTalkList->GetNameOfElementBySlot(mSelectedCharacter + mPresentationOffset));

            //--Render the character.
            rSpeakCharacter->RenderAtCenter(cX, VIRTUAL_CANVAS_Y - 50.0f);

            //--Cap!
            if(mCharacterFocusTimer == 0)
            {
                rSpeakCharacter->mPresentRenderX = cIdealX;
            }
        }

        //--Render the UI parts, and some text on this box.
        Images.Investigation.rShowMeBox->Draw();
        StarlightColor::SetMixer(0.0f, 0.0f, 0.0f, 1.0f);
        Images.Data.rHeadingFont->DrawText(435.0f, 200.0f, 0, 1.0f, "Got something to");
        Images.Data.rHeadingFont->DrawText(435.0f, 240.0f, 0, 1.0f, "show me?");
        StarlightColor::ClearMixer();

        //--Render the character's name over the previous box.
        Images.Investigation.rShowMeNameBox->Draw();
        Images.Data.rHeadingFont->DrawText(500.0f, 136.0f, 0, 1.0f, tNameBuf);

        //--Constants. These buttons use offsets since they have edging.
        float cInvBtnOffLft = -15.0f;
        float cInvBtnOffTop = -10.0f;
        float cInvBtnDiaLft = 20.0f;
        float cInvBtnDiaTop = 10.0f;

        //--Later.
        Images.Investigation.rButtonBlack->Draw(mPresentationButtons[DIABTN_PRS_LATER].mLft + cInvBtnOffLft, mPresentationButtons[DIABTN_PRS_LATER].mTop + cInvBtnOffTop);
        Images.Data.rHeadingFont->DrawText(mPresentationButtons[DIABTN_PRS_LATER].mLft + cInvBtnDiaLft, mPresentationButtons[DIABTN_PRS_LATER].mTop + cInvBtnDiaTop, 0, 1.0f, "Later");

        //--Chat.
        Images.Investigation.rButtonBlack->Draw(mPresentationButtons[DIABTN_PRS_CHAT].mLft + cInvBtnOffLft, mPresentationButtons[DIABTN_PRS_CHAT].mTop + cInvBtnOffTop);
        Images.Data.rHeadingFont->DrawText(mPresentationButtons[DIABTN_PRS_CHAT].mLft + cInvBtnDiaLft, mPresentationButtons[DIABTN_PRS_CHAT].mTop + cInvBtnDiaTop, 0, 1.0f, "Chat");

    }

    //--[Fixed UI Pieces]
    //--Scroll down based on this timer. If the player mouses over the inventory, it scrolls up.
    float tOffset = (1.0f - EasingFunction::QuadraticInOut(mInvestigationBottomScrollTimer, MAX_HOVER_TICKS)) * (756.0f - 687.0f);
    glTranslatef(0.0f, tOffset, 0.0f);

    //--Mode changing button uses a different icon in character talk mode.
    if(!mIsPresentationMode)
    {
        Images.Investigation.rMagnifyingLens->Draw(0.0f, tOffset);
    }
    //--Character talk mode is a hand icon.
    else
    {
        Images.Investigation.rHand->Draw(0.0f, tOffset);
    }

    //--These include the inventory and the buttons that change modes.
    Images.Investigation.rHouse->Draw(0.0f, tOffset);
    Images.Investigation.rInvArrows->Draw(0.0f, tOffset);

    //--Render outlines over the inventory buttons.
    for(int i = DIABTN_INV_ITEM0; i < DIABTN_INV_TOTAL; i ++)
    {
        Images.Investigation.rIconBacking->Draw(mInvestigationButtons[i].mLft - 21.0f, mInvestigationButtons[i].mTop - 21.0f);
    }

    //--Icons of investigated objects. They stretch to fit.
    for(int i = DIABTN_INV_ITEM0; i < DIABTN_INV_TOTAL; i ++)
    {
        //--Get and range check.
        InvestigationPack *rPack = (InvestigationPack *)mDiscoveredObjectList->GetElementBySlot(i-DIABTN_INV_ITEM0+mInventoryOffset);
        if(!rPack) break;

        //--Check the image.
        if(!rPack->rIconImg) continue;

        //--Bind it and render it in the object zone with an indent.
        rPack->rIconImg->Bind();
        glBegin(GL_QUADS);
            glTexCoord2f(rPack->mIconU1, rPack->mIconV2); glVertex2f(mInvestigationButtons[i].mLft+3.0f, mInvestigationButtons[i].mTop+3.0f);
            glTexCoord2f(rPack->mIconU2, rPack->mIconV2); glVertex2f(mInvestigationButtons[i].mRgt-3.0f, mInvestigationButtons[i].mTop+3.0f);
            glTexCoord2f(rPack->mIconU2, rPack->mIconV1); glVertex2f(mInvestigationButtons[i].mRgt-3.0f, mInvestigationButtons[i].mBot-3.0f);
            glTexCoord2f(rPack->mIconU1, rPack->mIconV1); glVertex2f(mInvestigationButtons[i].mLft+3.0f, mInvestigationButtons[i].mBot-3.0f);
        glEnd();
    }

    //--Clean.
    glTranslatef(0.0f, -tOffset, 0.0f);
}
