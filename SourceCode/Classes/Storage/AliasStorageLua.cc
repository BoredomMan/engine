//--Base
#include "AliasStorage.h"

//--Classes
//--CoreClasses
//--Definitions
//--GUI
//--Libraries
#include "DataLibrary.h"

//--Managers

//========================================= Lua Hooking ===========================================
void AliasStorage::HookToLuaState(lua_State *pLuaState)
{
    /* PathAlias_CreatePath(sPathName, sPath)
       Creates a new path which can be referenced via alias. */
    lua_register(pLuaState, "PathAlias_CreatePath", &Hook_PathAlias_CreatePath);

    /* PathAlias_CreateAliasToPath(sAlias, sPathName)
       Creates an alias that points to an existing path registered with PathAlias_CreatePath(). */
    lua_register(pLuaState, "PathAlias_CreateAliasToPath", &Hook_PathAlias_CreateAliasToPath);

    /* PathAlias_Clear()
       Clears existing path aliases. */
    lua_register(pLuaState, "PathAlias_Clear", &Hook_PathAlias_Clear);

    /* PathAlias_GetString(sAlias) (1 String)
       Returns the string found at the alias. Strictly checks type. Returns "Null" if not found. */
    lua_register(pLuaState, "PathAlias_GetString", &Hook_PathAlias_GetString);
}

//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
int Hook_PathAlias_CreatePath(lua_State *L)
{
    //PathAlias_CreatePath(sPathName, sPath)

    //--Argument check.
    int tArgs = lua_gettop(L);
    if(tArgs < 2) return LuaArgError("PathAlias_CreatePath");

    //--Execution.
    AliasStorage *rChapterPathStorage = AliasStorage::GetChapterPathAliasStorage();
    rChapterPathStorage->StoreString(lua_tostring(L, 1), lua_tostring(L, 2));

    return 0;
}
int Hook_PathAlias_CreateAliasToPath(lua_State *L)
{
    //PathAlias_CreateAliasToPath(sAlias, sPathName)

    //--Argument check.
    int tArgs = lua_gettop(L);
    if(tArgs < 2) return LuaArgError("PathAlias_CreateAliasToPath");

    //--Execution.
    AliasStorage *rChapterPathStorage = AliasStorage::GetChapterPathAliasStorage();
    rChapterPathStorage->RegisterAliasToString(lua_tostring(L, 1), lua_tostring(L, 2));

    return 0;
}
int Hook_PathAlias_Clear(lua_State *L)
{
    //PathAlias_Clear()
    AliasStorage *rChapterPathStorage = AliasStorage::GetChapterPathAliasStorage();
    rChapterPathStorage->Wipe();
    return 0;
}
int Hook_PathAlias_GetString(lua_State *L)
{
    //PathAlias_GetString(sAlias) (1 String)

    //--Argument check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1)
    {
        lua_pushstring(L, "Null");
        LuaArgError("PathAlias_GetString");
        return 1;
    }

    //--Execution.
    AliasStorage *rChapterPathStorage = AliasStorage::GetChapterPathAliasStorage();
    const char *rCheckString = rChapterPathStorage->GetString(lua_tostring(L, 1));
    if(!rCheckString)
    {
        lua_pushstring(L, "Null");
    }
    else
    {
        lua_pushstring(L, rCheckString);
    }

    return 1;
}
