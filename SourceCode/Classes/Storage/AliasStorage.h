//--[AliasStorage]
//--Stores a set of names which refer to other objects. This allows the same object to be
//  referred to by more than one name, or to be moved more easily when referred to by many objects.
//--The handler stores void pointers in a structure with a type definition, but can be flagged to
//  not check types if desired.

#pragma once

//--[Includes]
#include "Definitions.h"
#include "Structures.h"

//--[Local Structures]
typedef struct AliasStoragePack
{
    int mType;
    DeletionFunctionPtr rDeletionFunction;
    void *rPtr;
    void Initialize()
    {
        mType = POINTER_TYPE_FAIL;
        rDeletionFunction = NULL;
        rPtr = NULL;
    }
}AliasStoragePack;

//--[Local Definitions]
//--[Classes]
class AliasStorage : public RootObject
{
    private:
    //--System
    bool mCheckTypes;

    //--Storage
    SugarLinkedList *mAliasList;//AliasStoragePack *, master
    SugarLinkedList *mStoredStrings;//char *, master

    protected:

    public:
    //--System
    AliasStorage();
    ~AliasStorage();

    //--Public Variables
    //--Property Queries
    const char *GetString(const char *pAlias);

    //--Manipulators
    void StoreString(const char *pName, const char *pString);
    void RemoveString(const char *pName);
    void RegisterAliasToString(const char *pAlias, const char *pStringName);

    //--Core Methods
    void Wipe();

    private:
    //--Private Core Methods
    public:
    //--Update
    //--File I/O
    //--Drawing
    //--Pointer Routing
    //--Static Functions
    static AliasStorage *GetChapterPathAliasStorage();

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_PathAlias_CreatePath(lua_State *L);
int Hook_PathAlias_CreateAliasToPath(lua_State *L);
int Hook_PathAlias_Clear(lua_State *L);
int Hook_PathAlias_GetString(lua_State *L);
