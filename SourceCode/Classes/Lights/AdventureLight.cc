//--Base
#include "AdventureLight.h"

//--Classes
//--CoreClasses
//--Definitions
#include "GlDfn.h"
#include "HitDetection.h"
#include "OpenGLMacros.h"

//--Libraries
//--Managers
#include "CameraManager.h"
#include "DisplayManager.h"

//=========================================== System ==============================================
AdventureLight::AdventureLight()
{
    //--[RootObject]
    //--System
    mType = POINTER_TYPE_ADVENTURELIGHT;

    //--[AdventureLight]
    //--System
    mIsEnabled = true;
    mLocalName = InitializeString("Light");
    mMode = ADLIT_MODE_RADIAL;
    mNeedsToUpload = false;

    //--Common Properties
    mXPosition = 0.0f;
    mYPosition = 0.0f;
    mIntensity = 16.0f;
    mLightColor.SetRGBAF(1.0f, 1.0f, 1.0f, 1.0f);

    //--Ambient Properties
    mWidth = 1.0f;
    mHeight = 1.0f;

    //--Attaching
    mAttachedID = 0;
}
AdventureLight::~AdventureLight()
{
    free(mLocalName);
}

//====================================== Property Queries =========================================
const char *AdventureLight::GetName()
{
    return (const char *)mLocalName;
}
uint32_t AdventureLight::GetAttachedID()
{
    return mAttachedID;
}

//========================================= Manipulators ==========================================
void AdventureLight::Enable()
{
    mIsEnabled = true;
    mNeedsToUpload = true;
}
void AdventureLight::Disable()
{
    mIsEnabled = false;
    mNeedsToUpload = false;
}
void AdventureLight::SetName(const char *pName)
{
    if(!pName) return;
    ResetString(mLocalName, pName);
}
void AdventureLight::SetPosition(float pX, float pY)
{
    //--Returns whether or not the position is different from the original.
    float tOldX = mXPosition;
    float tOldY = mYPosition;
    mXPosition = pX;
    mYPosition = pY;
    mNeedsToUpload = (tOldX != mXPosition || tOldY != mYPosition);
}
void AdventureLight::SetRadial(float pIntensity)
{
    mMode = ADLIT_MODE_RADIAL;
    mIntensity = pIntensity;
    mNeedsToUpload = true;
}
void AdventureLight::SetSquareRadial(float pWidth, float pHeight, float pIntensity)
{
    mMode = ADLIT_MODE_SQUARERADIAL;
    mWidth = pWidth;
    mHeight = pHeight;
    mIntensity = pIntensity;
    mNeedsToUpload = true;
}
void AdventureLight::SetColor(float pRed, float pBlu, float pGrn, float pAlp)
{
    mLightColor.SetRGBAF(pRed, pBlu, pGrn, pAlp);
    mNeedsToUpload = true;
}
void AdventureLight::SetColor(StarlightColor pColor)
{
    memcpy(&mLightColor, &pColor, sizeof(StarlightColor));
    mNeedsToUpload = true;
}
void AdventureLight::AttachToEntity(uint32_t pID)
{
    mAttachedID = pID;
    mNeedsToUpload = true;
}

//========================================= Core Methods ==========================================
//===================================== Private Core Methods ======================================
//============================================ Update =============================================
//=========================================== File I/O ============================================
//=========================================== Drawing =============================================
void AdventureLight::Render()
{
}
void AdventureLight::UploadDataToShader(int &sLightsIndex, float pScale, TwoDimensionReal pCameraDim, bool pAlwaysUpload)
{
    //--[Documentation and Setup]
    //--Uploads the information from this light to the shader. Updates the lights index if any light
    //  information got uploaded.
    //--If information does not need to be uploaded, skips its pass. The flag pAlwaysUpload is used
    //  to force the information to upload.
    if(sLightsIndex >= 128) return;

    //--Fast-access pointers.
    GLint cShaderHandle = DisplayManager::Fetch()->mLastProgramHandle;

    //--[On-Screen Range Check]
    //--Enabled lights always do uSkipLight[] checks. Also do this if pAlwaysUpload is set.
    if(mIsEnabled)
    {
        //--Check if the light is offscreen. If so, we don't need to upload anything. First, Radial lights.
        if(mMode == ADLIT_MODE_RADIAL)
        {
            //--Point is not within the camera boundaries. Skip this light.
            if(!IsPointWithin(mXPosition, mYPosition, pCameraDim.mLft - 128.0f, pCameraDim.mTop - 128.0f, pCameraDim.mRgt + 128.0f, pCameraDim.mBot + 128.0f))
            {
                if(!mIsSkipped || pAlwaysUpload)
                {
                    mIsSkipped = true;
                    ShaderUniform1iArg(cShaderHandle, 1, "uSkipLight[%i]", sLightsIndex);
                }
            }
            //--Point is within the camera boundaries.
            else
            {
                if(mIsSkipped || pAlwaysUpload)
                {
                    mIsSkipped = false;
                    ShaderUniform1iArg(cShaderHandle, 0, "uSkipLight[%i]", sLightsIndex);
                }
            }
        }
        //--Square-radial lights.
        else
        {
            //--Point is not within the camera boundaries. Skip this light.
            if(!IsCollision(mXPosition, mYPosition, mXPosition+mWidth, mYPosition+mHeight, pCameraDim.mLft - 32.0f, pCameraDim.mTop - 32.0f, pCameraDim.mRgt + 32.0f, pCameraDim.mBot + 32.0f))
            {
                if(!mIsSkipped || pAlwaysUpload)
                {
                    mIsSkipped = true;
                    ShaderUniform1iArg(cShaderHandle, 1, "uSkipLight[%i]", sLightsIndex);
                }
            }
            //--Rectangle is within the boundaries.
            else
            {
                if(mIsSkipped || pAlwaysUpload)
                {
                    mIsSkipped = false;
                    ShaderUniform1iArg(cShaderHandle, 0, "uSkipLight[%i]", sLightsIndex);
                }
            }
        }
    }
    //--Disabled lights always skip.
    else
    {
        if(!mIsSkipped || pAlwaysUpload)
        {
            mIsSkipped = true;
            ShaderUniform1iArg(cShaderHandle, 1, "uSkipLight[%i]", sLightsIndex);
        }
    }

    //--[Disabled Lights]
    //--Always skip over a disabled light.
    if(!mIsEnabled && !pAlwaysUpload)
    {
        sLightsIndex ++;
        return;
    }
    //--If we don't need to update the program variables, skip this. Lights that don't move can
    //  skip their uploads to speed up rendering.
    else if(!mNeedsToUpload && !pAlwaysUpload)
    {
        sLightsIndex ++;
        return;
    }

    //--Unset the flag. Light will not need to upload again unless it changes properties.
    mNeedsToUpload = false;

    //--If being skipped, no further information needs to be uploaded.
    if(mIsSkipped && !pAlwaysUpload)
    {
        sLightsIndex ++;
        return;
    }

    //--[Upload Light Information]
    //--Upload the light type.
    ShaderUniform1iArg(cShaderHandle, mMode, "uLightType[%i]", sLightsIndex);

    //--Light position: Radial.
    if(mMode == ADLIT_MODE_RADIAL)
    {
        //--Positions. Only the left and top values are used.
        float cLft = (mXPosition * pScale);
        float cTop = (mYPosition * pScale);
        float cRgt = 0.0f;
        float cBot = 0.0f;

        //--Upload.
        ShaderUniform4fArg(cShaderHandle, cLft, cTop, cRgt, cBot, "uLightPosition[%i]", sLightsIndex);
    }
    //--Light position: Square-Radial.
    else if(mMode == ADLIT_MODE_SQUARERADIAL)
    {
        //--Positions.
        float cLft = (mXPosition * pScale);
        float cTop = (mYPosition * pScale);
        float cRgt = cLft + (mWidth * pScale);
        float cBot = cTop + (mHeight * pScale);

        //--Upload.
        ShaderUniform4fArg(cShaderHandle, cLft, cTop, cRgt, cBot, "uLightPosition[%i]", sLightsIndex);
    }

    //--Light mixer.
    ShaderUniform4fArg(cShaderHandle, mLightColor.r, mLightColor.g, mLightColor.b, 1.0f, "uLightColor[%i]", sLightsIndex);

    //--Intensity.
    ShaderUniform1fArg(cShaderHandle, mIntensity, "uLightIntensity[%i]", sLightsIndex);

    //--Increment the lights counter.
    sLightsIndex ++;
}

//======================================= Pointer Routing =========================================
//====================================== Static Functions =========================================
//========================================= Lua Hooking ===========================================
//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
