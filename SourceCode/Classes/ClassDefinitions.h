//--[AdvCombat]
class AdvCombat;
class AdvCombatAbility;
class AdvCombatAnimation;
class AdvCombatEntity;
class AdvCombatJob;

//--[Entities]
class RootEntity;
class Actor;
class RootEffect;
class TilemapActor;
class PairanormalDialogueCharacter;
class SpineExpression;
class ActorNotice;

//--[Events]
class RootEvent;
class ActorEvent;
class CameraEvent;
class TimerEvent;

//--[GUI]
class AdventureDebug;
class AdventureMenu;
class BeehiveUI;
class CorrupterMenu;
class DeckEditor;
class DialogueActor;
class DialogueTopic;
class StringEntry;
class WorldDialogue;
class PairanormalMenu;
class PairanormalSettingsMenu;
class PairanormalDialogue;
class StringTyrantTitle;

//--[Inventory]
class AdventureInventory;
class AdventureItem;
class InventoryItem;
class InventorySubClass;
class WorldContainer;

//--[Lights]
class AdventureLight;

//--[Minigame]
class KPopDanceGame;
class KPopDanceMove;
class KPopDancer;

//--[Packages]
class MagicPack;
class PerkPack;
class TransformPack;

//--[Storage]
class AliasStorage;

//--[World]
class RootLevel;
class AdventureLevelGenerator;
class ALGTile;
class AdventureLevel;
class BeehiveLevel;
struct BeehiveLevelTile;
class ContextMenu;
class PandemoniumLevel;
class PandemoniumRoom;
class FlexButton;
class FlexMenu;
class GalleryMenu;
class RunningMinigame;
class Skybox;
class TetrisLevel;
class TextLevel;
class TiledLevel;
class TextLevOptions;
class TypingCombat;
class VisualRoom;
class VisualLevel;
class WADFile;
class WordCard;
class WordCombat;
class WorldLight;
class WorldPolygon;
class ZoneEditor;
class ZoneNode;

//--[Class-Local Structures]
typedef struct
{
    //--Variables
    int mHP, mHPMax;
    int mWillPower, mWillPowerMax;
    int mStamina, mStaminaMax;
    int mAttackPower;
    int mDefensePower;
    int mAccuracy;

    //--Functions
    void Clear()
    {
        mHP = 1;
        mHPMax = 1;
        mWillPower = 1;
        mWillPowerMax = 1;
        mStamina = 1;
        mStaminaMax = 1;
        mAttackPower = 0;
        mDefensePower = 0;
        mAccuracy = 0;
    }
    void Set(int pHPMax, int pWPMax, int pSTAMax, int pATP, int pDEF, int pACC)
    {
        if(pHPMax < 1) pHPMax = 1;
        if(pWPMax < 1) pWPMax = 1;
        if(pSTAMax < 1) pSTAMax = 1;
        mHP = pHPMax;
        mHPMax = pHPMax;
        mWillPower = pWPMax;
        mWillPowerMax = pWPMax;
        mStamina = pSTAMax;
        mStaminaMax = pSTAMax;
        mAttackPower = pATP;
        mDefensePower = pDEF;
        mAccuracy = pACC;
    }
}CombatStats;
