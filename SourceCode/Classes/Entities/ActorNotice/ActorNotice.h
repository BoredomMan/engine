//--[ActorNotice]
//--An object that renders text and/or images in an AdventureLevel, typically spawned
//  by an actor of some sort. Used to notify the player of events of perform as
//  ambient dialogue.

#pragma once

//--[Includes]
#include "Definitions.h"
#include "Structures.h"

//--[Local Structures]
//--[Local Definitions]
//--[Classes]
class ActorNotice : public RootObject
{
    private:
    //--System
    int mTimer;
    int mTimerMax;

    //--Display
    char *mText;
    SugarFont *rRenderFont;

    //--Position
    float mX;
    float mY;

    //--Moving
    int mMoveTimer;
    int mMoveTimerMax;
    float mStrtX;
    float mStrtY;
    float mDestX;
    float mDestY;

    protected:

    public:
    //--System
    ActorNotice();
    virtual ~ActorNotice();

    //--Public Variables
    //--Property Queries
    bool IsExpired();
    float GetTargetX();
    float GetTargetY();
    float GetWidth();
    float GetHeight();

    //--Manipulators
    void SetTimerMax(int pTicks);
    void SetText(const char *pText);
    void MoveTo(float pDestX, float pDestY, int pMoveTicks);

    //--Core Methods
    private:
    //--Private Core Methods
    public:
    //--Update
    void Update();

    //--File I/O
    //--Drawing
    void Render();

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions

