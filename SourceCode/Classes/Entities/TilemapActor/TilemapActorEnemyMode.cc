//--Base
#include "TilemapActor.h"

//--Classes
#include "ActorNotice.h"
#include "AdvCombat.h"
#include "AdventureDebug.h"
#include "AdventureInventory.h"
#include "AdventureLevel.h"
#include "AliasStorage.h"
#include "TileLayer.h"
#include "WorldDialogue.h"

//--CoreClasses
#include "SugarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "HitDetection.h"

//--Libraries
//--Managers
#include "AudioManager.h"
#include "EntityManager.h"
#include "LuaManager.h"

//--[Local Definitions]
#define VOIDRIFT_ROLL_MIN 120
#define VOIDRIFT_ROLL_MAX 150

#define TRUE_OFF_X  4.0f
#define TRUE_OFF_Y  8.0f

//=========================================== System ==============================================
void TilemapActor::ActivateEnemyMode(const char *pAssociatedName)
{
    //--Causes this actor to become an enemy. Most of the other functions for enemies won't work at all until this is called,
    //  since it allocates memory for enemy mode.
    if(!pAssociatedName || mIsEnemy) return;

    //--Set the name. When this enemy is killed, this name is added to the list that prevents respawning.
    ResetString(mEnemyPackName, pAssociatedName);

    //--Flags.
    mIsEnemy = true;
    mIsWanderNPC = false;
    mAIState = TA_AI_WANDER;
    mMustRollWanderPoint = true;

    //--Memory Allocation.
    mEnemyListing = new SugarLinkedList(false);

    //--Use the standard shadow.
    SetShadow(xEnemyShadowPath);
}

//====================================== Property Queries =========================================
const char *TilemapActor::GetUniqueEnemyName()
{
    return mEnemyPackName;
}
bool TilemapActor::IsEnemy(const char *pUniqueName)
{
    //--If the passed in name is "Any", returns true if this entity is an enemy of any type.
    if(pUniqueName && !strcasecmp(pUniqueName, "Any"))
    {
        return mIsEnemy;
    }

    //--Otherwise, checks if this is a specific enemy.
    if(!mIsEnemy || !pUniqueName) return false;
    return !(strcasecmp(pUniqueName, mEnemyPackName));
}
float TilemapActor::GetRangeToPlayer()
{
    //--Returns the centroid-centroid range from this entity to the player. Useful for chase/leash logic.
    //  Will return a really high number if the player doesn't exist for some reason.
    float tPlayerX = -10000.0f;
    float tPlayerY = -10000.0f;
    float tDummyZ = 0.0f;
    GetPlayerPosition(tPlayerX, tPlayerY, tDummyZ);

    //--Return the centroid distance.
    return (GetPlanarDistance(mTrueX, mTrueY, tPlayerX, tPlayerY));
}
bool TilemapActor::IsPlayerInSightCone()
{
    //--[Documentation and Setup]
    //--Returns whether or not the player is currently in the enemy's sight cone.
    float tPlayerX, tPlayerY, tPlayerZ;
    GetPlayerPosition(tPlayerX, tPlayerY, tPlayerZ);
    tPlayerY = tPlayerY;

    //--[Initial Checks]
    //--Fail if the player is not in the same Z.
    if(tPlayerZ != mCollisionDepth) return false;

    //--If camouflage mode is active, the player is never within sight.
    if(AdventureDebug::xCamouflageMode) return false;

    //--Get range to player. If it's past the detection range, fail.
    float tRangeToPlayer = GetPlanarDistance(mTrueX, mTrueY, tPlayerX, tPlayerY);
    if(tRangeToPlayer > mViewDistance) return false;

    //--[Angle Determination]
    //--Get the angle to the player.
    float tPlayerAngle = atan2f(tPlayerY - mTrueY+4.0f, tPlayerX - mTrueX+4.0f) * TODEGREE;
    while(tPlayerAngle <    0.0f) tPlayerAngle = tPlayerAngle + 360.0f;
    while(tPlayerAngle >= 360.0f) tPlayerAngle = tPlayerAngle - 360.0f;

    //--Get our facing direction.
    float tFacingAngle = mCurrentViewAngle;
    while(tFacingAngle <    0.0f) tFacingAngle = tFacingAngle + 360.0f;
    while(tFacingAngle >= 360.0f) tFacingAngle = tFacingAngle - 360.0f;

    //--[Angle Check]
    //--Now, is the player's angle within the range of the facing angle? the first case checks for the simple case, the second checks for the
    //  360 degree loop case. They are effectively the same check.
    float cHalfView = mViewAngle / 2.0f;
    if((tPlayerAngle >= tFacingAngle - cHalfView          && tPlayerAngle <= tFacingAngle + cHalfView)          ||
       (tPlayerAngle >= tFacingAngle - cHalfView - 360.0f && tPlayerAngle <= tFacingAngle + cHalfView - 360.0f) ||
       (tPlayerAngle >= tFacingAngle - cHalfView + 360.0f && tPlayerAngle <= tFacingAngle + cHalfView + 360.0f))
    {
        //--So, the player is within range and within the visible angle. Is there a collision between us and them?
        AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
        if(rActiveLevel && rActiveLevel->IsLineClipped(mTrueX + 4.0f, mTrueY + 4.0f, tPlayerX, tPlayerY, mCollisionDepth))
        {
            return false;
        }

        //--Nothing between us and the player. Check passes.
        return true;
    }

    //--[Default]
    //--All checks failed, player is not in sight range.
    return false;
}
TwoDimensionReal TilemapActor::GetFollowPosition(const char *pRegisterName)
{
    //--Given the name of a follower, returns a structure telling that follower where it should be behind us.
    //  Having more followers changes the ruleset so they space evenly.
    TwoDimensionReal tReturnStruct;
    tReturnStruct.Set(mTrueX - 1.0f, mTrueY - 1.0f, mTrueX + 1.0f, mTrueY + 1.0f);

    //--Constants.
    float cFollowDistance = 8.0f;
    float cRadsPerEntity = 60.0f * TORADIAN;

    //--Check if this name already exists on the follower list. If it does, find out what slot it's in.
    int tSlot = mFollowerNameListing->GetSlotOfElementByName(pRegisterName);

    //--If it came back as -1, add it to the list.
    if(tSlot == -1)
    {
        static int xDummyPtr;
        mFollowerNameListing->AddElement(pRegisterName, &xDummyPtr);
        tSlot = mFollowerNameListing->GetListSize() - 1;
    }

    //--Error check: The listing can't somehow have 0 elements!
    if(mFollowerNameListing->GetListSize() < 1) return tReturnStruct;

    //--Set the fan position. The positions are increments 30 degrees off the center, but alternates left and right.
    float tAngle = tSlot * cRadsPerEntity;
    tAngle = tAngle - ((mFollowerNameListing->GetListSize() - 1) * cRadsPerEntity / 2.0f);

    //--Get the base angle based on this entity's facing.
    float tBaseAngle = (((float)mTrueFacing * 45.0f) + 90.0f) * TORADIAN;

    //--Compute positions.
    float cPointX = mTrueX + (cosf(tBaseAngle + tAngle) * cFollowDistance);
    float cPointY = mTrueY + (sinf(tBaseAngle + tAngle) * cFollowDistance);
    tReturnStruct.SetWH(cPointX - 1.0f, cPointY - 1.0f, 2.0f, 2.0f);
    return tReturnStruct;
}

//========================================= Manipulators ==========================================
void TilemapActor::ActivateBagRefillMode()
{
    mIsBagRefill = true;
    SetMuggable(false);
}
void TilemapActor::ActivateWanderMode()
{
    //--An NPC in wander mode behaves like an enemy but never chases or engages the player under any circumstances. Once active
    //  it cannot be deactivated. Enemies that are ignoring the player but are otherwise enemies are handled separately.
    //--This needs to be called last when creating an entity since it leashes to the current position.
    mIsEnemy = true;
    mIsWanderNPC = true;
    mIsIgnoringPlayer = true;

    //--Initial state: Wait a random number of ticks.
    mAIState = TA_AI_WAIT;
    mWaitTimer = 0;
    mWaitTimerMax = rand() % 150 + 30;

    //--Leash to our current position.
    SetLeashingPoint(mTrueX, mTrueY);
}
void TilemapActor::ActivatePatrolMode(const char *pPatrolPath)
{
    //--An NPC can be ordered to walk between a provided set of nodes. Node names are delimited by '|'. This routine stores each
    //  node in an array to be parsed as the entity moves.
    mIsPatrolling = false;
    for(int i = 0; i < mPatrolNodesTotal; i ++) free(mPatrolNodeList[i]);
    free(mPatrolNodeList);
    mPatrolNodesTotal = 0;
    mPatrolNodeList = NULL;
    if(!pPatrolPath || !strcasecmp(pPatrolPath, "Null")) return;

    //--Check how many nodes there are. There is always at least 1, and a '|' denotes another one even if the name is empty.
    int tLen = (int)strlen(pPatrolPath);
    mPatrolNodesTotal = 0;
    for(int i = 0; i < tLen; i ++)
    {
        if(pPatrolPath[i] == '|') mPatrolNodesTotal ++;
    }
    if(mPatrolNodesTotal < 1) return;

    //--Allocate space for the nodes.
    SetMemoryData(__FILE__, __LINE__);
    mPatrolNodeList = (char **)starmemoryalloc(sizeof(char *) * mPatrolNodesTotal);

    //--Begin iterating across the strings, allocating space and copying the data.
    int tStartLetter = 0;
    int tCurrentNode = 0;
    for(int i = 0; i < tLen; i ++)
    {
        //--Delimiter:
        if(pPatrolPath[i] == '|')
        {
            //--Allocate space, copy string.
            int tTotalLen = i - tStartLetter;
            SetMemoryData(__FILE__, __LINE__);
            mPatrolNodeList[tCurrentNode] = (char *)starmemoryalloc(sizeof(char) * (tTotalLen + 1));
            strncpy(mPatrolNodeList[tCurrentNode], &pPatrolPath[tStartLetter], tTotalLen);
            mPatrolNodeList[tCurrentNode][tTotalLen] = '\0';

            //--Move to next string.
            tStartLetter = i + 1;
            tCurrentNode ++;
            if(tCurrentNode >= mPatrolNodesTotal) break;
        }
        //--Skip normal letter.
        else
        {

        }
    }

    //--Other flags.
    mPatrolNodeCurrent = 0;
    mIsPatrolling = true;

    //--Debug.
    //fprintf(stderr, "Patrol Node List.\n");
    //for(int i = 0; i < mPatrolNodesTotal; i ++) fprintf(stderr, "%i: %s\n", i, mPatrolNodeList[i]);
}
void TilemapActor::SetWorldStopIgnoreFlag(bool pFlag)
{
    mIgnoreWorldStop = pFlag;
}
void TilemapActor::AddCombatEnemy(const char *pPrototypeName)
{
    static int xDummy = 0;
    if(!pPrototypeName) return;
    mEnemyListing->AddElement(pPrototypeName, &xDummy);

    //--Special: If the enemy name is "NOBATTLE" then the enemy has a special flag set, and does not trigger battles.
    if(!strcasecmp(pPrototypeName, "NOBATTLE"))
    {
        mIgnoresReinforcement = true;
        mDoesNotTriggerBattles = true;
    }
}
void TilemapActor::SetDefeatScene(const char *pName)
{
    ResetString(mDefeatSceneName, pName);
}
void TilemapActor::SetVictoryScene(const char *pName)
{
    ResetString(mVictorySceneName, pName);
}
void TilemapActor::SetRetreatScene(const char *pName)
{
    ResetString(mRetreatSceneName, pName);
}
void TilemapActor::SetLeashingPoint(float pX, float pY)
{
    mLeashX = pX;
    mLeashY = pY;
}
void TilemapActor::AddIgnoreString(const char *pString)
{
    static int xDummyVar = 6;
    if(!pString) return;
    mIgnorePlayerCodeList->AddElement(pString, &xDummyVar);
}
void TilemapActor::SetToughness(int pValue)
{
    mEnemyToughness = pValue;
}
void TilemapActor::SetFast(bool pFlag)
{
    mIsFast = pFlag;
}
void TilemapActor::SetRelentless(bool pFlag)
{
    mIsRelentless = pFlag;
}
void TilemapActor::SetNeverPauses(bool pFlag)
{
    mNeverPauses = pFlag;
}
void TilemapActor::SetFollowTarget(const char *pTarget)
{
    //--If the value is "Null" ignore it.
    if(!strcasecmp(pTarget, "Null")) return;

    ResetString(mFollowTarget, pTarget);
}
void TilemapActor::RemoveFollower(const char *pName)
{
    mFollowerNameListing->RemoveElementS(pName);
}
void TilemapActor::SetMuggable(bool pIsMuggable)
{
    mCanBeMugged = pIsMuggable;
}
void TilemapActor::SetMugLootable(bool pIsMugLootable)
{
    mHasAnythingToMug = pIsMugLootable;
}
void TilemapActor::ActivateTeamChase()
{
    //--Causes this entity to begin chasing the player. Called when another entity in the same follow group has spotted the player.
    mAIState = TA_AI_CHASE;
}
//========================================= Core Methods ==========================================
void TilemapActor::PulseIgnore(const char *pPulseString)
{
    //--Checks if the actor is ignoring the player in their current state. If so, they act like an NPC. They do not chase the player
    //  and may even have dialogue available.
    //--Note that wandering NPCs always ignore the player even if the pulse is run.
    mHalveLeashDistance = mIsWanderNPC;
    mIsIgnoringPlayer = mIsWanderNPC;
    if(!pPulseString) return;

    //--No need to check the ignore list if we're a wandering NPC.
    if(mIsWanderNPC) return;

    //--If the string is "ALWAYS" then always ignore the player.
    if(!strcasecmp(pPulseString, "ALWAYS")) { mIsIgnoringPlayer = true; return; }

    //--Check all the ignore-strings.
    if(mIgnorePlayerCodeList->GetElementByName(pPulseString))
    {
        mIsIgnoringPlayer = true;
    }
}
#include "DataLibrary.h"
void TilemapActor::BuildGraphicsFromName(const char *pName, bool pIsEightDirection, bool pIsTwoDirectional)
{
    //--Standardized graphics builder, enemies use a 4-direction sprite sheet setup.
    const char *cDirectionNames[TA_DIR_TOTAL] = {"North", "NE", "East", "SE", "South", "SW", "West", "NW"};

    //--Temporary buffer.
    char tBuffer[128];

    //--Special: Bag Refill uses the same set of sprites for all directions.
    if(!strcasecmp(pName, "BagRefill"))
    {
        for(int i = 0; i < TA_DIR_TOTAL; i ++)
        {
            for(int p = 0; p < 4; p ++)
            {
                //--Buffer.
                sprintf(tBuffer, "Root/Images/Sprites/BagRefill/%i", p);

                //--Set the image.
                SetMoveImage(i, p, tBuffer);
                SetRunImage(i, p, tBuffer);
            }
        }
        return;
    }

    //--Iterate. We skip the odd-numbered slots since this is a 4-direction character.
    if(!pIsEightDirection && !pIsTwoDirectional)
    {
        for(int i = 0; i < TA_DIR_TOTAL; i += 2)
        {
            for(int p = 0; p < 4; p ++)
            {
                //--Buffer.
                sprintf(tBuffer, "Root/Images/Sprites/%s/%s|%i", pName, cDirectionNames[i], p);

                //--Set the image.
                SetMoveImage(i, p, tBuffer);
                SetRunImage(i, p, tBuffer);

                //--Special case: If this is "West" case, that takes over for SW and NW.
                if(i == TA_DIR_WEST)
                {
                    SetMoveImage(TA_DIR_SW, p, tBuffer);
                    SetRunImage(TA_DIR_SW, p, tBuffer);
                    SetMoveImage(TA_DIR_NW, p, tBuffer);
                    SetRunImage(TA_DIR_NW, p, tBuffer);
                }
                //--If this is the "East" case, that takes over for SE and NE.
                else if(i == TA_DIR_EAST)
                {
                    SetMoveImage(TA_DIR_NE, p, tBuffer);
                    SetRunImage(TA_DIR_NE, p, tBuffer);
                    SetMoveImage(TA_DIR_SE, p, tBuffer);
                    SetRunImage(TA_DIR_SE, p, tBuffer);
                }
            }
        }
    }
    //--Two directional characters. Used for NPCs like sheep and chickens.
    else if(pIsTwoDirectional)
    {
        //--For each frame:
        for(int p = 0; p < 4; p ++)
        {
            //--Get the east-facing frame.
            sprintf(tBuffer, "Root/Images/Sprites/%s/East|%i", pName, p);

            //--Set the images for North and East variations.
            SetMoveImage(TA_DIR_NORTH, p, tBuffer);
            SetRunImage (TA_DIR_NORTH, p, tBuffer);
            SetMoveImage(TA_DIR_NE,    p, tBuffer);
            SetRunImage (TA_DIR_NE,    p, tBuffer);
            SetMoveImage(TA_DIR_EAST,  p, tBuffer);
            SetRunImage (TA_DIR_EAST,  p, tBuffer);
            SetMoveImage(TA_DIR_SE,    p, tBuffer);
            SetRunImage (TA_DIR_SE,    p, tBuffer);

            //--Get the west-facing frame.
            sprintf(tBuffer, "Root/Images/Sprites/%s/West|%i", pName, p);

            //--Set the images for South and West variations.
            SetMoveImage(TA_DIR_SOUTH, p, tBuffer);
            SetRunImage (TA_DIR_SOUTH, p, tBuffer);
            SetMoveImage(TA_DIR_SW,    p, tBuffer);
            SetRunImage (TA_DIR_SW,    p, tBuffer);
            SetMoveImage(TA_DIR_WEST,  p, tBuffer);
            SetRunImage (TA_DIR_WEST,  p, tBuffer);
            SetMoveImage(TA_DIR_NW,    p, tBuffer);
            SetRunImage (TA_DIR_NW,    p, tBuffer);
        }
    }
    //--Eight directional characters. Only used for a few NPCs.
    else
    {
        for(int i = 0; i < TA_DIR_TOTAL; i ++)
        {
            for(int p = 0; p < 4; p ++)
            {
                //--Buffer.
                sprintf(tBuffer, "Root/Images/Sprites/%s/%s|%i", pName, cDirectionNames[i], p);

                //--Set the image.
                SetMoveImage(i, p, tBuffer);
                SetRunImage(i, p, tBuffer);
            }
        }
    }

    //--Special flags: Scraprats. These use the special standing frame case.
    if(!strcasecmp(pName, "Scraprat"))
    {
        SetSpecialIdleFlag(true);
        SetTinyFlag(true);
    }

    //--Special flags: VoidRifts. Uses an alternate animation mode.
    if(!strcasecmp(pName, "VoidRift"))
    {
        mIsVoidRiftMode = true;
    }

    //--Wounded. Enemies need a wounded frame to appear correctly after being KO'd. Not all enemies
    //  have one, in which case their stunned bodies do not appear.
    //--This can be modified to play an animation.
    sprintf(tBuffer, "Root/Images/Sprites/Special/%s|Wounded", pName);
    AddSpecialFrame("Wounded", tBuffer);
}
void TilemapActor::GetPlayerPosition(float &sX, float &sY, float &sZ)
{
    //--Returns the player's position in the given floats.
    AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
    if(!rActiveLevel) return;

    //--Get the player.
    TilemapActor *rPlayer = rActiveLevel->LocatePlayerActor();
    if(!rPlayer) return;

    //--Set the values.
    sX = rPlayer->GetWorldX();
    sY = rPlayer->GetWorldY();
    sZ = rPlayer->GetCollisionDepth();
}
#define PATH_CAP 200
void TilemapActor::ComputeReinforcement()
{
    //--Called once combat starts. This entity will run through an internal set of checks to see if it
    //  can reinforce the currently battling enemy. If so, the enemy will add itself as reinforcements to
    //  the combat UI. If close enough, they reinforce on the 0th turn and stun themselves.
    //--If not an enemy or unable to path to the player, they do not reinforce. Also, there are some checks
    //  such as distraction or stunning that prevent reinforcement.
    mReinforcementTimer = 0;
    mLastComputedReinforcement = 0;
    if(!mIsEnemy || mIsDying || mSelfDestruct || mStunTimer > 0 || mIsIgnoringPlayer || mIsBagRefill) return;

    //--Don't do anything if this enemy ignores reinforcement.
    if(mIgnoresReinforcement) return;

    //--Compute the raw distance to the player. If it's over a given threshold, fail right away.
    float tRangeToPlayer = GetRangeToPlayer();
    if(tRangeToPlayer > TileLayer::cxSizePerTile * 10.0f) return;

    //--Store the player's position.
    float tPlayerX, tPlayerY, tPlayerZ;
    GetPlayerPosition(tPlayerX, tPlayerY, tPlayerZ);

    //--If the Z position is not the same, we can never reinforce.
    if(tPlayerZ != mCollisionDepth) return;

    //--Store our original properties.
    float tOrigX = mTrueX;
    float tOrigY = mTrueY;
    int tOrigTimer = mMoveTimer;
    bool tOrigState = mIsMoving;
    float tOrigFacing = mFacing;
    float tOrigRemX = mRemainderX;
    float tOrigRemY = mRemainderY;

    //--Now run the pathing routine towards the player. We are emulating movement here, so if collisions are in
    //  the way the enemy might be unable to reach the player.
    int tMoves = 0;
    while(tMoves < PATH_CAP)
    {
        //--Check the distance to the player. If low enough, end emulation.
        float tCurrentRangeToPlayer = GetRangeToPlayer();
        if(tCurrentRangeToPlayer < 8.0f) break;

        //--Run emulation.
        HandleMoveTo(tPlayerX, tPlayerY, -1.0f);

        //--Next.
        tMoves ++;
    }

    //--Reset everything.
    mTrueX = tOrigX;
    mTrueY = tOrigY;
    mMoveTimer = tOrigTimer;
    mIsMoving = tOrigState;
    mFacing = tOrigFacing;
    mRemainderX = tOrigRemX;
    mRemainderY = tOrigRemY;

    //--If the moves value hit PATH_CAP, we couldn't find a path.
    if(tMoves >= PATH_CAP)
    {
    }
    //--If the move range was low enough, join the battle immediately.
    else if(tMoves * mMoveSpeed < TileLayer::cxSizePerTile * 3.0f)
    {
        //--Stun us, same as if we entered the battle immediately.
        mStunTimer = 45;

        //--Add.
        AppendEnemiesToCombat();
        AdvCombat::Fetch()->RegisterWorldReference(this, 0);
    }
    //--Otherwise, become reinforcements at range.
    else
    {
        //--Compute reinforcement turns.
        float tDistance = tMoves * mMoveSpeed;
        mLastComputedReinforcement = tDistance / (TileLayer::cxSizePerTile * 3.0f);

        //--Append as reinforcements.
        AdvCombat::Fetch()->RegisterWorldReference(this, mLastComputedReinforcement);
        //fprintf(stderr, "Enemy %s became reinforcements: %f %i\n", mLocalName, tDistance, mLastComputedReinforcement);
    }
}
void TilemapActor::AppendEnemiesToCombat()
{
    //--Subroutine: Adds the enemies in this package to the combat UI. Can be called from the update routine, but
    //  can also be called by the CombatUI directly in case of reinforcements.

    //--Setup.
    AdvCombat *rAdventureCombat = AdvCombat::Fetch();
    AliasStorage *rEnemyPathStorage = rAdventureCombat->GetEnemyAliasStorage();

    //--Iterate across each entry. Most enemies have 1, some have multiple.
    void *rDummyPtr = mEnemyListing->PushIterator();
    while(rDummyPtr)
    {
        //--Get the name associated with this. It's actually an alias on the enemy listing.
        const char *rPath = rEnemyPathStorage->GetString(mEnemyListing->GetIteratorName());

        //--Call it.
        LuaManager::Fetch()->ExecuteLuaFile(rPath, 1, "N", (float)mEnemyToughness);

        //--Next.
        rDummyPtr = mEnemyListing->AutoIterate();
    }
}
void TilemapActor::HandleMugging()
{
    //--When the mugging timer is over the limit, this is called to move resources to the player.
    if(mHasAnythingToMug)
    {
        //--If there are no enemies in this entity's storage:
        if(mEnemyListing->GetListSize() < 1)
        {
            SpawnNotice("Nothing to steal...");
        }
        //--Otherwise, call the zeroth enemy's script.
        else
        {
            //--Setup.
            char tBuffer[256];
            AdvCombat *rAdventureCombat = AdvCombat::Fetch();
            AliasStorage *rEnemyPathStorage = rAdventureCombat->GetEnemyAliasStorage();

            //--Get the name associated with this. It's actually an alias on the enemy listing.
            const char *rPath = rEnemyPathStorage->GetString(mEnemyListing->GetNameOfElementBySlot(0));
            fprintf(stderr, "Enemy path is %s, alias was %s\n", rPath, mEnemyListing->GetNameOfElementBySlot(0));

            //--Clear static variables.
            TilemapActor::xMugPlatina = 0;
            TilemapActor::xMugExperience = 0;
            TilemapActor::xMugJobPoints = 0;
            TilemapActor::xMugItemList->ClearList();

            //--Set the flag and execute.
            TilemapActor::xIsMugCheck = true;
            LuaManager::Fetch()->ExecuteLuaFile(rPath, 1, "N", (float)mEnemyToughness);
            TilemapActor::xIsMugCheck = false;

            //--If all four come back empty, print a single notice.
            if(xMugPlatina < 1 && xMugExperience < 1 && xMugJobPoints < 1 && xMugItemList->GetListSize() < 1)
            {
                SpawnNotice("Nothing to steal...");
            }
            //--Otherwise, print notices for each.
            else
            {
                //--Make a temporary list of spawned notices.
                SugarLinkedList *trSpawnedNotices = new SugarLinkedList(false);

                //--Notice for each item.
                void *rDummyPtr = xMugItemList->PushIterator();
                while(rDummyPtr)
                {
                    //--The name is the item.
                    const char *rName = xMugItemList->GetIteratorName();
                    sprintf(tBuffer, "Stole a %s!", rName);
                    ActorNotice *nNotice = SpawnNotice(tBuffer);
                    trSpawnedNotices->AddElementAsTail("X", nNotice);

                    //--Put the item in the player's inventory.
                    LuaManager::Fetch()->ExecuteLuaFile(AdventureLevel::xItemListPath, 1, "S", rName);

                    //--Next.
                    rDummyPtr = xMugItemList->AutoIterate();
                }

                //--Job Points
                if(TilemapActor::xMugJobPoints > 0)
                {
                    //--Notice.
                    sprintf(tBuffer, "Got %i JP!", TilemapActor::xMugJobPoints);
                    ActorNotice *nNotice = SpawnNotice(tBuffer);
                    trSpawnedNotices->AddElementAsTail("X", nNotice);

                    //--Award to the player.
                    //TODO
                }

                //--Cash.
                if(TilemapActor::xMugPlatina > 0)
                {
                    //--Notice.
                    sprintf(tBuffer, "Stole %i platina!", TilemapActor::xMugPlatina);
                    ActorNotice *nNotice = SpawnNotice(tBuffer);
                    trSpawnedNotices->AddElementAsTail("X", nNotice);

                    //--Award to the player.
                    AdventureInventory *rInventory = AdventureInventory::Fetch();
                    rInventory->SetPlatina(AdventureInventory::Fetch()->GetPlatina() + TilemapActor::xMugPlatina);
                }

                //--Experience.
                if(TilemapActor::xMugExperience > 0)
                {
                    //--Notice.
                    sprintf(tBuffer, "Got %i exp!", TilemapActor::xMugExperience);
                    ActorNotice *nNotice = SpawnNotice(tBuffer);
                    trSpawnedNotices->AddElementAsTail("X", nNotice);

                    //--Award to the player.
                    AdvCombat::Fetch()->AwardXP(TilemapActor::xMugExperience);
                }

                //--Now rearrange the positions of the notices.
                float tXPosition = mTrueX + mOffsetX - TRUE_OFF_X + 16.0f;
                float tYPosition = mTrueY + mOffsetY - TRUE_OFF_Y;
                ActorNotice *rNotice = (ActorNotice *)trSpawnedNotices->PushIterator();
                while(rNotice)
                {
                    rNotice->MoveTo(tXPosition, tYPosition, TA_TRANSLATE_TICKS);
                    tYPosition = tYPosition - TA_NOTICE_SPACING;
                    rNotice = (ActorNotice *)trSpawnedNotices->AutoIterate();
                }

                //--Clean up.
                delete trSpawnedNotices;
            }
        }
    }
    else
    {
        SpawnNotice("Nothing to steal...");
    }

    //--Mark the enemy as having been looted.
    mHasAnythingToMug = false;
    AdventureLevel::MarkEnemyMugged(mEnemyPackName);

    //--Set the stun timer.
    mMugStunTimer = TA_MUG_STUN_TICKS;
    mMugTimer = 0;

    //--If this entity was a follower, they lose their leader.
    if(mFollowTarget)
    {
        //--If the entity exists, remove us from its follower listing.
        RootEntity *rCheckEntity = EntityManager::Fetch()->GetEntity(mFollowTarget);
        if(rCheckEntity && rCheckEntity->IsOfType(POINTER_TYPE_TILEMAPACTOR))
        {
            TilemapActor *rActor = (TilemapActor *)rCheckEntity;
            rActor->RemoveFollower(mLocalName);
        }

        //--Clear the flag.
        ResetString(mFollowTarget, NULL);
    }
}

//============================================ Update =============================================
bool TilemapActor::HandleEnemyActivation(int pX, int pY)
{
    //--Handles the player activating this entity when in enemy mode. Same as HandleActivation(), except done for
    //  enemies. Right now this handles mugging.
    //--Returns true if the Actor activated something in a fashion that would stop the rest of the activation
    //  update, false otherwise.
    if(!mCanBeMugged) return false;

    //--Entity can be mugged, so let's see if the activation was on them.
    const float cHalfSize = TA_SIZE * 1.0f;

    //--Compute the position coordinates. Unlike normal enemies, extended activation is not allowed.
    float tLft = mTrueX - cHalfSize;
    float tTop = mTrueY - cHalfSize;
    float tRgt = mTrueX + TA_SIZE + cHalfSize;
    float tBot = mTrueY + TA_SIZE + cHalfSize;

    //--Check position.
    if(IsPointWithin(pX, pY, tLft, tTop, tRgt, tBot) && false)
    {
        //--If the entity has spotted the player at all, or is already stunned, play a different sound.
        if(mSpottedPlayerTicks > 0 || mMugStunTimer > 0)
        {
            AudioManager::Fetch()->PlaySound("World|MugMiss");
            return true;
        }

        //--Increment.
        mMugTimer += TA_MUG_TICKS_PER_PRESS;

        //--If this put us over the cap, play a different sound.
        if(mMugTimer >= TA_MUG_TICKS_TOTAL)
        {
            AudioManager::Fetch()->PlaySound("World|MugSuccess");
        }
        //--Normal:
        else
        {
            AudioManager::Fetch()->PlaySound("World|MugHit");
        }
        return true;
    }

    //--All checks failed.
    return false;
}
void TilemapActor::UpdateEnemyMode()
{
    //--If the AdventureLevel is currently doing something to block our update, such as having the GUI be
    //  visible, then we stop here.
    if(AdventureLevel::IsWorldStopped())
    {
        //--Some enemies update when the world is stopped for a cutscene. This function will check that.
        if(mIgnoreWorldStop && AdventureLevel::IsWorldStoppedFromCutsceneOnly())
        {

        }
        //--Otherwise, this is a legitimate stop or the enemy doesn't ignore it.
        else
        {
            return;
        }
    }

    //--If the world is transitioning, in or out, stop them. This ignores the cutscene flag!
    if(AdventureLevel::Fetch()->IsLevelTransitioning())
    {
        return;
    }

    //--Bag Refill: If the player touches us, we refill their Doctor Bag and show a dialogue box.
    if(mIsBagRefill)
    {
        //--Get the player's position.
        float tPlayerX, tPlayerY, tPlayerZ;
        GetPlayerPosition(tPlayerX, tPlayerY, tPlayerZ);

        //--Get the range to the player. If the player is on a different Z plane, the player becomes infinitely far.
        float tRangeToPlayer = GetRangeToPlayer();
        if(mCollisionDepth != tPlayerZ) tRangeToPlayer = 10000.0f;

        //--If the player touches us:
        if(tRangeToPlayer < 8.0f)
        {
            //--Flag self for deletion.
            mSelfDestruct = true;
            AdventureLevel::KillEnemy(GetUniqueEnemyName());

            //--Dialogue pop up.
            WorldDialogue *rWorldDialogue = WorldDialogue::Fetch();
            rWorldDialogue->Show();
            rWorldDialogue->SetMajorSequence(false);
            rWorldDialogue->AppendString("[VOICE|Leader][SOUND|Combat|DoctorBag]Doctor Bag charges refilled!");
            rWorldDialogue->SetSilenceFlag(true);

            //--Refill the Doctor Bag.
            AdventureInventory::Fetch()->SetDoctorBagCharges(AdventureInventory::Fetch()->GetDoctorBagChargesMax());
        }

        //--Stop the update here.
        return;
    }

    //--If the enemy is dying, handle that instead. Dying enemies don't block unlimited stamina.
    if(mIsDying)
    {
        //--Timer.
        if(mDeathTimer < 30)
        {
            //--Increment.
            mDeathTimer ++;

            //--While dying, the enemy will randomly change directions.
            if(mDeathTimer % 3 == 0) mFacing = rand() % TA_DIR_TOTAL;

            //--On the exact 30, switch special frame:
            if(mDeathTimer == 30)
            {
                ActivateSpecialFrame("Wounded");
            }
        }

        //--Stop the update.
        return;
    }

    //--A stunned enemy doesn't move, but does count in terms of stamina blocking.
    if(mStunTimer > 0)
    {
        AdventureLevel::xEntitiesDrainStamina = true;
        mStunTimer --;
        return;
    }

    //--Void Rift timer. Used for animations.
    if(mIsVoidRiftMode)
    {
        mVoidRiftTimer ++;

        //--Every tick there's a 5% chance of resetting the rift timer. At the max, it always resets.
        if(mVoidRiftTimer >= VOIDRIFT_ROLL_MAX || (mVoidRiftTimer >= VOIDRIFT_ROLL_MIN && rand() % 100 < 5))
        {
            mVoidRiftTimer = 0;
            mVoidRiftFrame = rand() % 3;
        }
    }

    //--[Mugging]
    //--If an enemy has a mug timer over zero, they are in the process of being mugged. Decrement the timer.
    if(mMugTimer > 0)
    {
        //--If the mug timer is over the cap, the enemy is "Mugged" here.
        if(mMugTimer >= TA_MUG_TICKS_TOTAL)
        {
            HandleMugging();
        }
        //--Otherwise, decrement the timer.
        else
        {
            mMugTimer -= TA_MUG_TICK_LOSS_PER_TICK;
            if(mMugTimer < 0) mMugTimer = 0;
        }
    }
    //--If the enemy was stunned due to a mug, handle that here.
    else if(mMugStunTimer > 0)
    {
        //--Decrement.
        mMugStunTimer --;

        //--If the timer is still over 0, stop the update.
        return;
    }

    //--Enemies who are ignoring the player don't block unlimited stamina.
    if(!mIsIgnoringPlayer) AdventureLevel::xEntitiesDrainStamina = true;

    //--Get the player's position.
    float tPlayerX, tPlayerY, tPlayerZ;
    GetPlayerPosition(tPlayerX, tPlayerY, tPlayerZ);

    //--Special: If the player is at a different depth, we are always ignoring the player.
    bool tIgnoreFromDepth = false;
    if(mCollisionDepth != tPlayerZ) tIgnoreFromDepth = true;

    //--Special: If the player is in an invisibility zone, ignore them.
    bool tIgnoreFromInvisZone = AdventureLevel::xIsPlayerInInvisZone;

    //--Get the range to the player. If the player is on a different Z plane, the player becomes infinitely far.
    float tRangeToPlayer = GetRangeToPlayer();
    if(mCollisionDepth != tPlayerZ) tRangeToPlayer = 10000.0f;

    //--If the player is within a short range, trigger a battle. This does not happen if ignoring the player for any reason.
    if(tRangeToPlayer < 8.0f && !mIsIgnoringPlayer && !tIgnoreFromDepth && !tIgnoreFromInvisZone)
    {
        //--Special: Sometimes enemies trigger a cutscene instead of a battle. If this flag is set, handle that.
        if(mDoesNotTriggerBattles)
        {
            //--Immediately run the "defeat" script. This actor is set to be the stack head when executing.
            AliasStorage *rAliasStorage = AliasStorage::GetChapterPathAliasStorage();
            const char *rPath = rAliasStorage->GetString(mDefeatSceneName);
            fprintf(stderr, "Alias was %s, path was %s\n", mDefeatSceneName, rPath);
            LuaManager::Fetch()->PushExecPop(this, rPath);

            //--Because this execution could theoretically delete this entity, we return out immediately.
            return;
        }
        //--Normal case: Trigger a battle.
        else
        {
            //--Even if the enemy doesn't die (because the player retreated) they have a brief period of stun
            //  to allow the player to escape. This also prevents this enemy from re-adding itself during
            //  the reinforcements sequence.
            mStunTimer = 45;
            mReinforcementTimer = 0;

            //--Battle!
            AdvCombat *rAdventureCombat = AdvCombat::Fetch();
            rAdventureCombat->Reinitialize();
            rAdventureCombat->Activate();
            rAdventureCombat->PulseWorld(false);

            //--Get any applicable scripts and store them as overrides.
            AliasStorage *rAliasStorage = AliasStorage::GetChapterPathAliasStorage();
            if(mDefeatSceneName)
            {
                const char *rPath = rAliasStorage->GetString(mDefeatSceneName);
                rAdventureCombat->SetDefeatScript(rPath);
                fprintf(stderr, "Defeat alias was %s, path was %s\n", mDefeatSceneName, rPath);
            }
            if(mRetreatSceneName)
            {
                const char *rPath = rAliasStorage->GetString(mRetreatSceneName);
                rAdventureCombat->SetRetreatScript(rPath);
                fprintf(stderr, "Retreat alias was %s, path was %s\n", mDefeatSceneName, rPath);
            }
            if(mVictorySceneName)
            {
                const char *rPath = rAliasStorage->GetString(mVictorySceneName);
                rAdventureCombat->SetVictoryScript(rPath);
                fprintf(stderr, "Victory alias was %s, path was %s\n", mDefeatSceneName, rPath);
            }

            //--If the player snuck up on this enemy, they get a huge initiative bonus!
            if(mSpottedPlayerTicks == 0 && mAIState != TA_AI_LEASH && mAIState != TA_AI_CHASE)
            {
                rAdventureCombat->GivePlayerInitiative();
            }

            //--The subroutine will add the enemies for us.
            AppendEnemiesToCombat();

            //--This entity adds itself to the combat's reference list. If the enemy party is wiped out, this
            //  entity will die.
            rAdventureCombat->RegisterWorldReference(this, 0);
        }

        return;
    }

    //--If the player spotted timer is on, it increments a bit and then a chase occurs.
    bool tIsPlayerInSightCone = IsPlayerInSightCone();
    if(mSpottedPlayerTicks > 0 && tIsPlayerInSightCone)
    {
        //--Timer.
        mDropSuspicionTicks = 0;

        //--If we have spotted the player, set closest enemy distance to the lowest value that isn't a combat value.
        //  This increases music intensity.
        if(!mIsIgnoringPlayer && !tIgnoreFromDepth && !tIgnoreFromInvisZone)
        {
            if(AdventureLevel::xClosestEnemy > tRangeToPlayer) AdventureLevel::xClosestEnemy = 9.0f;
        }

        //--Detection increase: If closer than 25% of the detection range, detection rate is x4!
        if(tRangeToPlayer < mViewDistance * 0.25f)
        {
            mSpottedPlayerTicks += 4;
        }
        //--Midrange: Detection is doubled if closer than 50% of the max range.
        else if(tRangeToPlayer < mViewDistance * 0.50f)
        {
            mSpottedPlayerTicks += 2;
        }
        //--Normal detection speed.
        else
        {
            mSpottedPlayerTicks ++;
        }

        //--Stop the update here.
        if(mSpottedPlayerTicks < mSpottedPlayerTicksMax)
        {
            return;
        }
        //--Go to chase mode!
        else
        {
            mAIState = TA_AI_CHASE;
        }
    }
    //--Reset back to zero over time.
    else if(mSpottedPlayerTicks > 0)
    {
        //--Increment the drop-suspicion timer.
        mDropSuspicionTicks ++;

        //--Once the drop-suspicion timer passes a threshold, start dropping the spotting timer.
        if(mDropSuspicionTicks > 45)
        {
            if(mDropSuspicionTicks % 3 == 0) mSpottedPlayerTicks --;
        }
    }

    //--Move the view angle to match the ideal.
    if(!tIsPlayerInSightCone)
    {
        //--Ideal angle.
        float cIdealAngle = (mFacing * 45.0f) - 90.0f;

        //--Turn radius.
        float cTurnPerTick = 12.0f;

        //--Subroutine handles the rest.
        mCurrentViewAngle = HomeInOnAngleDeg(mCurrentViewAngle, cIdealAngle, cTurnPerTick);
    }

    //--Store the intensity-changing flag if needed.
    if(!mIsIgnoringPlayer && !tIgnoreFromDepth && !tIgnoreFromInvisZone)
    {
        //--Only store if closer than the current.
        if(AdventureLevel::xClosestEnemy > tRangeToPlayer) AdventureLevel::xClosestEnemy = tRangeToPlayer;
    }

    //--Chase distance.
    const float cChaseDistance = mViewDistance;

    //--Wandering. Pace around a bit.
    if(mAIState == TA_AI_WANDER)
    {
        //--Patrol case:
        if(mIsPatrolling && mPatrolNodesTotal > 0)
        {
            //--Range check. Chase the player if close enough.
            if(tRangeToPlayer <= cChaseDistance && !mIsIgnoringPlayer && !tIgnoreFromDepth && !tIgnoreFromInvisZone && IsPlayerInSightCone())
            {
                if(mSpottedPlayerTicks < 1) mSpottedPlayerTicks = 1;
            }
            //--Not close enough, path to the patrol node in question.
            else
            {
                //--Check the level.
                AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
                if(!rActiveLevel) return;

                //--Get the patrol node in question.
                TwoDimensionReal tDimensions = rActiveLevel->GetPatrolNode(mPatrolNodeList[mPatrolNodeCurrent]);

                //--Move to its center point.
                int tStoreMove = mMoveTimer;
                if(HandleMoveTo(tDimensions.mXCenter, tDimensions.mYCenter, mMoveSpeed * 0.40f))
                {
                    //--Flags.
                    mPatrolNodeCurrent = ((mPatrolNodeCurrent + 1) % mPatrolNodesTotal);

                    //--If this flag is set, we wait at each patrol node.
                    if(mUseStopOnEachNode)
                    {
                        //--This flag bypasses the use-stop flag. The entity patrols constantly.
                        if(mNeverPauses)
                        {

                        }
                        //--Normal case. Stop and look around.
                        else
                        {
                            mAIState = TA_AI_WAIT;
                            mWaitTimer = 0;
                            mWaitTimerMax = rand() % 240 + 60;
                            if(mNeverPauses)
                            {
                                mWaitTimerMax = 1;
                            }
                        }
                    }
                    //--Otherwise, the entity keeps going until a different timer runs out.
                    else
                    {
                        if(!mNeverRefaceOnNode)
                        {
                            mAIState = TA_AI_TURNTOFACE;
                            mWaitTimer = rand() % 30 + 30;
                        }
                    }

                    //--Set this as our leashing point.
                    SetLeashingPoint(tDimensions.mXCenter, tDimensions.mYCenter);
                }
                //--Not at the location yet.
                else
                {
                    //--Flags.
                    mIsMoving = true;
                    if(mMoveTimer != tStoreMove) mMoveTimer = tStoreMove + 1;

                    //--Decrement this timer. If it reaches zero, we stop and look around a bit. This does not occur if the
                    //  no-stop flag is set.
                    mPatrolTicksLeft --;
                    if(mPatrolTicksLeft < 1 && !mUseStopOnEachNode && !mNeverPauses)
                    {
                        mPatrolTicksLeft = (rand() % 360) + 90;
                        mAIState = TA_AI_WAIT;
                        mWaitTimer = 0;
                        mWaitTimerMax = rand() % 60 + 60;
                    }

                    //--Set this as our leashing point.
                    SetLeashingPoint(tDimensions.mXCenter, tDimensions.mYCenter);
                }
            }

            //--Store the true facing.
            mTrueFacing = mFacing;

            //--Stop the update.
            return;
        }
        //--If following a target:
        else if(mFollowTarget)
        {
            //--Range check. Chase the player if close enough.
            /*if(tRangeToPlayer <= cChaseDistance && !mIsIgnoringPlayer && !tIgnoreFromDepth && IsPlayerInSightCone())
            {
                if(mSpottedPlayerTicks < 1) mSpottedPlayerTicks = 1;
                return;
            }*/

            //--If the follow target does not exist, stop here.
            RootEntity *rCheckEntity = EntityManager::Fetch()->GetEntity(mFollowTarget);
            if(rCheckEntity && rCheckEntity->IsOfType(POINTER_TYPE_TILEMAPACTOR))
            {
                //--Cast.
                TilemapActor *rActor = (TilemapActor *)rCheckEntity;

                //--Register to this object, which will return a TDR indicating where we should path to.
                TwoDimensionReal tFollowPosition = rActor->GetFollowPosition(mLocalName);

                //--How fast we move. If the distance is close enough, don't move until it's higher.
                float cSpeedFactor = 0.40f;
                float tDistance = GetPlanarDistance(mTrueX, mTrueY, tFollowPosition.mXCenter, tFollowPosition.mYCenter);
                if(tDistance > mMoveSpeed * 0.70f * 5.0f)
                {
                    HandleMoveTo(tFollowPosition.mXCenter, tFollowPosition.mYCenter, mMoveSpeed * cSpeedFactor);
                    mTrueFacing = mFacing;
                }

                //--Reposition our leash point to where we are.
                SetLeashingPoint(mTrueX, mTrueY);

                //--Stop the update so the normal wander code doesn't occur.
                return;
            }
            //--If the entity died, unset the flags associated. It's not like they're coming back.
            else
            {
                ResetString(mFollowTarget, NULL);
            }
        }

        //--Normal Case: Either no special cases, or the special cases have an error (such as the follow target not existing).
        if(mMustRollWanderPoint)
        {
            //--Flag.
            mMustRollWanderPoint = false;

            //--Roll an angle and distance.
            float tAngleRoll = (rand() % 360) * 3.1415926f / 180.0f;
            float tDistanceRoll = 25.0f + (((rand() % 100) / 100.0f) * 15.0f);
            //if(mHalveLeashDistance) tDistanceRoll = tDistanceRoll / 2.0f;
            mWanderX = mLeashX + cosf(tAngleRoll) * tDistanceRoll;
            mWanderY = mLeashY + sinf(tAngleRoll) * tDistanceRoll;
        }

        //--Range check. Chase the player if close enough.
        if(tRangeToPlayer <= cChaseDistance && !mIsIgnoringPlayer && !tIgnoreFromDepth && !tIgnoreFromInvisZone && IsPlayerInSightCone())
        {
            if(mSpottedPlayerTicks < 1) mSpottedPlayerTicks = 1;
        }
        //--Not close enough. Wander.
        else
        {
            //--Wander towards the destination. This can be done by emulating an ActorInstruction. It will
            //  return true when we're at the location.
            if(HandleMoveTo(mWanderX, mWanderY, mMoveSpeed * 0.40f))
            {
                StopMoving();
                mAIState = TA_AI_WAIT;
                mWaitTimer = 0;
                mWaitTimerMax = rand() % 30 + 150;
            }
            //--Not at the location yet.
            else
            {

            }

            //--Store the true facing.
            mTrueFacing = mFacing;
        }
    }
    //--Wait around until the timer hits the given amount. When it does, go back to wander mode.
    else if(mAIState == TA_AI_WAIT)
    {
        //--Range check. Chase the player if close enough.
        if(tRangeToPlayer <= cChaseDistance && !mIsIgnoringPlayer && !tIgnoreFromDepth && !tIgnoreFromInvisZone && IsPlayerInSightCone())
        {
            if(mSpottedPlayerTicks < 1) mSpottedPlayerTicks = 1;
        }
        //--Otherwise, hold position.
        else
        {
            //--Monster may periodically change its facing direction. This does *not* change the true facing!
            if(mWaitTimer % 45 == 30) mFacing = mFacing + ((rand() % 3) - 1);
            if(mFacing < TA_DIR_NORTH) mFacing = mFacing + TA_DIR_TOTAL;
            if(mFacing >= TA_DIR_TOTAL) mFacing = mFacing - TA_DIR_TOTAL;

            //--Timer.
            mWaitTimer ++;

            //--Ending case.
            if(mWaitTimer >= mWaitTimerMax)
            {
                //--If we have patrol nodes, turn to face them.
                if(mIsPatrolling && mPatrolNodesTotal > 0)
                {
                    mAIState = TA_AI_TURNTOFACE;
                }
                //--Otherwise immediately return to wander mode.
                else
                {
                    mAIState = TA_AI_WANDER;
                }

                //--Reroll.
                mMustRollWanderPoint = true;
            }
        }
    }
    //--Chase after the player!
    else if(mAIState == TA_AI_CHASE)
    {
        //--Range check the player. A player that runs away will eventually trigger leashing logic.
        float tRangeToLeash = GetPlanarDistance(mTrueX, mTrueY, mLeashX, mLeashY);

        //--How far to leash is based on whether the enemy is "Relentless" or not. Relentless doubles the distance.
        float cLeashDistance = 128.0f;
        if(mIsRelentless) cLeashDistance = cLeashDistance * 2.0f;

        //--How fast we chase the player is based on the "Fast" flag. Increase speed if it's flipped.
        float cSpeedFactor = 1.3f;
        if(mIsFast) cSpeedFactor = cSpeedFactor * 1.80;

        //--If the player leaves this range, begin leashing.
        if(tRangeToPlayer >= cLeashDistance || mIsIgnoringPlayer || tIgnoreFromDepth || tIgnoreFromInvisZone)
        {
            mSpottedPlayerTicks = 0;
            mWaitTimer = 0;
            mAIState = TA_AI_LEASH;
        }
        //--Alternately, if we get far enough from the leash point, also leash back to it.
        else if(tRangeToLeash >= cLeashDistance)
        {
            mSpottedPlayerTicks = 0;
            mWaitTimer = 0;
            mAIState = TA_AI_LEASH;
        }
        //--Otherwise, persue the player.
        else
        {
            //--Store.
            float tOldTrueX = mTrueX;
            float tOldTrueY = mTrueY;

            //--Flags.
            AdventureLevel::xClosestEnemy = 0.0f;

            //--Move towards the player's position.
            HandleMoveTo(tPlayerX, tPlayerY, mMoveSpeed * cSpeedFactor);

            //--Store the position if it's not the same as the previous position.
            if(mTrueX != tOldTrueX || mTrueY != tOldTrueY)
            {
                SetMemoryData(__FILE__, __LINE__);
                TwoDimensionReal *nNewLeashPoint = (TwoDimensionReal *)starmemoryalloc(sizeof(TwoDimensionReal));
                nNewLeashPoint->Set(mTrueX, mTrueY, 1.0f, 1.0f);
                mLeashList->AddElementAsTail("X", nNewLeashPoint, &FreeThis);
            }

            //--Store the true facing.
            mTrueFacing = mFacing;

            //--Order who we're following to chase!
            if(mFollowTarget)
            {
                //--If the follow target does not exist, stop here.
                RootEntity *rCheckEntity = EntityManager::Fetch()->GetEntity(mFollowTarget);
                if(rCheckEntity && rCheckEntity->IsOfType(POINTER_TYPE_TILEMAPACTOR))
                {
                    TilemapActor *rActor = (TilemapActor *)rCheckEntity;
                    rActor->ActivateTeamChase();
                }
            }

            //--If we have anyone following us, order them to chase as well.
            void *rDummyPtr = mFollowerNameListing->PushIterator();
            while(rDummyPtr)
            {
                //--Use the name of the iterator, not the value.
                RootEntity *rCheckEntity = EntityManager::Fetch()->GetEntity(mFollowerNameListing->GetIteratorName());
                if(rCheckEntity && rCheckEntity->IsOfType(POINTER_TYPE_TILEMAPACTOR))
                {
                    TilemapActor *rActor = (TilemapActor *)rCheckEntity;
                    rActor->ActivateTeamChase();
                }

                //--Next.
                rDummyPtr = mFollowerNameListing->AutoIterate();
            }
        }
    }
    //--Return to the leash point.
    else if(mAIState == TA_AI_LEASH)
    {
        //--Wait a few ticks.
        if(mWaitTimer < 15)
        {
            mWaitTimer ++;
        }
        //--Move back to the leash point.
        else
        {
            //--If a TDR is on the leash list's tail, pop it and move to that position.
            TwoDimensionReal *rTail = (TwoDimensionReal *)mLeashList->GetTail();
            if(rTail)
            {
                //--Ending check.
                if(HandleMoveTo(rTail->mLft, rTail->mTop, mMoveSpeed * 0.60f))
                {
                    mLeashList->DeleteTail(); //Note: rTail is now deallocated!
                }

                //--If the list ran out of points, enter wander mode.
                if(mLeashList->GetListSize() < 1)
                {
                    mAIState = TA_AI_WANDER;
                    mMustRollWanderPoint = true;
                }
            }
            //--Something went wrong, so try to walk back to the leash point.
            else
            {
                //--Ending check.
                if(HandleMoveTo(mLeashX, mLeashY, mMoveSpeed * 0.40f))
                {
                    mAIState = TA_AI_WANDER;
                    mMustRollWanderPoint = true;
                }
                //--Not there yet.
                else
                {
                }
            }

            //--Store the true facing.
            mTrueFacing = mFacing;
        }
    }
    //--Turning to face the next node.
    else if(mAIState == TA_AI_TURNTOFACE)
    {
        //--Range check. Chase the player if close enough.
        if(tRangeToPlayer <= cChaseDistance && !mIsIgnoringPlayer && !tIgnoreFromDepth && !tIgnoreFromInvisZone && IsPlayerInSightCone())
        {
            if(mSpottedPlayerTicks < 1) mSpottedPlayerTicks = 1;
        }
        //--Not close enough, turn to face the path node.
        else
        {
            //--Check the level.
            AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
            if(!rActiveLevel) return;

            //--Immediately go back to wandering if we have no patrol nodes.
            if(!mIsPatrolling || mPatrolNodesTotal < 1)
            {
                mAIState = TA_AI_WANDER;
                return;
            }

            //--Get the patrol node in question.
            TwoDimensionReal tDimensions = rActiveLevel->GetPatrolNode(mPatrolNodeList[mPatrolNodeCurrent]);

            //--Wait 35 ticks between actions.
            mWaitTimer --;
            if(mWaitTimer < 1)
            {
                //--Reset.
                mWaitTimer = rand() % 30 + 30;

                //--Wait timer is 1/4'd when mNeverPauses is set.
                if(mNeverPauses) mWaitTimer = mWaitTimer / 4;

                //--Facing Angle.
                float cFaceAngle = (mFacing * 45.0f) - 90.0f;
                while(cFaceAngle <    0.0f) cFaceAngle = cFaceAngle + 360.0f;
                while(cFaceAngle >= 360.0f) cFaceAngle = cFaceAngle - 360.0f;

                //--Get the angle to its center point.
                float cAngleTo = GetAngleBetween(mTrueX, mTrueY, tDimensions.mXCenter, tDimensions.mYCenter) * TODEGREE;
                while(cAngleTo <    0.0f) cAngleTo = cAngleTo + 360.0f;
                while(cAngleTo >= 360.0f) cAngleTo = cAngleTo - 360.0f;

                //--Close enough.
                if(fabs(cAngleTo - cFaceAngle) <= 22.5f)
                {
                    mAIState = TA_AI_WANDER;
                }
                //--Decrease.
                else if(cAngleTo < cFaceAngle)
                {
                    mFacing = (mFacing - 1) % TA_DIR_TOTAL;
                    if(mFacing < 0) mFacing += TA_DIR_TOTAL;
                }
                //--Increase.
                else
                {
                    mFacing = (mFacing + 1) % TA_DIR_TOTAL;
                }
            }
        }

        //--Store the true facing.
        mTrueFacing = mFacing;

        //--Stop the update.
        return;
    }
}
