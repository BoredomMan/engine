//--[TilemapActor]
//--Entity that appears in the tilemap overworld. This has nothing to do with the Actor class.

#pragma once

//--[Includes]
#include "Definitions.h"
#include "Structures.h"
#include "RootEntity.h"

//--[Local Structures]
//--[Local Definitions]
#define TA_MOVE_IMG_TOTAL 4

#define TA_DIR_NORTH 0
#define TA_DIR_NE 1
#define TA_DIR_EAST 2
#define TA_DIR_SE 3
#define TA_DIR_SOUTH 4
#define TA_DIR_SW 5
#define TA_DIR_WEST 6
#define TA_DIR_NW 7
#define TA_DIR_TOTAL 8

//--Instructions
#define TA_INS_NONE 0
#define TA_INS_MOVE_NORTH 1
#define TA_INS_MOVE_EAST 2
#define TA_INS_MOVE_SOUTH 3
#define TA_INS_MOVE_WEST 4
#define TA_INS_HIGHEST 4

//--Sizing
#define TA_SIZE 8
#define TA_EXAMINE_DIM  10.0f
#define TA_EXAMINE_DIST 6.0f
#define TA_AUTOFIRE_DIST 32.0f

//--Death Sequence
#define TA_DEATH_TPF 4.0f
#define TA_DEATH_FRAMES_TOTAL 4

//--AI Definitions
#define TA_AI_WANDER 0
#define TA_AI_WAIT 1
#define TA_AI_CHASE 2
#define TA_AI_LEASH 3
#define TA_AI_TURNTOFACE 4

//--Mugging
#define TA_MUG_TICKS_TOTAL 600
#define TA_MUG_TICKS_PER_PRESS 100
#define TA_MUG_TICK_LOSS_PER_TICK 3
#define TA_MUG_STUN_TICKS 300
#define TA_MUG_STUN_FRAMES_TOTAL 3

//--Notices
#define TA_DEFAULT_NOTICE_TICKS 180
#define TA_TRANSLATE_TICKS 30
#define TA_NOTICE_SPACING 10.0f

//--[Local Structures]
//--Contains DL Paths for all of the images the entity can load. When certain flags are set,
//  this structure keeps a copy of all image paths for the entity, and the entity can re-load
//  all images. This is used during program boot, when entities may be created before the
//  images are loaded. Most entities never instantiate this.
#include "SugarLinkedList.h"
#define TA_DL_PATH_LEN 80
typedef struct
{
    //--Data.
    char mMoveImages[TA_DIR_TOTAL][TA_MOVE_IMG_TOTAL][TA_DL_PATH_LEN];
    char mRunImages[TA_DIR_TOTAL][TA_MOVE_IMG_TOTAL][TA_DL_PATH_LEN];
    SugarLinkedList *mSpecialImageList;//char *, master

    //--Functions.
    void Initialize()
    {
        memset(mMoveImages, 0, sizeof(char) * TA_DIR_TOTAL * TA_MOVE_IMG_TOTAL * TA_DL_PATH_LEN);
        memset(mRunImages, 0, sizeof(char) * TA_DIR_TOTAL * TA_MOVE_IMG_TOTAL * TA_DL_PATH_LEN);
        mSpecialImageList = new SugarLinkedList(true);
    }
    void Delete()
    {
        delete mSpecialImageList;
    }
}TA_Image_Resolve_Pack;

//--[Classes]
class TilemapActor : public RootEntity
{
    private:
    //--System
    bool mIsDisabled;
    bool mIgnoreWorldStop;
    bool mDoesntResetDepthOverride;
    bool mAutoDespawnAfterAnimation;
    int mCollisionDepth;
    uint32_t mLastInstructionHandle;
    bool mIgnoreSpecialLighting;
    bool mIgnoreCollisionsWhenMoving;
    bool mIsPartyEntity;
    bool mRendersAfterTiles;

    //--Position
    bool mIsClipped;
    int mFacing;
    int mTileX;
    int mTileY;
    bool mHasSlopeMoveLeft;
    float mTrueX;
    float mTrueY;
    float mRemainderX;
    float mRemainderY;

    //--Activation Handling
    bool mAutoActivates;
    float mAutoActivateRange;
    bool mHandlesActivation;
    int mExtendedActivationDirection;
    char *mActivationScript;
    TwoDimensionReal mActivationDim;

    //--Instruction Handling
    int mCurrentInstruction;
    SugarLinkedList *mInstructionList;

    //--Movement Handling
    bool mIsMoving;
    bool mIsRunning;
    int mMoveTimer;
    int mMoveDirection;
    float mMoveSpeed;
    float mRunSpeed;
    int mStepState;
    int mTicksSinceStop;

    //--Movement Specials
    bool mAutoAnimates;
    bool mAutoAnimatesFast;
    bool mYOscillates;
    int mYOscillateTimer;
    int mLastMoveTick;
    float mAmountMovedLastTick;
    int mNegativeMoveTimer;

    //--Footstep Sounds
    bool mNoFootstepSounds;
    bool mWasLastFootstepLeft;
    int mPreviousFootstepRollL;
    int mPreviousFootstepRollR;
    char mFootstepBuffer[STD_MAX_LETTERS];

    //--Images
    bool mIsVoidRiftMode;
    int mVoidRiftTimer;
    int mVoidRiftFrame;
    bool mUsesSpecialIdleImages;
    bool mIsTiny;
    float mOffsetX;
    float mOffsetY;
    float mTempOffsetX;
    float mTempOffsetY;
    float mWalkTicksPerFrame;
    float mRunTicksPerFrame;
    float mOverrideDepth;
    bool mNoAutomaticShadow;
    SugarBitmap *rMoveImages[TA_DIR_TOTAL][TA_MOVE_IMG_TOTAL];
    SugarBitmap *rRunImages[TA_DIR_TOTAL][TA_MOVE_IMG_TOTAL];
    SugarBitmap *rShadowImg;
    static bool xHasResolvedPoofImages;
    static SugarBitmap *xrPoofImages[TA_DEATH_FRAMES_TOTAL];

    //--Image Path Storage
    bool mIsStoringImagePaths;
    TA_Image_Resolve_Pack *mImagePathStorage;

    //--Special Images
    bool mAlwaysFacesUp;
    bool mIsShowingSpecialImage;
    bool mIsShowingFlashwhiteSequence;
    int mFlashwhiteTimer;
    SugarBitmap *rCurrentSpecialImage;
    SugarLinkedList *mSpecialImageList; //SugarBitmap *, ref
    char *mReserveSpecialImage;

    //--Enemy State
    bool mIsEnemy;
    bool mIsBagRefill;
    bool mHalveLeashDistance;
    bool mIsWanderNPC;
    bool mIsPredator;
    int mEnemyToughness;
    int mStunTimer;
    int mAIState;
    float mLeashX;
    float mLeashY;
    char *mEnemyPackName;
    char *mDefeatSceneName;
    char *mRetreatSceneName;
    char *mVictorySceneName;
    SugarLinkedList *mEnemyListing;

    //--Enemy Mugging
    bool mCanBeMugged;
    bool mHasAnythingToMug;
    int mMugTimer;
    int mMugStunTimer;
    SugarBitmap *rMugBarEmpty;
    SugarBitmap *rMugBarFull;
    SugarBitmap *rMugStun[TA_MUG_STUN_FRAMES_TOTAL];

    //--Enemy Detection
    float mViewDistance;
    float mViewAngle;
    float mCurrentViewAngle;
    int mDropSuspicionTicks;
    int mSpottedPlayerTicks;
    int mSpottedPlayerTicksMax;
    SugarBitmap *rSpottedExclamation;
    SugarBitmap *rSpottedQuestion;

    //--Special Enemies Properties
    bool mIgnoresReinforcement;
    bool mDoesNotTriggerBattles;
    bool mNeverPauses;
    int mTrueFacing;
    bool mIsFast;
    bool mIsRelentless;
    char *mFollowTarget;
    int mFollowersRegistered;
    SugarLinkedList *mFollowerNameListing;

    //--Leashing
    SugarLinkedList *mLeashList;

    //--Patrol Mode
    bool mIsPatrolling;
    bool mUseStopOnEachNode;
    bool mNeverRefaceOnNode;
    int mPatrolTicksLeft;
    int mPatrolNodesTotal;
    int mPatrolNodeCurrent;
    char **mPatrolNodeList;

    //--Immunity State
    bool mIsIgnoringPlayer;
    SugarLinkedList *mIgnorePlayerCodeList;

    //--Reinforcement Stuff
    int mLastComputedReinforcement;
    int mReinforcementTimer;

    //--AI Variables
    bool mMustRollWanderPoint;
    float mWanderX;
    float mWanderY;
    int mWaitTimer;
    int mWaitTimerMax;

    //--Dying Sequence
    bool mIsDying;
    int mDeathTimer;

    protected:

    public:
    //--System
    TilemapActor();
    virtual ~TilemapActor();

    //--Public Variables
    bool mRanLastTick;

    //--Static Public Variables
    static bool xAllowRender;
    static bool xIsRenderingBeforeTiles;
    static bool xRenderViewcone;
    static char *xEnemyShadowPath;
    static SugarFont *xrReinforcementFont;
    static bool xHasBuiltToughnessLookups;
    static StarlightColor xToughnessLookups[3];
    static int xMaxFootstepSounds;
    static bool xShouldStoreImagePaths;
    static int xAutofireCooldown;
    static bool xIsMugCheck;
    static int xMugPlatina;
    static int xMugExperience;
    static int xMugJobPoints;
    static SugarLinkedList *xMugItemList;

    //--Property Queries
    bool CanHandleInstructions();
    int GetX();
    int GetY();
    float GetWorldX();
    float GetWorldY();
    float GetRemainderX();
    float GetRemainderY();
    int GetFacing();
    int GetMoveTimer();
    bool IsHandlingInstruction();
    float GetMoveSpeed();
    float GetFrameSpeed();
    int GetCollisionDepth();
    bool IsPositionClipped(int pX, int pY);
    bool IsPositionClippedOverride(int pX, int pY);
    float GetMovementLastTick();
    bool IsPartyEntity();
    float GetViewDistance();

    //--Manipulators
    void SetDisable(bool pIsDisabled);
    void SetRendersAfterTiles(bool pFlag);
    void SetIgnoreDepthOverride(bool pFlag);
    void FlagHandledInstruction();
    void SetRunning(bool pFlag);
    void SetCollisionFlag(bool pFlag);
    void SetPosition(float pX, float pY);
    void SetPositionByPixel(float pX, float pY);
    void SetPositionByEntity(const char *pEntityName);
    void SetRemainders(float pX, float pY);
    void SetOffsets(float pXOffset, float pYOffset);
    void SetMoveImage(int pDirection, int pSlot, const char *pDLPath);
    void SetRunImage(int pDirection, int pSlot, const char *pDLPath);
    void SetShadow(const char *pPath);
    void SetMoveSpeed(float pValue);
    void SetFrameSpeed(float pValue);
    void SetActivationScript(const char *pPath);
    void SetExtendedActivationDirection(int pValue);
    void SetFacing(int pDirection);
    void SetFacingToNPC(const char *pName);
    void SetFacingToPoint(int pX, int pY);
    void ForceMoving(int pOverride);
    void StopMoving();
    void BeginDying();
    void ActivateFlashwhite();
    void SetSpecialIdleFlag(bool pFlag);
    void SetTinyFlag(bool pFlag);
    void SetAutoAnimateFlag(bool pFlag);
    void SetAutoAnimateFastFlag(bool pFlag);
    void SetOscillateFlag(bool pFlag);
    void SetCollisionDepth(int pValue);
    void SetOverrideDepth(float pValue);
    void SetMoveTimer(int pTimer);
    void SetAutoDespawn(bool pFlag);
    void SetIgnoreSpecialLights(bool pFlag);
    void SetIgnoreClipsWhenMoving(bool pFlag);
    void SetPartyEntityFlag(bool pFlag);
    void SetNoFootstepSounds(bool pFlag);
    void DisableAutomaticShadow(bool pFlag);
    void SetDontRefaceFlag(bool pFlag);
    void SetNegativeMoveTimer(int pTicks);
    void SetAutofire(float pRange);

    //--Core Methods
    bool HandleActivation(int pX, int pY);
    bool HandleAutoActivation(int pX, int pY);
    bool HandleInstruction(int pInstructionCode);
    void EnqueueInstruction(int pInstructionCode, bool pIsPlayerControl);
    void WipeInstructions();
    void AddSpecialFrame(const char *pName, const char *pDLPath);
    void ActivateSpecialFrame(const char *pName);
    void WipeSpecialFrames();
    void SetReserveFrame(const char *pName);
    char *GetFootstepSFX(bool pOverrideNoFootstep);
    void PrintSpecialFrames();
    void ReresolveLocalImages();
    ActorNotice *SpawnNotice(const char *pText);

    ///--Collisions
    bool IsLftClipped();
    bool IsTopClipped();
    bool IsRgtClipped();
    bool IsBotClipped();
    bool IsLftClippedNarrow();
    bool IsTopClippedNarrow();
    bool IsRgtClippedNarrow();
    bool IsBotClippedNarrow();

    ///--Enemy Methods
    //--System
    void ActivateEnemyMode(const char *pAssociatedName);

    //--Property Queries
    const char *GetUniqueEnemyName();
    bool IsEnemy(const char *pUniqueName);
    float GetRangeToPlayer();
    bool IsPlayerInSightCone();
    TwoDimensionReal GetFollowPosition(const char *pRegisterName);

    //--Manipulators
    void ActivateBagRefillMode();
    void ActivateWanderMode();
    void ActivatePatrolMode(const char *pPatrolPath);
    void SetWorldStopIgnoreFlag(bool pFlag);
    void AddCombatEnemy(const char *pPrototypeName);
    void SetDefeatScene(const char *pName);
    void SetVictoryScene(const char *pName);
    void SetRetreatScene(const char *pName);
    void SetLeashingPoint(float pX, float pY);
    void AddIgnoreString(const char *pString);
    void SetToughness(int pValue);
    void SetFast(bool pFlag);
    void SetRelentless(bool pFlag);
    void SetNeverPauses(bool pFlag);
    void SetFollowTarget(const char *pTarget);
    void RemoveFollower(const char *pName);
    void SetMuggable(bool pIsMuggable);
    void SetMugLootable(bool pIsMugLootable);
    void ActivateTeamChase();

    //--Core Methods
    void PulseIgnore(const char *pPulseString);
    void BuildGraphicsFromName(const char *pName, bool pIsEightDirection, bool pIsTwoDirectional);
    void GetPlayerPosition(float &sX, float &sY, float &sZ);
    void ComputeReinforcement();
    void AppendEnemiesToCombat();
    void HandleMugging();

    //--Update
    bool HandleEnemyActivation(int pX, int pY);
    void UpdateEnemyMode();

    ///--Instruction Handling
    bool HandleMoveTo(int pX, int pY, float pSpeed);
    bool HandleMoveAmount(float &sX, float &sY);

    ///--Movement Handling
    bool EmulateMovement(bool pLft, bool pTop, bool pRgt, bool pBot);
    bool HandleMovement(int pX, int pY, bool pAllowDiagonals);

    private:
    //--Private Core Methods
    void ResolveActivationDimensions();

    public:
    //--Update
    virtual void Update();
    bool HandlePlayerControls(bool pAllowRunning);

    //--File I/O
    //--Drawing
    virtual void Render();
    void RenderUI();
    void RenderMugBar();
    void RenderVisibilityCone(bool pIsLightingActive, GLint pShaderHandle);
    SugarBitmap *ResolveFrame();
    SugarBitmap *GetMapImage(int pTimer);

    //--Pointer Routing
    SugarBitmap *GetMoveImage(int pDirection, int pSlot);
    SugarBitmap *GetRunImage(int pDirection, int pSlot);
    SugarBitmap *GetShadowImage();

    //--Static Functions
    static int GetBestFacing(float pSubjectX, float pSubjectY, float pTargetX, float pTargetY);
    static void ReresolveAllImages();

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_TA_Create(lua_State *L);
int Hook_TA_CreateUsingPosition(lua_State *L);
int Hook_TA_GetProperty(lua_State *L);
int Hook_TA_SetProperty(lua_State *L);
int Hook_TA_ChangeCollisionFlag(lua_State *L);
