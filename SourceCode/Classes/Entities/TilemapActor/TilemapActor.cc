//--Base
#include "TilemapActor.h"

//--Classes
#include "ActorNotice.h"
#include "AdvCombat.h"
#include "AdventureDebug.h"
#include "AdventureLevel.h"
#include "TileLayer.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarFont.h"
#include "SugarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "EasingFunctions.h"
#include "Global.h"
#include "GlDfn.h"
#include "OpenGLMacros.h"
#include "HitDetection.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "ControlManager.h"
#include "DebugManager.h"
#include "DisplayManager.h"
#include "EntityManager.h"
#include "LuaManager.h"

//--[Debug Definitions]
//#define ALLOW_SPEED_TUNING
#define TRUE_OFF_X  4.0f
#define TRUE_OFF_Y  8.0f

//--[Other Definitions]
//--Flashwhite sequence.
#define FLASHWHITE_TICKS_UP 60
#define FLASHWHITE_TICKS_HOLD 30
#define FLASHWHITE_TICKS_DOWN 60
#define FLASHWHITE_TICKS_TOTAL (FLASHWHITE_TICKS_UP + FLASHWHITE_TICKS_HOLD + FLASHWHITE_TICKS_DOWN)

//--Oscillation constants.
#define OSCILLATION_TICKS 60
#define OSCILLATION_AMT 1.2f

//=========================================== System ==============================================
TilemapActor::TilemapActor()
{
    //--[RootObject]
    //--System
    mType = POINTER_TYPE_TILEMAPACTOR;

    //--[RootEntity]
    //--System
    //--Nameable
    //--Renderable
    //--Data Storage
    //--Public Variables

    //--[TilemapActor]
    //--System
    mIsDisabled = false;
    mIgnoreWorldStop = false;
    mDoesntResetDepthOverride = false;
    mAutoDespawnAfterAnimation = false;
    mCollisionDepth = 0;
    mLastInstructionHandle = 0;
    mIgnoreSpecialLighting = false;
    mIgnoreCollisionsWhenMoving = false;
    mIsPartyEntity = false;
    mRendersAfterTiles = false;

    //--Position
    mIsClipped = false;
    mFacing = TA_DIR_SOUTH;
    mTileX = 0;
    mTileY = 0;
    mHasSlopeMoveLeft = false;
    mTrueX = 0;
    mTrueY = 0;
    mRemainderX = 0.0f;
    mRemainderY = 0.0f;

    //--Activation Handling
    mAutoActivates = false;
    mAutoActivateRange = 0.0f;
    mHandlesActivation = false;
    mExtendedActivationDirection = -1;
    mActivationScript = NULL;
    mActivationDim.SetWH(-100.0f, -100.0f, 1.0f, 1.0f);

    //--Instruction Handling
    mCurrentInstruction = TA_INS_NONE;
    mInstructionList = new SugarLinkedList(true);

    //--Movement Handling
    mIsMoving = false;
    mIsRunning = false;
    mMoveTimer = 0;
    mMoveDirection = -1;
    mMoveSpeed = 1.25f;
    mRunSpeed = mMoveSpeed * 1.5f;
    mStepState = 0;
    mTicksSinceStop = 0;

    //--Movement Specials
    mAutoAnimates = false;
    mAutoAnimatesFast = false;
    mYOscillates = false;
    mYOscillateTimer = 0;
    mLastMoveTick = 0;
    mNegativeMoveTimer = 0;

    //--Footstep Sounds
    mNoFootstepSounds = false;
    mWasLastFootstepLeft = true;
    mPreviousFootstepRollL = -1;
    mPreviousFootstepRollR = -1;
    memset(mFootstepBuffer, 0, sizeof(char) * STD_MAX_LETTERS);

    //--Images
    mIsVoidRiftMode = false;
    mVoidRiftTimer = 0;
    mUsesSpecialIdleImages = false;
    mIsTiny = false;
    mOffsetX = -8.0f;
    mOffsetY = -16.0f;
    mTempOffsetX = 0.0f;
    mTempOffsetY = 0.0f;
    mWalkTicksPerFrame = 10.0f;
    mRunTicksPerFrame = 8.0f;
    mOverrideDepth = -2.0f;
    mNoAutomaticShadow = false;
    memset(rMoveImages, 0, sizeof(SugarBitmap *) * TA_DIR_TOTAL * TA_MOVE_IMG_TOTAL);
    memset(rRunImages,  0, sizeof(SugarBitmap *) * TA_DIR_TOTAL * TA_MOVE_IMG_TOTAL);
    rShadowImg = NULL;

    //--Image Path Storage
    mIsStoringImagePaths = false;
    mImagePathStorage = NULL;

    //--Special Images
    mAlwaysFacesUp = false;
    mIsShowingSpecialImage = false;
    mIsShowingFlashwhiteSequence = false;
    mFlashwhiteTimer = 0;
    rCurrentSpecialImage = NULL;
    mSpecialImageList = new SugarLinkedList(false);
    mReserveSpecialImage = NULL;

    //--Enemy State
    mIsEnemy = false;
    mIsBagRefill = false;
    mHalveLeashDistance = false;
    mIsWanderNPC = false;
    mIsPredator = false;
    mEnemyToughness = 0;
    mStunTimer = 0;
    mAIState = 0;
    mLeashX = 0.0f;
    mLeashY = 0.0f;
    mEnemyPackName = NULL;
    mDefeatSceneName = NULL;
    mRetreatSceneName = NULL;
    mVictorySceneName = NULL;
    mEnemyListing = NULL;

    //--Enemy Mugging
    mCanBeMugged = false;
    mMugTimer = 0;
    mMugStunTimer = 0;

    //--Enemy Detection
    mViewDistance = 80.0f;
    mViewAngle = 120.0f;
    mCurrentViewAngle = (mFacing * 0.45f) - 90.0f;
    mDropSuspicionTicks = 0;
    mSpottedPlayerTicks = 0;
    mSpottedPlayerTicksMax = 45;
    rSpottedExclamation = NULL;
    rSpottedQuestion = NULL;

    //--Special Enemies Properties
    mIgnoresReinforcement = false;
    mDoesNotTriggerBattles = false;
    mNeverPauses = false;
    mTrueFacing = DIR_UP;
    mIsFast = false;
    mIsRelentless = false;
    mFollowTarget = NULL;
    mFollowersRegistered = 0;
    mFollowerNameListing = new SugarLinkedList(false);

    //--Leashing
    mLeashList = new SugarLinkedList(true);

    //--Patrol Mode
    mIsPatrolling = false;
    mUseStopOnEachNode = false;
    mNeverRefaceOnNode = false;
    mPatrolTicksLeft = 0;
    mPatrolNodesTotal = 0;
    mPatrolNodeCurrent = 0;
    mPatrolNodeList = NULL;

    //--Immunity State
    mIsIgnoringPlayer = false;
    mIgnorePlayerCodeList = new SugarLinkedList(false);

    //--Reinforcement Stuff
    mLastComputedReinforcement = 0;
    mReinforcementTimer = 0;

    //--AI Variables
    mMustRollWanderPoint = true;
    mWanderX = 0.0f;
    mWanderY = 0.0f;
    mWaitTimer = 0;
    mWaitTimerMax = 1;

    //--Dying Sequence
    mIsDying = false;
    mDeathTimer = 0;

    //--Public Variables
    mRanLastTick = false;

    //--[Death Images Checker]
    if(!xHasResolvedPoofImages)
    {
        //--Clear.
        memset(xrPoofImages, 0, sizeof(SugarBitmap *) * TA_DEATH_FRAMES_TOTAL);

        //--Resolve.
        DataLibrary *rDataLibrary = DataLibrary::Fetch();
        xrPoofImages[0] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/EnemyPoof/0");
        xrPoofImages[1] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/EnemyPoof/1");
        xrPoofImages[2] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/EnemyPoof/2");
        xrPoofImages[3] = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/EnemyPoof/3");
        xHasResolvedPoofImages = VerifyStructure(xrPoofImages, sizeof(SugarBitmap *) * TA_DEATH_FRAMES_TOTAL, sizeof(void *));
    }

    //--[Toughness Colors]
    if(!xHasBuiltToughnessLookups)
    {
        //--Flag.
        xHasBuiltToughnessLookups = true;

        //--Set.
        xToughnessLookups[0].SetRGBF(0.0f, 0.5f, 0.2f);
        xToughnessLookups[1].SetRGBF(0.6f, 0.0f, 0.0f);
        xToughnessLookups[2].SetRGBF(0.4f, 0.1f, 0.8f);
    }

    //--Resolve system images.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    rSpottedExclamation = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Spotted/Exclamation");
    rSpottedQuestion    = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Spotted/Question");
    rMugBarEmpty        = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Mugging/BarEmpty");
    rMugBarFull         = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Mugging/BarFull");
    rMugStun[0]         = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Mugging/Stun0");
    rMugStun[1]         = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Mugging/Stun1");
    rMugStun[2]         = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Mugging/Stun2");

    //--[Storing Image Paths]
    //--If this flag is set, this actor needs to store image paths.
    if(xShouldStoreImagePaths)
    {
        mIsStoringImagePaths = true;
        SetMemoryData(__FILE__, __LINE__);
        mImagePathStorage = (TA_Image_Resolve_Pack *)starmemoryalloc(sizeof(TA_Image_Resolve_Pack));
        mImagePathStorage->Initialize();
    }
}
TilemapActor::~TilemapActor()
{
    delete mInstructionList;
    free(mActivationScript);
    delete mSpecialImageList;
    free(mReserveSpecialImage);
    free(mEnemyPackName);
    delete mEnemyListing;
    delete mIgnorePlayerCodeList;
    delete mLeashList;
    if(mImagePathStorage)
    {
        mImagePathStorage->Delete();
        free(mImagePathStorage);
    }
    for(int i = 0; i < mPatrolNodesTotal; i ++) free(mPatrolNodeList[i]);
    free(mPatrolNodeList);
    free(mFollowTarget);
    delete mFollowerNameListing;
    free(mDefeatSceneName);
    free(mRetreatSceneName);
    free(mVictorySceneName);
}

//--[Private Statics]
//--Images used when an enemy dies. These will clear to NULL if they haven't been resolved yet, at which
//  point the program tries to resolve them. If it fails (because they weren't loaded) this will still
//  be populated by NULL, so always check to make sure they exist!
bool TilemapActor::xHasResolvedPoofImages = false;
SugarBitmap *TilemapActor::xrPoofImages[TA_DEATH_FRAMES_TOTAL];

//--[Public Statics]
//--Flag used to prevent double-render. Because the TiledLevel has depth sorting and per-depth tiles,
//  and uses scaling, entities should render there and not in the EntityManager's render cycle.
//  This flag prevents rendering during the EM's cycle.
bool TilemapActor::xAllowRender = false;

//--Flag used for special entities that render AFTER all tile rendering is completed. Used for some special effect overlays.
bool TilemapActor::xIsRenderingBeforeTiles = false;

//--Whether or not to render the viewcone. Can be toggled from the options menu.
bool TilemapActor::xRenderViewcone = true;

//--DataLibrary path for shadows when creating enemies. This is the standard shadow, individual enemies can
//  override their shadow after creation.
char *TilemapActor::xEnemyShadowPath = InitializeString("None");

//--Font used to render the reinforcement counter that appears above the enemies when pulsing reinforcements.
//  Gets set by the AdventureCombatUI when it resolves its data.
SugarFont *TilemapActor::xrReinforcementFont = NULL;

//--Stores color outlines for tougher enemies.
bool TilemapActor::xHasBuiltToughnessLookups = false;
StarlightColor TilemapActor::xToughnessLookups[3];

//--Indicates the number of footstep sounds loaded into the audio manager. There must be an equal
//  number for both left and right steps. If 0 or lower, footsteps do not play.
int TilemapActor::xMaxFootstepSounds = 1;

//--When set to true, entities will store the paths of images they are sent. These can be used to
//  resolve the images later, because they haven't loaded yet.
bool TilemapActor::xShouldStoreImagePaths = false;

//--When entities are autofiring, this cooldown makes sure they don't all fire on the same tick and hang the game
//  if a whole bunch are in the same area. Typically only the first actor in range fires their script.
int TilemapActor::xAutofireCooldown = 0;

//--When an enemy gets mugged, this is set to true and the enemy script is called. The script should
//  return how much cash/xp/jp/items the enemy had.
//--There are slots for up to
bool TilemapActor::xIsMugCheck = false;
int TilemapActor::xMugPlatina = 0;
int TilemapActor::xMugExperience = 0;
int TilemapActor::xMugJobPoints = 0;
SugarLinkedList *TilemapActor::xMugItemList = new SugarLinkedList(false);

//====================================== Property Queries =========================================
bool TilemapActor::CanHandleInstructions()
{
    //--We can only handle one instruction per tick, so if multiple instructions are queued this will
    //  return false on subsequent instructions.
    return (mLastInstructionHandle < Global::Shared()->gTicksElapsed);
}
int TilemapActor::GetX()
{
    return mTileX;
}
int TilemapActor::GetY()
{
    return mTileY;
}
float TilemapActor::GetWorldX()
{
    return mTrueX + mTempOffsetX;
}
float TilemapActor::GetWorldY()
{
    return mTrueY + mTempOffsetY;
}
float TilemapActor::GetRemainderX()
{
    return mRemainderX;
}
float TilemapActor::GetRemainderY()
{
    return mRemainderY;
}
int TilemapActor::GetFacing()
{
    return mFacing;
}
int TilemapActor::GetMoveTimer()
{
    return mMoveTimer;
}
bool TilemapActor::IsHandlingInstruction()
{
    if(mCurrentInstruction == TA_INS_NONE) return false;
    return true;
}
float TilemapActor::GetMoveSpeed()
{
    return mMoveSpeed;
}
float TilemapActor::GetFrameSpeed()
{
    return mWalkTicksPerFrame;
}
int TilemapActor::GetCollisionDepth()
{
    return mCollisionDepth;
}
bool TilemapActor::IsPositionClipped(int pX, int pY)
{
    //--Replies whether or not this entity is blocking this position. Some entities never clip the
    //  player while others reply to multiple positions.
    if(!mIsClipped) return false;
    return (pX >= mTrueX && pX <= mTrueX + TA_SIZE && pY >= mTrueY && pY <= mTrueY + TA_SIZE);
}
bool TilemapActor::IsPositionClippedOverride(int pX, int pY)
{
    //--Strict interpretation of IsPositionClipped. Ignores the mIsClipped flag and just asks for a collision check.
    return (pX >= mTrueX && pX <= mTrueX + TA_SIZE && pY >= mTrueY && pY <= mTrueY + TA_SIZE);
}
float TilemapActor::GetMovementLastTick()
{
    return mAmountMovedLastTick;
}
bool TilemapActor::IsPartyEntity()
{
    return mIsPartyEntity;
}
float TilemapActor::GetViewDistance()
{
    return mViewDistance;
}

//========================================= Manipulators ==========================================
void TilemapActor::SetDisable(bool pIsDisabled)
{
    mIsDisabled = pIsDisabled;
}
void TilemapActor::SetRendersAfterTiles(bool pFlag)
{
    mRendersAfterTiles = pFlag;
}
void TilemapActor::SetIgnoreDepthOverride(bool pFlag)
{
    mDoesntResetDepthOverride = pFlag;
}
void TilemapActor::FlagHandledInstruction()
{
    mLastInstructionHandle = Global::Shared()->gTicksElapsed;
}
void TilemapActor::SetRunning(bool pFlag)
{
    mIsRunning = pFlag;
}
void TilemapActor::SetCollisionFlag(bool pFlag)
{
    mIsClipped = pFlag;
}
void TilemapActor::SetPosition(float pX, float pY)
{
    mTileX = (int)pX;
    mTileY = (int)pY;
    mTrueX = (pX * TileLayer::cxSizePerTile) + TRUE_OFF_X;
    mTrueY = (pY * TileLayer::cxSizePerTile) + TRUE_OFF_Y;
    if(!mDoesntResetDepthOverride) mOverrideDepth = -2.0f; //In party follower cases, this gets reset shortly after.
}
void TilemapActor::SetPositionByPixel(float pX, float pY)
{
    mTileX = (int)pX / TileLayer::cxSizePerTile;
    mTileY = (int)pY / TileLayer::cxSizePerTile;
    mTrueX = pX;
    mTrueY = pY;
    if(!mDoesntResetDepthOverride) mOverrideDepth = -2.0f; //In party follower cases, this gets reset shortly after.
}
void TilemapActor::SetPositionByEntity(const char *pEntityName)
{
    //--Positions by the given entity's position as opposed to fixed coordinates.
    if(!pEntityName) return;

    //--Setup.
    float sX, sY;
    AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
    if(!rActiveLevel) return;

    //--Level provides coordinates.
    rActiveLevel->GetLocationByName(pEntityName, sX, sY);
    SetPositionByPixel(sX + (TileLayer::cxSizePerTile * 0.25f), sY - (TileLayer::cxSizePerTile * 0.50f));
}
void TilemapActor::SetRemainders(float pX, float pY)
{
    mRemainderX = pX;
    mRemainderY = pY;
}
void TilemapActor::SetOffsets(float pXOffset, float pYOffset)
{
    //--Defaults are -8.0, -16.0
    mOffsetX = pXOffset;
    mOffsetY = pYOffset;
}
void TilemapActor::SetMoveImage(int pDirection, int pSlot, const char *pDLPath)
{
    //--Error check, set.
    if(pDirection < 0 || pDirection >= TA_DIR_TOTAL) return;
    if(pSlot      < 0 || pSlot      >= TA_MOVE_IMG_TOTAL) return;
    rMoveImages[pDirection][pSlot] = (SugarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath);

    //--If the image fails to resolve, and we're storing image paths, store the path. Duh.
    if(mIsStoringImagePaths && !rMoveImages[pDirection][pSlot] && mImagePathStorage)
    {
        strncpy(mImagePathStorage->mMoveImages[pDirection][pSlot], pDLPath, TA_DL_PATH_LEN);
    }
}
void TilemapActor::SetRunImage(int pDirection, int pSlot, const char *pDLPath)
{
    //--Error check, set.
    if(pDirection < 0 || pDirection >= TA_DIR_TOTAL) return;
    if(pSlot      < 0 || pSlot      >= TA_MOVE_IMG_TOTAL) return;
    rRunImages[pDirection][pSlot] = (SugarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath);

    //--If the image fails to resolve, and we're storing image paths, store the path. Duh.
    if(mIsStoringImagePaths && !rRunImages[pDirection][pSlot] && mImagePathStorage)
    {
        strncpy(mImagePathStorage->mRunImages[pDirection][pSlot], pDLPath, TA_DL_PATH_LEN);
    }
}
void TilemapActor::SetShadow(const char *pPath)
{
    rShadowImg = (SugarBitmap *)DataLibrary::Fetch()->GetEntry(pPath);
}
void TilemapActor::SetMoveSpeed(float pValue)
{
    mMoveSpeed = pValue;
    if(mMoveSpeed < 0.10f) mMoveSpeed = 0.10f;
}
void TilemapActor::SetFrameSpeed(float pValue)
{
    mWalkTicksPerFrame = pValue;
    if(mWalkTicksPerFrame < 1.0f) mWalkTicksPerFrame = 1.0f;
}
void TilemapActor::SetActivationScript(const char *pPath)
{
    //--Note: Implicitly activates activation if the script is not null.
    if(pPath && !strcasecmp(pPath, "Null"))
    {
        ResetString(mActivationScript, NULL);
        return;
    }

    //--Normal case.
    ResetString(mActivationScript, pPath);
}
void TilemapActor::SetExtendedActivationDirection(int pValue)
{
    mExtendedActivationDirection = pValue;
}
void TilemapActor::SetFacing(int pDirection)
{
    //--Note: If out of range, facing is not affected.
    if(pDirection < 0 || pDirection >= TA_DIR_TOTAL) return;
    mFacing = pDirection;
    mCurrentViewAngle = (mFacing * 45.0f) - 90.0f;
}
void TilemapActor::SetFacingToNPC(const char *pName)
{
    //--Looks at the NPC with the provided name. If they don't exist, does nothing.
    RootEntity *rCheckActor = EntityManager::Fetch()->GetEntity(pName);
    if(!rCheckActor || !rCheckActor->IsOfType(POINTER_TYPE_TILEMAPACTOR)) return;

    //--Right type, face this entity.
    TilemapActor *rTilemapActor = (TilemapActor *)rCheckActor;
    int tTargetX = rTilemapActor->GetWorldX();
    int tTargetY = rTilemapActor->GetWorldY();
    SetFacingToPoint(tTargetX, tTargetY);
}
void TilemapActor::SetFacingToPoint(int pX, int pY)
{
    //--Faces a position. Does nothing if that position happens to be our world position.
    if(mTrueX == pX && mTrueY == pY) return;

    //--Set.
    mFacing = GetBestFacing(mTrueX, mTrueY, pX, pY);
    mCurrentViewAngle = (mFacing * 45.0f) - 90.0f;
}
void TilemapActor::ForceMoving(int pOverride)
{
    //--Overrides the move timer with the given value. Can stop movement if it's -1.
    if(pOverride == -1) { StopMoving(); return; }
    mIsMoving = true;
    mMoveTimer = pOverride;
    mLastMoveTick = 0;
}
void TilemapActor::StopMoving()
{
    if(!mAutoAnimates)
    {
        mIsMoving = false;
        mMoveTimer = -1;
    }
    mRemainderX = 0.0f;
    mRemainderY = 0.0f;
}
void TilemapActor::BeginDying()
{
    //--What a world, what a world!
    mIsDying = true;
    mDeathTimer = 0;
}
void TilemapActor::ActivateFlashwhite()
{
    //--Flash the sprite to white, then drop the special frame and flash back down.
    mIsShowingFlashwhiteSequence = true;
    mFlashwhiteTimer = 0;

    //--If not using a special frame already, use whatever frame is currently animating.
    if(!rCurrentSpecialImage) rCurrentSpecialImage = ResolveFrame();

    //--This flag must be set *after* ResolveFrame() is called, otherwise it'd just return the
    //  special image (when we want the base image).
    mIsShowingSpecialImage = true;
}
void TilemapActor::SetSpecialIdleFlag(bool pFlag)
{
    mUsesSpecialIdleImages = pFlag;
}
void TilemapActor::SetTinyFlag(bool pFlag)
{
    mIsTiny = pFlag;
}
void TilemapActor::SetAutoAnimateFlag(bool pFlag)
{
    mAutoAnimates = pFlag;
    if(mAutoAnimates)
    {
        mMoveTimer = (rand() % TA_MOVE_IMG_TOTAL) * mWalkTicksPerFrame;
    }
}
void TilemapActor::SetAutoAnimateFastFlag(bool pFlag)
{
    //--Forces the entity to update the move timer every tick. Without this flag it's once every 3 ticks.
    mAutoAnimatesFast = pFlag;
    mMoveTimer = 0;
}
void TilemapActor::SetOscillateFlag(bool pFlag)
{
    mYOscillates = pFlag;
    if(mYOscillates) mYOscillateTimer = rand() % 100;
}
void TilemapActor::SetCollisionDepth(int pValue)
{
    //--Note: Should only be used during the loading sequence, otherwise, don't set this manually!
    mCollisionDepth = pValue;
}
void TilemapActor::SetOverrideDepth(float pValue)
{
    //--Note: -2.0f is the "do nothing" value. Normal ranges are -1.0f to 0.0f.
    if(!mDoesntResetDepthOverride) mOverrideDepth = pValue;
}
void TilemapActor::SetMoveTimer(int pTimer)
{
    mMoveTimer = pTimer;
}
void TilemapActor::SetAutoDespawn(bool pFlag)
{
    mAutoDespawnAfterAnimation = pFlag;
}
void TilemapActor::SetIgnoreSpecialLights(bool pFlag)
{
    mIgnoreSpecialLighting = pFlag;
}
void TilemapActor::SetIgnoreClipsWhenMoving(bool pFlag)
{
    mIgnoreCollisionsWhenMoving = pFlag;
}
void TilemapActor::SetPartyEntityFlag(bool pFlag)
{
    mIsPartyEntity = pFlag;
}
void TilemapActor::SetNoFootstepSounds(bool pFlag)
{
    mNoFootstepSounds = pFlag;
}
void TilemapActor::DisableAutomaticShadow(bool pFlag)
{
    mNoAutomaticShadow = pFlag;
}
void TilemapActor::SetDontRefaceFlag(bool pFlag)
{
    mNeverRefaceOnNode = pFlag;
}
void TilemapActor::SetNegativeMoveTimer(int pTicks)
{
    mNegativeMoveTimer = pTicks;
}
void TilemapActor::SetAutofire(float pRange)
{
    //--Set autofire to a range lower than 1.0f to deactivate it.
    if(pRange < 1.0f)
    {
        mAutoActivates = false;
        mAutoActivateRange = 0.0f;
    }
    else
    {
        mAutoActivates = true;
        mAutoActivateRange = pRange;
    }
}

//========================================= Core Methods ==========================================
bool TilemapActor::HandleActivation(int pX, int pY)
{
    //--If this Actor happens to do anything when activated, this will do that. The coordinates passed
    //  in are where the activation occurred.
    //--Returns true if the Actor activated something in a fashion that would stop the rest of the activation
    //  update, false otherwise.
    if(mIsEnemy) return HandleEnemyActivation(pX, pY);
    if(!mActivationScript) return false;
    if(mIsDisabled) return false;

    //--Setup.
    const float cHalfSize = TA_SIZE * 0.5f;

    //--Compute the position coordinates. It can be extended with a flag.
    float tLft = mTrueX - cHalfSize;
    float tTop = mTrueY - cHalfSize;
    float tRgt = mTrueX + TA_SIZE + cHalfSize;
    float tBot = mTrueY + TA_SIZE + cHalfSize;

    //--Extended direction: These handle cases where an NPC is behind a counter and can be talked to.
    //  First, the null case.
    if(mExtendedActivationDirection == -1)
    {

    }
    //--North.
    else if(mExtendedActivationDirection == TA_DIR_NORTH)
    {
        tTop = tTop - TileLayer::cxSizePerTile;
    }
    //--East.
    else if(mExtendedActivationDirection == TA_DIR_EAST)
    {
        tRgt = tRgt + TileLayer::cxSizePerTile;
    }
    //--South.
    else if(mExtendedActivationDirection == TA_DIR_SOUTH)
    {
        tBot = tBot + TileLayer::cxSizePerTile;
    }
    //--West.
    else if(mExtendedActivationDirection == TA_DIR_WEST)
    {
        tLft = tLft - TileLayer::cxSizePerTile;
    }

    //--Check position. If necessary, run the "Hello" script.
    if(IsPointWithin(pX, pY, tLft, tTop, tRgt, tBot))
    {
        LuaManager::Fetch()->PushExecPop(this, mActivationScript, 1, "S", "Hello");
        return true;
    }

    //--All checks failed.
    return false;
}
bool TilemapActor::HandleAutoActivation(int pX, int pY)
{
    //--Handles auto-activation based on range.
    if(!mAutoActivates) return false;

    //--Check range to this actor.
    if(GetPlanarDistance(pX, pY, mTrueX, mTrueY) < mAutoActivateRange)
    {
        xAutofireCooldown = 1;
        LuaManager::Fetch()->PushExecPop(this, mActivationScript, 1, "S", "Auto");
        return true;
    }

    //--Checks failed.
    return false;
}
void TilemapActor::AddSpecialFrame(const char *pName, const char *pDLPath)
{
    //--Adds a special frame, doesn't care about duplication.
    SugarBitmap *rImage = (SugarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath);
    mSpecialImageList->AddElementAsTail(pName, rImage);

    //--If the image fails to resolve, and we're storing image paths, store the path. Duh.
    if(mIsStoringImagePaths && !rImage && mImagePathStorage)
    {
        mImagePathStorage->mSpecialImageList->AddElementAsTail(pName, InitializeString(pDLPath), &FreeThis);
    }
}
void TilemapActor::ActivateSpecialFrame(const char *pName)
{
    //--Sets a special frame as the current one. If NULL is passed in, or "NULL", then special frames are deactivated.
    if(!pName || !strcasecmp(pName, "NULL"))
    {
        mIsShowingSpecialImage = false;
        rCurrentSpecialImage = NULL;
        return;
    }
    rCurrentSpecialImage = (SugarBitmap *)mSpecialImageList->GetElementByName(pName);
    mIsShowingSpecialImage = (rCurrentSpecialImage != NULL);
}
void TilemapActor::WipeSpecialFrames()
{
    mSpecialImageList->ClearList();
    if(mIsStoringImagePaths && mImagePathStorage)
    {
        mImagePathStorage->mSpecialImageList->ClearList();
    }
}
void TilemapActor::SetReserveFrame(const char *pName)
{
    ResetString(mReserveSpecialImage, pName);
}
char *TilemapActor::GetFootstepSFX(bool pOverrideNoFootstep)
{
    //--Resolve which footstep sound to use when running. Can legally return NULL.
    if(xMaxFootstepSounds < 1 || mRunTicksPerFrame == 0) return NULL;

    //--If this entity has a flag set to ignore footsteps, the override flag can bypass it.
    if(mNoFootstepSounds && !pOverrideNoFootstep) return NULL;

    //--Check which frame we're on. The footstep sound only plays on the first tick of the run
    //  frames where the foot leaves the ground.
    int tUseTimer = mMoveTimer;
    if(tUseTimer < 0) tUseTimer = 0;
    int tFrame = ((int)(tUseTimer / mRunTicksPerFrame) % TA_MOVE_IMG_TOTAL);
    if(tFrame == 0 || tFrame == 2) return NULL;
    if(tUseTimer % (int)mRunTicksPerFrame != 0) return NULL;

    //--Variables.
    int tRollNumber = (int)(rand() % xMaxFootstepSounds);
    char tLetter = 'L';

    //--Last step was left:
    if(mWasLastFootstepLeft)
    {
        //--Flags.
        mWasLastFootstepLeft = false;
        tLetter = 'R';

        //--If the footstep roll is the same value, modify until it isn't.
        if(mPreviousFootstepRollR == tRollNumber && xMaxFootstepSounds >= 2)
        {
            tRollNumber = ((tRollNumber + 1) % xMaxFootstepSounds);
        }

        //--Store.
        mPreviousFootstepRollR = tRollNumber;
    }
    //--Last step was right:
    else
    {
        //--Flags.
        tLetter = 'L';
        mWasLastFootstepLeft = true;

        //--If the footstep roll is the same value, modify until it isn't.
        if(mPreviousFootstepRollL == tRollNumber && xMaxFootstepSounds >= 2)
        {
            tRollNumber = ((tRollNumber + 1) % xMaxFootstepSounds);
        }

        //--Store.
        mPreviousFootstepRollL = tRollNumber;
    }

    //--Print into the footstep buffer and return it.
    sprintf(mFootstepBuffer, "World|Footstep%c_%02i", tLetter, tRollNumber);
    return mFootstepBuffer;
}
void TilemapActor::PrintSpecialFrames()
{
    //--Prints all special frames this entity has to the console. Used for debug.
    fprintf(stderr, "Printing special frames for %s\n", mLocalName);
    void *rFramePtr = mSpecialImageList->PushIterator();
    while(rFramePtr)
    {
        const char *rName = mSpecialImageList->GetIteratorName();
        fprintf(stderr, " %s %p\n", rName, rFramePtr);
        rFramePtr = mSpecialImageList->AutoIterate();
    }
}
void TilemapActor::ReresolveLocalImages()
{
    //--Orders the TilemapActor to attempt to find all the images it previously failed to find.
    //  These images have their paths stored in a structure designed for this purpose.
    if(!mIsStoringImagePaths || !mImagePathStorage) return;

    //--Fast-access pointers.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();

    //--Reresolve.
    for(int x = 0; x < TA_DIR_TOTAL; x ++)
    {
        for(int y = 0; y < TA_MOVE_IMG_TOTAL; y ++)
        {
            //--Movement image.
            if(mImagePathStorage->mMoveImages[x][y][0] != '\0')
            {
                rMoveImages[x][y] = (SugarBitmap *)rDataLibrary->GetEntry(mImagePathStorage->mMoveImages[x][y]);
            }

            //--Running image.
            if(mImagePathStorage->mRunImages[x][y][0] != '\0')
            {
                rRunImages[x][y]  = (SugarBitmap *)rDataLibrary->GetEntry(mImagePathStorage->mRunImages[x][y]);
            }
        }
    }

    //--Special image paths.
    const char *rPath = (const char *)mImagePathStorage->mSpecialImageList->PushIterator();
    while(rPath)
    {
        //--Create.
        mSpecialImageList->AddElementAsTail(mImagePathStorage->mSpecialImageList->GetIteratorName(), rDataLibrary->GetEntry(rPath));

        //--Next.
        rPath = (const char *)mImagePathStorage->mSpecialImageList->AutoIterate();
    }

    //--Clean.
    mIsStoringImagePaths = false;
    mImagePathStorage->Delete();
    free(mImagePathStorage);
    mImagePathStorage = NULL;
}
ActorNotice *TilemapActor::SpawnNotice(const char *pText)
{
    //--Creates and returns an ActorNotice positioned just above the TilemapActor's head. It is
    //  returned, and also registered with the AdventureLevel.
    if(!pText) return NULL;

    //--Fetch the level.
    AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
    if(!rActiveLevel) return NULL;

    //--Position. Center the X position.
    float tXPosition = mTrueX + mOffsetX - TRUE_OFF_X + 16.0f;
    float tYPosition = mTrueY + mOffsetY - TRUE_OFF_Y;

    //--Create.
    ActorNotice *nNotice = new ActorNotice();
    nNotice->SetTimerMax(TA_DEFAULT_NOTICE_TICKS);
    nNotice->SetText(pText);
    nNotice->MoveTo(tXPosition, tYPosition, 0);

    //--Register it to the AdventureLevel.
    rActiveLevel->RegisterNotice(nNotice);

    //--Pass it back.
    return nNotice;
}

//===================================== Private Core Methods ======================================
void TilemapActor::ResolveActivationDimensions()
{
    //--Sets the activation position based on the current facing.
    float tAngleRad = ((mFacing * 45.0f) - 90.0f) * 3.1415926f / 180.0f;

    //--Resolve the position, that is, the center of the activation square.
    float tXPosition = mTrueX + (TA_SIZE * 0.5f) + (cosf(tAngleRad) * TA_EXAMINE_DIST);
    float tYPosition = mTrueY + (TA_SIZE * 0.5f) + (sinf(tAngleRad) * TA_EXAMINE_DIST);

    //--Now position the box around this point.
    mActivationDim.SetWH(tXPosition - (TA_EXAMINE_DIM * 0.5f), tYPosition - (TA_EXAMINE_DIM * 0.5f), TA_EXAMINE_DIM, TA_EXAMINE_DIM);
}

//============================================ Update =============================================
void TilemapActor::Update()
{
    //--Disabled entities don't update.
    if(mIsDisabled) return;

    //--This timer always updates.
    if(mLastComputedReinforcement > 0 || mStunTimer == 45) mReinforcementTimer ++;

    //--If auto-animate is true, then this timer always runs if the character is not running.
    if((mAutoAnimates || mAutoAnimatesFast) && !mIsRunning)
    {
        //--Negative move timer. Entity does nothing until it zeroes.
        if(mNegativeMoveTimer > 0)
        {
            mNegativeMoveTimer --;
            return;
        }

        //--Fast animation, 1 tick per frame.
        if(mAutoAnimatesFast)
        {
            mIsMoving = true;
            mMoveTimer ++;
            //fprintf(stderr, "%s %i - %f\n", mLocalName, mMoveTimer, mWalkTicksPerFrame);
        }
        //--Normal, slow case. Most entities use this.
        else if(Global::Shared()->gTicksElapsed % 3 == 0)
        {
            mIsMoving = true;
            mMoveTimer ++;
        }

        //--Auto-despawn check.
        if(mAutoDespawnAfterAnimation && mMoveTimer >= mWalkTicksPerFrame * TA_MOVE_IMG_TOTAL)
        {
            mSelfDestruct = true;
        }
    }
    //--Otherwise, if we were moving in the last 3 ticks, increment.
    else if(mLastMoveTick < 3)
    {
        mLastMoveTick ++;
    }
    //--If we didn't move for 3 ticks, stop moving and reset the timers.
    else
    {
        if(!mAutoAnimates)
        {
            mIsMoving = false;
            mMoveTimer = 0;
        }
    }

    //--Flashwhite sequence.
    if(mIsShowingFlashwhiteSequence)
    {
        //--Timer.
        mFlashwhiteTimer ++;

        //--At this mark, disable the special frame. If there is a reserve frame, use that.
        if(mFlashwhiteTimer == FLASHWHITE_TICKS_UP + (FLASHWHITE_TICKS_HOLD / 2))
        {
            ActivateSpecialFrame(mReserveSpecialImage);
            ResetString(mReserveSpecialImage, NULL);
        }

        //--Ending case.
        if(mFlashwhiteTimer >= FLASHWHITE_TICKS_TOTAL)
        {
            mIsShowingFlashwhiteSequence = false;
        }
    }
    //--If oscillating, do that. This is exclusive with the flashwhite sequence.
    else if(mYOscillates)
    {
        mYOscillateTimer ++;
    }

    //--Never update during combat.
    if(AdvCombat::Fetch()->IsActive()) return;

    //--Enemy AI. Overrides all other priorities.
    if(mIsEnemy)
    {
        UpdateEnemyMode();
        return;
    }
}
bool TilemapActor::HandlePlayerControls(bool pAllowRunning)
{
    //--[Documentation]
    //--Entities move on a per-pixel basis. This handles that. Returns true if any movement controls were
    //  pressed, false on error or no movement keys pressed.
    ControlManager *rControlManager = ControlManager::Fetch();
    AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
    if(!rActiveLevel) return false;

    //--Reset the movement amount.
    mAmountMovedLastTick = 0.0f;

    //--[Activation Handling]
    if(rControlManager->IsFirstPress("Activate"))
    {
        //--List of activation points. The center is always the zeroth one.
        Point3D tActivationPoints[5];
        tActivationPoints[0].Set(mActivationDim.mXCenter, mActivationDim.mYCenter, 0.0f);
        tActivationPoints[1].Set(mActivationDim.mLft,     mActivationDim.mTop,     0.0f);
        tActivationPoints[2].Set(mActivationDim.mLft,     mActivationDim.mBot,     0.0f);
        tActivationPoints[3].Set(mActivationDim.mRgt,     mActivationDim.mTop,     0.0f);
        tActivationPoints[4].Set(mActivationDim.mRgt,     mActivationDim.mBot,     0.0f);
        for(int i = 0; i < 5; i ++)
        {
            if(rActiveLevel->ActivateAt(tActivationPoints[i].mX, tActivationPoints[i].mY))
            {
                return false;
            }
        }
    }

    //--[Run Toggling]
    //--Does not count as a control, sets the run state in the character.
    if(rControlManager->IsDown("Run") && pAllowRunning)
    {
        SetRunning(true);
    }
    else
    {
        SetRunning(false);
    }

    //--[Directional Keys]
    //--Store the keys.
    bool tIsLftPressed = rControlManager->IsDown("Left");
    bool tIsRgtPressed = rControlManager->IsDown("Right");
    bool tIsTopPressed = rControlManager->IsDown("Up");
    bool tIsBotPressed = rControlManager->IsDown("Down");
    EmulateMovement(tIsLftPressed, tIsTopPressed, tIsRgtPressed, tIsBotPressed);

    //--[Climbing Check]
    mAlwaysFacesUp = rActiveLevel->IsInClimbableZone(mTrueX + TA_SIZE * 0.5f, mTrueY + TA_SIZE * 0.5f);

    //--Return true if any of the movement keys were pressed. This is for following entities, it does
    //  not affect this entity.
    return (tIsLftPressed || tIsRgtPressed || tIsTopPressed || tIsBotPressed);
}

//=========================================== File I/O ============================================
//=========================================== Drawing =============================================
//======================================= Pointer Routing =========================================
SugarBitmap *TilemapActor::GetMoveImage(int pDirection, int pSlot)
{
    if(pDirection < 0 || pDirection >= TA_DIR_TOTAL)      return NULL;
    if(pSlot      < 0 || pSlot      >= TA_MOVE_IMG_TOTAL) return NULL;
    return rMoveImages[pDirection][pSlot];
}
SugarBitmap *TilemapActor::GetRunImage(int pDirection, int pSlot)
{
    if(pDirection < 0 || pDirection >= TA_DIR_TOTAL)      return NULL;
    if(pSlot      < 0 || pSlot      >= TA_MOVE_IMG_TOTAL) return NULL;
    return rRunImages[pDirection][pSlot];
}
SugarBitmap *TilemapActor::GetShadowImage()
{
    return rShadowImg;
}

//=========================================== File I/O ============================================
//====================================== Static Functions =========================================
int TilemapActor::GetBestFacing(float pSubjectX, float pSubjectY, float pTargetX, float pTargetY)
{
    //--Static function that attempts to resolve the best "facing" angle. The two points can be
    //  anywhere in relation to one another, but will be condensed into 8 possible facings.
    //--If the two positions are identical then "North" defaults.
    if(pSubjectX == pTargetX && pSubjectY == pTargetY) return TA_DIR_NORTH;

    //--Compute angle.
    float tAngle = atan2f(pTargetY - pSubjectY, pTargetX - pSubjectX);

    //--West. Remember this is from -pi to +pi.
    if(tAngle <= 3.1415926f * -0.875f || tAngle >= 3.1415926f * 0.875f)
    {
        return TA_DIR_WEST;
    }
    //--Northwest. Still in the negative quadrant.
    else if(tAngle < 3.1415926f * -0.625f)
    {
        return TA_DIR_NW;
    }
    //--North.
    else if(tAngle < 3.1415926f * -0.375f)
    {
        return TA_DIR_NORTH;
    }
    //--Northeast.
    else if(tAngle < 3.1415926f * -0.125f)
    {
        return TA_DIR_NE;
    }
    //--Southwest. Back on the positives.
    else if(tAngle > 3.1415926f * 0.625f)
    {
        return TA_DIR_SW;
    }
    //--South.
    else if(tAngle > 3.1415926f * 0.375f)
    {
        return TA_DIR_SOUTH;
    }
    //--Southeast.
    else if(tAngle > 3.1415926f * 0.125f)
    {
        return TA_DIR_SE;
    }
    //--Close enough to zero to be east.
    else
    {
        return TA_DIR_EAST;
    }
}
void TilemapActor::ReresolveAllImages()
{
    //--When a TilemapActor is created and loads its images, in some circumstances the images have
    //  not been loaded yet. When flagged to, the TilemapActor can store the paths to those images.
    //  Once image loading is done, this function will scan every TilemapActor that exists and order
    //  them to re-resolve all their missing images.
    //--If the image is found the first time, the image does not re-resolve. This does nothing if
    //  the re-resolve flag is off. You should probably turn the flag off when it is completed.
    if(!xShouldStoreImagePaths) return;

    //--Run through the EntityManager where all actors are stored.
    SugarLinkedList *rEntityList = EntityManager::Fetch()->GetEntityList();
    if(!rEntityList) return;

    //--Iterate.
    RootEntity *rEntity = (RootEntity *)rEntityList->PushIterator();
    while(rEntity)
    {
        //--Entity is not a TilemapActor? Ignore it.
        if(!rEntity->IsOfType(POINTER_TYPE_TILEMAPACTOR))
        {
            rEntity = (RootEntity *)rEntityList->AutoIterate();
            continue;
        }

        //--Call.
        TilemapActor *rAsActor = (TilemapActor *)rEntity;
        rAsActor->ReresolveLocalImages();

        //--Next.
        rEntity = (RootEntity *)rEntityList->AutoIterate();
    }
}

//========================================= Lua Hooking ===========================================
void TilemapActor::HookToLuaState(lua_State *pLuaState)
{
    /* TA_Create(sName)
       Creates a new TilemapActor, registers it, and pushes it on the Activity Stack. */
    lua_register(pLuaState, "TA_Create", &Hook_TA_Create);

    /* TA_CreateUsingPosition(sName, sPositionName)
       Creates a new TilemapActor, registers it, and pushes it on the Activity Stack. If the corresponding
       position exists in the active map, uses its properties to perform setup. */
    lua_register(pLuaState, "TA_CreateUsingPosition", &Hook_TA_CreateUsingPosition);

    /* TA_GetProperty("Is Mugging Check") (1 Boolean) (Static)
       TA_GetProperty("ID") (1 Integer)
       TA_GetProperty("Name") (1 String)
       TA_GetProperty("Position") (2 Floats)
       TA_GetProperty("Facing") (1 Integer)
       TA_GetProperty("Print Special Frames") (No Returns)
       Returns the named property in the active TilemapActor. */
    lua_register(pLuaState, "TA_GetProperty", &Hook_TA_GetProperty);

    /* TA_SetProperty("Enemy Path", sPath) (Static)
       TA_SetProperty("Standard Enemy Shadow", sDLPath) (Static)
       TA_SetProperty("Store Image Paths", bFlag) (Static)
       TA_SetProperty("Reresolve All Images") (Static)
       TA_SetProperty("Mug Platina", iPlatina) (Static)
       TA_SetProperty("Mug Experience", iExperience) (Static)
       TA_SetProperty("Mug Job Points", iJobPoints) (Static)
       TA_SetProperty("Mug Item", sItemName) (Static)
       TA_SetProperty("Disabled", bIsDisabled)
       TA_SetProperty("Name", sName)
       TA_SetProperty("Position", iX, iY)
       TA_SetProperty("Position By Entity", sEntityName)
       TA_SetProperty("Depth", iDepth)
       TA_SetProperty("Rendering Depth", fDepth)
       TA_SetProperty("Renders After Tiles", bFlag)
       TA_SetProperty("Clipping Flag", bFlag)
       TA_SetProperty("Activation Script", sScriptPath)
       TA_SetProperty("Extended Activation Direction", iDirectionFlag)
       TA_SetProperty("Move Frame", iDirection, iSlot, sPath)
       TA_SetProperty("Run Frame", iDirection, iSlot, sPath)
       TA_SetProperty("Rendering Offsets", fOffsetX, fOffsetY)
       TA_SetProperty("Shadow", sDLPath)
       TA_SetProperty("Facing", iDirection)
       TA_SetProperty("Face Position", iX, iY)
       TA_SetProperty("Face Character", sNPCName)
       TA_SetProperty("Stop Moving")
       TA_SetProperty("Add Special Frame", sRefName, sPath)
       TA_SetProperty("Set Special Frame", sRefName)
       TA_SetProperty("Wipe Special Frames")
       TA_SetProperty("Tiny", bFlag)
       TA_SetProperty("Flashwhite")
       TA_SetProperty("Flashwhite", sReserveFrame)
       TA_SetProperty("Auto Animates", bFlag)
       TA_SetProperty("Auto Animates Fast", bFlag)
       TA_SetProperty("Walk Ticks Per Frame", iValue)
       TA_SetProperty("Y Oscillates", bFlag)
       TA_SetProperty("Activate Wander Mode")
       TA_SetProperty("Set Ignore World Stop", bFlag)
       TA_SetProperty("Set Ignore Special Lights", bFlag)
       TA_SetProperty("Set Ignore Clips", bFlag)
       TA_SetProperty("No Footstep Sound", bFlag)
       TA_SetProperty("No Automatic Shadow", bFlag)
       TA_SetProperty("Reface On Node Flag", bFlag)
       Sets the named property in the active TilemapActor. */
    lua_register(pLuaState, "TA_SetProperty", &Hook_TA_SetProperty);

    /* TA_ChangeCollisionFlag(sEntityName, bFlag)
       Quickly changes the collision flag for the requested entity. Saves the scripter from having
       to push/change/pop since this is done fairly routinely. */
    lua_register(pLuaState, "TA_ChangeCollisionFlag", &Hook_TA_ChangeCollisionFlag);
}

//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
int Hook_TA_Create(lua_State *L)
{
    //TA_Create(sName)
    DataLibrary::Fetch()->PushActiveEntity();

    //--Arg check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("TA_Create");

    //--Create, push, register.
    TilemapActor *nActor = new TilemapActor();
    nActor->SetName(lua_tostring(L, 1));
    EntityManager::Fetch()->RegisterPointer(lua_tostring(L, 1), nActor);
    DataLibrary::Fetch()->rActiveObject = nActor;
    return 0;
}
int Hook_TA_CreateUsingPosition(lua_State *L)
{
    //TA_CreateUsingPosition(sName, sPositionName)
    DataLibrary::Fetch()->PushActiveEntity();

    //--Arg check.
    int tArgs = lua_gettop(L);
    if(tArgs < 2) return LuaArgError("TA_CreateUsingPosition");

    //--Create, push, register.
    TilemapActor *nActor = new TilemapActor();
    nActor->SetName(lua_tostring(L, 1));
    EntityManager::Fetch()->RegisterPointer(lua_tostring(L, 1), nActor);
    DataLibrary::Fetch()->rActiveObject = nActor;

    //--Tell the active AdventureLevel to do the default setup.
    AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
    rActiveLevel->SetActorToPositionPack(nActor, lua_tostring(L, 2));

    return 0;
}
int Hook_TA_GetProperty(lua_State *L)
{
    //TA_GetProperty("Is Mugging Check") (1 Boolean) (Static)
    //TA_GetProperty("ID") (1 Integer)
    //TA_GetProperty("Name") (1 String)
    //TA_GetProperty("Position") (2 Floats)
    //TA_GetProperty("Facing") (1 Integer)
    //TA_GetProperty("Print Special Frames") (No Returns)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("TA_GetProperty");

    //--Switching.
    int tReturns = 0;
    const char *rSwitchType = lua_tostring(L, 1);

    //--[Static Types]
    if(!strcasecmp(rSwitchType, "Is Mugging Check") && tArgs == 1)
    {
        lua_pushboolean(L, TilemapActor::xIsMugCheck);
        return 1;
    }

    //--Active object.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    TilemapActor *rActiveActor = (TilemapActor *)rDataLibrary->rActiveObject;
    if(!rActiveActor || !rActiveActor->IsOfType(POINTER_TYPE_TILEMAPACTOR)) return LuaTypeError("TA_GetProperty");

    //--ID of the TilemapActor.
    if(!strcasecmp(rSwitchType, "ID") && tArgs == 1)
    {
        lua_pushinteger(L, rActiveActor->GetID());
        tReturns = 1;
    }
    //--Name of the actor.
    else if(!strcasecmp(rSwitchType, "Name") && tArgs == 1)
    {
        lua_pushstring(L, rActiveActor->GetName());
        tReturns = 1;
    }
    //--Position in world coordinates.
    else if(!strcasecmp(rSwitchType, "Position") && tArgs == 1)
    {
        lua_pushnumber(L, rActiveActor->GetWorldX());
        lua_pushnumber(L, rActiveActor->GetWorldY());
        tReturns = 2;
    }
    //--Facing.
    else if(!strcasecmp(rSwitchType, "Facing") && tArgs == 1)
    {
        lua_pushinteger(L, rActiveActor->GetFacing());
        tReturns = 1;
    }
    //--Prints the special frames this entity has to the console.
    else if(!strcasecmp(rSwitchType, "Print Special Frames") && tArgs == 1)
    {
        rActiveActor->PrintSpecialFrames();
        tReturns = 0;
    }

    //--Error.
    else
    {
        LuaPropertyError("TA_GetProperty", rSwitchType, tArgs);
    }

    return tReturns;
}
int Hook_TA_SetProperty(lua_State *L)
{
    //TA_SetProperty("Enemy Path", sPath) (Static)
    //TA_SetProperty("Standard Enemy Shadow", sDLPath) (Static)
    //TA_SetProperty("Store Image Paths", bFlag) (Static)
    //TA_SetProperty("Reresolve All Images") (Static)
    //TA_SetProperty("Mug Platina", iPlatina) (Static)
    //TA_SetProperty("Mug Experience", iExperience) (Static)
    //TA_SetProperty("Mug Job Points", iJobPoints) (Static)
    //TA_SetProperty("Mug Item", sItemName) (Static)
    //TA_SetProperty("Disabled", bIsDisabled)
    //TA_SetProperty("Name", sName)
    //TA_SetProperty("Position", iX, iY)
    //TA_SetProperty("Position By Entity", sEntityName)
    //TA_SetProperty("Depth", iDepth)
    //TA_SetProperty("Rendering Depth", fDepth)
    //TA_SetProperty("Renders After Tiles", bFlag)
    //TA_SetProperty("Clipping Flag", bFlag)
    //TA_SetProperty("Activation Script", sScriptPath)
    //TA_SetProperty("Extended Activation Direction", iDirectionFlag)
    //TA_SetProperty("Move Frame", iDirection, iSlot, sPath)
    //TA_SetProperty("Run Frame", iDirection, iSlot, sPath)
    //TA_SetProperty("Rendering Offsets", fOffsetX, fOffsetY)
    //TA_SetProperty("Shadow", sDLPath)
    //TA_SetProperty("Facing", iDirection)
    //TA_SetProperty("Face Position", iX, iY)
    //TA_SetProperty("Face Character", sNPCName)
    //TA_SetProperty("Stop Moving")
    //TA_SetProperty("Add Special Frame", sRefName, sPath)
    //TA_SetProperty("Set Special Frame", sRefName)
    //TA_SetProperty("Wipe Special Frames")
    //TA_SetProperty("Tiny", bFlag)
    //TA_SetProperty("Flashwhite")
    //TA_SetProperty("Flashwhite", sReserveFrame)
    //TA_SetProperty("Auto Animates", bFlag)
    //TA_SetProperty("Auto Animates Fast", bFlag)
    //TA_SetProperty("Walk Ticks Per Frame", iValue)
    //TA_SetProperty("Y Oscillates", bFlag)
    //TA_SetProperty("Activate Wander Mode")
    //TA_SetProperty("Set Ignore World Stop", bFlag)
    //TA_SetProperty("Set Ignore Special Lights", bFlag)
    //TA_SetProperty("Set Ignore Clips", bFlag)
    //TA_SetProperty("No Footstep Sound", bFlag)
    //TA_SetProperty("No Automatic Shadow", bFlag)
    //TA_SetProperty("Reface On Node Flag", bFlag)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("TA_SetProperty");

    //--Switching.
    const char *rSwitchType = lua_tostring(L, 1);

    //--[Static Types]
    //--Sets what shadow appears under enemies.
    if(!strcasecmp(rSwitchType, "Standard Enemy Shadow") && tArgs == 2)
    {
        ResetString(TilemapActor::xEnemyShadowPath, lua_tostring(L, 2));
        return 0;
    }
    //--If true, entities will store the DLPath they received when a NULL image is passed to them.
    //  Use "Reresolve All Images" once image loading completes to fix them.
    else if(!strcasecmp(rSwitchType, "Store Image Paths") && tArgs == 2)
    {
        TilemapActor::xShouldStoreImagePaths = lua_toboolean(L, 2);
        return 0;
    }
    //--If an entity received a path to an image that didn't exist because it wasn't loaded yet,
    //  this attempts to re-resolve the image(s) from the DataLibrary for ALL TilemapActors.
    else if(!strcasecmp(rSwitchType, "Reresolve All Images") && tArgs == 1)
    {
        TilemapActor::ReresolveAllImages();
        return 0;
    }
    //--Sets how much money the mugged enemy gave.
    else if(!strcasecmp(rSwitchType, "Mug Platina") && tArgs == 2)
    {
        TilemapActor::xMugPlatina = lua_tointeger(L, 2);
        return 0;
    }
    //--Sets how much experience the mugged enemy gave.
    else if(!strcasecmp(rSwitchType, "Mug Experience") && tArgs == 2)
    {
        TilemapActor::xMugExperience = lua_tointeger(L, 2);
        return 0;
    }
    //--Sets how much JP the mugged enemy gave.
    else if(!strcasecmp(rSwitchType, "Mug Job Points") && tArgs == 2)
    {
        TilemapActor::xMugJobPoints = lua_tointeger(L, 2);
        return 0;
    }
    //--Adds an item to the list of items dropped by the mugged enemy.
    else if(!strcasecmp(rSwitchType, "Mug Item") && tArgs == 2)
    {
        TilemapActor::xMugItemList->AddElement(lua_tostring(L, 2), &TilemapActor::xMugItemList);
        return 0;
    }
    //--[Instantiated Types]
    //--Active object is required to exist.
    TilemapActor *rActiveActor = (TilemapActor *)DataLibrary::Fetch()->rActiveObject;
    if(!rActiveActor || !rActiveActor->IsOfType(POINTER_TYPE_TILEMAPACTOR))
    {
        LuaTypeError("TA_SetProperty");
        fprintf(stderr, " Data: ");
        for(int i = 0; i < tArgs; i ++) fprintf(stderr, "%s ", lua_tostring(L, i+1));
        fprintf(stderr, "\n");
        return 0;
    }

    //--Disabled flag. Disabled entities still exist but don't update or render until re-enabled.
    if(!strcasecmp(rSwitchType, "Disabled") && tArgs == 2)
    {
        rActiveActor->SetDisable(lua_toboolean(L, 2));
    }
    //--Name. Also affects the EntityManager's copy.
    else if(!strcasecmp(rSwitchType, "Name") && tArgs == 2)
    {
        rActiveActor->SetName(lua_tostring(L, 2));
        EntityManager::Fetch()->RenamePointer(lua_tostring(L, 2), rActiveActor);
    }
    //--Position of Actor.
    else if(!strcasecmp(rSwitchType, "Position") && tArgs == 3)
    {
        rActiveActor->SetPosition(lua_tonumber(L, 2), lua_tonumber(L, 3));
    }
    //--Position of Actor, using another Entity as its base.
    else if(!strcasecmp(rSwitchType, "Position By Entity") && tArgs == 2)
    {
        rActiveActor->SetPositionByEntity(lua_tostring(L, 2));
    }
    //--Depth of the Actor.
    else if(!strcasecmp(rSwitchType, "Depth") && tArgs == 2)
    {
        rActiveActor->SetCollisionDepth(lua_tointeger(L, 2));
    }
    //--Rendering Depth of the Actor.
    else if(!strcasecmp(rSwitchType, "Rendering Depth") && tArgs == 2)
    {
        rActiveActor->SetOverrideDepth(lua_tonumber(L, 2));
        rActiveActor->SetIgnoreDepthOverride(true);
    }
    //--Sets whether the entity renders after tiles do, used for special transparencies.
    else if(!strcasecmp(rSwitchType, "Renders After Tiles") && tArgs == 2)
    {
        rActiveActor->SetRendersAfterTiles(lua_toboolean(L, 2));
    }
    //--Collision flag.
    else if(!strcasecmp(rSwitchType, "Clipping Flag") && tArgs == 2)
    {
        rActiveActor->SetCollisionFlag(lua_toboolean(L, 2));
    }
    //--Activation script.
    else if(!strcasecmp(rSwitchType, "Activation Script") && tArgs == 2)
    {
        rActiveActor->SetActivationScript(lua_tostring(L, 2));
    }
    //--Direction of extended activation, for counters. Cannot be a diagonal.
    else if(!strcasecmp(rSwitchType, "Extended Activation Direction") && tArgs == 2)
    {
        rActiveActor->SetExtendedActivationDirection(lua_tointeger(L, 2));
    }
    //--Movement frames.
    else if(!strcasecmp(rSwitchType, "Move Frame") && tArgs == 4)
    {
        rActiveActor->SetMoveImage(lua_tointeger(L, 2), lua_tointeger(L, 3), lua_tostring(L, 4));
    }
    //--Movement frames, running.
    else if(!strcasecmp(rSwitchType, "Run Frame") && tArgs == 4)
    {
        rActiveActor->SetRunImage(lua_tointeger(L, 2), lua_tointeger(L, 3), lua_tostring(L, 4));
    }
    //--Set rendering offsets. Defaults are -8.0, -16.0
    else if(!strcasecmp(rSwitchType, "Rendering Offsets") && tArgs == 3)
    {
        rActiveActor->SetOffsets(lua_tonumber(L, 2), lua_tonumber(L, 3));
    }
    //--Shadow. Set to "Null" to disable shadows.
    else if(!strcasecmp(rSwitchType, "Shadow") && tArgs == 2)
    {
        rActiveActor->SetShadow(lua_tostring(L, 2));
    }
    //--Manually set the facing direction.
    else if(!strcasecmp(rSwitchType, "Facing") && tArgs == 2)
    {
        rActiveActor->SetFacing(lua_tointeger(L, 2));
    }
    //--Set facing to a position.
    else if(!strcasecmp(rSwitchType, "Face Position") && tArgs == 3)
    {
        rActiveActor->SetFacingToPoint(lua_tointeger(L, 2), lua_tointeger(L, 3));
    }
    //--Set facing to an NPC.
    else if(!strcasecmp(rSwitchType, "Face Character") && tArgs == 2)
    {
        rActiveActor->SetFacingToNPC(lua_tostring(L, 2));
    }
    //--Stops movement animation. Useful for making cutscenes look servicable.
    else if(!strcasecmp(rSwitchType, "Stop Moving") && tArgs == 1)
    {
        rActiveActor->StopMoving();
    }
    //--Special frames. Per-character, used by cutscenes.
    else if(!strcasecmp(rSwitchType, "Add Special Frame") && tArgs == 3)
    {
        rActiveActor->AddSpecialFrame(lua_tostring(L, 2), lua_tostring(L, 3));
    }
    //--Activates a special frame previous created with "Add Special Frame".
    else if(!strcasecmp(rSwitchType, "Set Special Frame") && tArgs == 2)
    {
        rActiveActor->ActivateSpecialFrame(lua_tostring(L, 2));
    }
    //--Remove outstanding special frames.
    else if(!strcasecmp(rSwitchType, "Wipe Special Frames") && tArgs == 1)
    {
        rActiveActor->WipeSpecialFrames();
    }
    //--Set the tiny flag. Used for some enemies when they double as NPCs.
    else if(!strcasecmp(rSwitchType, "Tiny") && tArgs == 2)
    {
        rActiveActor->SetTinyFlag(lua_toboolean(L, 2));
    }
    //--Flash the character's sprite to white and then back down.
    else if(!strcasecmp(rSwitchType, "Flashwhite") && tArgs == 1)
    {
        rActiveActor->ActivateFlashwhite();
    }
    //--Flash the character's sprite to white and then back down, switching to a special frame partway through.
    else if(!strcasecmp(rSwitchType, "Flashwhite") && tArgs == 2)
    {
        rActiveActor->ActivateFlashwhite();
        rActiveActor->SetReserveFrame(lua_tostring(L, 2));
    }
    //--Entity's animation timer runs even if they're not moving.
    else if(!strcasecmp(rSwitchType, "Auto Animates") && tArgs == 2)
    {
        rActiveActor->SetAutoAnimateFlag(lua_toboolean(L, 2));
    }
    //--Entity's animation timer runs every tick instead of every 3 ticks.
    else if(!strcasecmp(rSwitchType, "Auto Animates Fast") && tArgs == 2)
    {
        rActiveActor->SetAutoAnimateFlag(lua_toboolean(L, 2));
        rActiveActor->SetAutoAnimateFastFlag(lua_toboolean(L, 2));
    }
    //--How many ticks are used in the walk cycle for each frame.
    else if(!strcasecmp(rSwitchType, "Walk Ticks Per Frame") && tArgs == 2)
    {
        rActiveActor->SetFrameSpeed(lua_tonumber(L, 2));
    }
    //--Entity's Y position oscillates up and down slightly.
    else if(!strcasecmp(rSwitchType, "Y Oscillates") && tArgs == 2)
    {
        rActiveActor->SetOscillateFlag(lua_toboolean(L, 2));
    }
    //--Causes this NPC to activate enemy mode, but to always ignore the player.
    else if(!strcasecmp(rSwitchType, "Activate Wander Mode") && tArgs == 1)
    {
        rActiveActor->ActivateWanderMode();
    }
    //--This NPC will ignore world stop due to cutscenes (and only cutscenes).
    else if(!strcasecmp(rSwitchType, "Set Ignore World Stop") && tArgs == 2)
    {
        rActiveActor->SetWorldStopIgnoreFlag(lua_toboolean(L, 2));
    }
    //--This NPC will not use the 2-tile tall lighting flag.
    else if(!strcasecmp(rSwitchType, "Set Ignore Special Lights") && tArgs == 2)
    {
        rActiveActor->SetIgnoreSpecialLights(lua_toboolean(L, 2));
    }
    //--This NPC will move through collisions if this flag is true.
    else if(!strcasecmp(rSwitchType, "Set Ignore Clips") && tArgs == 2)
    {
        rActiveActor->SetIgnoreClipsWhenMoving(lua_toboolean(L, 2));
    }
    //--NPC has no footstep sound when running.
    else if(!strcasecmp(rSwitchType, "No Footstep Sound") && tArgs == 2)
    {
        rActiveActor->SetNoFootstepSounds(lua_toboolean(L, 2));
    }
    //--NPC has no automatic shadow rendering.
    else if(!strcasecmp(rSwitchType, "No Automatic Shadow") && tArgs == 2)
    {
        rActiveActor->DisableAutomaticShadow(lua_toboolean(L, 2));
    }
    //--If true, NPC doesn't stop and reface when touching a patrol node.
    else if(!strcasecmp(rSwitchType, "Reface On Node Flag") && tArgs == 2)
    {
        rActiveActor->SetDontRefaceFlag(lua_toboolean(L, 2));
    }
    //--Error.
    else
    {
        LuaPropertyError("TA_SetProperty", rSwitchType, tArgs);
    }

    return 0;
}
int Hook_TA_ChangeCollisionFlag(lua_State *L)
{
    //TA_ChangeCollisionFlag(sEntityName, bFlag)
    int tArgs = lua_gettop(L);
    if(tArgs < 2) return LuaArgError("TA_ChangeCollisionFlag");

    //--Get the actor in question.
    TilemapActor *rActiveActor = (TilemapActor *)EntityManager::Fetch()->GetEntity(lua_tostring(L, 1));
    if(!rActiveActor || !rActiveActor->IsOfType(POINTER_TYPE_TILEMAPACTOR)) return LuaTypeError("TA_ChangeCollisionFlag");

    //--Set.
    rActiveActor->SetCollisionFlag(lua_toboolean(L, 2));
    return 0;
}
