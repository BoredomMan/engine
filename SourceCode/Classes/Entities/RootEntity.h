//--[RootEntity]
//--Inheriting from the RootObject, this is what occupies the EntityManager. Exactly what properties
//  an entity should have in the root is dependent on your program and what the 'world' requires.
//--By default, Entities are capable of deleting themselves, are nameable, are renderable, and
//  can have variable-sized data lists.
//--RootEntity inherits from IRenderable, which inherits from RootObject. This is just a polymorphic
//  formality to prevent cast-from-void errors.

#pragma once

//--[Inclues]
#include "Definitions.h"
#include "Structures.h"
#include "RootObject.h"
#include "INameable.h"
#include "IRenderable.h"

//--[Local Definitions]
//--[Local Structures]
//--[Class]
class RootEntity : public IRenderable
{
    private:

    protected:
    //--System
    //--Nameable
    char *mLocalName;

    //--Renderable
    float mDepth;

    //--Data Storage
    DataList *mDataList;

    public:
    //--System
    RootEntity();
    virtual ~RootEntity();

    //--Public Variables
    bool mSelfDestruct;

    //--Property Queries
    const char *GetName();
    virtual float GetDepth();
    virtual bool HasHandledTurn();

    //--Manipulators
    void SetName(const char *pName);
    virtual void SetDepth(float pValue);

    //--Core Methods
    virtual void RespondToSelfDestruct();

    //--Update
    virtual void Update();
    virtual void PostUpdate();
    virtual void HandleTurnUpdate();
    virtual void HandleEndOfTurn();

    //--File I/O
    //--Drawing
    //--Pointer Routing
    DataList *GetDataList();

    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_RE_SetDestruct(lua_State *L);
int Hook_RE_GetDestruct(lua_State *L);
int Hook_RE_DefineData(lua_State *L);
int Hook_RE_GetData(lua_State *L);
int Hook_RE_SetData(lua_State *L);
int Hook_RE_SetDepth(lua_State *L);
