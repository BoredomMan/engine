//--Base
#include "SpineExpression.h"

//--Classes
#include "PairanormalLevel.h"

//--CoreClasses
#include "SugarBitmap.h"

//--Definitions
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "ControlManager.h"

//--Spine
#include "Attachment.h"
#include "Atlas.h"
#include "extension.h"
#include "Skeleton.h"
#include "Slot.h"
#include "AnimationState.h"
#include "SkeletonJson.h"

//#define EXP_DEBUG
#ifdef EXP_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

//=========================================== System ==============================================
SpineExpression::SpineExpression()
{
    //--[RootObject]
    //--System
    mType = POINTER_TYPE_SPINEEXPRESSION;

    //--[SpineExpression]
    //--System
    mIsReady = false;

    //--Spine Structures
    mSpineAtlas = NULL;
    mSpineSkeletonData = NULL;
    mAnimationStateData = NULL;

    //--Animations
    mSkeleton = NULL;
    mAnimationState = NULL;

    //--Vertices
    mRenderHFlipped = false;
    //float mWorldVerticesPositions[MAX_VERTICES_PER_ATTACHMENT];
    //Vertex mVertices[MAX_VERTICES_PER_ATTACHMENT];
}
SpineExpression::~SpineExpression()
{
    if(mSpineAtlas) spAtlas_dispose(mSpineAtlas);
}
void SpineExpression::Construct(const char *pBasePath, const char *pImagePath, const char *pEmotionA, const char *pEmotionB)
{
    //--Error check.
    if(!pBasePath || !pEmotionA || !pEmotionB) return;
    DebugPush(true, "Expression constructing: %s %s %s %s\n", pBasePath, pImagePath, pEmotionA, pEmotionB);

    //--Setup.
    char tBuffer[256];

    //--String used to load the image atlas. Make sure it actually exists or fail here.
    ResetString(SpineExpression::xLoadPath, pImagePath);
    SugarBitmap *rCheckPtr = (SugarBitmap *)DataLibrary::Fetch()->GetEntry(pImagePath);
    if(!rCheckPtr)
    {
        DebugPop("No image found.\n");
        return;
    }

    //--Debug.
    DebugPrint("Verify image: %p - %i %i\n", rCheckPtr, rCheckPtr->GetWidth(), rCheckPtr->GetHeight());

    //--Create the atlas.
    sprintf(tBuffer, "%s/skeleton.atlas", pBasePath);
    mSpineAtlas = spAtlas_createFromFile(tBuffer, 0);
    if(!mSpineAtlas)
    {
        DebugPop("No atlas found %s.\n", tBuffer);
        return;
    }

    //--Load and check the skeleton data.
    sprintf(tBuffer, "%s/skeleton.json", pBasePath);
    spSkeletonJson *tJSONFile = spSkeletonJson_create(mSpineAtlas);
    mSpineSkeletonData = spSkeletonJson_readSkeletonDataFile(tJSONFile, tBuffer);
    if(!mSpineSkeletonData)
    {
        DebugPop("No JSON found %s.\n", tBuffer);
        spAtlas_dispose(mSpineAtlas);
        return;
    }
    spSkeletonJson_dispose(tJSONFile);

    //--Setup mix times
    DebugPrint("Setting mix times.\n");
    mAnimationStateData = spAnimationStateData_create(mSpineSkeletonData);
    if(!mAnimationStateData)
    {
        DebugPop("Error in animation state data.\n");
        return;
    }
    mAnimationStateData->defaultMix = 0.5f;
	spAnimationStateData_setMixByName(mAnimationStateData, pEmotionA, pEmotionB, 0.2f);
	spAnimationStateData_setMixByName(mAnimationStateData, pEmotionB, pEmotionA, 0.2f);

    //--Boot the skeleton.
    DebugPrint("Booting skeleton.\n");
    mSkeleton = spSkeleton_create(mSpineSkeletonData);
    mSkeleton->x = 0.0f;
    mSkeleton->y = 0.0f;

    //--Create the animation state and set to neutral.
    DebugPrint("Booting animation state.\n");
    mAnimationState = spAnimationState_create(mAnimationStateData);
    if(!mAnimationState)
    {
        DebugPop("Error creating animation state.\n");
        spAtlas_dispose(mSpineAtlas);
        return;
    }
    DebugPrint("Booted animation state.\n");

    //--Check if the animation exists.
    spAnimation *rCheckAnimation = spSkeletonData_findAnimation(mAnimationState->data->skeletonData, pEmotionA);
    if(!rCheckAnimation)
    {
        //--List of animations.
        DebugPrint("Unable to boot to default emotion %s. Listing emotions.\n", pEmotionA);
        //const char *rUseZero = NULL;
        for(int i = 0; i < mAnimationState->data->skeletonData->animationsCount; i ++)
        {
            DebugPrint(stderr, " %i: %s\n", i, mAnimationState->data->skeletonData->animations[i]->name);
            if(i == 0) spAnimationState_setAnimationByName(mAnimationState, 0, mAnimationState->data->skeletonData->animations[i]->name, 1);
        }
    }
    else
    {
        spAnimationState_setAnimationByName(mAnimationState, 0, pEmotionA, 1);
    }

    //--Run it once.
    spAnimationState_update(mAnimationState, 1.0f / 60.0f);
    spAnimationState_apply(mAnimationState, mSkeleton);
    spSkeleton_updateWorldTransform(mSkeleton);


    //--All checks passed, this is ready to render.
    mIsReady = true;
    DebugPop("Completed normally.\n");
}

//--[Public Statics]
//--Because of the way Spine is set up, we can't use arguments to specify where a skeleton should
//  gets its image. Instead it's hard coded to use this path.
char *SpineExpression::xLoadPath = NULL;

//====================================== Property Queries =========================================
//========================================= Manipulators ==========================================
void SpineExpression::SetEmotion(const char *pEmotionName)
{
    //--Error check.
    if(!mIsReady) return;

    //--Check if the animation exists. If it does not, use the zeroth animation instead.
    spAnimation *rCheckAnimation = spSkeletonData_findAnimation(mAnimationState->data->skeletonData, pEmotionName);
    if(!rCheckAnimation)
    {
        //--If this somehow had no emotions...
        if(mAnimationState->data->skeletonData->animationsCount < 1) return;

        //--Otherwise, use the zeroth.
        spAnimationState_setAnimationByName(mAnimationState, 0, mAnimationState->data->skeletonData->animations[0]->name, 1);
    }
    else
    {
        spAnimationState_setAnimationByName(mAnimationState, 0, pEmotionName, 1);
    }
}
void SpineExpression::SetHFlipFlag(bool pFlag)
{
    mRenderHFlipped = pFlag;
}

//========================================= Core Methods ==========================================
//===================================== Private Core Methods ======================================
//============================================ Update =============================================
void SpineExpression::Update()
{
    //--Error check.
    if(!mIsReady) return;

    //--First update the animation state by the delta time
    spAnimationState_update(mAnimationState, 1.0f / 60.0f);

    //--Next, apply the state to the skeleton
    spAnimationState_apply(mAnimationState, mSkeleton);

    //--Calculate world transforms for rendering
    spSkeleton_updateWorldTransform(mSkeleton);
}

//=========================================== File I/O ============================================
//=========================================== Drawing =============================================
//--Worker function to quickly add vertices to the buffer.
void SpineExpression::AddVertex(float x, float y, float u, float v, float r, float g, float b, float a, int &index)
{
    float cTexCoordMult = 1.0f;
    if(PairanormalLevel::xIsLowResMode) cTexCoordMult = 0.5f;

    Vertex *rVertex = &mVertices[index];
    rVertex->x = x;
    rVertex->y = y;
    rVertex->u = u * cTexCoordMult;
    rVertex->v = v * cTexCoordMult;
    rVertex->r = r;
    rVertex->g = g;
    rVertex->b = b;
    rVertex->a = a;
    index ++;
}
void SpineExpression::RenderAtCenter(float pX, float pY, float pScale)
{
    //--For each slot in the draw order array of the skeleton...
    if(!mIsReady || pScale == 0.0f) return;

    //--Position.
    glTranslatef(pX, pY, 0.0f);

    //--If the scale is larger, we bottom-center the image.
    float cCalibration = 300.0f;
    if(pScale > 1.0f)
    {
        glTranslatef(0.0f, cCalibration * (pScale - 1.0f), 0.0f);
    }

    //--Scale flipping. Spine uses reversed coordinate systems.
    glScalef(pScale, pScale, 1.0f);
    glScalef(1.0f, -1.0f, 1.0f);

    //--H-flip.
    if(mRenderHFlipped) glScalef(-1.0f, 1.0f, 1.0f);

    //--Render slots.
    for(int i = 0; i < mSkeleton->slotsCount; ++i)
    {
        //--Get the slot.
        spSlot *rSlot = mSkeleton->drawOrder[i];
        if(!rSlot) continue;

        //--Fetch the currently active attachment, continue with the next slot in the draw order if no
        //  attachment is active on the slot.
        spAttachment *rAttachment = rSlot->attachment;
        if(!rAttachment) continue;

        //--Fetch the blend mode from the slot and translate it to the engine blend mode.
        //BlendMode tEngineBlendMode;
        if(rSlot->data->blendMode == SP_BLEND_MODE_ADDITIVE)
        {
            glBlendFunc(GL_ONE, GL_ONE);
        }
        else if(rSlot->data->blendMode == SP_BLEND_MODE_MULTIPLY)
        {
            glBlendFunc(GL_DST_COLOR, GL_ONE_MINUS_SRC_ALPHA);
        }
        else if(rSlot->data->blendMode == SP_BLEND_MODE_SCREEN)
        {
            glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_COLOR);
        }
        else
        {
            glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
        }

        //--Calculate the tinting color based on the skeleton's color and the slot's color. Each color
        //  channel is given in the range [0-1], you may have to multiply by 255 and cast to and int
        //  if your engine uses integer ranges for color channels.
        float tintR = 1.0f;//mNeutralSkeleton->r * rSlot->r;
        float tintG = 1.0f;//mNeutralSkeleton->g * rSlot->g;
        float tintB = 1.0f;//mNeutralSkeleton->b * rSlot->b;
        float tintA = 1.0f;//mNeutralSkeleton->a * rSlot->a;

        //--Fill the vertices array depending on the type of attachment
        int vertexIndex = 0;
        if(rAttachment->type == SP_ATTACHMENT_REGION)
        {
            //--Cast to an spRegionAttachment so we can get the rendererObject and compute the world vertices.
            spRegionAttachment *regionAttachment = (spRegionAttachment*)rAttachment;

            // Our engine specific Texture is stored in the spAtlasRegion which was
            // assigned to the attachment on load. It represents the texture atlas
            // page that contains the image the region attachment is mapped to
            SugarBitmap *rBitmap = (SugarBitmap*)((spAtlasRegion*)regionAttachment->rendererObject)->page->rendererObject;
            rBitmap->Bind();

            // Computed the world vertices positions for the 4 vertices that make up
            // the rectangular region attachment. This assumes the world transform of the
            // bone to which the slot (and hence attachment) is attached has been calculated
            // before rendering via spSkeleton_updateWorldTransform
            spRegionAttachment_computeWorldVertices(regionAttachment, rSlot->bone, mWorldVerticesPositions, 0, 2);

            // Create 2 triangles, with 3 vertices each from the region's
            // world vertex positions and its UV coordinates (in the range [0-1]).
            AddVertex(mWorldVerticesPositions[0], mWorldVerticesPositions[1],
                regionAttachment->uvs[0], regionAttachment->uvs[1],
                tintR, tintG, tintB, tintA, vertexIndex);

            AddVertex(mWorldVerticesPositions[2], mWorldVerticesPositions[3],
                regionAttachment->uvs[2], regionAttachment->uvs[3],
                tintR, tintG, tintB, tintA, vertexIndex);

            AddVertex(mWorldVerticesPositions[4], mWorldVerticesPositions[5],
                regionAttachment->uvs[4], regionAttachment->uvs[5],
                tintR, tintG, tintB, tintA, vertexIndex);

            AddVertex(mWorldVerticesPositions[4], mWorldVerticesPositions[5],
                regionAttachment->uvs[4], regionAttachment->uvs[5],
                tintR, tintG, tintB, tintA, vertexIndex);

            AddVertex(mWorldVerticesPositions[6], mWorldVerticesPositions[7],
                regionAttachment->uvs[6], regionAttachment->uvs[7],
                tintR, tintG, tintB, tintA, vertexIndex);

            AddVertex(mWorldVerticesPositions[0], mWorldVerticesPositions[1],
                regionAttachment->uvs[0], regionAttachment->uvs[1],
                tintR, tintG, tintB, tintA, vertexIndex);
        }
        else if (rAttachment->type == SP_ATTACHMENT_MESH)
        {
            // Cast to an spMeshAttachment so we can get the rendererObject
            // and compute the world vertices
            spMeshAttachment* mesh = (spMeshAttachment*)rAttachment;

            // Check the number of vertices in the mesh attachment. If it is bigger
            // than our scratch buffer, we don't render the mesh. We do this here
            // for simplicity, in production you want to reallocate the scratch buffer
            // to fit the mesh.
            if(mesh->super.worldVerticesLength > MAX_VERTICES_PER_ATTACHMENT) continue;

            // Our engine specific Texture is stored in the spAtlasRegion which was
            // assigned to the attachment on load. It represents the texture atlas
            // page that contains the image the mesh attachment is mapped to
            SugarBitmap *rBitmap = (SugarBitmap *)((spAtlasRegion*)mesh->rendererObject)->page->rendererObject;
            rBitmap->Bind();

            // Computed the world vertices positions for the vertices that make up
            // the mesh attachment. This assumes the world transform of the
            // bone to which the slot (and hence attachment) is attached has been calculated
            // before rendering via spSkeleton_updateWorldTransform
            spVertexAttachment_computeWorldVertices(SUPER(mesh), rSlot, 0, mesh->super.worldVerticesLength, mWorldVerticesPositions, 0, 2);

            // Mesh attachments use an array of vertices, and an array of indices to define which
            // 3 vertices make up each triangle. We loop through all triangle indices
            // and simply emit a vertex for each triangle's vertex.
            for (int i = 0; i < mesh->trianglesCount; ++i)
            {
                int index = mesh->triangles[i] << 1;
                AddVertex(mWorldVerticesPositions[index], mWorldVerticesPositions[index + 1],
                   mesh->uvs[index], mesh->uvs[index + 1],
                   tintR, tintG, tintB, tintA, vertexIndex);
            }
        }

        //--Now actually render the thing.
        glBegin(GL_TRIANGLES);
        for(int i = 0; i < vertexIndex; i += 3)
        {
            glTexCoord2f(mVertices[i+0].u, 1.0f - mVertices[i+0].v); glVertex2f(mVertices[i+0].x, mVertices[i+0].y);
            glTexCoord2f(mVertices[i+1].u, 1.0f - mVertices[i+1].v); glVertex2f(mVertices[i+1].x, mVertices[i+1].y);
            glTexCoord2f(mVertices[i+2].u, 1.0f - mVertices[i+2].v); glVertex2f(mVertices[i+2].x, mVertices[i+2].y);
        }
        glEnd();
    }

    //--H-flip.
    if(mRenderHFlipped) glScalef(-1.0f, 1.0f, 1.0f);

    //--Clean.
    glScalef(1.0f, -1.0f, 1.0f);
    glScalef(1.0f / pScale, 1.0f / pScale, 1.0f);
    glTranslatef(-pX, -pY, 0.0f);
    if(pScale > 1.0f)
    {
        glTranslatef(0.0f, cCalibration * (pScale - 1.0f) * -1.0f, 0.0f);
    }
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

//======================================= Pointer Routing =========================================
//====================================== Static Functions =========================================
//========================================= Lua Hooking ===========================================
//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
