//--[PairanormalDialogueCharacter]
//--Represents one of the speaking characters during dialogue in Pairanormal. They blink and animate on their own.
//  These are not RootEntity objects.

#pragma once

//--[Includes]
#include "Definitions.h"
#include "Structures.h"
#include "RootObject.h"

//--[Local Structures]
typedef struct
{
    char mExpression[32];
    char mEmotion[32];
}EmotionMap;

//--[Classes]
class PairanormalDialogueCharacter : public RootObject
{
    private:
    //--System
    char *mInternalName;
    char *mLastExpressionStore;

    //--Rendering Position
    bool mIsFading;
    bool mIsFadingOut;
    int mPositionTimer;
    int mPositionTimerMax;
    float mRenderHalfWidth;
    float mRenderX;
    float mRenderY;
    float mPreviousRenderX;
    float mPreviousRenderY;
    float mRenderOffsetY;

    //--Greying
    bool mIsFocusCharacter;
    bool mIsOverrideFocusCharacter;
    int mGreyTimer;

    //--Chatting
    char *mDialoguePath;

    //--Aliases
    SugarLinkedList *mAliasList;

    //--Emotions
    int mExpressionCrossfadeTimer;
    SpineExpression *rLastActiveExpression;
    SpineExpression *rActiveExpression;
    SugarLinkedList *mExpressionList;//SpineExpression *
    SugarLinkedList *mEmotionMapList;//EmotionMap *

    protected:

    public:
    //--System
    PairanormalDialogueCharacter();
    virtual ~PairanormalDialogueCharacter();

    //--Public Variables
    bool mIsNextTransitionSlideFromSide;
    bool mIsNextTransitionFade;
    float mPresentRenderX;
    float mPresentRenderY;

    //--Property Queries
    const char *GetInternalName();
    const char *GetEmotionName();
    const char *GetDialoguePath();
    bool IsAliasOnList(const char *pAlias);
    float GetRenderX();
    float GetRenderY();
    bool IsFadingOut();

    //--Manipulators
    void SetInternalName(const char *pName);
    void SetRenderOffset(float pValue);
    void SetDialoguePath(const char *pPath);
    void AddAlias(const char *pName);
    void AddExpression(const char *pName, const char *pBasePath, const char *pImagePath, const char *pStateA, const char *pStateB);
    void SetEmotion(const char *pEmotion);
    void AddEmotionMap(const char *pEmotionName, const char *pExpression, const char *pEmotionStateName);
    void SetHalfWidth(float pValue);
    void SetFocus(bool pIsFocused);
    void SetOverrideFocus(bool pIsFocused);
    void SetRenderPosition(float pX, float pY);
    void SetRenderPositionByFade(float pX, float pY, int pFadeTicks);
    void SetRenderPositionBySlide(float pX, float pY, int pSlideTicks);
    void MaxOutPosition();
    void SetRenderHFlip(bool pFlag);

    //--Core Methods
    private:
    //--Private Core Methods
    public:
    //--Update
    void Update(bool pAlwaysUpdateExpression);

    //--File I/O
    //--Drawing
    void Render(float pZoom);
    void RenderAtCenter(float pX, float pY);

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_DiaChar_SetProperty(lua_State *L);

