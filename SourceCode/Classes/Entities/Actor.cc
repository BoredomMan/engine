//--Base
#include "Actor.h"

//--Classes
#include "InventoryItem.h"
#include "InventorySubClass.h"
#include "PandemoniumLevel.h"
#include "PandemoniumRoom.h"
#include "VisualLevel.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "Global.h"

//--GUI
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "DebugManager.h"
#include "EntityManager.h"
#include "LuaManager.h"

//=========================================== System ==============================================
Actor::Actor()
{
    //--[RootObject]
    //--System
    mType = POINTER_TYPE_ACTOR;

    //--[RootEntity]
    //--System
    //--Nameable
    //--Renderable
    //--Data Storage
    //--Public Variables

    //--[Actor]
    //--System
    mDescription = InitializeString("This Actor has no description.");

    //--Manual Positioning
    mHasManualPosition = false;
    mManualX = 0;
    mManualY = 0;
    mManualZ = SPECIAL_DEACTIVATE_MANUAL_POSITION;

    //--Control Scheme
    mIsPlayerControlled = false;

    //--Obscuring
    mLastSetVisible = 0;
    mIsVisibleTimer = 0;
    mObscuringList = new SugarLinkedList(true);

    //--AIs
    mLastInstruction = INSTRUCTION_NOENTRY;
    mAIScript = NULL;
    mForceOverrideScript = NULL;
    mWakeupScript = NULL;
    mTeamFlags = TEAM_NONE;

    //--Combat Statistics
    mHasCombatStats = false;
    mFleesFromCombat = false;
    mStaminaRegenTimer = 0;
    mCombatStats.Clear();
    mWeapon = NULL;
    mArmor = NULL;

    //--Bane Properties
    mBanePropertyList = new SugarLinkedList(false);

    //--Combat Phrasing
    mAsPlayerAttackPhrase = InitializeString("You attack %%s!");
    mAsEnemyAttackPhrase = InitializeString("%%s attacks you!");
    mAsNeutralAttackPhrase = InitializeString("%%s attacks %%s!");

    //--Knockout Behavior
    mHasDecrementedStunThisTurn = false;
    mPersistsAfterKnockout = false;
    mRecoversToFullHP = false;
    mTurnsToRecover = 0;
    mScoreForKill = 0;

    //--Area Knowledge
    mIsMovementRestricted = false;
    rPreviousRoom = NULL;
    rCurrentRoom = NULL;

    //--Turn Handling
    mHasHandledTurn = false;
    mPendingInstructionsTotal = 0;
    for(int i = 0; i < MAX_PENDING_INSTRUCTIONS; i ++) mPendingInstructionList[i] = INSTRUCTION_NOENTRY;

    //--Display
    mRenderFlag = RENDER_HUMAN;
    mLocalColor.SetRGBAF(0.0f, 0.0f, 0.0f, 1.0f);

    //--Inventory Control
    mIgnoresItems = false;
    mReconstituteInventory = true;
    mInventory = new InventorySubClass();

    //--Images
    rActiveImage = NULL;
    mImageList = new SugarLinkedList(false);

    //--Public Variables
    mLastRenderSlot = -1;
    mIconPack = NULL;
}
Actor::~Actor()
{
    //--Deregister from the current room, if any.
    if(rCurrentRoom)
    {
        rCurrentRoom->UnregisterActor(this);
    }

    //--Standard.
    delete mObscuringList;
    free(mDescription);
    free(mAIScript);
    free(mForceOverrideScript);
    free(mWakeupScript);
    delete mInventory;
    delete mImageList;
    delete mWeapon;
    delete mArmor;
    free(mAsPlayerAttackPhrase);
    free(mAsEnemyAttackPhrase);
    free(mAsNeutralAttackPhrase);
    delete mBanePropertyList;
    free(mIconPack);

    //--Remove reference copies.
    PandemoniumLevel *rCheckLevel = PandemoniumLevel::Fetch();
    if(rCheckLevel) rCheckLevel->RemoveActorPack(mIconPack);
}

//--[Private Statics]
//--[Public Statics]
bool Actor::xAllowOneWeapon = true;
bool Actor::xAllowOneArmor = true;
bool Actor::xAreControlsEnabled = false;
int Actor::xInstruction = INSTRUCTION_NOENTRY;
bool Actor::xWakeUpFlag = false;
bool Actor::xPrintCombatRolls = true;
bool Actor::xPrintPaddingLines = true;
const char *Actor::xrCorrupterCheckName = NULL;

//--How much stamina is consumed when someone flees in combat. Can be modified by scripts and is the
//  base value before perks are calculated in Corrupter Mode.
int Actor::xBaseFleeCost = 3;

//====================================== Property Queries =========================================
uint8_t Actor::GetTeam()
{
    return mTeamFlags;
}
char *Actor::GetDescription()
{
    return mDescription;
}
char *Actor::GetOverrideScript()
{
    return mForceOverrideScript;
}
bool Actor::IsPlayerControlled()
{
    return mIsPlayerControlled;
}
bool Actor::PersistsThroughKnockout()
{
    return mPersistsAfterKnockout;
}
int Actor::GetWorldX()
{
    //--Note: If there's no current room, returns the code -1000. This is true for all position
    //  queries. This doesn't come up during normal gameplay!
    if(mHasManualPosition) return mManualX;
    if(!rCurrentRoom) return -1000;
    return rCurrentRoom->GetWorldX();
}
int Actor::GetWorldY()
{
    if(mHasManualPosition) return mManualY;
    if(!rCurrentRoom) return -1000;
    return rCurrentRoom->GetWorldY();
}
int Actor::GetWorldZ()
{
    if(mHasManualPosition) return mManualZ;
    if(!rCurrentRoom) return -1000;
    return rCurrentRoom->GetWorldZ();
}
PandemoniumRoom *Actor::GetLocation()
{
    //--Note: Can legally return NULL.
    return rCurrentRoom;
}
int Actor::GetWeaponDefenseBonus()
{
    if(mWeapon) return mWeapon->GetDefensePower();
    return 0;
}
bool Actor::HasHandledTurn()
{
    return mHasHandledTurn;
}
uint16_t Actor::GetRenderFlag()
{
    return mRenderFlag;
}
StarlightColor Actor::GetLocalColor()
{
    //--Returns a color matching the entity's normal color. Enemies are black.
    return mLocalColor;
}
SugarBitmap *Actor::GetActiveImage()
{
    //--Returns whatever the active image is. If the Actor does not have an active image, can return one of the special
    //  images if the Actor is flagged specially.
    if(!rActiveImage)
    {
        //--Silver Rilmani Mirror.
        if(mRenderFlag == RENDER_MIRROR)
        {
            return (SugarBitmap *)DataLibrary::Fetch()->GetEntry("Root/Images/GUI/3DUI/SilverMirror");
        }
        //--Harpy Nest.
        else if(mRenderFlag == RENDER_NEST)
        {
            return (SugarBitmap *)DataLibrary::Fetch()->GetEntry("Root/Images/GUI/3DUI/HarpyNest");
        }
        //--Golem Tube.
        else if(mRenderFlag == RENDER_TUBE)
        {
            return (SugarBitmap *)DataLibrary::Fetch()->GetEntry("Root/Images/GUI/3DUI/GolemTube");
        }
        //--Glyph of Power.
        else if(mRenderFlag == RENDER_GLYPH)
        {
            return (SugarBitmap *)DataLibrary::Fetch()->GetEntry("Root/Images/GUI/3DUI/Glyph");
        }
        //--Coffin.
        else if(mRenderFlag == RENDER_COFFIN)
        {
            return (SugarBitmap *)DataLibrary::Fetch()->GetEntry("Root/Images/GUI/3DUI/Coffin");
        }

        //--All checks failed, return NULL.
        return NULL;
    }

    //--We have an active image, so return that.
    return rActiveImage;
}
SugarBitmap *Actor::GetImageRef(const char *pImageName)
{
    return (SugarBitmap *)mImageList->GetElementByName(pImageName);
}
int Actor::GetLastInstruction()
{
    return mLastInstruction;
}
bool Actor::IsStunned()
{
    return (mTurnsToRecover > 0);
}
bool Actor::IsControlled()
{
    return (mForceOverrideScript != NULL);
}
bool Actor::IgnoresItems()
{
    return mIgnoresItems;
}
int Actor::GetTurnsToRecovery()
{
    return mTurnsToRecover;
}
bool Actor::IsObscured()
{
    return (mObscuringList->GetListSize() > 0);
}
bool Actor::IsObscuredBy(uint32_t pID)
{
    uint32_t *rObscurerIDPtr = (uint32_t *)mObscuringList->PushIterator();
    while(rObscurerIDPtr)
    {
        if(*rObscurerIDPtr == pID)
        {
            mObscuringList->PopIterator();
            return true;
        }

        rObscurerIDPtr = (uint32_t *)mObscuringList->AutoIterate();
    }
    return false;
}
int Actor::GetVisibilityTimer()
{
    return mIsVisibleTimer;
}
bool Actor::JustBecameInvisible()
{
    //--Returns true if the Actor became invisible on the previous tick.
    return (mLastSetVisible == (int)Global::Shared()->gTicksElapsed - 1);
}
bool Actor::HasBane(const char *pName)
{
    //--Reports true if the named Bane exists on our list, false otherwise.
    return (mBanePropertyList->GetElementByName(pName) != NULL);
}
bool Actor::CanChangeRooms(const char *pTargetRoomName)
{
    //--Returns true if the Actor can change rooms under their own power under the current circumstances.
    //  This accounts for monsters and stamina, as well as control and stunning.
    //--The room name is optional unless we have the movement-restriction flag. If that's the case, we're a
    //  Rusalka and can only move to certain rooms.
    //fprintf(stderr, "Checking room change case.\n");
    if(!rCurrentRoom) return false;
    //fprintf(stderr, " Control check.\n");
    if(IsControlled()) return false;
    //fprintf(stderr, " Stun check.\n");
    if(IsStunned()) return false;

    //--Check for hostile entities. 3 Stamina is required to run from them, if we're still a human.
    //fprintf(stderr, " Hostility check.\n");
    xrCorrupterCheckName = mLocalName;
    if(rCurrentRoom->HasHostileEntity(mTeamFlags) && mFleesFromCombat)
    {
        //--If we don't have 3 or more stamina, we can't flee!
        if(mCombatStats.mStamina < 3)
        {
            xrCorrupterCheckName = NULL;
            return false;
        }
    }
    xrCorrupterCheckName = NULL;

    //--Rusalka check.
    //fprintf(stderr, " Rusalka check.\n");
    if(mIsMovementRestricted)
    {
        //--If the name is NULL, assume a failure.
        if(!pTargetRoomName) return false;

        //--Is it not one of the three special rooms?
        if(strcasecmp(pTargetRoomName, "Lakeside West") && strcasecmp(pTargetRoomName, "Lakeside East") && strcasecmp(pTargetRoomName, "Lakeside Centre"))
        {
            return false;
        }
    }

    //--All checks passed, you can move.
    //fprintf(stderr, " Checks passed.\n");
    return true;
}

//========================================= Manipulators ==========================================
void Actor::SetPlayerControl(bool pFlag)
{
    mIsPlayerControlled = pFlag;
}
void Actor::SetWorldPosition(int pX, int pY, int pZ)
{
    //--Note: Only does anything useful in 3D mode. Otherwise, the active room handles the position.
    //  Pass SPECIAL_DEACTIVATE_MANUAL_POSITION for the Z to unset manual positioning.
    mManualX = pX;
    mManualY = pY;
    mManualZ = pZ;
    mHasManualPosition = (mManualZ != SPECIAL_DEACTIVATE_MANUAL_POSITION);
}
void Actor::SetDescription(const char *pDescription)
{
    if(!pDescription) return;
    ResetString(mDescription, pDescription);
}
void Actor::SetTeam(uint8_t pFlags)
{
    mTeamFlags = pFlags;
}
void Actor::SetIgnoresItems(bool pFlag)
{
    mIgnoresItems = pFlag;
}
void Actor::SetRoom(PandemoniumRoom *pRoom)
{
    //--Store the previous room.
    rPreviousRoom = rCurrentRoom;

    //--Unset old connection.
    if(rCurrentRoom)
    {
        rCurrentRoom->UnregisterActor(this);
    }

    //--Set.
    rCurrentRoom = pRoom;

    //--Double-linking.
    if(rCurrentRoom)
    {
        rCurrentRoom->RegisterActor(this);
    }
}
void Actor::SetRoomByName(const char *pName)
{
    //--Locates the room and sets it as the current room. Passing NULL does nothing, unlike SetRoom()
    //  where passing NULL sets no active room. Pay attention to that difference!
    PandemoniumLevel *rActiveLevel = PandemoniumLevel::Fetch();
    if(!rActiveLevel) return;

    //--Now find the room inside that.
    PandemoniumRoom *rRoom = rActiveLevel->GetRoom(pName);
    if(!rRoom) return;

    //--Unset, set, double-link.
    if(rCurrentRoom) rCurrentRoom->UnregisterActor(this);
    rPreviousRoom = rCurrentRoom;
    rCurrentRoom = rRoom;
    rCurrentRoom->RegisterActor(this);
}
void Actor::SetRenderFlag(uint16_t pFlag)
{
    mRenderFlag = pFlag;
}
void Actor::SetLocalColor(StarlightColor pColor)
{
    memcpy(&mLocalColor, &pColor, sizeof(StarlightColor));
}
void Actor::SetAIScript(const char *pPath)
{
    ResetString(mAIScript, pPath);
}
void Actor::SetOverrideScript(const char *pPath)
{
    //--Set.
    ResetString(mForceOverrideScript, pPath);

    //--Special case: Nullify.
    if(mForceOverrideScript && !strcasecmp(mForceOverrideScript, "NULL"))
    {
        ResetString(mForceOverrideScript, NULL);
    }
    //--Flag.
    else if(mIsPlayerControlled)
    {
        xAreControlsEnabled = false;
    }
}
void Actor::SetWakeupScript(const char *pPath)
{
    ResetString(mWakeupScript, pPath);
}
void Actor::RegisterImageS(const char *pName, const char *pImagePath)
{
    //--Registers using the DataLibrary.
    if(!pName || !pImagePath) return;
    mImageList->AddElement(pName, DataLibrary::Fetch()->GetEntry(pImagePath));
}
void Actor::RegisterImageP(const char *pName, SugarBitmap *pImage)
{
    //--Registers with a direct pointer.
    if(!pName || !pImage) return;
    mImageList->AddElement(pName, pImage);
}
void Actor::SetActiveImageS(const char *pName)
{
    //--Sets by the local list.
    rActiveImage = (SugarBitmap *)mImageList->GetElementByName(pName);
}
void Actor::SetActiveImageDL(const char *pDLPath)
{
    //--Sets by a global image reference.
    rActiveImage = (SugarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath);
}
void Actor::SetHealth(int pAmount)
{
    //--Check: If the health value is a decrease, it does not check the max HP.
    bool tIsDecrease = pAmount < mCombatStats.mHP;

    mCombatStats.mHP = pAmount;
    if(mCombatStats.mHP < 0) mCombatStats.mHP = 0;
    if(mCombatStats.mHP >= mCombatStats.mHPMax && !tIsDecrease) mCombatStats.mHP = mCombatStats.mHPMax;

    //--Health hits zero: Run death code.
    if(mCombatStats.mHP < 1) RespondToKilled(true);
}
void Actor::SetHealthBypass(int pAmount)
{
    mCombatStats.mHP = pAmount;
    if(mCombatStats.mHP < 0) mCombatStats.mHP = 0;

    //--Health hits zero: Run death code.
    if(mCombatStats.mHP < 1) RespondToKilled(true);
}
void Actor::SetWillPower(int pAmount)
{
    //--Boilerplate.
    int mOldWillpower = mCombatStats.mWillPower;
    mCombatStats.mWillPower = pAmount;

    //--Bounding.
    if(mCombatStats.mWillPower < -5) mCombatStats.mWillPower = -5;
    if(mCombatStats.mWillPower >= mCombatStats.mWillPowerMax) mCombatStats.mWillPower = mCombatStats.mWillPowerMax;

    //--If there was a loss of willpower, reset this timer.
    if(mOldWillpower > mCombatStats.mWillPower)
    {
        mWillpowerRegenTimer = 0;
    }

    //--Willpower hits zero, but wasn't zero before. Run this code.
    if(mOldWillpower > 0 && mCombatStats.mWillPower <= 0)
    {
        RespondToConfounded(true);
    }
}
void Actor::ResetWillpowerRegen()
{
    mWillpowerRegenTimer = 0;
}
void Actor::SetStamina(int pAmount)
{
    mCombatStats.mStamina = pAmount;
    if(mCombatStats.mStamina < 0) mCombatStats.mStamina = 0;
    if(mCombatStats.mStamina >= mCombatStats.mStaminaMax) mCombatStats.mStamina = mCombatStats.mStaminaMax;
}
void Actor::SetAsPlayerAttackPhrase(const char *pPhrase)
{
    if(!pPhrase) return;
    ResetString(mAsPlayerAttackPhrase, pPhrase);
}
void Actor::SetAsEnemyAttackPhrase(const char *pPhrase)
{
    if(!pPhrase) return;
    ResetString(mAsEnemyAttackPhrase, pPhrase);
}
void Actor::SetAsNeutralAttackPhrase(const char *pPhrase)
{
    if(!pPhrase) return;
    ResetString(mAsNeutralAttackPhrase, pPhrase);
}
void Actor::AddObscurer(uint32_t pID)
{
    //--Check for duplicates.
    uint32_t *rObscurerIDPtr = (uint32_t *)mObscuringList->PushIterator();
    while(rObscurerIDPtr)
    {
        if(*rObscurerIDPtr == pID)
        {
            mObscuringList->PopIterator();
            return;
        }

        rObscurerIDPtr = (uint32_t *)mObscuringList->AutoIterate();
    }

    //--No duplicates, add new.
    SetMemoryData(__FILE__, __LINE__);
    uint32_t *nPtr = (uint32_t *)starmemoryalloc(sizeof(uint32_t));
    *nPtr = pID;
    mObscuringList->AddElement("X", nPtr, &FreeThis);
}
void Actor::RemoveObscurer(uint32_t pID)
{
    //--If they pass 0, clear all obscuring info.
    if(pID == 0) { mObscuringList->ClearList(); return; }

    //--Otherwise, remove the offending ID.
    uint32_t *rObscurerIDPtr = (uint32_t *)mObscuringList->SetToHeadAndReturn();
    while(rObscurerIDPtr)
    {
        if(*rObscurerIDPtr == pID)
        {
            mObscuringList->RemoveRandomPointerEntry();
        }

        rObscurerIDPtr = (uint32_t *)mObscuringList->IncrementAndGetRandomPointerEntry();
    }
}
void Actor::AddBane(const char *pName)
{
    if(!pName) return;
    static int xDummyPtr = 0;
    mBanePropertyList->AddElement(pName, &xDummyPtr);
}
void Actor::SetMovementRestriction(bool pFlag)
{
    mIsMovementRestricted = pFlag;
}
void Actor::IncrementVisible()
{
    //--Increments the visible timer, and stores the tick it happened on.
    mLastSetVisible = Global::Shared()->gTicksElapsed;
    if(mIsVisibleTimer < VL_ENTITY_FADE_TICKS) mIsVisibleTimer ++;
}
void Actor::DecrementVisible()
{
    //--Decrements the visible timer if (and only if) the mLastVisible is not equal to the current tick.
    //  This means all entities which do not increment automatically decrement.
    if(mLastSetVisible == (int)Global::Shared()->gTicksElapsed) return;

    //--All checks passed, entity is invisible. Decrement.
    if(mIsVisibleTimer > 0) mIsVisibleTimer --;
}
void Actor::AppendNoMoveErrorToConsole(const char *pRoomTarget)
{
    //--If unable to move to a location due to something like stamina loss or movement restriction, this
    //  function determines what the issue is and prints it to the console in RED LETTERS.
    //--pRoomTarget is only required if the Actor is a Rusalka.
    if(!rCurrentRoom) return;

    //--Can't move because a script is overriding your actions.
    if(IsControlled())
    {
        PandemoniumLevel::AppendToConsoleStatic("You can't move, something is controlling your actions!", 0, StarlightColor::MapRGBAF(0.7f, 0.0f, 0.0f, 1.0f));
        return;
    }
    if(IsStunned())
    {
        PandemoniumLevel::AppendToConsoleStatic("You can't move, you're stunned!", 0, StarlightColor::MapRGBAF(0.7f, 0.0f, 0.0f, 1.0f));
        return;
    }

    //--Check for hostile entities. 3 Stamina is required to run from them, if we're still a human.
    xrCorrupterCheckName = mLocalName;
    if(rCurrentRoom->HasHostileEntity(mTeamFlags) && mFleesFromCombat)
    {
        //--If we don't have 3 or more stamina, we can't flee!
        if(mCombatStats.mStamina < 3)
        {
            xrCorrupterCheckName = NULL;
            PandemoniumLevel::AppendToConsoleStatic("You can't run, you're out of stamina!", 0, StarlightColor::MapRGBAF(0.7f, 0.0f, 0.0f, 1.0f));
            return;
        }
    }
    xrCorrupterCheckName = NULL;

    //--Rusalka check.
    if(mIsMovementRestricted)
    {
        //--If the name is NULL, assume a failure.
        if(!pRoomTarget) return;

        //--Is it not one of the three special rooms?
        if(strcasecmp(pRoomTarget, "Lakeside West") && strcasecmp(pRoomTarget, "Lakeside East") && strcasecmp(pRoomTarget, "Lakeside Centre"))
        {
            PandemoniumLevel::AppendToConsoleStatic("You can't move away from the lake as a rusalka.", 0, StarlightColor::MapRGBAF(0.7f, 0.0f, 0.0f, 1.0f));
            return;
        }
    }

    //--If we got this far, the reason for being unable to move is defined.
    PandemoniumLevel::AppendToConsoleStatic("You can't move for an undefined reason!", 0, StarlightColor::MapRGBAF(0.7f, 0.0f, 0.0f, 1.0f));
}

//========================================= Core Methods ==========================================
int Actor::DetermineFleeCost()
{
    //--Determines how much stamina is required to flee from an enemy. Right now, always returns the
    //  static value, but in the future there may be perks which affect this.
    return xBaseFleeCost;
}
void Actor::AppendInstructionS(const char *pInstruction)
{
    //--Same as the HandleInstruction version, but appends the instruction instead.
    AppendInstruction(GetInstructionFromString(pInstruction));
}
void Actor::AppendInstruction(int pInstruction)
{
    //--Appends an instruction onto the end of the instruction list. Uses a function similar to
    //  string handling. If there are already too many instructions, it gets *dropped*.
    if(mPendingInstructionsTotal >= MAX_PENDING_INSTRUCTIONS)
    {
        DebugManager::ForcePrint("Actor:AppendInstruction - Failed, instruction list too long.\n");
        return;
    }

    //--If an invalid instruction, do nothing.
    if(pInstruction == INSTRUCTION_NOENTRY) return;

    //--Normal case.
    mPendingInstructionList[mPendingInstructionsTotal+0] = pInstruction;
    mPendingInstructionList[mPendingInstructionsTotal+1] = INSTRUCTION_NOENTRY;
    mPendingInstructionsTotal ++;
}
void Actor::HandleInstructionS(const char *pInstruction)
{
    //--Translates the instruction from a human-readable string to an instruction code. Useful for
    //  scripts. Automatically calls HandleInstruction() unless an error occurred.
    HandleInstruction(GetInstructionFromString(pInstruction));
}
void Actor::WipeInstructions()
{
    for(int i = 0; i < MAX_PENDING_INSTRUCTIONS; i ++) mPendingInstructionList[0] = INSTRUCTION_NOENTRY;
    mPendingInstructionsTotal = 0;
}
void Actor::HandleInstruction(int pInstruction)
{
    //--Handles a given instruction. Note that this may come from the player, or it may come from
    //  an AI, or it may be a queued instruction. In any case, this subroutine checks if it's legal
    //  and carries it out. Also, the mHasHandledTurn flag will be thrown.

    //--Skip the turn, but do so in a fashion called externally. The last instruction still counts as
    //  being a SKIPTURN instruction.
    if(pInstruction == INSTRUCTION_COMPLETETURN)
    {
        //--Flags.
        mHasHandledTurn = true;
        mLastInstruction = INSTRUCTION_SKIPTURN;

        //--Print.
        //PrintByCode(PRINTCODE_MOVE_WAIT, true, mLocalName, "Null", "Null");
    }
    //--Skip the turn and do nothing.
    else if(pInstruction == INSTRUCTION_SKIPTURN)
    {
        //--Flags.
        mHasHandledTurn = true;
        mLastInstruction = pInstruction;

        //--Print.
        PrintByCode(PRINTCODE_MOVE_WAIT, true, mLocalName, "Null", "Null");
    }
    //--Attempt to move in a direction. May fail.
    else if(pInstruction >= INSTRUCTION_MOVENORTH && pInstruction <= INSTRUCTION_MOVEDOWN)
    {
        //--Error check.
        if(!rCurrentRoom) return;

        //--Fleeing check. If there's a hostile entity in the room, and we flee from those, then
        //  we need to have at least 3 stamina to flee.
        mLastMoveWasFlee = false;
        if(mFleesFromCombat)
        {
            //--Check for a hostile entity.
            xrCorrupterCheckName = mLocalName;
            if(rCurrentRoom->HasHostileEntity(mTeamFlags))
            {
                //--If we don't have 3 or more stamina, we can't flee! Waste a turn.
                if(mCombatStats.mStamina < DetermineFleeCost())
                {
                    xrCorrupterCheckName = NULL;
                    mHasHandledTurn = true;
                    mLastInstruction = INSTRUCTION_SKIPTURN;
                    PrintByCode(PRINTCODE_MOVE_CANTFLEE, true, mLocalName, "Null", "Null");
                    return;
                }
                //--We have enough stamina. Set the retreat flag and lose DetermineFleeCost() stamina.
                else
                {
                    mLastMoveWasFlee = true;
                    mCombatStats.mStamina -= DetermineFleeCost();
                }
            }
            xrCorrupterCheckName = NULL;
        }

        //--If the room connection exists, it will return. If it's null, can't go that way.
        PandemoniumRoom *rTargetRoom = rCurrentRoom->GetConnection(pInstruction-NAVIGATION_OFFSET);
        if(rTargetRoom)
        {
            //--If our movement is restricted, this target room must be on the lakeside.
            if(mIsMovementRestricted && strcasecmp(rTargetRoom->GetName(), "Lakeside West") && strcasecmp(rTargetRoom->GetName(), "Lakeside East") && strcasecmp(rTargetRoom->GetName(), "Lakeside Centre"))
            {
                PandemoniumLevel::AppendToConsoleStatic("You cannot move away from the lake in your current form.");
                return;
            }

            //--Store the previous room.
            PandemoniumRoom *rPreviousRoom = rCurrentRoom;

            //--Move to the room.
            SetRoom(rTargetRoom);
            mHasHandledTurn = true;

            //--Store this.
            mLastInstruction = pInstruction;

            //--If we are a player-controlled entity, print the contents of the room to the console.
            //  If not, the room may still want to respond.
            rTargetRoom->RespondToActorEntry(this, rPreviousRoom);

            //--Also inform the room we left.
            rPreviousRoom->RespondToActorExit(this);
        }
        //--No dice, so we can't go that way. Print an error.
        else
        {

        }

        //--Clean up.
        mLastMoveWasFlee = false;
    }
    //--Erroneous instruction, do nothing.
    else
    {
        DebugManager::ForcePrint("Error: Instruction code %i cannot be handled.\n", pInstruction);
    }
}
void Actor::AppendMoveToRoom(const char *pRoomName)
{
    //--The Actor will determine which room it is in and append instructions needed to find the
    //  destination room. If it's the current room, does nothing, you goof.
    if(!pRoomName || !rCurrentRoom) return;

    //--Get the current room. Check if it's the destination!
    if(!strcasecmp(rCurrentRoom->GetName(), pRoomName)) return;

    //--Wipe the instruction list, in case there was garbage on there.
    WipeInstructions();

    //--All is good. Get the path to the target room.
    PathInstructions *rPathInstructions = rCurrentRoom->GetPathTo(pRoomName);
    if(!rPathInstructions) return;

    //--Append all the instructions.
    for(int i = 0; i < rPathInstructions->mListSize; i ++)
    {
        AppendInstruction(rPathInstructions->mPathList[i] + NAVIGATION_OFFSET);
    }
}
int Actor::GetInstructionForMoveTo(const char *pRoomName)
{
    //--Returns the instruction code representing the first step in moving to the given room.
    //  In theory, this should not result in jams as the entity moves to the target unless
    //  the pathing changes on-the-fly. That'd be odd.
    //--Returns -1 on failure, or if the destination is the current room.
    if(!pRoomName || !rCurrentRoom) return -1;

    //--Get the current room. Check if it's the destination!
    if(!strcasecmp(rCurrentRoom->GetName(), pRoomName)) return -1;

    //--All is good. Get the path to the target room.
    PathInstructions *rPathInstructions = rCurrentRoom->GetPathTo(pRoomName);
    if(!rPathInstructions) return -1;

    //--Take the 0th instruction and return it.
    return rPathInstructions->mPathList[0] + NAVIGATION_OFFSET;
}
bool Actor::IsHostileTo(Actor *pOtherActor)
{
    //--Overload, gets the team flags from the Actor and checks if they're hostile that way.
    if(!pOtherActor) return false;

    //--Call the subroutine.
    uint8_t tTheirTeam = pOtherActor->GetTeam();
    return IsHostileTo(tTheirTeam);
}
#include "DataList.h"
bool Actor::IsHostileTo(uint8_t pTeamFlags)
{
    //--To qualify as hostile, we must be on opposing teams. Neutrals are never hostile.
    if(mTeamFlags == TEAM_NONE || pTeamFlags == TEAM_NONE) return false;

    //--If the team flags happen to be TEAM_PLAYER, and we're in Corrupter Mode, then we need to check if
    //  this Actor is the player and if they are disguised.
    if(pTeamFlags == TEAM_PLAYER && mDataList->FetchDataEntry("iIsCorrupterMode") && xrCorrupterCheckName)
    {
        //--Check if we're still disguised to the human who is calling this function.
        char tBuffer[32];
        sprintf(tBuffer, "iDisguise%s", xrCorrupterCheckName);
        float *rCheckPtr = (float *)mDataList->FetchDataEntry(tBuffer);

        //--Value must exist...
        if(rCheckPtr)
        {
            //--Value is non-zero: We're not hostile, still in disguise.
            if(*rCheckPtr != 0.0f)
            {
                return false;
            }
            //--Value is 0.0f, cover is blown!
            else if(*rCheckPtr == 0.0f)
            {
                return true;
            }
        }
    }

    return (mTeamFlags != pTeamFlags);
}
char *Actor::AssembleAttackPhrase(Actor *pVictim)
{
    //--Assembles and returns a heap-allocated string that is used during attacking. You are
    //  responsible for cleaning up the string when done with it.
    //--If an error occurs, returns a string representing the error.
    char *nString = NULL;
    if(!pVictim)
    {
        nString = InitializeString("Error: Victim was NULL.");
    }
    //--Players use the player phrasing.
    else if(mIsPlayerControlled)
    {
        //String Format: "You attack %s!"
        nString = InitializeString(mAsPlayerAttackPhrase, pVictim->GetName());
        //fprintf(stderr, "String: %s\n", mAsPlayerAttackPhrase);
        //fprintf(stderr, "Target Name: %s\n", pVictim->GetName());
        //fprintf(stderr, "Final: %s\n", nString);
    }
    //--If the victim is player controlled, use a different phrasing.
    else if(pVictim->IsPlayerControlled())
    {
        //String Format: "%s attacks you!"
        nString = InitializeString(mAsEnemyAttackPhrase, mLocalName);
    }
    //--Neither is player controlled, so this is a third-person observation.
    else
    {
        //String Format: "%s attacks %s!"
        nString = InitializeString(mAsNeutralAttackPhrase, mLocalName, pVictim->GetName());
    }

    //--Done.
    return nString;
}
void Actor::RecheckObscurers()
{
    //--Checks if anything that is alive is currently obscuring us. If there are any IDs on the obscuring
    //  list, checks if they still exist. If not, remove them.
    EntityManager *rEntityManager = EntityManager::Fetch();
    uint32_t *rObscurerIDPtr = (uint32_t *)mObscuringList->SetToHeadAndReturn();
    while(rObscurerIDPtr)
    {
        //--Check.
        if(!rEntityManager->GetEntityI(*rObscurerIDPtr))
        {
            mObscuringList->RemoveRandomPointerEntry();
        }

        //--Next.
        rObscurerIDPtr = (uint32_t *)mObscuringList->IncrementAndGetRandomPointerEntry();
    }
}
float Actor::GetBaneOfTarget(uint32_t pTargetID)
{
    //--Returns the bane that applies against the given target, assuming it exists. If the bane does not exist, or the target
    //  does not exist, or anything else goes wrong, returns 1.0f.
    if(!mWeapon) return 1.0f;

    //--Find the target.
    Actor *rTarget = (Actor *)EntityManager::Fetch()->GetEntityI(pTargetID);
    if(!rTarget) return 1.0f;

    //--Target exists, check if our weapon has any banes on it.
    SugarLinkedList *rBaneList = mWeapon->GetBaneList();
    float *rBaneAmount = (float *)rBaneList->PushIterator();
    while(rBaneAmount)
    {
        //--If the bane exists, we can return it right now. Duplicates are ignored.
        if(rTarget->HasBane(rBaneList->GetIteratorName()))
        {
            rBaneList->PopIterator();
            return *rBaneAmount;
        }

        //--Next.
        rBaneAmount = (float *)rBaneList->AutoIterate();
    }

    //--No match, return 1.0f.
    return 1.0f;
}

//===================================== Private Core Methods ======================================
//============================================ Update =============================================
void Actor::HandleTurnUpdate()
{
    //--During an Actor's turn, this function is called until the Actor has satisfactorally resolved
    //  and performed its action. AIs typically do this instantly, while the player may call this
    //  repeatedly until they get an action done.
    RecheckObscurers();
    if(mHasHandledTurn) return;

    //--Debug.
    //DebugManager::PushPrint(!mIsPlayerControlled, "Non-player AI %s updates.\n", mLocalName);
    DebugManager::PushPrint(false, "Non-player AI %s updates.\n", mLocalName);

    //--If we're stunned, auto-pass the turn until that's not the case.
    if(mTurnsToRecover > 0)
    {
        //--Debug
        DebugManager::Print("A\n");

        //--Basic behavior.
        mHasHandledTurn = true;
        if(!mHasDecrementedStunThisTurn) mTurnsToRecover --;
        mHasDecrementedStunThisTurn = true;

        //--We got back up!
        if(mTurnsToRecover == 0)
        {
            //--Run the subroutine. Allow it to print.
            Revive(false);
        }
        //--When player-controlled, we don't auto-handle the turn. Typically they just wait it
        //  out anyway, though.
        else if(mIsPlayerControlled)
        {
            mHasHandledTurn = false;
        }
        //--This update is done.
        else
        {
            DebugManager::PopPrint("Non-player AI completes, stunned.\n");
            return;
        }
    }
    //--If our willpower is less than 1, we're "Confounded" and unable to act. Willpower
    //  still regenerates in this state.
    else if(mHasCombatStats && mCombatStats.mWillPower < 1)
    {
        //--Debug
        DebugManager::Print("B\n");

        //--Basic behavior.
        mHasHandledTurn = true;
        DebugManager::PopPrint("Non-player AI completes, confounded.\n");
        return;
    }
    //--Are there pending instructions? If so, deal with those.
    else if(mPendingInstructionsTotal > 0)
    {
        //--Debug
        DebugManager::Print("C\n");

        //--Reset this flag.
        xWakeUpFlag = false;

        //--If the entity has a wakeup script, run that here.
        if(mWakeupScript && !mForceOverrideScript)
        {
            //--Exec.
            LuaManager::Fetch()->PushExecPop(this, mWakeupScript);

            //--If the flag got tripped, print this.
            //if(xWakeUpFlag) fprintf(stderr, "%s wakes up!\n", mLocalName);
        }

        //--Was the flag tripped? If not, continue following the automatic instructions.
        if(!xWakeUpFlag)
        {
            //--Deal with the instruction.
            HandleInstruction(mPendingInstructionList[0]);

            //--Remove the instruction from the list.
            for(int i = 0; i < mPendingInstructionsTotal; i ++)
            {
                mPendingInstructionList[i+0] = mPendingInstructionList[i+1];
            }

            //--Decrement the list length.
            mPendingInstructionList[mPendingInstructionsTotal-1] = INSTRUCTION_NOENTRY;
            mPendingInstructionsTotal --;

            //--This counts as handling the turn, regardless of what the instruction was.
            mHasHandledTurn = true;
            //fprintf(stderr, "Automatically %s handles turn %i.\n", mLocalName, mLastInstruction);
            DebugManager::PopPrint("Non-player AI completes, no wakeup.\n");
            return;
        }
    }

    //--If controlled by an override script, we run that script instead. Note that it will
    //  enqueue instructions, but they don't execute until the player passes (if present).
    if(mForceOverrideScript)
    {
        //--Debug
        DebugManager::Print("D\n");

        //--For AI characters, just run the script as if it were the AI script.
        if(!mIsPlayerControlled)
        {
            //--Run.
            LuaManager::Fetch()->PushExecPop(this, mForceOverrideScript);
            //fprintf(stderr, "The ForceOverride changed an AI action to %i!\n", xInstruction);

            //--Are there pending instructions? Do those now.
            if(mPendingInstructionsTotal > 0 && xInstruction == INSTRUCTION_NOENTRY)
            {
                //--Deal with the instruction.
                HandleInstruction(mPendingInstructionList[0]);

                //--Remove the instruction from the list.
                for(int i = 0; i < mPendingInstructionsTotal; i ++)
                {
                    mPendingInstructionList[i+0] = mPendingInstructionList[i+1];
                }

                //--Decrement the list length.
                mPendingInstructionList[mPendingInstructionsTotal-1] = INSTRUCTION_NOENTRY;
                mPendingInstructionsTotal --;

                //--This counts as handling the turn, regardless of what the instruction was.
                mHasHandledTurn = true;
                DebugManager::PopPrint("Non-player AI completes, command overrided.\n");
                return;
            }
            //--If there is a single pending instruction, do that now.
            else if(xInstruction != INSTRUCTION_NOENTRY)
            {
                HandleInstruction(xInstruction);
                xInstruction = INSTRUCTION_NOENTRY;
            }
            //--If there's no instructions at all, the AI script banjaxed something. Skip the turn.
            //  This also happens if the AI does something (like a TF) which does not directly call
            //  the Actor's functions.
            else
            {
                mHasHandledTurn = true;
            }
        }
        //--For players, we run the script once. After that, the player can examine stuff. Once they
        //  pass their turn, the script will run the stored command.
        else
        {
            //--Run.
            if(!xAreControlsEnabled)
            {
                //--Set the flags.
                xAreControlsEnabled = true;
                xInstruction = INSTRUCTION_NOENTRY;
                LuaManager::Fetch()->PushExecPop(this, mForceOverrideScript);

                //--The instruction should get set. Store it.
                mPendingOverrideInstruction = xInstruction;
                xInstruction = INSTRUCTION_NOENTRY;

                //--Set the GUI to display our active portrait, if any.
                PandemoniumLevel *rActiveLevel = PandemoniumLevel::Fetch();
                if(rActiveLevel)
                {
                    rActiveLevel->SetActivePortraitName(mLocalName);
                    rActiveLevel->SetActivePortrait(rActiveImage);
                }
            }
            //--On subsequent ticks, wait for an instruction. If one is not present, do nothing.
            else if(xInstruction == INSTRUCTION_NOENTRY)
            {

            }
            //--An instruction was received. Carry it out. If the instruction is illegal, clear
            //  the flag and don't consume a turn.
            else
            {
                //--Debug.
                fprintf(stderr, "Instruction received. %i %i\n", xInstruction, mPendingOverrideInstruction);

                //--If stunned, the only legal action is to wait.
                xInstruction = mPendingOverrideInstruction;
                //fprintf(stderr, "The ForceOverride changed your action to %i!\n", xInstruction);
                if(mTurnsToRecover > 0 || xInstruction == INSTRUCTION_NOENTRY)
                {
                    xInstruction = INSTRUCTION_COMPLETETURN;
                    PrintByCode(PRINTCODE_TIME_PASSES_STUNNED, true, mLocalName, "Null", "Null");
                    //fprintf(stderr, " But we skipped our turn anyway.\n");
                }

                //--Handle it. If it's legal, the subroutine will flip the mHasHandledTurn flag.
                HandleInstruction(xInstruction);

                //--Clear the instruction.
                xInstruction = INSTRUCTION_NOENTRY;

                //--If we handled the turn, disable controls.
                if(mHasHandledTurn)
                {
                    xAreControlsEnabled = false;
                }

                //--Give the VisualLevel, if it exists, the opportunity to respond.
                VisualLevel *rCheckLevel = VisualLevel::Fetch();
                if(rCheckLevel) rCheckLevel->HandlePlayerCommandOverride();
            }
        }
    }
    //--If player controlled, turn on the control settings.
    else if(mIsPlayerControlled)
    {
        //--Debug
        DebugManager::Print("E\n");

        //--On the first tick, enable the controls and reset any flags.
        if(!xAreControlsEnabled)
        {
            //--Flags.
            xAreControlsEnabled = true;
            xInstruction = INSTRUCTION_NOENTRY;

            //--Set the GUI to display our active portrait, if any.
            PandemoniumLevel *rActiveLevel = PandemoniumLevel::Fetch();
            if(rActiveLevel)
            {
                rActiveLevel->SetActivePortraitName(mLocalName);
                rActiveLevel->SetActivePortrait(rActiveImage);
            }
        }
        //--On subsequent ticks, wait for an instruction. If one is not present, do nothing.
        else if(xInstruction == INSTRUCTION_NOENTRY)
        {

        }
        //--An instruction was received. Carry it out. If the instruction is illegal, clear
        //  the flag and don't consume a turn.
        else
        {
            //--If stunned, the only legal action is to wait.
            if(mTurnsToRecover > 0)
            {
                xInstruction = INSTRUCTION_COMPLETETURN;
                PrintByCode(PRINTCODE_TIME_PASSES_STUNNED, true, mLocalName, "Null", "Null");
            }

            //--Handle it. If it's legal, the subroutine will flip the mHasHandledTurn flag.
            HandleInstruction(xInstruction);

            //--Clear the instruction.
            xInstruction = INSTRUCTION_NOENTRY;

            //--If we handled the turn, disable controls.
            if(mHasHandledTurn)
            {
                //fprintf(stderr, "Player %s handles turn.\n", mLocalName);
                xAreControlsEnabled = false;
            }
        }
    }
    //--Otherwise, let the AI handle it.
    else if(mAIScript)
    {
        //--Debug
        DebugManager::Print("F\n");

        //--Push this as the active object.
        LuaManager::Fetch()->PushExecPop(this, mAIScript);

        //--Are there pending instructions? Do those now.
        if(mPendingInstructionsTotal > 0 && xInstruction == INSTRUCTION_NOENTRY)
        {
            //--Debug.
            DebugManager::Print("1\n");

            //--Deal with the instruction.
            HandleInstruction(mPendingInstructionList[0]);

            //--Remove the instruction from the list.
            for(int i = 0; i < mPendingInstructionsTotal; i ++)
            {
                mPendingInstructionList[i+0] = mPendingInstructionList[i+1];
            }

            //--Decrement the list length.
            mPendingInstructionList[mPendingInstructionsTotal-1] = INSTRUCTION_NOENTRY;
            mPendingInstructionsTotal --;

            //--This counts as handling the turn, regardless of what the instruction was.
            mHasHandledTurn = true;
            DebugManager::PopPrint("Non-player AI completes, AI handled.\n");
            return;
        }
        //--If there is a single pending instruction, do that now.
        else if(xInstruction != INSTRUCTION_NOENTRY)
        {
            //--Debug.
            DebugManager::Print("2\n");

            HandleInstruction(xInstruction);
            xInstruction = INSTRUCTION_NOENTRY;
        }
        //--If there's no instructions at all, the AI script banjaxed something. Skip the turn.
        //  This also happens if the AI does something (like a TF) which does not directly call
        //  the Actor's functions.
        else
        {
            //--Debug.
            DebugManager::Print("3\n");

            mHasHandledTurn = true;
        }
    }
    //--No AI? Turn completes immediately.
    else
    {
        //--Debug
        DebugManager::Print("G\n");

        mHasHandledTurn = true;
    }

    //--Debug.
    DebugManager::PopPrint("Non-player AI completes, normally.\n");
}
void Actor::HandleEndOfTurn()
{
    //--At the end of an Actor's turn, reset any flags and run any status effects that may be
    //  present. This includes things like damage-over-times or infections.
    mHasHandledTurn = false;
    mHasDecrementedStunThisTurn = false;
    DebugManager::Print(" %s ends turn.\n", mLocalName);

    //--Regenerate willpower. This happens every 3 turns.
    if(mHasCombatStats && mCombatStats.mWillPower < mCombatStats.mWillPowerMax)
    {
        mWillpowerRegenTimer ++;
        if(mWillpowerRegenTimer >= 3)
        {
            //--Reset.
            mWillpowerRegenTimer = 0;
            mCombatStats.mWillPower ++;

            //--If the Actor regains their faculties as a result of this, print the result.
            if(mCombatStats.mWillPower == 1)
            {
                PrintByCode(PRINTCODE_REVIVE_FROM_WILLPOWER, true, mLocalName, "Null", "Null");
            }
        }
    }

    //--If we use stamina, regenerate stamina after 3 turns in a location with no hostile entities.
    xrCorrupterCheckName = mLocalName;
    if(!rCurrentRoom || rCurrentRoom->HasHostileEntity(mTeamFlags))
    {
        mStaminaRegenTimer = 0;
    }
    //--No hostiles, regenerate stamina.
    else
    {
        mStaminaRegenTimer ++;
        if(mStaminaRegenTimer > 2)
        {
            mStaminaRegenTimer = 0;
            mCombatStats.mStamina ++;
            if(mCombatStats.mStamina >= mCombatStats.mStaminaMax) mCombatStats.mStamina = mCombatStats.mStaminaMax;
        }
    }

    //--Clean.
    xrCorrupterCheckName = NULL;
}

//=========================================== File I/O ============================================
//=========================================== Drawing =============================================
//======================================= Pointer Routing =========================================
PandemoniumRoom *Actor::GetCurrentRoom()
{
    return rCurrentRoom;
}
InventoryItem *Actor::GetWeapon()
{
    return mWeapon;
}
InventoryItem *Actor::GetArmor()
{
    return mArmor;
}

//====================================== Static Functions =========================================
Actor *Actor::FindPlayer()
{
    //--Find the first Actor that is controlled by the player, and returns it. Can return NULL if
    //  it doesn't find anyone.
    SugarLinkedList *rEntityList = EntityManager::Fetch()->GetEntityList();
    RootEntity *rEntity = (RootEntity *)rEntityList->PushIterator();
    while(rEntity)
    {
        //--Must be an Actor.
        if(rEntity->GetType() == POINTER_TYPE_ACTOR)
        {
            Actor *rActor = (Actor *)rEntity;
            if(rActor->IsPlayerControlled())
            {
                rEntityList->PopIterator();
                return rActor;
            }
        }

        //--Next.
        rEntity = (RootEntity *)rEntityList->AutoIterate();
    }

    //--None found.
    return NULL;
}
int Actor::GetInstructionFromString(const char *pInstruction)
{
    //--Returns an instruction code matching the given string, or invalid if the string is bad.
    if(!pInstruction) return INSTRUCTION_NOENTRY;

    //--Skip turn.
    if(!strcasecmp(pInstruction, "Skip Turn"))
        return INSTRUCTION_SKIPTURN;
    //--Move north.
    else if(!strcasecmp(pInstruction, "Move North") || !strcasecmp(pInstruction, "N"))
        return INSTRUCTION_MOVENORTH;
    //--Move east.
    else if(!strcasecmp(pInstruction, "Move East") || !strcasecmp(pInstruction, "E"))
        return INSTRUCTION_MOVEEAST;
    //--Move south.
    else if(!strcasecmp(pInstruction, "Move South") || !strcasecmp(pInstruction, "S"))
        return INSTRUCTION_MOVESOUTH;
    //--Move west.
    else if(!strcasecmp(pInstruction, "Move West") || !strcasecmp(pInstruction, "W"))
        return INSTRUCTION_MOVEWEST;
    //--Move up.
    else if(!strcasecmp(pInstruction, "Move Up") || !strcasecmp(pInstruction, "U"))
        return INSTRUCTION_MOVEUP;
    //--Move down.
    else if(!strcasecmp(pInstruction, "Move Down") || !strcasecmp(pInstruction, "D"))
        return INSTRUCTION_MOVEDOWN;
    //--Error.
    else
    {
        DebugManager::ForcePrint("Actor:GetInstructionFromString - Error: Unknown instruction %s\n", pInstruction);
    }
    return INSTRUCTION_NOENTRY;
}

