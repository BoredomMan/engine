//--Base
#include "WADFile.h"

//--Classes
#include "Skybox.h"
#include "WorldPolygon.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarLinkedList.h"

//--Definitions
#include "GlDfn.h"
#include "HitDetection.h"
#define GL_ARRAY_BUFFER                   0x8892
#define GL_STATIC_DRAW                    0x88E4
#define GL_CLAMP_TO_EDGE                  0x812F

//--GUI
//--Libraries
//--Managers
#include "CameraManager.h"
#include "DisplayManager.h"
#include "DebugManager.h"

//--[Debug Functions]
//#define WFRENDER_DEBUG
#ifdef WFRENDER_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

//--[Debug Flags]
#define WRENDER_DEBUG_COMPILE false

//--[Other Flags]
#define WRENDER_ALLOW_GLLIST

//--[Shader Functions]
void WADFile::UploadShaderData(bool pNeedsTexSampler)
{
    //--Upload the necessary information for the shader. Used right before we do rendering, doesn't matter if it's the
    //  multi-texture variety or fullbright. Lightmaps ignore this.
    int tProgramHandle = DisplayManager::Fetch()->mLastProgramHandle;
    if(tProgramHandle && mTextureAtlas && mCompressedTextureTable)
    {
        //fprintf(stderr, "Uploading shader data %i.\n", mCompressedTextureTable->GetListSize());
        //--How many textures we have.
        uint32_t tHandle = sglGetUniformLocation(tProgramHandle, "uTotalTextures");
        sglUniform1i(tHandle, mCompressedTextureTable->GetListSize());

        //--Fill texture data.
        int i = 0;
        char tBuffer[32];
        SugarBitmap *rTexData = (SugarBitmap *)mCompressedTextureTable->PushIterator();
        while(rTexData)
        {
            //--Get handle.
            sprintf(tBuffer, "uClamps[%i]", i);
            uint32_t tHandle = sglGetUniformLocation(tProgramHandle, tBuffer);

            //--Compute.
            float tLft = (mTexCoordAtlas[i].mX) / (float)mTextureAtlas->GetWidth();
            float tTop = (mTexCoordAtlas[i].mY) / (float)mTextureAtlas->GetHeight();
            float tRgt = tLft + (rTexData->GetWidth()  / (float)mTextureAtlas->GetWidth());
            float tBot = tTop + (rTexData->GetHeight() / (float)mTextureAtlas->GetHeight());
            sglUniform4f(tHandle, tLft, tRgt, tTop, tBot);
            //fprintf(stderr, "%i: %f %f %f %f\n", i, tLft, tTop, tRgt, tBot);

            //--Next.
            i ++;
            rTexData = (SugarBitmap *)mCompressedTextureTable->AutoIterate();
        }

        //--Texture sampler indices. 0 is the texture, 1 is the lightmap.
        if(pNeedsTexSampler)
        {
            tHandle = sglGetUniformLocation(tProgramHandle, "xTexSampler");
            sglUniform1i(tHandle, 0);
            tHandle = sglGetUniformLocation(tProgramHandle, "xLghSampler");
            sglUniform1i(tHandle, 1);
        }
    }
}

//--[Execution Functions]
void WADFile::RenderBackground()
{
    //--The 'background' is the world geometry. This file format does not render a foreground since
    //  that's offloaded to the skybox.
    Render();
}
void WADFile::SetupFrustum()
{
    //--First part of the rendering cycle, sets up the 3D world frustum.
    DisplayManager::CleanOrtho();
    DisplayManager::StdFrustum();
}
void WADFile::SetupCamera()
{
    //--Second part of the rendering cycle, positions the camera in the world geometry.
    SugarCamera3D *r3DCamera = CameraManager::FetchActiveCamera3D();

    //--Get values.
    float tX, tY, tZ;
    r3DCamera->Get3DPosition(tX, tY, tZ);
    float tPitch, tYaw, tRoll;
    r3DCamera->Get3DRotation(tPitch, tYaw, tRoll);

    //--Size and position.
    float cScale = 0.05f;
    glScalef(cScale, cScale, cScale);
    glRotatef(tPitch, 1, 0, 0);
    glRotatef(tYaw, 0, 1, 0);
    glRotatef(tRoll, 0, 0, 1);
    glTranslatef(tX, tY, tZ);
}
void WADFile::CompileVertexData()
{
    //--To speed up rendering, we can compile all the information required to render the level into a Vertex Array Object.
    //  That's done here, and the Render() routine will automatically use it.
    DebugPush(WRENDER_DEBUG_COMPILE, "WADFile:CompileVertexData - Begin.\n");

    //--Constants.
    const int cFloatsPerVertexPos = 3;
    const int cFloatsPerVertexTex = 3;//3rd tex coord is the texture index.
    const int cFloatsPerVertexLgt = 2;
    const int cVerticesPerTriangle = 3;
    const size_t cSizePerEntry = (sizeof(float) * (cFloatsPerVertexPos + cFloatsPerVertexTex + cFloatsPerVertexLgt));

    //--Count how many triangles are needed to render. The polygons will decompose into triangles with a subroutine.
    DebugPrint("Counting total triangles.\n");
    mTriangleCount = 0;
    WorldPolygon *rPolygon = (WorldPolygon *)mArbitraryPolysConstructionList->PushIterator();
    while(rPolygon)
    {
        mTriangleCount += rPolygon->GetTriangleCount();
        rPolygon = (WorldPolygon *)mArbitraryPolysConstructionList->AutoIterate();
    }

    //--Allocate space.
    DebugPrint("Got %i triangles, allocating space.\n", mTriangleCount);
    size_t tArrayLength = mTriangleCount * cSizePerEntry * cVerticesPerTriangle;
    SetMemoryData(__FILE__, __LINE__);
    float *tArrayData = (float *)starmemoryalloc(tArrayLength);

    //--Populate the data.
    DebugPrint("Allocated %i bytes, populating data.\n", tArrayLength);
    int tCursor = 0;
    rPolygon = (WorldPolygon *)mArbitraryPolysConstructionList->PushIterator();
    while(rPolygon)
    {
        rPolygon->PopulateTriangleData(tCursor, tArrayData);
        rPolygon = (WorldPolygon *)mArbitraryPolysConstructionList->AutoIterate();
    }

    //--Generate the vertex buffer and set its basic parts.
    DebugPrint("Setting basic vertex buffer information.\n");
    sglGenVertexArrays(1, &mVertexBufferHandle);
    sglBindVertexArray(mVertexBufferHandle);

    //--Generate a buffer for the VAO to reference.
    DebugPrint("Generating attached buffer.\n");
    sglGenBuffers(1, &mDataBufferHandle);
    sglBindBuffer(GL_ARRAY_BUFFER, mDataBufferHandle);
    sglBufferData(GL_ARRAY_BUFFER, tArrayLength, tArrayData, GL_STATIC_DRAW);

    //--Bind everything for this VAO.
    DebugPrint("Filling stride data.\n");
    if(!xRenderLightmaps)
    {
        glVertexPointer(cFloatsPerVertexPos, GL_FLOAT, cSizePerEntry, 0);
        glTexCoordPointer(cFloatsPerVertexTex, GL_FLOAT, cSizePerEntry, (void *)(sizeof(float) * cFloatsPerVertexPos));
    }
    //--Multitexture version.
    else
    {
        //--Texture.
        sglClientActiveTexture(GL_TEXTURE0);
        glEnableClientState(GL_VERTEX_ARRAY);
        glEnableClientState(GL_TEXTURE_COORD_ARRAY);
        glVertexPointer(cFloatsPerVertexPos, GL_FLOAT, cSizePerEntry, 0);
        glTexCoordPointer(cFloatsPerVertexTex, GL_FLOAT, cSizePerEntry, (void *)(sizeof(float) * cFloatsPerVertexPos));

        //--Lightmap.
        sglClientActiveTexture(GL_TEXTURE1);
        glEnableClientState(GL_TEXTURE_COORD_ARRAY);
        glTexCoordPointer(cFloatsPerVertexLgt, GL_FLOAT, cSizePerEntry, (void *)(sizeof(float) * (cFloatsPerVertexPos + cFloatsPerVertexTex)));
        sglClientActiveTexture(GL_TEXTURE0);
    }

    //--Clean.
    DebugPrint("Cleaning up.\n");
    sglBindVertexArray(0);
    free(tArrayData);

    //--Finish up.
    DebugPop("WADFile:CompileVertexData - Complete.\n");
}
void WADFile::Render()
{
    //--Third part of the rendering cycle, renders all the world geometry.
    if(!mIsReady || !mHasActiveLevel) return;

    //--Face culling. Normals don't need to be computed here, as the poly generator should have flipped
    //  vertices for everything correctly.
    glEnable(GL_CULL_FACE);
    glCullFace(GL_FRONT); //Winding order is flipped in Starlight Engine since the Y coordinate is flipped.

    //--If we have a vertex array object to handle this, use that. This requires atlases to have been build
    //  since VAOs can't bind textures while rendering. Atlases are built during geometry loading.
    if(xRenderWithVAOs && mTextureAtlas && mLightmapAtlas)
    {
        //--Setup the GL state.
        sglActiveTexture(GL_TEXTURE0);
        glEnable(GL_TEXTURE_2D);
        mTextureAtlas->Bind();

        sglActiveTexture(GL_TEXTURE1);
        glEnable(GL_TEXTURE_2D);
        mLightmapAtlas->Bind();

        //--If the data hasn't been compiled yet, do that here.
        if(!mVertexBufferHandle) CompileVertexData();

        //--Render it.
        if(mVertexBufferHandle)
        {
            //--Activate and send shader data to the graphics card. 0th is the standard shader.
            if(mShaderCode == 0)
            {
                DisplayManager::Fetch()->ActivateProgram("WADFile RenderAtlasLightmap");
            }
            //--White code.
            else if(mShaderCode == 1)
            {
                DisplayManager::Fetch()->ActivateProgram("WADFile RenderAtlasLightmapWhite");
            }
            //--Use zeroth texture.
            else if(mShaderCode == 2)
            {
                DisplayManager::Fetch()->ActivateProgram("WADFile RenderAtlasLightmapZero");
            }
            UploadShaderData(true);

            //--Render.
            sglBindVertexArray(mVertexBufferHandle);
            glDrawArrays(GL_TRIANGLES, 0, mTriangleCount * 3);

            //--Clean.
            DisplayManager::Fetch()->ActivateProgram(NULL);
        }

        sglClientActiveTexture(GL_TEXTURE0);
        sglActiveTexture(GL_TEXTURE1);
        glDisable(GL_TEXTURE_2D);
        sglActiveTexture(GL_TEXTURE0);
    }
    //--If this has been done in a list, we can stop here. The list does it for us. If the flag WRENDER_ALLOW_GLLIST
    //  is not defined, the list will never be generated.
    else if(mListHandle)
    {
        glCallList(mListHandle);
        glDisable(GL_CULL_FACE);
        if(mTextureAtlas)
        {
            glTranslatef(3000.0f, 3000.0f, 10.0f);
            glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
            mTextureAtlas->Draw();
            glRotatef(90.0f, -1.0f, 0.0f, 0.0f);
            glTranslatef(-3000.0f, -3000.0f, -10.0f);
        }
        glEnable(GL_CULL_FACE);
    }
    //--Arbitrary polygon rendering mode.
    else if(mArbitraryPolysConstructionList)
    {
        //fprintf(stderr, " Arbitrary poly.\n");
        //--Generate a new list.
        #ifdef WRENDER_ALLOW_GLLIST
            mListHandle = glGenLists(1);
            glNewList(mListHandle, GL_COMPILE_AND_EXECUTE);
        #endif

        //--Full render. Handles textures and lightmaps.
        if(xRenderTextures && xRenderLightmaps)
        {
            //--Activate and send shader data to the graphics card. 0th is the standard shader.
            if(mShaderCode == 0)
            {
                DisplayManager::Fetch()->ActivateProgram("WADFile RenderAtlasLightmap");
            }
            //--White code.
            else if(mShaderCode == 1)
            {
                DisplayManager::Fetch()->ActivateProgram("WADFile RenderAtlasLightmapWhite");
            }
            //--Use zeroth texture.
            else if(mShaderCode == 2)
            {
                DisplayManager::Fetch()->ActivateProgram("WADFile RenderAtlasLightmapZero");
            }
            UploadShaderData(true);
            RenderEverything();
        }
        //--Render fullbright textures, no lightmaps necessary.
        else if(xRenderTextures && !xRenderLightmaps)
        {
            if(mShaderCode == 0)
            {
                DisplayManager::Fetch()->ActivateProgram("WADFile RenderAtlas");
            }
            //--White code.
            else if(mShaderCode == 1)
            {
                DisplayManager::Fetch()->ActivateProgram("WADFile RenderAtlasWhite");
            }
            //--Use zeroth texture.
            else if(mShaderCode == 2)
            {
                DisplayManager::Fetch()->ActivateProgram("WADFile RenderAtlasZero");
            }
            UploadShaderData(true);
            RenderFullbright();
        }
        //--Render lightmaps with no textures.
        else if(!xRenderTextures && xRenderLightmaps)
        {
            RenderLightmaps();
        }

        //--Deactivate the program.
        DisplayManager::Fetch()->ActivateProgram(NULL);

        //--Set for skybox, if we have one.
        if(mLocalSkybox) DeactivateSkyboxStencil();

        //--If no rendering was set, or the static flag is set to always render wireframes, do that.
        if(xAlwaysRenderWireframes || (!xRenderTextures && !xRenderLightmaps))
        {
            glLineWidth(3.0f);
            RenderWireframes();
        }

        //--Finish up.
        #ifdef WRENDER_ALLOW_GLLIST
        glEndList();
        #endif
    }
    //--Subdivision has not occurred yet, so render the primitive view. This view often gets some
    //  geometry wrong due to limitations in non-UDMF maps.
    else
    {
        //--Generate a new list.
        #ifdef WRENDER_ALLOW_GLLIST
            mListHandle = glGenLists(1);
            glNewList(mListHandle, GL_COMPILE_AND_EXECUTE);
        #endif

        //--Render.
        RenderPrimitive();

        //--Finish up.
        #ifdef WRENDER_ALLOW_GLLIST
            glEndList();
        #endif
    }

    //--Clean up.
    glDisable(GL_CULL_FACE);

    //--Render world sprites in their own rendering pass, independent of geometry.
    RenderWorldSprites();
}
void WADFile::RenderEverything()
{
    //--Used for most normal situations, renders textures with lightmaps over them. Uses multi-texture to render, and
    //  is improved with a shader to increase speed and allow texture atlasing.

    //--Upload the necessary information for the shader.
    UploadShaderData(true);

    //--Setup texture 0, which is used for face textures.
    sglActiveTexture(GL_TEXTURE0);
    glEnable(GL_TEXTURE_2D);
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
    glTexEnvf(GL_TEXTURE_ENV, GL_COMBINE_RGB, GL_REPLACE);

    //--If we have a texture atlas, bind this to texture 0.
    if(mTextureAtlas) mTextureAtlas->Bind();

    //--Setup texture 1, which is the lightmap information.
    sglActiveTexture(GL_TEXTURE1);
    glEnable(GL_TEXTURE_2D);
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_COMBINE);
    glTexEnvf(GL_TEXTURE_ENV, GL_COMBINE_RGB, GL_MODULATE);

    //--If we have a lightmap atlas, bind that to texture 1.
    if(mLightmapAtlas) mLightmapAtlas->Bind();

    //--Iterate.
    WorldPolygon *rWorldPolygon = (WorldPolygon *)mArbitraryPolysConstructionList->PushIterator();
    while(rWorldPolygon)
    {
        //--Must have both a lightmap and a texture. In atlas mode, this is ignored.
        if((!rWorldPolygon->mLightmap || !rWorldPolygon->rTexture) && !mLightmapAtlas && !mTextureAtlas)
        {
            rWorldPolygon = (WorldPolygon *)mArbitraryPolysConstructionList->AutoIterate();
            continue;
        }

        //--If the texture happens to be the sky texture, don't render it.
        if(rWorldPolygon->rTexture == rSkyTexture)
        {
            rWorldPolygon = (WorldPolygon *)mArbitraryPolysConstructionList->AutoIterate();
            continue;
        }

        //--Bind the base texture.
        if(!mTextureAtlas)
        {
            sglActiveTexture(GL_TEXTURE0);
            rWorldPolygon->rTexture->Bind();
        }

        //--Expected Z position.
        float cUseZ = 0.0f;
        if(mTextureAtlas)
        {
            cUseZ = rWorldPolygon->mTemporaryTextureIndex + WP_GFX_CARD_OFFSET;
        }
        else
        {
            rWorldPolygon->rTexture->Bind();
        }

        //--Bind the lightmap. Doesn't do anything in atlas mode.
        if(!mLightmapAtlas)
        {
            sglActiveTexture(GL_TEXTURE1);
            rWorldPolygon->mLightmap->Bind();
        }

        //--Render coordinates.
        glBegin(GL_POLYGON);
            for(int p = 0; p < rWorldPolygon->mPointsTotal; p ++)
            {
                sglMultiTexCoord3f(GL_TEXTURE0, rWorldPolygon->mTexCoords[p].mX,      rWorldPolygon->mTexCoords[p].mY, cUseZ);
                sglMultiTexCoord2f(GL_TEXTURE1, rWorldPolygon->mLightmapCoords[p].mX, rWorldPolygon->mLightmapCoords[p].mY);
                glVertex3f(rWorldPolygon->mPoints[p].mX, rWorldPolygon->mPoints[p].mY, rWorldPolygon->mPoints[p].mZ);
            }
        glEnd();

        //--Render the normal.
        if(WADFile::xRenderNormals || true)
        {
            float cNormalVisFactor = 12.0f;
            glDisable(GL_TEXTURE_2D);
            glBegin(GL_LINES);
                glVertex3f(rWorldPolygon->mAverage.mX,                                              rWorldPolygon->mAverage.mY,                                              rWorldPolygon->mAverage.mZ);
                glVertex3f(rWorldPolygon->mAverage.mX + rWorldPolygon->mNormal.mX*cNormalVisFactor, rWorldPolygon->mAverage.mY + rWorldPolygon->mNormal.mY*cNormalVisFactor, rWorldPolygon->mAverage.mZ + rWorldPolygon->mNormal.mZ*cNormalVisFactor);
            glEnd();
            glEnable(GL_TEXTURE_2D);
        }

        //--Next.
        rWorldPolygon = (WorldPolygon *)mArbitraryPolysConstructionList->AutoIterate();
    }

    //--Clean back to single texture more.
    sglActiveTexture(GL_TEXTURE1);
    glDisable(GL_TEXTURE_2D);
    sglActiveTexture(GL_TEXTURE0);
    glEnable(GL_TEXTURE_2D);
}
void WADFile::RenderFullbright()
{
    //--Used when lightmaps are not generated. Renders everything at maximum brightness.
    UploadShaderData(false);

    //--Binding case.
    if(mTextureAtlas) mTextureAtlas->Bind();

    //--Iterate.
    WorldPolygon *rWorldPolygon = (WorldPolygon *)mArbitraryPolysConstructionList->PushIterator();
    while(rWorldPolygon)
    {
        //--Error check.
        if(!rWorldPolygon->rTexture && !mTextureAtlas)
        {
            rWorldPolygon = (WorldPolygon *)mArbitraryPolysConstructionList->AutoIterate();
            continue;
        }

        //--Don't render the sky texture.
        if(rWorldPolygon->rTexture == rSkyTexture)
        {
            rWorldPolygon = (WorldPolygon *)mArbitraryPolysConstructionList->AutoIterate();
            continue;
        }

        //--Bind.
        float cUseTexture = 0.0f;
        if(mTextureAtlas)
        {
            cUseTexture = (float)rWorldPolygon->mTemporaryTextureIndex + WP_GFX_CARD_OFFSET;
        }
        else
        {
            rWorldPolygon->rTexture->Bind();
        }

        //--Three sided mode?
        if(rWorldPolygon->mPointsTotal == 3)
        {
            glBegin(GL_TRIANGLES);
                glTexCoord3f(rWorldPolygon->mTexCoords[0].mX, rWorldPolygon->mTexCoords[0].mY, cUseTexture); glVertex3f(rWorldPolygon->mPoints[0].mX, rWorldPolygon->mPoints[0].mY, rWorldPolygon->mPoints[0].mZ);
                glTexCoord3f(rWorldPolygon->mTexCoords[1].mX, rWorldPolygon->mTexCoords[1].mY, cUseTexture); glVertex3f(rWorldPolygon->mPoints[1].mX, rWorldPolygon->mPoints[1].mY, rWorldPolygon->mPoints[1].mZ);
                glTexCoord3f(rWorldPolygon->mTexCoords[2].mX, rWorldPolygon->mTexCoords[2].mY, cUseTexture); glVertex3f(rWorldPolygon->mPoints[2].mX, rWorldPolygon->mPoints[2].mY, rWorldPolygon->mPoints[2].mZ);
            glEnd();
        }
        //--Four sided.
        else if(rWorldPolygon->mPointsTotal == 4)
        {
            glBegin(GL_QUADS);
                glTexCoord3f(rWorldPolygon->mTexCoords[0].mX, rWorldPolygon->mTexCoords[0].mY, cUseTexture); glVertex3f(rWorldPolygon->mPoints[0].mX, rWorldPolygon->mPoints[0].mY, rWorldPolygon->mPoints[0].mZ);
                glTexCoord3f(rWorldPolygon->mTexCoords[1].mX, rWorldPolygon->mTexCoords[1].mY, cUseTexture); glVertex3f(rWorldPolygon->mPoints[1].mX, rWorldPolygon->mPoints[1].mY, rWorldPolygon->mPoints[1].mZ);
                glTexCoord3f(rWorldPolygon->mTexCoords[2].mX, rWorldPolygon->mTexCoords[2].mY, cUseTexture); glVertex3f(rWorldPolygon->mPoints[2].mX, rWorldPolygon->mPoints[2].mY, rWorldPolygon->mPoints[2].mZ);
                glTexCoord3f(rWorldPolygon->mTexCoords[3].mX, rWorldPolygon->mTexCoords[3].mY, cUseTexture); glVertex3f(rWorldPolygon->mPoints[3].mX, rWorldPolygon->mPoints[3].mY, rWorldPolygon->mPoints[3].mZ);
            glEnd();
        }
        //--Polygon with arbitrary number of sides.
        else if(rWorldPolygon->mPointsTotal >= 5)
        {
            glBegin(GL_POLYGON);
                for(int p = 0; p < rWorldPolygon->mPointsTotal; p ++)
                {
                    //--Texture.
                    glTexCoord3f(rWorldPolygon->mTexCoords[p].mX, rWorldPolygon->mTexCoords[p].mY, cUseTexture);
                    glVertex3f(rWorldPolygon->mPoints[p].mX, rWorldPolygon->mPoints[p].mY, rWorldPolygon->mPoints[p].mZ);
                }
            glEnd();
        }

        //--Next.
        rWorldPolygon = (WorldPolygon *)mArbitraryPolysConstructionList->AutoIterate();
    }
}
void WADFile::RenderLightmaps()
{
    //--Used when lightmaps are generated but textures are not set to render. Useful for debugging lightmaps.

    //--Binding case.
    if(mLightmapAtlas) mLightmapAtlas->Bind();

    //--Iterate.
    WorldPolygon *rWorldPolygon = (WorldPolygon *)mArbitraryPolysConstructionList->PushIterator();
    while(rWorldPolygon)
    {
        //--Error check.
        if(!rWorldPolygon->mLightmap && !mLightmapAtlas)
        {
            rWorldPolygon = (WorldPolygon *)mArbitraryPolysConstructionList->AutoIterate();
            continue;
        }

        //--Bind.
        if(!mLightmapAtlas) rWorldPolygon->mLightmap->Bind();

        //--Three sided mode?
        if(rWorldPolygon->mPointsTotal == 3)
        {
            glBegin(GL_TRIANGLES);
                glTexCoord2f(rWorldPolygon->mLightmapCoords[0].mX, rWorldPolygon->mLightmapCoords[0].mY); glVertex3f(rWorldPolygon->mPoints[0].mX, rWorldPolygon->mPoints[0].mY, rWorldPolygon->mPoints[0].mZ);
                glTexCoord2f(rWorldPolygon->mLightmapCoords[1].mX, rWorldPolygon->mLightmapCoords[1].mY); glVertex3f(rWorldPolygon->mPoints[1].mX, rWorldPolygon->mPoints[1].mY, rWorldPolygon->mPoints[1].mZ);
                glTexCoord2f(rWorldPolygon->mLightmapCoords[2].mX, rWorldPolygon->mLightmapCoords[2].mY); glVertex3f(rWorldPolygon->mPoints[2].mX, rWorldPolygon->mPoints[2].mY, rWorldPolygon->mPoints[2].mZ);
            glEnd();
        }
        //--Four sided.
        else if(rWorldPolygon->mPointsTotal == 4)
        {
            glBegin(GL_QUADS);
                glTexCoord2f(rWorldPolygon->mLightmapCoords[0].mX, rWorldPolygon->mLightmapCoords[0].mY); glVertex3f(rWorldPolygon->mPoints[0].mX, rWorldPolygon->mPoints[0].mY, rWorldPolygon->mPoints[0].mZ);
                glTexCoord2f(rWorldPolygon->mLightmapCoords[1].mX, rWorldPolygon->mLightmapCoords[1].mY); glVertex3f(rWorldPolygon->mPoints[1].mX, rWorldPolygon->mPoints[1].mY, rWorldPolygon->mPoints[1].mZ);
                glTexCoord2f(rWorldPolygon->mLightmapCoords[2].mX, rWorldPolygon->mLightmapCoords[2].mY); glVertex3f(rWorldPolygon->mPoints[2].mX, rWorldPolygon->mPoints[2].mY, rWorldPolygon->mPoints[2].mZ);
                glTexCoord2f(rWorldPolygon->mLightmapCoords[3].mX, rWorldPolygon->mLightmapCoords[3].mY); glVertex3f(rWorldPolygon->mPoints[3].mX, rWorldPolygon->mPoints[3].mY, rWorldPolygon->mPoints[3].mZ);
            glEnd();
        }
        //--Polygon with arbitrary number of sides.
        else if(rWorldPolygon->mPointsTotal >= 5)
        {
            glBegin(GL_POLYGON);
                for(int p = 0; p < rWorldPolygon->mPointsTotal; p ++)
                {
                    //--Texture.
                    glTexCoord2f(rWorldPolygon->mLightmapCoords[p].mX, rWorldPolygon->mLightmapCoords[p].mY);
                    glVertex3f(rWorldPolygon->mPoints[p].mX, rWorldPolygon->mPoints[p].mY, rWorldPolygon->mPoints[p].mZ);
                }
            glEnd();
        }

        //--Next.
        rWorldPolygon = (WorldPolygon *)mArbitraryPolysConstructionList->AutoIterate();
    }
}
void WADFile::RenderWireframes()
{
    //--Used when neither textures nor lightmaps are set to render. Useful for debugging level geometry.
    glColor3f(0.0f, 1.0f, 0.0f);
    glDisable(GL_TEXTURE_2D);

    //--Iterate.
    WorldPolygon *rWorldPolygon = (WorldPolygon *)mArbitraryPolysConstructionList->PushIterator();
    while(rWorldPolygon)
    {
        //--Render a loop around the polygon. This does not require them to be convex or monotone.
        glBegin(GL_LINE_LOOP);
            for(int i = 0; i < rWorldPolygon->mPointsTotal; i ++)
            {
                glVertex3f(rWorldPolygon->mPoints[i].mX, rWorldPolygon->mPoints[i].mY, rWorldPolygon->mPoints[i].mZ);
            }
        glEnd();

        //--Next.
        rWorldPolygon = (WorldPolygon *)mArbitraryPolysConstructionList->AutoIterate();
    }

    //--Clean up.
    glEnable(GL_TEXTURE_2D);
}
void WADFile::RenderPrimitive()
{
    //--If subdivision has not occurred yet, this function is called. It will render the world using the raw polygon
    //  information, which often gets some things wrong since this information will not take into account UDMF behavior.
    //  Slopes will be incorrect, 3D walls may face the wrong direction, and so on.
    //--However, this is probably the fastest non-wireframe method. Good for previews.

    //--GL render state.
    glEnable(GL_TEXTURE_2D);
    glColor3f(1.0f, 1.0f, 1.0f);

    //--Render textured geometry.
    if(mIsPrecacheReady)
    {
        //--Set for skybox, if we have one.
        if(mLocalSkybox) SetupSkyboxStencil();

        //--Floor polygons.
        for(int i = 0; i < mFloorPolysTotal; i ++)
        {
            //--Floor part.
            if(mFloorPolys[i].rFloorTexture)
            {
                //--If this is a sky texture, change the stencil behavior.
                SetSkyboxStencilState(mFloorPolys[i].rFloorTexture == rSkyTexture);

                //--Light coloring.
                if(mFloorPolys[i].mAssociatedSector >= 0 && mFloorPolys[i].mAssociatedSector < mSectorsTotal)
                {
                    float tLighting = (float)mSectors[mFloorPolys[i].mAssociatedSector].mLightLevel / 255.0f;
                    tLighting = tLighting * tLighting;
                    glColor3f(tLighting, tLighting, tLighting);
                }

                //--Render.
                mFloorPolys[i].rFloorTexture->Bind();
                glBegin(GL_POLYGON);
                    for(int p = 0; p < mFloorPolys[i].mVertexesTotal; p ++)
                    {
                        //--Light color.
                        glColor3f(mFloorPolys[i].mVertexColorsF[p].r, mFloorPolys[i].mVertexColorsF[p].g, mFloorPolys[i].mVertexColorsF[p].b);

                        //--Texture.
                        glTexCoord2f((mFloorPolys[i].mVertices[p].mX + mSectors[mFloorPolys[i].mAssociatedSector].mXOffsetF) / 64.0f, (mFloorPolys[i].mVertices[p].mY + mSectors[mFloorPolys[i].mAssociatedSector].mYOffsetF) / 64.0f);

                        //--Normal case.
                        if(!mFloorPolys[i].mVertices[p].mHasSpecialFloor)
                            glVertex3f(mFloorPolys[i].mVertices[p].mX, mFloorPolys[i].mVertices[p].mY, mFloorPolys[i].mFloorHeight);
                        else
                            glVertex3f(mFloorPolys[i].mVertices[p].mX, mFloorPolys[i].mVertices[p].mY, mFloorPolys[i].mVertices[p].mFloorZ);
                    }
                glEnd();
            }

            //--Ceiling part. Different Y, same points.
            if(mFloorPolys[i].rCeilingTexture)
            {
                //--If this is a sky texture, change the stencil behavior.
                SetSkyboxStencilState(mFloorPolys[i].rCeilingTexture == rSkyTexture);

                //--Light coloring.
                if(mFloorPolys[i].mAssociatedSector >= 0 && mFloorPolys[i].mAssociatedSector < mSectorsTotal)
                {
                    float tLighting = (float)mSectors[mFloorPolys[i].mAssociatedSector].mLightLevel / 255.0f;
                    tLighting = tLighting * tLighting;
                    glColor3f(tLighting, tLighting, tLighting);
                }

                //--Render. We always render in the opposite order here since ceilings are always wound backwards
                //  and use the same vertices as the floor. There's no way around this.
                mFloorPolys[i].rCeilingTexture->Bind();
                glBegin(GL_POLYGON);
                    for(int p = mFloorPolys[i].mVertexesTotal-1; p >= 0; p --)
                    {
                        //--Light color.
                        glColor3f(mFloorPolys[i].mVertexColorsC[p].r, mFloorPolys[i].mVertexColorsC[p].g, mFloorPolys[i].mVertexColorsC[p].b);

                        //--Texture.
                        glTexCoord2f((mFloorPolys[i].mVertices[p].mX + mSectors[mFloorPolys[i].mAssociatedSector].mXOffsetC) / 64.0f, (mFloorPolys[i].mVertices[p].mY + mSectors[mFloorPolys[i].mAssociatedSector].mYOffsetC) / 64.0f);

                        //--Normal case.
                        if(!mFloorPolys[i].mVertices[p].mHasSpecialCeiling)
                            glVertex3f(mFloorPolys[i].mVertices[p].mX, mFloorPolys[i].mVertices[p].mY, mFloorPolys[i].mCeilingHeight);
                        else
                            glVertex3f(mFloorPolys[i].mVertices[p].mX, mFloorPolys[i].mVertices[p].mY, mFloorPolys[i].mVertices[p].mCeilingZ);
                    }
                glEnd();
            }

            //--Subroutine, renders the 3D floors if any exist.
            Render3DFloor(&mFloorPolys[i]);
        }

        //--Clean pending lighting.
        glColor3f(1.0f, 1.0f, 1.0f);

        //--Wall Polygons.
        glEnable(GL_TEXTURE_2D);
        glColor3f(1.0f, 1.0f, 1.0f);
        for(int i = 0; i < mWallPolysTotal; i ++)
        {
            //--Normal case.
            if(mWallPolys[i].rTexture)
            {
                //--Bind.
                mWallPolys[i].rTexture->Bind();

                //--Quick-access pointers.
                float tTexLft = 0.0f;
                float tTexRgt = 1.0f;
                float tTexTop = 0.0f;
                float tTexBot = 1.0f;

                //--Texture vertical alignment.
                if(mWallPolys[i].mTexStoreH > 0)
                {
                    //--Bottom alignment (start at floor and render up)
                    if(mWallPolys[i].mIsBotAligned)
                    {
                        //--Base.
                        tTexTop = 1.0f;
                        tTexBot = tTexTop + ((mWallPolys[i].mVertZ0 - mWallPolys[i].mVertZ1) / mWallPolys[i].mTexStoreH);

                        //--Modify the alignment by the offset.
                        tTexTop = tTexTop + (mWallPolys[i].mYTexOffset / mWallPolys[i].mTexStoreH);
                        tTexBot = tTexBot + (mWallPolys[i].mYTexOffset / mWallPolys[i].mTexStoreH);
                    }
                    //--Top alignment (start at ceiling and render down)
                    else
                    {
                        //--Base.
                        tTexBot = 0.0f;
                        tTexTop = tTexBot - ((mWallPolys[i].mVertZ0 - mWallPolys[i].mVertZ1) / mWallPolys[i].mTexStoreH);

                        //--Modify the alignment by the offset.
                        tTexTop = tTexTop + (mWallPolys[i].mYTexOffset / mWallPolys[i].mTexStoreH);
                        tTexBot = tTexBot + (mWallPolys[i].mYTexOffset / mWallPolys[i].mTexStoreH);
                    }
                }

                //--Left alignment.
                if(mWallPolys[i].mTexStoreW > 0)
                {
                    //--Compute.
                    float tDistance = GetPlanarDistance(mWallPolys[i].mVertX0, mWallPolys[i].mVertY0, mWallPolys[i].mVertX1, mWallPolys[i].mVertY1);
                    tTexLft = (mWallPolys[i].mXTexOffset / mWallPolys[i].mTexStoreW);
                    tTexRgt = tTexLft + ((tDistance) / mWallPolys[i].mTexStoreW);
                }

                //--If this is a sky texture, change the stencil behavior.
                SetSkyboxStencilState(mWallPolys[i].rTexture == rSkyTexture);

                //--Light coloring.
                if(mWallPolys[i].mAssociatedSector >= 0 && mWallPolys[i].mAssociatedSector < mSectorsTotal)
                {
                    float tLighting = (float)mSectors[mWallPolys[i].mAssociatedSector].mLightLevel / 255.0f;
                    tLighting = tLighting * tLighting;
                    glColor3f(tLighting, tLighting, tLighting);
                }

                //--Render the quad.
                glBegin(GL_QUADS);
                    glTexCoord2f(tTexLft, tTexBot); glVertex3f(mWallPolys[i].mVertX0, mWallPolys[i].mVertY0, mWallPolys[i].mVertZ0);
                    glTexCoord2f(tTexRgt, tTexBot); glVertex3f(mWallPolys[i].mVertX1, mWallPolys[i].mVertY1, mWallPolys[i].mVertZ0);
                    glTexCoord2f(tTexRgt, tTexTop); glVertex3f(mWallPolys[i].mVertX1, mWallPolys[i].mVertY1, mWallPolys[i].mVertZ1);
                    glTexCoord2f(tTexLft, tTexTop); glVertex3f(mWallPolys[i].mVertX0, mWallPolys[i].mVertY0, mWallPolys[i].mVertZ1);
                glEnd();

                //--Clean.
                glStencilFunc(GL_ALWAYS, STENCIL_DONT_RENDER_SKY, 0xFF);
            }
            //--Texture not found. Render a bright pink.
            else
            {
                glColor3f(1.0f, 0.0f, 1.0f);
                glDisable(GL_TEXTURE_2D);
                glBegin(GL_QUADS);
                    glVertex3f(mWallPolys[i].mVertX0, mWallPolys[i].mVertY0, mWallPolys[i].mVertZ0);
                    glVertex3f(mWallPolys[i].mVertX1, mWallPolys[i].mVertY1, mWallPolys[i].mVertZ0);
                    glVertex3f(mWallPolys[i].mVertX1, mWallPolys[i].mVertY1, mWallPolys[i].mVertZ1);
                    glVertex3f(mWallPolys[i].mVertX0, mWallPolys[i].mVertY0, mWallPolys[i].mVertZ1);
                glEnd();
                glEnable(GL_TEXTURE_2D);
                glColor3f(1.0f, 1.0f, 1.0f);
            }
        }

        //--Clean up.
        if(mLocalSkybox) DeactivateSkyboxStencil();
    }
}
void WADFile::TakeDownCamera()
{
    //--Fourth part of the rendering cycle, repositions the camera back to 0,0,0. If you're going to
    //  render a skybox using stencilling, you would do it after the camera is reset.
    SugarCamera3D *r3DCamera = CameraManager::FetchActiveCamera3D();

    //--Get values.
    float cScale = 0.05f;
    float tX, tY, tZ;
    r3DCamera->Get3DPosition(tX, tY, tZ);
    float tPitch, tYaw, tRoll;
    r3DCamera->Get3DRotation(tPitch, tYaw, tRoll);

    //--Reset back to the zero position.
    glTranslatef(-tX, -tY, -tZ);
    glRotatef(tRoll, 0, 0, -1);
    glRotatef(tYaw, 0, -1, 0);
    glRotatef(tPitch, -1, 0, 0);
    glScalef(1.0f / cScale, 1.0f / cScale, 1.0f / cScale);
}
void WADFile::RenderSkybox()
{
    //--Optional skybox rendering. Should be done after the camera has been positioned at 0,0,0.
    if(!mLocalSkybox) return;

    //--Get values.
    float tPitch, tYaw, tRoll;
    SugarCamera3D *r3DCamera = CameraManager::FetchActiveCamera3D();
    r3DCamera->Get3DRotation(tPitch, tYaw, tRoll);

    //--Set rotations.
    glRotatef(tPitch, 1, 0, 0);
    glRotatef(tYaw, 0, 1, 0);
    glRotatef(tRoll, 0, 0, 1);

    //--Render.
    //mLocalSkybox->RenderWithStencil(STENCIL_DONT_RENDER_SKY);
    mLocalSkybox->Render();

    //--Unrotate.
    glRotatef(tRoll, 0, 0, 1);
    glRotatef(tYaw, 0, 1, 0);
    glRotatef(tPitch, 1, 0, 0);
}
void WADFile::TakeDownFrustum()
{
    //--Fifth and final part of the rendering cycle, resets the program back to orthographic rendering
    //  so you can render the UI.
    DisplayManager::CleanFrustum();
    DisplayManager::StdOrtho();
}
