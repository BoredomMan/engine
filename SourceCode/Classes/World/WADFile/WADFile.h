//--[WADFile]
//--Level file structure used by games like Doom and Hexen. Very old, doesn't support slopes natively
//  and has a million and a half weak spots. Notable because it's extremely low-tech and fast.
//--This structure is not typically used for the purposes of rendering, though it can be. Instead, in this
//  program you should use this to compile the file into another structure which renders more quickly.

#pragma once

//--[Includes]
#include "Definitions.h"
#include "Structures.h"
#include "RootLevel.h"
#include "WadStructures.h"
#include "UDMFStructures.h"

//--[Local Structures]
//--[Local Definitions]
#define STENCIL_NO_SPECIAL_RENDER 0
#define STENCIL_DONT_RENDER_SKY 1
class WorldPolygon;

//--[Class]
class WADFile : public RootLevel
{
    private:
    //--System
    bool mIsReady;
    int mShaderCode;

    //--Storage
    uint32_t mLumps;
    WAD_Directory_Entry *mDirectoryEntries;

    //--Levels
    uint32_t mLevelsTotal;
    WAD_Level *mLevels;

    //--Active Level
    bool mHasActiveLevel;
    int mLinedefsTotal;
    UDMFLinedef *mLinedefs;
    int mSidedefsTotal;
    UDMFSidedef *mSidedefs;
    int mVerticesTotal;
    UDMFVertex *mVertices;
    int mSectorsTotal;
    UDMFSector *mSectors;

    //--World Sprite Registry
    int mWorldSpritesTotal;
    WAD_WorldSprite *mWorldSprites;
    SugarLinkedList *mWorldSpriteConstructionList;

    //--Thing Lookups
    int mThingLookupsTotal;
    WAD_ThingLookup *mThingLookups;

    //--Lighting Information
    int mLightsTotal;
    WorldLight **mLightList;

    //--Compiled data
    SugarLinkedList *mArbitraryPolysConstructionList;
    SugarLinkedList *rActiveShadowList;
    int mHeightPolysTotal;
    WorldPolygon **rHeightPolys;

    //--PatchNames
    int mPatchesTotal;
    char **mPatchNames;

    //--Precached level data
    bool mIsPrecacheReady;
    int mFloorPolysTotal;
    WAD_FloorPoly *mFloorPolys;
    int mWallPolysTotal;
    WAD_WallPoly *mWallPolys;

    //--Palette data
    StarlightColor mPaletteData[256];

    //--Textures data
    SugarBitmap *rSkyTexture; //Only use this for comparison. Do NOT dereference!
    int mFlatTexturesTotal;
    WAD_Texture *mFlatTextures;
    int mWallTexturesTotal;
    WAD_Texture *mWallTextures;

    //--Global Texture Geometry
    bool mIsCompressedTableMasterCopy;
    SugarLinkedList *mCompressedTextureTable;
    SugarLinkedList *mGlobalTexturePolyList;

    //--Skybox.
    Skybox *mLocalSkybox;

    //--GL Rendering List
    Point3D *mTexCoordAtlas;
    SugarBitmap *mTextureAtlas;
    SugarBitmap *mLightmapAtlas;
    int mTriangleCount;
    uint32_t mListHandle;
    GLuint mDataBufferHandle;
    GLuint mVertexBufferHandle;

    protected:

    public:
    //--System
    WADFile();
    virtual ~WADFile();

    //--Public Variables
    static bool xIsRecurse;
    static float xStaticArea;
    static float xRunningArea;
    static bool xAllowDoomSkyHack;
    static bool xRenderWithVAOs;
    static bool xRenderNormals;
    static bool xRenderTextures;
    static bool xRenderLightmaps;
    static bool xAlwaysRenderWireframes;
    static bool xDisallowTextureAtlas;
    static bool xForceNPOTAtlas;

    //--Property Queries
    void GetPositionOfVertex(int pIndex, float &sX, float &sY);
    float GetHeightAtPoint(float pX, float pY);
    float GetHeightAtPoint(float pX, float pY, float pZ);
    int GetIndexOfCompressedTexture(SugarBitmap *pBitmap);
    bool IsSkyTexture(void *pCheckPtr);

    //--Manipulators
    void SetShaderCode(int pCode);

    //--Core Methods
    void Clear(bool pIsExitCase);
    void BuildCompressedTextureTable();
    WAD_Directory_Entry *FindLump(const char *pName);
    WAD_Directory_Entry *FindLumpNonFlat(const char *pName);
    void SetLevelPointers(int pNumber);
    void ActivateLevel(int pNumber);

    //--3D Floors
    void Render3DFloor(WAD_FloorPoly *pFloorPolygon);
    void Build3DFloorInfo();
    void BuildWallsFor3DFloors(WAD_FloorPoly *pSourcePoly, UDMFLinedef *pSourceLine, UDMFSector *pControlSector);

    //--Binary Mesh
    SugarLinkedList *BuildBinaryMesh(SugarLinkedList *pSectorPolyList);
    void Subdivide(float pLft, float pTop, float pRgt, float pBot, SugarLinkedList *pPolygonList, SugarLinkedList *pSectorPolyList);
    WAD_FloorPoly *BuildPolyFrom(float pCheckX, float pCheckY, SugarLinkedList *pSectorPolyList);

    //--Collision
    void BuildHeightLookups();

    //--Light Builder
    void BuildLighting();
    float GetCorrectZ(WAD_FloorPoly *pPolygon, int pIndex, bool pIsFloor);
    void SubdividePolygons();
    void ComputeNormals();
    void ComputeLights();

    //--Saving
    void SaveToFile(const char *pPath);
    void LoadFromFile(const char *pPath);
    void BuildTextureAtlas(int16_t *pWidths, int16_t *pHeights, uint8_t **pTextureData);
    void BuildLightmapAtlas(bool pBuildWhitePixel);

    //--Skybox
    Skybox *LiberateSkybox();
    void ReceiveSkybox(Skybox *pSkybox);
    void BuildSkyboxPattern(const char *pPattern);
    static void SetupSkyboxStencil();
    static void SetSkyboxStencilState(bool pShouldRenderSkybox);
    static void DeactivateSkyboxStencil();

    //--Textures
    int GetTexturesTotal();
    WAD_Texture *GetTextureEntry(int pSlot);
    SugarBitmap *GetFlatTexture(const char *pName);
    SugarBitmap *GetWallTexture(const char *pName);
    void LoadPatchNames();
    char **GetFlatTexturesNeeded(int &sTotalTextures);
    char **GetWallTexturesNeeded(int &sTotalTextures);
    void LoadTextures();
    void LoadFlats();
    void LoadWalls(bool pIsSecondRun);
    SugarBitmap *UploadWallTexture(WAD_Texture_Definition *pDefinition);
    void FloorsReResolveTextures();
    void WallsReResolveTextures();

    //--Polygons
    void ReprocessPolygon(WAD_FloorPoly *pPolygon, SugarLinkedList *pPolygonList);
    SugarLinkedList *GetPolygonData(WAD_FloorPoly *pPolygon);
    void EliminateBisectors(SugarLinkedList *pPolygonData);
    void ForceRightHandednessFP(WAD_FloorPoly *pPolygon);
    void ForceRightHandedness(SugarLinkedList *pPolygonData);
    bool CutInternalPolygons(WAD_FloorPoly *pPolygonData, SugarLinkedList *pPolyGlobalList);
    void MakeMonotone(SugarLinkedList *pPolygonData, SugarLinkedList *pOrigPolyData, SugarLinkedList *pPolyGlobalList, bool &sPerformedAnyAction, SugarLinkedList *pExternalPolys);
    void ReuploadPolygonData(WAD_FloorPoly *pPolygon, SugarLinkedList *pPolygonData);
    static bool DoesLineCrossAnyOther(float pXA, float pYA, float pXB, float pYB, SugarLinkedList *pPolygonData);
    void CheckInternalPolygons(WAD_FloorPoly *pPolygon, SugarLinkedList *pPolygonList);

    private:
    //--Precaching
    void ClearPrecache();
    void PrecacheFloors();
    WAD_FloorPoly *ScanPolygon(int pPolygonCode, int pStartLinedef, int pSector, int *pInvolvementLookups);
    int GetConnectingLinedef(int pVertex, int pStartLinedef, int pPrevLinedef, int pSectorNeeded, int *pInvolvementLookups);
    void PrecacheWalls();

    public:
    //--Shadows
    int DoesSegmentIntersectPolygon(float pX1, float pY1, float pZ1, float pX2, float pY2, float pZ2, WorldPolygon *pPolygon, float &sInterceptX, float &sInterceptY, float &sInterceptZ);
    bool DoesLineCrossFace(float pX1, float pY1, float pZ1, float pX2, float pY2, float pZ2, void *pExceptThis);

    //--Things
    void BuildThingLookups();
    void HandleUDMFThings(SugarLinkedList *pProtoList);
    void ProcessThingsLump(WAD_Directory_Entry *pThingEntry);
    void LoadThingSprites();
    SugarBitmap *LoadSprite(WAD_Directory_Entry *pSpriteEntry);
    void RenderWorldSprites();
    void BuildEnvironmentGeometry();
    void CreateEnvironmentPolyAt(float pX, float pY, float pZ, SugarBitmap *pImage);

    //--UDMF Handling
    void LoadAsUDMF(WAD_Directory_Entry *pEntry);
    void ParseUDMF(int pDataSize, uint8_t *pData);
    void RemoveComments(char *pBuffer, bool &sBlockCommentStatus);
    bool SplitToThree(char *pBaseBuffer, char *pFieldBuffer, char *pOperatorBuffer, char *pValueBuffer);
    void HandleThing(char *pField, char *pOperator, char *pValue, WAD_WorldSprite *pWorldSprite);
    void HandleVertex(char *pField, char *pOperator, char *pValue, UDMFVertex *pVertex);
    void HandleSidedef(char *pField, char *pOperator, char *pValue, UDMFSidedef *pSidedef);
    void HandleSector(char *pField, char *pOperator, char *pValue, UDMFSector *pSector);

    //--UDMF Conversaions
    void LoadVertexData(WAD_Directory_Entry *pVertexEntry);
    void LoadLinedefData(WAD_Directory_Entry *pLinedefEntry);
    void LoadSidedefData(WAD_Directory_Entry *pSidedefEntry);
    void LoadSectorData(WAD_Directory_Entry *pSectorEntry);

    //--Workers
    static int GetLineIntersection(float pXSA, float pYSA, float pXEA, float pYEA, float pXSB, float pYSB, float pXEB, float pYEB, float *pInterceptX, float *pInterceptY);
    WAD_FloorPoly *FindContainingFloorPoly(float pXPoint, float pYPoint, SugarLinkedList *pPolyList, bool pIsSubCall);
    WAD_FloorPoly *FindContainingFloorPoly(float pXPoint, float pYPoint);
    static bool MarkDuplicateInternals(SugarLinkedList *pPolygonList);
    static bool CompareVertices(WAD_FloorPoly *pPolyA, WAD_FloorPoly *pPolyB);
    static void MarkPoly(int pPolyCode, SugarLinkedList *pSearchList);

    //--Update
    //--File I/O
    void ParseFile(const char *pPath);
    void MergeLumps(int pLumpsTotal, WAD_Directory_Entry *pLumps);
    void BeginLevel(int pEntry);

    //--Drawing
    virtual void RenderBackground();
    void UploadShaderData(bool pNeedsTexSampler);
    void SetupFrustum();
    void SetupCamera();
    void Render();
    void RenderEverything();
    void RenderFullbright();
    void RenderLightmaps();
    void RenderWireframes();
    void RenderPrimitive();
    void TakeDownCamera();
    void RenderSkybox();
    void TakeDownFrustum();
    void CompileVertexData();

    //--Pointer Routing
    //--Static Functions
    static WADFile *CreateFromFile(const char *pPath);

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions

