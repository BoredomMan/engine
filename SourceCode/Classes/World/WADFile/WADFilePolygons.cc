//--Base
#include "WADFile.h"

//--Classes
//--CoreClasses
#include "SugarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "HitDetection.h"

//--GUI
//--Libraries
//--Managers
#include "DebugManager.h"

//--[Forward Declarations]
int get_line_intersection(float p0_x, float p0_y, float p1_x, float p1_y, float p2_x, float p2_y, float p3_x, float p3_y, float *i_x, float *i_y);

bool IsPolygonWithin(WAD_FloorPoly *pPolygonExternal, WAD_FloorPoly *pPolygonInternal)
{
    //--Returns true if the polygon provided is within the polygon on the outside. Note that we are not interested
    //  in the reverse case, this is only if Internal is inside External.
    if(!pPolygonExternal || !pPolygonInternal || pPolygonExternal == pPolygonInternal) return false;

    //--Check if any of the lines crossed. If they did, then the polygons can't be wholly inside one another.
    for(int i = 0; i < pPolygonExternal->mVertexesTotal; i ++)
    {
        for(int p = 0; p < pPolygonInternal->mVertexesTotal; p ++)
        {
            //--Fast access.
            int tNextPointI = (i + 1) % pPolygonExternal->mVertexesTotal;
            int tNextPointP = (p + 1) % pPolygonInternal->mVertexesTotal;

            //--Check for a cross. Colinear counts!
            float tInterceptX, tInterceptY;
            int tCheck = get_line_intersection(pPolygonExternal->mVertices[i].mX, pPolygonExternal->mVertices[i].mY, pPolygonExternal->mVertices[tNextPointI].mX, pPolygonExternal->mVertices[tNextPointI].mY, pPolygonInternal->mVertices[p].mX, pPolygonInternal->mVertices[p].mY, pPolygonInternal->mVertices[tNextPointP].mX, pPolygonInternal->mVertices[tNextPointP].mY, &tInterceptX, &tInterceptY);
            if(tCheck == 1)
            {
                return false;
            }
        }
    }

    //--If we got this far, then there were no crossing lines. Next we need to check if any one point on the internal
    //  polygon is inside the external one. For simplicity, we check slot 0.
    float tTargetPointX = pPolygonInternal->mVertices[0].mX;
    float tTargetPointY = pPolygonInternal->mVertices[0].mY;
    bool tIsWithinPoly = false;

    //--For each vertex on the polygon, check it against the next vertex.
    for(int i = 0; i < pPolygonExternal->mVertexesTotal; i ++)
    {
        //--Get the slot of the next vertex.
        int j = ((i + 1) % pPolygonExternal->mVertexesTotal);

        //--Checks.
        if(((pPolygonExternal->mVertices[i].mY > tTargetPointY) != (pPolygonExternal->mVertices[j].mY > tTargetPointY)) &&
           (tTargetPointX < (pPolygonExternal->mVertices[j].mX - pPolygonExternal->mVertices[i].mX) * (tTargetPointY - pPolygonExternal->mVertices[i].mY) / (pPolygonExternal->mVertices[j].mY - pPolygonExternal->mVertices[i].mY) + pPolygonExternal->mVertices[i].mX))
            tIsWithinPoly = !tIsWithinPoly;
    }

    //--Done.
    return tIsWithinPoly;
}

void WADFile::CheckInternalPolygons(WAD_FloorPoly *pPolygon, SugarLinkedList *pPolygonList)
{
    //--Given a polygon, scans the provided list of other polygons to see if any of them are completely
    //  surrounded by this polygon. The WAD_FloorPoly structure has a linked-list inside it which is
    //  used exclusively for this, and should be NULL when this function is called. A reference to all
    //  polygons which are surrounded will be added to that list.
    //--If no polygons were surrounded, that list be empty, but it will still have been initialized.
    if(!pPolygon || !pPolygonList) return;
    return;

    //--If the list already existed, delete it.
    delete pPolygon->mInternalPolygons;

    //--Boot the list.
    pPolygon->mInternalPolygons = new SugarLinkedList(true);

    //--Iterate across the polygon list.
    int tPolyCount = 0;
    WAD_FloorPoly *rPolygon = (WAD_FloorPoly *)pPolygonList->PushIterator();
    while(rPolygon)
    {
        //--Run the subroutine.
        if(IsPolygonWithin(pPolygon, rPolygon) && !CompareVertices(pPolygon, rPolygon))
        {
            //--Add it.
            pPolygon->mInternalPolygons->AddElementAsTail("X", rPolygon->Clone(), &WAD_FloorPoly::DeleteThis);
            //fprintf(stderr, "Polygon %i/%i is within %i/%i.\n", rPolygon->mPolygonCode, rPolygon->mAssociatedSector, pPolygon->mPolygonCode, pPolygon->mAssociatedSector);

            //--If it happens to match our sector identity, it's one of the legal internal duplicates.
            //  Flag it as such.
            if(rPolygon->mAssociatedSector == pPolygon->mAssociatedSector)
            {
                rPolygon->rFloorTexture = NULL;
            }
        }

        //--Next polygon.
        tPolyCount ++;
        rPolygon = (WAD_FloorPoly *)pPolygonList->AutoIterate();
    }

    //--If we have exactly one contained polygon, tell it that we contain it.
    if(pPolygon->mInternalPolygons->GetListSize() == 1)
    {
        WAD_FloorPoly *rInternalPoly = (WAD_FloorPoly *)pPolygon->mInternalPolygons->GetElementBySlot(0);
        rInternalPoly->rEsconcingPolygon = pPolygon;
    }

    //--Now that we've gathered a list of all the polygons that are within us, we need to remove any polygons
    //  that are also within a polygon that we contain. That is, if we contain polygons B, C, and D, but polygon
    //  B contains polygon C, then we want to remove polygon D.
    //--There is no need to do this if we don't contain at least 2 polygons, obviously.
    if(pPolygon->mInternalPolygons->GetListSize() < 2) return;

    //--Debug.
    SugarLinkedList *rInternalPolyList = pPolygon->mInternalPolygons;
    //fprintf(stderr, "Before cleanup, this polygon has %i polygons inside it.\n", rInternalPolyList->GetListSize());

    //--Because we're operating on a list that is modifying itself, we need to keep running pulses.
    bool tDidAnything = true;
    while(tDidAnything)
    {
        //--Flag.
        tDidAnything = false;

        //--Reset the SLL to its head.
        WAD_FloorPoly *rInternalPolygon = (WAD_FloorPoly *)rInternalPolyList->SetToHeadAndReturn();
        while(rInternalPolygon)
        {
            //--Run through all the polygons. If a polygon is inside both this and the internal polygon, remove
            //  it from the external list.
            //--Note that the list may not have been built yet for the internal polygon, so we need to
            //  check internality directly instead of list-referencing.
            WAD_FloorPoly *rCheckingPolygon = (WAD_FloorPoly *)pPolygonList->PushIterator();
            while(rCheckingPolygon)
            {
                //--It's within both, so remove it from the outermost polygon's list.
                if(rInternalPolyList->IsElementOnList(rCheckingPolygon) && IsPolygonWithin(rInternalPolygon, rCheckingPolygon))
                {
                    //fprintf(stderr, "Polygon %p is within both %p and %p, removing.\n", rCheckingPolygon, pPolygon, rInternalPolygon);
                    tDidAnything = true;
                    rInternalPolyList->RemoveElementP(rCheckingPolygon);
                    pPolygonList->PopIterator();
                    break;
                }

                //--Otherwise, go to the next polygon.
                rCheckingPolygon = (WAD_FloorPoly *)pPolygonList->AutoIterate();
            }

            //--If we did anything, stop here.
            if(tDidAnything) break;

            //--Next polygon.
            rInternalPolygon = (WAD_FloorPoly *)rInternalPolyList->IncrementAndGetRandomPointerEntry();
        }
    }

    //--Debug.
    //fprintf(stderr, "After cleanup, this polygon has %i polygons inside it.\n", rInternalPolyList->GetListSize());

    //--All remaining polygons must be informed that we contain them.
    WAD_FloorPoly *rInternalPoly = (WAD_FloorPoly *)rInternalPolyList->PushIterator();
    while(rInternalPoly)
    {
        rInternalPoly->rEsconcingPolygon = pPolygon;
        rInternalPoly = (WAD_FloorPoly *)rInternalPolyList->AutoIterate();
    }
}

void WADFile::ReprocessPolygon(WAD_FloorPoly *pPolygon, SugarLinkedList *pPolygonList)
{
    //--Given a particular polygon, removes all non-convex parts of it and adds them as their
    //  own polygons to the list. In the end, the polygon is also added to the list.
    if(!pPolygon || !pPolygonList) return;

    //--If the polygon has already been reprocessed, we can skip this.
    if(pPolygon->mHasBeenReprocessed == true) return;

    //--A polygon cannot be reprocessed if it contains another polygon. Stop here.
    if(pPolygon->mInternalPolygons && pPolygon->mInternalPolygons->GetListSize() > 0) return;
    pPolygon->mHasBeenReprocessed = true;

    //--If this polygon is wholly esconced by another, then that polygon must always reprocess before this one.
    if(pPolygon->rEsconcingPolygon) ReprocessPolygon(pPolygon->rEsconcingPolygon, pPolygonList);

    //--Debug.
    DebugManager::PushPrint(false, "[Reprocessing Polygon] with %i vertices.\n", pPolygon->mVertexesTotal);
    DebugManager::Print("There are currently %i polygons active.\n", pPolygonList->GetListSize());
    DebugManager::Print("Polygon vertices: %i.\n", pPolygon->mVertexesTotal);
    for(int i = 0; i < pPolygon->mVertexesTotal; i ++)
    {
        DebugManager::Print(" %i: %.0f %.0f\n", i, pPolygon->mVertices[i].mX, pPolygon->mVertices[i].mY);
    }

    //--The tPolygonSplitList stores a final list of all the polygons created by slicing up this one.
    SugarLinkedList *tPolygonSplitList = new SugarLinkedList(true);

    //--First convert the polygon to SLL format so it's easier to edit.
    SugarLinkedList *tPolygonData = GetPolygonData(pPolygon);
    if(!tPolygonData)
    {
        DebugManager::Pop();
        return;
    }

    //--Eliminate bisectors to simplify the polygon data.
    EliminateBisectors(tPolygonData);

    //--Make sure the polygon is right-handed. Monotone calcs fail on left-handed polys.
    ForceRightHandedness(tPolygonData);

    //--If after eliminating bisectors, the polygon is now down to 3 or 4 points...
    if(tPolygonData->GetListSize() == 3 || tPolygonData->GetListSize() == 4)
    {
        ReuploadPolygonData(pPolygon, tPolygonData);
        pPolygonList->AddElement("X", pPolygon, &FreeThis);
        pPolygon->mHasBeenReprocessed = true;
        DebugManager::Pop();
        return;
    }

    //--Create a temporary copy of the original polygon data. This will be used for checking if lines
    //  overlap later on (if vertices are removed, it changes the polygon).
    SugarLinkedList *tPolyCopy = new SugarLinkedList(true);
    UDMFVertex *rVertex = (UDMFVertex *)tPolygonData->PushIterator();
    while(rVertex)
    {
        SetMemoryData(__FILE__, __LINE__);
        UDMFVertex *nCopyVertex = (UDMFVertex *)starmemoryalloc(sizeof(UDMFVertex));
        memcpy(nCopyVertex, rVertex, sizeof(UDMFVertex));
        nCopyVertex->mX = rVertex->mX;
        nCopyVertex->mY = rVertex->mY;

        tPolyCopy->AddElement("X", nCopyVertex, &FreeThis);
        rVertex = (UDMFVertex *)tPolygonData->AutoIterate();
    }

    //--Store the area of the polygon. Reset the running count.
    xStaticArea = AreaOfPoly(tPolygonData);
    xRunningArea = 0.0f;

    //--The 0th entry on the global list is always the original polygon. If it gets sliced up, it
    //  is handled dynamically.
    tPolygonSplitList->AddElement("X", tPolygonData, &SugarLinkedList::DeleteThis);

    //--Keep on slicing up the polygon until it can't be sliced up anymore.
    int tPasses = 0;
    bool tIsComplete = false;
    while(!tIsComplete)
    {
        //--Var
        bool tDidAnyCuts = false;

        //--Cut up the polygon and get a list of all the cuts.
        MakeMonotone(tPolygonData, tPolyCopy, tPolygonSplitList, tDidAnyCuts, NULL);
        ForceRightHandedness(tPolygonData);
        EliminateBisectors(tPolygonData);

        //--No cuts? We're done.
        if(!tDidAnyCuts || tPolygonData->GetListSize() <= 4) tIsComplete = true;
        DebugManager::Print("Slice completed.\n");
        tPasses ++;

        //if(tPasses >= 1)
        {
           // fprintf(stderr, "STTOPPPPPPEDDDD!\n");
         //   tIsComplete = true;
        }
    }
    DebugManager::Print("Finished slicing poly in %i passes.\n", tPasses);

    //--Clean up from the montonous polygon stuff.
    delete tPolyCopy;

    //--Add every polygon to the global list that was passed in. Note that this always includes
    //  the original polygon, and that deleting the polygon split list also deletes the polygon
    //  data we created earlier.
    float tRunningPolySize = 0.0f;
    SugarLinkedList *rPolyDataList = (SugarLinkedList *)tPolygonSplitList->PushIterator();
    while(rPolyDataList)
    {
        //--Special case: This is the original polygon (usually at the head of the list).
        if(rPolyDataList == tPolygonData)
        {
            ReuploadPolygonData(pPolygon, rPolyDataList);
            pPolygonList->AddElement("X", pPolygon, &FreeThis);
            tRunningPolySize = tRunningPolySize + AreaOfPoly(rPolyDataList);
            //fprintf(stderr, "<-- 0th poly was %f\n", AreaOfPoly(rPolyDataList));
        }
        //--Normal case: We need a new polygon.
        else
        {
            SetMemoryData(__FILE__, __LINE__);
            WAD_FloorPoly *nPolygon = (WAD_FloorPoly *)starmemoryalloc(sizeof(WAD_FloorPoly));
            nPolygon->Initialize();
            nPolygon->mAssociatedSector = pPolygon->mAssociatedSector;
            nPolygon->mFloorHeight = pPolygon->mFloorHeight;
            nPolygon->mCeilingHeight = pPolygon->mCeilingHeight;
            nPolygon->rFloorTexture = pPolygon->rFloorTexture;
            nPolygon->rCeilingTexture = pPolygon->rCeilingTexture;
            ReuploadPolygonData(nPolygon, rPolyDataList);
            pPolygonList->AddElement("X", nPolygon, &FreeThis);
            tRunningPolySize = tRunningPolySize + AreaOfPoly(rPolyDataList);
        }

        //--Iterate
        rPolyDataList = (SugarLinkedList *)tPolygonSplitList->AutoIterate();
    }

    //--Debug.
    //fprintf(stderr, "<-- Comparison %f %f %f\n", xStaticArea, xRunningArea, tRunningPolySize);
    DebugManager::Print("After splitting, there are %i polygons.\n", pPolygonList->GetListSize());
    DebugManager::Print("1 poly became %i polygons.\n", tPolygonSplitList->GetListSize());

    //--Clean up. The RLL deletes tPolygonData for us.
    delete tPolygonSplitList;
    DebugManager::PopPrint("Completed\n");
}
SugarLinkedList *WADFile::GetPolygonData(WAD_FloorPoly *pPolygon)
{
    //--Worker function. Takes in a given polygon and returns a SugarLinkedList with all its vertex
    //  data as pointer-clones. You must delete the list when done with it.
    //--Returns NULL on failure.
    if(!pPolygon) return NULL;

    SugarLinkedList *nPolygonData = new SugarLinkedList(false);
    for(int i = 0; i < pPolygon->mVertexesTotal; i ++)
    {
        nPolygonData->AddElement("X", &pPolygon->mVertices[i]);
        //fprintf(stderr, " Report %i: %i %i\n", i, pPolygon->mVertices[i].mX, pPolygon->mVertices[i].mY);
    }
    return nPolygonData;
}
void WADFile::EliminateBisectors(SugarLinkedList *pPolygonData)
{
    //--In a polygon, there may be unnecessary vertices which bisect a line between others. Floor
    //  polygons do not care about these vertices (they are used for walls) so we can eliminate them.
    if(!pPolygonData) return;

    //--Constants.
    const float cEpsilon = 0.001f;

    //--Keep eliminating until we go through the entire polygon without eliminating any vertices.
    bool tEliminatedAnything = true;
    while(tEliminatedAnything)
    {
        //--Reset.
        tEliminatedAnything = false;

        //--Eliminate vertices which merely bisect another line. This will simplify rendering later.
        int tSlot = 0;
        bool tIsDone = false;
        UDMFVertex *rPrevVertex = NULL;
        UDMFVertex *rNextVertex = NULL;
        UDMFVertex *rVertex = (UDMFVertex *)pPolygonData->SetToHeadAndReturn();
        while(!tIsDone)
        {
            //--If this is the 0th vertex, get the last vertex.
            if(!rPrevVertex)
            {
                rPrevVertex = (UDMFVertex *)pPolygonData->GetTail();
            }

            //--If this is the last vertex, the next vertex is the 0th one.
            void *rCheckTail = pPolygonData->GetTail();
            if(rCheckTail == rVertex)
            {
                rNextVertex = (UDMFVertex *)pPolygonData->GetHead();
                tIsDone = true;
            }
            //--Otherwise, it's just the next vertex.
            else
            {
                rNextVertex = (UDMFVertex *)pPolygonData->GetElementBySlot(tSlot+1);
            }

            //--Debug.
            //fprintf(stderr, " %2i: %5i %5i to %5i %5i\n", tSlot, rVertex->mX, rVertex->mY, rNextVertex->mX, rNextVertex->mY);

            //--Check the angle between the two. If it's a straight line, eliminate this vertex.
            float tAngleToCur = atan2f(rVertex->mY     - rPrevVertex->mY, rVertex->mX     - rPrevVertex->mX);
            float tAngleToNex = atan2f(rNextVertex->mY - rPrevVertex->mY, rNextVertex->mX - rPrevVertex->mX);
            //fprintf(stderr, "  %4.2f - %4.2f\n", tAngleToCur, tAngleToNex);

            //--Use a 3-digit range.
            if(fabs(tAngleToCur - tAngleToNex) < cEpsilon)
            {
                //fprintf(stderr, "  Eliminated!\n");
                tEliminatedAnything = true;
                pPolygonData->RemoveRandomPointerEntry();
                rVertex = (UDMFVertex *)pPolygonData->IncrementAndGetRandomPointerEntry();
                //if(rVertex) fprintf(stderr, "  Next vertex: %5i %5i\n", rVertex->mX, rVertex->mY);
            }
            //--Normal case, just iterate up.
            else
            {
                rPrevVertex = rVertex;
                rVertex = (UDMFVertex *)pPolygonData->IncrementAndGetRandomPointerEntry();
                //if(rVertex) fprintf(stderr, "  Next vertex: %5i %5i\n", rVertex->mX, rVertex->mY);
                tSlot ++;
            }
        }
    }
}
void WADFile::ForceRightHandednessFP(WAD_FloorPoly *pPolygon)
{
    //--Overload, uses a WAD_FloorPoly structure to get the handedness using the below function.
    if(!pPolygon) return;

    //--Create a list.
    SugarLinkedList *tVertexList = new SugarLinkedList(false);
    for(int i = 0; i < pPolygon->mVertexesTotal; i ++)
    {
        SetMemoryData(__FILE__, __LINE__);
        UDMFVertex *nVertex = (UDMFVertex *)starmemoryalloc(sizeof(UDMFVertex));
        memcpy(nVertex, &pPolygon->mVertices[i], sizeof(UDMFVertex));
        tVertexList->AddElementAsTail("X", nVertex, &FreeThis);
    }

    //--Force the handedness.
    ForceRightHandedness(tVertexList);

    //--Reinsert.
    for(int i = 0; i < pPolygon->mVertexesTotal; i ++)
    {
        UDMFVertex *rVertex = (UDMFVertex *)tVertexList->GetElementBySlot(i);
        memcpy(&pPolygon->mVertices[i], rVertex, sizeof(UDMFVertex));
    }

    //--Clean.
    tVertexList->SetDeallocation(true);
    delete tVertexList;
}
void WADFile::ForceRightHandedness(SugarLinkedList *pPolygonData)
{
    //--A polygon may have two possible winding orders. Right handed means the interior of the polygon
    //  is always to the 'right' of any given line (0->1). If a polygon violates this rule, all we
    //  need to do is flip all the entries.
    if(!pPolygonData || pPolygonData->GetListSize() <= 2) return;

    //--Debug
    bool tIsLeftHanded = false;
    DebugManager::PushPrint(false, "[Force Right Handedness]\n");

    #define THIRD_METHOD
    #if defined FIRST_METHOD

        //--Setup.
        float tRunningRight = 0.0f;
        float tRunningLeft = 0.0f;

        //--To get whether or not this is right handed, we need to sum the right-and-left angles of
        //  every turn. The interior is always lower than the exterior angles.
        for(int i = 0; i < pPolygonData->GetListSize(); i ++)
        {
            //--Setup.
            UDMFVertex *rPrev = NULL;
            UDMFVertex *rCurr = NULL;
            UDMFVertex *rNext = NULL;

            //--Get the prev point. On 0, this is the last point.
            if(i == 0)
            {
                rPrev = (UDMFVertex *)pPolygonData->GetTail();
            }
            //--It's the previous index.
            else
            {
                rPrev = (UDMFVertex *)pPolygonData->GetElementBySlot(i-1);
            }

            //--Get the current point.
            rCurr = (UDMFVertex *)pPolygonData->GetElementBySlot(i);

            //--Get the next point. At the end, this is the 0th point.
            if(i == pPolygonData->GetListSize()-1)
            {
                rNext = (UDMFVertex *)pPolygonData->GetHead();
            }
            //--It's the next index.
            else
            {
                rNext = (UDMFVertex *)pPolygonData->GetElementBySlot(i+1);
            }

            //--All points resolved, get and print the angle calc.
            float tToCurr = atan2f(rCurr->mY - rPrev->mY, rCurr->mX - rPrev->mX);
            float tToNext = atan2f(rNext->mY - rCurr->mY, rNext->mX - rCurr->mX);

            //--Calc how many rads on a right turn.
            float tRightTurn = tToNext - tToCurr;
            while(tRightTurn > 6.2831852f) tRightTurn = tRightTurn - 6.2831852f;
            while(tRightTurn < 0.0000000f) tRightTurn = tRightTurn + 6.2831852f;

            //--Calc how many rads on a left turn.
            float tLeftTurn = tToCurr - tToNext;
            while(tLeftTurn > 6.2831852f) tLeftTurn = tLeftTurn - 6.2831852f;
            while(tLeftTurn < 0.0000000f) tLeftTurn = tLeftTurn + 6.2831852f;
            DebugManager::Print(" Turns %f %f\n", tRightTurn, tLeftTurn);

            //--Add and continue.
            tRunningRight = tRunningRight + tRightTurn;
            tRunningLeft  = tRunningLeft  + tLeftTurn;
        }

        if(tRunningRight > tRunningLeft) tIsLeftHanded = true;

        //--Debug.
        DebugManager::Print("Polygon data: %f %f\n", tRunningRight, tRunningLeft);

    #elif defined SECOND_METHOD

        //--Second method.
        //--Using trapezoid logic, calculate the area of each triangle produced by the points in sequence.
        //  If this area is negative, then the polygon was left-handed.
        float tTotalArea = 0.0f;

        UDMFVertex *rPrevVertex = (UDMFVertex *)pPolygonData->GetTail();
        UDMFVertex *rCurrVertex = (UDMFVertex *)pPolygonData->PushIterator();
        while(rCurrVertex)
        {
            //--Calc and add.
            tTotalArea = tTotalArea + ( (rCurrVertex->mX - rPrevVertex->mX) * (rCurrVertex->mY - rPrevVertex->mY) / 2.0f );

            //--Iterate up.
            rPrevVertex = rCurrVertex;
            rCurrVertex = (UDMFVertex *)pPolygonData->AutoIterate();
        }

        DebugManager::Print("Total polygon area is %f\n", tTotalArea);

        if(tTotalArea < 0.0f) tIsLeftHanded = true;

    #elif defined THIRD_METHOD

        //--This method uses cross-products of two lines.
        float tCrossProduct = 0.0f;
        int tCount = 0;

        int tTotalVertices = pPolygonData->GetListSize();
        for(int i = 0; i < tTotalVertices; i ++)
        {
            UDMFVertex *rVertexA = (UDMFVertex *)pPolygonData->GetElementBySlot( i+0                   );
            UDMFVertex *rVertexB = (UDMFVertex *)pPolygonData->GetElementBySlot((i+1) % tTotalVertices );
            UDMFVertex *rVertexC = (UDMFVertex *)pPolygonData->GetElementBySlot((i+2) % tTotalVertices );

            tCrossProduct  = (rVertexB->mX - rVertexA->mX) * (rVertexC->mY - rVertexB->mY);
            tCrossProduct -= (rVertexB->mY - rVertexA->mY) * (rVertexC->mX - rVertexB->mX);
            if (tCrossProduct < 0)
                tCount--;
            else if (tCrossProduct > 0)
                tCount++;
        }

        if (tCount < 0)
            tIsLeftHanded = true;
        else if (tCount > 0)
            tIsLeftHanded = false;
        else
            tIsLeftHanded = false;

    #endif

    //--Flip all the positions.
    if(tIsLeftHanded)
    {
        DebugManager::Print("This polygon is left handed!\n");
        SugarLinkedList *tFlippedList = new SugarLinkedList(false);
        UDMFVertex *rVertex = (UDMFVertex *)pPolygonData->PushIterator();
        while(rVertex)
        {
            tFlippedList->AddElementAsTail("X", rVertex);
            rVertex = (UDMFVertex *)pPolygonData->AutoIterate();
        }

        //--Clear the list and reinsert everything.
        pPolygonData->ClearList();
        rVertex = (UDMFVertex *)tFlippedList->PushIterator();
        while(rVertex)
        {
            pPolygonData->AddElementAsHead("X", rVertex);
            rVertex = (UDMFVertex *)tFlippedList->AutoIterate();
        }

        //--Clean up.
        delete tFlippedList;
    }
    else
    {
        DebugManager::Print("Polygon was already right handed.\n");
    }
    DebugManager::PopPrint("[Force Right Handedness] Complete\n");
}
#include "HitDetection.h"
float IsLegalCut(UDMFVertex pVertexA, UDMFVertex pVertexB, UDMFVertex pVertexC, SugarLinkedList *pOriginalPolygon, float &sTurnAngle)
{
    //--Given a set of three vertices, a polygon to compare against, and an area, returns whether or not the polygon
    //  can be subdivided by decomposing it into a triangle and a polygon without consuming more area.
    //--Returns the area under the expected cut, or 0.0f if the cut was illegal. The sTurnAngle will be set to whatever the turn
    //  angle was, even if the cut was illegal.
    if(!pOriginalPolygon) return 0.0f;

    //--Check the angle between the two lines present. We cannot allow a line that doesn't make a right-handed
    //  turn to be cut.
    float tToCurr = atan2f(pVertexB.mY - pVertexA.mY, pVertexB.mX - pVertexA.mX);
    float tToNext = atan2f(pVertexC.mY - pVertexB.mY, pVertexC.mX - pVertexB.mX);

    //--Get the turn angle.
    sTurnAngle = tToNext - tToCurr;
    while(sTurnAngle > 6.2831852f) sTurnAngle = sTurnAngle - 6.2831852f;
    while(sTurnAngle < 0.0000000f) sTurnAngle = sTurnAngle + 6.2831852f;
    if(sTurnAngle >= 3.1415926f || sTurnAngle == 0.0f) return 0.0f;

    //--If the cut crosses any line on the outside of the polygon, then it's illegal.
    if(WADFile::DoesLineCrossAnyOther(pVertexA.mX, pVertexA.mY, pVertexC.mX, pVertexC.mY, pOriginalPolygon)) return 0.0f;
    if(WADFile::DoesLineCrossAnyOther(pVertexB.mX, pVertexB.mY, pVertexA.mX, pVertexA.mY, pOriginalPolygon)) return 0.0f;
    if(WADFile::DoesLineCrossAnyOther(pVertexB.mX, pVertexB.mY, pVertexC.mX, pVertexC.mY, pOriginalPolygon)) return 0.0f;

    //--The cut is legal! Return the area under the new triangle.
    return AreaOfTriangle(pVertexA.mX, pVertexA.mY, pVertexB.mX, pVertexB.mY, pVertexC.mX, pVertexC.mY);
}
bool CutsAnyAdditionalPolys(UDMFVertex pVertexA, UDMFVertex pVertexB, UDMFVertex pVertexC, SugarLinkedList *pAdditionalPolyList)
{
    //--Returns true if the provided line segments cut any lines on the pAdditionalPolyList. If the list was NULL
    //  then always returns false.
    if(!pAdditionalPolyList) return false;

    //--Iterate across the polygons. They are in WAD_FloorPoly format.
    WAD_FloorPoly *rPolygon = (WAD_FloorPoly *)pAdditionalPolyList->PushIterator();
    while(rPolygon)
    {
        //--Check all lines.
        float tInterceptX, tInterceptY;
        for(int i = 0; i < rPolygon->mVertexesTotal; i ++)
        {
            //--Compute next slot, wrapping if needed.
            int u = (i + 1) % rPolygon->mVertexesTotal;
            if(get_line_intersection(pVertexA.mX, pVertexA.mY, pVertexB.mX, pVertexB.mY, rPolygon->mVertices[i].mX, rPolygon->mVertices[i].mY, rPolygon->mVertices[u].mX, rPolygon->mVertices[u].mY, &tInterceptX, &tInterceptY) == 1 ||
               get_line_intersection(pVertexB.mX, pVertexB.mY, pVertexC.mX, pVertexC.mY, rPolygon->mVertices[i].mX, rPolygon->mVertices[i].mY, rPolygon->mVertices[u].mX, rPolygon->mVertices[u].mY, &tInterceptX, &tInterceptY) == 1 ||
               get_line_intersection(pVertexA.mX, pVertexA.mY, pVertexC.mX, pVertexC.mY, rPolygon->mVertices[i].mX, rPolygon->mVertices[i].mY, rPolygon->mVertices[u].mX, rPolygon->mVertices[u].mY, &tInterceptX, &tInterceptY) == 1)
            {
                pAdditionalPolyList->PopIterator();
                return true;
            }
        }

        //--Next.
        rPolygon = (WAD_FloorPoly *)pAdditionalPolyList->AutoIterate();
    }

    //--All checks passed.
    return false;
}
int FindNarrowestCut(SugarLinkedList *pPolygonData, SugarLinkedList *pOriginalPolygon, SugarLinkedList *pAdditionalPolys)
{
    //--Returns the index of the narrowest cut that can be made to this polygon to make it more monotone. The SLL
    //  should be a list containing UDMFVertex structures arranged in a right-handed polygon.
    //--pAdditionalPolys is optional, it contains polygons which the line cannot cut across. If NULL, it is ignored.
    //--Returns -1 if no cut can be made.
    if(!pPolygonData || pPolygonData->GetListSize() <= 3 || !pOriginalPolygon) return -1;

    //--Setup.
    int tSlot = 0;
    UDMFVertex *rPrevVertex = NULL;
    UDMFVertex *rNextVertex = NULL;
    UDMFVertex *rCurrVertex = NULL;

    //--Narrow storage.
    int tNarrowestSlot = -1;
    float tNarrowestAngle = 3.1415926f * 0.0f;

    //--Other.
    int tPolygonVerticesTotal = pPolygonData->GetListSize();

    //--Start iteration.
    while(tSlot < tPolygonVerticesTotal)
    {
        //--Get the vertex listing.
        int tPrevSlot = tSlot - 1;
        if(tPrevSlot < 0) tPrevSlot += tPolygonVerticesTotal;
        rPrevVertex = (UDMFVertex *)pPolygonData->GetElementBySlot(tPrevSlot);
        rCurrVertex = (UDMFVertex *)pPolygonData->GetElementBySlot((tSlot + 0) % tPolygonVerticesTotal);
        rNextVertex = (UDMFVertex *)pPolygonData->GetElementBySlot((tSlot + 1) % tPolygonVerticesTotal);

        //--Run this routine, which will check if the cut is a right-handed turn that doesn't cross any external bounds.
        float tCheckTurnAngle = 0.0f;
        float tAreaUnderTriangle = IsLegalCut(*rPrevVertex, *rCurrVertex, *rNextVertex, pOriginalPolygon, tCheckTurnAngle);

        //--If the area came back as 0.0f, then the cut wasn't legal. Also, if the turn angle was narrower than our
        //  current turn angle, ignore it.
        if(tAreaUnderTriangle > 0.0f && tCheckTurnAngle > tNarrowestAngle)
        {
            //--If we have pAdditionalPolys, this function will check if the given cut will cross any lines in the additional poly list.
            if(CutsAnyAdditionalPolys(*rPrevVertex, *rCurrVertex, *rNextVertex, pAdditionalPolys))
            {
                //DebugManager::ForcePrint("Cut failed, cut hits one of the additional polys.\n");
            }
            else
            {
                //--In order to be legal, the area under the three points must be equal to or less than the area under the polygon as a whole.
                float tPolyMinusArea = AreaOfPolyWithout(pPolygonData, tSlot);
                float tThreePointArea = AreaOfTriangle(rPrevVertex->mX, rPrevVertex->mY, rCurrVertex->mX, rCurrVertex->mY, rNextVertex->mX, rNextVertex->mY);
                if(WADFile::xRunningArea + tPolyMinusArea + tThreePointArea == WADFile::xStaticArea)
                {
                    tNarrowestSlot = tSlot;
                    tNarrowestAngle = tCheckTurnAngle;
                    //fprintf(stderr, "Numbers: %.0f: %.0f + %.0f + %.0f = %.0f\n", WADFile::xStaticArea, WADFile::xRunningArea, tPolyMinusArea, tThreePointArea, WADFile::xRunningArea + tPolyMinusArea + tThreePointArea);
                }
                else
                {
                    //DebugManager::ForcePrint("Cut failed, area %f + %f is more than %f\n", tThreePointArea, tPolyMinusArea, tWholePolyArea);
                }
            }
        }

        //--Next slot.
        tSlot ++;
    }

    //--Return the narrowest slot. If none were found, this will be -1.
    return tNarrowestSlot;
}

#include "HitDetection.h"
bool WADFile::CutInternalPolygons(WAD_FloorPoly *pPolygonData, SugarLinkedList *pPolyGlobalList)
{
    //--If the polygon has internal polygons, we need to cut those out before we can run MakeMonotone() to decompose the polygon.
    //  Fortunately, this is relatively easy. A single triangle can be cut out to each internal polygon which will make them
    //  touch the edge, at which point the polygon can be legally decomposed.
    //--For this algorithm, the triangle which cuts the least area is selected. The cut cannot breach any of the internal lines
    //  nor can it breach any of the external lines.
    //--The resulting triangle is not considered part of the polygon anymore, and the internal polygon is removed from the polygon's
    //  internal data since it now touches an edge. The internal polygon's vertices are instead added to the polygon's vertices in
    //  reverse order, creating a polygon which has one fewer hole.
    //--Returns true if it cut out a polygon, false if it could not.
    if(!pPolygonData || !pPolyGlobalList) return false;

    //--If there are no internal polygons, we're done.
    if(!pPolygonData->mInternalPolygons || pPolygonData->mInternalPolygons->GetListSize() < 1) return false;

    //--[Method 2]
    //--Since we know, for sure, that the polygon surrounds the internal polygon, we can be guaranteed that
    //  there is a north-south split. Find the nearest vertex that is above the internal polygon, and the nearest vertex
    //  that is below it, and bisect the polygon along that vertex.
    //--Once that's done, we just need to reconstitute the polygons. Easy.
    WAD_FloorPoly *rInternalPolygon = (WAD_FloorPoly *)pPolygonData->mInternalPolygons->GetElementBySlot(0);

    //--Get the northwestmost internal vertex, and the southeastmost internal vertex. Ties are okay, but
    //  under no circumstances can they be the same vertex.
    int tNWVertexSlot = 0;
    int tSEVertexSlot = 0;
    for(int i = 1; i < rInternalPolygon->mVertexesTotal; i ++)
    {
        //--Point is north, takes priority.
        if(rInternalPolygon->mVertices[i].mY < rInternalPolygon->mVertices[tNWVertexSlot].mY)
        {
            tNWVertexSlot = i;
        }
        //--Point is equal, but to the west, takes priority.
        else if(rInternalPolygon->mVertices[i].mY == rInternalPolygon->mVertices[tNWVertexSlot].mY &&
                rInternalPolygon->mVertices[i].mX <  rInternalPolygon->mVertices[tNWVertexSlot].mX)
        {
            tNWVertexSlot = i;
        }
    }

    //--Prevent them from being the same slot.
    if(tNWVertexSlot == 0) tSEVertexSlot = 1;

    //--Scan for SE case. Can't be the same as the NW slot.
    for(int i = 0; i < rInternalPolygon->mVertexesTotal; i ++)
    {
        //--Point is south, takes priority.
        if(rInternalPolygon->mVertices[i].mY > rInternalPolygon->mVertices[tSEVertexSlot].mY)
        {
            tSEVertexSlot = i;
        }
        //--Point is equal, but to the east, takes priority.
        else if(rInternalPolygon->mVertices[i].mY == rInternalPolygon->mVertices[tSEVertexSlot].mY &&
                rInternalPolygon->mVertices[i].mX >  rInternalPolygon->mVertices[tSEVertexSlot].mX)
        {
            tSEVertexSlot = i;
        }
    }

    //--Next, get the vertex that is most northerly to the northern point. This is done by getting the X variation.
    int tBestNSlot = -1;
    int tBestX = abs(rInternalPolygon->mVertices[tNWVertexSlot].mX - pPolygonData->mVertices[0].mX);
    for(int i = 1; i < pPolygonData->mVertexesTotal; i ++)
    {
        //--Must be north of.
        if(pPolygonData->mVertices[i].mY < rInternalPolygon->mVertices[tNWVertexSlot].mY)
        {
            //--Whichever has the lowest X variation wins.
            int tXVariation = abs(rInternalPolygon->mVertices[tNWVertexSlot].mX - pPolygonData->mVertices[i].mX);
            if(tXVariation < tBestX || tBestNSlot == -1)
            {
                tBestX = tXVariation;
                tBestNSlot = i;
                fprintf(stderr, "New best N slot is %i\n", tBestNSlot);
            }
        }
    }

    //--Now get the vertex that is south of the southern point. This will mathematically never be the
    //  same as the best northern slot.
    int tBestSSlot = -1;
    tBestX = abs(rInternalPolygon->mVertices[tSEVertexSlot].mX - pPolygonData->mVertices[0].mX);
    for(int i = 1; i < pPolygonData->mVertexesTotal; i ++)
    {
        //--Must be south of.
        if(pPolygonData->mVertices[i].mY > rInternalPolygon->mVertices[tSEVertexSlot].mY)
        {
            //--Whichever has the lowest X variation wins.
            int tXVariation = abs(rInternalPolygon->mVertices[tSEVertexSlot].mX - pPolygonData->mVertices[i].mX);
            if((tXVariation < tBestX && i != tBestNSlot) || tBestSSlot == -1)
            {
                tBestX = tXVariation;
                tBestSSlot = i;
                fprintf(stderr, "New best S slot is %i\n", tBestSSlot);
            }
        }
    }

    //--We now have some polygons we can hack up. Let's compose a new polygon.
    SugarLinkedList *tNewPolyList = new SugarLinkedList(true);
    fprintf(stderr, "Slice vertex NorthMost: %.0f %.0f\n", pPolygonData->mVertices[tBestNSlot].mX,        pPolygonData->mVertices[tBestNSlot].mY);
    fprintf(stderr, "Slice vertex NorthIntr: %.0f %.0f\n", rInternalPolygon->mVertices[tNWVertexSlot].mX, rInternalPolygon->mVertices[tNWVertexSlot].mY);
    fprintf(stderr, "Slice vertex SouthIntr: %.0f %.0f\n", rInternalPolygon->mVertices[tSEVertexSlot].mX, rInternalPolygon->mVertices[tSEVertexSlot].mY);
    fprintf(stderr, "Slice vertex SouthMost: %.0f %.0f\n", pPolygonData->mVertices[tBestSSlot].mX,        pPolygonData->mVertices[tBestSSlot].mY);

    //--Starting at the tBestSSlot, count up to the tBestNSlot. We might have to go around the polygon.
    for(int i = tBestSSlot; i < tBestSSlot + pPolygonData->mVertexesTotal; i ++)
    {
        //--Wrap.
        int tTrueSlot = i % pPolygonData->mVertexesTotal;

        //--Add this vertex.
        SetMemoryData(__FILE__, __LINE__);
        UDMFVertex *nVertex = (UDMFVertex *)starmemoryalloc(sizeof(UDMFVertex));
        memcpy(nVertex, &pPolygonData->mVertices[tTrueSlot], sizeof(UDMFVertex));
        tNewPolyList->AddElementAsTail("X", nVertex);
        //fprintf(stderr, "Set vertex %i %i\n", nVertex->mX, nVertex->mY);

        //--If this vertex happens to be the northern slot, it's time to duck inwards.
        if(tTrueSlot == tBestNSlot) break;
    }

    //--Put in the internal slots. This will produce a properly-wound polygon.
    for(int i = tNWVertexSlot; i < tNWVertexSlot + rInternalPolygon->mVertexesTotal; i ++)
    {
        //--Wrap.
        int tTrueSlot = i % rInternalPolygon->mVertexesTotal;
        if(tTrueSlot < 0) tTrueSlot += rInternalPolygon->mVertexesTotal;

        //--Add this vertex.
        SetMemoryData(__FILE__, __LINE__);
        UDMFVertex *nVertex = (UDMFVertex *)starmemoryalloc(sizeof(UDMFVertex));
        memcpy(nVertex, &rInternalPolygon->mVertices[tTrueSlot], sizeof(UDMFVertex));
        tNewPolyList->AddElementAsTail("X", nVertex);
        //fprintf(stderr, "Set vertex %i %i\n", nVertex->mX, nVertex->mY);

        //--If this is the southern slot, we're done.
        if(tTrueSlot == tSEVertexSlot) break;
    }

    //--Now reconstitute the polygon.
    free(pPolygonData->mVertices);
    pPolygonData->AllocVertices(tNewPolyList->GetListSize());
    for(int i = 0; i < pPolygonData->mVertexesTotal; i ++)
    {
        UDMFVertex *rRetrieveVertex = (UDMFVertex *)tNewPolyList->GetElementBySlot(i);
        if(!rRetrieveVertex) continue;
        memcpy(&pPolygonData->mVertices[i], rRetrieveVertex, sizeof(UDMFVertex));
    }

    //--Clean.
    delete tNewPolyList;

    //--Remove the internal polygon from consideration.
    pPolygonData->mInternalPolygons->RemoveElementI(0);
    return false;
}
void WADFile::MakeMonotone(SugarLinkedList *pPolygonData, SugarLinkedList *pOrigPolyData, SugarLinkedList *pPolyGlobalList, bool &sPerformedAnyAction, SugarLinkedList *pExternalPolys)
{
    //--Given a set of polygon point data, splits the polygon into a series of monotone polygons,
    //  with the clipped ears usually being triangles. A RLL containing the original polygon and
    //  all the created sub-polygons is returned.
    //--The pExternalPolys list can legally be NULL, which means the polygon is assumed to have no previously internal polygons.
    //--The polygon MUST be right-handed or this routine will fail horribly.
    //--The polygon may not be altered at all if it was already monotone.
    if(!pPolygonData || !pPolyGlobalList || !pOrigPolyData) return;
    DebugManager::PushPrint(false, "[Make Monotone] Beginning.\n");

    //--Get variables.
    int tPolygonVerticesTotal = pPolygonData->GetListSize();

    //--Find the narrowest cut we can make. This is whichever cut has the lowest internal turn angle that
    //  is also below 180 degrees (outside cuts cannot be made).
    int tCutSlot = FindNarrowestCut(pPolygonData, pOrigPolyData, pExternalPolys);
    if(tCutSlot == -1)
    {
        DebugManager::PopPrint("[Make Monotone] No further cuts can be made.\n");
        return;
    }

    //--We got a cut! Perform that. These will form a new polygon.
    int tPrevSlot = tCutSlot - 1;
    if(tPrevSlot < 0) tPrevSlot += tPolygonVerticesTotal;
    UDMFVertex *rPrevVertex = (UDMFVertex *)pPolygonData->GetElementBySlot(tPrevSlot);
    UDMFVertex *rCurrVertex = (UDMFVertex *)pPolygonData->GetElementBySlot((tCutSlot + 0) % tPolygonVerticesTotal);
    UDMFVertex *rNextVertex = (UDMFVertex *)pPolygonData->GetElementBySlot((tCutSlot + 1) % tPolygonVerticesTotal);

    //--Create a new polygon composed of these three nodes. Add it to the global list.
    SugarLinkedList *nNewPolyData = new SugarLinkedList(true);
    SetMemoryData(__FILE__, __LINE__);
    UDMFVertex *nNewVertexA = (UDMFVertex *)starmemoryalloc(sizeof(UDMFVertex));
    SetMemoryData(__FILE__, __LINE__);
    UDMFVertex *nNewVertexB = (UDMFVertex *)starmemoryalloc(sizeof(UDMFVertex));
    SetMemoryData(__FILE__, __LINE__);
    UDMFVertex *nNewVertexC = (UDMFVertex *)starmemoryalloc(sizeof(UDMFVertex));
    memcpy(nNewVertexA, rPrevVertex, sizeof(UDMFVertex));
    memcpy(nNewVertexB, rCurrVertex, sizeof(UDMFVertex));
    memcpy(nNewVertexC, rNextVertex, sizeof(UDMFVertex));
    nNewPolyData->AddElement("X", nNewVertexA, &FreeThis);
    nNewPolyData->AddElement("X", nNewVertexB, &FreeThis);
    nNewPolyData->AddElement("X", nNewVertexC, &FreeThis);
    pPolyGlobalList->AddElement("X", nNewPolyData, &SugarLinkedList::DeleteThis);

    //--Whenever something is added to the pPolyGlobalList, update the running area tally.
    xRunningArea = xRunningArea + AreaOfPoly(nNewPolyData);
    //fprintf(stderr, "Area of poly is now %f vs %f\n", xRunningArea, xStaticArea);

    //--Remove the middle vertex from the original polygon data.
    pPolygonData->RemoveElementI(tCutSlot);
    sPerformedAnyAction = true;
    DebugManager::Print("Cutting %i %i to %i %i to %i %i\n", nNewVertexA->mX, nNewVertexA->mY, nNewVertexB->mX, nNewVertexB->mY, nNewVertexC->mX, nNewVertexC->mY);
    DebugManager::PopPrint("[Make Monotone] Completed with a cut in slot %i. %i vertices are left.\n", tCutSlot, pPolygonData->GetListSize());
}
void WADFile::ReuploadPolygonData(WAD_FloorPoly *pPolygon, SugarLinkedList *pPolygonData)
{
    //--Given a list of polygon data, reuploads it to the polygon in question.
    if(!pPolygon || !pPolygonData) return;

    //--Store old data pointer, to be freed later. The polygon data does not copy, it pointer-clones,
    //  therefore this cannot be freed until the reupload is complete.
    UDMFVertex *rOldData = pPolygon->mVertices;

    //--Debug.
    //fprintf(stderr, "New polygon has %i points from %i\n", pPolygonData->GetListSize(), pPolygon->mVertexesTotal);

    //--Allocate and copy.
    pPolygon->AllocVertices(pPolygonData->GetListSize());
    for(int i = 0; i < pPolygon->mVertexesTotal; i ++)
    {
        UDMFVertex *rVertex = (UDMFVertex *)pPolygonData->GetElementBySlot(i);
        memcpy(&pPolygon->mVertices[i], rVertex, sizeof(UDMFVertex));
        //fprintf(stderr, " Report %i: %i %i\n", i, rVertex->mX, rVertex->mY);
    }

    //--Now we can free the old data.
    free(rOldData);
}

float Lowest(float pNumberA, float pNumberB)
{
    //--Returns whichever of the two is the lowest.
    if(pNumberA < pNumberB) return pNumberA;
    return pNumberB;
}
float Highest(float pNumberA, float pNumberB)
{
    //--Returns whichever of the two is the highest.
    if(pNumberA > pNumberB) return pNumberA;
    return pNumberB;
}
int get_line_intersection(float p0_x, float p0_y, float p1_x, float p1_y, float p2_x, float p2_y, float p3_x, float p3_y, float *i_x, float *i_y)
{
    float s02_x, s02_y, s10_x, s10_y, s32_x, s32_y, s_numer, t_numer, denom, t;
    s10_x = p1_x - p0_x;
    s10_y = p1_y - p0_y;
    s32_x = p3_x - p2_x;
    s32_y = p3_y - p2_y;

    denom = s10_x * s32_y - s32_x * s10_y;
    if (denom == 0)
        return 2; // Collinear
    bool denomPositive = denom > 0;

    s02_x = p0_x - p2_x;
    s02_y = p0_y - p2_y;
    s_numer = s10_x * s02_y - s10_y * s02_x;
    if ((s_numer < 0) == denomPositive)
        return 0; // No collision

    t_numer = s32_x * s02_y - s32_y * s02_x;
    if ((t_numer < 0) == denomPositive)
        return 0; // No collision

    if (((s_numer > denom) == denomPositive) || ((t_numer > denom) == denomPositive))
        return 0; // No collision
    // Collision detected
    t = t_numer / denom;
    if (i_x != NULL)
        *i_x = p0_x + (t * s10_x);
    if (i_y != NULL)
        *i_y = p0_y + (t * s10_y);

    return 1;
}
bool WADFile::DoesLineCrossAnyOther(float pXA, float pYA, float pXB, float pYB, SugarLinkedList *pPolygonData)
{
    //--Worker function, finds out if the line between two given vertices crosses any of the lines
    //  on the polygon. That is, does the line ever 'exit' the polygon.
    //--The polygon is always assumed to be right-handed.
    if(!pPolygonData) return false;

    //--For each line...
    bool tCompletionFlag = false;
    UDMFVertex *rVertexA = (UDMFVertex *)pPolygonData->PushIterator();
    while(!tCompletionFlag)
    {
        //--Get the next vertex.
        UDMFVertex *rVertexB = (UDMFVertex *)pPolygonData->AutoIterate();
        if(!rVertexB)
        {
            tCompletionFlag = true;
            rVertexB = (UDMFVertex *)pPolygonData->GetElementBySlot(0);
        }

        //--Quick Access
        float tXC = rVertexA->mX;
        float tYC = rVertexA->mY;
        float tXD = rVertexB->mX;
        float tYD = rVertexB->mY;

        //--If any of the vertices are identical, ignore them.
        if( (pXA == tXC && pYA == tYC) || (pXB == tXC && pYB == tYC) ||
            (pXA == tXD && pYA == tYD) || (pXB == tXD && pYB == tYD))
        {
            rVertexA = rVertexB;
            continue;
        }

        //--Check for a line intersection.
        float tInterceptX = 0.0f;
        float tInterceptY = 0.0f;
        int tCheck = get_line_intersection(pXA, pYA, pXB, pYB, tXC, tYC, tXD, tYD, &tInterceptX, &tInterceptY);
        if(tCheck == 1)
        {
            DebugManager::Print(" Line %5.0f, %5.0f to %5.0f, %5.0f intersects or is colinear.\n", pXA, pYA, pXB, pYB);
            pPolygonData->PopIterator();
            return true;
        }

        //--Iterate up if there were no hits.
        rVertexA = rVertexB;
    }

    DebugManager::Print(" Line %5.0f, %5.0f to %5.0f, %5.0f is okay.\n", pXA, pYA, pXB, pYB);
    return false;
}
