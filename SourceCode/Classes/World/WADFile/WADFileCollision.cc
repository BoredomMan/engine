//--Base
#include "WADFile.h"

//--Classes
#include "WorldPolygon.h"

//--CoreClasses
#include "SugarLinkedList.h"

//--Definitions
//--GUI
//--Libraries
//--Managers
#include "DebugManager.h"

//--Functions related to determining collision. At present, this just handles height lookups since
//  arbitrary polygons cannot use sector data.

//#define WFCOLLISION_DEBUG

//--[Construction]
void WADFile::BuildHeightLookups()
{
    //--Given a list of polygons, scans those for all polygons that are facing up in some capacity and assembles
    //  those onto a list. Polygons need their normals computed, so they get computed her if that hasn't happened yet.
    //--Note that the height polygon table is a reference list. If the original polygons get deleted, accessing it
    //  becomes undefined.
    if(!mArbitraryPolysConstructionList) return;

    //--Reset variables.
    free(rHeightPolys);
    rHeightPolys = NULL;
    mHeightPolysTotal = 0;

    //--Iterate.
    WorldPolygon *rPolygon = (WorldPolygon *)mArbitraryPolysConstructionList->PushIterator();
    while(rPolygon)
    {
        //--Compute the normal.
        rPolygon->ComputeNormal();

        //--If the polygon's normal has a positive Z component, it points up. Even if it's very close to vertical, it
        //  still counts, so add it.
        if(rPolygon->mNormal.mZ > 0.0f) mHeightPolysTotal ++;

        //--Next.
        rPolygon = (WorldPolygon *)mArbitraryPolysConstructionList->AutoIterate();
    }

    //--Allocate space for the references.
    SetMemoryData(__FILE__, __LINE__);
    rHeightPolys = (WorldPolygon **)starmemoryalloc(sizeof(WorldPolygon *) * mHeightPolysTotal);

    //--Fill references.
    int tSlot = 0;
    rPolygon = (WorldPolygon *)mArbitraryPolysConstructionList->PushIterator();
    while(rPolygon)
    {
        //--Apply.
        if(rPolygon->mNormal.mZ > 0.0f)
        {
            rHeightPolys[tSlot] = rPolygon;
            tSlot ++;
        }

        //--Next.
        rPolygon = (WorldPolygon *)mArbitraryPolysConstructionList->AutoIterate();
    }

    //--Debug.
    #ifdef WFCOLLISION_DEBUG
        DebugManager::ForcePrint("WADFile:BuildHeightLookups - From %i polygons, got %i height polys.\n", mArbitraryPolysConstructionList->GetListSize(), mHeightPolysTotal);
    #endif
}
