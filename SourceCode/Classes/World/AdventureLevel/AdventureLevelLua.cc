//--Base
#include "AdventureLevel.h"

//--Classes
#include "AdventureLight.h"
#include "KPopDanceGame.h"
#include "TilemapActor.h"
#include "RunningMinigame.h"

//--CoreClasses
//--Definitions
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "DebugManager.h"
#include "MapManager.h"
#include "SugarLumpManager.h"

//========================================= Lua Hooking ===========================================
void AdventureLevel::HookToLuaState(lua_State *pLuaState)
{
    /* AL_Create()
       Creates a new AdventureLevel and pushes it to the MapManager.*/
    lua_register(pLuaState, "AL_Create", &Hook_AL_Create);

    /* AL_ParseSLF(sSLFPath)
       Orders the active AdventureLevel to parse information from the provided SLF file. This modified the
       SugarLumpManager. */
    lua_register(pLuaState, "AL_ParseSLF", &Hook_AL_ParseSLF);

    /* AL_GetProperty("Switch State") (1 boolean) (Static)
       AL_GetProperty("Name") (1 string)
       AL_GetProperty("Player Start") (2 integers)
       AL_GetProperty("Is Character Following", sName) (1 boolean)
       AL_GetProperty("Party Leader Name") (1 string)
       AL_GetProperty("Follower Name", iIndex) (1 string)
       AL_GetProperty("Last Chest Position") (2 floats)
       AL_GetProperty("Exit Exists", sExitName) (1 boolean)
       Returns the requested property from the current AdventureLevel. */
    lua_register(pLuaState, "AL_GetProperty", &Hook_AL_GetProperty);

    /* AL_SetProperty("Root Path", sPath) (Static)
       AL_SetProperty("Item Path", sPath) (Static)
       AL_SetProperty("Item Image Path", sPath) (Static)
       AL_SetProperty("Catalyst Path", sPath) (Static)
       AL_SetProperty("Trigger Handled Update") (Static)
       AL_SetProperty("Switch Handled Update") (Static)
       AL_SetProperty("Last Save Point", sLevelName) (Static)
       AL_SetProperty("Music", sMusicName) (Static)
       AL_SetProperty("Wipe Destroyed Enemies") (Static)
       AL_SetProperty("Ambient Light Boost", fValue) (Static)
       AL_SetProperty("Mandated Music Intensity", fValue) (Static)
       AL_SetProperty("Mandated Music Intensity Now", fValue) (Static)
       AL_SetProperty("Name", sName)
       AL_SetProperty("Background", sDLPath, fStartX, fStartY, fScrollX, fScrollY)
       AL_SetProperty("Player Actor ID", iUniqueID)
       AL_SetProperty("Follow Actor ID", iUniqueID)
       AL_SetProperty("Unfollow Actor Name", sName)
       AL_SetProperty("Open Door", sName)
       AL_SetProperty("Close Door", sName)
       AL_SetProperty("Set Layer Disabled", sLayerName, bFlag)
       AL_SetProperty("Set Animation Disabled", sLayerName, bFlag)
       AL_SetProperty("Base Path", sPath)
       AL_SetProperty("Examine Script", sPath)
       AL_SetProperty("Trigger Script", sPath)
       AL_SetProperty("Add Examinable", sName, iX, iY)
       AL_SetProperty("Add Examinable", sName, iX, iY, iW, iH)
       AL_SetProperty("Add Enemy Spawn", sName, iX, iY, sParty, sAppearance, sScene, iToughness, sPatrolPath, sFollow)
       AL_SetProperty("Add Chest", sName, iX, iY, bIsFuture, sContents)
       AL_SetProperty("Camera Lock", bFlag)
       AL_SetProperty("Run Enemy Spawner")
       AL_SetProperty("Switch State", sSwitchName, bIsUp)
       AL_SetProperty("Activate Fade", iTickDuration, iDepthFlag, bHoldsOnCompletion, fSRed, fSGreen, fSBlue, fSAlpha, fERed, fEGreen, fEBlue, fEAlpha)
       AL_SetProperty("Activate Fade", iTickDuration, iDepthFlag, bHoldsOnCompletion, fSRed, fSGreen, fSBlue, fSAlpha, fERed, fEGreen, fEBlue, fEAlpha, bAlternateFade)
       AL_SetProperty("Reposition Party", fXPos, fYPos)
       AL_SetProperty("Reposition Party", fXPos, fYPos, iFacing)
       AL_SetProperty("Fold Party")
       AL_SetProperty("Activate Underwater")
       AL_SetProperty("Deactivate Underwater")
       AL_SetProperty("Set Parallel Cutscenes During Dialogue Flag", bFlag)
       AL_SetProperty("Set Parallel Cutscenes During Cutscenes Flag", bFlag)
       AL_SetProperty("Activate Lights")
       AL_SetProperty("Deactivate Lights")
       AL_SetProperty("Set Ambient Light", fRed, fGreen, fBlue, fAlpha)
       AL_SetProperty("Register Radial Light", sName, fXPos, fYPos, fIntensity)
       AL_SetProperty("Enable Light", sName)
       AL_SetProperty("Disable Light", sName)
       AL_SetProperty("Modify Light Color", sName, pR, pG, pB, pA)
       AL_SetProperty("Activate Player Light", iCurrentPower, iMaxPower)
       AL_SetProperty("Deactivate Player Light")
       AL_SetProperty("Set Player Light No Drain", bFlag)
       AL_SetProperty("Set Collision", iX, iY, iZ, iCollisionValue)
       AL_SetProperty("Allocate Foregrounds", iTotal)
       AL_SetProperty("Foreground Image", iSlot, sPath)
       AL_SetProperty("Foreground Render Offsets", iSlot, fOffsetX, fOffsetY, fScalerX, fScalerY)
       AL_SetProperty("Foreground Alpha", iSlot, fAlphaTarget, iTicks)
       AL_SetProperty("Foreground Autoscroll", iSlot, fAutoscrollX, fAutoscrollY)
       AL_SetProperty("Foreground Scale", iSlot, fScaleFactor)
       AL_SetProperty("Register Objective", sObjectiveName)
       AL_SetProperty("Flag Objective False", sObjectiveName)
       AL_SetProperty("Flag Objective True", sObjectiveName)
       AL_SetProperty("Clear Objectives")
       AL_SetProperty("Add Dislocation", sName, sLayerName, iX, iY, iW, iH, fXRender, fYRender)
       AL_SetProperty("Modify Dislocation", sName, fXRender, fYRender)
       AL_SetProperty("Major Animation", sMajorAnimationName)
       AL_SetProperty("Add Animation", sName, fXRender, fYRender, fYRender, sTilesetName, fSizePerFrameX, fTicksPerFrame, iTotalFrames)
       AL_SetProperty("Set Animation Rendering", sName, bIsRendering)
       AL_SetProperty("Set Animation Destination", sName, fDestinationFrame)
       AL_SetProperty("Set Animation Destination", sName, fDestinationFrame, fTickRate)
       AL_SetProperty("Set Animation Loop", sName, fStartFrame, fEndFrame, sLoopType, fTickRateToMidpoint, fTicksPerWholeLoop) (sLoopType is "Sin", "Loop", or "Reverse")
       AL_SetProperty("Set Animation Frame", sName, fOverrideFrame)
       AL_SetProperty("Reset Actor Graphics")
       AL_SetProperty("Screen Shake", iScreenShakeTimer)
       AL_SetProperty("Screen Shake Periodicity", iLength, iPeriod, iPeriodScatter)
       AL_SetProperty("Add Shake Sound", sSoundPath)
       AL_SetProperty("Rock Fall Chance", iRollChance)
       AL_SetProperty("Add Rock Fall Image", sDLPath)
       AL_SetProperty("Hide All Viewcones", bFlag)
       AL_SetProperty("Create Staircase", sName, fX, fY, fW, fH, sDirection, sStairDest, sMapDest)
       AL_SetProperty("Activate Running Minigame")
       AL_SetProperty("Activate KPop Minigame")
       Returns the requested property from the current AdventureLevel. */
    lua_register(pLuaState, "AL_SetProperty", &Hook_AL_SetProperty);

    /* AL_BeginTransitionTo(sMapName, sTransitionPostExitScript)
       AL_BeginTransitionTo(sMapName, "FORCEPOS:XXXxYYYxDDD")
       Begins transition to the named map. If the map doesn't exist, this will fail. It should be
       the name of the containing folder. The sTransitionPostExecScript will fire immediately
       after the transition occurs, meaing while the screen is fading.
       If the FORCEPOS: code is used, the position is specified directly, including the depth. The position
       is in tiles and can include decimal places. Use 'x' as a delimiter.*/
    lua_register(pLuaState, "AL_BeginTransitionTo", &Hook_AL_BeginTransitionTo);

    /* AL_RemoveObject("Chest", sChestName)
       AL_RemoveObject("To Fake Chest", sChestName)
       AL_RemoveObject("Door", sDoorName)
       AL_RemoveObject("Exit", sExitName)
       Removes the requested object from the level. The object will remain viable for at least the
       duration of the tick, as it will be placed on the MemoryManager's heap. After the tick it
       is unstable, so be sure to clear it from the Activity Stack if you are using it. */
    lua_register(pLuaState, "AL_RemoveObject", &Hook_AL_RemoveObject);

    /* AL_PulseIgnore()
       AL_PulseIgnore(sString)
       Pulses the ignore state for all living entities using the provided string. If no string is passed,
       all enemies will become hostile to the player. */
    lua_register(pLuaState, "AL_PulseIgnore", &Hook_AL_PulseIgnore);

    /* AL_CreateObject("Examinable", sExaminableString, iLft, iTop, iWid, iHei)
       AL_CreateObject("Path Node", sName, iX, iY, iW, iH)
       Creates the given object in the level. Does not push the object, all properties needed are in the arg list. */
    lua_register(pLuaState, "AL_CreateObject", &Hook_AL_CreateObject);
}

//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
int Hook_AL_Create(lua_State *L)
{
    //AL_Create()
    AdventureLevel *nNewLevel = new AdventureLevel();
    MapManager::Fetch()->ReceiveLevel(nNewLevel);

    //--Finish up.
    return 0;
}
#include "VirtualFile.h"
#include "TileLayer.h"
int Hook_AL_ParseSLF(lua_State *L)
{
    //AL_ParseSLF(sSLFPath)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("AL_ParseSLF");

    //--Get and check.
    AdventureLevel *rCheckLevel = AdventureLevel::Fetch();
    if(!rCheckLevel) return LuaTypeError("AL_ParseSLF");

    //--Size setters.
    TileLayer::cxSizePerTile = 16.0f;
    SugarLumpManager::xTileSizeX = 16;
    SugarLumpManager::xTileSizeY = 16;

    //--Open the SLF file. AdventureLevels must *always* be opened in memory mode.
    bool tOldFlag = VirtualFile::xUseRAMLoading;
    VirtualFile::xUseRAMLoading = true;
    SugarLumpManager *rSLM = SugarLumpManager::Fetch();
    rSLM->Open(lua_tostring(L, 1));

    //--Instruction.
    rCheckLevel->ParseFile(rSLM);

    //--Finish up.
    rSLM->Close();
    VirtualFile::xUseRAMLoading = tOldFlag;
    return 0;
}
int Hook_AL_GetProperty(lua_State *L)
{
    //AL_GetProperty("Last Save") (1 string) (Static)
    //AL_GetProperty("Switch State") (1 boolean) (Static)
    //AL_GetProperty("Music") (1 string) (Static)
    //AL_GetProperty("Name") (1 string)
    //AL_GetProperty("Is Door Open", sDoorName) (1 boolean)
    //AL_GetProperty("Player Start") (2 integers)
    //AL_GetProperty("Is Character Following", sName) (1 boolean)
    //AL_GetProperty("Party Leader Name") (1 string)
    //AL_GetProperty("Follower Name", iIndex) (1 string)
    //AL_GetProperty("Last Chest Position") (2 floats)
    //AL_GetProperty("Exit Exists", sExitName) (1 boolean)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("AL_GetProperty");

    //--Switching.
    int tReturns = 0;
    const char *rSwitchType = lua_tostring(L, 1);

    //--[Static Values]
    //--The name of the last room we saved in.
    if(!strcasecmp(rSwitchType, "Last Save") && tArgs == 1)
    {
        lua_pushstring(L, AdventureLevel::xLastSavePoint);
        return 1;
    }
    //--Whether or not the switch currently under investigation is up.
    else if(!strcasecmp(rSwitchType, "Switch State") && tArgs == 1)
    {
        lua_pushboolean(L, AdventureLevel::xIsSwitchUp);
        return 1;
    }
    //--Name of the music playing.
    else if(!strcasecmp(rSwitchType, "Music") && tArgs == 1)
    {
        lua_pushstring(L, AdventureLevel::xLevelMusic);
        return 1;
    }

    //--Active object.
    AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
    if(!rActiveLevel) return LuaTypeError("AL_GetProperty");

    //--Name of the current level.
    if(!strcasecmp(rSwitchType, "Name") && tArgs == 1)
    {
        lua_pushstring(L, rActiveLevel->GetName());
        tReturns = 1;
    }
    else if(!strcasecmp(rSwitchType, "Is Door Open") && tArgs == 2)
    {
        lua_pushboolean(L, rActiveLevel->IsDoorOpen(lua_tostring(L, 2)));
        tReturns = 1;
    }
    //AL_GetProperty("") (1 boolean)
    //--Player Start, used for debug and/or first spawn situation.
    else if(!strcasecmp(rSwitchType, "Player Start") && tArgs == 1)
    {
        lua_pushinteger(L, rActiveLevel->GetPlayerStartX());
        lua_pushinteger(L, rActiveLevel->GetPlayerStartY());
        tReturns = 2;
    }
    //--Whether or not a given character is in the party. Also checks the party leader.
    else if(!strcasecmp(rSwitchType, "Is Character Following") && tArgs == 2)
    {
        lua_pushboolean(L, rActiveLevel->IsCharacterInWorldParty(lua_tostring(L, 2)));
        tReturns = 1;
    }
    //--Name of the party leader.
    else if(!strcasecmp(rSwitchType, "Party Leader Name") && tArgs == 1)
    {
        //--Normal case:
        TilemapActor *rPartyLeader = rActiveLevel->LocatePlayerActor();
        if(rPartyLeader)
        {
            lua_pushstring(L, rPartyLeader->GetName());
        }
        //--Error.
        else
        {
            lua_pushstring(L, "Null");
        }

        //--Common.
        tReturns = 1;
    }
    //--Name of the follower in the given slot.
    else if(!strcasecmp(rSwitchType, "Follower Name") && tArgs == 2)
    {
        //--Normal case:
        TilemapActor *rFollower = rActiveLevel->LocateFollowingActor(lua_tointeger(L, 2));
        if(rFollower)
        {
            lua_pushstring(L, rFollower->GetName());
        }
        //--Error.
        else
        {
            lua_pushstring(L, "Null");
        }

        //--Common.
        tReturns = 1;
    }
    //--Location of the last opened chest.
    else if(!strcasecmp(rSwitchType, "Last Chest Position") && tArgs == 1)
    {
        lua_pushnumber(L, rActiveLevel->mLastChestX);
        lua_pushnumber(L, rActiveLevel->mLastChestY);
        tReturns = 2;
    }
    //--Returns true if an exit of the given name exists. Only exits, not doors.
    else if(!strcasecmp(rSwitchType, "Exit Exists") && tArgs == 2)
    {
        lua_pushboolean(L, rActiveLevel->DoesExitExist(lua_tostring(L, 2)));
        tReturns = 1;
    }
    //--Error.
    else
    {
        LuaPropertyError("TL_GetProperty", rSwitchType, tArgs);
    }

    //--Finish up.
    return tReturns;
}
int Hook_AL_SetProperty(lua_State *L)
{
    //--[Static System]
    //AL_SetProperty("Root Path", sPath) (Static)
    //AL_SetProperty("Item Path", sPath) (Static)
    //AL_SetProperty("Item Image Path", sPath) (Static)
    //AL_SetProperty("Catalyst Path", sPath) (Static)
    //AL_SetProperty("Trigger Handled Update") (Static)
    //AL_SetProperty("Switch Handled Update") (Static)
    //AL_SetProperty("Last Save Point", sLevelName) (Static)
    //AL_SetProperty("Music", sMusicName) (Static)
    //AL_SetProperty("Remappings Total", iCount) (Static)
    //AL_SetProperty("Remapping", iSlot, sStartName, sRemappedName) (Static)
    //AL_SetProperty("Wipe Destroyed Enemies") (Static)
    //AL_SetProperty("Ambient Light Boost", fValue) (Static)
    //AL_SetProperty("Mandated Music Intensity", fValue) (Static)
    //AL_SetProperty("Mandated Music Intensity Now", fValue) (Static)

    //--[Static Layered Music]
    //AL_SetProperty("Layered Track Routing Path", sPath) (Static)
    //AL_SetProperty("Is Layering Music", bFlag) (Static)
    //AL_SetProperty("Is Combat Max Intensity", bFlag) (Static)
    //AL_SetProperty("Total Layering Tracks", iTotal) (Static)
    //AL_SetProperty("Layered Track Name", iSlot, sName) (Static)
    //AL_SetProperty("Layered Track Volume At Intensity", iSlot, iIntensity, fVolume) (Static)

    //--[Other Properties]
    //AL_SetProperty("Name", sName)
    //AL_SetProperty("Background", sDLPath, fStartX, fStartY, fScrollX, fScrollY)
    //AL_SetProperty("Player Actor ID", iUniqueID)
    //AL_SetProperty("Follow Actor ID", iUniqueID)
    //AL_SetProperty("Unfollow Actor Name", sName)
    //AL_SetProperty("Open Door", sName)
    //AL_SetProperty("Close Door", sName)
    //AL_SetProperty("Set Layer Disabled", sLayerName, bFlag)
    //AL_SetProperty("Set Animation Disabled", sLayerName, bFlag)
    //AL_SetProperty("Examine Script", sPath)
    //AL_SetProperty("Trigger Script", sPath)
    //AL_SetProperty("Add Examinable", sName, iX, iY)
    //AL_SetProperty("Add Examinable", sName, iX, iY, iW, iH)
    //AL_SetProperty("Add Enemy Spawn", sName, iX, iY, sParty, sAppearance, sScene, iToughness, sPatrolPath, sFollow)
    //AL_SetProperty("Add Chest", sName, iX, iY, bIsFuture, sContents)
    //AL_SetProperty("Camera Lock", bFlag)
    //AL_SetProperty("Run Enemy Spawner")
    //AL_SetProperty("Switch State", sSwitchName, bIsUp)
    //AL_SetProperty("Activate Fade", iTickDuration, iDepthFlag, bHoldsOnCompletion, fSRed, fSGreen, fSBlue, fSAlpha, fERed, fEGreen, fEBlue, fEAlpha)
    //AL_SetProperty("Activate Fade", iTickDuration, iDepthFlag, bHoldsOnCompletion, fSRed, fSGreen, fSBlue, fSAlpha, fERed, fEGreen, fEBlue, fEAlpha, bAlternateFade)
    //AL_SetProperty("Reposition Party", fXPos, fYPos)
    //AL_SetProperty("Reposition Party", fXPos, fYPos, iFacing)
    //AL_SetProperty("Fold Party")
    //AL_SetProperty("Activate Underwater")
    //AL_SetProperty("Deactivate Underwater")
    //AL_SetProperty("Set Parallel Cutscenes During Dialogue Flag", bFlag)
    //AL_SetProperty("Set Parallel Cutscenes During Cutscenes Flag", bFlag)
    //AL_SetProperty("Activate Lights")
    //AL_SetProperty("Deactivate Lights")
    //AL_SetProperty("Set Ambient Light", fRed, fGreen, fBlue, fAlpha)
    //AL_SetProperty("Register Radial Light", sName, fXPos, fYPos, fIntensity)
    //AL_SetProperty("Attach Light To Entity", sLightName, sEntityName)
    //AL_SetProperty("Enable Light", sName)
    //AL_SetProperty("Disable Light", sName)
    //AL_SetProperty("Modify Light Color", sName, pR, pG, pB, pA)
    //AL_SetProperty("Activate Player Light", iCurrentPower, iMaxPower)
    //AL_SetProperty("Deactivate Player Light")
    //AL_SetProperty("Set Player Light No Drain", bFlag)
    //AL_SetProperty("Set Collision", iX, iY, iZ, iCollisionValue)
    //AL_SetProperty("Allocate Foregrounds", iTotal)
    //AL_SetProperty("Foreground Image", iSlot, sPath)
    //AL_SetProperty("Foreground Render Offsets", iSlot, fOffsetX, fOffsetY, fScalerX, fScalerY)
    //AL_SetProperty("Foreground Alpha", iSlot, fAlphaTarget, iTicks)
    //AL_SetProperty("Foreground Autoscroll", iSlot, fAutoscrollX, fAutoscrollY)
    //AL_SetProperty("Foreground Scale", iSlot, fScaleFactor)
    //AL_SetProperty("Allocate Backgrounds", iTotal)
    //AL_SetProperty("Background Image", iSlot, sPath)
    //AL_SetProperty("Background Render Offsets", iSlot, fOffsetX, fOffsetY, fScalerX, fScalerY)
    //AL_SetProperty("Background Alpha", iSlot, fAlphaTarget, iTicks)
    //AL_SetProperty("Background Autoscroll", iSlot, fAutoscrollX, fAutoscrollY)
    //AL_SetProperty("Background Scale", iSlot, fScaleFactor)
    //AL_SetProperty("Register Objective", sObjectiveName)
    //AL_SetProperty("Flag Objective False", sObjectiveName)
    //AL_SetProperty("Flag Objective True", sObjectiveName)
    //AL_SetProperty("Clear Objectives")
    //AL_SetProperty("Add Dislocation", sName, sLayerName, iX, iY, iW, iH, fXRender, fYRender)
    //AL_SetProperty("Modify Dislocation", sName, fXRender, fYRender)
    //AL_SetProperty("Major Animation", sMajorAnimationName)
    //AL_SetProperty("Add Animation", sName, fXRender, fYRender)
    //AL_SetProperty("Set Animation From Pattern", sName, sPattern, iFramesExpected)
    //AL_SetProperty("Set Animation Rendering", sName, bIsRendering)
    //AL_SetProperty("Set Animation Destination", sName, fDestinationFrame)
    //AL_SetProperty("Set Animation Destination", sName, fDestinationFrame, fTickRate)
    //AL_SetProperty("Set Animation Loop", sName, fStartFrame, fEndFrame, sLoopType, fTickRateToMidpoint, fTicksPerWholeLoop) (sLoopType is "Sin", "Loop", or "Reverse")
    //AL_SetProperty("Set Animation Frame", sName, fOverrideFrame)
    //AL_SetProperty("Reset Actor Graphics")
    //AL_SetProperty("Screen Shake", iScreenShakeTimer)
    //AL_SetProperty("Screen Shake Periodicity", iLength, iPeriod, iPeriodScatter)
    //AL_SetProperty("Add Shake Sound", sSoundPath)
    //AL_SetProperty("Rock Fall Chance", iRollChance)
    //AL_SetProperty("Add Rock Fall Image", sDLPath)
    //AL_SetProperty("Hide All Viewcones", bFlag)
    //AL_SetProperty("Create Staircase", sName, fX, fY, fW, fH, sDirection, sStairDest, sMapDest)
    //AL_SetProperty("Activate Running Minigame")
    //AL_SetProperty("Activate KPop Minigame")
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("AL_SetProperty");

    //--Switching.
    const char *rSwitchType = lua_tostring(L, 1);

    //--[Static Values]
    //--Path of the base of Adventure Mode.
    if(!strcasecmp(rSwitchType, "Root Path") && tArgs == 2)
    {
        ResetString(AdventureLevel::xRootPath, lua_tostring(L, 2));
        return 0;
    }
    //--Path of the global item storage script.
    else if(!strcasecmp(rSwitchType, "Item Path") && tArgs == 2)
    {
        ResetString(AdventureLevel::xItemListPath, lua_tostring(L, 2));
        return 0;
    }
    //--Path of the item image remapper.
    else if(!strcasecmp(rSwitchType, "Item Image Path") && tArgs == 2)
    {
        ResetString(AdventureLevel::xItemImageListPath, lua_tostring(L, 2));
        return 0;
    }
    //--Path of the global catalyst handler script.
    else if(!strcasecmp(rSwitchType, "Catalyst Path") && tArgs == 2)
    {
        ResetString(AdventureLevel::xCatalystHandlerPath, lua_tostring(L, 2));
        return 0;
    }
    //--Flags that a trigger handled the update and stops player input.
    else if(!strcasecmp(rSwitchType, "Trigger Handled Update") && tArgs == 1)
    {
        AdventureLevel::xTriggerHandledUpdate = true;
        return 0;
    }
    //--Flags that the switch which was activated handled the update.
    else if(!strcasecmp(rSwitchType, "Switch Handled Update") && tArgs == 1)
    {
        AdventureLevel::xExaminationDidSomething = true;
        return 0;
    }
    //--Last room the player used a save point in. Used for manually overriding. Player returns to last save if beaten.
    else if(!strcasecmp(rSwitchType, "Last Save Point") && tArgs == 2)
    {
        ResetString(AdventureLevel::xLastSavePoint, lua_tostring(L, 2));
        return 0;
    }
    //--Which music track is playing. "Null" is legal and will stop the music.
    else if(!strcasecmp(rSwitchType, "Music") && tArgs == 2)
    {
        //--If the music name is "NullSlow" then the music will slowly fade out. Only used for cutscenes.
        if(!strcasecmp(lua_tostring(L, 2), "NullSlow"))
        {
            AdventureLevel::SetMusic(lua_tostring(L, 2), true);
        }
        //--Otherwuse, half-second fade.
        else
        {
            AdventureLevel::SetMusic(lua_tostring(L, 2), false);
        }

        return 0;
    }
    //--How many remappings there are. Deallocates existing remappings.
    else if(!strcasecmp(rSwitchType, "Remappings Total") && tArgs == 2)
    {
        //--Clear data.
        for(int i = 0; i < AdventureLevel::xRemappingsTotal; i ++)
        {
            free(AdventureLevel::xRemappingsCheck[i]);
            free(AdventureLevel::xRemappingsResult[i]);
        }
        free(AdventureLevel::xRemappingsCheck);
        free(AdventureLevel::xRemappingsResult);
        AdventureLevel::xRemappingsCheck = NULL;
        AdventureLevel::xRemappingsResult = NULL;

        //--Check.
        AdventureLevel::xRemappingsTotal = 0;
        int tTotal = lua_tointeger(L, 2);
        if(tTotal < 1) return 0;

        //--Allocate.
        AdventureLevel::xRemappingsTotal = tTotal;
        SetMemoryData(__FILE__, __LINE__);
        AdventureLevel::xRemappingsCheck  = (char **)starmemoryalloc(sizeof(char *) * AdventureLevel::xRemappingsTotal);
        SetMemoryData(__FILE__, __LINE__);
        AdventureLevel::xRemappingsResult = (char **)starmemoryalloc(sizeof(char *) * AdventureLevel::xRemappingsTotal);

        //--Null them off.
        for(int i = 0; i < AdventureLevel::xRemappingsTotal; i ++)
        {
            AdventureLevel::xRemappingsCheck[i] = NULL;
            AdventureLevel::xRemappingsResult[i] = NULL;
        }

        return 0;
    }
    //--Set a remapping.
    else if(!strcasecmp(rSwitchType, "Remapping") && tArgs == 4)
    {
        //--Range check.
        int tSlot = lua_tointeger(L, 2);
        if(tSlot < 0 || tSlot >= AdventureLevel::xRemappingsTotal) return 0;

        //--Set.
        ResetString(AdventureLevel::xRemappingsCheck[tSlot], lua_tostring(L, 3));
        ResetString(AdventureLevel::xRemappingsResult[tSlot], lua_tostring(L, 4));

        //--Debug:
        if(false && tSlot == AdventureLevel::xRemappingsTotal - 1)
        {
            fprintf(stderr, "Printing level remaps:\n");
            for(int i = 0; i < AdventureLevel::xRemappingsTotal; i ++)
            {
                fprintf(stderr, " %3i: %s %s\n", i, AdventureLevel::xRemappingsCheck[i], AdventureLevel::xRemappingsResult[i]);
            }
        }
        return 0;
    }
    //--Remove all destroyed enemies from the static list.
    else if(!strcasecmp(rSwitchType, "Wipe Destroyed Enemies") && tArgs == 1)
    {
        AdventureLevel::WipeDestroyedEnemies();
        return 0;
    }
    //--Sets the Ambient Light Boost. Only use during boot.
    else if(!strcasecmp(rSwitchType, "Ambient Light Boost") && tArgs == 2)
    {
        AdventureLevel::SetLightBoost(lua_tonumber(L, 2));
        return 0;
    }
    //--Overrides layered track intensity. Use -1.0f to disable manual intensity.
    else if(!strcasecmp(rSwitchType, "Mandated Music Intensity") && tArgs == 2)
    {
        AdventureLevel::xScriptMandatedIntensity = lua_tonumber(L, 2);
        return 0;
    }
    //--Orders intensity mandate to occur immediately.
    else if(!strcasecmp(rSwitchType, "Mandated Music Intensity Now") && tArgs == 2)
    {
        AdventureLevel::xScriptMandatedIntensity = lua_tonumber(L, 2);
        AdventureLevel::xScriptMandateIntensityNow = true;
        return 0;
    }
    //--[Static Layered Music]
    //--Routing file used when setting music properties.
    else if(!strcasecmp(rSwitchType, "Layered Track Routing Path") && tArgs == 2)
    {
        ResetString(AdventureLevel::xLayerResolverPath, lua_tostring(L, 2));
        return 0;
    }
    //--Flag to indicate whether or not the routing file handled the music.
    else if(!strcasecmp(rSwitchType, "Is Layering Music") && tArgs == 2)
    {
        AdventureLevel::xIsLayeringMusic = lua_toboolean(L, 2);
        return 0;
    }
    //--If true, the combat track is replaced by intensity going to 100.
    else if(!strcasecmp(rSwitchType, "Is Combat Max Intensity") && tArgs == 2)
    {
        AdventureLevel::xIsCombatMaxIntensity = lua_toboolean(L, 2);
        return 0;
    }
    //--How many tracks to use. Max is MAX_MUSIC_LAYERS. Minimum is 1.
    else if(!strcasecmp(rSwitchType, "Total Layering Tracks") && tArgs == 2)
    {
        AdventureLevel::xCurrentlyRunningTracks = lua_tointeger(L, 2);
        if(AdventureLevel::xCurrentlyRunningTracks < 1) AdventureLevel::xCurrentlyRunningTracks = 1;
        if(AdventureLevel::xCurrentlyRunningTracks > MAX_MUSIC_LAYERS) AdventureLevel::xCurrentlyRunningTracks = MAX_MUSIC_LAYERS;
        return 0;
    }
    //--Name of the layered track as it appears in the AudioManager's music list.
    else if(!strcasecmp(rSwitchType, "Layered Track Name") && tArgs == 3)
    {
        //--Get slot, range check.
        int tSlot = lua_tointeger(L, 2);
        if(tSlot < 0 || tSlot >= MAX_MUSIC_LAYERS)
        {
            DebugManager::ForcePrint("Error setting layered track name: Slot %i is out of range.\n", tSlot);
            return 0;
        }

        //--Valid, set.
        strncpy(AdventureLevel::xLayerNames[tSlot], lua_tostring(L, 3), STD_MAX_LETTERS - 1);
        return 0;
    }
    //--Volume of a given track for a given intensity.
    else if(!strcasecmp(rSwitchType, "Layered Track Volume At Intensity") && tArgs == 4)
    {
        //--Get slot, range check.
        int tSlot = lua_tointeger(L, 2);
        if(tSlot < 0 || tSlot >= MAX_MUSIC_LAYERS)
        {
            DebugManager::ForcePrint("Error setting layered track volume: Slot %i is out of range.\n", tSlot);
            return 0;
        }

        //--Get intensity, range check.
        int tIntensity = lua_tointeger(L, 3);
        if(tIntensity < 0 || tIntensity > 100)
        {
            DebugManager::ForcePrint("Error setting layered track intensity: Intensity %i is out of range.\n", tIntensity);
            return 0;
        }

        //--Valid. Set.
        AdventureLevel::xLayerVolumes[tSlot][tIntensity] = lua_tonumber(L, 4);
        return 0;
    }

    //--[Dynamic Values]
    //--Active object.
    AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
    if(!rActiveLevel) return LuaTypeError("AL_SetProperty");

    //--Name of the level, used for chests.
    if(!strcasecmp(rSwitchType, "Name") && tArgs == 2)
    {
        rActiveLevel->SetName(lua_tostring(L, 2));
    }
    //--Background info.
    else if(!strcasecmp(rSwitchType, "Background") && tArgs == 6)
    {
        rActiveLevel->SetBackground(lua_tostring(L, 2));
        rActiveLevel->SetBackgroundPositions(lua_tonumber(L, 3), lua_tonumber(L, 4));
        rActiveLevel->SetBackgroundScrolls(lua_tonumber(L, 5), lua_tonumber(L, 6));
    }
    //--Sets which entity will handle player input.
    else if(!strcasecmp(rSwitchType, "Player Actor ID") && tArgs == 2)
    {
        rActiveLevel->SetControlEntityID(lua_tointeger(L, 2));
    }
    //--Adds an entity which follows the player.
    else if(!strcasecmp(rSwitchType, "Follow Actor ID") && tArgs == 2)
    {
        rActiveLevel->AddFollowEntityID(lua_tointeger(L, 2));
    }
    //--Stops this entity from following the player.
    else if(!strcasecmp(rSwitchType, "Unfollow Actor Name") && tArgs == 2)
    {
        rActiveLevel->RemoveFollowEntityName(lua_tostring(L, 2));
    }
    //--Opens a door. Does not play any SFX by default.
    else if(!strcasecmp(rSwitchType, "Open Door") && tArgs == 2)
    {
        rActiveLevel->OpenDoor(lua_tostring(L, 2));
    }
    //--Closes a door. Does not play any SFX by default.
    else if(!strcasecmp(rSwitchType, "Close Door") && tArgs == 2)
    {
        rActiveLevel->CloseDoor(lua_tostring(L, 2));
    }
    //--Turns a layer's display on or off. By default, all layers except collisions are visible.
    else if(!strcasecmp(rSwitchType, "Set Layer Disabled") && tArgs == 3)
    {
        rActiveLevel->SetRenderingDisabled(lua_tostring(L, 2), lua_toboolean(L, 3));
    }
    //--Turns a layer's animation on or off. Has no effect on a layer that isn't animating.
    else if(!strcasecmp(rSwitchType, "Set Animation Disabled") && tArgs == 3)
    {
        rActiveLevel->SetAnimationDisabled(lua_tostring(L, 2), lua_toboolean(L, 3));
    }
    //--Set the directory that the .slf file is in.
    else if(!strcasecmp(rSwitchType, "Base Path") && tArgs == 2)
    {
        rActiveLevel->SetBasePath(lua_tostring(L, 2));
    }
    //--Set which script is used to execute examination calls.
    else if(!strcasecmp(rSwitchType, "Examine Script") && tArgs == 2)
    {
        rActiveLevel->SetExaminationScript(lua_tostring(L, 2));
    }
    //--Script used for trigger calls.
    else if(!strcasecmp(rSwitchType, "Trigger Script") && tArgs == 2)
    {
        rActiveLevel->SetTriggerScript(lua_tostring(L, 2));
    }
    //--Creates a 1x1 examinable zone.
    else if(!strcasecmp(rSwitchType, "Add Examinable") && tArgs == 4)
    {
        rActiveLevel->AddExaminable(lua_tointeger(L, 3), lua_tointeger(L, 4), lua_tostring(L, 2));
    }
    //--Creates a variable-sized examinable zone.
    else if(!strcasecmp(rSwitchType, "Add Examinable") && tArgs == 6)
    {
        rActiveLevel->AddExaminable(lua_tointeger(L, 3), lua_tointeger(L, 4), lua_tointeger(L, 5), lua_tointeger(L, 6), 0, lua_tostring(L, 2));
    }
    //--Creates a new enemy spawn package.
    else if(!strcasecmp(rSwitchType, "Add Enemy Spawn") && tArgs == 10)
    {
        rActiveLevel->AddEnemy(lua_tostring(L, 2), lua_tointeger(L, 3), lua_tointeger(L, 4), lua_tostring(L, 5), lua_tostring(L, 6), lua_tostring(L, 7), lua_tointeger(L, 8), lua_tostring(L, 9), lua_tostring(L, 10));
    }
    //--Creates a new enemy spawn package.
    else if(!strcasecmp(rSwitchType, "Add Chest") && tArgs == 6)
    {
        rActiveLevel->AddChest(lua_tostring(L, 2), lua_tointeger(L, 3), lua_tointeger(L, 4), lua_toboolean(L, 5), lua_tostring(L, 6));
    }
    //--Whether or not the camera will auto-follow the player character.
    else if(!strcasecmp(rSwitchType, "Camera Lock") && tArgs == 2)
    {
        rActiveLevel->SetCameraLocking(lua_toboolean(L, 2));
    }
    //--Runs the enemy spawn routines. Occurs automatically when the player rests, but can/should be called
    //  manually during level loading. You can then despawn or modify the NPCs as desired.
    else if(!strcasecmp(rSwitchType, "Run Enemy Spawner") && tArgs == 1)
    {
        rActiveLevel->SpawnEnemies();
    }
    //--Sets whether or not the named switch is up or down.
    else if(!strcasecmp(rSwitchType, "Switch State") && tArgs == 3)
    {
        rActiveLevel->SetSwitchState(lua_tostring(L, 2), lua_toboolean(L, 3));
    }
    //--Starts the fading process as managed by scripts. Not the same as transition cases.
    else if(!strcasecmp(rSwitchType, "Activate Fade") && tArgs == 12)
    {
        StarlightColor tStartColor = StarlightColor::MapRGBAF(lua_tonumber(L, 5), lua_tonumber(L, 6), lua_tonumber(L, 7), lua_tonumber(L, 8));
        StarlightColor tEndColor   = StarlightColor::MapRGBAF(lua_tonumber(L, 9), lua_tonumber(L, 10), lua_tonumber(L, 11), lua_tonumber(L, 12));
        rActiveLevel->ActivateScriptFade(lua_tointeger(L, 2), tStartColor, tEndColor, lua_tointeger(L, 3), lua_toboolean(L, 4));
    }
    //--As above, but also activates alterative blending. Used for nighttime overlays.
    else if(!strcasecmp(rSwitchType, "Activate Fade") && tArgs == 13)
    {
        StarlightColor tStartColor = StarlightColor::MapRGBAF(lua_tonumber(L, 5), lua_tonumber(L, 6), lua_tonumber(L, 7), lua_tonumber(L, 8));
        StarlightColor tEndColor   = StarlightColor::MapRGBAF(lua_tonumber(L, 9), lua_tonumber(L, 10), lua_tonumber(L, 11), lua_tonumber(L, 12));
        rActiveLevel->ActivateScriptFade(lua_tointeger(L, 2), tStartColor, tEndColor, lua_tointeger(L, 3), lua_toboolean(L, 4));
        rActiveLevel->SetAlternateBlending(lua_toboolean(L, 13));
    }
    //--Repositions the party to the given location. Does not change their facing.
    else if(!strcasecmp(rSwitchType, "Reposition Party") && tArgs == 3)
    {
        rActiveLevel->RepositionParty(NULL, lua_tointeger(L, 2), lua_tointeger(L, 3));
    }
    //--Repositions the party to the given location, facing the given direction.
    else if(!strcasecmp(rSwitchType, "Reposition Party") && tArgs == 4)
    {
        TilemapActor *rPlayerActor = rActiveLevel->LocatePlayerActor();
        if(rPlayerActor) rPlayerActor->SetFacing(lua_tointeger(L, 4));
        rActiveLevel->RepositionParty(rPlayerActor, lua_tointeger(L, 2), lua_tointeger(L, 3));
    }
    //--Repositions the party to the current leader position.
    else if(!strcasecmp(rSwitchType, "Fold Party") && tArgs == 1)
    {
        rActiveLevel->RepositionParty(NULL, -700.0f, -700.0f);
    }
    //--Activates underwater mode.
    else if(!strcasecmp(rSwitchType, "Activate Underwater") && tArgs == 1)
    {
        rActiveLevel->ActivateUnderwaterShader();
    }
    //--Deactivates underwater mode.
    else if(!strcasecmp(rSwitchType, "Deactivate Underwater") && tArgs == 1)
    {
        rActiveLevel->DeactivateUnderwaterShader();
    }
    //--Toggles on/off parallel cutscenes running when dialogue is playing.
    else if(!strcasecmp(rSwitchType, "Set Parallel Cutscenes During Dialogue Flag") && tArgs == 2)
    {
        rActiveLevel->SetParallelCutsceneDuringDialogueFlag(lua_toboolean(L, 2));
    }
    //--Toggles on/off parallel cutscenes running when a cutscene is playing.
    else if(!strcasecmp(rSwitchType, "Set Parallel Cutscenes During Cutscenes Flag") && tArgs == 2)
    {
        rActiveLevel->SetParallelCutsceneDuringCutsceneFlag(lua_toboolean(L, 2));
    }
    //--Activates lighting mode.
    else if(!strcasecmp(rSwitchType, "Activate Lights") && tArgs == 1)
    {
        rActiveLevel->ActivateLighting();
    }
    //--Deactivates lighting.
    else if(!strcasecmp(rSwitchType, "Deactivate Lights") && tArgs == 1)
    {
        rActiveLevel->DeactivateLighting();
    }
    //--Manually sets ambient lighting.
    else if(!strcasecmp(rSwitchType, "Set Ambient Light") && tArgs == 5)
    {
        rActiveLevel->SetAmbientLight(lua_tonumber(L, 2), lua_tonumber(L, 3), lua_tonumber(L, 4), lua_tonumber(L, 5));
    }
    //--Registers a light of radial type with the given properties.
    else if(!strcasecmp(rSwitchType, "Register Radial Light") && tArgs == 5)
    {
        AdventureLight *nLight = new AdventureLight();
        nLight->SetName(lua_tostring(L, 2));
        nLight->SetPosition(lua_tonumber(L, 3), lua_tonumber(L, 4));
        nLight->SetRadial(lua_tonumber(L, 5));
        rActiveLevel->RegisterLight(nLight);
    }
    //--Associates an entity with a given light. When the entity moves, so does the light.
    else if(!strcasecmp(rSwitchType, "Attach Light To Entity") && tArgs == 3)
    {
        rActiveLevel->AttachLightToEntity(lua_tostring(L, 2), lua_tostring(L, 3));
    }
    //--Enable a light.
    else if(!strcasecmp(rSwitchType, "Enable Light") && tArgs == 2)
    {
        rActiveLevel->EnableLight(lua_tostring(L, 2));
    }
    //--Disable a light.
    else if(!strcasecmp(rSwitchType, "Disable Light") && tArgs == 2)
    {
        rActiveLevel->DisableLight(lua_tostring(L, 2));
    }
    //--Changes the light color.
    else if(!strcasecmp(rSwitchType, "Modify Light Color") && tArgs == 6)
    {
        rActiveLevel->ModifyLightColor(lua_tostring(L, 2), lua_tonumber(L, 3), lua_tonumber(L, 4), lua_tonumber(L, 5), lua_tonumber(L, 6));
    }
    //--Gives the player a mobile light source.
    else if(!strcasecmp(rSwitchType, "Activate Player Light") && tArgs == 3)
    {
        rActiveLevel->ActivatePlayerLight();
        rActiveLevel->SetPlayerLightPower(lua_tointeger(L, 2));
        rActiveLevel->SetPlayerLightPowerMax(lua_tointeger(L, 3));
    }
    //--Take away the player's mobile light source.
    else if(!strcasecmp(rSwitchType, "Deactivate Player Light") && tArgs == 1)
    {
        rActiveLevel->DeactivatePlayerLight();
    }
    //--Mobile light source is set to max power and does not drain. Used in mid-light situations.
    else if(!strcasecmp(rSwitchType, "Set Player Light No Drain") && tArgs == 2)
    {
        rActiveLevel->SetPlayerLightNoDrain(lua_toboolean(L, 2));
    }
    //--Changes a map collision. Does NOT affect the ray-casting engine!
    else if(!strcasecmp(rSwitchType, "Set Collision") && tArgs == 5)
    {
        rActiveLevel->SetCollision(lua_tointeger(L, 2), lua_tointeger(L, 3), lua_tointeger(L, 4), lua_tointeger(L, 5));
    }
    //--[Foregrounds/Overlays]
    //--How many foreground layers there are. Default is 0.
    else if(!strcasecmp(rSwitchType, "Allocate Foregrounds") && tArgs == 2)
    {
        rActiveLevel->AllocateForegroundPacks(lua_tointeger(L, 2));
    }
    //--Sets which image is used for the foreground. Pass "Null" to disable.
    else if(!strcasecmp(rSwitchType, "Foreground Image") && tArgs == 3)
    {
        rActiveLevel->SetForegroundImage(lua_tointeger(L, 2), lua_tostring(L, 3));
    }
    //--Sets the offsets when rendering the foreground image. Scaler can be negative.
    else if(!strcasecmp(rSwitchType, "Foreground Render Offsets") && tArgs == 6)
    {
        rActiveLevel->SetForegroundOffsets(lua_tointeger(L, 2), lua_tonumber(L, 3), lua_tonumber(L, 4), lua_tonumber(L, 5), lua_tonumber(L, 6));
    }
    //--Changes the alpha of the foreground over time to the desired value. Pass 1 or lower to set it instantly.
    else if(!strcasecmp(rSwitchType, "Foreground Alpha") && tArgs == 4)
    {
        rActiveLevel->SetForegroundAlpha(lua_tointeger(L, 2), lua_tonumber(L, 3), lua_tointeger(L, 4));
    }
    //--Sets a foreground layer to automatically scroll.
    else if(!strcasecmp(rSwitchType, "Foreground Autoscroll") && tArgs == 4)
    {
        rActiveLevel->SetForegroundAutoscroll(lua_tointeger(L, 2), lua_tonumber(L, 3), lua_tonumber(L, 4));
    }
    //--Sets the scaling factor of the foreground layer. Higher is bigger, 1:1 is 3.0f.
    else if(!strcasecmp(rSwitchType, "Foreground Scale") && tArgs == 3)
    {
        rActiveLevel->SetForegroundScaler(lua_tointeger(L, 2), lua_tonumber(L, 3));
    }
    //--[Backgrounds/Underlays]
    //--How many background layers there are. Default is 0.
    else if(!strcasecmp(rSwitchType, "Allocate Backgrounds") && tArgs == 2)
    {
        rActiveLevel->AllocateBackgroundPacks(lua_tointeger(L, 2));
    }
    //--Sets which image is used for the foreground. Pass "Null" to disable.
    else if(!strcasecmp(rSwitchType, "Background Image") && tArgs == 3)
    {
        rActiveLevel->SetBackgroundImage(lua_tointeger(L, 2), lua_tostring(L, 3));
    }
    //--Sets the offsets when rendering the foreground image. Scaler can be negative.
    else if(!strcasecmp(rSwitchType, "Background Render Offsets") && tArgs == 6)
    {
        rActiveLevel->SetBackgroundOffsets(lua_tointeger(L, 2), lua_tonumber(L, 3), lua_tonumber(L, 4), lua_tonumber(L, 5), lua_tonumber(L, 6));
    }
    //--Changes the alpha of the foreground over time to the desired value. Pass 1 or lower to set it instantly.
    else if(!strcasecmp(rSwitchType, "Background Alpha") && tArgs == 4)
    {
        rActiveLevel->SetBackgroundAlpha(lua_tointeger(L, 2), lua_tonumber(L, 3), lua_tointeger(L, 4));
    }
    //--Sets a foreground layer to automatically scroll.
    else if(!strcasecmp(rSwitchType, "Background Autoscroll") && tArgs == 4)
    {
        rActiveLevel->SetBackgroundAutoscroll(lua_tointeger(L, 2), lua_tonumber(L, 3), lua_tonumber(L, 4));
    }
    //--Sets the scaling factor of the foreground layer. Higher is bigger, 1:1 is 3.0f.
    else if(!strcasecmp(rSwitchType, "Background Scale") && tArgs == 3)
    {
        rActiveLevel->SetBackgroundScaler(lua_tointeger(L, 2), lua_tonumber(L, 3));
    }
    //--[Objectives]
    //--Registers a new objective.
    else if(!strcasecmp(rSwitchType, "Register Objective") && tArgs == 2)
    {
        rActiveLevel->RegisterObjective(lua_tostring(L, 2));
    }
    //--Flags the named objective as incomplete.
    else if(!strcasecmp(rSwitchType, "Flag Objective False") && tArgs == 2)
    {
        rActiveLevel->FlagObjectiveIncomplete(lua_tostring(L, 2));
    }
    //--Flags the named objective as complete.
    else if(!strcasecmp(rSwitchType, "Flag Objective True") && tArgs == 2)
    {
        rActiveLevel->FlagObjectiveComplete(lua_tostring(L, 2));
    }
    //--Clears objectives.
    else if(!strcasecmp(rSwitchType, "Clear Objectives") && tArgs == 1)
    {
        rActiveLevel->ClearObjectives();
    }
    //--Adds a new rendering dislocation.
    else if(!strcasecmp(rSwitchType, "Add Dislocation") && tArgs == 9)
    {
        rActiveLevel->RegisterDislocationPack(lua_tostring(L, 2), lua_tostring(L, 3), lua_tointeger(L, 4), lua_tointeger(L, 5), lua_tointeger(L, 6), lua_tointeger(L, 7), lua_tonumber(L, 8), lua_tonumber(L, 9));
    }
    //--Modifies a rendering dislocation.
    else if(!strcasecmp(rSwitchType, "Modify Dislocation") && tArgs == 4)
    {
        rActiveLevel->ModifyDislocationPack(lua_tostring(L, 2), lua_tonumber(L, 3), lua_tonumber(L, 4));
    }
    //--Sets a Major Animation case. Pass "Null" to clear.
    else if(!strcasecmp(rSwitchType, "Major Animation") && tArgs == 2)
    {
        const char *rString = lua_tostring(L, 2);
        if(!strcasecmp(rString, "Null"))
        {
            rActiveLevel->SetMajorAnimationMode(NULL);
        }
        else
        {
            rActiveLevel->SetMajorAnimationMode(rString);
        }
    }
    //--Adds a new animation which can be activated and set to play.
    else if(!strcasecmp(rSwitchType, "Add Animation") && tArgs == 5)
    {
        rActiveLevel->AddAnimation(lua_tostring(L, 2), lua_tonumber(L, 3), lua_tonumber(L, 4), lua_tonumber(L, 5));
    }
    else if(!strcasecmp(rSwitchType, "Set Animation From Pattern") && tArgs == 4)
    {
        rActiveLevel->SetAnimationFromPattern(lua_tostring(L, 2), lua_tostring(L, 3), lua_tointeger(L, 4));
    }
    //--Sets whether or not the named animation is currently rendering.
    else if(!strcasecmp(rSwitchType, "Set Animation Rendering") && tArgs == 3)
    {
        rActiveLevel->SetAnimationRender(lua_tostring(L, 2), lua_toboolean(L, 3));
    }
    //--Sets which frame the animation should run to.
    else if(!strcasecmp(rSwitchType, "Set Animation Destination") && tArgs == 3)
    {
        rActiveLevel->SetAnimationDestinationFrame(lua_tostring(L, 2), lua_tonumber(L, 3));
    }
    //--Sets which frame the animation should run to, and how fast. Default is 1.0.
    else if(!strcasecmp(rSwitchType, "Set Animation Destination") && tArgs == 4)
    {
        rActiveLevel->SetAnimationDestinationFrame(lua_tostring(L, 2), lua_tonumber(L, 3), lua_tonumber(L, 4));
    }
    //--Sets a destination frame start and end. The animation will oscillate between the two using logic specified in the string.
    //  Valid examples are "Sin" (sinusoidal), "Loop" (1, 2, 3, 1, 2, 3), and "Reverse" (1, 2, 3, 2, 1, 2, 3, 2, 1)
    else if(!strcasecmp(rSwitchType, "Set Animation Loop") && tArgs == 7)
    {
        rActiveLevel->SetAnimationLoop(lua_tostring(L, 2), lua_tonumber(L, 3), lua_tonumber(L, 4), lua_tostring(L, 5), lua_tonumber(L, 6), lua_tonumber(L, 7));
    }
    //--Sets the frame the animation is currently on. Overrides the old value.
    else if(!strcasecmp(rSwitchType, "Set Animation Frame") && tArgs == 3)
    {
        rActiveLevel->SetAnimationCurrentFrame(lua_tostring(L, 2), lua_tonumber(L, 3));
    }
    //--Resets all actors in the level to their position pack graphics, assuming they used those.
    else if(!strcasecmp(rSwitchType, "Reset Actor Graphics") && tArgs == 1)
    {
        rActiveLevel->ResetActorGraphics();
    }
    //--Sets the current number of ticks to shake the screen.
    else if(!strcasecmp(rSwitchType, "Screen Shake") && tArgs == 2)
    {
        rActiveLevel->SetScreenShake(lua_tointeger(L, 2));
    }
    //--Sets how often the screen shakes, automatically. Set to zero periodicity to disable.
    else if(!strcasecmp(rSwitchType, "Screen Shake Periodicity") && tArgs == 4)
    {
        rActiveLevel->SetScreenShakePeriodicity(lua_tointeger(L, 2), lua_tointeger(L, 3), lua_tointeger(L, 4));
    }
    //--Sets a sound effect to play when the screen shakes due to periodicity rolls.
    else if(!strcasecmp(rSwitchType, "Add Shake Sound") && tArgs == 2)
    {
        rActiveLevel->AddShakeSoundEffect(lua_tostring(L, 2));
    }
    //--Sets the chance for a falling rock to spawn each tick. Set to 0 to disable.
    else if(!strcasecmp(rSwitchType, "Rock Fall Chance") && tArgs == 2)
    {
        rActiveLevel->SetRockFallChance(lua_tointeger(L, 2));
    }
    //--Adds a rock image for falling rocks. Rocks are selected at random.
    else if(!strcasecmp(rSwitchType, "Add Rock Fall Image") && tArgs == 2)
    {
        rActiveLevel->AddRockFallImage(lua_tostring(L, 2));
    }
    //--Hides all enemy viewcones. Often used for cutscenes.
    else if(!strcasecmp(rSwitchType, "Hide All Viewcones") && tArgs == 2)
    {
        rActiveLevel->SetHideViewcones(lua_toboolean(L, 2));
    }
    //--Registers a new staircase. Fails if a duplicate is found.
    else if(!strcasecmp(rSwitchType, "Create Staircase") && tArgs == 9)
    {
        rActiveLevel->RegisterStaircase(lua_tostring(L, 2), lua_tonumber(L, 3), lua_tonumber(L, 4), lua_tonumber(L, 5), lua_tonumber(L, 6), lua_tostring(L, 7), lua_tostring(L, 8), lua_tostring(L, 9));
    }
    //--Starts the running minigame. Run! Run!
    else if(!strcasecmp(rSwitchType, "Activate Running Minigame") && tArgs == 1)
    {
        RunningMinigame *nMinigame = new RunningMinigame();
        nMinigame->Construct();
        nMinigame->AssemblePlayerImages();
        nMinigame->GenerateLevel();
        nMinigame->Activate();
        nMinigame->SetHoldingPattern(true);
        rActiveLevel->ReceiveRunningMinigame(nMinigame);
    }
    //--Start the KPop Minigame.
    else if(!strcasecmp(rSwitchType, "Activate KPop Minigame") && tArgs == 1)
    {
        KPopDanceGame *nMinigame = new KPopDanceGame();
        rActiveLevel->ReceiveKPopMinigame(nMinigame);
    }
    //--Error.
    else
    {
        LuaPropertyError("AL_SetProperty", rSwitchType, tArgs);
    }

    //--Finish up.
    return 0;
}
int Hook_AL_BeginTransitionTo(lua_State *L)
{
    //AL_BeginTransitionTo(sMapName, sTransitionPostExitScript)
    int tArgs = lua_gettop(L);
    if(tArgs < 2) return LuaArgError("AL_BeginTransitionTo");

    //--Active object.
    AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
    if(!rActiveLevel) return LuaTypeError("AL_BeginTransitionTo");

    //--If the level name is "LASTSAVEINSTANT" then reposition the player to the save point.
    if(!strcasecmp("LASTSAVEINSTANT", lua_tostring(L, 1)))
    {
        TilemapActor *rPlayerActor = rActiveLevel->LocatePlayerActor();
        if(rPlayerActor)
        {
            rActiveLevel->RepositionActorToExit("SavePoint", TA_DIR_SOUTH, 1.0f, rPlayerActor);
        }
        return 0;
    }
    //--If the level name is "RANDOMLEVEL" then the program will use the RLG.
    else
    {

    }

    //--Set.
    rActiveLevel->SetTransitionDestination(lua_tostring(L, 1), "PlayerStart");
    rActiveLevel->SetTransitionPostExec(lua_tostring(L, 2));
    return 0;
}
int Hook_AL_RemoveObject(lua_State *L)
{
    //AL_RemoveObject("Chest", sChestName)
    //AL_RemoveObject("To Fake Chest", sChestName)
    //AL_RemoveObject("Door", sDoorName)
    //AL_RemoveObject("Exit", sExitName)
    //AL_RemoveObject("Enemy", sEnemyName)
    int tArgs = lua_gettop(L);
    if(tArgs < 2) return LuaArgError("AL_RemoveObject");

    //--Active object.
    AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
    if(!rActiveLevel) return LuaTypeError("AL_RemoveObject");

    //--Set.
    rActiveLevel->RemoveObject(lua_tostring(L, 1), lua_tostring(L, 2));
    return 0;
}
int Hook_AL_PulseIgnore(lua_State *L)
{
    //AL_PulseIgnore()
    //AL_PulseIgnore(sString)
    int tArgs = lua_gettop(L);

    //--Active object.
    AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
    if(!rActiveLevel) return 0; //Fails silently, can be used without a map existing.

    //--No args, call with null.
    if(tArgs < 1)
    {
        rActiveLevel->PulseIgnore(NULL);
    }
    //--Use the 1st string.
    else
    {
        rActiveLevel->PulseIgnore(lua_tostring(L, 1));
    }
    return 0;
}
int Hook_AL_CreateObject(lua_State *L)
{
    //AL_CreateObject("Examinable", sExaminableString, iLft, iTop, iWid, iHei)
    //AL_CreateObject("Path Node", sName, iX, iY, iW, iH)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("AL_CreateObject");

    //--Active object.
    AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
    if(!rActiveLevel) return LuaTypeError("AL_CreateObject");

    //--Switching.
    const char *rSwitchType = lua_tostring(L, 1);

    //--Examinable object.
    if(!strcasecmp("Examinable", rSwitchType) && tArgs == 6)
    {
        rActiveLevel->AddExaminable(lua_tointeger(L, 3), lua_tointeger(L, 4), lua_tointeger(L, 5), lua_tointeger(L, 6), 0, lua_tostring(L, 2));
    }
    //--Path Node.
    else if(!strcasecmp("Path Node", rSwitchType) && tArgs == 6)
    {
        rActiveLevel->AddPatrolNode(lua_tostring(L, 2), lua_tointeger(L, 3), lua_tointeger(L, 4), lua_tointeger(L, 5), lua_tointeger(L, 6));
    }
    //--Error.
    else
    {
        LuaPropertyError("AL_SetProperty", rSwitchType, tArgs);
    }

    //--Finish up.
    return 0;
}
