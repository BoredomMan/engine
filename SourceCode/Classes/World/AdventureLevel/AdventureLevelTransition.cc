//--Base
#include "AdventureLevel.h"

//--Classes
#include "AdventureLevelGenerator.h"
#include "TileLayer.h"
#include "TilemapActor.h"

//--CoreClasses
#include "LoadInterrupt.h"
#include "SugarFileSystem.h"
#include "SugarLinkedList.h"

//--Definitions
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "ControlManager.h"
#include "DebugManager.h"
#include "EntityManager.h"
#include "LuaManager.h"
#include "MapManager.h"

//--[Local Definitions]
#define TRANSITION_HOLD_TICKS 10
#define TRANSITION_FADE_TICKS 15
#define PULSE_TICKS 90

//#define AL_TRANS_DEBUG
#ifdef AL_TRANS_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

bool AdventureLevel::HandleTransition()
{
    //--Causes the program to change the active level. This is done by creating a new AdventureLevel and
    //  replacing the one in the MapManager, if necessary. Entities will be copied across. This object
    //  will inherently become unstable if this function returns true, so the caller should
    //  immediately return and cease using the calling class.
    //--Returns false if no transitions were pending. A transition occurs if the map destination is
    //  set, the exit destination is optional.
    if(!mTransitionDestinationMap) return false;

    //--Flag.
    mIsTransitionOut = true;

    //--Transition hold timer.
    if(mTransitionHoldTimer == -1)
    {
        mTransitionHoldTimer = TRANSITION_HOLD;
        return true;
    }
    //--Transition is counting down.
    else if(mTransitionHoldTimer > 0)
    {
        mTransitionHoldTimer --;
        return true;
    }

    //--If the transition is "LASTSAVE" then replace it with the last save instance.
    bool tIsSaveCase = false;
    if(!strcasecmp(mTransitionDestinationMap, "LASTSAVE") && xLastSavePoint)
    {
        tIsSaveCase = true;
        ResetString(mTransitionDestinationMap, xLastSavePoint);
    }
    //--If the transition is "RANDOMLEVEL" then we need to use the random level generator.
    else if(!strcasecmp(mTransitionDestinationMap, "RANDOMLEVEL"))
    {
        HandleTransitionToRandom();
        return true;
    }

    //--Search the remappings. If no remapping is found, use the default path.
    const char *rUsePathing = mTransitionDestinationMap;
    for(int i = 0; i < xRemappingsTotal; i ++)
    {
        if(!strcasecmp(xRemappingsCheck[i], mTransitionDestinationMap))
        {
            rUsePathing = xRemappingsResult[i];
            break;
        }
    }

    //--Check if the associated constructor and map data exist. If they don't, spit a warning and fail here.
    char tSLFBuffer[256];
    char tConstructorBuffer[256];
    const char *rAdventurePath = DataLibrary::GetGamePath("Root/Paths/System/Startup/sAdventurePath");
    sprintf(tSLFBuffer,         "%s/Maps/%s/MapData.slf",     rAdventurePath, rUsePathing);
    sprintf(tConstructorBuffer, "%s/Maps/%s/Constructor.lua", rAdventurePath, rUsePathing);

    bool tMapDataExists = SugarFileSystem::FileExists(tSLFBuffer);
    bool tConstructorExists = SugarFileSystem::FileExists(tConstructorBuffer);
    if(!tConstructorExists || !tMapDataExists)
    {
        DebugManager::ForcePrint("AdventureLevel:HandleTransition - Error finding map data. %s - %i %i\n", rUsePathing, tConstructorExists, tMapDataExists);
        ResetString(mTransitionDestinationMap, NULL);
        ResetString(mTransitionDestinationExit, NULL);
        return false;
    }

    //--Liberate this object from the MapManager so it doesn't get deleted upon replacement.
    RootLevel *rThisObjectPtr = MapManager::Fetch()->LiberateLevel();

    //--Purge this section of the DataLibrary each time the level loads.
    DataLibrary::Fetch()->Purge("Root/Images/TempImg/");

    //--Locate the player actor. This one needs to be preserved.
    EntityManager *rEntityManager = EntityManager::Fetch();
    TilemapActor *rPlayerActor = LocatePlayerActor();
    if(rPlayerActor) rEntityManager->LiberatePointerP(rPlayerActor);

    //--Storage for all entities following the player. There is a variable number, and the scripts can have as many
    //  as is necessary. Therefore, we need a linked list. Note that the elements are added in reverse order, so they
    //  will be re-added in the correct order later on.
    int i = 0;
    SugarLinkedList *tFollowingList = new SugarLinkedList(false);
    TilemapActor *rFollowingActorX = LocateFollowingActor(i);
    while(rFollowingActorX)
    {
        //--Add this actor.
        tFollowingList->AddElementAsTail("X", rFollowingActorX);

        //--Liberate it from the EntityManager.
        rEntityManager->LiberatePointerP(rFollowingActorX);

        //--Next.
        i ++;
        rFollowingActorX = LocateFollowingActor(i);
    }

    //--Entity Manager now wipes out entities.
    rEntityManager->ClearAll();

    //--Call the constructor script. Constructors should create a new level and pass it to the MapManager.
    char tBuffer[256];
    sprintf(tBuffer, "%s/Maps/%s/Constructor.lua", rAdventurePath, rUsePathing);
    bool tOldFlag = LoadInterrupt::Fetch()->mIsSuppressed;
    LoadInterrupt::Fetch()->mIsSuppressed = true;
    LuaManager::Fetch()->ExecuteLuaFile(tBuffer, 1, "N", 0.0f);
    LoadInterrupt::Fetch()->mIsSuppressed = tOldFlag;

    //--If the player actor was preserved, register it here.
    if(rPlayerActor)
    {
        //--[Base]
        //--Now we need to reposition the entity. Get the newly loaded level.
        AdventureLevel *rNewLevel = AdventureLevel::Fetch();
        if(!rNewLevel) return true;

        //--[Player's Party]
        //--Go through the following actors and re-add them as followers. They will always be in the same
        //  position as the player actor. This must be done before the party leader so they will
        //  render in the correct order in the event of ties.
        TilemapActor *rFollowingActorX = (TilemapActor *)tFollowingList->PushIterator();
        while(rFollowingActorX)
        {
            //--Re-register.
            rEntityManager->RegisterPointer(rFollowingActorX->GetName(), rFollowingActorX, &RootObject::DeleteThis);

            //--Add this entity's ID to the following list.
            rNewLevel->AddFollowEntityID(rFollowingActorX->GetID());

            //--Next.
            rFollowingActorX = (TilemapActor *)tFollowingList->AutoIterate();
        }

        //--[Player]
        //--Once the party is re-registered, register the player herself.
        rEntityManager->RegisterPointer(rPlayerActor->GetName(), rPlayerActor, &RootObject::DeleteThis);

        //--Normal case: Reposition to exit. Order that level to reposition the player's actor using its internal function.
        if(!tIsSaveCase)
        {
            rNewLevel->RepositionActorToExit(mTransitionDestinationExit, mTransitionDirection, mTransitionPercentage, rPlayerActor);
        }
        //--Save case. Reposition the Actor to the location of the zeroth save, but up 1 tile.
        else
        {
            rNewLevel->RepositionActorToExit("Savepoint", TA_DIR_SOUTH, 1.0f, rPlayerActor);
            rPlayerActor->SetCollisionDepth(0);
        }

        //--Store the player's position.
        float tPlayerX = rPlayerActor->GetWorldX();
        float tPlayerY = rPlayerActor->GetWorldY();
        int tPlayerFacing = rPlayerActor->GetFacing();

        //--Take control of the entity.
        rNewLevel->SetControlEntityID(rPlayerActor->GetID());

        //--[Reposition the Party]
        //--Get the entity's position and set all the follower positions as that. This will cause the followers to wait
        //  before they start moving.
        for(int i = 0; i < PLAYER_MOVE_STORAGE_TOTAL; i ++)
        {
            rNewLevel->mPlayerXPositions[i] = tPlayerX;
            rNewLevel->mPlayerYPositions[i] = tPlayerY;
            rNewLevel->mPlayerFacings[i] = tPlayerFacing;
            rNewLevel->mPlayerTimers[i] = 0;
            rNewLevel->mPlayerDepths[i] = 0;
            rNewLevel->mPlayerRunnings[i] = false;
        }

        //--Order all the party members to position to the player immediately.
        rFollowingActorX = (TilemapActor *)tFollowingList->PushIterator();
        while(rFollowingActorX)
        {
            //--Position.
            rFollowingActorX->SetPositionByPixel(tPlayerX, tPlayerY);
            rFollowingActorX->SetFacing(tPlayerFacing);

            //--Next.
            rFollowingActorX = (TilemapActor *)tFollowingList->AutoIterate();
        }

        //--Pass along light properties.
        if(mPlayerHasLight)
        {
            rNewLevel->ActivatePlayerLight();
            rNewLevel->SetPlayerLightPower(mPlayerLightPower);
            rNewLevel->SetPlayerLightPowerMax(mPlayerLightPowerMax);
        }
    }

    //--If we have a post-exec script, execute it.
    if(mTransitionPostScript)
    {
        //--Check if this is using the "FORCEPOS:" syntax. If it is, we just drop the player at the specified position and
        //  fold the party. The script is not executed.
        if(!strncasecmp(mTransitionPostScript, "FORCEPOS:", 9) && rPlayerActor)
        {
            //--Buffers.
            char tXBuffer[32];
            char tYBuffer[32];
            char tZBuffer[32];

            //--Store data into the buffers.
            int tPos = 0;
            char *rCurBuf = tXBuffer;
            int tLen = (int)strlen(mTransitionPostScript);
            for(int i = 9; i < tLen; i ++)
            {
                //--Check the letter. 'x' is the delimiter.
                if(mTransitionPostScript[i] == 'x')
                {
                    //--If this is the X buffer...
                    if(rCurBuf == tXBuffer)
                    {
                        rCurBuf = tYBuffer;
                    }
                    //--If this is the Y buffer...
                    else if(rCurBuf == tYBuffer)
                    {
                        rCurBuf = tZBuffer;
                    }
                    //--Z buffer ends the sequence.
                    else if(rCurBuf == tZBuffer)
                    {
                        break;
                    }

                    //--Reset the position counter.
                    tPos = 0;
                }
                //--Otherwise, add it to the current buffer.
                else
                {
                    //--Set.
                    rCurBuf[tPos+0] = mTransitionPostScript[i];
                    rCurBuf[tPos+1] = '\0';

                    //--Increment.
                    tPos ++;
                }
            }

            //--Scanner.
            float tXPosition = atof(tXBuffer);
            float tYPosition = atof(tYBuffer);
            float tZPosition = atof(tZBuffer);

            //--Reposition the player character.
            rPlayerActor->SetPosition(tXPosition, tYPosition);
            rPlayerActor->SetCollisionDepth(tZPosition);

            //--Store the player's position. The placement function offsets the player automatically.
            float tPlayerX = rPlayerActor->GetWorldX();
            float tPlayerY = rPlayerActor->GetWorldY();

            //--Now reposition the party to this position.
            AdventureLevel *rNewLevel = AdventureLevel::Fetch();
            if(rNewLevel)
            {
                for(int i = 0; i < PLAYER_MOVE_STORAGE_TOTAL; i ++)
                {
                    rNewLevel->mPlayerXPositions[i] = tPlayerX;
                    rNewLevel->mPlayerYPositions[i] = tPlayerY;
                    rNewLevel->mPlayerTimers[i] = 0;
                    rNewLevel->mPlayerDepths[i] = tZPosition;
                    rNewLevel->mPlayerRunnings[i] = false;
                }
            }

            //--Order all the party members to position to the player immediately.
            rFollowingActorX = (TilemapActor *)tFollowingList->PushIterator();
            while(rFollowingActorX)
            {
                //--Position.
                rFollowingActorX->SetPositionByPixel(tPlayerX, tPlayerY);
                rFollowingActorX->SetCollisionDepth(tZPosition);

                //--Recompute the depth.
                float tZPosition = DEPTH_MIDGROUND + ((tPlayerY) / TileLayer::cxSizePerTile * DEPTH_PER_TILE);
                tZPosition = tZPosition + (DEPTH_PER_TILE * (float)tZPosition * 2.0f);
                rFollowingActorX->SetOverrideDepth(tZPosition);

                //--Next.
                rFollowingActorX = (TilemapActor *)tFollowingList->AutoIterate();
            }
        }
        //--The "ENTPOS:" tag indicates to use a given entity position. The format is:
        //  ENTPOS:[entity name]:[direction]
        //  [entity name] is the name of the entity as it appears in the level.
        //  [direction] is a single letter, N S E W, indicating where the player should spawn relative to that entity.
        else if(!strncasecmp(mTransitionPostScript, "ENTPOS:", 7) && rPlayerActor)
        {
            //--Storage Buffers
            char tEntityName[STD_MAX_LETTERS];
            int tEntityDirFlag = -1;

            //--Parse out the name.
            int tPos = 0;
            int tLetterCurrent = 7;
            int tLen = (int)strlen(mTransitionPostScript);
            for(int i = 7; i < tLen; i ++)
            {
                //--Store what letter we're parsing.
                tLetterCurrent = i;

                //--Check the letter. ':' is the delimiter.
                if(mTransitionPostScript[i] == ':')
                {
                    break;
                }
                //--Otherwise, add it to the current buffer.
                else
                {
                    //--Set.
                    tEntityName[tPos+0] = mTransitionPostScript[i];
                    tEntityName[tPos+1] = '\0';

                    //--Increment.
                    tPos ++;
                }
            }

            //--Get the direction. Skip the delimiter.
            tLetterCurrent ++;
            if(mTransitionPostScript[tLetterCurrent] == 'N' || mTransitionPostScript[tLetterCurrent] == 'U')
            {
                tEntityDirFlag = DIR_UP;
            }
            else if(mTransitionPostScript[tLetterCurrent] == 'W' || mTransitionPostScript[tLetterCurrent] == 'L')
            {
                tEntityDirFlag = DIR_LEFT;
            }
            else if(mTransitionPostScript[tLetterCurrent] == 'E' || mTransitionPostScript[tLetterCurrent] == 'R')
            {
                tEntityDirFlag = DIR_RIGHT;
            }
            else
            {
                tEntityDirFlag = DIR_DOWN;
            }

            //--Next, locate the entity in question. Get the center position and add one tile in the needed direction.
            int tLft, tTop, tRgt, tBot;
            AdventureLevel *rNewLevel = AdventureLevel::Fetch();
            if(rNewLevel) rNewLevel->GetExaminablePos(tEntityName, tLft, tTop, tRgt, tBot);
            if(tLft == -1 && tTop == -1) fprintf(stderr, "Unable to locate examinable %s\n", tEntityName);
            float tXPos = (tLft + tRgt) / 2.0f;
            float tYPos = (tTop + tBot) / 2.0f;
            if(tEntityDirFlag == DIR_UP)
            {
                tYPos = tTop - (TileLayer::cxSizePerTile * 0.50f);
            }
            else if(tEntityDirFlag == DIR_LEFT)
            {
                tXPos = tLft - (TileLayer::cxSizePerTile * 0.50f);
            }
            else if(tEntityDirFlag == DIR_RIGHT)
            {
                tXPos = tRgt + (TileLayer::cxSizePerTile * 0.50f);
            }
            else
            {
                tYPos = tBot + (TileLayer::cxSizePerTile * 0.50f);
            }

            //--Change it to tile coordinates.
            tXPos = tXPos / (float)TileLayer::cxSizePerTile;
            tYPos = tYPos / (float)TileLayer::cxSizePerTile;

            //--Offset the .25/.50 values the player entity uses to center.
            tXPos = tXPos - (0.50f);
            tYPos = tYPos - (0.50f);

            //--Reposition the player character.
            rPlayerActor->SetPosition(tXPos, tYPos);
            rPlayerActor->SetCollisionDepth(0);
            rPlayerActor->SetFacing(tEntityDirFlag);

            //--Store the player's position. The placement function offsets the player automatically.
            float tPlayerX = rPlayerActor->GetWorldX();
            float tPlayerY = rPlayerActor->GetWorldY();

            //--Now reposition the party to this position.
            if(rNewLevel)
            {
                for(int i = 0; i < PLAYER_MOVE_STORAGE_TOTAL; i ++)
                {
                    rNewLevel->mPlayerXPositions[i] = tPlayerX;
                    rNewLevel->mPlayerYPositions[i] = tPlayerY;
                    rNewLevel->mPlayerTimers[i] = 0;
                    rNewLevel->mPlayerDepths[i] = 0;
                    rNewLevel->mPlayerRunnings[i] = false;
                    rNewLevel->mPlayerFacings[i] = tEntityDirFlag;
                }
            }

            //--Order all the party members to position to the player immediately.
            rFollowingActorX = (TilemapActor *)tFollowingList->PushIterator();
            while(rFollowingActorX)
            {
                //--Position.
                rFollowingActorX->SetPositionByPixel(tPlayerX, tPlayerY);
                rFollowingActorX->SetCollisionDepth(0);
                rFollowingActorX->SetFacing(tEntityDirFlag);

                //--Recompute the depth.
                float tZPosition = DEPTH_MIDGROUND + ((tPlayerY) / TileLayer::cxSizePerTile * DEPTH_PER_TILE);
                tZPosition = tZPosition + (DEPTH_PER_TILE * (float)tZPosition * 2.0f);
                rFollowingActorX->SetOverrideDepth(tZPosition);

                //--Next.
                rFollowingActorX = (TilemapActor *)tFollowingList->AutoIterate();
            }
        }
        //--Otherwise, treat it as a script.
        else
        {
            LuaManager::Fetch()->ExecuteLuaFile(mTransitionPostScript);
        }
    }

    //--Second-half of the constructor. Usually used for deleting objects if the script calls for them to be deleted.
    //  This is done after the player is positioned since the exit they were using could be deleted.
    tOldFlag = LoadInterrupt::Fetch()->mIsSuppressed;
    LoadInterrupt::Fetch()->mIsSuppressed = true;
    LuaManager::Fetch()->ExecuteLuaFile(tBuffer, 1, "N", 1.0f);
    LoadInterrupt::Fetch()->mIsSuppressed = tOldFlag;

    //--Add this object to the garbage heap. It is now unstable and could be deleted at any time.
    Memory::AddGarbage(rThisObjectPtr, &RootObject::DeleteThis);

    //--Clear the following list.
    delete tFollowingList;

    //--Reset the transition hold timer.
    mTransitionHoldTimer = -1;

    //--Transition is pending now.
    return true;
}
void AdventureLevel::HandleTransitionToRandom()
{
    //--Handles transition to a randomly generated level.

    //--Liberate this object from the MapManager so it doesn't get deleted upon replacement.
    RootLevel *rThisObjectPtr = MapManager::Fetch()->LiberateLevel();
    //fprintf(stderr, "Liberated this object.\n");

    //--Locate the player actor. This one needs to be preserved.
    EntityManager *rEntityManager = EntityManager::Fetch();
    TilemapActor *rPlayerActor = LocatePlayerActor();
    if(rPlayerActor) rEntityManager->LiberatePointerP(rPlayerActor);
    //fprintf(stderr, "Preserved player actor.\n");

    //--Storage for all entities following the player. There is a variable number, and the scripts can have as many
    //  as is necessary. Therefore, we need a linked list. Note that the elements are added in reverse order, so they
    //  will be re-added in the correct order later on.
    int i = 0;
    SugarLinkedList *tFollowingList = new SugarLinkedList(false);
    TilemapActor *rFollowingActorX = LocateFollowingActor(i);
    while(rFollowingActorX)
    {
        //--Add this actor.
        tFollowingList->AddElementAsTail("X", rFollowingActorX);

        //--Liberate it from the EntityManager.
        rEntityManager->LiberatePointerP(rFollowingActorX);

        //--Next.
        i ++;
        rFollowingActorX = LocateFollowingActor(i);
    }
    //fprintf(stderr, "Preserved party\n");

    //--Entity Manager now wipes out entities.
    rEntityManager->ClearAll();
    //fprintf(stderr, "Cleared entities.\n");

    //--Call the random level generation script.
    char tPathBuffer[256];
    const char *rAdventurePath = DataLibrary::GetGamePath("Root/Paths/System/Startup/sAdventurePath");
    sprintf(tPathBuffer, "%s/System/901 Random Level Generator.lua", rAdventurePath);
    bool tOldFlag = LoadInterrupt::Fetch()->mIsSuppressed;
    LoadInterrupt::Fetch()->mIsSuppressed = true;
    LuaManager::Fetch()->ExecuteLuaFile(tPathBuffer);
    LoadInterrupt::Fetch()->mIsSuppressed = tOldFlag;
    //fprintf(stderr, "Built random level generator.\n");

    //--We expect that the MapManager has a random level generator in it now.
    AdventureLevelGenerator *rActiveGenerator = (AdventureLevelGenerator *)MapManager::Fetch()->GetActiveLevel();
    if(!rActiveGenerator) return;
    //fprintf(stderr, "Checked random level generator.\n");

    //--Order the generator to perform the generation steps.
    rActiveGenerator->GenerateSizes();
    rActiveGenerator->GenerateBaseLayout();
    rActiveGenerator->GenerateTunnels();
    rActiveGenerator->PlaceExits();
    rActiveGenerator->PlaceTreasure();
    rActiveGenerator->PlaceEnemies();
    rActiveGenerator->BuildTextures();
    //fprintf(stderr, "Ran RLG to end.\n");
    AdventureLevel *nNewLevel = rActiveGenerator->ExportToAdventureLevel();
    //fprintf(stderr, "Exported RLG.\n");
    if(!nNewLevel) return;

    //--Register the level now.
    MapManager::Fetch()->ReceiveLevel(nNewLevel);
    //fprintf(stderr, "Sent new level.\n");

    //--If the player actor was preserved, register it here.
    if(rPlayerActor)
    {
        //--[Base]
        //--Now we need to reposition the entity. Get the newly loaded level.
        AdventureLevel *rNewLevel = AdventureLevel::Fetch();
        if(!rNewLevel) return;

        //--[Player's Party]
        //--Go through the following actors and re-add them as followers. They will always be in the same
        //  position as the player actor. This must be done before the party leader so they will
        //  render in the correct order in the event of ties.
        TilemapActor *rFollowingActorX = (TilemapActor *)tFollowingList->PushIterator();
        while(rFollowingActorX)
        {
            //--Re-register.
            rEntityManager->RegisterPointer(rFollowingActorX->GetName(), rFollowingActorX, &RootObject::DeleteThis);

            //--Add this entity's ID to the following list.
            rNewLevel->AddFollowEntityID(rFollowingActorX->GetID());

            //--Next.
            rFollowingActorX = (TilemapActor *)tFollowingList->AutoIterate();
        }

        //--[Player]
        //--Once the party is re-registered, register the player herself.
        rEntityManager->RegisterPointer(rPlayerActor->GetName(), rPlayerActor, &RootObject::DeleteThis);
        rNewLevel->RepositionActorToExit("Nonexistant", mTransitionDirection, mTransitionPercentage, rPlayerActor);

        //--Store the player's position.
        float tPlayerX = rPlayerActor->GetWorldX();
        float tPlayerY = rPlayerActor->GetWorldY();
        int tPlayerFacing = rPlayerActor->GetFacing();

        //--Take control of the entity.
        rNewLevel->SetControlEntityID(rPlayerActor->GetID());

        //--[Reposition the Party]
        //--Get the entity's position and set all the follower positions as that. This will cause the followers to wait
        //  before they start moving.
        for(int i = 0; i < PLAYER_MOVE_STORAGE_TOTAL; i ++)
        {
            rNewLevel->mPlayerXPositions[i] = tPlayerX;
            rNewLevel->mPlayerYPositions[i] = tPlayerY;
            rNewLevel->mPlayerFacings[i] = tPlayerFacing;
            rNewLevel->mPlayerTimers[i] = 0;
            rNewLevel->mPlayerDepths[i] = 0;
            rNewLevel->mPlayerRunnings[i] = false;
        }

        //--Order all the party members to position to the player immediately.
        rFollowingActorX = (TilemapActor *)tFollowingList->PushIterator();
        while(rFollowingActorX)
        {
            //--Position.
            rFollowingActorX->SetPositionByPixel(tPlayerX, tPlayerY);
            rFollowingActorX->SetFacing(tPlayerFacing);

            //--Next.
            rFollowingActorX = (TilemapActor *)tFollowingList->AutoIterate();
        }

        //--Pass along light properties.
        if(mPlayerHasLight)
        {
            rNewLevel->ActivatePlayerLight();
            rNewLevel->SetPlayerLightPower(mPlayerLightPower);
            rNewLevel->SetPlayerLightPowerMax(mPlayerLightPowerMax);
        }
    }
    //fprintf(stderr, "Replaced player.\n");

    //--Second-half of the constructor. Standard script is used.
    char *tPath = InitializeString("%s/Maps/RegulusMines/RandomLevel/Constructor.lua", xRootPath);
    //fprintf(stderr, "Executing constructors: %s\n", tPath);
    tOldFlag = LoadInterrupt::Fetch()->mIsSuppressed;
    LoadInterrupt::Fetch()->mIsSuppressed = true;
    LuaManager::Fetch()->ExecuteLuaFile(tPath, 1, "N", 0.0f);

    //--Execute the second constructor pass.
    LuaManager::Fetch()->ExecuteLuaFile(tPath, 1, "N", 1.0f);
    LoadInterrupt::Fetch()->mIsSuppressed = tOldFlag;
    free(tPath);

    //--Add this object to the garbage heap. It is now unstable and could be deleted at any time.
    Memory::AddGarbage(rThisObjectPtr, &RootObject::DeleteThis);

    //--Clear the following list.
    delete tFollowingList;

    //--Reset the transition hold timer.
    mTransitionHoldTimer = -1;

    //--Transition is pending now.
    //fprintf(stderr, "Done.\n");
    return;
}
void AdventureLevel::RepositionActorToExit(const char *pExitName, int pDirection, float pPercentage, TilemapActor *pActor)
{
    //--Called after a level transition, repositions the TilemapActor in question to the named exit, offset
    //  to account for the position of the exit.
    if(!pExitName || !pActor) return;

    //--If the Exit name is "Savepoint", then get the 0th savepoint and reposition there.
    if(!strcasecmp(pExitName, "Savepoint"))
    {
        //--Get and check. If it doesn't exist, put the player on the playerstart.
        SavePointPackage *rZerothSavePoint = (SavePointPackage *)mSavePointList->GetElementBySlot(0);
        if(!rZerothSavePoint)
        {
            pActor->SetPosition(mPlayerStartX, mPlayerStartY);
            return;
        }

        //--Save point exists, go there.
        RepositionParty(pActor, rZerothSavePoint->mX + (0.25f * TileLayer::cxSizePerTile), rZerothSavePoint->mY - (0.5f * TileLayer::cxSizePerTile));
        //pActor->SetPosition(rZerothSavePoint->mX / TileLayer::cxSizePerTile, (rZerothSavePoint->mY / TileLayer::cxSizePerTile) - 1);
        pActor->SetFacing(pDirection);
        pActor->SetCollisionDepth(0);
        return;
    }

    //--Find the exit. If it doesn't exist, put them at the PlayerStart entity's location.
    ExitPackage *rPackage = (ExitPackage *)mExitList->GetElementByName(pExitName);
    if(!rPackage)
    {
        //--Alternate: Check the door listing. If a door exists with this name, it can also act as an exit.
        DoorPackage *rCheckDoorPackage = (DoorPackage *)mDoorList->GetElementByName(pExitName);
        if(rCheckDoorPackage)
        {
            //--Flags.
            mIsPlayerOnStaircase = false;
            mIsExitStaircase = false;

            //--If the player is facing up, they appear 1 tile above the door. Easy.
            if(pActor->GetFacing() == TA_DIR_NORTH || pActor->GetFacing() == TA_DIR_NE || pActor->GetFacing() == TA_DIR_NW)
            {
                pActor->SetPosition(rCheckDoorPackage->mX / TileLayer::cxSizePerTile, (rCheckDoorPackage->mY - 16) / TileLayer::cxSizePerTile);
                pActor->SetCollisionDepth(rCheckDoorPackage->mZ);
            }
            //--Otherwise, 1 tile below the door.
            else
            {
                pActor->SetPosition(rCheckDoorPackage->mX / TileLayer::cxSizePerTile, (rCheckDoorPackage->mY + 16) / TileLayer::cxSizePerTile);
                pActor->SetCollisionDepth(rCheckDoorPackage->mZ);
            }
        }
        //--Neither exit nor door exists. Dump the player at the PlayerStart location.
        else
        {
            mIsPlayerOnStaircase = false;
            mIsExitStaircase = false;
            pActor->SetPosition(mPlayerStartX, mPlayerStartY);
            pActor->SetCollisionDepth(0);
        }

        //--In both cases, finish here.
        return;
    }

    //--Flags.
    mIsPlayerOnStaircase = rPackage->mIsStaircase;
    mIsExitStaircase = rPackage->mIsStaircase;
    pActor->SetCollisionDepth(rPackage->mDepth);

    //--Figure out whether this is a horizontal or vertical exit.
    bool tIsExitHorizontal = false;
    if(rPackage->mW > rPackage->mH) tIsExitHorizontal = true;

    //--Staircase: Position the player on the staircase's middle.
    if(mIsExitStaircase)
    {
        pActor->SetPosition((rPackage->mX + (rPackage->mW * 0.0f)) / TileLayer::cxSizePerTile, (rPackage->mY + (rPackage->mH * 0.0f)) / TileLayer::cxSizePerTile);
    }
    //--Horizontal: The player should wind up north or south by 1 tile depending on the facing direction.
    else if(tIsExitHorizontal)
    {
        //--Compute the X position.
        float tXPos = (rPackage->mX + (rPackage->mW * pPercentage)) / TileLayer::cxSizePerTile;

        //--Subtract 0.25f for centering.
        tXPos = tXPos - 0.25f;

        //--North.
        if(pDirection == TA_DIR_NE || pDirection == TA_DIR_NORTH || pDirection == TA_DIR_NW)
        {
            pActor->SetPosition(tXPos, (rPackage->mY - TileLayer::cxSizePerTile) / TileLayer::cxSizePerTile);
        }
        //--South.
        else
        {
            pActor->SetPosition(tXPos, (rPackage->mY + TileLayer::cxSizePerTile) / TileLayer::cxSizePerTile);
        }
    }
    //--Vertical: The player should wind up west or east by 1 tile depending on the facing direction.
    else
    {
        //--Compute the Y position.
        float tYPos = (rPackage->mY + (rPackage->mH * pPercentage)) / TileLayer::cxSizePerTile;

        //--For centering, reposition the player up slightly.
        tYPos = tYPos - (8.0f  / (float)TileLayer::cxSizePerTile);

        //--West.
        if(pDirection == TA_DIR_NW || pDirection == TA_DIR_WEST || pDirection == TA_DIR_SW)
        {
            pActor->SetPosition((rPackage->mX - TileLayer::cxSizePerTile) / TileLayer::cxSizePerTile, tYPos);
        }
        //--East.
        else
        {
            pActor->SetPosition((rPackage->mX + TileLayer::cxSizePerTile) / TileLayer::cxSizePerTile, tYPos);
        }
    }
}
