//--[Structures]
//--There are a lot of substructures used by AdventureLevels, so they got their own file.

#pragma once

//--[Climbable]
//--Can be either a rope or a ladder.
typedef struct ClimbablePackage
{
    //--Position. Refers to the top of the zone.
    int mX;
    int mY;
    int mW;
    int mH;

    //--State
    bool mIsLadder; //false == rope.
    bool mIsActivated;
    bool mIsAlwaysActivated;

    //--Depths
    int mLowerDepth;
    int mUpperDepth;

    //--Methods
    void Initialize()
    {
        //--Position
        mX = 0;
        mY = 0;
        mW = 1;
        mH = 1;

        //--State
        mIsLadder = false;
        mIsActivated = false;

        //--Depths
        mLowerDepth = 0;
        mUpperDepth = 0;
    }
}ClimbablePackage;

//--[Door]
//--Exactly what it sounds like.
typedef struct DoorPackage
{
    //--Position
    int mX;
    int mY;
    int mZ;
    bool mIsWide;

    //--State
    bool mIsOpened;
    bool mIsScriptLocked;
    char *mExamineScript;
    char *mLevelTransitOnOpen;
    char *mLevelTransitExit;
    bool mUseSpaceSound;
    bool mIsSpaceEW;

    //--Contents. Used for chests only.
    char *mContents;

    //--Rendering
    SugarBitmap *rRenderOpen;
    SugarBitmap *rRenderClosed;

    //--Methods
    void Initialize()
    {
        //--Position
        mX = -100;
        mY = -100;
        mZ = 0;
        mIsWide = false;

        //--Contents
        mContents = NULL;

        //--State
        mIsOpened = false;
        mIsScriptLocked = false;
        mExamineScript = NULL;
        mLevelTransitOnOpen = NULL;
        mLevelTransitExit = NULL;
        mUseSpaceSound = false;
        mIsSpaceEW = false;

        //--Rendering
        rRenderOpen = NULL;
        rRenderClosed = NULL;
    }
    static void DeleteThis(void *pPtr)
    {
        DoorPackage *rPackage = (DoorPackage *)pPtr;
        free(rPackage->mContents);
        free(rPackage->mExamineScript);
        free(rPackage->mLevelTransitOnOpen);
        free(rPackage->mLevelTransitExit);
        free(rPackage);
    }
}DoorPackage;

//--[Examinable]
//--Calls a script with a string instruction to distinguish it.
#define EXAMINE_NOT_FROM_NORTH 1
typedef struct
{
    //--Position
    int mX;
    int mY;
    int mW;
    int mH;

    //--Examination Flags
    uint8_t mFlags;

    //--Automated Transition
    bool mIsAutoTransition;
    char mAutoRoomDest[STD_MAX_LETTERS];
    char mAutoLocationDest[STD_MAX_LETTERS];
    char mAutoSound[STD_MAX_LETTERS];

    //--Methods
    void Initialize()
    {
        //--Position
        mX = -100;
        mY = -100;
        mW = 1;
        mH = 1;

        //--Flags
        mFlags = 0;

        //--Automated Transition
        mIsAutoTransition = false;
        mAutoRoomDest[0] = '\0';
        mAutoLocationDest[0] = '\0';
        mAutoSound[0] = '\0';
    }
}ExaminePackage;

//--[Exit]
//--Travels to a new map.
typedef struct
{
    //--Position
    int mX;
    int mY;
    int mW;
    int mH;
    int mDepth;

    //--Destination Info
    char mMapDestination[STD_MAX_LETTERS];
    char mExitDestination[STD_MAX_LETTERS];

    //--Rendering
    bool mIsStaircase;
    SugarBitmap *rRenderImg;

    //--Methods
    void Initialize()
    {
        mX = -100;
        mY = -100;
        mW = 1;
        mH = 1;
        mDepth = 0;
        mMapDestination[0] = '\0';
        mExitDestination[0] = '\0';
        mIsStaircase = false;
        rRenderImg = NULL;
    }
}ExitPackage;

//--[Inflection Zone]
//--Causes entities to move between depths.
typedef struct
{
    //--Position
    int mX;
    int mY;
    int mW;
    int mH;

    //--Depths
    int mLowerDepth;
    int mUpperDepth;

    //--Methods
    void Initialize()
    {
        //--Position
        mX = -100;
        mY = -100;
        mW = 1;
        mH = 1;
        mLowerDepth = 0;
        mUpperDepth = 0;
    }
}InflectionPackage;

//--[Trigger]
//--These fire when the player steps in them. A script is called with the trigger's name.
typedef struct
{
    //--System
    bool mSelfDestruct;

    //--Position
    TwoDimensionReal mDimensions;

    //--Firing Properties
    char mFiringName[STD_MAX_LETTERS];

    //--Methods
    void Initialize()
    {
        mSelfDestruct = false;
        mDimensions.SetWH(-100.0f, -100.0f, 1.0f, 1.0f);
        strcpy(mFiringName, "Unset");
    }
}TriggerPackage;

//--[Spawn Package]
//--Used to spawn enemy NPCs.
#define PATROLPATH_MAX_LETTERS 2048
typedef struct
{
    //--System
    char mRawName[STD_MAX_LETTERS];
    char mUniqueSpawnName[STD_MAX_LETTERS];
    char mPartyEnemy[STD_MAX_LETTERS];
    char mAppearancePath[STD_MAX_LETTERS];
    char mLoseCutscene[STD_MAX_LETTERS];
    char mIgnoreFlag[256];

    //--Flags.
    bool mIsFast;
    bool mIsRelentless;
    bool mNeverPauses;
    int mToughness;
    bool mDontRefaceFlag;
    char mPatrolPathway[PATROLPATH_MAX_LETTERS];
    char mFollowTarget[STD_MAX_LETTERS];

    //--Position
    TwoDimensionReal mDimensions;
    int mDepth;

    //--Methods
    void Initialize()
    {
        //--System.
        mRawName[0] = '\0';
        mUniqueSpawnName[0] = '\0';
        mPartyEnemy[0] = '\0';
        mAppearancePath[0] = '\0';
        mLoseCutscene[0] = '\0';
        mIgnoreFlag[0] = '\0';

        //--Flags.
        mIsFast = false;
        mIsRelentless = false;
        mNeverPauses = false;
        mToughness = 0;
        mPatrolPathway[0] = '\0';
        mFollowTarget[0] = '\0';
        mDontRefaceFlag = false;

        //--Position.
        mDimensions.SetWH(-100.0f, -100.0f, 1.0f, 1.0f);
        mDepth = 0;
    }
}SpawnPackage;

//--[Save Point]
//--Lets you do a variety of lovely things, saving being the least interesting.
typedef struct
{
    //--System
    bool mIsLit;
    int mTimer;

    //--Position
    int mX;
    int mY;

    //--Properties
    bool mIsSpace;
    bool mIsBench;

    //--Images
    SugarBitmap *rUnlitImg;
    SugarBitmap *rLitImgA;
    SugarBitmap *rLitImgB;

    //--Methods
    void Initialize()
    {
        mIsLit = false;
        mTimer = 0;
        mIsSpace = false;
        mIsBench = false;
        rUnlitImg = NULL;
        rLitImgA = NULL;
        rLitImgB = NULL;
    }
}SavePointPackage;

//--[Position]
//--Used to spawn things, can store useful data. Typically used for NPCs.
typedef struct
{
    //--System
    char mSpawnedActor[128];
    int mX;
    int mY;
    int mZ;
    int mFacing;
    bool mIsEightDirectional;
    bool mIsTwoDirectional;

    //--NPC Properties
    char mDialoguePath[256];
    char mSpritePath[64];
    bool mIsClipped;
    bool mAutoWander;
    bool mAutofireDialogue;

    //--Methods
    void Initialize()
    {
        //--System
        mSpawnedActor[0] = '\0';
        mX = 0;
        mY = 0;
        mZ = 0;
        mFacing = DIR_DOWN;
        mIsEightDirectional = false;
        mIsTwoDirectional = false;

        //--NPC Properties
        mDialoguePath[0] = '\0';
        mSpritePath[0] = '\0';
        mIsClipped = true;
        mAutoWander = false;
        mAutofireDialogue = false;
    }
}PositionPackage;

//--[Overlays]
//--This represents a foreground layer.
typedef struct
{
    //--Image.
    SugarBitmap *rImage;

    //--Scale Factor
    float mScaleFactor;

    //--Autoscroll
    float mAutoscrollX;
    float mAutoscrollY;

    //--Alpha.
    int mAlphaTimer;
    int mAlphaTimerMax;
    float mAlphaCurrent;
    float mAlphaStart;
    float mAlphaEnd;

    //--Offset Computation.
    float mOffsetBaseX;
    float mOffsetBaseY;
    float mScrollFactorX;
    float mScrollFactorY;

    //--Methods
    void Initialize()
    {
        //--Image.
        rImage = NULL;

        //--Scale Factor
        mScaleFactor = 1.0f;

        //--Autoscroll
        mAutoscrollX = 0.0f;
        mAutoscrollY = 0.0f;

        //--Alpha.
        mAlphaTimer = 100;
        mAlphaTimerMax = 100;
        mAlphaCurrent = 0.0f;
        mAlphaStart = 0.0f;
        mAlphaEnd = 0.0f;

        //--Offset Computation.
        mOffsetBaseX = 0.0f;
        mOffsetBaseY = 0.0f;
        mScrollFactorX = 0.0f;
        mScrollFactorY = 0.0f;
    }
}OverlayPackage;

//--[Dislocated Render Package]
//--Causes a section of the level to render in a different location. Used for special effects.
typedef struct
{
    //--Variables.
    char mLayerName[STD_MAX_LETTERS];
    int mOrigXStart;
    int mOrigYStart;
    int mWidth;
    int mHeight;
    float mNewXRender;
    float mNewYRender;

    //--Functions
    void Initialize()
    {
        mLayerName[0] = '\0';
        mOrigXStart = 0;
        mOrigYStart = 0;
        mWidth = 1;
        mHeight = 1;
        mNewXRender = 0.0f;
        mNewYRender = 0.0f;
    }
}DislocatedRenderPackage;

//--[Per-map Animation]
//--Per-map Animation. Uses a tileset to create animations, usually of characters. This means that animations
//  only used in one area don't clog up the main loading sequence and get dropped when the map is out of scope.
//--Images will be tilesets, usually one long filmstrip.
#define PMA_LOOP_NONE 0
#define PMA_LOOP_SINUSOIDAL 1
#define PMA_LOOP_LINEAR 2
#define PMA_LOOP_REVERSE 3
typedef struct PerMapAnimation
{
    //--Variables.
    bool mIsRendering;
    float mRenderDepth;
    float mRenderPosX;
    float mRenderPosY;
    float mTimer;
    float mTicksPerFrame;
    float mDestinationFrame;
    int mTotalFrames;
    SugarBitmap **mrImagePtrs;
    float mSizePerFrameX;
    float mTickRate;

    //--Simple loop handler.
    bool mRebootLoopTimer;
    int8_t mLoopType;
    float mLowFrame;
    float mHiFrame;
    float mInternalLoopTimer;
    float mTicksPerWholeLoop;

    //--Functions
    void Initialize()
    {
        mIsRendering = false;
        mRenderDepth = 0.0f;
        mRenderPosX = 0.0f;
        mRenderPosY = 0.0f;
        mTimer = 0;
        mTicksPerFrame = 5;
        mDestinationFrame = 0;
        mTotalFrames = 0;
        mrImagePtrs = NULL;
        mSizePerFrameX = 10.0f;
        mTickRate = 1.0f;
        mRebootLoopTimer = false;
        mLoopType = PMA_LOOP_NONE;
        mLowFrame = 0.0f;
        mHiFrame = 0.0f;
        mInternalLoopTimer = 0.0f;
        mTicksPerWholeLoop = 10.0f;
    }
    void Allocate(int pTotalFrames)
    {
        mTotalFrames = 0;
        free(mrImagePtrs);
        mrImagePtrs = NULL;
        if(pTotalFrames < 1) return;

        mTotalFrames = pTotalFrames;
        SetMemoryData(__FILE__, __LINE__);
        mrImagePtrs = (SugarBitmap **)starmemoryalloc(sizeof(SugarBitmap *) * mTotalFrames);
        memset(mrImagePtrs, 0, sizeof(SugarBitmap *) * mTotalFrames);
    }
    static void DeleteThis(void *pPtr)
    {
        PerMapAnimation *rPtr = (PerMapAnimation *)pPtr;
        free(rPtr->mrImagePtrs);
        free(rPtr);
    }

    //--Ticks towards the requested value. Returns true if the target was reached, false otherwise.
    bool TickTowardsValue(float pTarget, float pTickRate)
    {
        if(mTimer < pTarget - pTickRate)
        {
            mTimer = mTimer + pTickRate;
            return false;
        }
        else if(mTimer > pTarget + pTickRate)
        {
            mTimer = mTimer - pTickRate;
            return false;
        }
        else
        {
            mTimer = pTarget;
            return true;
        }
    }

    //--Logic tick for the animation.
    void Tick()
    {
        //--Normal ticking when not looping.
        if(mLoopType == PMA_LOOP_NONE)
        {
            TickTowardsValue(mDestinationFrame * mTicksPerFrame, mTickRate);
        }
        //--When doing a sinusoidal loop:
        else if(mLoopType == PMA_LOOP_SINUSOIDAL)
        {
            //--If we haven't booted the loop yet, run to the middle target.
            if(mRebootLoopTimer)
            {
                float tMidframe = (mLowFrame + mHiFrame) / 2.0f;
                bool tHitResult = TickTowardsValue(tMidframe * mTicksPerFrame, mTickRate);
                if(tHitResult) mRebootLoopTimer = false;
                mInternalLoopTimer = 0.0f;
            }
            //--Loop is booted. Start running the internal timer.
            else
            {
                //--Timer handling.
                mInternalLoopTimer = mInternalLoopTimer + 1.0f;

                //--Computations.
                float tMidframe = (mLowFrame + mHiFrame) / 2.0f;
                float tAmplitude = mHiFrame - tMidframe;
                float tTargetFrame = tMidframe + (sinf(mInternalLoopTimer * 3.1415926f / mTicksPerWholeLoop) * tAmplitude);

                //--Override the timer.
                mTimer = tTargetFrame * mTicksPerFrame;
            }
        }
        //--When doing a linear loop:
        else if(mLoopType == PMA_LOOP_LINEAR)
        {
            //--If we haven't booted the loop yet, run to the end target.
            if(mRebootLoopTimer)
            {
                bool tHitResult = TickTowardsValue(mHiFrame * mTicksPerFrame, mTickRate);
                if(tHitResult) mRebootLoopTimer = false;
                mInternalLoopTimer = 0.0f;
            }
            //--Loop is booted. Start running the internal timer.
            else
            {
                //--Timer handling.
                mInternalLoopTimer = mInternalLoopTimer + 1.0f;

                //--Computations.
                float tPercentComplete = (float)fmod((mInternalLoopTimer / mTicksPerWholeLoop), 1.0f);
                float tFramesFromZero = (mHiFrame - mLowFrame) * tPercentComplete;
                float tSetFrame = mLowFrame + tFramesFromZero;

                //--Override the timer.
                mTimer = tSetFrame * mTicksPerFrame;
            }
        }
        //--When doing a reverse loop:
        else if(mLoopType == PMA_LOOP_REVERSE)
        {
            //--If we haven't booted the loop yet, run to the start target.
            if(mRebootLoopTimer)
            {
                bool tHitResult = TickTowardsValue(mLowFrame * mTicksPerFrame, mTickRate);
                if(tHitResult) mRebootLoopTimer = false;
                mInternalLoopTimer = 0.0f;
            }
            //--Loop is booted. Start running the internal timer.
            else
            {
                //--Timer handling.
                mInternalLoopTimer = mInternalLoopTimer + 1.0f;

                //--Computations.
                float tPercentComplete = (float)fmod((mInternalLoopTimer / mTicksPerWholeLoop), 2.0f);
                if(tPercentComplete > 1.0f) tPercentComplete = 2.0f - tPercentComplete;
                float tFramesFromZero = (mHiFrame - mLowFrame) * tPercentComplete;
                float tSetFrame = mLowFrame + tFramesFromZero;

                //--Override the timer.
                mTimer = tSetFrame * mTicksPerFrame;
            }
        }
    }
}PerMapAnimation;

//--[Invisible Zone]
//--When the player is in one of these, enemy AI can't see them.
typedef struct
{
    //--Position. Top-left of the zone and Width/Height. Units are in tiles.
    TwoDimensionReal mDimensions;

    //--State. Can be deactivated by script commands.
    bool mIsActivated;

    //--Methods
    void Initialize()
    {
        //--Position
        mDimensions.Set(0.0f, 0.0f, 1.0f, 1.0f);

        //--State
        mIsActivated = true;
    }

}InvisibleZone;
