//--Base
#include "AdventureLevel.h"

//--Classes
#include "TileLayer.h"
#include "TilemapActor.h"

//--CoreClasses
#include "SugarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "HitDetection.h"

//--Libraries
//--Managers
#include "EntityManager.h"

//--[Static]
bool AdventureLevel::IsEnemyDead(const char *pName)
{
    //--If the enemy is on the destroyed list, it's dead.
    if(xDestroyedEnemiesList->GetElementByName(pName) != NULL)
    {
        //--In randomly generated levels, this always fails. Enemies always spawn.
        AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
        if(rActiveLevel && !strcasecmp(rActiveLevel->GetName(), "RandomLevel")) return false;

        //--Otherwise, it's dead.
        return true;
    }

    //--If the enemy is currently available as an NPC, it's not dead.
    SugarLinkedList *rEntityList = EntityManager::Fetch()->GetEntityList();
    RootEntity *rCheckEntity = (RootEntity *)rEntityList->PushIterator();
    while(rCheckEntity)
    {
        //--Entity must be of the right type.
        if(rCheckEntity->IsOfType(POINTER_TYPE_TILEMAPACTOR))
        {
            //--Cast.
            TilemapActor *rActor = (TilemapActor *)rCheckEntity;

            //--Is it the enemy in question? It's alive, then.
            if(rActor->IsEnemy(pName))
            {
                rEntityList->PopIterator();
                return false;
            }
        }

        //--Next.
        rCheckEntity = (RootEntity *)rEntityList->AutoIterate();
    }

    //--No NPC was found holding this entity, so it must be dead.
    return true;
}
bool AdventureLevel::IsEnemyMugged(const char *pName)
{
    //--If the enemy is on the mugged list, it has been mugged.
    if(xMuggedEnemiesList->GetElementByName(pName) != NULL)
    {
        //--In randomly generated levels, this always fails.
        AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
        if(rActiveLevel && !strcasecmp(rActiveLevel->GetName(), "RandomLevel")) return false;

        //--Otherwise, it has been mugged.
        return true;
    }

    //--Entity was not on the mugged list, therefore, it was not mugged.
    return false;
}

//--[Manipulators]
void AdventureLevel::RegisterEnemyPack(SpawnPackage *pPackage)
{
    if(!pPackage) return;
    mEnemySpawnList->AddElement("X", pPackage, &FreeThis);
}
void AdventureLevel::KillEnemy(const char *pName)
{
    static int xDummy = 0;
    xDestroyedEnemiesList->AddElement(pName, &xDummy);
}
void AdventureLevel::MarkEnemyMugged(const char *pName)
{
    static int xDummy = 0;
    xMuggedEnemiesList->AddElement(pName, &xDummy);
}
void AdventureLevel::KillSpawnedEnemy(const char *pName)
{
    //--If an enemy has already spawned and we want to remove it after the fact, we do this instead of KillEnemy().
    KillEnemy(pName);

    //--Find the corresponding field actor and remove it.
    SugarLinkedList *rEntityList = EntityManager::Fetch()->GetEntityList();
    RootEntity *rCheckEntity = (RootEntity *)rEntityList->PushIterator();
    while(rCheckEntity)
    {
        //--Entity must be of the right type.
        if(rCheckEntity->IsOfType(POINTER_TYPE_TILEMAPACTOR))
        {
            //--Cast.
            TilemapActor *rActor = (TilemapActor *)rCheckEntity;

            //--Is it the enemy in question? It's alive, then.
            if(rActor->IsEnemy(pName))
            {
                rActor->mSelfDestruct = true;
                rEntityList->PopIterator();
                break;
            }
        }

        //--Next.
        rCheckEntity = (RootEntity *)rEntityList->AutoIterate();
    }
}
void AdventureLevel::WipeDestroyedEnemies()
{
    xDestroyedEnemiesList->ClearList();
    xMuggedEnemiesList->ClearList();
}
void AdventureLevel::SpawnEnemies()
{
    //--Check if this is a randomly generated level. If so, don't check the destroyed list.
    bool tBypassDestroyedList = (!strcasecmp(mName, "RandomLevel"));

    //--Go through all the enemy spawn packs and spawn any that aren't dead.
    SpawnPackage *rPackage = (SpawnPackage *)mEnemySpawnList->PushIterator();
    while(rPackage)
    {
        //--If the enemy is on the destroyed list, we don't respawn it.
        if(xDestroyedEnemiesList->GetElementByName(rPackage->mUniqueSpawnName) != NULL && !tBypassDestroyedList)
        {
        }
        //--Check if this enemy is "dead", but not on the destroyed list. If it is "dead", spawn it.
        else if(IsEnemyDead(rPackage->mUniqueSpawnName) || tBypassDestroyedList)
        {
            //--Create, set basic flags.
            TilemapActor *nActor = new TilemapActor();
            nActor->SetName(rPackage->mRawName);
            nActor->ActivateEnemyMode(rPackage->mUniqueSpawnName);
            nActor->SetLeashingPoint(rPackage->mDimensions.mXCenter, rPackage->mDimensions.mYCenter);
            nActor->SetPosition(rPackage->mDimensions.mLft / TileLayer::cxSizePerTile, (rPackage->mDimensions.mTop / TileLayer::cxSizePerTile) - 1.0f);
            nActor->SetCollisionDepth(rPackage->mDepth);
            nActor->SetDontRefaceFlag(rPackage->mDontRefaceFlag);
            nActor->SetMuggable(false);

            //--Special: If the name of the appearance is "DoctorBag" then this is a DoctorBag restoring item.
            if(!strcasecmp(rPackage->mAppearancePath, "DoctorBag"))
            {
                //--Set this special flag.
                nActor->ActivateBagRefillMode();

                //--Give this entity its appearance data.
                nActor->BuildGraphicsFromName("BagRefill", false, false);

                //--Always auto-animates.
                nActor->SetAutoAnimateFlag(true);
            }
            //--All other enemies:
            else
            {
                //--Flags.
                nActor->AddCombatEnemy(rPackage->mPartyEnemy);
                nActor->SetToughness(rPackage->mToughness);
                nActor->SetFast(rPackage->mIsFast);
                nActor->SetRelentless(rPackage->mIsRelentless);
                nActor->SetNeverPauses(rPackage->mNeverPauses);
                if(rPackage->mFollowTarget[0] != '\0') nActor->SetFollowTarget(rPackage->mFollowTarget);
                if(rPackage->mPatrolPathway[0] != '\0') nActor->ActivatePatrolMode(rPackage->mPatrolPathway);
                nActor->SetMuggable(true);
                nActor->SetMugLootable(true);

                //--Give this enemy its appearance data.
                nActor->BuildGraphicsFromName(rPackage->mAppearancePath, false, false);

                //--If the appearance happens to be "Bee" then set it to auto-animate.
                if(!strcasecmp(rPackage->mAppearancePath, "BeeGirl"))
                {
                    nActor->SetAutoAnimateFlag(true);
                    nActor->SetOscillateFlag(true);
                }

                //--If the entity is on the mugged list, they don't give additional loot when mugged.
                if(IsEnemyMugged(rPackage->mUniqueSpawnName))
                {
                    nActor->SetMugLootable(false);
                }

                //--Set which cutscene plays if the battle ends and the player loses.
                nActor->SetDefeatScene(rPackage->mLoseCutscene);

                //--Iterate across the mIgnoreFlag string. Delimiter is '|'. Each unique string goes on the ignore list.
                int tStartOfString = 0;
                int tLen = (int)strlen(rPackage->mIgnoreFlag);
                for(int i = 0; i < tLen; i ++)
                {
                    //--If this is a '|', then add the string to the entity's list.
                    if(rPackage->mIgnoreFlag[i] == '|')
                    {
                        rPackage->mIgnoreFlag[i] = '\0';
                        nActor->AddIgnoreString(&rPackage->mIgnoreFlag[tStartOfString]);
                        rPackage->mIgnoreFlag[i] = '|';
                        tStartOfString = i + 1;
                    }
                }
            }

            //--Register to the EntityManager.
            EntityManager::Fetch()->RegisterPointer(rPackage->mRawName, nActor, &RootEntity::DeleteThis);
        }

        //--Next.
        rPackage = (SpawnPackage *)mEnemySpawnList->AutoIterate();
    }
}
void AdventureLevel::CheckActorAgainstInvisZones(TilemapActor *pActor)
{
    //--Checks the given actor against all invisible zones. If the actor is in such a zone, sets the static flag so enemies
    //  know to ignore the actor.
    if(!pActor) return;

    //--Iterate.
    InvisibleZone *rPackage = (InvisibleZone *)mInvisibleZoneList->PushIterator();
    while(rPackage)
    {
        //--If the package is not active, ignore it.
        if(!rPackage->mIsActivated)
        {

        }
        //--Check for a full collision.
        else if(pActor->GetWorldX() >= rPackage->mDimensions.mLft && pActor->GetWorldX() + TA_SIZE <= rPackage->mDimensions.mRgt &&
           pActor->GetWorldY() >= rPackage->mDimensions.mTop && pActor->GetWorldY() + TA_SIZE <= rPackage->mDimensions.mBot)
        {
            xIsPlayerInInvisZone = true;
        }
        //--Check for a partial collision.
        else if(IsCollision2DReal(rPackage->mDimensions, pActor->GetWorldX(), pActor->GetWorldY(), pActor->GetWorldX() + TA_SIZE, pActor->GetWorldY() + TA_SIZE))
        {
            xIsPlayerInInvisZone = true;
        }
        //--No hit.
        else
        {
        }

        //--If this flag is set, stop iterating here.
        if(xIsPlayerInInvisZone)
        {
            mInvisibleZoneList->PopIterator();
            break;
        }

        //--Next package.
        rPackage = (InvisibleZone *)mInvisibleZoneList->AutoIterate();
    }
}
