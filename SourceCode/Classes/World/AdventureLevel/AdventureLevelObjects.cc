//--Base
#include "AdventureLevel.h"

//--Classes
#include "AdventureLight.h"
#include "AdventureInventory.h"
#include "RootEntity.h"
#include "TileLayer.h"
#include "TilemapActor.h"

//--CoreClasses
#include "SugarAutoBuffer.h"
#include "SugarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "DebugManager.h"
#include "EntityManager.h"

void AdventureLevel::RegisterStaircase(const char *pName, float pX, float pY, float pW, float pH, const char *pDir, const char *pExitDest, const char *pMapDest)
{
    //--Registers a new staircase. Used by scripts typically.
    if(!pName || !pDir || !pExitDest || !pMapDest) return;

    //--Special: If the MapDest is "None", stop here.
    if(!strcasecmp(pMapDest, "None")) return;

    //--Special: Stop if a staircase named that is already there.
    if(mExitList->GetElementByName(pName) != NULL) return;

    //--Create the package.
    SetMemoryData(__FILE__, __LINE__);
    ExitPackage *nExitPack = (ExitPackage *)starmemoryalloc(sizeof(ExitPackage));
    nExitPack->Initialize();

    //--Copy position data.
    nExitPack->mIsStaircase = true;
    nExitPack->mX = pX;
    nExitPack->mY = pY;
    nExitPack->mW = pW;
    nExitPack->mH = pH;
    strcpy(nExitPack->mExitDestination, pExitDest);
    strcpy(nExitPack->mMapDestination, pMapDest);

    //--Image setup.
    char tBuffer[128];
    sprintf(tBuffer, "Root/Images/Sprites/Objects/Stair%s", pDir);
    nExitPack->rRenderImg = (SugarBitmap *)DataLibrary::Fetch()->GetEntry(tBuffer);

    //--Register it.
    mExitList->AddElement(pName, nExitPack, &FreeThis);
}
void AdventureLevel::ParseObjectData()
{
    //--Once the data is read and stored, we can parse through it to pull out the parts we want.
    //  Note that this usually is not done right when the objects are read out, as there may be limiters
    //  set or objects might be discarded by script values.

    //--Iterate across them all.
    ObjectInfoPack *rObjectPack = (ObjectInfoPack *)mObjectData->PushIterator();
    while(rObjectPack)
    {
        HandleObject(rObjectPack);
        rObjectPack = (ObjectInfoPack *)mObjectData->AutoIterate();
    }
}
void AdventureLevel::HandleObject(ObjectInfoPack *pPack)
{
    //--Subroutine, handles the object pack as expected in ParseObjectData().
    if(!pPack) return;

    //--Fast-access pointers.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    const char *rType = pPack->mType;

    //--Chest. General-purpose, always contains an item by default. Can also have special flags set to do things
    //  other than... contain an item. Also implicitly stores unique-ID information for save files.
    //--Incidentally, chests use the same object type as doors. Figure that one out.
    if(!strcasecmp(rType, "Chest"))
    {
        //--Create a new package.
        SetMemoryData(__FILE__, __LINE__);
        DoorPackage *nChestPack = (DoorPackage *)starmemoryalloc(sizeof(DoorPackage));
        nChestPack->Initialize();

        //--Copy position data.
        nChestPack->mX = pPack->mX;
        nChestPack->mY = pPack->mY - TileLayer::cxSizePerTile;

        //--Temporary.
        bool tIsFuture = false;

        //--Set images.
        nChestPack->rRenderClosed = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Objects/ChestC");
        nChestPack->rRenderOpen   = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Objects/ChestO");

        //--Property Check:
        for(int i = 0; i < pPack->mProperties->mPropertiesTotal; i ++)
        {
            //--Fast-access.
            char *rKey = pPack->mProperties->mKeys[i];
            char *rVal = pPack->mProperties->mVals[i];

            //--This property means the chest cannot be opened by activating it directly.
            if(!strcasecmp(rKey, "ScriptLocked"))
            {
                nChestPack->mIsScriptLocked = true;
            }
            //--This property causes the chest to run an examination case when the player tries to open it.
            //  It will not stop the chest from opening normally, but no notification of items received will
            //  appear if something else got printed.
            else if(!strcasecmp(rKey, "Examine"))
            {
                ResetString(nChestPack->mExamineScript, rVal);
            }
            //--Contents. Whatever item is contained in the chest.
            else if(!strcasecmp(rKey, "Contents"))
            {
                ResetString(nChestPack->mContents, rVal);
            }
            //--Depth. Used for rendering.
            else if(!strcasecmp(rKey, "Depth"))
            {
                nChestPack->mZ = atoi(rVal);
            }
            //--Futuristic chest. Changes rendering pattern.
            else if(!strcasecmp(rKey, "IsFuture"))
            {
                tIsFuture = true;
                nChestPack->rRenderClosed = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Objects/ChestFC");
                nChestPack->rRenderOpen   = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Objects/ChestFO");
            }
        }

        //--Special: If the contents were CATALYST|X (X can be any of the five catalyst types) this is a blue chest.
        if(nChestPack->mContents && !strncasecmp(nChestPack->mContents, "CATALYST|", 9))
        {
            //--Change color.
            nChestPack->rRenderClosed = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Objects/ChestBC");
            nChestPack->rRenderOpen   = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Objects/ChestBO");

            //--Futuristic chests use futuristic catalyst images.
            if(tIsFuture)
            {
                nChestPack->rRenderClosed = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Objects/ChestFBC");
                nChestPack->rRenderOpen   = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Objects/ChestFBO");
            }

            //--Tally:
            if(!strcasecmp(&nChestPack->mContents[ 9], "Health"))     xCountCatalyst[CATALYST_HEALTH] ++;
            if(!strcasecmp(&nChestPack->mContents[ 9], "Attack"))     xCountCatalyst[CATALYST_ATTACK] ++;
            if(!strcasecmp(&nChestPack->mContents[ 9], "Initiative")) xCountCatalyst[CATALYST_INITIATIVE] ++;
            if(!strcasecmp(&nChestPack->mContents[ 9], "Dodge"))      xCountCatalyst[CATALYST_DODGE] ++;
            if(!strcasecmp(&nChestPack->mContents[ 9], "Accuracy"))   xCountCatalyst[CATALYST_ACCURACY] ++;
            if(!strcasecmp(&nChestPack->mContents[10], "Skill"))      xCountCatalyst[CATALYST_SKILL] ++;
        }

        //--Register.
        mChestList->AddElement(pPack->mName, nChestPack, &FreeThis);
    }
    //--It's called a door. You go through it. Move.
    else if(!strcasecmp(rType, "Door"))
    {
        //--Door. Create a new package.
        SetMemoryData(__FILE__, __LINE__);
        DoorPackage *nDoorPack = (DoorPackage *)starmemoryalloc(sizeof(DoorPackage));
        nDoorPack->Initialize();

        //--Copy position data.
        nDoorPack->mX = pPack->mX;
        nDoorPack->mY = pPack->mY;

        //--Other.
        nDoorPack->rRenderClosed = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Objects/DoorNS");

        //--Property Check:
        for(int i = 0; i < pPack->mProperties->mPropertiesTotal; i ++)
        {
            //--Fast Access
            char *rKey = pPack->mProperties->mKeys[i];
            char *rVal = pPack->mProperties->mVals[i];

            //--If any property is "IsEW" then change the render image.
            if(!strcasecmp(rKey, "IsEW"))
            {
                nDoorPack->rRenderClosed = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Objects/DoorEW");
            }
            //--This property means the door does not open on its own, and must be manually opened by a script call.
            else if(!strcasecmp(rKey, "ScriptLocked"))
            {
                nDoorPack->mIsScriptLocked = true;
            }
            //--This property causes the door to run an examination case when the player tries to open it.
            //  It will not stop the door from opening if it would have done so.
            else if(!strcasecmp(rKey, "Examine"))
            {
                ResetString(nDoorPack->mExamineScript, rVal);
            }
            //--Dungeon door, uses different graphics.
            else if(!strcasecmp(rKey, "IsDungeon"))
            {
                nDoorPack->rRenderClosed = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Objects/DoorNSDungeonC");
                nDoorPack->rRenderOpen   = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Objects/DoorNSDungeonO");
            }
            //--Dungeon door with vines, uses different graphics.
            else if(!strcasecmp(rKey, "IsDungeonVine"))
            {
                nDoorPack->rRenderClosed = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Objects/DoorNSDungeonCV");
                nDoorPack->rRenderOpen   = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Objects/DoorNSDungeonOV");
            }
            //--Silver door, uses different graphics. Requires a key to open.
            else if(!strcasecmp(rKey, "IsSilver"))
            {
                nDoorPack->rRenderClosed = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Objects/DoorNSSilverC");
                nDoorPack->rRenderOpen   = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Objects/DoorNSSilverO");
            }
            //--Spooky door. Wooden, used for the Mansion tileset.
            else if(!strcasecmp(rKey, "IsSpooky"))
            {
                nDoorPack->rRenderClosed = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Objects/DoorNSSpooky");
                nDoorPack->rRenderOpen   = NULL;
            }
            //--Space door. Used in Regulus.
            else if(!strcasecmp(rKey, "IsSpace"))
            {
                nDoorPack->rRenderClosed = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Objects/DoorNSRegulusC");
                nDoorPack->rRenderOpen   = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Objects/DoorNSRegulusO");
                nDoorPack->mUseSpaceSound = true;
            }
            //--Flashback door.
            else if(!strcasecmp(rKey, "IsFlashback"))
            {
                nDoorPack->rRenderClosed = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Objects/DoorEWRegulusFC");
                nDoorPack->rRenderOpen   = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Objects/DoorEWRegulusFO");
            }
            //--Space door, EW.
            else if(!strcasecmp(rKey, "IsSpaceEW"))
            {
                nDoorPack->rRenderClosed = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Objects/DoorEWRegulusC");
                nDoorPack->rRenderOpen = NULL;
                nDoorPack->mIsSpaceEW = true;
                nDoorPack->mUseSpaceSound = true;
            }
            //--Space door wide. Used in Regulus.
            else if(!strcasecmp(rKey, "IsSpaceWide"))
            {
                nDoorPack->rRenderClosed = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Objects/DoorNSRegulusWC");
                nDoorPack->rRenderOpen   = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Objects/DoorNSRegulusWO");
                nDoorPack->mIsWide = true;
                nDoorPack->mUseSpaceSound = true;
            }
            //--Immediate transit. Opening this door causes a level transition.
            else if(!strcasecmp(rKey, "LevelTransition"))
            {
                ResetString(nDoorPack->mLevelTransitOnOpen, rVal);
            }
            //--Which exit to go to after transition.
            else if(!strcasecmp(rKey, "LevelTransitionExit"))
            {
                ResetString(nDoorPack->mLevelTransitExit, rVal);
            }
            //--Depth. When using this door to spawn at, places the player at the listed depth. Default is 0.
            else if(!strcasecmp(rKey, "Depth"))
            {
                nDoorPack->mZ = atoi(rVal);
            }
        }

        //--Register it.
        mDoorList->AddElement(pPack->mName, nDoorPack, &FreeThis);
    }
    //--Player Start. Sets where the player spawns if they are using debug functions. Only the last one found is
    //  used if there are many.
    else if(!strcasecmp(rType, "PlayerStart"))
    {
        mPlayerStartX = pPack->mX / (float)TileLayer::cxSizePerTile;
        mPlayerStartY = pPack->mY / (float)TileLayer::cxSizePerTile;
    }
    //--Examinable. A point the player can examine to perform some action or bring up dialogue.
    //  Examinable coordinates are integers.
    else if(!strcasecmp(rType, "Examinable"))
    {
        //--Position.
        int tExamineL = pPack->mX;
        int tExamineT = pPack->mY;
        int tExamineW = pPack->mW;
        int tExamineH = pPack->mH;

        //--Transition Cases
        char tAutoTransRoom[STD_MAX_LETTERS];
        char tAutoTransLocation[STD_MAX_LETTERS];
        char tAutoTransSound[STD_MAX_LETTERS];
        tAutoTransRoom[0] = '\0';
        tAutoTransLocation[0] = '\0';
        tAutoTransSound[0] = '\0';

        //--Property Check: Determine flags.
        uint8_t tFlags = 0;
        for(int i = 0; i < pPack->mProperties->mPropertiesTotal; i ++)
        {
            //--Fast Access
            char *rKey = pPack->mProperties->mKeys[i];
            char *rVal = pPack->mProperties->mVals[i];

            if(!strcasecmp(rKey, "NotNorth"))
            {
                tFlags = tFlags | EXAMINE_NOT_FROM_NORTH;
            }
            else if(!strcasecmp(rKey, "TransitionRoom"))
            {
                strncpy(tAutoTransRoom, rVal, STD_MAX_LETTERS);
            }
            else if(!strcasecmp(rKey, "TransitionLocation"))
            {
                strncpy(tAutoTransLocation, rVal, STD_MAX_LETTERS);
            }
            else if(!strcasecmp(rKey, "TransitionSound"))
            {
                strncpy(tAutoTransSound, rVal, STD_MAX_LETTERS);
            }
        }

        //--Check if all three automatic transition flags were set. If so, this is a special transition entity.
        if(tAutoTransRoom[0] != '\0' || tAutoTransLocation[0] != '\0' || tAutoTransSound[0] != '\0')
        {
            AddExaminableTransition(pPack->mName, tExamineL, tExamineT, tExamineW, tExamineH, tFlags, tAutoTransRoom, tAutoTransLocation, tAutoTransSound);
        }
        //--Normal examinable.
        else
        {
            AddExaminable(tExamineL, tExamineT, tExamineW, tExamineH, tFlags, pPack->mName);
        }
    }
    //--Charger. Recharges the player's light.
    else if(!strcasecmp(rType, "Charger"))
    {
        //--Position.
        int tExamineL = pPack->mX;
        int tExamineT = pPack->mY;
        int tExamineW = pPack->mW;
        int tExamineH = pPack->mH;
        AddCharger(tExamineL, tExamineT, tExamineW, tExamineH);
    }
    //--Exit. Also serves as an entrance point.
    else if(!strcasecmp(rType, "Exit"))
    {
        //--Create the package.
        SetMemoryData(__FILE__, __LINE__);
        ExitPackage *nExitPack = (ExitPackage *)starmemoryalloc(sizeof(ExitPackage));
        nExitPack->Initialize();

        //--Copy position data.
        nExitPack->mX = pPack->mX;
        nExitPack->mY = pPack->mY;
        nExitPack->mW = pPack->mW;
        nExitPack->mH = pPack->mH;

        //--Property Check: Store destination strings.
        for(int i = 0; i < pPack->mProperties->mPropertiesTotal; i ++)
        {
            //--Fast Access
            char *rKey = pPack->mProperties->mKeys[i];
            char *rVal = pPack->mProperties->mVals[i];

            if(!strcasecmp(rKey, "ExitDest"))
            {
                strcpy(nExitPack->mExitDestination, rVal);
            }
            else if(!strcasecmp(rKey, "MapDest"))
            {
                strcpy(nExitPack->mMapDestination, rVal);
                if(!strcasecmp(nExitPack->mMapDestination, "None"))
                {
                    free(nExitPack);
                    return;
                }
            }
            else if(!strcasecmp(rKey, "Depth"))
            {
                nExitPack->mDepth = atoi(rVal);
            }
        }

        //--Register it.
        mExitList->AddElement(pPack->mName, nExitPack, &FreeThis);
    }
    //--Save Point. Restores HP, lets you save the game, allows party lineup changes.
    else if(!strcasecmp(rType, "SavePoint"))
    {
        //--Create the package.
        SetMemoryData(__FILE__, __LINE__);
        SavePointPackage *nSavePackage = (SavePointPackage *)starmemoryalloc(sizeof(SavePointPackage));
        nSavePackage->Initialize();

        //--Copy position data.
        nSavePackage->mX = pPack->mX;
        nSavePackage->mY = pPack->mY;

        //--Images.
        nSavePackage->rUnlitImg = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Objects/FireUnlit");
        nSavePackage->rLitImgA  = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Objects/FireLit0");
        nSavePackage->rLitImgB  = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Objects/FireLit1");

        //--Property Check.
        for(int i = 0; i < pPack->mProperties->mPropertiesTotal; i ++)
        {
            //--Fast Access
            char *rKey = pPack->mProperties->mKeys[i];
            //char *rVal = pPack->mProperties->mVals[i];

            //--Changes this to a futuristic heating coil.
            if(!strcasecmp(rKey, "IsSpace"))
            {
                nSavePackage->mIsSpace = true;
                if(!nSavePackage->mIsBench)
                {
                    nSavePackage->rUnlitImg = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Objects/CoilUnlit");
                    nSavePackage->rLitImgA  = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Objects/CoilLit0");
                    nSavePackage->rLitImgB  = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Objects/CoilLit1");
                }
                else
                {
                    nSavePackage->rUnlitImg = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Objects/BenchMetal");
                    nSavePackage->rLitImgA  = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Objects/BenchMetal");
                    nSavePackage->rLitImgB  = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Objects/BenchMetal");
                }
            }
            //--Bench.
            else if(!strcasecmp(rKey, "IsBench"))
            {
                nSavePackage->mIsBench = true;
                if(!nSavePackage->mIsSpace)
                {
                    nSavePackage->rUnlitImg = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Objects/BenchWood");
                    nSavePackage->rLitImgA  = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Objects/BenchWood");
                    nSavePackage->rLitImgB  = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Objects/BenchWood");
                }
                else
                {
                    nSavePackage->rUnlitImg = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Objects/BenchMetal");
                    nSavePackage->rLitImgA  = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Objects/BenchMetal");
                    nSavePackage->rLitImgB  = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Objects/BenchMetal");
                }
            }
        }

        //--Register it.
        mSavePointList->AddElement("X", nSavePackage, &FreeThis);
    }
    //--Staircase. Performs the same function as an exit, but with slightly different mechanics.
    else if(!strcasecmp(rType, "Staircase"))
    {
        //--Create the package.
        SetMemoryData(__FILE__, __LINE__);
        ExitPackage *nExitPack = (ExitPackage *)starmemoryalloc(sizeof(ExitPackage));
        nExitPack->Initialize();

        //--Copy position data.
        nExitPack->mIsStaircase = true;
        nExitPack->mX = pPack->mX;
        nExitPack->mY = pPack->mY;
        nExitPack->mW = pPack->mW;
        nExitPack->mH = pPack->mH;

        //--Property Check: Store destination strings.
        for(int i = 0; i < pPack->mProperties->mPropertiesTotal; i ++)
        {
            //--Fast Access
            char *rKey = pPack->mProperties->mKeys[i];
            char *rVal = pPack->mProperties->mVals[i];

            //--Which staircase to travel to.
            if(!strcasecmp(rKey, "StairDest"))
            {
                strcpy(nExitPack->mExitDestination, rVal);
            }
            //--Which map to travel to.
            else if(!strcasecmp(rKey, "MapDest"))
            {
                strcpy(nExitPack->mMapDestination, rVal);
                if(!strcasecmp(nExitPack->mMapDestination, "None"))
                {
                    free(nExitPack);
                    return;
                }
            }
            //--The image to use.
            else if(!strcasecmp(rKey, "Direction"))
            {
                char tBuffer[128];
                sprintf(tBuffer, "Root/Images/Sprites/Objects/Stair%s", rVal);
                nExitPack->rRenderImg = (SugarBitmap *)DataLibrary::Fetch()->GetEntry(tBuffer);
            }
        }

        //--Register it.
        mExitList->AddElement(pPack->mName, nExitPack, &FreeThis);
    }
    //--Trigger zone. Does different things based on the scripts.
    else if(!strcasecmp(rType, "Trigger"))
    {
        //--Create.
        SetMemoryData(__FILE__, __LINE__);
        TriggerPackage *nPackage = (TriggerPackage *)starmemoryalloc(sizeof(TriggerPackage));
        nPackage->Initialize();

        //--Position.
        nPackage->mDimensions.SetWH(pPack->mX, pPack->mY, pPack->mW, pPack->mH);

        //--The trigger name is the name of the package. Multiple triggers can share the same name.
        strncpy(nPackage->mFiringName, pPack->mName, STD_MAX_LETTERS - 1);

        //--Register.
        mTriggerList->AddElement(pPack->mName, nPackage, &FreeThis);
    }
    //--Enemy. NPC with an AI routine, touching it triggers a battle. This just records the spawn point,
    //  spawning is handled using different logic.
    else if(!strcasecmp(rType, "Enemy"))
    {
        //--Create.
        SetMemoryData(__FILE__, __LINE__);
        SpawnPackage *nPackage = (SpawnPackage *)starmemoryalloc(sizeof(SpawnPackage));
        nPackage->Initialize();
        strncpy(nPackage->mRawName, pPack->mName, STD_MAX_LETTERS);

        //--Position. Add one tile to the Y position.
        nPackage->mDimensions.SetWH(pPack->mX, pPack->mY, pPack->mW, pPack->mH);

        //--Property Check: Store destination strings.
        for(int i = 0; i < pPack->mProperties->mPropertiesTotal; i ++)
        {
            //--Fast Access
            char *rKey = pPack->mProperties->mKeys[i];
            char *rVal = pPack->mProperties->mVals[i];

            //--Which enemy this represents.
            if(!strcasecmp(rKey, "Party"))
            {
                strcpy(nPackage->mPartyEnemy, rVal);
            }
            //--Which sprites to use.
            else if(!strcasecmp(rKey, "Appearance"))
            {
                strcpy(nPackage->mAppearancePath, rVal);
            }
            //--Which cutscene to play if the player loses.
            else if(!strcasecmp(rKey, "Scene"))
            {
                strcpy(nPackage->mLoseCutscene, rVal);
            }
            //--Ignore the player when they are in this form. Can be several instances of this.
            else if(!strncasecmp(rKey, "Ignore", 6))
            {
                strcat(nPackage->mIgnoreFlag, rVal);
                strcat(nPackage->mIgnoreFlag, "|");
            }
            //--Which collision depth to use.
            else if(!strcasecmp(rKey, "Depth"))
            {
                nPackage->mDepth = atoi(rVal);
            }
            //--Toughness rating.
            else if(!strcasecmp(rKey, "Toughness"))
            {
                nPackage->mToughness = atoi(rVal);
            }
            //--Patrol pathway.
            else if(!strcasecmp(rKey, "PatrolPath"))
            {
                strncpy(nPackage->mPatrolPathway, rVal, PATROLPATH_MAX_LETTERS);
            }
            //--Fast. Enemy chases the player at double speed.
            else if(!strcasecmp(rKey, "Fast"))
            {
                nPackage->mIsFast = true;
            }
            //--Relentless. Enemy chases the player twice as far.
            else if(!strcasecmp(rKey, "Relentless"))
            {
                nPackage->mIsRelentless = true;
            }
            //--Never pauses on patrol route to look around.
            else if(!strcasecmp(rKey, "Never Pauses"))
            {
                nPackage->mNeverPauses = true;
            }
            //--Follow. Entity follows another entity unless it doesn't exist.
            else if(!strcasecmp(rKey, "Follow"))
            {
                strncpy(nPackage->mFollowTarget, rVal, STD_MAX_LETTERS);
            }
        }

        //--Store the unique ID for this enemy. This will be updated later, since the level has not received a
        //  name yet (AL_SetProperty() does this after the level is parsed).
        sprintf(nPackage->mUniqueSpawnName, "%s", pPack->mName);

        //--Register.
        //fprintf(stderr, "Registered enemy package: %s %s\n", pPack->mName, nPackage->mUniqueSpawnName);
        RegisterEnemyPack(nPackage);
    }
    //--Location. Used to spawn NPCs.
    else if(!strcasecmp(rType, "Location"))
    {
        //--Create.
        SetMemoryData(__FILE__, __LINE__);
        TwoDimensionReal *nPackage = (TwoDimensionReal *)starmemoryalloc(sizeof(TwoDimensionReal));

        //--Position. Add one tile to the Y position.
        nPackage->SetWH(pPack->mX, pPack->mY, pPack->mW, pPack->mH);

        //--Store.
        mLocationList->AddElement(pPack->mName, nPackage, &FreeThis);
    }
    //--Switches. Use the DoorPackage structure, but always trigger examination and can store their flip states
    //  even when the room changes (sometimes).
    //--The "Closed" state equates to the switch being down.
    else if(!strcasecmp(rType, "Switch"))
    {
        //--Door. Create a new package.
        SetMemoryData(__FILE__, __LINE__);
        DoorPackage *nSwitchPack = (DoorPackage *)starmemoryalloc(sizeof(DoorPackage));
        nSwitchPack->Initialize();

        //--Copy position data.
        nSwitchPack->mX = pPack->mX;
        nSwitchPack->mY = pPack->mY;

        //--Other.
        nSwitchPack->rRenderClosed = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Objects/SwitchDn");
        nSwitchPack->rRenderOpen   = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Sprites/Objects/SwitchUp");

        //--Register.
        mSwitchList->AddElement(pPack->mName, nSwitchPack, &DoorPackage::DeleteThis);
    }
    //--Inflection points. Causes entities to change which collision set they're using.
    else if(!strcasecmp(rType, "CollisionInflection"))
    {
        //--Uses an InflectionPackage structure.
        SetMemoryData(__FILE__, __LINE__);
        InflectionPackage *nPackage = (InflectionPackage *)starmemoryalloc(sizeof(InflectionPackage));
        nPackage->Initialize();

        //--Position data.
        nPackage->mX = pPack->mX;
        nPackage->mY = pPack->mY;
        nPackage->mW = pPack->mW;
        nPackage->mH = pPack->mH;

        //--Property Check: Store depths.
        for(int i = 0; i < pPack->mProperties->mPropertiesTotal; i ++)
        {
            //--Fast Access
            char *rKey = pPack->mProperties->mKeys[i];
            char *rVal = pPack->mProperties->mVals[i];

            //--Depth when exiting the bottom of the entity.
            if(!strcasecmp(rKey, "LowerDepth"))
            {
                nPackage->mLowerDepth = atoi(rVal);
            }
            //--Depth when exiting the top of the entity.
            else if(!strcasecmp(rKey, "UpperDepth"))
            {
                nPackage->mUpperDepth = atoi(rVal);
            }
        }

        //--Register.
        mInflectionList->AddElement(pPack->mName, nPackage, &FreeThis);
    }
    //--Climbable object. Can be a rope or a ladder.
    else if(!strcasecmp(rType, "Climbable"))
    {
        //--Uses an ClimbablePackage structure.
        SetMemoryData(__FILE__, __LINE__);
        ClimbablePackage *nPackage = (ClimbablePackage *)starmemoryalloc(sizeof(ClimbablePackage));
        nPackage->Initialize();

        //--Position data.
        nPackage->mX = pPack->mX;
        nPackage->mY = pPack->mY;
        nPackage->mW = pPack->mW;
        nPackage->mH = pPack->mH;

        //--Property Check: Store depths.
        for(int i = 0; i < pPack->mProperties->mPropertiesTotal; i ++)
        {
            //--Fast Access
            char *rKey = pPack->mProperties->mKeys[i];
            char *rVal = pPack->mProperties->mVals[i];

            //--Entity cannot be examined or removed.
            if(!strcasecmp(rKey, "AlwaysActive"))
            {
                nPackage->mIsActivated = true;
                nPackage->mIsAlwaysActivated = true;
            }
            //--Whether this is a ladder or a rope.
            else if(!strcasecmp(rKey, "IsRope"))
            {
                if(!strcasecmp(rVal, "True"))
                {
                    nPackage->mIsLadder = false;
                }
                else
                {
                    nPackage->mIsLadder = true;
                }
            }
            //--Depth when exiting the bottom of the entity.
            else if(!strcasecmp(rKey, "LowerDepth"))
            {
                nPackage->mLowerDepth = atoi(rVal);
            }
            //--Depth when exiting the top of the entity.
            else if(!strcasecmp(rKey, "UpperDepth"))
            {
                nPackage->mUpperDepth = atoi(rVal);
            }
        }

        //--Register.
        mClimbableList->AddElement(pPack->mName, nPackage, &FreeThis);
    }
    //--PatrolNode. Enemies use these to patrol around the level.
    else if(!strcasecmp(rType, "PatrolNode"))
    {
        SetMemoryData(__FILE__, __LINE__);
        TwoDimensionReal *nZone = (TwoDimensionReal *)starmemoryalloc(sizeof(TwoDimensionReal));
        nZone->SetWH(pPack->mX, pPack->mY, pPack->mW, pPack->mH);
        mPatrolNodes->AddElement(pPack->mName, nZone, &FreeThis);
    }
    //--Camera zone. Camera can never collide with one of these.
    else if(!strcasecmp(rType, "CameraZone"))
    {
        mIsPositiveCameraMode = true;
        SetMemoryData(__FILE__, __LINE__);
        TwoDimensionReal *nZone = (TwoDimensionReal *)starmemoryalloc(sizeof(TwoDimensionReal));
        nZone->SetWH(pPack->mX, pPack->mY, pPack->mW, pPack->mH);
        mCameraZoneList->AddElement("X", nZone, &FreeThis);
    }
    //--Invisible Zone. Enemy AIs can't see the player when in one of these.
    else if(!strcasecmp(rType, "InvisZone"))
    {
        SetMemoryData(__FILE__, __LINE__);
        InvisibleZone *nZone = (InvisibleZone *)starmemoryalloc(sizeof(InvisibleZone));
        nZone->mDimensions.SetWH(pPack->mX, pPack->mY, pPack->mW, pPack->mH);
        nZone->mIsActivated = true;
        mInvisibleZoneList->AddElement("X", nZone, &FreeThis);
    }
    //--Light. Has many modes.
    else if(!strcasecmp(rType, "Light"))
    {
        //--Baseline.
        AdventureLight *nLight = new AdventureLight();
        nLight->SetName(pPack->mName);
        nLight->SetPosition(pPack->mX + pPack->mW * 0.5f, pPack->mY + pPack->mH * 0.5f);

        //--Common Properties.
        int tUseMode = ADLIT_MODE_RADIAL;
        float tIntensity = 16.0f;

        //--Square-Radial Properties.
        float tWidth = pPack->mW;
        float tHeight = pPack->mH;

        //--Color Properties.
        StarlightColor tFutureColor = StarlightColor::MapRGBAF(1.0f, 1.0f, 1.0f, 1.0f);

        //--Property Check: Store colors and type.
        for(int i = 0; i < pPack->mProperties->mPropertiesTotal; i ++)
        {
            //--Fast Access
            char *rKey = pPack->mProperties->mKeys[i];
            char *rVal = pPack->mProperties->mVals[i];

            //--Light mode.
            if(!strcasecmp(rKey, "Mode"))
            {
                //--Radial:
                if(!strcasecmp(rVal, "Radial"))
                {
                    tUseMode = ADLIT_MODE_RADIAL;
                }
                //--Square-Radial:
                else if(!strcasecmp(rVal, "SquareRadial"))
                {
                    tUseMode = ADLIT_MODE_SQUARERADIAL;
                }
                //--Ambient.
                else if(!strcasecmp(rVal, "Ambient"))
                {
                    tUseMode = -1;
                }
            }
            //--Light intensity.
            else if(!strcasecmp(rKey, "Intensity"))
            {
                tIntensity = atof(rVal);
            }
            //--Blue value of color.
            else if(!strcasecmp(rKey, "ColBlue"))
            {
                tFutureColor.b = atof(rVal);
            }
            //--Green value of color.
            else if(!strcasecmp(rKey, "ColGreen"))
            {
                tFutureColor.g = atof(rVal);
            }
            //--Red value of color.
            else if(!strcasecmp(rKey, "ColRed"))
            {
                tFutureColor.r = atof(rVal);
            }
        }

        //--Radial light.
        if(tUseMode == ADLIT_MODE_RADIAL)
        {
            nLight->SetRadial(tIntensity);
        }
        //--Square-Radial light.
        else if(tUseMode == ADLIT_MODE_SQUARERADIAL)
        {
            nLight->SetPosition(pPack->mX, pPack->mY);
            nLight->SetSquareRadial(tWidth, tHeight, tIntensity);
        }
        //--Ambient. Changes ambient values, doesn't get registered.
        else if(tUseMode == -1)
        {
            mAmbientLight.r = tFutureColor.r;
            mAmbientLight.g = tFutureColor.g;
            mAmbientLight.b = tFutureColor.b;
            delete nLight;
            return;
        }

        //--Color info.
        nLight->SetColor(tFutureColor);

        //--Register.
        RegisterLight(nLight);
    }
    //--Position. Used to spawn things, mostly NPCs.
    else if(!strcasecmp(rType, "Position"))
    {
        //--Baseline.
        SetMemoryData(__FILE__, __LINE__);
        PositionPackage *nPosition = (PositionPackage *)starmemoryalloc(sizeof(PositionPackage));
        nPosition->Initialize();
        nPosition->mX = pPack->mX;
        nPosition->mY = pPack->mY;

        //--Property Check.
        for(int i = 0; i < pPack->mProperties->mPropertiesTotal; i ++)
        {
            //--Fast Access
            char *rKey = pPack->mProperties->mKeys[i];
            char *rVal = pPack->mProperties->mVals[i];

            //--Depth. Default is zero.
            if(!strcasecmp(rKey, "Depth"))
            {
                nPosition->mZ = atoi(rVal);
            }
            //--Facing. Default is up.
            else if(!strcasecmp(rKey, "Facing"))
            {
                if(!strcasecmp(rVal, "Up")) nPosition->mFacing = DIR_UP;
                if(!strcasecmp(rVal, "Right")) nPosition->mFacing = DIR_RIGHT;
                if(!strcasecmp(rVal, "Down")) nPosition->mFacing = DIR_DOWN;
                if(!strcasecmp(rVal, "Left")) nPosition->mFacing = DIR_LEFT;
            }
            //--Dialogue path.
            else if(!strcasecmp(rKey, "Dialogue"))
            {
                strcpy(nPosition->mDialoguePath, rVal);
            }
            //--Sprite DL path.
            else if(!strcasecmp(rKey, "Sprite"))
            {
                strcpy(nPosition->mSpritePath, rVal);
            }
            //--Eight-directional NPC.
            else if(!strcasecmp(rKey, "EightDir"))
            {
                nPosition->mIsEightDirectional = true;
            }
            //--Two-directional NPC.
            else if(!strcasecmp(rKey, "TwoDir"))
            {
                nPosition->mIsTwoDirectional = true;
            }
            //--NPC activates wander mode when spawning.
            else if(!strcasecmp(rKey, "AutoWander"))
            {
                nPosition->mAutoWander = true;
            }
            //--NPC is not clipped.
            else if(!strcasecmp(rKey, "NoClip"))
            {
                nPosition->mIsClipped = false;
            }
            //--Automatically fires the dialogue script when within range of the player.
            else if(!strcasecmp(rKey, "AutofireDialogue"))
            {
                nPosition->mAutofireDialogue = true;
            }
        }

        //--Register.
        mPositionList->AddElement(pPack->mName, nPosition, &FreeThis);
    }
    //--Error, type not found.
    else
    {
        DebugManager::ForcePrint("Object Error: Unable to resolve type %s\n", rType);
    }
}
void AdventureLevel::SetActorToPositionPack(TilemapActor *pActor, const char *pPositionPack)
{
    //--Given a TilemapActor, locates the PositionPack and fills in default data as needed.
    //  Does nothing if the pack doesn't exist.
    if(!pActor || !pPositionPack) return;

    //--Locate the pack.
    PositionPackage *rPackage = (PositionPackage *)mPositionList->GetElementByName(pPositionPack);
    if(!rPackage) return;

    //--Package gains the name of the actor.
    strcpy(rPackage->mSpawnedActor, pActor->GetName());

    //--X/Y/Z.
    pActor->SetPosition(rPackage->mX / TileLayer::cxSizePerTile, (rPackage->mY / TileLayer::cxSizePerTile) - 1);
    pActor->SetCollisionDepth(rPackage->mZ);
    pActor->SetCollisionFlag(rPackage->mIsClipped);
    pActor->SetFacing(rPackage->mFacing);

    //--Paths.
    SugarAutoBuffer *tFinalBuffer = new SugarAutoBuffer();
    char *tBuffer = InitializeString("%s%s", xRootPath, rPackage->mDialoguePath);

    //--Parser. Look for special sequences within the path.
    int tLen = (int)strlen(tBuffer);
    for(int i = 0; i < tLen; i ++)
    {
        //--[MAPDIR] replaces that part of the string with the map's directory.
        if(tLen >= 8 && !strncasecmp(&tBuffer[i], "[MAPDIR]", 8))
        {
            //--Start at the part of the base path that isn't in common with the dialogue path.
            int tRootPathLen = (int)strlen(xRootPath);

            tFinalBuffer->AppendStringWithoutNull(&mBasePath[tRootPathLen]);
            i = i + 8;
        }
        //--Otherwise, append the character.
        else
        {
            tFinalBuffer->AppendCharacter(tBuffer[i]);
        }
    }

    //--Append a NULL.
    tFinalBuffer->AppendNull();

    //--Set.
    pActor->SetActivationScript((char *)tFinalBuffer->GetRawData());
    if(rPackage->mAutofireDialogue) pActor->SetAutofire(TA_AUTOFIRE_DIST);

    //--Clean.
    free(tBuffer);
    delete tFinalBuffer;

    //--Sprites.
    pActor->BuildGraphicsFromName(rPackage->mSpritePath, rPackage->mIsEightDirectional, rPackage->mIsTwoDirectional);

    //--Optional: Actor activates wander mode.
    if(rPackage->mAutoWander) pActor->ActivateWanderMode();
}
void AdventureLevel::ResetActorGraphics()
{
    //--Resets all actor graphics based on their position packs.
    PositionPackage *rPackage = (PositionPackage *)mPositionList->PushIterator();
    while(rPackage)
    {
        //--If the package has an associated name...
        if(rPackage->mSpawnedActor[0] != '\0')
        {
            //--Locate the actor.
            TilemapActor *rActor = (TilemapActor *)EntityManager::Fetch()->GetEntity(rPackage->mSpawnedActor);
            if(rActor) SetActorToPositionPack(rActor, mPositionList->GetIteratorName());

        }

        //--Next.
        rPackage = (PositionPackage *)mPositionList->AutoIterate();
    }
}
void AdventureLevel::RemoveObject(const char *pObjectType, const char *pObjectName)
{
    //--Removes the given object from the requested list. The object is placed in the MemoryManager, so it will survive
    //  at least until the end of the tick.
    if(!pObjectName || !pObjectType) return;

    //--Figure out which list to check.
    SugarLinkedList *rCheckList = NULL;
    if(!strcasecmp(pObjectType, "Chest"))
    {
        rCheckList = mChestList;
    }
    //--Special case: Convert the given chest to a fake chest.
    else if(!strcasecmp(pObjectType, "To Fake Chest"))
    {
        //--Locate the object and liberate it.
        void *rLiberatedPtr = mChestList->GetElementByName(pObjectName);
        mChestList->SetRandomPointerToThis(rLiberatedPtr);
        mChestList->LiberateRandomPointerEntry();

        //--Pass it to the fake chest list.
        mFakeChestList->AddElement(pObjectName, rLiberatedPtr, &FreeThis);
        return;
    }
    else if(!strcasecmp(pObjectType, "Door"))
    {
        rCheckList = mDoorList;
    }
    else if(!strcasecmp(pObjectType, "Exit"))
    {
        rCheckList = mExitList;
    }
    //--Special case: Enemies are handled through the EntityManager.
    else if(!strcasecmp(pObjectType, "Enemy"))
    {
        RootEntity *rEntity = EntityManager::Fetch()->GetEntity(pObjectName);
        if(rEntity) rEntity->mSelfDestruct = true;
        return;
    }

    //--If the list resolved, liberate and flag for deletion.
    if(!rCheckList) return;
    void *rLiberatedPtr = rCheckList->GetElementByName(pObjectName);
    rCheckList->SetRandomPointerToThis(rLiberatedPtr);
    rCheckList->LiberateRandomPointerEntry();
    Memory::AddGarbage(rLiberatedPtr, &FreeThis);
}
