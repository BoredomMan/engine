//--Base
#include "AdventureLevel.h"

//--Classes
#include "ActorNotice.h"
#include "TileLayer.h"
#include "TilemapActor.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarLinkedList.h"

//--Definitions
#include "Global.h"
#include "OpenGLMacros.h"
#include "HitDetection.h"

//--Libraries
//--Managers
#include "DisplayManager.h"
#include "EntityManager.h"

//--[Local Definitions]
//#define ALSR_DEBUG
#ifdef ALSR_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

//--[Rendering Functions]
void AdventureLevel::RenderEntities(bool pIsBeforeTiles)
{
    //--Renders the entities in the world.
    DebugPrint("Render entities.\n");
    TilemapActor::xAllowRender = true;

    //--Flag: Set whether or not entities that render before or after tiles should render. After-tile rendering
    //  is used for special effects or entities that have special transparencies.
    TilemapActor::xIsRenderingBeforeTiles = pIsBeforeTiles;

    //--Fast-access pointers.
    SugarLinkedList *rEntityList = EntityManager::Fetch()->GetEntityList();

    //--Iterate.
    RootEntity *rCheckEntity = (RootEntity *)rEntityList->PushIterator();
    while(rCheckEntity)
    {
        //--Right type? Cast and render.
        if(rCheckEntity->IsOfType(POINTER_TYPE_TILEMAPACTOR))
        {
            rCheckEntity->Render();
        }

        //--Next.
        rCheckEntity = (RootEntity *)rEntityList->AutoIterate();
    }
}
void AdventureLevel::RenderTilemap(float pLft, float pTop, float pRgt, float pBot)
{
    //--Render all the layers. It's that easy!
    DebugPrint("Render tile layers.\n");

    //--If lighting is active, don't use the override values by default. Some tiles may use them.
    if(mAreLightsActive)
    {
        uint32_t cShaderHandle = DisplayManager::Fetch()->mLastProgramHandle;
        ShaderUniform1i(cShaderHandle, 0, "uUseOverrideX");
        ShaderUniform1i(cShaderHandle, 0, "uUseOverrideY");
    }

    //--Render.
    TileLayer *rLayer = (TileLayer *)mTileLayers->PushIterator();
    while(rLayer)
    {
        rLayer->RenderRange(pLft, pTop, pRgt, pBot);
        rLayer = (TileLayer *)mTileLayers->AutoIterate();
    }

    //--Tile dislocations.
    DislocatedRenderPackage *rPackage = (DislocatedRenderPackage *)mDislocatedRenderList->PushIterator();
    while(rPackage)
    {
        //--Locate the tile layer in question.
        TileLayer *rCheckLayer = (TileLayer *)mTileLayers->GetElementByName(rPackage->mLayerName);
        if(!rCheckLayer)
        {
            rPackage = (DislocatedRenderPackage *)mDislocatedRenderList->AutoIterate();
            continue;
        }

        //--Layer exists, render dislocated.
        rCheckLayer->RenderDislocated(rPackage->mOrigXStart, rPackage->mOrigYStart, rPackage->mOrigXStart + rPackage->mWidth, rPackage->mOrigYStart + rPackage->mHeight, rPackage->mNewXRender, rPackage->mNewYRender);

        //--Next.
        rPackage = (DislocatedRenderPackage *)mDislocatedRenderList->AutoIterate();
    }
}
void AdventureLevel::RenderDoors()
{
    //--Render doors. All doors are midground objects.
    DebugPrint("Render doors.\n");
    DoorPackage *rDoorPackage = (DoorPackage *)mDoorList->PushIterator();
    while(rDoorPackage)
    {
        //--[Narrow Doors]
        if(!rDoorPackage->mIsWide)
        {
            //--Closed door.
            if(rDoorPackage->rRenderClosed && !rDoorPackage->mIsOpened)
            {
                //--Move up one Y position for space EW doors.
                float tRenderY = rDoorPackage->mY - 16.0f;
                if(rDoorPackage->mIsSpaceEW)
                {
                    tRenderY = tRenderY - 16.0f;
                }

                //--Compute the position.
                float tZPosition = DEPTH_MIDGROUND + (rDoorPackage->mY / TileLayer::cxSizePerTile * DEPTH_PER_TILE) + 0.000050f;
                tZPosition = tZPosition + (DEPTH_PER_TILE * (float)rDoorPackage->mZ * 2.0f);
                glTranslatef(rDoorPackage->mX, tRenderY, tZPosition);

                //--Rendering.
                rDoorPackage->rRenderClosed->Bind();
                glBegin(GL_QUADS);

                    //--Top half.
                    glTexCoord2f(0.0f, 1.0f); glVertex2f( 0.0f,  0.0f);
                    glTexCoord2f(1.0f, 1.0f); glVertex2f(16.0f,  0.0f);
                    glTexCoord2f(1.0f, 0.5f); glVertex2f(16.0f, 16.0f);
                    glTexCoord2f(0.0f, 0.5f); glVertex2f( 0.0f, 16.0f);

                    //--Bottom half.
                    glTexCoord2f(0.0f, 0.5f); glVertex3f( 0.0f, 16.0f, -DEPTH_PER_TILE);
                    glTexCoord2f(1.0f, 0.5f); glVertex3f(16.0f, 16.0f, -DEPTH_PER_TILE);
                    glTexCoord2f(1.0f, 0.0f); glVertex3f(16.0f, 32.0f, -DEPTH_PER_TILE);
                    glTexCoord2f(0.0f, 0.0f); glVertex3f( 0.0f, 32.0f, -DEPTH_PER_TILE);

                //--Finish up.
                glEnd();
                glTranslatef(rDoorPackage->mX * -1.0f, tRenderY * -1.0f, -tZPosition);
            }
            //--Open door.
            else if(rDoorPackage->rRenderOpen && rDoorPackage->mIsOpened)
            {
                //--Compute the position.
                float tZPosition = DEPTH_MIDGROUND + (rDoorPackage->mY / TileLayer::cxSizePerTile * DEPTH_PER_TILE) + 0.000050f;
                tZPosition = tZPosition + (DEPTH_PER_TILE * (float)rDoorPackage->mZ * 2.0f);
                glTranslatef(rDoorPackage->mX, rDoorPackage->mY - 16.0f, tZPosition);

                //--Rendering.
                rDoorPackage->rRenderOpen->Bind();
                glBegin(GL_QUADS);

                    //--Top half.
                    glTexCoord2f(0.0f, 1.0f); glVertex2f( 0.0f,  0.0f);
                    glTexCoord2f(1.0f, 1.0f); glVertex2f(16.0f,  0.0f);
                    glTexCoord2f(1.0f, 0.5f); glVertex2f(16.0f, 16.0f);
                    glTexCoord2f(0.0f, 0.5f); glVertex2f( 0.0f, 16.0f);

                    //--Bottom half.
                    glTexCoord2f(0.0f, 0.5f); glVertex3f( 0.0f, 16.0f, -DEPTH_PER_TILE);
                    glTexCoord2f(1.0f, 0.5f); glVertex3f(16.0f, 16.0f, -DEPTH_PER_TILE);
                    glTexCoord2f(1.0f, 0.0f); glVertex3f(16.0f, 32.0f, -DEPTH_PER_TILE);
                    glTexCoord2f(0.0f, 0.0f); glVertex3f( 0.0f, 32.0f, -DEPTH_PER_TILE);

                //--Finish up.
                glEnd();
                glTranslatef(rDoorPackage->mX * -1.0f, (rDoorPackage->mY - 16.0f) * -1.0f, -tZPosition);
            }
        }
        //--[Wide Door]
        else
        {
            //--Closed door.
            if(rDoorPackage->rRenderClosed && !rDoorPackage->mIsOpened)
            {
                float tZPosition = DEPTH_MIDGROUND + ((rDoorPackage->mY) / TileLayer::cxSizePerTile * DEPTH_PER_TILE);
                tZPosition = tZPosition + (DEPTH_PER_TILE * (float)rDoorPackage->mZ * 2.0f);
                glTranslatef(0.0f, 0.0f, tZPosition);
                rDoorPackage->rRenderClosed->Draw(rDoorPackage->mX, rDoorPackage->mY - 16.0f);
                glTranslatef(0.0f, 0.0f, -tZPosition);
            }
            //--Open door.
            else if(rDoorPackage->rRenderOpen && rDoorPackage->mIsOpened)
            {
                //--Compute the position.
                float tZPosition = DEPTH_MIDGROUND + (rDoorPackage->mY / TileLayer::cxSizePerTile * DEPTH_PER_TILE) + 0.000050f;
                tZPosition = tZPosition + (DEPTH_PER_TILE * (float)rDoorPackage->mZ * 2.0f);
                glTranslatef(rDoorPackage->mX, rDoorPackage->mY - 16.0f, tZPosition);

                //--Rendering.
                rDoorPackage->rRenderOpen->Bind();
                glBegin(GL_QUADS);

                    //--Top half.
                    glTexCoord2f(0.0f, 1.0f); glVertex2f( 0.0f,  0.0f);
                    glTexCoord2f(1.0f, 1.0f); glVertex2f(32.0f,  0.0f);
                    glTexCoord2f(1.0f, 0.5f); glVertex2f(32.0f, 16.0f);
                    glTexCoord2f(0.0f, 0.5f); glVertex2f( 0.0f, 16.0f);

                    //--Bottom half.
                    glTexCoord2f(0.0f, 0.5f); glVertex3f( 0.0f, 16.0f, -DEPTH_PER_TILE);
                    glTexCoord2f(1.0f, 0.5f); glVertex3f(32.0f, 16.0f, -DEPTH_PER_TILE);
                    glTexCoord2f(1.0f, 0.0f); glVertex3f(32.0f, 32.0f, -DEPTH_PER_TILE);
                    glTexCoord2f(0.0f, 0.0f); glVertex3f( 0.0f, 32.0f, -DEPTH_PER_TILE);

                //--Finish up.
                glEnd();
                glTranslatef(rDoorPackage->mX * -1.0f, (rDoorPackage->mY - 16.0f) * -1.0f, -tZPosition);
            }
        }

        rDoorPackage = (DoorPackage *)mDoorList->AutoIterate();
    }
}
void AdventureLevel::RenderClimbables()
{
    //--Render climbables. These are midground objects.
    ClimbablePackage *rClimbPackage = (ClimbablePackage *)mClimbableList->PushIterator();
    while(rClimbPackage)
    {
        //--If this is a rope...
        if(!rClimbPackage->mIsLadder)
        {
            //--Cursors.
            float tXCur = rClimbPackage->mX;
            float tYCur = rClimbPackage->mY - TileLayer::cxSizePerTile;
            float tZPosition = DEPTH_MIDGROUND + (((rClimbPackage->mY / TileLayer::cxSizePerTile) - 1.0f) * DEPTH_PER_TILE) - 0.000049f + (DEPTH_PER_TILE * (float)rClimbPackage->mUpperDepth * 2.0f);

            //--Position to the anchor point, not the top of the entity.
            glTranslatef(tXCur, tYCur, tZPosition);

            //--If not active, render the flat anchor point and stop rendering.
            if(!rClimbPackage->mIsActivated)
            {
                Images.Data.rRopeAnchor->Draw();
            }
            //--If the rope is active, render the anchor and the rest of the rope.
            else
            {
                //--Top. Always renders without a depth bonus so party followers don't render behind it.
                float tUDepth = DEPTH_PER_TILE * (float)rClimbPackage->mUpperDepth * 2.0f;
                glTranslatef(0.0f, 0.0f, -tUDepth);
                Images.Data.rRopeTop->Draw();
                glTranslatef(0.0f, 0.0f, tUDepth);

                //--Cross the inflection point.
                glTranslatef(0.0f, 0.0f, -(DEPTH_PER_TILE * (float)rClimbPackage->mUpperDepth * 2.0f));
                glTranslatef(0.0f, 0.0f,  (DEPTH_PER_TILE * (float)rClimbPackage->mLowerDepth * 2.0f));
                tZPosition = tZPosition - (DEPTH_PER_TILE * (float)rClimbPackage->mUpperDepth * 2.0f);
                tZPosition = tZPosition + (DEPTH_PER_TILE * (float)rClimbPackage->mLowerDepth * 2.0f);

                //--Iterate down.
                for(float i = 0.0f; i < rClimbPackage->mH; i = i + TileLayer::cxSizePerTile)
                {
                    //--Move the cursors.
                    tYCur = tYCur + TileLayer::cxSizePerTile;
                    tZPosition = tZPosition + DEPTH_PER_TILE;
                    glTranslatef(0.0f, TileLayer::cxSizePerTile, DEPTH_PER_TILE);

                    //--Render.
                    Images.Data.rRopeMid->Draw();
                }

                //--Render the bottom-most part.
                tYCur = tYCur + TileLayer::cxSizePerTile;
                tZPosition = tZPosition + DEPTH_PER_TILE;
                glTranslatef(0.0f, TileLayer::cxSizePerTile, DEPTH_PER_TILE);
                Images.Data.rRopeBot->Draw();
            }

            //--Clean.
            glTranslatef(-tXCur, -tYCur, -tZPosition);
        }
        //--If this is a ladder. It also has to be active, inactive ladders render nothing.
        else if(rClimbPackage->mIsLadder && rClimbPackage->mIsActivated)
        {
            //--Cursors.
            float tXCur = rClimbPackage->mX;
            float tYCur = rClimbPackage->mY - TileLayer::cxSizePerTile;
            float tZPosition = DEPTH_MIDGROUND + (((rClimbPackage->mY / TileLayer::cxSizePerTile) - 1.0f) * DEPTH_PER_TILE) - 0.000049f + (DEPTH_PER_TILE * (float)rClimbPackage->mUpperDepth * 2.0f);

            //--Position to the anchor point, not the top of the entity.
            glTranslatef(tXCur, tYCur, tZPosition);

            //--Render.
            Images.Data.rLadderTop->Draw();

            //--Cross the inflection point.
            glTranslatef(0.0f, 0.0f, -(DEPTH_PER_TILE * (float)rClimbPackage->mUpperDepth * 2.0f));
            glTranslatef(0.0f, 0.0f,  (DEPTH_PER_TILE * (float)rClimbPackage->mLowerDepth * 2.0f));
            tZPosition = tZPosition - (DEPTH_PER_TILE * (float)rClimbPackage->mUpperDepth * 2.0f);
            tZPosition = tZPosition + (DEPTH_PER_TILE * (float)rClimbPackage->mLowerDepth * 2.0f);

            //--Iterate down.
            for(float i = 0.0f; i < rClimbPackage->mH; i = i + TileLayer::cxSizePerTile)
            {
                //--Move the cursors.
                tYCur = tYCur + TileLayer::cxSizePerTile;
                tZPosition = tZPosition + DEPTH_PER_TILE;
                glTranslatef(0.0f, TileLayer::cxSizePerTile, DEPTH_PER_TILE);

                //--Render.
                Images.Data.rLadderMid->Draw();
            }

            //--Render the bottom-most part.
            tYCur = tYCur + TileLayer::cxSizePerTile;
            tZPosition = tZPosition + DEPTH_PER_TILE;
            glTranslatef(0.0f, TileLayer::cxSizePerTile, DEPTH_PER_TILE);
            Images.Data.rLadderBot->Draw();

            //--Clean.
            glTranslatef(-tXCur, -tYCur, -tZPosition);
        }

        rClimbPackage = (ClimbablePackage *)mClimbableList->AutoIterate();
    }
}
void AdventureLevel::RenderChests()
{
    //--Render chests. Like doors, they are midground objects.
    DebugPrint("Render chests.\n");
    DoorPackage *rChestPackage = (DoorPackage *)mChestList->PushIterator();
    while(rChestPackage)
    {
        if(rChestPackage->rRenderClosed && !rChestPackage->mIsOpened)
        {
            float tZPosition = DEPTH_MIDGROUND + (rChestPackage->mY / TileLayer::cxSizePerTile * DEPTH_PER_TILE);
            glTranslatef(0.0f, 0.0f, tZPosition);
            rChestPackage->rRenderClosed->Draw(rChestPackage->mX, rChestPackage->mY - 16.0f);
            glTranslatef(0.0f, 0.0f, -tZPosition);
        }
        else if(rChestPackage->rRenderOpen && rChestPackage->mIsOpened)
        {
            //--Compute the position.
            float tZPosition = DEPTH_MIDGROUND + (rChestPackage->mY / TileLayer::cxSizePerTile * DEPTH_PER_TILE);
            glTranslatef(rChestPackage->mX, rChestPackage->mY - 16.0f, tZPosition);

            //--Rendering.
            rChestPackage->rRenderOpen->Bind();
            glBegin(GL_QUADS);

                //--Top half.
                glTexCoord2f(0.0f, 1.0f); glVertex2f( 0.0f,  0.0f);
                glTexCoord2f(1.0f, 1.0f); glVertex2f(16.0f,  0.0f);
                glTexCoord2f(1.0f, 0.5f); glVertex2f(16.0f, 16.0f);
                glTexCoord2f(0.0f, 0.5f); glVertex2f( 0.0f, 16.0f);

                //--Bottom half.
                glTexCoord2f(0.0f, 0.5f); glVertex3f( 0.0f, 16.0f, -DEPTH_PER_TILE);
                glTexCoord2f(1.0f, 0.5f); glVertex3f(16.0f, 16.0f, -DEPTH_PER_TILE);
                glTexCoord2f(1.0f, 0.0f); glVertex3f(16.0f, 32.0f, -DEPTH_PER_TILE);
                glTexCoord2f(0.0f, 0.0f); glVertex3f( 0.0f, 32.0f, -DEPTH_PER_TILE);

            //--Finish up.
            glEnd();
            glTranslatef(rChestPackage->mX * -1.0f, (rChestPackage->mY - 16.0f) * -1.0f, -tZPosition);
        }

        rChestPackage = (DoorPackage *)mChestList->AutoIterate();
    }

    //--Render fake chests. Same property as real chests but they don't open.
    DebugPrint("Render fake chests.\n");
    rChestPackage = (DoorPackage *)mFakeChestList->PushIterator();
    while(rChestPackage)
    {
        if(rChestPackage->rRenderClosed && !rChestPackage->mIsOpened)
        {
            float tZPosition = DEPTH_MIDGROUND + (rChestPackage->mY / TileLayer::cxSizePerTile * DEPTH_PER_TILE);
            glTranslatef(0.0f, 0.0f, tZPosition);
            rChestPackage->rRenderClosed->Draw(rChestPackage->mX, rChestPackage->mY - 16.0f);
            glTranslatef(0.0f, 0.0f, -tZPosition);
        }
        else if(rChestPackage->rRenderOpen && rChestPackage->mIsOpened)
        {
            //--Compute the position.
            float tZPosition = DEPTH_MIDGROUND + (rChestPackage->mY / TileLayer::cxSizePerTile * DEPTH_PER_TILE);
            glTranslatef(rChestPackage->mX, rChestPackage->mY - 16.0f, tZPosition);

            //--Rendering.
            rChestPackage->rRenderOpen->Bind();
            glBegin(GL_QUADS);

                //--Top half.
                glTexCoord2f(0.0f, 1.0f); glVertex2f( 0.0f,  0.0f);
                glTexCoord2f(1.0f, 1.0f); glVertex2f(16.0f,  0.0f);
                glTexCoord2f(1.0f, 0.5f); glVertex2f(16.0f, 16.0f);
                glTexCoord2f(0.0f, 0.5f); glVertex2f( 0.0f, 16.0f);

                //--Bottom half.
                glTexCoord2f(0.0f, 0.5f); glVertex3f( 0.0f, 16.0f, -DEPTH_PER_TILE);
                glTexCoord2f(1.0f, 0.5f); glVertex3f(16.0f, 16.0f, -DEPTH_PER_TILE);
                glTexCoord2f(1.0f, 0.0f); glVertex3f(16.0f, 32.0f, -DEPTH_PER_TILE);
                glTexCoord2f(0.0f, 0.0f); glVertex3f( 0.0f, 32.0f, -DEPTH_PER_TILE);

            //--Finish up.
            glEnd();
            glTranslatef(rChestPackage->mX * -1.0f, (rChestPackage->mY - 16.0f) * -1.0f, -tZPosition);
        }

        rChestPackage = (DoorPackage *)mFakeChestList->AutoIterate();
    }
}
void AdventureLevel::RenderExits()
{
    //--Render exits. Only staircases typically render.
    DebugPrint("Render exits.\n");
    ExitPackage *rPackage = (ExitPackage *)mExitList->PushIterator();
    while(rPackage)
    {
        if(rPackage->rRenderImg)
        {
            glTranslatef(0.0f, 0.0f, -0.900000f);
            rPackage->rRenderImg->Draw(rPackage->mX, rPackage->mY);
            glTranslatef(0.0f, 0.0f,  0.900000f);
        }
        rPackage = (ExitPackage *)mExitList->AutoIterate();
    }
}
void AdventureLevel::RenderSwitches()
{
    //--Render switches.
    DebugPrint("Render switches.\n");
    DoorPackage *rSwitchPackage = (DoorPackage *)mSwitchList->PushIterator();
    while(rSwitchPackage)
    {
        //--Setup.
        SugarBitmap *rRenderImg = NULL;

        //--If open:
        if(rSwitchPackage->mIsOpened)
        {
            rRenderImg = rSwitchPackage->rRenderOpen;
        }
        //--If closed:
        else
        {
            rRenderImg = rSwitchPackage->rRenderClosed;
        }

        //--Render it.
        if(rRenderImg)
        {
            //--Positions.
            float tXPos = rSwitchPackage->mX;
            float tYPos = rSwitchPackage->mY - 16.0f;
            float tZPos = DEPTH_MIDGROUND + (rSwitchPackage->mY / TileLayer::cxSizePerTile * DEPTH_PER_TILE);

            //--Rendering.
            rRenderImg->Bind();
            glTranslatef(tXPos, tYPos, 0.0f);
            glBegin(GL_QUADS);

                //--Top half.
                glTexCoord2f(0.0f, 1.0f); glVertex3f( 0.0f,  0.0f, -0.400000f);
                glTexCoord2f(1.0f, 1.0f); glVertex3f(16.0f,  0.0f, -0.400000f);
                glTexCoord2f(1.0f, 0.5f); glVertex3f(16.0f, 16.0f, -0.400000f);
                glTexCoord2f(0.0f, 0.5f); glVertex3f( 0.0f, 16.0f, -0.400000f);

                //--Bottom half.
                glTexCoord2f(0.0f, 0.5f); glVertex3f( 0.0f, 16.0f, tZPos);
                glTexCoord2f(1.0f, 0.5f); glVertex3f(16.0f, 16.0f, tZPos);
                glTexCoord2f(1.0f, 0.0f); glVertex3f(16.0f, 32.0f, tZPos);
                glTexCoord2f(0.0f, 0.0f); glVertex3f( 0.0f, 32.0f, tZPos);

            //--Finish up.
            glEnd();
            glTranslatef(-tXPos, -tYPos, 0.0f);
        }

        //--Next.
        rSwitchPackage = (DoorPackage *)mSwitchList->AutoIterate();
    }
}
void AdventureLevel::RenderSavePoints()
{
    //--Save points.
    DebugPrint("Render save points.\n");
    SavePointPackage *rSavePackage = (SavePointPackage *)mSavePointList->PushIterator();
    while(rSavePackage)
    {
        //--Setup.
        SugarBitmap *rImage = NULL;
        float tZPos = DEPTH_MIDGROUND + (rSavePackage->mY / TileLayer::cxSizePerTile * DEPTH_PER_TILE);

        //--Modify the X position if it's a bench.
        float tXPos = rSavePackage->mX;
        if(rSavePackage->mIsBench) tXPos = tXPos - (TileLayer::cxSizePerTile * 0.50f);

        //--Inactive.
        if(!rSavePackage->mIsLit)
        {
            rImage = rSavePackage->rUnlitImg;
        }
        //--Active.
        else
        {
            rImage = rSavePackage->rLitImgA;
            if(Global::Shared()->gTicksElapsed % 30 < 15) rImage = rSavePackage->rLitImgB;
        }

        //--Render.
        glTranslatef(0.0f, 0.0f, tZPos);
        if(rImage) rImage->Draw(tXPos, rSavePackage->mY);
        glTranslatef(0.0f, 0.0f, -tZPos);

        //--Next.
        rSavePackage = (SavePointPackage *)mSavePointList->AutoIterate();
    }
}
void AdventureLevel::RenderViewcones()
{
    //--If this flag is set, don't render any viewcones. It may be used during cutscenes.
    if(mHideAllViewcones) return;

    //--If lighting is active, set up the shader for the viewcones. Set to the default mixer.
    GLint cShaderHandle = 0;
    if(mAreLightsActive)
    {
        //--Shader.
        cShaderHandle = DisplayManager::Fetch()->mLastProgramHandle;
        ShaderUniform1i(cShaderHandle, 1, "uIsViewcone");
    }

    //--Texture binding.
    Images.Data.rViewconePixel->Bind();

    //--Modified camera coordinates.

    //--Render the entity viewcones.
    DebugPrint("Render entity viewcones.\n");
    SugarLinkedList *rEntityList = EntityManager::Fetch()->GetEntityList();
    RootEntity *rCheckEntity = (RootEntity *)rEntityList->PushIterator();
    while(rCheckEntity)
    {
        //--Right type? Cast and render.
        if(rCheckEntity->IsOfType(POINTER_TYPE_TILEMAPACTOR))
        {
            //--Get the actor's position. They must have their sight range be within the visible screen area.
            TilemapActor *rActor = (TilemapActor *)rCheckEntity;
            float cActorX = rActor->GetWorldX();
            float cActorY = rActor->GetWorldY();
            float cViewDist = rActor->GetViewDistance();
            TwoDimensionReal cModdedCamera;
            cModdedCamera.Set(mCameraDimensions.mLft - cViewDist, mCameraDimensions.mTop - cViewDist, mCameraDimensions.mRgt + cViewDist, mCameraDimensions.mBot + cViewDist);

            //--Actor is within the camera, render. We allow padding equal to the view distance to avoid the case where
            //  the actor is just offscreen and their viewcone gets hidden.
            if(IsPointWithin2DReal(cActorX, cActorY, cModdedCamera))
            {
                rActor->RenderVisibilityCone(mAreLightsActive, cShaderHandle);
            }
        }

        //--Next.
        rCheckEntity = (RootEntity *)rEntityList->AutoIterate();
    }

    //--Clean up.
    if(mAreLightsActive)
    {
        cShaderHandle = DisplayManager::Fetch()->mLastProgramHandle;
        ShaderUniform1i(cShaderHandle, 0, "uIsViewcone");
    }
}
void AdventureLevel::RenderPerMapAnimations()
{
    //--Renders any one-off animations that are on the map currently. Individual animations can be flagged to not render.
    //  Rendering is blocked during a major animation if that animation happens to be the major one. The others will
    //  render as normal.
    /*
    PerMapAnimation *rAnimation = (PerMapAnimation *)mPerMapAnimationList->PushIterator();
    while(rAnimation)
    {
        //--If the animation is not rendering, ignore it. Also ignore it if the information is illegal.
        if(!rAnimation->mIsRendering || !rAnimation->mrImagePtrs || rAnimation->mTotalFrames < 1)
        {
            rAnimation = (PerMapAnimation *)mPerMapAnimationList->AutoIterate();
            continue;
        }

        //--If this animation happens to be the major animation, don't render it.
        if(mIsMajorAnimationMode && !strcasecmp(mMajorAnimationName, mPerMapAnimationList->GetIteratorName()))
        {
            rAnimation = (PerMapAnimation *)mPerMapAnimationList->AutoIterate();
            continue;
        }

        //--Get information.
        float cTotalSizeX = (float)rAnimation->rImagePtr->GetWidth();

        //--Determine which frame we're playing.
        float tFrame = rAnimation->mTimer / rAnimation->mTicksPerFrame;
        if(tFrame >= rAnimation->mTotalFrames) tFrame = rAnimation->mTotalFrames - 1;
        if(tFrame < 1) tFrame = 0;

        //--Convert to an integer so frames don't scroll.
        tFrame = (int)tFrame;

        //--Compute the coordinates for this frame.
        float cLft = rAnimation->mRenderPosX;
        float cTop = rAnimation->mRenderPosY;
        float cRgt = cLft + rAnimation->mSizePerFrameX;
        float cBot = cTop + rAnimation->rImagePtr->GetHeight();
        float cRenderLft = (rAnimation->mSizePerFrameX *  tFrame)   / cTotalSizeX;
        float cRenderRgt = cRenderLft + (rAnimation->mSizePerFrameX / cTotalSizeX);
        float cZPos = rAnimation->mRenderDepth;

        //--Render it.
        rAnimation->rImagePtr->Bind();
        glBegin(GL_QUADS);
            glTexCoord2f(cRenderLft, 0.0f); glVertex3f(cLft, cTop, cZPos);
            glTexCoord2f(cRenderRgt, 0.0f); glVertex3f(cRgt, cTop, cZPos);
            glTexCoord2f(cRenderRgt, 1.0f); glVertex3f(cRgt, cBot, cZPos);
            glTexCoord2f(cRenderLft, 1.0f); glVertex3f(cLft, cBot, cZPos);
        glEnd();

        //--Next.
        rAnimation = (PerMapAnimation *)mPerMapAnimationList->AutoIterate();
    }*/
}
void AdventureLevel::RenderEntityUILayer()
{
    //--Entities have a "UI Layer" that renders over the entities themselves.
    DebugPrint("Render entities.\n");

    //--Fast-access pointers.
    SugarLinkedList *rEntityList = EntityManager::Fetch()->GetEntityList();

    //--Iterate.
    RootEntity *rCheckEntity = (RootEntity *)rEntityList->PushIterator();
    while(rCheckEntity)
    {
        //--Right type? Cast and render.
        if(rCheckEntity->IsOfType(POINTER_TYPE_TILEMAPACTOR))
        {
            TilemapActor *rActor = (TilemapActor *)rCheckEntity;
            rActor->RenderUI();
        }

        //--Next.
        rCheckEntity = (RootEntity *)rEntityList->AutoIterate();
    }

    //--Render all notices.
    ActorNotice *rNotice = (ActorNotice *)mNotices->PushIterator();
    while(rNotice)
    {
        rNotice->Render();
        rNotice = (ActorNotice *)mNotices->AutoIterate();
    }
}
