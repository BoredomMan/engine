//--Base
#include "AdventureLevel.h"

//--Classes
#include "TilemapActor.h"

//--CoreClasses
#include "SugarLinkedList.h"

//--Definitions
//--Libraries
//--Managers
#include "AudioManager.h"
#include "AudioPackage.h"
#include "DebugManager.h"
#include "LuaManager.h"

//--[Declarations]
char *AdventureLevel::xLayerResolverPath = NULL;
bool AdventureLevel::xIsLayeringMusic = false;
bool AdventureLevel::xIsCombatMaxIntensity = false;
int AdventureLevel::xCurrentlyRunningTracks = 0;
float AdventureLevel::xCurrentIntensity = 0.0f;
float AdventureLevel::xLayerVolumes[MAX_MUSIC_LAYERS][101];
char AdventureLevel::xLayerNames[MAX_MUSIC_LAYERS][STD_MAX_LETTERS];
bool AdventureLevel::xForceZeroLayerVolume = false;
int AdventureLevel::xZeroOffIntensityTicks = 0;
float AdventureLevel::xScriptMandatedIntensity = -1.0f;
bool AdventureLevel::xScriptMandateIntensityNow = false;
float AdventureLevel::xCombatMandatedIntensity = -1.0f;

//--[Functions]
bool AdventureLevel::IsLayeringMusic()
{
    return xIsLayeringMusic;
}
void AdventureLevel::ActivateMusicLayering()
{
    //--If already layering, do nothing.
    if(xIsLayeringMusic || AudioManager::xBlockAllAudio || AudioManager::xBlockMusic) return;

    //--If there is no resolver path, don't do anything.
    if(!xLayerResolverPath) return;

    //--Otherwise, call the layer routing file with the name of the level music. It will do the rest.
    LuaManager::Fetch()->ExecuteLuaFile(xLayerResolverPath, 1, "S", xLevelMusic);

    //--If this flag didn't get set, fail. It means we didn't handle the script correctly.
    if(!xIsLayeringMusic) return;

    //--Now order all the tracks to play.
    AudioManager *rAudioManager = AudioManager::Fetch();
    for(int i = 0; i < xCurrentlyRunningTracks; i ++)
    {
        //--Get the audio package.
        AudioPackage *rLayer = rAudioManager->GetMusicPack(xLayerNames[i]);
        if(!rLayer)
        {
            DebugManager::ForcePrint("Error playing layered track: %s track does not exist.\n", xLayerNames[i]);
        }
        //--It exists. Play it.
        else
        {
            rLayer->Play();
            rLayer->SetVolume(xLayerVolumes[i][0]);
            rLayer->FadeIn(15);
        }
    }
}
void AdventureLevel::DeactivateMusicLayering()
{
    //--Do nothing if already not layering.
    if(!xIsLayeringMusic) return;

    //--Iterate across the musics.
    AudioManager *rAudioManager = AudioManager::Fetch();
    for(int i = 0; i < xCurrentlyRunningTracks; i ++)
    {
        //--Get and check.
        AudioPackage *rLayer = rAudioManager->GetMusicPack(xLayerNames[i]);
        if(!rLayer) continue;

        //--Fade to stop.
        rLayer->FadeOutToStop(30);
    }

    //--Clear flags.
    xIsLayeringMusic = false;
    xCurrentIntensity = 0;
}
void AdventureLevel::SetForceZeroVolumeForLayers(bool pFlag)
{
    //--Do nothing if already not layering.
    if(!xIsLayeringMusic) return;
    xForceZeroLayerVolume = pFlag;
}
void AdventureLevel::SetLowIntensityTicks(int pTicks)
{
    //--Do nothing if already not layering.
    if(!xIsLayeringMusic) return;
    xZeroOffIntensityTicks = pTicks;
}
void AdventureLevel::UpdateMusicLayering()
{
    //--[Documentation and Setup]
    //--Does nothing if not layering.
    if(!xIsLayeringMusic) return;

    //--[Intensity Handling]
    //--First, figure out what the intensity should be.
    float cIncrement = 0.15f;
    float tIdealIntensity = 50.0f;

    //--If the script is mandating an intensity other than -1.0f, use that.
    if(xScriptMandatedIntensity != -1.0f)
    {
        //--Set.
        tIdealIntensity = xScriptMandatedIntensity;
        cIncrement = 0.50f;

        //--If this flag is set, toggle it off and set the intensity immediatelly.
        if(xScriptMandateIntensityNow)
        {
            xScriptMandateIntensityNow = false;
            cIncrement = 100.0f;
        }
    }
    //--If combat is mandating an intensity other than -1.0f, use that.
    else if(xCombatMandatedIntensity != -1.0f)
    {
        tIdealIntensity = xCombatMandatedIntensity;
        cIncrement = 1.00f;
    }
    //--Check the closest enemy. Set intensity based on that.
    else if(xClosestEnemy == 0.0f)
    {
        tIdealIntensity = 100.0f;
    }
    //--Enemy is close. Set intensity to 90. This is the case when an enemy has spotted you, or is otherwise very close.
    else if(xClosestEnemy < 10.0f)
    {
        tIdealIntensity = 90.0f;
    }
    //--Enemy is in the same general area.
    else if(xClosestEnemy < 192.0f)
    {
        tIdealIntensity = 60.0f;
    }
    //--No nearby enemies. Low intensity.
    else
    {
        tIdealIntensity = 30.0f;
    }

    //--If this timer is still set, ignore enemy closeness.
    if(xZeroOffIntensityTicks > 0)
    {
        xZeroOffIntensityTicks --;
        xCurrentIntensity = tIdealIntensity;
    }

    //--Reset the closest enemy flag.
    //fprintf(stderr, "Closest enemy %5.2f\n", xClosestEnemy);
    xClosestEnemy = 10000.0f;

    //--Move towards the ideal.
    if(tIdealIntensity > xCurrentIntensity + cIncrement)
        xCurrentIntensity = xCurrentIntensity + cIncrement;
    else if(tIdealIntensity < xCurrentIntensity - cIncrement)
        xCurrentIntensity = xCurrentIntensity - cIncrement;
    else
        xCurrentIntensity = tIdealIntensity;
    //fprintf(stderr, "Intensity: %5.2f %5.2f\n", xCurrentIntensity, tIdealIntensity);

    //--[Volume Handling]
    //--Get the integer of the intensity and clamp it.
    int tIntensity = (int)xCurrentIntensity;
    if(tIntensity <   0) tIntensity =   0;
    if(tIntensity > 100) tIntensity = 100;

    //--Set volumes accordingly.
    bool tAllAudioPacksFailed = true;
    AudioManager *rAudioManager = AudioManager::Fetch();
    for(int i = 0; i < xCurrentlyRunningTracks; i ++)
    {
        //--Get the audio package.
        AudioPackage *rPackage = rAudioManager->GetMusicPack(xLayerNames[i]);
        if(!rPackage) continue;

        //--If any one audio package exists, set the flag.
        tAllAudioPacksFailed = false;

        //--Special: Force all volumes to zero.
        if(xForceZeroLayerVolume)
        {
            rPackage->SetVolume(0.0f);
        }
        //--Normal case: Set the volume of the audio package to wherever the current intensity mandates it is.
        else
        {
            rPackage->SetVolume(xLayerVolumes[i][tIntensity]);
        }
    }

    //--If all the audio packages failed to resolve, deactivate layering. There's no need to do all this processing
    //  if there's no music to layer!
    if(tAllAudioPacksFailed) DeactivateMusicLayering();
}
