//--[AdventureLevel]
//--Represents a level used in Adventure Mode. Uses most of the basic storage and parsing properties
//  of the TiledLevel it derives from, but with additional lists for objects like doors and exits.

#pragma once

//--[Includes]
#include "Definitions.h"
#include "Structures.h"
#include "TiledLevel.h"
class RayCollisionEngine;

//--[Local Structures]
//--Base.
#include "AdventureLevelStructures.h"

//--[Local Definitions]
//--Level Transition Stuff
#define TRANSITION_HOLD 15

//--Control Locking
#define AL_CONTROL_LOCK_TICKS 25
#define AL_CONTROL_LOCK_PIXELS 85

//--Resting Duration
#define RESTING_TICKS_FADEOUT 30
#define RESTING_TICKS_HOLD 30
#define RESTING_TICKS_FADEIN 30
#define RESTING_TICKS_TOTAL (RESTING_TICKS_FADEOUT + RESTING_TICKS_HOLD + RESTING_TICKS_FADEIN)

//--Script Fading Depth
#define SCRIPT_FADE_UNDER_CHARACTERS 0
#define SCRIPT_FADE_UNDER_UI 1
#define SCRIPT_FADE_OVER_UI 2
#define SCRIPT_FADE_OVER_UI_BLACKOUT_WORLD 3

//--Inflection Codes
#define INFLECTION_NO_CHANGE -1

//--Movement Storage
#define PLAYER_MOVE_STORAGE_TOTAL 81
#define FOLLOWER_SPACING 16

//--Stamina Stuff
#define STAMINA_VIS_TICKS 15
#define STAMINA_MAX 100.0f
#define STAMINA_RUN_LOWER 20.0f
#define STAMINA_RUN_ALLOW 70.0f
#define STAMINA_CONSUME_TICK 0.2f
#define STAMINA_REGEN_TICK 0.5f
#define STAMINA_REGEN_STANDING 3.5f

//--Maximum Music Layers
#define MAX_MUSIC_LAYERS 5 //Must always be at least 1

//--[Local Structures]
typedef struct
{
    float mX, mY;
    float mYSpeed;
    float mTheta, mThetaDelta;
    SugarBitmap *rImage;
    void Initialize()
    {
        mX = 0.0f;
        mY = 0.0f;
        mYSpeed = 0.0f;
        mTheta = 0.0f;
        mThetaDelta = 0.0f;
        rImage = NULL;
    }
}FallingRockPack;

//--[Classes]
class AdventureLevel : public TiledLevel
{
    private:
    //--System
    bool mIsRandomLevel;
    friend class AdventureLevelGenerator;
    int mCannotOpenMenuTimer;
    bool mIsPlayerOnStaircase;
    float mPlayerStartX;
    float mPlayerStartY;
    char *mBasePath;
    bool mRunParallelDuringDialogue;
    bool mRunParallelDuringCutscene;

    //--Minigames
    RunningMinigame *mRunningMinigame;
    KPopDanceGame *mKPopDanceGame;

    //--Ray Collision Engines
    int mRayCollisionEnginesTotal;
    RayCollisionEngine **mRayCollisionEngines; //Should be one for each collision layer.

    //--Per-map animations.
    bool mIsMajorAnimationMode;
    char mMajorAnimationName[STD_MAX_LETTERS];
    SugarLinkedList *mPerMapAnimationList;

    //--Debug Handling
    bool mWasMenuUpLastTick;
    AdventureDebug *mDebugMenu;
    static bool xHasBuiltListing ;
    static char **xListing;
    static int xIntListing[11];

    //--Menu
    bool mIsRestSequence;
    int mRestingTimer;
    AdventureMenu *mAdventureMenu;

    //--Object Information
    SugarLinkedList *mChestList;
    SugarLinkedList *mFakeChestList;
    SugarLinkedList *mDoorList;
    SugarLinkedList *mExitList;
    SugarLinkedList *mInflectionList;
    SugarLinkedList *mClimbableList;
    SugarLinkedList *mLocationList;
    SugarLinkedList *mChargerList;//ExaminePackage
    SugarLinkedList *mPositionList;//PositionPackage
    SugarLinkedList *mInvisibleZoneList;//InvisZone *

    //--Catalyst Tone
    bool mPlayCatalystTone;
    int mCatalystToneCountdown;

    //--Examinables
    char *mExaminableScript;
    SugarLinkedList *mExaminablesList;

    //--Camera
    bool mIsCameraLocked;
    uint32_t mCameraFollowID;
    TwoDimensionReal mCameraDimensions;

    //--Backgrounds
    SugarBitmap *rBackgroundImg;
    float mBackgroundStartX;
    float mBackgroundStartY;
    float mBackgroundScrollX;
    float mBackgroundScrollY;

    //--Foreground/Background Overlays
    int mUnderlaysTotal;
    OverlayPackage *mUnderlayPacks;
    int mOverlaysTotal;
    OverlayPackage *mOverlayPacks;

    //--Control Locking/Handling
    int mControlLockTimer;
    uint32_t mControlEntityID;
    SugarLinkedList *mFollowEntityIDList;
    SugarLinkedList *mControlLockList;
    float mPlayerXPositions[PLAYER_MOVE_STORAGE_TOTAL];
    float mPlayerYPositions[PLAYER_MOVE_STORAGE_TOTAL];
    int mPlayerFacings[PLAYER_MOVE_STORAGE_TOTAL];
    int mPlayerTimers[PLAYER_MOVE_STORAGE_TOTAL];
    int mPlayerDepths[PLAYER_MOVE_STORAGE_TOTAL];
    bool mPlayerRunnings[PLAYER_MOVE_STORAGE_TOTAL];

    //--Stamina
    int mStaminaVisTimer;
    bool mBlockRunningToRegen;
    float mPlayerStamina;
    float mPlayerStaminaMax;

    //--Transition Handling
    bool mIsExitStaircase;
    char *mTransitionDestinationMap;
    char *mTransitionDestinationExit;
    char *mTransitionPostScript;
    int mTransitionDirection;
    float mTransitionPercentage;

    //--Transition Fading
    int mTransitionHoldTimer;
    bool mIsTransitionOut;
    bool mIsTransitionIn;
    int mTransitionTimer;

    //--Script-Controlled Fading
    bool mUseAlternateBlend;
    bool mIsScriptFading;
    bool mScriptFadeHolds;
    int mScriptFadeDepthFlag;
    int mScriptFadeTimer;
    int mScriptFadeTimerMax;
    StarlightColor mStartFadeColor;
    StarlightColor mEndFadeColor;

    //--Zones and Triggers
    char *mTriggerScript;
    SugarLinkedList *mTriggerList;
    SugarLinkedList *mInvisZoneList;

    //--Enemy Handling
    bool mHideAllViewcones;
    SugarLinkedList *mEnemySpawnList;
    static SugarLinkedList *xDestroyedEnemiesList;
    static SugarLinkedList *xMuggedEnemiesList;

    //--Switch Handling
    SugarLinkedList *mSwitchList;

    //--Save Points
    SugarLinkedList *mSavePointList;

    //--Reinforcement Pulsing
    bool mIsPulseMode;
    int mPulseTimer;

    //--Patrol Nodes
    SugarLinkedList *mPatrolNodes;

    //--Camera Handling
    bool mIsPositiveCameraMode;
    int mFirstLock;
    SugarLinkedList *mCameraZoneList;

    //--Screen Shaking and Other Effects
    int mScreenShakeTimer;
    int mShakePeriodLength;
    int mShakePeriodicityRoot;
    int mShakePeriodicityScatter;
    SugarLinkedList *mShakeSoundList; //char *
    int mRockFallingFrequency;
    SugarLinkedList *mFallingRockList; //FallingRockPack *
    SugarLinkedList *mrFallingRockImageList; //SugarBitmap *, ref

    //--Lights
    bool mAreLightsActive;
    SugarLinkedList *mLightsList;
    StarlightColor mAmbientLight;
    static float xLightBoost;
    bool mReuploadStandardLightData;
    bool mReuploadAllLightData;
    float mLastUploadScaleX;
    float mLastUploadScaleY;
    float mLastUploadViewportW;
    float mLastUploadViewportH;

    //--Underwater
    bool mIsUnderwaterMode;
    GLuint mUnderwaterFramebuffer;
    GLuint mUnderwaterTexture;
    GLuint mUnderwaterDepth;

    //--Player Light
    bool mPlayerHasLight;
    bool mPlayerLightIsActive;
    bool mPlayerLightDoesNotDrain;
    int mPlayerLightPower;
    int mPlayerLightPowerMax;
    AdventureLight *mPlayerLightObject;

    //--Objectives Tracker
    SugarLinkedList *mObjectivesList;

    //--Dislocated Render Listing
    SugarLinkedList *mDislocatedRenderList; //DislocatedRenderPackage *, master

    //--Notices System
    SugarLinkedList *mNotices; //ActorNotice *, master

    //--Music Layering
    public:
    static char *xLayerResolverPath;
    static bool xIsLayeringMusic;
    static bool xIsCombatMaxIntensity;
    static int xCurrentlyRunningTracks;
    static float xCurrentIntensity;
    static float xLayerVolumes[MAX_MUSIC_LAYERS][101];
    static char xLayerNames[MAX_MUSIC_LAYERS][STD_MAX_LETTERS];
    static bool xForceZeroLayerVolume;
    static int xZeroOffIntensityTicks;
    static float xScriptMandatedIntensity;
    static bool xScriptMandateIntensityNow;
    static float xCombatMandatedIntensity;

    //--Images
    struct
    {
        bool mIsReady;
        struct
        {
            //--Utility
            SugarBitmap *rViewconePixel;
            SugarFont *rUIFont;

            //--Ladder and Rope
            SugarBitmap *rLadderTop;
            SugarBitmap *rLadderMid;
            SugarBitmap *rLadderBot;
            SugarBitmap *rRopeAnchor;
            SugarBitmap *rRopeTop;
            SugarBitmap *rRopeMid;
            SugarBitmap *rRopeBot;

            //--Stamina Bar
            SugarBitmap *rStaminaBarOver;
            SugarBitmap *rStaminaBarMiddle;
            SugarBitmap *rStaminaBarUnder;
        }Data;
    }Images;

    protected:

    public:
    //--System
    AdventureLevel();
    virtual ~AdventureLevel();

    //--Public Variables
    static char *xRootPath;
    static char *xItemListPath;
    static char *xItemImageListPath;
    static char *xCatalystHandlerPath;
    static bool xTriggerHandledUpdate;
    static bool xIsSwitchUp;
    static bool xExaminationDidSomething;
    static char *xLastSavePoint;
    static char *xLevelMusic;
    static char *xLastIgnorePulse;
    static float xClosestEnemy;
    static int xRemappingsTotal;
    static char **xRemappingsCheck;
    static char **xRemappingsResult;
    static bool xEntitiesDrainStamina;
    static bool xIsPlayerInInvisZone;

    //--Public, non-static: Used for chest opening.
    float mLastChestX;
    float mLastChestY;

    //--Debug
    static int xCountCatalyst[6];

    //--Property Queries
    virtual bool IsOfType(int pType);
    static bool IsWorldStopped();
    static bool IsWorldStoppedFromCutsceneOnly();
    bool IsLevelTransitioning();
    bool AreControlsLocked();
    bool IsPulsing();
    virtual bool GetClipAt(float pX, float pY, float pZ, int pLayer);
    virtual bool GetClipAt(float pX, float pY, float pZ);
    bool GetEntityClipAt(float pX, float pY, float pZ);
    bool GetWanderClipAt(float pX, float pY);
    static bool IsClippedWithin(const uint8_t &sClipVal, const int &sInTileX, const int &sInTileY);
    float GetPlayerStartX();
    float GetPlayerStartY();
    float GetCameraLft();
    float GetCameraTop();
    float GetCameraScale();
    bool IsCharacterInWorldParty(const char *pName);
    TwoDimensionReal GetPatrolNode(const char *pName);
    void GetLocationByName(const char *pName, float &sX, float &sY);
    bool DoesPlayerHaveLightSource();
    static float GetLightBoost();
    bool IsStaminaBarVisible();
    void GetLineCollision(int pCollisionLayer, float pXA, float pYA, float pXB, float pYB, float &sReturnX, float &sReturnY);
    bool IsDoorOpen(const char *pName);
    bool DoesExitExist(const char *pName);
    void GetExaminablePos(const char *pName, int &sLft, int &sTop, int &sRgt, int &sBot);
    bool DoesNoticeExist(void *pPtr);

    //--Manipulators
    virtual void SetName(const char *pName);
    void SetBasePath(const char *pPath);
    void SetControlEntityID(uint32_t pID);
    void AddFollowEntityID(uint32_t pID);
    void RemoveFollowEntityName(const char *pName);
    void AddLockout(const char *pName);
    void RemoveLockout(const char *pName);
    void SetExaminationScript(const char *pPath);
    void AddExaminable(int pX, int pY, const char *pFiringString);
    void AddExaminable(int pX, int pY, int pW, int pH, uint8_t pFlags, const char *pFiringString);
    void AddExaminableTransition(const char *pName, int pX, int pY, int pW, int pH, uint8_t pFlags, const char *pRoomDest, const char *pPosDest, const char *pPlaySound);
    void AddPatrolNode(const char *pName, int pX, int pY, int pW, int pH);
    void AddEnemy(const char *pName, int pX, int pY, const char *pParty, const char *pAppearance, const char *pScene, int pToughness, const char *pPatrolPath, const char *pFollow);
    void AddChest(const char *pName, int pX, int pY, bool pIsFuture, const char *pContents);
    void AddCharger(int pX, int pY, int pW, int pH);
    void SetTransitionDestination(const char *pMapDestination, const char *pExitDestination);
    void SetTransitionPostExec(const char *pScriptPath);
    void SetTransitionPosition(int pDirection, float pPercentageOffset);
    void OpenDoor(const char *pName);
    void CloseDoor(const char *pName);
    void SetSwitchState(const char *pName, bool pIsUp);
    void SetReinforcementPulseMode(bool pFlag);
    void BeginRestSequence();
    void SetBackground(const char *pPath);
    void SetBackgroundPositions(float pX, float pY);
    void SetBackgroundScrolls(float pX, float pY);
    void FlagCatalystTone();
    static void SetLightBoost(float pValue);
    void SetRenderingDisabled(const char *pLayerName, bool pFlag);
    void SetAnimationDisabled(const char *pLayerName, bool pFlag);
    void SetCollision(int pX, int pY, int pZ, int pCollisionValue);
    void RegisterObjective(const char *pName);
    void FlagObjectiveIncomplete(const char *pName);
    void FlagObjectiveComplete(const char *pName);
    void ClearObjectives();
    void RegisterDislocationPack(const char *pName, const char *pLayer, int pXStart, int pYStart, int pWid, int pHei, float pXTarget, float pYTarget);
    void ModifyDislocationPack(const char *pName, float pXTarget, float pYTarget);
    void SetMajorAnimationMode(const char *pAnimationName);
    void AddAnimation(const char *pName, float pX, float pY, float pZ);
    void SetAnimationFromPattern(const char *pName, const char *pPattern, int pFrames);
    void SetAnimationRender(const char *pName, bool pIsRendering);
    void SetAnimationDestinationFrame(const char *pName, float pDestinationFrame);
    void SetAnimationDestinationFrame(const char *pName, float pDestinationFrame, float pFramerate);
    void SetAnimationLoop(const char *pName, float pLoopStartFrame, float pLoopEndFrame, const char *pLoopType, float pFramerateToTarget, float pTicksForWholeLoop);
    void SetAnimationCurrentFrame(const char *pName, float pOverrideFrame);
    void ActivateUnderwaterShader();
    void DeactivateUnderwaterShader();
    void SetParallelCutsceneDuringDialogueFlag(bool pFlag);
    void SetParallelCutsceneDuringCutsceneFlag(bool pFlag);
    void SetScreenShake(int pTicks);
    void SetScreenShakePeriodicity(int pShakeLength, int pTickPeriod, int pScatter);
    void AddShakeSoundEffect(const char *pSoundName);
    void SetRockFallChance(int pChance);
    void AddRockFallImage(const char *pDLPath);
    void SetHideViewcones(bool pFlag);
    void ReceiveRunningMinigame(RunningMinigame *pRunningMinigame);
    void ReceiveKPopMinigame(KPopDanceGame *pMinigame);
    void RegisterNotice(ActorNotice *pNotice);

    //--Core Methods
    TilemapActor *LocatePlayerActor();
    TilemapActor *LocateFollowingActor(int pIndex);
    int GetEffectiveDepth(float pX, float pY);
    bool IsInClimbableZone(float pX, float pY);
    void PulseIgnore(const char *pString);
    void RepositionParty(TilemapActor *pPlayerActor, float pX, float pY);
    bool IsLineClipped(float pX1, float pY1, float pX2, float pY2, int pDepth);

    //--Activation
    bool ActivateAt(int pX, int pY);

    //--Camera Handling
    TwoDimensionReal GetCameraDimensions();
    void SetCameraLocking(bool pFlag);
    void SetCameraFollowTarget(uint32_t pID);
    void SetCameraPosition(float pLft, float pTop);
    void SetCameraPosition(TwoDimensionReal pPosition);
    TwoDimensionReal GetIdealCameraPosition(float pTargetX, float pTargetY);
    void UpdateCameraPosition();

    //--Chase Offsets
    static void CalibrateChaseOffsets(int pFollowIndex, bool pIsRunning, float &sOffsetX, float &sOffsetY);

    //--Debug Handling
    void Debug_WipeFieldEnemies();
    void Debug_RespawnFieldEnemies();
    void Debug_TransitionTo(const char *pMapTarget);
    void Debug_FireCutscene(const char *pCutsceneTarget);

    //--Enemy Handling
    static bool IsEnemyDead(const char *pName);
    static bool IsEnemyMugged(const char *pName);
    void RegisterEnemyPack(SpawnPackage *pPackage);
    static void KillEnemy(const char *pName);
    static void MarkEnemyMugged(const char *pName);
    void KillSpawnedEnemy(const char *pName);
    static void WipeDestroyedEnemies();
    void SpawnEnemies();
    void CheckActorAgainstInvisZones(TilemapActor *pActor);

    //--Layering
    void AllocateForegroundPacks(int pTotal);
    void SetForegroundImage(int pSlot, const char *pPath);
    void SetForegroundOffsets(int pSlot, float pOffsetX, float pOffsetY, float pScalerX, float pScalerY);
    void SetForegroundAutoscroll(int pSlot, float pAutoscrollX, float pAutoscrollY);
    void SetForegroundAlpha(int pSlot, float pTargetAlpha, int pTicks);
    void SetForegroundScaler(int pSlot, float pScale);
    void AllocateBackgroundPacks(int pTotal);
    void SetBackgroundImage(int pSlot, const char *pPath);
    void SetBackgroundOffsets(int pSlot, float pOffsetX, float pOffsetY, float pScalerX, float pScalerY);
    void SetBackgroundAutoscroll(int pSlot, float pAutoscrollX, float pAutoscrollY);
    void SetBackgroundAlpha(int pSlot, float pTargetAlpha, int pTicks);
    void SetBackgroundScaler(int pSlot, float pScale);

    //--Lighting
    bool IsLightingActive();
    void ActivateLighting();
    void DeactivateLighting();
    void SetAmbientLight(float pR, float pG, float pB, float pA);
    void RegisterLight(AdventureLight *pLight);
    void ModifyLightColor(const char *pName, float pR, float pG, float pB, float pA);
    void EnableLight(const char *pName);
    void DisableLight(const char *pName);
    void AttachLightToEntity(const char *pLightName, const char *pEntityName);
    void ActivatePlayerLight();
    void DeactivatePlayerLight();
    void SetPlayerLightPower(int pTicksLeft);
    void SetPlayerLightPowerMax(int pTicksLeft);
    void SetPlayerLightNoDrain(bool pFlag);
    void UpdateEntityLightPositions();
    void RenderLights();

    //--Music Layering
    static bool IsLayeringMusic();
    static void ActivateMusicLayering();
    static void DeactivateMusicLayering();
    static void SetForceZeroVolumeForLayers(bool pFlag);
    static void SetLowIntensityTicks(int pTicks);
    static void UpdateMusicLayering();

    //--Object Parsing
    void RegisterStaircase(const char *pName, float pX, float pY, float pW, float pH, const char *pDir, const char *pExitDest, const char *pMapDest);
    virtual void ParseObjectData();
    void HandleObject(ObjectInfoPack *pPack);
    void RemoveObject(const char *pObjectType, const char *pObjectName);
    void SetActorToPositionPack(TilemapActor *pActor, const char *pPositionPack);
    void ResetActorGraphics();

    //--Patrol Nodes
    void RenderPatrolNodes();

    //--Saving
    void WriteToBuffer(SugarAutoBuffer *pBuffer);
    void ReadFromFile(VirtualFile *fInfile);

    //--Script Fading
    void SetAlternateBlending(bool pFlag);
    void ActivateScriptFade(int pTicks, StarlightColor pStartColor, StarlightColor pEndColor, int pDepthFlag, bool pHoldsOnComplete);
    void UpdateScriptFade();
    void RenderScriptFade(int pAtDepth);

    //--Transitions
    bool HandleTransition();
    void HandleTransitionToRandom();
    void RepositionActorToExit(const char *pExitName, int pDirection, float pPercentage, TilemapActor *pActor);

    //--Triggers
    void SetTriggerScript(const char *pPath);
    bool CheckActorAgainstTriggers(TilemapActor *pActor);

    private:
    //--Private Core Methods
    bool CheckActorAgainstExits(TilemapActor *pActor);
    virtual void CompileAfterParse();

    public:
    //--Update
    virtual void Update();
    void MovePartyMembers();
    bool AutofoldPartyMembers();

    //--File I/O
    //--Drawing
    virtual void AddToRenderList(SugarLinkedList *pRenderList);
    virtual void Render();
    void RenderEntities(bool pIsBeforeTiles);
    void RenderTilemap(float pLft, float pTop, float pRgt, float pBot);
    void RenderDoors();
    void RenderClimbables();
    void RenderChests();
    void RenderExits();
    void RenderSwitches();
    void RenderSavePoints();
    void RenderViewcones();
    void RenderPerMapAnimations();
    void RenderEntityUILayer();

    //--Pointer Routing
    AdventureMenu *GetMenu();
    KPopDanceGame *GetDanceMinigame();

    //--Static Functions
    static AdventureLevel *Fetch();
    static void SetMusic(const char *pName, bool pSlowly);

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_AL_Create(lua_State *L);
int Hook_AL_ParseSLF(lua_State *L);
int Hook_AL_GetProperty(lua_State *L);
int Hook_AL_SetProperty(lua_State *L);
int Hook_AL_BeginTransitionTo(lua_State *L);
int Hook_AL_RemoveObject(lua_State *L);
int Hook_AL_PulseIgnore(lua_State *L);
int Hook_AL_CreateObject(lua_State *L);
