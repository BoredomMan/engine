//--[GalleryMenu]
//--Specially designed menu explicitly for showing off images that are ostensibly from the gallery. Does
//  not inherit from SugarMenu and has its own special code sections since it'd take a lot of effort to
//  make a FlexMenu do this and do it well.

#pragma once

//--[Includes]
#include "Definitions.h"
#include "Structures.h"
#include "RootObject.h"

//--[Local Structures]
typedef struct GalleryEntry
{
    char *mDisplayTitle;
    char *mAccompanyText;
    SugarBitmap *rImage;
    static void DeleteThis(void *pPtr)
    {
        GalleryEntry *rEntry = (GalleryEntry *)pPtr;
        free(rEntry->mDisplayTitle);
        free(rEntry->mAccompanyText);
        free(rEntry);
    }
}GalleryEntry;

//--[Local Definitions]
//--[Classes]
class GalleryMenu : public RootObject
{
    private:
    //--System
    //--Sizing
    float cTextSize;
    float cButtonIndentX;
    float cButtonIndentY;
    float cButtonSizeX;
    float cButtonSizeY;
    float cButtonSpaceY;

    //--Entries
    int mSelectedEntry;
    SugarLinkedList *mEntryList;

    //--Associated Text
    char mAssociatedText[10][256];

    //--Images
    struct
    {
        bool mIsReady;
        struct
        {
            SugarBitmap *rBorderCard;
            SugarFont *rUIFont;
        }Data;
    }Images;

    protected:

    public:
    //--System
    GalleryMenu();
    virtual ~GalleryMenu();
    void Construct();

    //--Public Variables
    //--Property Queries
    //--Manipulators
    void ClearEntries();
    void AddEntry(const char *pTitle, const char *pText, const char *pImagePath);
    void SetSelectedEntry(int pIndex);

    //--Core Methods
    private:
    //--Private Core Methods
    public:
    //--Update
    void Update();

    //--File I/O
    //--Drawing
    void Render();
    void RenderAccompanyText(const char *pText, float pYPosition);

    //--Pointer Routing
    //--Static Functions
    static GalleryMenu *Fetch();

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_GM_Show(lua_State *L);
int Hook_GM_Hide(lua_State *L);
int Hook_GM_Clear(lua_State *L);
int Hook_GM_AddEntry(lua_State *L);
