//--Base
#include "VisualLevel.h"

//--Classes
#include "Actor.h"
#include "WADFile.h"
#include "VisualRoom.h"
#include "ZoneEditor.h"
#include "ZoneNode.h"

//--CoreClasses
#include "SugarBitmap.h"

//--Definitions
#include "EasingFunctions.h"
#include "HitDetection.h"

//--GUI
//--Libraries
//--Managers
#include "CameraManager.h"
#include "ControlManager.h"
#include "DebugManager.h"
#include "EntityManager.h"

//--When the VisualLevel is not using the NodeEditor and not allowing Free Movement, it is in
//  "Node Movement" mode, wherein the player has their position controlled by these functions.
//  These also control turning to face different directions.

//--[Functions]
//#define VL_NODEMOVE_DEBUG
#ifdef VL_NODEMOVE_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(true, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

//--[Flags]
#define DEBUG_NODE_MOVEMENT_FLAG false

void VisualLevel::FlagTeleportation()
{
    //--Marks that the player's last movement action was a teleportation. This is done instead of the StartMovementTo() action.
    //  Teleportation of this manner refers to in-game teleportation, NOT teleportation between auto-transition nodes.
    Actor *rPlayerEntity = EntityManager::Fetch()->GetLastPlayerEntity();
    if(!rPlayerEntity) return;

    //--The player's location in terms of room needs to be resolved. It should be set *before* this is called!
    VisualRoom *rRoom = (VisualRoom *)rPlayerEntity->GetCurrentRoom();
    if(!rRoom || !rRoom->IsOfType(POINTER_TYPE_VISUAL_ROOM)) return;

    //--Get the position of the target room's node.
    ZoneNode *rRoomNode = GetNodeByID(rRoom->GetAssociatedNodeID());
    if(!rRoomNode) return;

    //--Everything looks good, set flags.
    mIsTeleporting = true;
    mTeleportTimer = 0;
    mTeleportStart.Set(mPlayerX, mPlayerY, mPlayerZ);
    mTeleportEnd.Set(rRoomNode->GetX(), -rRoomNode->GetY(), rRoomNode->GetZ() - PLAYER_HEIGHT);

    //--Other movement flags.
    mPlayerNode = rRoom->GetAssociatedNodeID();
    mRefreshClickPoints = 2;
}

//--[Entry Function]
void VisualLevel::HandleNodeMovementUpdate()
{
    //--[Documentation]
    //--Called from VisualLevel's Update(), handles input when in NodeMovement mode (which is most gameplay).
    //  First, check if we're undergoing a turn or an automatic mode (in that order, turn gets priority).
    DebugPush(DEBUG_NODE_MOVEMENT_FLAG, "VisualLevel:HandleNodeMovementUpdate - Begin.\n");

    //--[Setup]
    //--Camera position.
    float tXRot, tYRot, tZRot;
    SugarCamera3D *rActiveCamera = CameraManager::FetchActiveCamera3D();
    rActiveCamera->Get3DRotation(tXRot, tYRot, tZRot);

    //--Reset the player's position in case it wasn't set yet.
    Actor *rPlayerEntity = EntityManager::Fetch()->GetLastPlayerEntity();
    if(rPlayerEntity) rPlayerEntity->SetWorldPosition(-mPlayerX, -mPlayerY, mPlayerZ);

    //--Constants.
    float cClimbRange = 12.0f;
    const float cGravity = -0.50f * (mPlayerMoveSpeed / PLAYER_BASE_MOVE_SPEED);

    //--[Teleportation]
    //--If flagged, the player's position will teleport to the target location. Presently this just
    //  involves moving the viewpoint without rotating it. This also locks out controls.
    if(mIsTeleporting)
    {
        //--Timer.
        mTeleportTimer ++;

        //--Completion Rate.
        float tPercent = (float)mTeleportTimer / 60.0f;
        if(tPercent < 1.0f)
        {
            //--Compute the position.
            mPlayerX = mTeleportStart.mX + ((mTeleportEnd.mX - mTeleportStart.mX) * tPercent);
            mPlayerY = mTeleportStart.mY + ((mTeleportEnd.mY - mTeleportStart.mY) * tPercent);
            mPlayerZ = mTeleportStart.mZ + ((mTeleportEnd.mZ - mTeleportStart.mZ) * tPercent);
        }
        //--Done, finish teleportation.
        else
        {
            //--Set to end position.
            mIsTeleporting = false;
            mPlayerX = mTeleportEnd.mX;
            mPlayerY = mTeleportEnd.mY;
            mPlayerZ = mTeleportEnd.mZ;
        }

        //--Set the camera's position accordingly.
        rActiveCamera->SetPosition(-mPlayerX, -mPlayerY, -(mPlayerZ + PLAYER_HEIGHT));

        //--Finish up.
        DebugPop("VisualLevel:HandleNodeMovementUpdate - Done, teleportation handled movement.\n");
        return;
    }

    //--[Height Checking]
    //--The player may reposition at any time, so we check the height and set it as necessary.
    DebugPrint("Checking player height.\n");
    float tExpectedHeight = PLAYER_HEIGHT * -1.0f;
    float tExpectedHeightUp = tExpectedHeight + cClimbRange;
    if(mLocalWADFile)
    {
        tExpectedHeight = mLocalWADFile->GetHeightAtPoint(mPlayerX, mPlayerY, mPlayerZ);
        tExpectedHeightUp = mLocalWADFile->GetHeightAtPoint(mPlayerX, mPlayerY, mPlayerZ + cClimbRange);
    }

    //--Move up. This is always instant, since we're assumed to be climbing a step.
    DebugPrint("Handling gravity/climbing cases.\n");
    if(tExpectedHeight > mPlayerZ || tExpectedHeightUp > mPlayerZ)
    {
        mIsPlayerOnGround = true;
        mPlayerZSpeed = 0.0f;
        mPlayerZ = tExpectedHeight;
        if(tExpectedHeightUp > tExpectedHeight) mPlayerZ = tExpectedHeightUp;
    }
    //--Move down. This follows gravity.
    else if(tExpectedHeight < mPlayerZ + mPlayerZSpeed)
    {
        //--Check the downstep case. If we are moving down 12 or fewer units, and have just exited the ground....
        if(mIsPlayerOnGround && tExpectedHeight >= mPlayerZ - cClimbRange)
        {
            mIsPlayerOnGround = true;
            mPlayerZSpeed = 0.0f;
            mPlayerZ = tExpectedHeight;
        }
        //--Falling case.
        else
        {
            mIsPlayerOnGround = false;
            mPlayerZSpeed = mPlayerZSpeed + cGravity;
            mPlayerZ = mPlayerZ + mPlayerZSpeed;
        }
    }
    //--Close enough, match position exactly.
    else
    {
        mIsPlayerOnGround = true;
        mPlayerZSpeed = 0.0f;
        mPlayerZ = tExpectedHeight;
    }

    //--[Update Routing]
    //--See if the turning support function should handle the update.
    DebugPrint("Running update.\n");
    if(mIsTurning)
    {
        //--Debug.
        DebugPrint("Player is turning.\n");
        float tXPos, tYPos, tZPos;
        rActiveCamera->Get3DPosition(tXPos, tYPos, tZPos);

        //--Navigation alphas decrease to zero when turning.
        for(int i = 0; i < NAV_ALPHAS_TOTAL; i ++)
        {
            mNavigationAlphas[i] = mNavigationAlphas[i] - NAV_ALPHA_DELTA;
            if(mNavigationAlphas[i] < 0.0f) mNavigationAlphas[i] = 0.0f;
        }

        //--This function returns true when the turn is done. It also changes the mPlayerFacing value
        //  internally after the first pass.
        if(TurnToFace(mPlayerNode, mIsTurningRight, mPlayerFacing))
        {
            mIsTurning = false;
            mRefreshClickPoints = 2;
        }

        //--Wipe the click points.
        mRefreshClickPoints = 2;
        memset(mClickPoints, 0, sizeof(TwoDimensionReal) * VL_ENTITIES_PER_SCREEN);

        //--Debug.
        DebugPop("VisualLevel:HandleNodeMovementUpdate - Complete, turning handled update.\n");
        return;
    }
    //--Moving to a node.
    else if(mMoveToNodeID)
    {
        //--Debug.
        DebugPrint("Player is moving to node %i.\n", mMoveToNodeID);

        //--Wipe the click points.
        mRefreshClickPoints = 2;
        memset(mClickPoints, 0, sizeof(TwoDimensionReal) * VL_ENTITIES_PER_SCREEN);

        //--Navigation alphas decrease to zero when moving.
        for(int i = 0; i < NAV_ALPHAS_TOTAL; i ++)
        {
            mNavigationAlphas[i] = mNavigationAlphas[i] - NAV_ALPHA_DELTA;
            if(mNavigationAlphas[i] < 0.0f) mNavigationAlphas[i] = 0.0f;
        }

        //--Function returns true when it's done.
        DebugPrint("Running AutoMoveTo().\n");
        if(AutoMoveTo(mMoveToNodeID))
        {
            //--Set.
            DebugPrint("Automatic move has completed, running handler.\n");
            uint32_t tPreviousNode = mPlayerNode;
            mPlayerNode = mMoveToNodeID;
            mMoveToNodeID = 0;
            mRefreshClickPoints = 2;

            //--Refresh camera info.
            DebugPrint("Refreshing camera info.\n");
            rActiveCamera->Get3DRotation(tXRot, tYRot, tZRot);

            //--Node must exist. If it doesn't, that's an error.
            DebugPrint("Checking target node for existence.\n");
            ZoneNode *rPlayerNode = mZoneEditor->GetNode(mPlayerNode);
            if(!rPlayerNode)
            {
                DebugPop("VisualLevel:HandleNodeMovementUpdate - Complete, finished moving, player had no node destination.\n");
                return;
            }

            //--Does the new node have an auto-transition? Check.
            DebugPrint("Checking for automatic transitions.\n");
            AutoTransitionInfo *rCheckInfo = rPlayerNode->GetAutoTransitionFrom(tPreviousNode);
            if(rCheckInfo)
            {
                //--Set.
                StartMovementTo(rCheckInfo->mNodeDestination);

                //--Set this flag to 100, so we move at full speed without slowing.
                mTicksMovingSoFar = 100;

                //--Hey! Is this auto-transition a teleport? If it is, move there instantly.
                if(rCheckInfo->mIsTeleport)
                {
                    //--Consider us to be at the destination node instead of the one we're at right now.
                    mPlayerNode = rCheckInfo->mNodeDestination;
                    ZoneNode *rPlayerNode = mZoneEditor->GetNode(mPlayerNode);
                    if(rPlayerNode)
                    {
                        mPlayerX = rPlayerNode->GetX();
                        mPlayerY = -rPlayerNode->GetY();
                        rActiveCamera->SetPosition(-mPlayerX, -mPlayerY, -(mPlayerZ + PLAYER_HEIGHT));

                        //--Update the player entity's position.
                        Actor *rPlayerEntity = EntityManager::Fetch()->GetLastPlayerEntity();
                        if(rPlayerEntity) rPlayerEntity->SetWorldPosition(-mPlayerX, -mPlayerY, mPlayerZ);
                    }

                    //--Issue a turn order.
                    mProceedImmediately = true;
                    mLookingTimer = 100;
                    mLookingFlag = LOOK_PERFORM_ENDING;

                    //--Re-run the routine.
                    DebugPop("VisualLevel:HandleNodeMovementUpdate - Complete, teleported. Rerunning to handle teleport.\n");
                    return;
                }

                //--Debug.
                DebugPop("VisualLevel:HandleNodeMovementUpdate - Complete, finished moving and used an auto-transition.\n");
                return;
            }

            //--No automatic transition, so set to face the facing nearest to the one we're currently at.
            DebugPrint("Checking for a destination facing.\n");
            FacingInfo *rNearestFacing = rPlayerNode->GetNearestFacingByAngle(tZRot);
            if(!rNearestFacing)
            {
                DebugPop("VisualLevel:HandleNodeMovementUpdate - Complete, finished moving but had no facing.\n");
                return;
            }

            //--Recompute the entity we should be facing.
            mEntitySwitchTimer = 0;
            mStoredEntityCount = CountVisibleEntities();
            mSelected3DEntity = mStoredEntityCount / 2;
            mSelected3DEntityPrevious = mSelected3DEntity;

            //--Set. The camera will not rotate if the manual rotation flag is set.
            DebugPrint("Setting facing.\n");
            mPlayerFacing = rPlayerNode->GetSlotOfFacing(rNearestFacing);
            if(!xRenderEntitiesInWorldRotation) rActiveCamera->SetRotation(tXRot, tYRot, rNearestFacing->mZRotation);
        }
        else
        {
            mRecheckFacing = false;
        }

        //--Debug.
        DebugPop("VisualLevel:HandleNodeMovementUpdate - Complete, moving handled update.\n");
        return;
    }

    //--[Player Input]
    //--If neither of the above cases is true, handle player input.
    DebugPrint("Handling player input.\n");
    int tMouseX, tMouseY, tMouseZ;
    ControlManager *rControlManager = ControlManager::Fetch();
    rControlManager->GetMouseCoords(tMouseX, tMouseY, tMouseZ);

    //--Get the node the player is currently at.
    DebugPrint("Checking player node %i.\n", mPlayerNode);
    ZoneNode *rPlayerNode = mZoneEditor->GetNode(mPlayerNode);
    if(!rPlayerNode)
    {
        DebugPop("VisualLevel:HandleNodeMovementUpdate - Complete, no ZoneEditor node found.\n");
        return;
    }

    //--Set the alphas for the navigation GUI elements.
    DebugPrint("Setting navigation alphas.\n");
    for(int i = 0; i < NAV_ALPHAS_TOTAL; i ++)
    {
        //--When not moving, the navigation alphas are based on the location of the mouse.
        float tIdealAlpha = (150.0f - (GetPlanarDistance(cComparisonPoints[i*2+0], cComparisonPoints[i*2+1], tMouseX, tMouseY) - 65)) / 150.0f;

        //--When a button is fading back in from zero (where it gets set during transitions), fade it in considerably slower unless
        //  the mouse happens to be over it.
        if(mNavigationAlphas[i] < NAV_ALPHA_CLAMP_LOWER && tIdealAlpha < NAV_ALPHA_CLAMP_LOWER)
        {
            mNavigationAlphas[i] = mNavigationAlphas[i] + NAV_ALPHA_ALT_DELTA;
        }
        //--Move up in alpha.
        else if(tIdealAlpha >= mNavigationAlphas[i] + NAV_ALPHA_DELTA)
        {
            mNavigationAlphas[i] = mNavigationAlphas[i] + NAV_ALPHA_DELTA;
            if(mNavigationAlphas[i] > NAV_ALPHA_CLAMP_UPPER) mNavigationAlphas[i] = NAV_ALPHA_CLAMP_UPPER;
        }
        //--Move down in alpha.
        else if(tIdealAlpha <= mNavigationAlphas[i] - NAV_ALPHA_DELTA)
        {
            mNavigationAlphas[i] = mNavigationAlphas[i] - NAV_ALPHA_DELTA;
            if(mNavigationAlphas[i] < NAV_ALPHA_CLAMP_LOWER) mNavigationAlphas[i] = NAV_ALPHA_CLAMP_LOWER;
        }
        //--Close case. Set to end.
        else
        {
            mNavigationAlphas[i] = tIdealAlpha;
        }
    }

    //--If the node the player is at has exactly one facing, disable the navigation for left and right turns.
    DebugPrint("Clamping navigation alphas.\n");
    mNavigationAlphas[1] = 0.0f;
    if(rPlayerNode->GetFacingsTotal() <= 1)
    {
        mNavigationAlphas[2] = 0.0f;
        mNavigationAlphas[3] = 0.0f;
    }

    //--If the facing in question has no destination, the 0th alpha is forced to 0.
    DebugPrint("Resetting navigation alphas in case of no facing.\n");
    FacingInfo *rFacingInfo = rPlayerNode->GetNearestFacingByAngle(tZRot);
    if(rFacingInfo && rFacingInfo->mNodeDestination == 0)
    {
        mNavigationAlphas[0] = 0.0f;
    }

    //--If the player is unable to move, then all the navigation alphas immediately go to 0.
    DebugPrint("Resetting navigation alphas in case of player AI override.\n");
    Actor *rPlayerCharacter = EntityManager::Fetch()->GetLastPlayerEntity();
    if(rPlayerCharacter && (rPlayerCharacter->IsControlled() || rPlayerCharacter->IsStunned()))
    {
        mNavigationAlphas[0] = 0.0f;
        mNavigationAlphas[1] = 0.0f;
        mNavigationAlphas[2] = 0.0f;
        mNavigationAlphas[3] = 0.0f;
    }

    //--[Left Clicking]
    //--If the player clicks, navigate!
    if(rControlManager->IsFirstPress("MouseLft"))
    {
        //--Debug.
        DebugPrint("Handling player left click.\n");

        //--UI. Clicking the console show/hide button.
        if(mConsoleOffsetDelta == 0)
        {
            if(mConsoleOffsetTimer == 0 && IsPointWithin(tMouseX, tMouseY, ConsolePane.cLft + Images.Data.rSkipTurnBtn->GetWidth(), ConsolePane.cTop - 16.0f, ConsolePane.cLft + 32.0f + Images.Data.rSkipTurnBtn->GetWidth(), ConsolePane.cTop))
            {
                mConsoleOffsetDelta = 1;
                return;
            }
            else if(mConsoleOffsetTimer == VL_CONSOLE_SLIDE_TICKS && IsPointWithin(tMouseX, tMouseY, ConsolePane.cLft + Images.Data.rSkipTurnBtn->GetWidth(), ConsolePane.cTop - 16.0f + mConsoleOffsetY, ConsolePane.cLft + 32.0f + Images.Data.rSkipTurnBtn->GetWidth(), ConsolePane.cTop + mConsoleOffsetY))
            {
                mConsoleOffsetDelta = -1;
                return;
            }
        }

        //--Entities, check which entity is clicked on.
        if(!mRefreshClickPoints)
        {
            //--Loop through each entity.
            for(int i = 0; i < VL_ENTITIES_PER_SCREEN; i ++)
            {
                //--Check for a click.
                if(!IsPointWithin2DReal(tMouseX, tMouseY, mClickPoints[i]) || mClickPoints[i].GetHeight() < 1.0f) continue;

                //--[Normal Case]
                //--Switches the 3D entity if it wasn't clicked on, or opens the menu if it was.
                if(!xRenderEntitiesInWorldRotation)
                {
                    //--If this is the center entity, we open a context menu on them.
                    if(i == VL_ENTITIES_PER_SCREEN / 2)
                    {
                        ShowContextMenuAround(mSelected3DEntity);
                        SetContextPositionAround(tMouseX, tMouseY);
                    }
                    //--Move a slot to the left.
                    else if(i < VL_ENTITIES_PER_SCREEN / 2)
                    {
                        mRefreshClickPoints = 2;
                        if(mSelected3DEntity > 0)
                        {
                            mSelected3DEntityPrevious = mSelected3DEntity;
                            mSelected3DEntity --;
                            mSlotsMoving = -1;
                            mEntitySwitchTimer = -VL_ENTITY_SLIDE_TICKS;
                        }
                    }
                    //--Move a slot to the right.
                    else if(i > VL_ENTITIES_PER_SCREEN / 2)
                    {
                        mRefreshClickPoints = 2;
                        if(mSelected3DEntity < mStoredEntityCount - 1)
                        {
                            mSelected3DEntityPrevious = mSelected3DEntity;
                            mSelected3DEntity ++;
                            mSlotsMoving = 1;
                            mEntitySwitchTimer = VL_ENTITY_SLIDE_TICKS;
                        }
                    }
                    break;
                }
                //--[3D Rotation Case]
                //--Opens the menu for any entity, 3D selected entity is not used.
                else
                {
                    ShowContextMenuAround(i);
                    SetContextPositionAround(tMouseX, tMouseY);
                    break;
                }
            }
        }
        //--Reset these variables.
        else if(!xRenderEntitiesInWorldRotation)
        {
            mSlotsMoving = 0;
            memset(mClickPoints, 0, sizeof(TwoDimensionReal) * VL_ENTITIES_PER_SCREEN);
        }
    }

    //--[Right Clicking]
    //--Used for mouselook. Only the X is respected.
    if(rControlManager->IsDown("MouseRgt"))
    {
        //--Move.
        tZRot = tZRot + ((mOldMouseX - (float)tMouseX) * 0.75f);
        rActiveCamera->SetRotation(tXRot, tYRot, tZRot);
    }

    //--Store the mouse position for later.
    mOldMouseX = tMouseX;

    //--[Hotkeys]
    //--Use the WASD pattern. This is done when we're navigating using facings.
    if(!xRenderEntitiesInWorldRotation)
    {
        //--Forward. Move to whatever room we're facing.
        if((rControlManager->IsFirstPress("Up") && mNavigationAlphas[0] > 0.0f) || mProceedImmediately)
        {
            //--Debug.
            DebugPrint("Handling player keypress Up.\n");

            //--Set the auto-move to move to the node at this facing.
            FacingInfo *rFacingInfo = rPlayerNode->GetNearestFacingByAngle(tZRot);
            if(rFacingInfo)
            {
                //--Get the room associated with the target. Make sure we can actually move to it.
                uint32_t tFinalRoom = VisualRoom::FollowIDTrail(mPlayerNode, rFacingInfo->mNodeDestination, this);
                PandemoniumRoom *rAssociatedRoom = GetRoomByNodeID(tFinalRoom);
                if(rAssociatedRoom && rPlayerEntity)
                {
                    //--Move it.
                    if(rPlayerEntity->CanChangeRooms(rAssociatedRoom->GetName()))
                    {
                        StartMovementTo(rFacingInfo->mNodeDestination);
                    }
                    //--Error.
                    else
                    {
                        rPlayerEntity->AppendNoMoveErrorToConsole(rAssociatedRoom->GetName());
                    }
                }
            }

            //--Set this to keep rotation fast.
            if(mProceedImmediately)
            {
                mTicksMovingSoFar = 100;
                mProceedImmediately = false;
            }
        }
        //--Turn left.
        else if(rControlManager->IsFirstPress("Left") && mNavigationAlphas[2] > 0.0f)
        {
            //--Debug.
            DebugPrint("Handling player keypress Left.\n");

            //--Navigation: Turn the camera left.
            mIsTurning = true;
            mIsTurningRight = true;
            mLookingTimer = 0;
        }
        //--Turn right.
        else if(rControlManager->IsFirstPress("Right") && mNavigationAlphas[3] > 0.0f)
        {
            //--Debug.
            DebugPrint("Handling player keypress Right.\n");

            //--Navigation: Turn the camera right.
            mIsTurning = true;
            mIsTurningRight = false;
            mLookingTimer = 0;
        }
    }
    //--Use the WASD pattern, but allow incremental turning. Forward doesn't do anything anymore, unless
    //  flagged to proceed by an auto-transition.
    else
    {
        //--Forward. Move to whatever room we're facing.
        if((rControlManager->IsFirstPress("Up") && mNavigationAlphas[0] > 0.0f) || mProceedImmediately)
        {
            //--Debug.
            DebugPrint("Handling automatic forward movement.\n");

            //--Set the auto-move to move to the node at this facing.
            FacingInfo *rFacingInfo = rPlayerNode->GetNearestFacingByAngle(tZRot);
            if(rFacingInfo)
            {
                //--Close enough, move immediately.
                if(fabsf(rFacingInfo->mZRotation - tZRot) < 5.0f)
                {
                    //--Get the room associated with the target. Make sure we can actually move to it.
                    uint32_t tFinalRoom = VisualRoom::FollowIDTrail(mPlayerNode, rFacingInfo->mNodeDestination, this);
                    PandemoniumRoom *rAssociatedRoom = GetRoomByNodeID(tFinalRoom);
                    if(rAssociatedRoom && rPlayerEntity)
                    {
                        //--Move it.
                        if(rPlayerEntity->CanChangeRooms(rAssociatedRoom->GetName()))
                        {
                            StartMovementTo(rFacingInfo->mNodeDestination);
                        }
                        //--Error.
                        else
                        {
                            rPlayerEntity->AppendNoMoveErrorToConsole(rAssociatedRoom->GetName());
                        }
                    }
                }
                //--Turn to face it, then proceed.
                else
                {
                    //--Get the room associated with the target. Make sure we can actually move to it.
                    uint32_t tFinalRoom = VisualRoom::FollowIDTrail(mPlayerNode, rFacingInfo->mNodeDestination, this);
                    PandemoniumRoom *rAssociatedRoom = GetRoomByNodeID(tFinalRoom);
                    if(rAssociatedRoom && rPlayerEntity)
                    {
                        //--Move it.
                        if(rPlayerEntity->CanChangeRooms(rAssociatedRoom->GetName()))
                        {
                            mPlayerFacing = -1;
                            StartMovementTo(rFacingInfo->mNodeDestination);
                        }
                        //--Error.
                        else
                        {
                            rPlayerEntity->AppendNoMoveErrorToConsole(rAssociatedRoom->GetName());
                        }
                    }
                }
            }

            //--Set this to keep rotation fast.
            if(mProceedImmediately)
            {
                mTicksMovingSoFar = 100;
                mProceedImmediately = false;
            }
        }
        //--Turn left.
        else if(rControlManager->IsDown("Left"))
        {
            //--Debug.
            DebugPrint("Handling player keypress Left.\n");

            //--Navigation: Turn the camera left.
            if(mKeyboardRotationClamp == 1)
            {
                rActiveCamera->SetRotation(tXRot, tYRot, tZRot + mKeyboardRotationSpeed);
                mRefreshClickPoints = 1;
            }
            //--Increment the timer.
            else
            {
                if(mKeyboardRotationTimer < mKeyboardRotationClamp) mKeyboardRotationTimer ++;
            }
        }
        //--Turn right.
        else if(rControlManager->IsDown("Right"))
        {
            //--Debug.
            DebugPrint("Handling player keypress Right.\n");

            //--Navigation: Turn the camera right.
            if(mKeyboardRotationClamp == 1)
            {
                rActiveCamera->SetRotation(tXRot, tYRot, tZRot - mKeyboardRotationSpeed);
                mRefreshClickPoints = 1;
            }
            //--Increment the timer.
            else
            {
                if(mKeyboardRotationTimer > -mKeyboardRotationClamp) mKeyboardRotationTimer --;
            }
        }
        //--If neither left nor right is true, decrement the turning timer.
        else
        {
            if(mKeyboardRotationTimer > 0) mKeyboardRotationTimer --;
            if(mKeyboardRotationTimer < 0) mKeyboardRotationTimer ++;
        }

        //--Handle the rotation if the clamp is not 0 or 1.
        if(mKeyboardRotationClamp > 1)
        {
            //--Refresh positions if the timer is non-zero.
            if(mKeyboardRotationTimer) mRefreshClickPoints = 1;

            //--Rotation.
            float tPercent = (float)mKeyboardRotationTimer / (float)mKeyboardRotationClamp;
            rActiveCamera->SetRotation(tXRot, tYRot, tZRot + (mKeyboardRotationSpeed * tPercent));
        }
    }

    //--[Finish Up]
    //--Debug.
    DebugPop("VisualLevel:HandleNodeMovementUpdate - Complete.\n");
}

//--[Support Functions]
void VisualLevel::StartMovementTo(uint32_t pNodeID)
{
    //--Begins the process of moving towards a given node. Once this is called, AutoMoveTo() will take over.
    mRecheckFacing = true;
    mMoveToNodeID = pNodeID;
    mTicksMovingSoFar = 0;
    mTicksSlowingDown = 0;
    mLookingTimer = 0;
    mLookingFlag = LOOK_INITIAL;

    //--Node must exist, or cancel!
    ZoneNode *rTargetNode = mZoneEditor->GetNode(pNodeID);
    if(!rTargetNode)
    {
        mMoveToNodeID = 0;
        return;
    }

    //--Check the facings. If our current room doesn't have the target in its facing list, we may need to
    //  track it back.
    ZoneNode *rPlayerNode = mZoneEditor->GetNode(mPlayerNode);
    if(rPlayerNode)
    {
        //--Loop.
        FacingInfo *rCheckInfo = rPlayerNode->GetFacingByDestination(pNodeID);

        //--Doesn't exist, follow it back.
        if(!rCheckInfo)
        {
            pNodeID = VisualRoom::FindFirstID(mPlayerNode, pNodeID, this);
        }
    }

    //--Follow the instruction sequence to its end, passing auto-transitions as necessary. Order the
    //  player's "world" entity to follow the matching exit.
    uint32_t tDestinationNodeID = VisualRoom::FollowIDTrail(mPlayerNode, mMoveToNodeID, this);
    VisualRoom *rCurrentRoom = GetRoomByNodeID(mPlayerNode);
    VisualRoom *rTargetRoom = GetRoomByNodeID(tDestinationNodeID);
    //fprintf(stderr, "Start node: %i\n", mPlayerNode);
    //fprintf(stderr, "Target node: %i\n", mMoveToNodeID);
    //fprintf(stderr, "Final node: %i\n", tDestinationNodeID);
    //fprintf(stderr, "Results %p %p\n", rCurrentRoom, rTargetRoom);
    if(rCurrentRoom && rTargetRoom && !mStopMovingAfterTurn)
    {
        //--Figure out which direction the room goes that leads to the target.
        for(int i = 0; i < NAV_TOTAL; i ++)
        {
            void *rCheckPtr = rCurrentRoom->GetConnection(i);
            if(rCheckPtr == rTargetRoom)
            {
                Actor::xInstruction = i + NAVIGATION_OFFSET;
                //fprintf(stderr, "Visual Level is moving to instruction %i\n", Actor::xInstruction);
                break;
            }
        }
    }

    //--Store our facing direction.
    float tXRot, tYRot, tZRot;
    CameraManager::FetchActiveCamera3D()->Get3DRotation(tXRot, tYRot, tZRot);
    mStartLookZ = tZRot;

    //--Compute the target facing direction.
    float tNodeX = rTargetNode->GetX();
    float tNodeY = rTargetNode->GetY() * -1.0f;
    float tMoveAngle = atan2f(tNodeY - mPlayerY, tNodeX - mPlayerX);
    mEndLookZ = 270.0f - (tMoveAngle * TODEGREE);
}
bool VisualLevel::TurnToFace(uint32_t pNodeID, bool pToTheRight, int pPreviousFacing)
{
    //--Turns the camera to face the next facing to the right or left of the existing facing. Cannot end on pPreviousFacing unless
    //  that's the only facing available (in which case nothing will happen and the function exits immediately).
    //--Returns true if we're done moving, false otherwise.
    ZoneNode *rTargetNode = mZoneEditor->GetNode(pNodeID);
    if(!rTargetNode) return true;

    //--If the node has exactly one facing, stop here.
    if(rTargetNode->GetFacingsTotal() <= 1) return true;

    //--Camera information.
    float tXRot, tYRot, tZRot;
    SugarCamera3D *rCamera = CameraManager::FetchActiveCamera3D();
    rCamera->Get3DRotation(tXRot, tYRot, tZRot);

    //--If this hasn't been set yet, we need to find our destination angle.
    if(mLookingTimer == 0)
    {
        //--Set startup variables.
        int tTotalFacings = rTargetNode->GetFacingsTotal();
        mStartLookZ = tZRot;

        //--Turning right code:
        if(pToTheRight)
        {
            //--Default.
            mEndLookZ = 360.0f * 3.0f;

            //--Scan all the facings. All facings which are less than the starting lookup get 360.0f added
            //  to deal with full-circle cases. Whichever facing has the lowest angle is the target.
            for(int i = 0; i < tTotalFacings; i ++)
            {
                //--Ignore if this is the angle we started at.
                if(i == pPreviousFacing) continue;

                //--Get the angle. Add 360.0f if it's less than the starting angle.
                FacingInfo *rFacingInfo = rTargetNode->GetFacing(i);
                if(!rFacingInfo) continue;
                float tExamineAngle = rFacingInfo->mZRotation;
                if(tExamineAngle <= mStartLookZ) tExamineAngle = tExamineAngle + 360.0f;

                //--Is it less than the next lowest case?
                if(tExamineAngle < mEndLookZ)
                {
                    mPlayerFacing = i;
                    mEndLookZ = tExamineAngle;
                }
            }
        }
        //--Turning left code:
        else
        {
            //--Default.
            mEndLookZ = 360.0f * 3.0f * -1.0f;

            //--Scan all the facings. All facings which are greater than the starting lookup get 360.0f subtracted
            //  to deal with full-circle cases. Whichever facing has the highest angle is the target.
            for(int i = 0; i < tTotalFacings; i ++)
            {
                //--Ignore if this is the angle we started at.
                if(i == pPreviousFacing) continue;

                //--Get the angle. Add 360.0f if it's less than the starting angle.
                FacingInfo *rFacingInfo = rTargetNode->GetFacing(i);
                if(!rFacingInfo) continue;

                //--Compute.
                float tExamineAngle = rFacingInfo->mZRotation;
                if(tExamineAngle >= mStartLookZ) tExamineAngle = tExamineAngle - 360.0f;

                //--Is it greater than the next highest case?
                if(tExamineAngle > mEndLookZ)
                {
                    mPlayerFacing = i;
                    mEndLookZ = tExamineAngle;
                }
            }
        }

        //--Check to ake sure we didn't somehow wind up with the starting angle again, or an otherwise illegal angle.
        if(mEndLookZ == 360.0f * 3.0f)
        {
            DebugManager::ForcePrint("VisualLevel:TurnToFace - Error, end angle was never resolved but more than one angle exists.\n");
            return true;
        }

        //--Clamp.
        while(mEndLookZ <    0.0f) mEndLookZ = mEndLookZ + 360.0f;
        while(mEndLookZ >= 360.0f) mEndLookZ = mEndLookZ - 360.0f;

        //--Debug.
        //DebugManager::ForcePrint("Get ending angle of %f, turning to face in slot %i.\n", mEndLookZ, mPlayerFacing);
    }

    //--Timer.
    mLookingTimer ++;

    //--Compute the max turning speed. This is in degrees.
    float cTurnSpeedMax = 6.0f;
    float cActualTurnSpeed = ((mLookingTimer + 7.5f) / 22.5f) * cTurnSpeedMax;
    if(cActualTurnSpeed > cTurnSpeedMax) cActualTurnSpeed = cTurnSpeedMax;
    if(!pToTheRight) cActualTurnSpeed = cActualTurnSpeed * -1.0f;

    //--Can we finish the turn this tick?
    if(fabsf(tZRot - mEndLookZ) <= fabsf(cActualTurnSpeed) || fabsf(tZRot - mEndLookZ) >= 360.0f - fabsf(cActualTurnSpeed))
    {
        rCamera->SetRotation(tXRot, tYRot, mEndLookZ);
        return true;
    }

    //--If we got this far, turn.
    rCamera->SetRotation(tXRot, tYRot, tZRot + cActualTurnSpeed);
    return false;
}
bool VisualLevel::AutoMoveTo(uint32_t pNodeID)
{
    //--Moves the player towards the node with the matching ID. They will also change their facing to look towards the node
    //  before starting moving if they weren't already facing it perfectly. This will not change their pitch, just their
    //  roll (the Z rotation).
    //--Returns true if we're done moving, false if still moving to the node (or the next node).
    if(!pNodeID) return true;

    //--Get the matching node. If it doesn't exist, cancel this update.
    ZoneNode *rTargetNode = mZoneEditor->GetNode(pNodeID);
    if(!rTargetNode) return true;

    //--Camera information.
    float tXRot, tYRot, tZRot;
    SugarCamera3D *rActiveCamera = CameraManager::FetchActiveCamera3D();
    rActiveCamera->Get3DRotation(tXRot, tYRot, tZRot);

    //--Tick counter.
    mTicksMovingSoFar ++;

    //--Speed control.
    float tActualSpeed = (mTicksMovingSoFar / 15.0f) * mPlayerMoveSpeed;
    if(tActualSpeed > mPlayerMoveSpeed) tActualSpeed = mPlayerMoveSpeed;

    //--Get the horizontal angle. Vertical doesn't matter since we're moving in noclip mode.
    float tNodeX = rTargetNode->GetX();
    float tNodeY = rTargetNode->GetY() * -1.0f;
    float tMoveAngle = atan2f(tNodeY - mPlayerY, tNodeX - mPlayerX);

    //--Clamp.
    while(tMoveAngle <  0.0f)       tMoveAngle = tMoveAngle + 6.2831852f;
    while(tMoveAngle >= 6.2831852f) tMoveAngle = tMoveAngle - 6.2831852f;

    //--Turn the Z rotation to face this angle. This stops movement until it's done, but doesn't interrupt movement
    //  if the camera angle was already very close.
    if(fabsf(mStartLookZ - mEndLookZ) > 5.0f && mLookingFlag == LOOK_INITIAL)
    {
        //--Timer.
        mLookingTimer ++;

        //--Clamp checking.
        float tUseEnd = mEndLookZ;
        if(fabsf(mStartLookZ - mEndLookZ) > 180.0f)
        {
            if(mStartLookZ <= 180.0f)
            {
                tUseEnd = mEndLookZ - 360.0f;
            }
            else
            {
                tUseEnd = mEndLookZ + 360.0f;
            }
        }

        //--Estimate how many ticks it should take to turn to face this.
        float cMaxTicks = 15.0f * (mPlayerMoveSpeed / PLAYER_BASE_MOVE_SPEED);
        if(mKeyboardRotationSpeed > 0.0f)
        {
            cMaxTicks = (fabsf(mStartLookZ - tUseEnd) / mKeyboardRotationSpeed) * 1.5f;
            if(mTicksMovingSoFar >= 100) cMaxTicks = cMaxTicks / 2.0f;
        }

        //--Compute how far to turn based on an easing function. It's very fast.
        float tPercent = EasingFunction::QuadraticOut((float)mLookingTimer, cMaxTicks);
        if(tPercent >= 1.0f)
        {
            //--Set.
            mStartLookZ = mEndLookZ;
            CameraManager::FetchActiveCamera3D()->SetRotation(90.0f, tYRot, mEndLookZ);
            mLookingFlag = LOOK_RESOLVE_ENDING;

            //--If flagged, we're not interested in actually going to the node.
            if(mStopMovingAfterTurn)
            {
                mMoveToNodeID = 0;
                mStopMovingAfterTurn = false;
                return false;
            }
        }
        //--Partial case.
        else
        {
            float tNewFacingZ = mStartLookZ + ((tUseEnd - mStartLookZ) * tPercent);
            CameraManager::FetchActiveCamera3D()->SetRotation(90.0f, tYRot, tNewFacingZ);
        }

        return false;
    }
    //--Close enough, set it and move on.
    else if(mLookingFlag == LOOK_INITIAL)
    {
        //--Set.
        CameraManager::FetchActiveCamera3D()->SetRotation(90.0f, tYRot, mEndLookZ);
        mLookingFlag = LOOK_RESOLVE_ENDING;

        //--If this flag is set, we are not interested in actually moving anywhere. Stop movement here.
        if(mStopMovingAfterTurn)
        {
            mMoveToNodeID = 0;
            mStopMovingAfterTurn = false;
            return false;
        }
    }

    //--Check distance. Complete if we're at the target.
    float tDistance = GetPlanarDistance(mPlayerX, mPlayerY, tNodeX, tNodeY);

    //--Movement speed reduction. This occurs if the destination node does not have an automatic transition.
    //  It slows down the movement speed right before we arrive at the destination so it looks less jerky.
    float tFinalSpeed = tActualSpeed;
    AutoTransitionInfo *rCheckInfo = rTargetNode->GetAutoTransitionFrom(mPlayerNode);
    if(!rCheckInfo)
    {
        //--Downstep the speed.
        if(tDistance <= tActualSpeed * VL_SLOW_DOWN_FACTOR * VL_DISTANCE_FACTOR)
        {
            //--Timer.
            mTicksSlowingDown ++;

            //--Speed uses an easing function, but never goes slower than half.
            float tPercent = EasingFunction::QuadraticIn((float)mTicksSlowingDown, VL_SLOW_DOWN_FACTOR);
            if(tPercent > 1.0f) tPercent = 1.0f;

            //--Set.
            tFinalSpeed = tActualSpeed - (tActualSpeed * tPercent * VL_MAX_SPEED_REDUCTION);
        }
    }

    //--Check completion when at the target.
    if(tDistance <= tFinalSpeed)
    {
        //--Place us at the target.
        mPlayerX = tNodeX;
        mPlayerY = tNodeY;

        //--Reposition the camera to be in the player's eyes.
        rActiveCamera->SetPosition(-mPlayerX, -mPlayerY, -(mPlayerZ + PLAYER_HEIGHT));

        //--Update the player entity's position.
        Actor *rPlayerEntity = EntityManager::Fetch()->GetLastPlayerEntity();
        if(rPlayerEntity) rPlayerEntity->SetWorldPosition(-mPlayerX, -mPlayerY, mPlayerZ);

        //--Return true to indicate no further movment is necessary.
        return true;
    }

    //--Otherwise, move towards the target.
    float cIncrement = PLAYER_BASE_MOVE_SPEED;
    float tUseSpeed = tFinalSpeed;
    while(tUseSpeed > 0.0f)
    {
        //--[Movement]
        //--Use either the max, or whatever is left.
        float tSpeedThisPass = tUseSpeed;
        if(tSpeedThisPass > cIncrement) tSpeedThisPass = cIncrement;

        //--Move.
        mPlayerX = mPlayerX + (cosf(tMoveAngle) * tSpeedThisPass);
        mPlayerY = mPlayerY + (sinf(tMoveAngle) * tSpeedThisPass);

        //--Decrement the total speed.
        tUseSpeed = tUseSpeed - tSpeedThisPass;

        //--[Z Handling]
        //--Position and setup.
        mIsPlayerOnGround = false;
        const float cGravity = -0.50f;
        float cClimbRange = 12.0f;
        float tExpectedHeight = (mLocalWADFile->GetHeightAtPoint(mPlayerX, mPlayerY, mPlayerZ));
        float tExpectedHeightUp = (mLocalWADFile->GetHeightAtPoint(mPlayerX, mPlayerY, mPlayerZ+cClimbRange));

        //--Move up. This is always instant, since we're assumed to be climbing a step.
        if(tExpectedHeight > mPlayerZ || tExpectedHeightUp > mPlayerZ)
        {
            mIsPlayerOnGround = true;
            mPlayerZSpeed = 0.0f;
            mPlayerZ = tExpectedHeight;
            if(tExpectedHeightUp > tExpectedHeight) mPlayerZ = tExpectedHeightUp;
        }
        //--Move down. This follows gravity.
        else if(tExpectedHeight < mPlayerZ + mPlayerZSpeed)
        {
            //--Check the downstep case. If we are moving down 12 or fewer units, and have just exited the ground....
            if(mIsPlayerOnGround && tExpectedHeight >= mPlayerZ - cClimbRange)
            {
                mIsPlayerOnGround = true;
                mPlayerZSpeed = 0.0f;
                mPlayerZ = tExpectedHeight;
            }
            //--Falling case.
            else
            {
                mIsPlayerOnGround = false;
                mPlayerZSpeed = mPlayerZSpeed + cGravity;
                mPlayerZ = mPlayerZ + mPlayerZSpeed;
            }
        }
        //--Close enough, match position exactly.
        else
        {
            mIsPlayerOnGround = true;
            mPlayerZSpeed = 0.0f;
            mPlayerZ = tExpectedHeight;
        }
    }

    //--If we're within 10 ticks of the target, start sliding the facing to that direction. This does not occur if the manual
    //  facing flag is on.
    if(tDistance <= tFinalSpeed * VL_TURN_TO_FACE_FACTOR && !xRenderEntitiesInWorldRotation)
    {
        //--If this is the first call, set the targets. We need to figure out the nearest facing at the destination to our heading.
        if(mLookingFlag == LOOK_RESOLVE_ENDING)
        {
            //--Set the flag.
            mLookingFlag = LOOK_PERFORM_ENDING;

            //--Get the target node. If it's somehow NULL, that's an error.
            ZoneNode *rDestinationNode = mZoneEditor->GetNode(pNodeID);
            if(!rDestinationNode) return false;

            //--Get the facing closest to the heading we're using right now. There might be an exact match.
            FacingInfo *rFacingInfo = rDestinationNode->GetNearestFacingByAngle(mEndLookZ);
            if(!rFacingInfo) return false;

            //--Everything checked out, set the turning variables.
            mLookingTimer = 0;
            mStartLookZ = mEndLookZ;
            mEndLookZ = rFacingInfo->mZRotation;

            //--Clamp.
            ClampToCircle(mStartLookZ);
            ClampToCircle(mEndLookZ);
            if(fabsf(mStartLookZ - mEndLookZ) > 180.0f)
            {
                if(mStartLookZ <= 180.0f)
                {
                    mEndLookZ = mEndLookZ - 360.0f;
                }
                else
                {
                    mEndLookZ = mEndLookZ + 360.0f;
                }
            }
        }

        //--Turn to face the target.
        mLookingTimer ++;

        //--Compute how far to turn based on an easing function. It's very fast.
        if(mLookingTimer < (int)VL_TURN_TO_FACE_FACTOR)
        {
            float tPercent = EasingFunction::QuadraticOut((float)mLookingTimer, VL_TURN_TO_FACE_FACTOR - 1.0f);
            float tNewFacingZ = mStartLookZ + ((mEndLookZ - mStartLookZ) * tPercent);
            CameraManager::FetchActiveCamera3D()->SetRotation(90.0f, tYRot, tNewFacingZ);
        }
    }

    //--Reposition the camera to be in the player's eyes.
    rActiveCamera->SetPosition(-mPlayerX, -mPlayerY, -(mPlayerZ + PLAYER_HEIGHT));

    //--Update the player entity's position.
    Actor *rPlayerEntity = EntityManager::Fetch()->GetLastPlayerEntity();
    if(rPlayerEntity) rPlayerEntity->SetWorldPosition(-mPlayerX, -mPlayerY, mPlayerZ);

    //--Still has more movement in store.
    return false;
}
