//--Base
#include "VisualLevel.h"

//--Classes
#include "Actor.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarFont.h"
#include "SugarLinkedList.h"

//--Definitions
#include "EasingFunctions.h"
#include "Global.h"

//--GUI
//--Libraries
//--Managers
#include "ControlManager.h"
#include "DisplayManager.h"
#include "LuaManager.h"

//--When something of particular importance is happening on the screen, it's helpful to display what that is
//  in greater detail. The interface for that is called the "story display" and it's handled here.
//--In normal gameplay, the story display can be written to directly, or it can be set to intercept calls that
//  would normally land in the console.

//--[Property Queries]
bool VisualLevel::IsStoryModeActive()
{
    return mShowStoryDisplay;
}

//--[Manipulators]
void VisualLevel::SetStoryModeActivity(bool pFlag)
{
    if(pFlag)
        ActivateStoryMode();
    else
        DeactivateStoryMode();
}
void VisualLevel::ActivateStoryMode()
{
    //--Note: Implicitly clears story mode if it wasn't active. Does nothing if it was.
    if(mShowStoryDisplay) return;
    mShowStoryDisplay = true;
    ClearStoryMode();
}
void VisualLevel::DeactivateStoryMode()
{
    //--Note: Doesn't clear story mode, just stops showing it.
    mShowStoryDisplay = false;
}
void VisualLevel::AppendStoryString(ConsoleString *pString)
{
    //--Appends and takes ownership of the string.
    mStoryConsoleStrings->AddElementAsTail("X", pString, &ConsoleString::DeleteThis);
}

//--[Core Methods]
void VisualLevel::ClearStoryMode()
{
    //--Resets all the story display variables back to their factory zero, excluding the display boolean.
    mStoryConsoleWaitForKeypress = false;
    mStoryFadeTimer = VL_CONSOLE_FADE_TICKS;
    rCurrentStoryBitmap = NULL;
    mStoryConsoleStrings->ClearList();
    mStoryConsoleFadingString->ClearList();
}

//--[Update]
void VisualLevel::UpdateStoryMode()
{
    //--For now, just close story mode when a key is pressed.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Run the timer.
    mStoryFadeTimer ++;

    //--No keys pressed, done here.
    if(!rControlManager->IsAnyKeyPressed()) return;

    //--Run the post exec if it exists.
    if(mPostExecScript)
    {
        //--Reset the timer.
        mStoryFadeTimer = 0;

        //--Move all the strings on to the fading list.
        mStoryConsoleFadingString->ClearList();
        ConsoleString *rString = (ConsoleString *)mStoryConsoleStrings->SetToHeadAndReturn();
        while(rString)
        {
            mStoryConsoleFadingString->AddElement("X", mStoryConsoleStrings->LiberateRandomPointerEntry(), &ConsoleString::DeleteThis);
            rString = (ConsoleString *)mStoryConsoleStrings->SetToHeadAndReturn();
        }

        //--Free the old copy. We don't do this after the script runs, since it might replace the path (or not).
        char *tOldScriptPtr = mPostExecScript;
        mPostExecScript = NULL;

        //--Execute.
        LuaManager::Fetch()->ExecuteLuaFile(tOldScriptPtr, 1, "N", (float)mPostExecFiringCode);

        //--Clean up.
        free(tOldScriptPtr);
    }
    //--No post-exec exists, so just deactivate story mode.
    else
    {
        Actor::xAreControlsEnabled = false;
        mIsConsoleWaitingOnKeypress = false;
        DeactivateStoryMode();
    }
}

//--[Render]
void VisualLevel::RenderStoryMode()
{
    //--[Documentation]
    //--Renders the story display. Presently, this is a black overlay, the required image, and whatever strings are
    //  currently in the console. Might expand later.

    //--[Setup]
    //--Base borders.
    float cLft = 0.0f;
    float cTop = 0.0f;
    float cRgt = VIRTUAL_CANVAS_X;
    float cBot = VIRTUAL_CANVAS_Y;

    //--Recompute the borders. We need to make the overlay cover the whole screen, accounting for no letterbox in 3D mode.
    GLOBAL *rGlobal =  Global::Shared();
    if(rGlobal->gWindowWidth > VIRTUAL_CANVAS_X)
    {
        cLft = cLft - DisplayManager::xScaledOffsetX;
        cRgt = cRgt + DisplayManager::xScaledOffsetX;
    }

    //--Font.
    SugarFont *rStoryFont = PandemoniumLevel::Images.Data.rUIFontMedium;

    //--[Overlay]
    //--Render a grey overlay.
    glColor4f(0.0f, 0.0f, 0.0f, 0.75f);
    glDisable(GL_TEXTURE_2D);
    glBegin(GL_QUADS);
        glVertex2f(cLft, cTop);
        glVertex2f(cRgt, cTop);
        glVertex2f(cRgt, cBot);
        glVertex2f(cLft, cBot);
    glEnd();
    glEnable(GL_TEXTURE_2D);
    glColor4f(1.0f, 1.0f, 1.0f, 1.0f);

    //--[Portrait]
    //--Render whatever image is meant to be on screen, if there is on. The image is always centered.
    if(rActivePortrait)
    {
        float tX = ((cLft + cRgt - rActivePortrait->GetWidth()) / 2.0f);
        float tY = ((cTop + cBot - rActivePortrait->GetHeight()) / 2.0f);
        rActivePortrait->Draw(tX, tY);
    }

    //--[Console]
    //--Setup.
    float cIndent = 8.0f;
    float cSizeFactor = 1.0f;
    float cSizePerLine = 20.0f * cSizeFactor;
    float cTextTop = VIRTUAL_CANVAS_Y - cIndent - (cSizePerLine * 6.5f);

    //--Border.
    RenderBorderCardOver(0.0f, cTextTop - cIndent, VIRTUAL_CANVAS_X, VIRTUAL_CANVAS_Y, 0x01FF);

    //--Render the fading strings. These are cloned from the base strings to make things look pretty. Obviously, nothing renders
    //  if it's all faded out.
    if(mStoryFadeTimer < VL_CONSOLE_FADE_TICKS)
    {
        //--Reset.
        int i = 0;
        float tCursor = cTextTop;
        float tPercent = EasingFunction::QuadraticOut(mStoryFadeTimer, VL_CONSOLE_FADE_TICKS);

        //--Compute alpha.
        float tAlpha = 1.0f - ((float)mStoryFadeTimer / (float)VL_CONSOLE_FADE_TICKS);

        //--Iterate. All the positions are the same.
        ConsoleString *rString = (ConsoleString *)mStoryConsoleFadingString->PushIterator();
        while(rString)
        {
            //--If a string consists of exactly one space, it's a padding line. Render it at half-size, and
            //  don't render it at all if this is the 0th line.
            if(rString->mString[0] == ' ' && rString->mString[1] == '\0')
            {
                if(i > 0) tCursor = tCursor + (cSizePerLine*0.5f);
                i ++;
                rString = (ConsoleString *)mStoryConsoleFadingString->AutoIterate();
                continue;
            }

            //--Compute position.
            float tRenderX = cIndent;
            float tRenderY = tCursor + (3.0f * tPercent);

            //--Position setup. Uses a similar offset to the console.
            glTranslatef(tRenderX, tRenderY, 0.0f);
            glScalef(cSizeFactor, cSizeFactor, 1.0f);

            //--Render.
            glColor4f(rString->mColor.r, rString->mColor.g, rString->mColor.b, tAlpha);
            if(rString->mColor.r == 0.0f && rString->mColor.g == 0.0f && rString->mColor.b == 0.0f) glColor4f(1.0f, 1.0f, 1.0f, tAlpha);
            rStoryFont->DrawText(0, 0, 0, rString->mString);

            //--Clean.
            glScalef(1.0f / cSizeFactor, 1.0f / cSizeFactor, 1.0f);
            glTranslatef(-tRenderX, -tRenderY, 0.0f);

            //--Next.
            i ++;
            tCursor = tCursor + cSizePerLine;
            rString = (ConsoleString *)mStoryConsoleFadingString->AutoIterate();
        }
    }

    //--Render the attached console strings. For now, just render them all.
    int i = 0;
    float tCursor = cTextTop;
    ConsoleString *rString = (ConsoleString *)mStoryConsoleStrings->PushIterator();
    while(rString)
    {
        //--If a string consists of exactly one space, it's a padding line. Render it at half-size, and
        //  don't render it at all if this is the 0th line.
        if(rString->mString[0] == ' ' && rString->mString[1] == '\0')
        {
            if(i > 0) tCursor = tCursor + (cSizePerLine*0.5f);
            i ++;
            rString = (ConsoleString *)mStoryConsoleStrings->AutoIterate();
            continue;
        }

        //--Position setup. Uses a similar offset to the console.
        glTranslatef(cIndent, tCursor, 0.0f);
        glScalef(cSizeFactor, cSizeFactor, 1.0f);

        //--Render.
        rString->mColor.SetAsMixer();
        if(rString->mColor.r == 0.0f && rString->mColor.g == 0.0f && rString->mColor.b == 0.0f) glColor3f(1.0f, 1.0f, 1.0f);
        rStoryFont->DrawText(0, 0, 0, rString->mString);

        //--Clean.
        glScalef(1.0f / cSizeFactor, 1.0f / cSizeFactor, 1.0f);
        glTranslatef(-cIndent, -tCursor, 0.0f);

        //--Next.
        i ++;
        tCursor = tCursor + cSizePerLine;
        rString = (ConsoleString *)mStoryConsoleStrings->AutoIterate();
    }

    //--[Clean Up]
    //--Clean.
    glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
}
