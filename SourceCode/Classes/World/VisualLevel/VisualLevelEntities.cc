//--Base
#include "VisualLevel.h"

//--Classes
#include "Actor.h"
#include "InventoryItem.h"
#include "VisualRoom.h"
#include "WorldContainer.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarLinkedList.h"
#include "SugarFont.h"

//--Definitions
#include "DeletionFunctions.h"
#include "EasingFunctions.h"

//--GUI
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "CameraManager.h"
#include "DisplayManager.h"
#include "EntityManager.h"
#include "OptionsManager.h"

//--[Property Queries]
bool VisualLevel::ShouldEntityRender(Actor *pActor)
{
    //--Returns whether or not the given Actor needs to render on the Entity Display cycle.
    if(!pActor) return false;

    //--The player never renders itself.
    Actor *rPlayerActor = EntityManager::Fetch()->GetLastPlayerEntity();
    if(rPlayerActor == pActor) return false;

    //--An Actor that has a visibility timer of 0 does not render, but *does* consume a spot in the rendering list.
    //  Therefore, this function does not check that!

    //--All checks passed, this entity should render.
    return true;
}
int VisualLevel::CountVisibleEntities()
{
    //--Counts and returns how many entities should be visible. Uses the same basic logic as the rendering.
    //  Implicitly uses the room the player is currently in.
    Actor *rPlayerActor = EntityManager::Fetch()->GetLastPlayerEntity();
    if(!rPlayerActor) return 0;
    PandemoniumRoom *rActiveRoom = rPlayerActor->GetCurrentRoom();
    if(!rActiveRoom) return 0;

    //--Setup.
    int tCount = 0;
    SugarLinkedList *rEntityList = rActiveRoom->GetEntityList();

    //--Loop.
    Actor *rActor = (Actor *)rEntityList->PushIterator();
    while(rActor)
    {
        //--Run this subroutine.
        if(ShouldEntityRender(rActor)) tCount ++;

        //--Next actor.
        rActor = (Actor *)rEntityList->AutoIterate();
    }

    //--Add 1 for every item.
    tCount += rActiveRoom->GetCompressedInventorySize();

    //--Add 1 for every container. Containers marked for self-destruct are ignored.
    SugarLinkedList *rContainerList = rActiveRoom->GetContainerList();
    WorldContainer *rContainer = (WorldContainer *)rContainerList->PushIterator();
    while(rContainer)
    {
        if(!rContainer->IsSelfDestructing()) tCount ++;
        rContainer = (WorldContainer *)rContainerList->AutoIterate();
    }

    //--All done.
    return tCount;
}

//--[Rendering]
void VisualLevel::RenderEntitiesPane()
{
    //--[Documentation]
    //--Override for the PandemoniumLevel version. This isn't actually a pane in 3D mode, the entities are
    //  rendered on the 3D field (abstractly) and can be cycled through while there. Context menus follow
    //  the same logic but the handling isn't different, just where you click.
    Actor *rPlayerActor = EntityManager::Fetch()->GetLastPlayerEntity();
    if(!rPlayerActor) return;

    //--[Setup]
    //--We need a room. If the acting entity is not in a room, we have nothing to display.
    PandemoniumRoom *rActiveRoom = rPlayerActor->GetCurrentRoom();
    if(!rActiveRoom) return;

    //--Entity list.
    //SugarLinkedList *rEntityList = rActiveRoom->GetEntityList();
    mStoredEntityCount = CountVisibleEntities();

    //--Option Flags
    /*OptionsManager *rOptionManager = OptionsManager::Fetch();
    bool tShowHP     = rOptionManager->GetOptionB("Show Entity HP");
    bool tShowWP     = rOptionManager->GetOptionB("Show Entity WP");
    bool tShowWeapon = rOptionManager->GetOptionB("Show Entity Weapon");*/

    //--Font.
    //SugarFont *rEntitiesFont = PandemoniumLevel::Images.Data.rUIFontMedium;

    //--[Fading Objects]
    //--Render the extra Actors/Items/Containers. These are objects which are currently fading out. They don't
    //  have hitboxes but otherwise render like everything else.
    //--This list also removes entities which don't render anything or are fully invisible.
    mDontRepositionOnWorldRender = true;
    ExtraEntityRenderPack *rRenderPack = (ExtraEntityRenderPack *)mExtraRenderEntitiesList->SetToHeadAndReturn();
    while(rRenderPack)
    {
        //--Check the image. Remove if it's NULL.
        if(!rRenderPack->rRenderImg)
        {
            mExtraRenderEntitiesList->RemoveRandomPointerEntry();
        }
        //--If the render slot is -1, remove it. It was offscreen when it was created.
        else if(rRenderPack->mRenderSlot == -1)
        {
            mExtraRenderEntitiesList->RemoveRandomPointerEntry();
        }
        //--Render the image.
        else
        {
            //--Render.
            float tAlpha = (float)rRenderPack->mRenderTicksLeft / (float)VL_ENTITY_FADE_TICKS;
            RenderBitmapInWorld(rRenderPack->mRenderSlot, tAlpha, rRenderPack->rRenderImg);

            //--Decrement the timer. If it hits 0, remove the image.
            rRenderPack->mRenderTicksLeft --;
            if(rRenderPack->mRenderTicksLeft < 1) mExtraRenderEntitiesList->RemoveRandomPointerEntry();
        }

        //--Next.
        rRenderPack = (ExtraEntityRenderPack *)mExtraRenderEntitiesList->IncrementAndGetRandomPointerEntry();
    }

    //--Reposition flag. If this flag is true, then the entities will not reposition themselves even if the
    //  selected entity is not in the middle. This is used when objects are fading out to allow them time
    //  to fade before things move around.
    mDontRepositionOnWorldRender = true;
    if(mExtraRenderEntitiesList->GetListSize() < 1 || IsMoving()) mDontRepositionOnWorldRender = false;

    //--Flags. Clear the mixer in case something else failed to render.
    StarlightColor::ClearMixer();

    //--[Rendering Cycle]
    //--New rendering technique! Renders using a unified object type.
    RefreshEntityArray();

    //--Render each unified pack.
    int tRenderCount = 0;
    for(int i = 0; i < VL_ENTITIES_PER_SCREEN; i ++)
    {
        //--Debug.
        //fprintf(stderr, "Rendering in slot %i: %p %p %p\n", i, mUnifiedRenderListing[i].rActorPtr, mUnifiedRenderListing[i].rInventoryPackPtr, mUnifiedRenderListing[i].rContainerPtr);

        //--Render this Actor if it has a valid pointer.
        if(mUnifiedRenderListing[i].rActorPtr)
        {
            tRenderCount ++;
            RenderActor(mUnifiedRenderListing[i].rActorPtr, i);
        }
        //--Inventory Pack case.
        else if(mUnifiedRenderListing[i].rInventoryPackPtr)
        {
            //--Simple render.
            tRenderCount ++;
            RenderInventoryPack(mUnifiedRenderListing[i].rInventoryPackPtr, i);
        }
        //--WorldContainer case.
        else if(mUnifiedRenderListing[i].rContainerPtr)
        {
            tRenderCount ++;
            RenderContainer(mUnifiedRenderListing[i].rContainerPtr, i);
        }
        //--No object, so clear the click position.
        else
        {
            mClickPoints[i].SetWH(-100.0f, -100.0f, 0.0f, 0.0f);
        }

        //fprintf(stderr, " Done.\n");
    }

    //--[Actors]
    //--Loop through the actors.
    /*
    int tRenderCount = 0;
    Actor *rActor = (Actor *)rEntityList->PushIterator();
    while(rActor)
    {
        //--Never render certain actors.
        if(!ShouldEntityRender(rActor))
        {
            rActor = (Actor *)rEntityList->AutoIterate();
            continue;
        }

        //--Damage animation. Entities flash when struck. The pack provided can be NULL.
        DamageAnimPack *rPack = (DamageAnimPack *)mDamageAnimationQueue->GetElementBySlot(0);
        if(rPack && rPack->rTargetRefPtr == rActor)
        {
            //--In this condition, animate a flashing by not rendering the Actor in question.
            if(rPack->mDamageTimer < PL_DAM_FLASH_TICKS && rPack->mDamageTimer % 4 < 2 && (rPack->mDamageHP > 0 || rPack->mDamageWP > 0))
            {
                tRenderCount ++;
                rActor = (Actor *)rEntityList->AutoIterate();
                continue;
            }
        }

        //--Get the image we should render with.
        float tRenderAlpha = (float)rActor->GetVisibilityTimer() / (float)VL_ENTITY_FADE_TICKS;
        SugarBitmap *rRenderImg = rActor->GetActiveImage();

        //--Normal case.
        int tRenderSlot = -1;
        if(!mDontRepositionOnWorldRender)
        {
            tRenderSlot = RenderBitmapInWorld(tRenderCount, tRenderAlpha, rRenderImg);
            rActor->mLastRenderSlot = tRenderCount;
        }
        //--Hold position as other objects animate.
        else
        {
            tRenderSlot = RenderBitmapInWorld(rActor->mLastRenderSlot, 1.0f, rRenderImg);
        }

        //--Render the name above the image.
        if(rRenderImg && tRenderSlot != -1)
        {
            //--Special: Set this flag to figure out if we need to render the extra information.
            bool tRenderExtraInfo = (rActor->GetRenderFlag() == RENDER_HUMAN || rActor->GetRenderFlag() == RENDER_MONSTER);

            //--Buffer. Figure out what to render. We render more detail if the HP/WP flags are set.
            char tBuffer[256];
            sprintf(tBuffer, "%s", rActor->GetName());

            //--Position.
            float tX = mLastRenderX - (rEntitiesFont->GetTextWidth(tBuffer) * 0.5f * cScreenTextScale);
            float tY = (VIRTUAL_CANVAS_Y * 0.45f) - (rRenderImg->GetTrueHeight() * 0.6f * mLastRenderScale);

            //--If the flag for HP/MP render is set, move the Y up a bit.
            if((tShowHP || tShowWP) && tRenderExtraInfo) tY = tY - 15.0f;

            //--If the flag tShowWeapon is true, move the Y up a bit. We render the weapon name below.
            InventoryItem *rActorWeapon = rActor->GetWeapon();
            if(tShowWeapon && rActorWeapon && tRenderExtraInfo) tY = tY - 15.0f;

            //--Render the text.
            glColor4f(1.0f, 1.0f, 1.0f, tRenderAlpha);
            rEntitiesFont->DrawText(tX, tY, 0, cScreenTextScale, tBuffer);
            tY = tY + 20.0f;

            //--Ignore the extra renders for non-humans and non-monsters.
            if(tRenderExtraInfo)
            {
                //--Render with just HP.
                if(tShowHP && !tShowWP)
                {
                    CombatStats tActorStats = rActor->GetCombatStatistics();
                    sprintf(tBuffer, "(H: %i/%i)", tActorStats.mHP, tActorStats.mHPMax);
                    rEntitiesFont->DrawText(tX, tY, 0, cScreenTextScale * 0.75, tBuffer);
                    tY = tY + 15.0f;
                }
                //--Render with just WP.
                else if(!tShowHP && tShowWP)
                {
                    CombatStats tActorStats = rActor->GetCombatStatistics();
                    sprintf(tBuffer, "(W: %i/%i)", tActorStats.mWillPower, tActorStats.mWillPowerMax);
                    rEntitiesFont->DrawText(tX, tY, 0, cScreenTextScale * 0.75, tBuffer);
                    tY = tY + 15.0f;
                }
                //--Render with both HP and WP.
                else if(tShowHP && tShowWP)
                {
                    CombatStats tActorStats = rActor->GetCombatStatistics();
                    sprintf(tBuffer, "H:%i W:%i", tActorStats.mHP, tActorStats.mWillPower);
                    rEntitiesFont->DrawText(tX, tY, 0, cScreenTextScale * 0.75, tBuffer);
                    tY = tY + 15.0f;
                }

                //--Render the weapon name.
                if(tShowWeapon && rActorWeapon)
                {
                    rEntitiesFont->DrawText(tX, tY, 0, cScreenTextScale * 0.75f, rActorWeapon->GetName());
                }
            }
        }

        //--Damage rendering. Show the HP/WP/Resist text.
        if(rPack && rPack->rTargetRefPtr == rActor)
        {
            //--Position/Setup.
            float tX = mLastRenderX;
            float tY = (VIRTUAL_CANVAS_Y * 0.45f) - (rRenderImg->GetTrueHeight() * 0.6f * mLastRenderScale);
            float cTextScale = 3.0f;
            float tOffset = 0.0f + (EasingFunction::QuadraticInOut(rPack->mDamageTimer, PL_DAM_TICKS_TOTAL) * 60.0f);

            //--If the HP damage is 0 and the WP is -1, this is a "miss".
            if(rPack->mDamageHP == 0 && rPack->mDamageWP < 0)
            {
                //--Miss is rendered in grey.
                glColor3f(0.5f, 0.5f, 0.5f);

                //--Compute the position.
                float tRenderX = tX - (rEntitiesFont->GetTextWidth("Miss!") * 0.5f * cTextScale);
                float tRenderY = tY - tOffset;

                //--Render it.
                rEntitiesFont->DrawText(tRenderX, tRenderY, 0, cTextScale, "Miss!");
            }
            //--If the HP damage is -1 and the WP is 0, this is a "resist".
            else if(rPack->mDamageHP < 0 && rPack->mDamageWP == 0)
            {
                //--Resist is rendered in violet.
                glColor3f(1.0f, 0.4f, 0.9f);

                //--Compute the position.
                float tRenderX = tX - (rEntitiesFont->GetTextWidth("Resist!") * 0.5f * cTextScale);
                float tRenderY = tY - tOffset;

                //--Render it.
                rEntitiesFont->DrawText(tRenderX, tRenderY, 0, cTextScale, "Resist!");
            }
            //--Otherwise, animate whichever damage is above 0. This can be both.
            else
            {
                //--HP damage.
                if(rPack->mDamageHP > 0)
                {
                    //--HP damage is red.
                    glColor3f(1.0f, 0.0f, 0.0f);

                    //--Buffer the damage.
                    char tBuffer[16];
                    sprintf(tBuffer, "%i", rPack->mDamageHP);

                    //--Compute the position.
                    float tRenderX = tX - (rEntitiesFont->GetTextWidth(tBuffer) * 0.5f * cTextScale);
                    float tRenderY = tY - tOffset;

                    //--If WP damage was dealt, the X render moves to the left.
                    if(rPack->mDamageWP > 0) tRenderX = tRenderX - tOffset;

                    //--Render it.
                    rEntitiesFont->DrawText(tRenderX, tRenderY, 0, cTextScale, tBuffer);
                }

                //--WP damage. Not exclusive with HP damage!
                if(rPack->mDamageWP > 0)
                {
                    //--HP damage is violet.
                    glColor3f(0.8f, 0.1f, 1.0f);

                    //--Buffer the damage.
                    char tBuffer[16];
                    sprintf(tBuffer, "%i", rPack->mDamageWP);

                    //--Compute the position.
                    float tRenderX = tX - (rEntitiesFont->GetTextWidth(tBuffer) * 0.5f * cTextScale);
                    float tRenderY = tY - tOffset;

                    //--If HP damage was dealt, the X render moves to the right.
                    if(rPack->mDamageHP > 0) tRenderX = tRenderX + tOffset;

                    //--Render it.
                    rEntitiesFont->DrawText(tRenderX, tRenderY, 0, cTextScale, tBuffer);
                }
            }
        }

        //--Next.
        tRenderCount ++;
        rActor = (Actor *)rEntityList->AutoIterate();
    }

    //--Clear mixer.
    StarlightColor::ClearMixer();

    //--[Items]
    //--Loop through the inventory entities.
    for(int i = 0; i < rActiveRoom->GetCompressedInventorySize(); i ++)
    {
        //--Get the item in the slot.
        InventoryPack *rItemPack = rActiveRoom->GetItemPack(i);
        if(!rItemPack || !rItemPack->rItem) continue;

        //--Render an item icon.
        float tRenderAlpha = (float)rItemPack->mIsVisibleTimer / (float)VL_ENTITY_FADE_TICKS;
        SugarBitmap *rItemIcon = rItemPack->rItem->Get3DImageFor();

        //--Normal case.
        int tRenderSlot = -1;
        if(!mDontRepositionOnWorldRender)
        {
            tRenderSlot = RenderBitmapInWorld(tRenderCount, tRenderAlpha, rItemIcon);
            rItemPack->mLastRenderSlot = tRenderCount;
        }
        //--Hold position as other objects animate.
        else
        {
            tRenderSlot = RenderBitmapInWorld(rItemPack->mLastRenderSlot, tRenderAlpha, rItemIcon);
        }

        //--Render the name of the item right below it. Items always use the same icon so
        //  the offset is fixed.
        const char *rItemName = rItemPack->rItem->GetName();
        if(rItemName && tRenderSlot != -1)
        {
            //--Resolve the text. Print the quantity in addition to the name if it's 2 or more.
            char tBuffer[64];
            strcpy(tBuffer, rItemName);
            if(rItemPack->mQuantity == 1)
            {
                sprintf(tBuffer, "%s", rItemName);
            }
            else if(rItemPack->mQuantity > 1)
            {
                sprintf(tBuffer, "%sx%i", rItemName, rItemPack->mQuantity);
            }

            //--Compute text position. Account for scaling!
            float tX = mLastRenderX - (rEntitiesFont->GetTextWidth(tBuffer) * 0.5f * cScreenTextScale);
            float tY = (VIRTUAL_CANVAS_Y * 0.5f) - (rItemIcon->GetTrueHeight() * 0.6f * mLastRenderScale);

            //--Render the text.
            glColor4f(1.0f, 1.0f, 1.0f, tRenderAlpha);
            rEntitiesFont->DrawText(tX, tY, 0, cScreenTextScale, tBuffer);
        }

        //--Next item.
        tRenderCount ++;
    }

    //--Clean up.
    StarlightColor::ClearMixer();

    //--[Containers]
    //--Render containers. These all appear as chests.
    SugarLinkedList *rContainerList = rActiveRoom->GetContainerList();
    WorldContainer *rContainer = (WorldContainer *)rContainerList->PushIterator();
    while(rContainer)
    {
        //--Skip containers that are self-destructing.
        if(rContainer->IsSelfDestructing())
        {
            rContainer = (WorldContainer *)rContainerList->AutoIterate();
            continue;
        }

        //--Get the icon and render it. It never fails.
        float tRenderAlpha = (float)rContainer->GetVisibilityTimer() / (float)VL_ENTITY_FADE_TICKS;
        SugarBitmap *rChestIcon = (SugarBitmap *)DataLibrary::Fetch()->GetEntry("Root/Images/GUI/3DUI/TreasureChest");

        //--Normal case.
        int tRenderSlot = -1;
        if(!mDontRepositionOnWorldRender)
        {
            tRenderSlot = RenderBitmapInWorld(tRenderCount, tRenderAlpha, rChestIcon);
            rContainer->mLastRenderSlot = tRenderCount;
        }
        //--Hold position as other objects animate.
        else
        {
            tRenderSlot = RenderBitmapInWorld(rContainer->mLastRenderSlot, tRenderAlpha, rChestIcon);
        }

        //--Don't render if the object... shouldn't render.
        if(tRenderSlot != -1)
        {
            //--Position.
            float tX = mLastRenderX - (rEntitiesFont->GetTextWidth(rContainer->GetName()) * 0.5f * cScreenTextScale);
            float tY = (VIRTUAL_CANVAS_Y * 0.5f) - (rChestIcon->GetTrueHeight() * 0.6f * mLastRenderScale);

            //--Render the text.
            glColor4f(1.0f, 1.0f, 1.0f, tRenderAlpha);
            rEntitiesFont->DrawText(tX, tY, 0, cScreenTextScale, rContainer->GetName());
        }

        //--Next.
        tRenderCount ++;
        rContainer = (WorldContainer *)rContainerList->AutoIterate();
    }*/

    //--Clean up.
    StarlightColor::ClearMixer();

    //--Flag.
    if(mEntitySwitchTimer == 0 && mRefreshClickPoints > 0) mRefreshClickPoints --;

    //--[Debug]
    if(false)
    {
        for(int i = 0; i < VL_ENTITIES_PER_SCREEN; i ++)
        {
            glDisable(GL_TEXTURE_2D);
            glBegin(GL_LINE_LOOP);
                glVertex2f(mClickPoints[i].mLft, mClickPoints[i].mTop);
                glVertex2f(mClickPoints[i].mRgt, mClickPoints[i].mTop);
                glVertex2f(mClickPoints[i].mRgt, mClickPoints[i].mBot);
                glVertex2f(mClickPoints[i].mLft, mClickPoints[i].mBot);
            glEnd();
            glEnable(GL_TEXTURE_2D);
        }
    }

    //--[Finish Up]
    //--Clean.
    mDontRepositionOnWorldRender = false;
    StarlightColor::ClearMixer();
}
int VisualLevel::RenderBitmapInWorld(int pSlot, float pActorAlpha, SugarBitmap *pBitmap)
{
    //--[Documentation]
    //--Given a bitmap, renders it in the world and stores its location in the mClickPoints array, if flagged.
    //  Note that this does not make distinctions between Actors, Items, or anything else.
    //--Returns the slot rendered in (which may not be pSlot) on success, -1 on error.
    if(!pBitmap || pSlot == -1) return -1;

    //--[Alt-Function]
    //--If this flag is set, use the by-rotation renderer instead.
    if(xRenderEntitiesInWorldRotation) return RenderBitmapInWorldRot(pSlot, pActorAlpha, pBitmap);

    //--[Constants]
    //--Position constants. Lazy-initted the first time this runs.
    if(xMustBuildConstants)
    {
        //--Flag.
        xMustBuildConstants = false;

        //--Set.
        cxPositions[ 0] = VIRTUAL_CANVAS_X *-0.35f;
        cxPositions[ 1] = VIRTUAL_CANVAS_X *-0.25f;
        cxPositions[ 2] = VIRTUAL_CANVAS_X *-0.15f;
        cxPositions[ 3] = VIRTUAL_CANVAS_X *-0.05f;
        cxPositions[ 4] = VIRTUAL_CANVAS_X * 0.05f;
        cxPositions[ 5] = VIRTUAL_CANVAS_X * 0.25f;
        cxPositions[ 6] = VIRTUAL_CANVAS_X * 0.50f;
        cxPositions[ 7] = VIRTUAL_CANVAS_X * 0.75f;
        cxPositions[ 8] = VIRTUAL_CANVAS_X * 0.95f;
        cxPositions[ 9] = VIRTUAL_CANVAS_X * 1.05f;
        cxPositions[10] = VIRTUAL_CANVAS_X * 1.15f;
        cxPositions[11] = VIRTUAL_CANVAS_X * 1.25f;
        cxPositions[12] = VIRTUAL_CANVAS_X * 1.35f;
    }

    //--Compute the center point of the field. It is slightly offset, since the console occupies the bottom.
    float cCenterX = VIRTUAL_CANVAS_X * 0.5f;
    float cCenterY = VIRTUAL_CANVAS_Y * 0.45f;

    //--[Position Computation]
    //--Compute the position of this Bitmap. The selected Bitmap is in the center slot. If we go below the edges of the array,
    //  then don't render the Bitmap (it's offscreen anyway).
    int tRenderSlot = (VL_ENTITIES_PER_SCREEN / 2) + pSlot - mSelected3DEntity;
    if(tRenderSlot < 1 || tRenderSlot >= VL_ENTITIES_PER_SCREEN - 2) return - 1;

    //--Variables.
    mLastRenderScale = 0.75f;
    mLastRenderX = cxPositions[tRenderSlot];
    float tRenderY = cCenterY;
    float tRenderZ = 0.5f;

    //--Compute the next-up render position. This is used when entities are scrolling.
    int tNextRenderSlot = tRenderSlot + (mSelected3DEntity - mSelected3DEntityPrevious);
    if(tNextRenderSlot != tRenderSlot && tNextRenderSlot >= 0 && tNextRenderSlot < VL_ENTITIES_PER_SCREEN)
    {
        //--Next position, compute difference.
        float tNextRenderX = cxPositions[tNextRenderSlot];
        float tPercent = EasingFunction::QuadraticInOut(fabsf(mEntitySwitchTimer), VL_ENTITY_SLIDE_TICKS);

        //--Modify the rendering position. The scale will handle this dynamically.
        if(!mDontRepositionOnWorldRender) mLastRenderX = mLastRenderX + ((tNextRenderX - mLastRenderX) * tPercent);
    }

    //--Scale, clamps downwards to 0.25f.
    mLastRenderScale = mLastRenderScale - (fabsf(cCenterX - mLastRenderX) * 0.0005f);
    if(mLastRenderScale < 0.55f) mLastRenderScale = 0.55;

    //--Z, so very wide entities don't render in front of things that are bigger than them.
    tRenderZ = 0.5f - (mLastRenderScale * 0.01f);

    //--[Rendering]
    //--Reposition to the center of the rendering point.
    glTranslatef(mLastRenderX, tRenderY, -tRenderZ);
    glScalef(mLastRenderScale, mLastRenderScale, 1.0f);

    //--Sizes.
    float cWid = pBitmap->GetTrueWidth()  * 0.5f;
    float cHei = pBitmap->GetTrueHeight() * 0.5f;

    //--If requested, store the location that the image occupies on the screen. This will allow the
    //  UI to dynamically set click hitboxes, including scaling them.
    if(mRefreshClickPoints)
    {
        //--Modify.
        cWid = cWid * mLastRenderScale;
        cHei = cHei * mLastRenderScale;

        //--Set. Store the previous width.
        mClickPoints[tRenderSlot].Set(mLastRenderX - cWid, tRenderY - cHei, mLastRenderX + cWid, tRenderY + cHei);

        //--Clean.
        cWid = cWid / mLastRenderScale;
        cHei = cHei / mLastRenderScale;
    }
    else
    {
    }

    //--Compute alpha/fading.
    float cColor = 1.0f - (0.25f * (1.0f - (mLastRenderScale / 0.75f)));
    glColor4f(cColor, cColor, cColor, pActorAlpha);

    //--Render.
    pBitmap->Draw(-cWid, -cHei);

    //--[Clean]
    glScalef(1.0f / mLastRenderScale, 1.0f / mLastRenderScale, 1.0f);
    glTranslatef(-mLastRenderX, -tRenderY, tRenderZ);
    glColor3f(1.0f, 1.0f, 1.0f);

    //--Finish up.
    return tRenderSlot;
}
int VisualLevel::RenderBitmapInWorldRot(int pSlot, float pActorAlpha, SugarBitmap *pBitmap)
{
    //--Alternative rendering function, assumes that the player is looking at objects in a 3D space instead of
    //  a 2D space. If the player rotates their camera left or right, then the entities rotate along with it.
    //  This looks a bit more natural, but relies on the radar for navigation.
    //--First, get the current rotational state.
    float tDummyX, tDummyY;
    CameraManager::FetchActiveCamera3D()->Get3DRotation(tDummyX, tDummyY, mCurrentRotation);

    //--Calculate the difference.
    float tRotationDif = mInitialRotation - mCurrentRotation;

    //--Which slot the entity is in determines its world coordinates. Note that the selected entity is not
    //  used for rendering here, and it will be skipped during click detection as well.
    int tRenderSlot = pSlot;
    tRotationDif = tRotationDif + (30.0f * (pSlot));

    //--Convert the rotation to radians.
    tRotationDif = tRotationDif * TORADIAN;

    //--Compute the fake 3D coordinates. The sin is the distance from the screen, the cos is the position on screen.
    float cRadius = VIRTUAL_CANVAS_X * 0.80f;
    float tFakeX = cosf(tRotationDif) * cRadius;
    float tFakeY = sinf(tRotationDif) * cRadius;

    //--Objects which are behind us don't render.
    if(tFakeY < 0.0f) return -1;

    //--Compute the center point of the field. It is slightly offset, since the console occupies the bottom.
    float cCenterX = VIRTUAL_CANVAS_X * 0.5f;
    float cCenterY = VIRTUAL_CANVAS_Y * 0.45f;

    //--Finally, translate to canvas coordinates.
    mLastRenderX = cCenterX + tFakeX;
    float tRenderY = cCenterY;

    //--Scale, clamps downwards to 0.25f.
    mLastRenderScale = 0.65f - (fabsf(cCenterX - mLastRenderX) * 0.0005f);
    if(mLastRenderScale < 0.55f) mLastRenderScale = 0.55;
    if(mLastRenderScale > 0.65f) mLastRenderScale = 0.65;
    float tRenderZ = 0.5f - (mLastRenderScale * 0.01f);

    //--[Rendering]
    //--Reposition to the center of the rendering point.
    glTranslatef(mLastRenderX, tRenderY, -tRenderZ);
    glScalef(mLastRenderScale, mLastRenderScale, 1.0f);

    //--Sizes.
    float cWid = pBitmap->GetTrueWidth()  * 0.5f;
    float cHei = pBitmap->GetTrueHeight() * 0.5f;

    //--If requested, store the location that the image occupies on the screen. This will allow the
    //  UI to dynamically set click hitboxes, including scaling them.
    if(mRefreshClickPoints)
    {
        //--Modify.
        cWid = cWid * mLastRenderScale;
        cHei = cHei * mLastRenderScale;

        //--Set. Store the previous width.
        mClickPoints[tRenderSlot].Set(mLastRenderX - cWid, tRenderY - cHei, mLastRenderX + cWid, tRenderY + cHei);

        //--Clean.
        cWid = cWid / mLastRenderScale;
        cHei = cHei / mLastRenderScale;
    }
    else
    {
    }

    //--Compute alpha/fading.
    float cColor = 1.0f - (0.25f * (1.0f - (mLastRenderScale / 0.75f)));
    glColor4f(cColor, cColor, cColor, pActorAlpha);

    //--Render.
    pBitmap->Draw(-cWid, -cHei);

    //--[Clean]
    glScalef(1.0f / mLastRenderScale, 1.0f / mLastRenderScale, 1.0f);
    glTranslatef(-mLastRenderX, -tRenderY, tRenderZ);
    glColor3f(1.0f, 1.0f, 1.0f);

    //--Finish up.
    return tRenderSlot;
}
void VisualLevel::RefreshEntityArray()
{
    //--[Documentation]
    //--Scans the entity list and places entities in the mIDListing array so they are closest to the player's
    //  line-of-sight. This can also remove entities that are no longer in the slots.
    //--Once this function is called, it can safely be assumed that all entities are legal or NULL.
    EntityManager *rEntityManager = EntityManager::Fetch();
    Actor *rPlayerActor = rEntityManager->GetLastPlayerEntity();
    if(!rPlayerActor) return;

    //--[Setup]
    //--We need a room. If the acting entity is not in a room, we have nothing to display.
    PandemoniumRoom *rActiveRoom = rPlayerActor->GetCurrentRoom();
    if(!rActiveRoom) return;

    //--Entity/Container list.
    SugarLinkedList *rEntityList = rActiveRoom->GetEntityList();
    SugarLinkedList *rContainerList = rActiveRoom->GetContainerList();
    SugarLinkedList *tItemPackList = new SugarLinkedList(false);

    //--Inventory, compressed packs.
    for(int i = 0; i < rActiveRoom->GetCompressedInventorySize(); i ++)
    {
        //--Get the item in the slot.
        InventoryPack *rItemPack = rActiveRoom->GetItemPack(i);
        if(!rItemPack || !rItemPack->rItem) continue;
        tItemPackList->AddElement("X", rItemPack);
    }

    //--[Removal]
    //--First, remove entities that are no longer visible.
    for(int i = 0; i < VL_ENTITIES_PER_SCREEN; i ++)
    {
        //--If the slot is empty already, skip it.
        if(!mUnifiedRenderListing[i].rActorPtr && !mUnifiedRenderListing[i].rContainerPtr && !mUnifiedRenderListing[i].rInventoryPackPtr) continue;

        //--Check if the Actor in question exists. If so, we're done.
        if(rEntityList->IsElementOnList(mUnifiedRenderListing[i].rActorPtr)) continue;

        //--If the Container in question exists, we're done.
        if(rContainerList->IsElementOnList(mUnifiedRenderListing[i].rContainerPtr)) continue;

        //--If the InventoryPack in question exists, we're done.
        if(tItemPackList->IsElementOnList(mUnifiedRenderListing[i].rInventoryPackPtr)) continue;

        //--If we got this far, the object referenced by the unified listing no longer exists. Remove it.
        memset(&mUnifiedRenderListing[i], 0, sizeof(UnifiedObjectPack));
    }

    //--[Location]
    //--Now that all entities have been removed that were no longer visible, create a list of new UnifiedObjectPacks and
    //  insert them into the new list. Any packs that are still on the list will get dropped as there's no space to display them.
    SugarLinkedList *tAdditionList = AssembleNewEntityList(rEntityList, tItemPackList, rContainerList);

    //--[Addition]
    //--Now insert them onto the list.
    for(int i = 0; i < VL_ENTITIES_PER_SCREEN; i ++)
    {
        //--If this slot is already occupied, skip it.
        if(mUnifiedRenderListing[i].rActorPtr || mUnifiedRenderListing[i].rContainerPtr || mUnifiedRenderListing[i].rInventoryPackPtr) continue;

        //--No more objects to remove? Done.
        if(tAdditionList->GetListSize() < 1) break;

        //--Liberate the 0th entry from the addition list and put it here.
        tAdditionList->SetRandomPointerToHead();
        UnifiedObjectPack *tInsertObject = (UnifiedObjectPack *)tAdditionList->LiberateRandomPointerEntry();
        memcpy(&mUnifiedRenderListing[i], tInsertObject, sizeof(UnifiedObjectPack));

        //--Deallocate the unneeded object.
        free(tInsertObject);
    }

    //--[Clean Up]
    //--Remove the temporary lists.
    delete tItemPackList;
    delete tAdditionList;
}
SugarLinkedList *VisualLevel::AssembleNewEntityList(SugarLinkedList *pEntityList, SugarLinkedList *pInvPackList, SugarLinkedList *pContainerList)
{
    //--Creates a new SugarLinkedList containing all entities not currently in the visible display but that
    //  are in the room that the player can see. This is a pointer reference list.
    //--It is valid for the list to contain no entries, but the list will always exist and must be deallocated.
    SugarLinkedList *nList = new SugarLinkedList(true);

    //--Entities.
    if(pEntityList)
    {
        //--Iterate.
        Actor *rCheckActor = (Actor *)pEntityList->PushIterator();
        while(rCheckActor)
        {
            //--Check if this entity is already represented on the visible list. If so, ignore it.
            bool tIsAlreadyVisible = false;
            for(int i = 0; i < VL_ENTITIES_PER_SCREEN; i ++)
            {
                if(mUnifiedRenderListing[i].rActorPtr == rCheckActor)
                {
                    tIsAlreadyVisible = true;
                    break;
                }
            }

            //--Never render certain actors.
            if(!ShouldEntityRender(rCheckActor))
            {
                tIsAlreadyVisible = true;
            }

            //--If we got this far, the object is not already visible, so add it.
            if(!tIsAlreadyVisible)
            {
                SetMemoryData(__FILE__, __LINE__);
                UnifiedObjectPack *nPack = (UnifiedObjectPack *)starmemoryalloc(sizeof(UnifiedObjectPack));
                memset(nPack, 0, sizeof(UnifiedObjectPack));
                nPack->rActorPtr = rCheckActor;
                nList->AddElement("X", nPack, &FreeThis);
            }

            //--Next.
            rCheckActor = (Actor *)pEntityList->AutoIterate();
        }
    }

    //--InventoryPacks.
    if(pInvPackList)
    {
        //--Iterate.
        InventoryPack *rCheckPack = (InventoryPack *)pInvPackList->PushIterator();
        while(rCheckPack)
        {
            //--Check if this entity is already represented on the visible list. If so, ignore it.
            bool tIsAlreadyVisible = false;
            for(int i = 0; i < VL_ENTITIES_PER_SCREEN; i ++)
            {
                if(mUnifiedRenderListing[i].rInventoryPackPtr == rCheckPack)
                {
                    tIsAlreadyVisible = true;
                    break;
                }
            }

            //--If we got this far, the object is not already visible, so add it.
            if(!tIsAlreadyVisible)
            {
                SetMemoryData(__FILE__, __LINE__);
                UnifiedObjectPack *nPack = (UnifiedObjectPack *)starmemoryalloc(sizeof(UnifiedObjectPack));
                memset(nPack, 0, sizeof(UnifiedObjectPack));
                nPack->rInventoryPackPtr = rCheckPack;
                nList->AddElement("X", nPack, &FreeThis);
            }

            //--Next.
            rCheckPack = (InventoryPack *)pInvPackList->AutoIterate();
        }
    }

    //--Containers.
    if(pContainerList)
    {
        //--Iterate.
        WorldContainer *rCheckContainer = (WorldContainer *)pContainerList->PushIterator();
        while(rCheckContainer)
        {
            //--Check if this entity is already represented on the visible list. If so, ignore it.
            bool tIsAlreadyVisible = false;
            for(int i = 0; i < VL_ENTITIES_PER_SCREEN; i ++)
            {
                if(mUnifiedRenderListing[i].rContainerPtr == rCheckContainer)
                {
                    tIsAlreadyVisible = true;
                    break;
                }
            }

            //--If we got this far, the object is not already visible, so add it.
            if(!tIsAlreadyVisible)
            {
                SetMemoryData(__FILE__, __LINE__);
                UnifiedObjectPack *nPack = (UnifiedObjectPack *)starmemoryalloc(sizeof(UnifiedObjectPack));
                memset(nPack, 0, sizeof(UnifiedObjectPack));
                nPack->rContainerPtr = rCheckContainer;
                nList->AddElement("X", nPack, &FreeThis);
            }

            //--Next.
            rCheckContainer = (WorldContainer *)pContainerList->AutoIterate();
        }
    }

    //--Pass it back.
    return nList;
}
