//--Base
#include "VisualLevel.h"

//--Classes
//--CoreClasses
#include "SugarBitmap.h"
#include "SugarFont.h"
#include "SugarLinkedList.h"

//--Definitions
#include "EasingFunctions.h"

//--GUI
//--Libraries
//--Managers
#include "DisplayManager.h"

//--The console rendering subroutine is modified to make it easier on the eyes in 3D mode. It takes up less screen space by
//  virtue of being transparent, and the text uses a fixed-width font.
//--It got its own file because it's longer than the other UI functions.

//--[Append]
void VisualLevel::AppendToConsole(const char *pString, float pTextSize, StarlightColor pColor)
{
    //--Basically identical to the parent class version, except sizes things differently since the console uses a different font.
    if(!pString || !Images.mIsReady) return;

    //--Resolve which font to use.
    SugarFont *rConsoleFont = PandemoniumLevel::Images.Data.rUIFontMedium;
    if(PandemoniumLevel::xUseSmallConsoleFont) rConsoleFont = PandemoniumLevel::Images.Data.rUIFontSmall;

    //--Create the string pack.
    SetMemoryData(__FILE__, __LINE__);
    ConsoleString *nStringPack = (ConsoleString *)starmemoryalloc(sizeof(ConsoleString));
    nStringPack->Initialize();
    nStringPack->mHasBreak = xNextLineHasBreak;
    nStringPack->mTextSize = pTextSize;
    memcpy(&nStringPack->mColor, &pColor, sizeof(StarlightColor));

    //--Get the length of the string. If necessary, cut the string up and paste it multiple times.
    const float cBufferLen = 15.0f;
    float tLength = ConsolePane.cWid - (ConsolePane.cIndent * 2.0f) - (cBufferLen * 3.0f);
    if(mShowStoryDisplay) tLength = 1320.0f;

    //--Get the length of the string.
    float tStringLen = rConsoleFont->GetTextWidth(pString);
    if(tStringLen > tLength)
    {
        //--Setup.
        int tCursor = 0;
        int tRunningCursor = 0;

        //--Move characters into a new buffer until the string length is hit.
        char tBuffer[256];
        while(tCursor < 255 && tRunningCursor < (int)strlen(pString))
        {
            //--Append the letter.
            tBuffer[tCursor+0] = pString[tRunningCursor];
            tBuffer[tCursor+1] = '\0';

            //--Check the length.
            float tNewLength = rConsoleFont->GetTextWidth(tBuffer);

            //--If it's too long, we're done.
            if(tNewLength > tLength) break;

            //--Otherwise, go up one.
            tCursor ++;
            tRunningCursor ++;
        }

        //--Now scan backwards until we hit a space. This is the cutoff point.
        int tCutoffLetter = -1;
        for(int i = (int)strlen(tBuffer) - 1; i >= 0; i --)
        {
            if(tBuffer[i] == ' ')
            {
                tCutoffLetter = i+1;
                break;
            }
        }

        //--If the cutoff letter is -1, then no spaces were found. Just hard-break on the letter.
        if(tCutoffLetter == -1)
        {
            //--Flags.
            xNextLineHasBreak = false;

            //--Add the buffer.
            nStringPack->mString = InitializeString("%s", tBuffer);
            if(!mShowStoryDisplay)
                mConsoleStrings->AddElementAsHead("X", nStringPack, &ConsoleString::DeleteThis);
            else
                AppendStoryString(nStringPack);

            //--Append the rest of the string as its own string.
            int tStartLetter = (int)strlen(nStringPack->mString);
            PandemoniumLevel::AppendToConsole(&pString[tStartLetter]);
        }
        //--Otherwise, cutoff on the space.
        else
        {
            //--Flags.
            xNextLineHasBreak = false;

            //--Cut the buffer.
            tBuffer[tCutoffLetter] = '\0';

            //--Add the buffer.
            nStringPack->mString = InitializeString("%s", tBuffer);
            if(!mShowStoryDisplay)
                mConsoleStrings->AddElementAsHead("X", nStringPack, &ConsoleString::DeleteThis);
            else
                AppendStoryString(nStringPack);

            //--Append the rest of the string as its own string.
            int tStartLetter = (int)strlen(nStringPack->mString);
            PandemoniumLevel::AppendToConsole(&pString[tStartLetter]);
        }

    }
    //--If the string wasn't wider than the console, just append it.
    else
    {
        //--Flags.
        xNextLineHasBreak = false;

        //--Append it.
        nStringPack->mString = InitializeString("%s", pString);
        if(!mShowStoryDisplay)
            mConsoleStrings->AddElementAsHead("X", nStringPack, &ConsoleString::DeleteThis);
        else
            AppendStoryString(nStringPack);
    }
}

//--[Rendering]
void VisualLevel::RenderConsolePane()
{
    //--[Documentation]
    //--Same as the regular rendering but can offset based on timers. A button is provided to show
    //  or hide the console. Also we modify the color to make it better in 3D.
    if(!Images.mIsReady) return;

    //--Resolve which font to use.
    SugarFont *rConsoleFont = PandemoniumLevel::Images.Data.rUIFontMedium;
    if(PandemoniumLevel::xUseSmallConsoleFont) rConsoleFont = PandemoniumLevel::Images.Data.rUIFontSmall;

    float cCurrentOffset = EasingFunction::QuadraticInOut(mConsoleOffsetTimer, VL_CONSOLE_SLIDE_TICKS) * mConsoleOffsetY;

    //--[Show/Hide Button]
    //--Render the button at the top of the console. This allows the user to show or hide the console if desired.
    if(mConsoleOffsetTimer < VL_CONSOLE_SLIDE_TICKS)
    {
        PandemoniumLevel::Images.Data.rHideConsole->Draw(ConsolePane.cLft + Images.Data.rSkipTurnBtn->GetWidth(), cCurrentOffset + ConsolePane.cTop - PandemoniumLevel::Images.Data.rHideConsole->GetTrueHeight());
    }
    //--Showing case. Doesn't render console.
    else
    {
        PandemoniumLevel::Images.Data.rShowConsole->Draw(ConsolePane.cLft + Images.Data.rSkipTurnBtn->GetWidth(), cCurrentOffset + ConsolePane.cTop - PandemoniumLevel::Images.Data.rShowConsole->GetTrueHeight());
    }

    //--[Skip Turn Button]
    //--Aligns with the console, so it renders here.
    Images.Data.rSkipTurnBtn->Draw(ConsolePane.cLft, cCurrentOffset + ConsolePane.cTop - Images.Data.rSkipTurnBtn->GetHeight());
    mSkipTurnBtn.SetWH(ConsolePane.cLft, cCurrentOffset + ConsolePane.cTop - Images.Data.rSkipTurnBtn->GetHeight(), Images.Data.rSkipTurnBtn->GetWidth(), Images.Data.rSkipTurnBtn->GetHeight());

    //--If the console is fully hidden, then stop rendering here.
    if(mConsoleOffsetTimer == VL_CONSOLE_SLIDE_TICKS) return;

    //--[Border Rendering]
    //--Much of this is the same basic code from PandemoniumLevel, but modified to handle the different color
    //  scheme used by the new console. The text is backed to make it stand out more, and is white-on-black.
    float cSizeFactor = 1.0f;

    //--Position setup.
    glTranslatef(0.0f, cCurrentOffset, 0.0f);
    glTranslatef(ConsolePane.cLft, ConsolePane.cTop, 0.0f);

    //--Border.
    RenderBorderCardOver(0.0f, 0.0f, ConsolePane.cWid, ConsolePane.cHei, 0x01FF);

    //--Clean up.
    glColor3f(1.0f, 1.0f, 1.0f);
    glEnable(GL_TEXTURE_2D);

    //--[Console Rendering]
    //--Activate the shader. This makes the text have an automatic backing color. It looks great on transparent backgrounds!

    //--Render as much of the console as we can get away with. The most recent entry is at the
    //  bottom of the console.

    //--Sizes.
    float tStepRate = cFontSpacing * -1.15f * cSizeFactor;
    float tCursor = ConsolePane.cHei - ConsolePane.cIndent + tStepRate - 15.0f;

    //--If this is present, then print "Press any key to continue".
    if(mIsConsoleWaitingOnKeypress)
    {
        //--Setup.
        glTranslatef(ConsolePane.cIndent, tCursor, 0.0f);
        glScalef(cSizeFactor, cSizeFactor, 1.0f);

        //--Render.
        glColor3f(1.0f, 0.0f, 0.0f);
        rConsoleFont->DrawTextArgs(0, 0, 0, 1.0f, "Press any key to continue.");

        //--Clean.
        glScalef(1.0f / cSizeFactor, 1.0f / cSizeFactor, 1.0f);
        glTranslatef(-ConsolePane.cIndent, -tCursor, 0.0f);

        //--Position for the next line.
        tCursor = tCursor + tStepRate;
    }

    //--Loop through strings. Stop if no string is found, or if we go over the edge.
    int i = 0;
    ConsoleString *rString = (ConsoleString *)mConsoleStrings->PushIterator();
    while(rString)
    {
        //--If a string consists of exactly one space, it's a padding line. Render it at half-size, and
        //  don't render it at all if this is the 0th line.
        if(rString->mString[0] == ' ' && rString->mString[1] == '\0')
        {
            if(i > 0) tCursor = tCursor - ((20.0f * cSizeFactor) - 2.0f) * 0.5f;
            i ++;
            rString = (ConsoleString *)mConsoleStrings->AutoIterate();
            continue;
        }

        //--Position setup.
        glTranslatef(ConsolePane.cIndent * 2.5f, tCursor, 0.0f);
        glScalef(cSizeFactor, cSizeFactor, 1.0f);

        //--Render.
        rString->mColor.SetAsMixer();
        if(rString->mColor.r == 0.0f && rString->mColor.g == 0.0f && rString->mColor.b == 0.0f) glColor3f(1.0f, 1.0f, 1.0f);
        rConsoleFont->DrawText(0, 0, 0, rString->mString);

        //--Clean.
        glScalef(1.0f / cSizeFactor, 1.0f / cSizeFactor, 1.0f);
        glTranslatef(ConsolePane.cIndent * -2.5f, -tCursor, 0.0f);

        //--Ending case check.
        //tCursor = tCursor - ((rString->mTextSize * cSizeFactor) + 2.0f);
        tCursor = tCursor - (20.0f * cSizeFactor) - 2.0f;
        if(tCursor < ConsolePane.cIndent - (tStepRate * 0.7f))
        {
            mConsoleStrings->PopIterator();
            break;
        }

        //--Next.
        i ++;
        rString = (ConsoleString *)mConsoleStrings->AutoIterate();
    }

    //--Final clean up.
    DisplayManager::Fetch()->ActivateProgram(NULL);
    glTranslatef(-ConsolePane.cLft, -ConsolePane.cTop, 0.0f);
    glTranslatef(0.0f, -cCurrentOffset, 0.0f);
    glColor3f(1.0f, 1.0f, 1.0f);
}
