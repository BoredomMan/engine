//--Base
#include "VisualLevel.h"

//--Classes
#include "WADFile.h"
#include "ZoneEditor.h"
#include "ZoneNode.h"

//--CoreClasses
//--Definitions
//--GUI
//--Libraries
//--Managers
#include "CameraManager.h"
#include "ControlManager.h"
#include "DisplayManager.h"
#include "DebugManager.h"

//--Handles movement. This can be done either by direct controls, or by instruction sequences. Also
//  handles things like visual bobbing.

//#define VL_MOVE_DEBUG
#ifdef VL_MOVE_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(true, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

void VisualLevel::HandleMovement()
{
    //--[Entry]
    //--Entry point, called during the update cycle. Requires a WADFile to be used for level geometry.
    //  If no WADFile exists, then movement is redundant, just teleport!
    if(!mLocalWADFile) return;

    //--[Variables]
    //--Debug.
    DebugPush(true, "VisualLevel:HandleMovement - Beginning movement handler.\n");

    //--Setup.
    ControlState *rControlState;
    ControlManager *rControlManager = ControlManager::Fetch();

    //--To determine which direction to move, we need the SugarCamera's rotation.
    float tXRot, tYRot, tZRot;
    SugarCamera3D *rActiveCamera = CameraManager::FetchActiveCamera3D();
    rActiveCamera->Get3DRotation(tXRot, tYRot, tZRot);

    //--[Mouselook]
    //--Move the player's viewing angle based on the movement of the mouse.
    DebugPrint("Running mouselook computations.\n");
    int tNewMouseX, tNewMouseY, tNewMouseZ;
    rControlManager->GetMouseCoords(tNewMouseX, tNewMouseY, tNewMouseZ);

    //--Mouse movement.
    float tXMove = 0.0f;
    float tYMove = 0.0f;
    if(mOldMouseX != -100.0f)
    {
        tXMove = tNewMouseX - mOldMouseX;
        tYMove = tNewMouseY - mOldMouseY;
    }

    //--If the mouse was unlocked, the mouselook never occurs.
    bool tIgnoreKeyboardInput = false;
    if(!DisplayManager::Fetch()->IsMouseLocked() || !DisplayManager::Fetch()->HasGrabbedMouse())
    {
        //--Cancel mouselook.
        tXMove = 0.0f;
        tYMove = 0.0f;

        //--Update the node editor.
        tIgnoreKeyboardInput = mZoneEditor->Update();
        HandlePlayerReposition();
    }
    else
    {
        //if(fabsf(tYMove) > 50.0f) fprintf(stderr, "%f - %i %.0f\n", tYMove, tNewMouseY, mOldMouseY);
    }

    //--Store the mouse positions for the next tick.
    mOldMouseX = DisplayManager::xMouseLockX;
    mOldMouseY = DisplayManager::xMouseLockY;

    //--Clamp. On the first tick we read mouse information, the drivers might spit insane data.
    if(tXMove < -1000.0f || tXMove > 1000.0f || tYMove < -1000.0f || tYMove > 1000.0f)
    {
        tXMove = 0.0f;
        tYMove = 0.0f;
    }

    //--Rotation Handling
    float cMouseSensitivity = 1.0f;
    tZRot = tZRot - (tXMove * cMouseSensitivity);
    tXRot = tXRot - (tYMove * cMouseSensitivity);

    //--Clamp rotations. It's really weird to look up and keep going.
    if(tXRot <=   0.0f) tXRot =   0.0f;
    if(tXRot >= 180.0f) tXRot = 180.0f;

    //--Set the rotations.
    if(tXMove != 0.0f || tYMove != 0.0f) rActiveCamera->SetRotation(tXRot, tYRot, tZRot);

    //--[Movement]
    //--Movement only counts in the horizontal plane. We don't care about the X since the player cannot
    //  manually move up and down.
    //--First, increase the player's movement speed if they're holding the run key.
    DebugPrint("Handling movement.\n");
    float tSpeedMultiplier = 1.0f;
    if(rControlManager->IsDown("Run"))
    {
        tSpeedMultiplier = 2.0f;
    }

    //--Forward
    rControlState = rControlManager->GetControlState("Up");
    if(rControlState->mIsDown && !tIgnoreKeyboardInput)
    {
        //--Unlike the free-camera, we don't care if the player is looking up or down.
        float tz2 = 1.0f;

        //Rotate around X-axis:
        mPlayerX = mPlayerX - (tz2 * sin(tZRot * TORADIAN) * mPlayerMoveSpeed * tSpeedMultiplier);
        mPlayerY = mPlayerY - (tz2 * cos(tZRot * TORADIAN) * mPlayerMoveSpeed * tSpeedMultiplier);
    }

    //--Backwards.
    rControlState = rControlManager->GetControlState("Down");
    if(rControlState->mIsDown && !tIgnoreKeyboardInput)
    {
        //--Unlike the free-camera, we don't care if the player is looking up or down.
        float tz2 = 1.0f;

        //--Rotate around X-axis:
        mPlayerX = mPlayerX + (tz2 * sin(tZRot * TORADIAN) * mPlayerMoveSpeed * tSpeedMultiplier);
        mPlayerY = mPlayerY + (tz2 * cos(tZRot * TORADIAN) * mPlayerMoveSpeed * tSpeedMultiplier);
    }

    //--Sidestep left.
    rControlState = rControlManager->GetControlState("Left");
    if(rControlState->mIsDown && !tIgnoreKeyboardInput)
    {
        //Rotate around X-axis:
        tZRot = tZRot + 90;
        mPlayerX = mPlayerX - (sin(tZRot * TORADIAN) * mPlayerMoveSpeed * tSpeedMultiplier);
        mPlayerY = mPlayerY - (cos(tZRot * TORADIAN) * mPlayerMoveSpeed * tSpeedMultiplier);
        tZRot = tZRot - 90;
    }

    //--Sidestep right.
    rControlState = rControlManager->GetControlState("Right");
    if(rControlState->mIsDown && !tIgnoreKeyboardInput)
    {
        //--Rotate around X-axis:
        tZRot = tZRot + 90;
        mPlayerX = mPlayerX + (sin(tZRot * TORADIAN) * mPlayerMoveSpeed * tSpeedMultiplier);
        mPlayerY = mPlayerY + (cos(tZRot * TORADIAN) * mPlayerMoveSpeed * tSpeedMultiplier);
        tZRot = tZRot - 90;
    }

    //--[Height Checking]
    //--Now that we've repositioned we need to move the player's height. The player moves upwards instantly
    //  since that's considered to be climbing, but falls downwards under gravity. The WADFile will tell us
    //  the lowest height the player can be at when moving.
    DebugPrint("Running height check.\n");

    //--Position and setup.
    mIsPlayerOnGround = false;
    const float cGravity = -0.50f;
    float cClimbRange = 12.0f;
    float tExpectedHeight = (mLocalWADFile->GetHeightAtPoint(mPlayerX, mPlayerY, mPlayerZ));
    float tExpectedHeightUp = (mLocalWADFile->GetHeightAtPoint(mPlayerX, mPlayerY, mPlayerZ+cClimbRange));

    //--Move up. This is always instant, since we're assumed to be climbing a step.
    if(tExpectedHeight > mPlayerZ || tExpectedHeightUp > mPlayerZ)
    {
        mIsPlayerOnGround = true;
        mPlayerZSpeed = 0.0f;
        mPlayerZ = tExpectedHeight;
        if(tExpectedHeightUp > tExpectedHeight) mPlayerZ = tExpectedHeightUp;
    }
    //--Move down. This follows gravity.
    else if(tExpectedHeight < mPlayerZ + mPlayerZSpeed)
    {
        //--Check the downstep case. If we are moving down 12 or fewer units, and have just exited the ground....
        if(mIsPlayerOnGround && tExpectedHeight >= mPlayerZ - cClimbRange)
        {
            mIsPlayerOnGround = true;
            mPlayerZSpeed = 0.0f;
            mPlayerZ = tExpectedHeight;
        }
        //--Falling case.
        else
        {
            mIsPlayerOnGround = false;
            mPlayerZSpeed = mPlayerZSpeed + cGravity;
            mPlayerZ = mPlayerZ + mPlayerZSpeed;
        }
    }
    //--Close enough, match position exactly.
    else
    {
        mIsPlayerOnGround = true;
        mPlayerZSpeed = 0.0f;
        mPlayerZ = tExpectedHeight;
    }

    //--Jumping. Can only be used if the player is currently on the ground.
    if(rControlManager->IsFirstPress("Jump") && mIsPlayerOnGround && !tIgnoreKeyboardInput)
    {
        mPlayerZSpeed = 7.5f;
    }

    //--Reposition the camera to be in the player's eyes.
    rActiveCamera->SetPosition(-mPlayerX, -mPlayerY, -(mPlayerZ + PLAYER_HEIGHT));

    //--[System]
    //--When the player presses this key, we disable mouselook and unlock the mouse.
    DebugPrint("Running system checks.\n");
    if(rControlManager->IsFirstPress("Escape"))
    {
        //--Setup.
        DisplayManager *rDisplayManager = DisplayManager::Fetch();

        //--Is the mouse locked? Unlock it.
        if(rDisplayManager->IsMouseLocked())
        {
            rDisplayManager->UnlockMouse();
            rDisplayManager->ShowHardwareMouse();
        }
        //--Mouse is unlocked, so lock it.
        else
        {
            rDisplayManager->LockMouse();
            rDisplayManager->HideHardwareMouse();
        }
    }

    //--[Finish]
    //--Debug.
    DebugPop("VisualLevel:HandleMovement - Movement handler complete.\n");
}
