//--Base
#include "VisualRoom.h"

//--Classes
#include "Actor.h"
#include "VisualLevel.h"
#include "ZoneNode.h"

//--CoreClasses
#include "SugarLinkedList.h"

//--Definitions
#include "Global.h"
#include "HitDetection.h"

//--GUI
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "CameraManager.h"
#include "DebugManager.h"
#include "EntityManager.h"
#include "OptionsManager.h"

//=========================================== System ==============================================
VisualRoom::VisualRoom()
{
    //--[RootObject]
    //--System
    mType = POINTER_TYPE_VISUAL_ROOM;

    //--[PandemoniumRoom]
    //--System
    //--Position
    //--Connectivity
    //--Entities
    //--Zone Effects
    //--Inventory
    //--Container Listing
    //--Rendering
    //--Public Variables

    //--[VisualRoom]
    //--System
    mStoredNodeID = 0;

    //--Connection Building
    memset(mConnectionNodeIDs, 0, sizeof(uint32_t) * NAV_TOTAL);

    //--Public Variables
    mIsHighlighted = false;
}
VisualRoom::~VisualRoom()
{
}

//--[Public Statics]
//--Determines how spaced the rooms on the radar are. The lower it is, the more bunched together the
//  rooms will appear to be. When zooming in, it should be increased to avoid clutter.
float VisualRoom::xDistanceScale = 0.20f;

//====================================== Property Queries =========================================
bool VisualRoom::IsOfType(int pType)
{
    //--VisualRoom can be both a PandemoniumRoom and a VisualRoom.
    if(pType == POINTER_TYPE_PANDEMONIUM_ROOM) return true;
    return (pType == mType);
}
uint32_t VisualRoom::GetAssociatedNodeID()
{
    return mStoredNodeID;
}

//========================================= Manipulators ==========================================
void VisualRoom::SetPositionByNode(const char *pNodeName)
{
    //--Sets the position based on a node found in the construction VisualLevel, assuming it exists.
    //  On error, does not modify the position.
    if(!pNodeName || !VisualLevel::xrConstructionLevel) return;

    //--Get the node. It can be NULL if the name isn't found. Names are not case-sensitive.
    ZoneNode *rNode = VisualLevel::xrConstructionLevel->GetNodeByName(pNodeName);
    if(!rNode)
    {
        DebugManager::ForcePrint("VisualRoom:SetPositionByNode - Error, no node %s.\n", pNodeName);
        return;
    }

    //--Set position.
    mStoredNodeID = rNode->GetNodeID();
    SetPosition(rNode->GetX() * WAD_TO_VR_SCALE, rNode->GetY() * WAD_TO_VR_SCALE * -1.0f, mWorldZ);

    //--Debug.
    //DebugManager::Print("Pos: %s Node: %s - %.0f %.0f\n", mLocalName, pNodeName, mWorldX, mWorldY);

    //--Connections are based on the Node's transitions. If a target node has an auto-transition then
    //  we go to the end of the sequence, not the target node.
    int tBuildNode = 0;
    int tFacingsTotal = rNode->GetFacingsTotal();
    for(int i = 0; i < tFacingsTotal; i ++)
    {
        //--Get the facing.
        FacingInfo *rFacingInfo = rNode->GetFacing(i);
        if(!rFacingInfo) continue;

        //--Get the node this facing goes to. Facings that go to node 0 don't go anywhere and are ignored.
        if(rFacingInfo->mNodeDestination == 0) continue;

        //--Get the target node. The subroutine will follow across auto-transitions until the end is found.
        uint32_t tEndNodeID = FollowIDTrail(rNode->GetNodeID(), rFacingInfo->mNodeDestination, VisualLevel::xrConstructionLevel);
        ZoneNode *rEndNode = VisualLevel::xrConstructionLevel->GetNodeByID(tEndNodeID);

        //--Now set the next connection's build node to this target.
        if(rEndNode) mConnectionNodeIDs[tBuildNode] = rEndNode->GetNodeID();

        //--Clamp, never have more than six connections.
        tBuildNode ++;
        if(tBuildNode >= NAV_TOTAL) break;
    }
}

//========================================= Core Methods ==========================================
void VisualRoom::BuildConnectivity(int pRoomsTotal, PandemoniumRoom **pRoomList)
{
    //--Replaces the existing BuildConnectivity() behavior. VisualRooms are expected to be using
    //  nodes and node IDs to build their connectivity information.
    for(int i = 0; i < NAV_TOTAL; i ++)
    {
        //--If the node target is 0, ignore it.
        if(!mConnectionNodeIDs[i]) continue;

        //--Otherwise, scan the list for the room with the matching ID.
        for(int p = 0; p < pRoomsTotal; p ++)
        {
            //--Error check.
            VisualRoom *rRoom = (VisualRoom *)pRoomList[p];
            if(!pRoomList[p] || pRoomList[p] == this || !pRoomList[p]->IsOfType(POINTER_TYPE_VISUAL_ROOM)) continue;

            //--If the node ID matches, set it.
            if(rRoom->GetAssociatedNodeID() == mConnectionNodeIDs[i])
            {
                DebugManager::Print(" Room %s connects to %s\n", mLocalName, pRoomList[p]->GetName());
                rConnection[i] = pRoomList[p];
                break;
            }
        }

        //--Nothing found, error!
        if(!rConnection[i])
        {
            //DebugManager::ForcePrint("Error: %s No connection %s\n", mLocalName, mConnectivityNames[i]);
        }
    }
}
bool VisualRoom::RecursivePather(PandemoniumRoom *pCurrentRoom, int pMovesSoFar, int *pMoveList)
{
    //--Recursive pathing function, same as the base class except uses facings instead of directions.
    if(!pCurrentRoom) return false;

    //--This room must be a VisualRoom.
    if(!pCurrentRoom->IsOfType(POINTER_TYPE_VISUAL_ROOM)) return false;
    VisualRoom *rCurrentVisualRoom = (VisualRoom *)pCurrentRoom;

    //--The VisualLevel is needed to find the pathing using nodes.
    VisualLevel *rVisualLevel = VisualLevel::Fetch();
    if(!rVisualLevel) return false;

    //--Get the pathing pack associated with this room.
    PathInstructions *rPack = GetPathingPackP(pCurrentRoom);
    if(!rPack) return false;
    DebugManager::Print("Room %p - Pack %s %p\n", pCurrentRoom, pCurrentRoom->GetName(), rPack);

    //--If the move distance is -1, then no pather has reached it yet. Immediately copy.
    if(rPack->mShortestPath == -1)
    {
        DebugManager::Print("First path created.\n");
        rPack->mShortestPath = pMovesSoFar;
        rPack->CopyPathList(pMovesSoFar, pMoveList);
        if(xFirstPath) pCurrentRoom->mPathDistance = pMovesSoFar;
    }
    //--Otherwise, only copy if the moves is shorter than the previous list. If this happens,
    //  the new path is shorter, so use that instead.
    else if(rPack->mShortestPath > pMovesSoFar)
    {
        DebugManager::Print("Shorter path located.\n");
        rPack->mShortestPath = pMovesSoFar;
        rPack->CopyPathList(pMovesSoFar, pMoveList);
        if(xFirstPath) pCurrentRoom->mPathDistance = pMovesSoFar;
    }
    //--If neither case is true, then we're done. There is already a shorter/equidistant path.
    else
    {
        DebugManager::Print("Done, shorter path.\n");
        return true;
    }

    //--Increment the moves count.
    pMovesSoFar ++;

    //--Node ID of the room in question.
    uint32_t tStoredID = rCurrentVisualRoom->GetAssociatedNodeID();

    //--For each of the connections, create a new list and run the recursive pather on that.
    for(int i = 0; i < NAV_TOTAL; i ++)
    {
        //--Follow the ID trail in case there are auto-transitions.
        uint32_t tFinalID = FollowIDTrail(tStoredID, rCurrentVisualRoom->mConnectionNodeIDs[i], rVisualLevel);
        if(tFinalID == 0) continue;

        //--Get the room associated with this ID.
        VisualRoom *rConnection = rVisualLevel->GetRoomByNodeID(tFinalID);

        //--If there's no connection here, skip it.
        if(!rConnection) continue;
        DebugManager::PushPrint(false, "Recurse %s\n", rConnection->GetName());

        //--Create a new list.
        SetMemoryData(__FILE__, __LINE__);
        int *tMoveList = (int *)starmemoryalloc(sizeof(int) * pMovesSoFar);
        for(int p = 0; p < pMovesSoFar - 1; p ++) tMoveList[p] = pMoveList[p];

        //--The last movement is the NAV_ movement.
        tMoveList[pMovesSoFar-1] = i;

        //--Run the recursive pather.
        RecursivePather(rConnection, pMovesSoFar, tMoveList);

        //--Clean.
        free(tMoveList);
        DebugManager::PopPrint("Done.\n");
    }

    //--Return true to indicate something happened.
    return true;
}
uint32_t VisualRoom::FollowIDTrail(uint32_t pStartNodeID, uint32_t pMoveToNodeID, VisualLevel *pLevel)
{
    //--Given a node ID, follows the auto-transitions along that node until none remain. Returns the
    //  ID of the final node in the sequence.
    //--Can legally return 0 on error, or if the given node tree doesn't actually go anywhere. Can also
    //  return pMoveToNodeID if the node doesn't have any auto-transitions.
    if(!pLevel) return 0;

    //--Setup.
    uint32_t tPrevNodeID = pStartNodeID;

    //--Begin following the trail.
    ZoneNode *rTargetNode = pLevel->GetNodeByID(pMoveToNodeID);
    while(rTargetNode && rTargetNode->GetAutoTransitionsTotal() > 0)
    {
        //--Store the target node for comparison reasons.
        void *rCheckPtr = rTargetNode;

        //--Loop through the auto-transitions. If one matches our previous node ID, then "move" to the next
        //  node in the sequence until we hit one with no auto-transitions.
        int tAutoTransitions = rTargetNode->GetAutoTransitionsTotal();
        for(int p = 0; p < tAutoTransitions; p ++)
        {
            //--Check.
            AutoTransitionInfo *rAutoTransInfo = rTargetNode->GetAutoTransition(p);
            if(!rAutoTransInfo) continue;

            //--Match?
            if(rAutoTransInfo->mPreviousNode == tPrevNodeID)
            {
                tPrevNodeID = rTargetNode->GetNodeID();
                rTargetNode = pLevel->GetNodeByID(rAutoTransInfo->mNodeDestination);
                break;
            }
        }

        //--If we got through the list and the node didn't change, we're at the destination.
        if(rCheckPtr == rTargetNode) break;
    }

    //--Return the ID of the target node. If the node didn't exist, return 0 (error).
    if(!rTargetNode) return 0;
    return rTargetNode->GetNodeID();
}
uint32_t VisualRoom::FindFirstID(uint32_t pStartNodeID, uint32_t pEndNode, VisualLevel *pLevel)
{
    //--Given a start and end node, finds the first node in the trail that connects them. When moving
    //  between rooms (who only store the final instance) it may be necessary to know which transition
    //  nodes are between them. This function finds that for you.
    //--Can legally return 0 if there is no way to join the two nodes along auto-transitions.
    if(!pLevel || !pStartNodeID || !pEndNode) return 0;

    //--Get the starting node.
    ZoneNode *rStartNode = pLevel->GetNodeByID(pStartNodeID);
    if(!rStartNode) return 0;

    //--Go through each of its facings and walk the node tree. If any of them land at the end node, then
    //  that's the node we want.
    //--Note that it's theoretically possible for multiple facings to lead to the same node. In that case,
    //  the first one found is returned.
    for(int i = 0; i < rStartNode->GetFacingsTotal(); i ++)
    {
        //--Get and check the facing info.
        FacingInfo *rFacingInfo = rStartNode->GetFacing(i);
        if(!rFacingInfo) continue;

        //--Walk this node tree.
        uint32_t tEndNode = FollowIDTrail(pStartNodeID, rFacingInfo->mNodeDestination, pLevel);
        if(tEndNode == pEndNode) return rFacingInfo->mNodeDestination;
    }

    //--If we got this far, then we can't find a path between the two nodes along auto-transitions.
    return 0;
}

//===================================== Private Core Methods ======================================
//============================================ Update =============================================
void VisualRoom::RespondToActorExit(Actor *pActor)
{
    //--Identical to the base class, but uses different names for the directions. They are resolved
    //  by facings, as opposed to directions.
    if(!pActor) return;

    //--There must be a VisualLevel to resolve the directional difference.
    VisualLevel *rVisualLevel = VisualLevel::Fetch();
    if(!rVisualLevel) return;

    //--The player must be in the current room, *not* the room of the Actor that just left. Just
    //  check if they're in the current room.
    Actor *rPlayerActor = EntityManager::Fetch()->GetLastPlayerEntity();
    if(!rPlayerActor || rPlayerActor->GetCurrentRoom() != this) return;

    //--Resolve the direction string.
    char *tDirectionString = NULL;
    VisualRoom *rTargetRoom = (VisualRoom *)pActor->GetCurrentRoom();
    if(!rTargetRoom || !rTargetRoom->IsOfType(POINTER_TYPE_VISUAL_ROOM)) return;

    //--Get the nodes associated with these.
    ZoneNode *rThisNode = rVisualLevel->GetNodeByID(mStoredNodeID);
    ZoneNode *rThatNode = rVisualLevel->GetNodeByID(rTargetRoom->GetAssociatedNodeID());
    if(!rThisNode || !rThatNode) return;

    //--Vertical check, takes priority.
    if(rThisNode->GetZ() > rThatNode->GetZ() + 32)
    {
        ResetString(tDirectionString, "down");
    }
    //--Above check.
    else if(rThisNode->GetZ() < rThatNode->GetZ() - 32)
    {
        ResetString(tDirectionString, "up");
    }
    //--Angle check.
    else
    {
        //--Compute the angle. Clamp it within the 360 degree range.
        float tAngle = (atan2f(rThatNode->GetY() - rThisNode->GetY(), rThatNode->GetX() - rThisNode->GetX()) * TODEGREE) - 90.0f;
        while(tAngle <    0.0f) tAngle = tAngle + 360.0f;
        while(tAngle >= 360.0f) tAngle = tAngle - 360.0f;

        //--North
        if(tAngle >= 315.0f || tAngle < 45.0f)
        {
            ResetString(tDirectionString, "north");
        }
        //--West
        else if(tAngle < 135.0f)
        {
            ResetString(tDirectionString, "west");
        }
        //--South
        else if(tAngle < 225.0f)
        {
            ResetString(tDirectionString, "south");
        }
        //--East
        else if(tAngle < 3155.0f)
        {
            ResetString(tDirectionString, "east");
        }
    }

    //--If this action was a fleeing one, change the printcode.
    if(pActor->WasLastMoveFlee())
    {
        //--Find the first entity that is hostile to the Actor. Use their name.
        const char *rUseName = NULL;
        Actor *rActor = (Actor *)mEntityList->PushIterator();
        while(rActor)
        {
            if(rActor != pActor && pActor->IsHostileTo(rActor))
            {
                rUseName = rActor->GetName();
                mEntityList->PopIterator();
                break;
            }

            rActor = (Actor *)mEntityList->AutoIterate();
        }

        pActor->PrintByCode(PRINTCODE_MOVE_FLEEING_EXIT, false, pActor->GetName(), rUseName, tDirectionString);
    }
    //--Normal movement action.
    else
    {
        pActor->PrintByCode(PRINTCODE_MOVE_EXIT, false, pActor->GetName(), "Name2", tDirectionString);
    }

    //--Clean up.
    free(tDirectionString);
}
void VisualRoom::RespondToActorEntry(Actor *pActor, PandemoniumRoom *pPreviousRoom)
{
    //--When an Actor changes which room it's in, this gets called with the Actor in question
    //  as the parameter. It should not be called with a NULL or it fails silently.
    if(!pActor || !pPreviousRoom) return;

    //--The pPreviousRoom must be of a VisualRoom type.
    if(!pPreviousRoom->IsOfType(POINTER_TYPE_VISUAL_ROOM)) return;
    VisualRoom *rPreviousVisualRoom = (VisualRoom *)pPreviousRoom;

    //--There must be a VisualLevel to resolve the directional difference.
    VisualLevel *rVisualLevel = VisualLevel::Fetch();
    if(!rVisualLevel) return;

    //--The player must be in the current room, *not* the room of the Actor that just left. Just
    //  check if they're in the current room.
    Actor *rPlayerActor = EntityManager::Fetch()->GetLastPlayerEntity();
    if(!rPlayerActor || rPlayerActor->GetCurrentRoom() != this) return;

    //--Resolve the direction string.
    char *tDirectionString = NULL;
    ZoneNode *rThisNode = rVisualLevel->GetNodeByID(mStoredNodeID);
    ZoneNode *rThatNode = rVisualLevel->GetNodeByID(rPreviousVisualRoom->GetAssociatedNodeID());
    if(!rThisNode || !rThatNode) return;

    //--Vertical check, takes priority.
    if(rThisNode->GetZ() > rThatNode->GetZ() + 32)
    {
        ResetString(tDirectionString, "below");
    }
    //--Above check.
    else if(rThisNode->GetZ() < rThatNode->GetZ() - 32)
    {
        ResetString(tDirectionString, "above");
    }
    //--Angle check.
    else
    {
        //--Compute the angle. Clamp it within the 360 degree range.
        float tAngle = (atan2f(rThatNode->GetY() - rThisNode->GetY(), rThatNode->GetX() - rThisNode->GetX()) * TODEGREE) - 90.0f;
        while(tAngle <    0.0f) tAngle = tAngle + 360.0f;
        while(tAngle >= 360.0f) tAngle = tAngle - 360.0f;

        //--North
        if(tAngle >= 315.0f || tAngle < 45.0f)
        {
            ResetString(tDirectionString, "the north");
        }
        //--West
        else if(tAngle < 135.0f)
        {
            ResetString(tDirectionString, "the west");
        }
        //--South
        else if(tAngle < 225.0f)
        {
            ResetString(tDirectionString, "the south");
        }
        //--East
        else if(tAngle < 3155.0f)
        {
            ResetString(tDirectionString, "the east");
        }
    }

    //--If this action was a fleeing one, change the printcode.
    bool tPrintedAnything = false;
    if(pActor->WasLastMoveFlee())
    {
        tPrintedAnything = pActor->PrintByCode(PRINTCODE_MOVE_FLEEING_ENTER, true, pActor->GetName(), mLocalName, tDirectionString);
    }
    //--Normal movement action.
    else
    {
        tPrintedAnything = pActor->PrintByCode(PRINTCODE_MOVE_ENTER, true, pActor->GetName(), mLocalName, tDirectionString);
    }

    //--If we printed anything (because the player was present) we should also spit out a list of
    //  the entities that are visible.
    if(tPrintedAnything && pActor == EntityManager::Fetch()->GetLastPlayerEntity())
    {
         Actor::PrintEntitiesInRoom(mEntityList);
    }

    //--Clean up.
    free(tDirectionString);
}

//=========================================== File I/O ============================================
//=========================================== Drawing =============================================
void VisualRoom::Render()
{
    //--Almost identical to the base class, but doesn't use the spacing variable. The spacing should
    //  be set by the nodes automatically.
    //--Also renders connectivity under the body, since we use centroids instead of edges to compute position.

    //--Reposition.
    glTranslatef(mWorldX * VisualRoom::xDistanceScale, mWorldY * VisualRoom::xDistanceScale, 0.0f);
    glTranslatef(0.50f, 0.50f, 0.0f);

    //--Render connectivity.
    glTranslatef(0.0f, 0.0f, VR_CONN_DEPTH);
    RenderConnections();
    glTranslatef(0.0f, 0.0f, -VR_CONN_DEPTH);

    //--Render the body.
    glTranslatef(0.0f, 0.0f, VR_BODY_DEPTH);
    RenderBody();
    glTranslatef(0.0f, 0.0f, -VR_BODY_DEPTH);

    //--Unrotate if the radar is rotating.
    float tXRot, tYRot, tZRot;
    if(VisualLevel::xRadarRotatesWithPlayer)
    {
        CameraManager::FetchActiveCamera3D()->Get3DRotation(tXRot, tYRot, tZRot);
        glRotatef(tZRot, 0.0f, 0.0f, -1.0f);
    }

    //--Render local entities. Typically, only player-controlled entities render.
    RenderEntities();

    //--Re-rotate.
    if(VisualLevel::xRadarRotatesWithPlayer)
    {
        glRotatef(tZRot, 0.0f, 0.0f, 1.0f);
    }

    //--Clean.
    glTranslatef(-0.50f, -0.50f, 0.0f);
    glTranslatef(-mWorldX * VisualRoom::xDistanceScale, -mWorldY * VisualRoom::xDistanceScale, 0.0f);
}
void VisualRoom::RenderConnections()
{
    //--Renders lines representing connection directions. Unlike the default PandemoniumRoom, this version
    //  accounts for cases where rooms are not placed orthogonally at fixed distances. It still draws
    //  half-lines to avoid over-rendering.
    float cGrey = 191.0f / 255.0f;
    glDisable(GL_TEXTURE_2D);
    glColor3f(cGrey, cGrey, cGrey);

    //--Loop through the connections.
    for(int i = 0; i < NAV_TOTAL; i ++)
    {
        //--Skip if there's no connection.
        if(!rConnection[i]) continue;

        //--Target.
        float tTargetX = rConnection[i]->GetWorldX();
        float tTargetY = rConnection[i]->GetWorldY();
        float tAngle = atan2f(tTargetY - mWorldY, tTargetX - mWorldX);
        float tDistance = (GetPlanarDistance(mWorldX, mWorldY, tTargetX, tTargetY) * 0.5f * VisualRoom::xDistanceScale) + 2.0f;

        //--Render.
        glBegin(GL_LINES);
            glVertex2f(0.0f, 0.0f);
            glVertex2f(cosf(tAngle) * tDistance, sinf(tAngle) * tDistance);
        glEnd();
    }

    //--Clean.
    glEnable(GL_TEXTURE_2D);
    glColor3f(1.0f, 1.0f, 1.0f);
}
void VisualRoom::RenderBody()
{
    //--Similar to the base class but uses smaller spacings.
    glDisable(GL_TEXTURE_2D);
    float cGrey = 128.0f / 255.0f;
    float cMiddle = 255.0f / 255.0f;

    //--Size of the squares.
    float cOut = cSquareOuter / 2.0f * VR_BODY_FACTOR;
    float cInn = cSquareInner / 2.0f * VR_BODY_FACTOR;

    //--Normal case: Display a square.
    if(!HasDarknessCloud() && (IsPlayerPresent() || IsPlayerAdjacent()))
    {
        //--Outer square.
        if(!mIsHighlighted)
            glColor3f(cGrey, cGrey, cGrey);
        else
            glColor3f(cGrey, cGrey / 2.0f, cGrey / 2.0f);

        //--Render.
        glBegin(GL_QUADS);
            glVertex2f(-cOut, -cOut);
            glVertex2f( cOut, -cOut);
            glVertex2f( cOut,  cOut);
            glVertex2f(-cOut,  cOut);
        glEnd();

        //--Inner square.
        glColor4f(cMiddle, cMiddle, cMiddle, 1.0f);
        glBegin(GL_QUADS);
            glVertex2f(-cInn, -cInn);
            glVertex2f( cInn, -cInn);
            glVertex2f( cInn,  cInn);
            glVertex2f(-cInn,  cInn);
        glEnd();
    }
    //--Darkness cloud!
    else if(HasDarknessCloud() && (IsPlayerPresent() || IsPlayerAdjacent()))
    {
        //--Outer square.
        float cRadians = ((float)Global::Shared()->gTicksElapsed / 90.0f) * 3.1415926f;
        float cTickFactor = sinf(cRadians);
        float cFactor = 0.20f + (cTickFactor * 0.10f);
        glColor3f(cGrey * cFactor, cGrey * cFactor, cGrey * cFactor);
        glBegin(GL_QUADS);
            glVertex2f(-cOut, -cOut);
            glVertex2f( cOut, -cOut);
            glVertex2f( cOut,  cOut);
            glVertex2f(-cOut,  cOut);
        glEnd();

        //--Inner square.
        glColor4f(cMiddle * cFactor, cMiddle * cFactor, cMiddle * cFactor, 1.0f);
        glBegin(GL_QUADS);
            glVertex2f(-cInn, -cInn);
            glVertex2f( cInn, -cInn);
            glVertex2f( cInn,  cInn);
            glVertex2f(-cInn,  cInn);
        glEnd();
    }
    //--Player is not adjacent or present. Grey it out slightly.
    else
    {
        //--Outer square.
        float cFactor = 0.75f;
        glColor4f(cGrey * cFactor, cGrey * cFactor, cGrey * cFactor, 1.0f);
        glBegin(GL_QUADS);
            glVertex2f(-cOut, -cOut);
            glVertex2f( cOut, -cOut);
            glVertex2f( cOut,  cOut);
            glVertex2f(-cOut,  cOut);
        glEnd();

        //--Inner square.
        glColor4f(cMiddle * cFactor, cMiddle * cFactor, cMiddle * cFactor, 1.0f);
        glBegin(GL_QUADS);
            glVertex2f(-cInn, -cInn);
            glVertex2f( cInn, -cInn);
            glVertex2f( cInn,  cInn);
            glVertex2f(-cInn,  cInn);
        glEnd();
    }

    //--Clean.
    glColor3f(1.0f, 1.0f, 1.0f);
    glEnable(GL_TEXTURE_2D);
}
void VisualRoom::RenderEntities()
{
    //--Same as the parent class but handles the scaling much more smoothly. It is expected that
    //  the player will be dynamically resizing the map a lot, might as well make it look nice.
    float cScale = (WAD_TO_VR_SCALE * 1.5f);
    if(cScale <= 0.0f) return;

    //--Flags.
    bool tRadarFlag = OptionsManager::Fetch()->GetOptionB("Always Show Radar Indicators");
    bool tFogFlag   = OptionsManager::Fetch()->GetOptionB("No Fog of War for Items");

    //--Special icons. These render independent of visibility.
    if((IsPlayerPresent() || IsPlayerAdjacent()) || tFogFlag || tRadarFlag)
    {
        RenderSpecialIcons();
    }

    //--First, count how many 'visible' entities there are. There are several sizes and organizations
    //  required for visible entities.
    int tLegalCount = 0;
    Actor *rActor = (Actor *)mEntityList->PushIterator();
    while(rActor)
    {
        //--If an entity is obscured, don't count it.
        if(rActor->IsObscured())
        {

        }
        //--If the tRadarFlag is false, only show entities that are the player, or are adjacent to the player.
        else if(!tRadarFlag)
        {
            //--If the entity is the player, this is legal.
            if(rActor->IsPlayerControlled())
            {
                tLegalCount ++;
            }
            //--If the entity is in the same room as the player, they are visible.
            else if(IsPlayerPresent() && (rActor->GetRenderFlag() == RENDER_MONSTER || rActor->GetRenderFlag() == RENDER_HUMAN))
            {
                tLegalCount ++;
            }
            //--If the entity is adjacent to a room with the player, they should render.
            else if(IsPlayerAdjacent() && (rActor->GetRenderFlag() == RENDER_MONSTER || rActor->GetRenderFlag() == RENDER_HUMAN))
            {
                tLegalCount ++;
            }
        }
        //--All checks passed, this is legal.
        else if((rActor->GetRenderFlag() == RENDER_MONSTER || rActor->GetRenderFlag() == RENDER_HUMAN))
        {
            tLegalCount ++;
        }

        //--Next.
        rActor = (Actor *)mEntityList->AutoIterate();
    }

    //--Hey, no need to render anything.
    if(tLegalCount < 1) return;

    //--Other setup.
    int tSlot = 0;
    int i = tLegalCount - 1;
    if(i >= MAX_DIE) i = MAX_DIE - 1;

    //--Begin rendering.
    rActor = (Actor *)mEntityList->PushIterator();
    while(rActor)
    {
        //--Fail to render when obscured.
        bool tShouldRender = false;
        if(rActor->IsObscured())
        {
        }
        //--Radar flag is off, so only show things the player can see.
        else if(!tRadarFlag)
        {
            //--Player can always see herself.
            if(rActor->IsPlayerControlled())
            {
                tShouldRender = true;
            }
            //--If the entity is in the same room as the player, they are visible.
            else if(IsPlayerPresent())
            {
                tShouldRender = true;
            }
            //--If the entity is adjacent to a room with the player, they should render.
            else if(IsPlayerAdjacent())
            {
                tShouldRender = true;
            }
        }
        //--All checks passed, render.
        else
        {
            tShouldRender = true;
        }

        //--Render.
        if(tShouldRender)
        {
            //--Base.
            int p = tSlot;
            rActor->GetLocalColor().SetAsMixer();
            glScalef(cScale, cScale, 1.0f);
            glTranslatef(xDicePackages[i][p].mX, xDicePackages[i][p].mY, 0.0f);

            //--Humans:
            if(rActor->GetRenderFlag() == RENDER_HUMAN)
            {
                RenderHumanSymbol(0, 0);
                tSlot ++;
            }
            //--Monsters:
            else if(rActor->GetRenderFlag() == RENDER_MONSTER)
            {
                RenderMonsterSymbol(0, 0);
                tSlot ++;
            }

            //--Clean.
            glTranslatef(-xDicePackages[i][p].mX, -xDicePackages[i][p].mY, 0.0f);
            glScalef(1.0f / cScale, 1.0f / cScale, 1.0f);

            //--Stop if the slot goes over the edge. Should be logically impossible, but this is in
            //  case someone messes with the code.
            //--I'm looking at YOU, person reading this!
            if(tSlot > i)
            {
                mEntityList->PopIterator();
                break;
            }
        }

        //--Next.
        rActor = (Actor *)mEntityList->AutoIterate();
    }

    //--Clean.
    glColor3f(1.0f, 1.0f, 1.0f);
}
void VisualRoom::RenderSpecialIcons()
{
    //--Same as the base, but uses a smaller scale.
    float cScale = (WAD_TO_VR_SCALE * 1.5f);
    if(cScale <= 0.0f) return;

    //--Unrotate.
    /*if(VisualLevel::xRadarRotatesWithPlayer)
    {
        float tXRot, tYRot, tZRot;
        CameraManager::FetchActiveCamera3D()->Get3DRotation(tXRot, tYRot, tZRot);
        glRotatef(-tZRot, 0.0f, 0.0f, -1.0f);
    }*/

    //--Setup.
    glScalef(cScale, cScale, 1.0f);

    //--Constants.
    float cFactor = 0.65f; //For rendering just inside the room icon.
    float cOffset = 8.0f;

    //--At least one container is present. Render the container icon.
    if(GetContainersTotal() > 0)
    {
        RenderChestSymbol(cSquareInner * -cFactor, cSquareInner * -cFactor);
    }

    //--At least one item is present. Render the item icon.
    if(IsOneItemPresent())
    {
        RenderItemSymbol((cSquareInner * -cFactor) + cOffset, cSquareInner * -cFactor);
    }

    //--At least one weapon is present.
    if(IsOneWeaponPresent())
    {
        RenderWeaponSymbol((cSquareInner * -cFactor) + (cOffset*2.0f), cSquareInner * -cFactor);
    }

    //--Check for Golem Tube. Always renders.
    if(IsActorWithTagPresent(RENDER_TUBE))
    {
        RenderTubeSymbol((cSquareInner * cFactor) + (cOffset*0.0f) - 1.0f, cSquareInner * cFactor + 1.0f);
    }

    //--Rilmani mirror.
    if(IsActorWithTagPresent(RENDER_MIRROR))
    {
        RenderMirrorSymbol((cSquareInner * cFactor) + (cOffset*0.0f) - 1.0f, cSquareInner * cFactor + 1.0f);
    }

    //--Harpy nest.
    if(IsActorWithTagPresent(RENDER_NEST))
    {
        RenderNestSymbol((cSquareInner * cFactor) + (cOffset*0.0f) - 1.0f, cSquareInner * cFactor + 1.0f);
    }

    //--Vampire Coffin
    if(IsActorWithTagPresent(RENDER_COFFIN))
    {
        RenderCoffinSymbol((cSquareInner * cFactor) + (cOffset*0.0f) - 1.0f, cSquareInner * cFactor + 1.0f);
    }

    //--Glyph of Power.
    if(IsActorWithTagPresent(RENDER_GLYPH))
    {
        RenderGlyphSymbol((cSquareInner * cFactor) + (cOffset*0.0f) - 1.0f, cSquareInner * cFactor + 1.0f);
    }

    //--Clean.
    glScalef(1.0f / cScale, 1.0f / cScale, 1.0f);
    /*if(VisualLevel::xRadarRotatesWithPlayer)
    {
        float tXRot, tYRot, tZRot;
        CameraManager::FetchActiveCamera3D()->Get3DRotation(tXRot, tYRot, tZRot);
        glRotatef(tZRot, 0.0f, 0.0f, -1.0f);
    }*/
}

//======================================= Pointer Routing =========================================
//====================================== Static Functions =========================================
//========================================= Lua Hooking ===========================================
void VisualRoom::HookToLuaState(lua_State *pLuaState)
{
    /* VR_SetProperty("Position", fXPosition, fYPosition)
       VR_SetProperty("Position Node", sNodeName)
       Sets the requested property in the active VisualRoom. */
    lua_register(pLuaState, "VR_SetProperty", &Hook_VR_SetProperty);
}

//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
int Hook_VR_SetProperty(lua_State *L)
{
    //VR_SetProperty("Position", fXPosition, fYPosition)
    //VR_SetProperty("Position Node", sNodeName)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("VR_SetProperty");

    //--Active object.
    VisualRoom *rActiveRoom = (VisualRoom *)DataLibrary::Fetch()->rActiveObject;
    if(!rActiveRoom || !rActiveRoom->IsOfType(POINTER_TYPE_VISUAL_ROOM)) return LuaTypeError("VR_SetProperty");

    //--Switching.
    const char *rSwitchType = lua_tostring(L, 1);

    //--Position, using fixed coordinates. Calls to the base PandemoniumRoom class.
    if(!strcasecmp(rSwitchType, "Position") && tArgs == 3)
    {
        rActiveRoom->SetPosition(lua_tonumber(L, 2), lua_tonumber(L, 3), 0);
    }
    //--Position, using the position of a node found in the owning level.
    if(!strcasecmp(rSwitchType, "Position Node") && tArgs == 2)
    {
        rActiveRoom->SetPositionByNode(lua_tostring(L, 2));
    }
    //--Error.
    else
    {
        LuaPropertyError("VR_SetProperty", rSwitchType, tArgs);
    }

    return 0;
}
