//--Base
#include "BlueSphereLevel.h"

//--Classes
//--CoreClasses
#include "VirtualFile.h"

//--Definitions
//--Libraries
//--Managers
#include "DebugManager.h"
#include "SugarLumpManager.h"

void BlueSphereLevel::ReadFromFile(const char *pPath)
{
    //--[Documentation and Setup]
    //--Given a path, opens this as an SLF file. This is a map file using the same basic format as
    //  the adventure levels, except we use tiles to represent spheres.
    if(!pPath) return;

    //--Open it in the SLM.
    SugarLumpManager *rSLM = SugarLumpManager::Fetch();
    rSLM->Open(pPath);

    //--[Header Check]
    //--Basics.
    DebugManager::PushPrint(true, "Orthogonal Tiled Level - Begin parse\n");
    if(!rSLM->StandardSeek("AMapInfo", "MAPINFO"))
    {
        DebugManager::PopPrint("Error: No AMapInfo lump was located.\n");
        return;
    }

    //--Get the VirtualFile from within the SLM.
    VirtualFile *rSLFFile = rSLM->GetVirtualFile();

    //--Make sure that the MapInfoLump has 4 as its type. Fail if it doesn't.
    uint8_t tMapType;
    rSLFFile->Read(&tMapType, sizeof(uint8_t), 1);
    if(tMapType != 4)
    {
        DebugManager::PopPrint("Error: AMapInfo lump lists type as %i, not 4.\n", tMapType);
        return;
    }

    //--Begin work on tile layers. There can be up to 100 (!) and are always in order.
    int tLumpCount = 0;
    char tLumpNameBuffer[80];
    sprintf(tLumpNameBuffer, "Layer %02i", tLumpCount);
    while(rSLM->StandardSeek(tLumpNameBuffer, "LAYERDATA"))
    {
        //--Does this lump have flipping data?
        bool tHasFlipData = false;
        char *tHeaderString = rSLM->GetHeaderOf(tLumpNameBuffer);
        if(tHeaderString[9] >= '1') tHasFlipData = true;

        //--Clean up.
        free(tHeaderString);

        //--Get the layer's type. We handle "Tile" right now.
        char *tType = rSLFFile->ReadLenString();
        if(!strcasecmp(tType, "Tile"))
        {
            //--Read the name of the layer.
            char *nName = rSLFFile->ReadLenString();
            DebugManager::Print(" Layer name: %s\n", nName);

            //--Custom properties. Uses a 16 bit integer.
            int tCustomPropertiesTotal = 0;
            rSLFFile->Read(&tCustomPropertiesTotal, sizeof(int16_t), 1);

            //--Read out the properties as key-value strings. Bypass them.
            for(int i = 0; i < tCustomPropertiesTotal; i ++)
            {
                //--Get the key/value pair.
                char *tKey = rSLFFile->ReadLenString();
                char *tVal = rSLFFile->ReadLenString();

                //--Clean.
                free(tKey);
                free(tVal);
            }
            DebugManager::Print(" Layer has %i properties\n", tCustomPropertiesTotal);

            //--Sizing and offset information.
            int16_t tSizingData[4];
            rSLFFile->Read(tSizingData, sizeof(int16_t), 4);

            //--Tile data.
            SetMemoryData(__FILE__, __LINE__);
            int16_t *tTileData = (int16_t *)starmemoryalloc(sizeof(int16_t) * tSizingData[2] * tSizingData[3]);
            rSLFFile->Read(tTileData, sizeof(int16_t), tSizingData[2] * tSizingData[3]);

            //--Flip data. Only gets read if the flag is on. Otherwise, is the same size as tile data.
            int16_t *tFlipData = NULL;
            if(tHasFlipData)
            {
                SetMemoryData(__FILE__, __LINE__);
                tFlipData = (int16_t *)starmemoryalloc(sizeof(int16_t) * tSizingData[2] * tSizingData[3]);
                rSLFFile->Read(tFlipData, sizeof(int16_t), tSizingData[2] * tSizingData[3]);
            }

            //--Size the level.
            SizeMap(tSizingData[2], tSizingData[3]);

            //--Start inputting map elements.
            for(int x = 0; x < mXSize; x ++)
            {
                for(int y = 0; y < mYSize; y ++)
                {
                    //--Get the tile.
                    int16_t tTileAt = tTileData[(x * mYSize) + y];

                    //--Blue sphere.
                    if(tTileAt == 0)
                    {
                        SetMapElement(x, y, BSL_BLUE_SPHERE);
                    }
                    //--Red sphere.
                    else if(tTileAt == 1)
                    {
                        SetMapElement(x, y, BSL_RED_SPHERE);
                    }
                    //--Launcher.
                    else if(tTileAt == 2)
                    {
                        SetMapElement(x, y, BSL_LAUNCHER);
                    }
                    //--Bumper.
                    else if(tTileAt == 3)
                    {
                        SetMapElement(x, y, BSL_BUMPER);
                    }
                    //--Player start up.
                    else if(tTileAt == 16)
                    {
                        SetPlayerPosition(x, y);
                        SetPlayerFacing(DIR_UP);
                    }
                    //--Player start down.
                    else if(tTileAt == 17)
                    {
                        SetPlayerPosition(x, y);
                        SetPlayerFacing(DIR_DOWN);
                    }
                    //--Player start left.
                    else if(tTileAt == 18)
                    {
                        SetPlayerPosition(x, y);
                        SetPlayerFacing(DIR_LEFT);
                    }
                    //--Player start right.
                    else if(tTileAt == 19)
                    {
                        SetPlayerPosition(x, y);
                        SetPlayerFacing(DIR_RIGHT);
                    }
                }
            }

            //--Clean up.
            free(nName);
            free(tTileData);
            free(tFlipData);
        }
        //--Okay, so it's an Object type layer. Scan it for enemies, cameras, slip zones, etc.
        else if(!strcasecmp(tType, "Object"))
        {
        }
        //--It's an image. These are used for backgrounds.
        else if(!strcasecmp(tType, "Image"))
        {
        }
        //--Other types are unhandled.
        else
        {
            DebugManager::ForcePrint("Error parsing layer lump: Unknown type %s\n", tType);
        }

        DebugManager::Print("Layer parsed.\n");

        //--Clean up.
        free(tType);

        //--Seek to the next object.
        tLumpCount ++;
        sprintf(tLumpNameBuffer, "Layer %02i", tLumpCount);
    }

    //--Clean up.
    DebugManager::PopPrint("Parsing completed.\n");
}
