//--Base
#include "PairanormalLevel.h"

//--Classes
#include "PairanormalDialogue.h"
#include "PairanormalMenu.h"
#include "PairanormalSettingsMenu.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarLinkedList.h"

//--Definitions
#include "Global.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "CutsceneManager.h"
#include "DebugManager.h"
#include "DisplayManager.h"
#include "LuaManager.h"
#include "MapManager.h"
#include "OptionsManager.h"
#include "SaveManager.h"

//--[Local Definitions]
#define DEPTH_BACKGROUND -1.0f

//=========================================== System ==============================================
PairanormalLevel::PairanormalLevel()
{
    //--[RootObject]
    //--System
    mType = POINTER_TYPE_PAIRANORMALLEVEL;

    //--[IRenderable]
    //--System

    //--[RootLevel]
    //--System

    //--[PairanormalLevel]
    //--System
    mInitialFadeTimer = 0;
    mBootToMainMenu = false;
    mMouseX = 0.0f;
    mMouseY = 0.0f;

    //--Checkpoint
    mCurrentCheckpointName = InitializeString("No Checkpoint");
    mCurrentCheckpointScript = InitializeString("No Script");
    mCurrentCheckpointArg = InitializeString("No Argument");

    //--Background
    mFadeTimer = 0;
    mBackgroundXOffset = BG_SCROLL_MAX * 0.50f;
    mLastBackgroundPath = InitializeString("No Background");
    rPreviousBackground = NULL;
    rCurrentBackground = NULL;

    //--Screen Shake
    mScreenShakeTimer = 0;
    mScreenShakeCurrent = 0.0f;

    //--Buttons
    memset(mBtnDim, 0, sizeof(TwoDimensionReal) * PLBTN_TOTAL);

    //--Images
    memset(&Images, 0, sizeof(Images));

    //--[Construction]
    //--Load image data.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();

    //--Icons
    Images.Data.rIconClose = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Common/Close");
    Images.Data.rIconSettings = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/UI/Common/Settings");

    //--Verify.
    Images.mIsReady = VerifyStructure(&Images.Data, sizeof(Images.Data), sizeof(void *));

    //--Standard background.
    rCurrentBackground = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/Backgrounds/All/Classroom");

    //--Position the buttons.
    mBtnDim[PLBTN_QUIT].SetWH(     2.0f,   2.0f,  30.0f, 27.0f);
    mBtnDim[PLBTN_SETTINGS].SetWH(41.0f,   3.0f,  25.0f, 25.0f);
}
PairanormalLevel::~PairanormalLevel()
{
    free(mCurrentCheckpointName);
    free(mCurrentCheckpointScript);
    free(mCurrentCheckpointArg);
    free(mLastBackgroundPath);
}

//--[Public Statics]
bool PairanormalLevel::xIsRunningToCheckpoint = false;
int PairanormalLevel::xPlayTime = 0;
char *PairanormalLevel::xActiveGamePath = NULL;
bool PairanormalLevel::xIsLowResMode = false;

//====================================== Property Queries =========================================
const char *PairanormalLevel::GetCheckpointName()
{
    return (const char *)mCurrentCheckpointName;
}
const char *PairanormalLevel::GetCheckpointScript()
{
    return (const char *)mCurrentCheckpointScript;
}
const char *PairanormalLevel::GetCheckpointArg()
{
    return (const char *)mCurrentCheckpointArg;
}
float PairanormalLevel::GetBackgroundOffset()
{
    return mBackgroundXOffset;
}
float PairanormalLevel::GetScreenShake()
{
    return mScreenShakeCurrent;
}
const char *PairanormalLevel::GetLastBackgroundPath()
{
    return mLastBackgroundPath;
}

//========================================= Manipulators ==========================================
void PairanormalLevel::FlagToMainMenu()
{
    mBootToMainMenu = true;
}
void PairanormalLevel::SetBackground(const char *pPath)
{
    //--Store the previous background. It can be NULL.
    mFadeTimer = 0;
    rPreviousBackground = rCurrentBackground;

    //--Sysvar for the background.
    SysVar *rLastBackground = (SysVar *)DataLibrary::Fetch()->GetEntry("Root/Variables/System/Player/LastBackground");

    //--"NULL" just nulls out the background. This will usually happen with the DL anyway.
    if(!strcasecmp(pPath, "Null"))
    {
        ResetString(mLastBackgroundPath, "No Background");
        rCurrentBackground = NULL;

        //--Save the background variable.
        if(rLastBackground) ResetString(rLastBackground->mAlpha, mLastBackgroundPath);
        return;
    }

    //--Store.
    rCurrentBackground = (SugarBitmap *)DataLibrary::Fetch()->GetEntry(pPath);
    ResetString(mLastBackgroundPath, pPath);

    //--Save the background variable.
    if(rLastBackground) ResetString(rLastBackground->mAlpha, mLastBackgroundPath);
}
void PairanormalLevel::SetCheckpoint(const char *pName, const char *pScriptPath, const char *pArgument)
{
    if(!pName || !pScriptPath || !pArgument) return;
    ResetString(mCurrentCheckpointName, pName);
    ResetString(mCurrentCheckpointScript, pScriptPath);
    ResetString(mCurrentCheckpointArg, pArgument);
}
void PairanormalLevel::SetBackgroundOffset(float pAmount)
{
    mBackgroundXOffset = pAmount;
    if(mBackgroundXOffset < BG_SCROLL_MAX) mBackgroundXOffset = BG_SCROLL_MAX;
    if(mBackgroundXOffset >          0.0f) mBackgroundXOffset =          0.0f;
}
void PairanormalLevel::ShakeScreen()
{
    mScreenShakeTimer = 15;
}

//========================================= Core Methods ==========================================
//===================================== Private Core Methods ======================================
//============================================ Update =============================================
void PairanormalLevel::Update()
{
    //--[Documentation and Setup]
    //--Handles updates and controls. This is mostly a mouse-driven UI.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Initial fade timer.
    if(mInitialFadeTimer < FADE_TICKS_MAX) mInitialFadeTimer ++;

    //--[Screen Shake Timer]
    if(mScreenShakeTimer > 0)
    {
        //--Decrement.
        mScreenShakeTimer --;

        //--Change the shake every few ticks.
        if(mScreenShakeTimer % 2 == 0)
        {
            //--If left, go right on the next pass.
            if(mScreenShakeCurrent <= 0)
            {
                mScreenShakeCurrent = rand() % 5 + 5;
            }
            //--If right, go left.
            else
            {
                mScreenShakeCurrent = (rand() % 5 + 5) * -1;
            }
        }
    }
    else
    {
        mScreenShakeCurrent = 0;
    }

    //--[Background Fade Timer]
    if(mFadeTimer < FADE_TICKS_MAX) mFadeTimer ++;

    //--[Main Menu Switch]
    //--If flagged, switches back to the Main Menu.
    if(mBootToMainMenu)
    {
        //--Reset the dialogue.
        PairanormalDialogue::Fetch()->FactoryZero();
        CutsceneManager::Fetch()->DropAllEvents();

        //--Create. Give it to the MapManager.
        PairanormalMenu *nMainMenu = new PairanormalMenu();
        MapManager::Fetch()->ReceiveLevel(nMainMenu);

        //--Object is now unstable.
        return;
    }

    //--[Settings Menu]
    PairanormalSettingsMenu *rSettingsMenu = PairanormalSettingsMenu::Fetch();
    if(rSettingsMenu->IsOpen())
    {
        rSettingsMenu->Update();
        return;
    }
    else
    {
        rSettingsMenu->NotUpdate();
    }

    //--Gameplay Time.
    xPlayTime ++;

    //--Cutscene Manager updates.
    CutsceneManager::Fetch()->Update();

    //--[Mouse Position]
    //--Get mouse location.
    float tMouseZ;
    rControlManager->GetMouseCoordsF(mMouseX, mMouseY, tMouseZ);

    //--[Mouse Clicking]
    //--Player clicks the mouse at a location on screen.
    if(rControlManager->IsFirstPress("MouseLft"))
    {
        //--Quit button....
        if(mBtnDim[PLBTN_QUIT].IsPointWithin(mMouseX, mMouseY))
        {
            //--SFX.
            AudioManager::Fetch()->PlaySound("UI|Select");

            //--Reset the dialogue.
            PairanormalDialogue::Fetch()->FactoryZero();
            CutsceneManager::Fetch()->DropAllEvents();

            //--Paths.
            char tPathBuffer[256];
            const char *rPairanormalPath = DataLibrary::GetGamePath("Root/Paths/System/Startup/sPairanormalPath");
            sprintf(tPathBuffer, "%s/GalleryController.lua", rPairanormalPath);

            //--Create. Give it to the MapManager.
            PairanormalMenu *nMainMenu = new PairanormalMenu();
            MapManager::Fetch()->ReceiveLevel(nMainMenu);
            LuaManager::Fetch()->ExecuteLuaFile(tPathBuffer);

            //--Object is now unstable.
            return;
        }
        //--Settings button....
        else if(mBtnDim[PLBTN_SETTINGS].IsPointWithin(mMouseX, mMouseY))
        {
            //--SFX.
            AudioManager::Fetch()->PlaySound("UI|Select");

            //--Open the settings menu.
            rSettingsMenu->Open();
        }
    }
    if(rSettingsMenu->IsOpen()) return;

    //--[PairanormalDialogue Update]
    //--Run it.
    PairanormalDialogue *rDialogue = PairanormalDialogue::Fetch();
    rDialogue->Update();
}

//=========================================== File I/O ============================================
void PairanormalLevel::RunToCheckpointAfterLoading()
{
    //--Runs to the checkpoint, called after the SaveManager loads game data. Note that the checkpoint
    //  info should already have been uploaded to this object.
    LuaManager::Fetch()->ExecuteLuaFile(mCurrentCheckpointScript, 1, "S", mCurrentCheckpointArg);
}
#include "RootEvent.h"
void PairanormalLevel::HandleCheckpoint(const char *pName)
{
    //--If not running to a checkpoint, ignore this.
    if(!PairanormalLevel::xIsRunningToCheckpoint) return;
    //fprintf(stderr, "Passed checkpoint %s vs. %s\n", pName, mCurrentCheckpointName);

    //--If this is the same as the current checkpoint, stop running to checkpoint.
    if(mCurrentCheckpointName && !strcasecmp(pName, mCurrentCheckpointName))
    {
        xIsRunningToCheckpoint = false;
        //fprintf(stderr, " Checkpoint ends.\n");
    }
}

//=========================================== Drawing =============================================
void PairanormalLevel::AddToRenderList(SugarLinkedList *pRenderList)
{
    pRenderList->AddElement("X", this);
}
void PairanormalLevel::Render()
{
    //--[Documentation and Setup]
    //--Renders the main menu, or gallery menu, depending on which is currently set.
    MapManager::xHasRenderedMenus = true;
    if(!Images.mIsReady) return;
    DebugManager::PushPrint(false, "Rendering PairanormalLevel.\n");
    float cScl = BG_SCALE;
    float cInv = 1.00f / cScl;
    float cUseBgOffset = mBackgroundXOffset;

    //--When in low-res mode, double the BG scale.
    if(xIsLowResMode && rCurrentBackground && rCurrentBackground->GetWidth() < 2000.0f)
    {
        cScl = cScl * 2.0f;
        cInv = 1.00f / cScl;
    }

    //--[Screen Shake]
    glTranslatef(mScreenShakeCurrent, 0.0f, 0.0f);

    //--[VHS Handler]
    //--During glitch sequences, the entire game renders with an offset. If the offset is 0.0f,
    //  don't perform any special cases. Never takes effect if the OptionsManager blocks it.
    PairanormalDialogue *rDialogue = PairanormalDialogue::Fetch();
    float cGlitchOffset = rDialogue->GetRenderDoubleOffset();
    if(cGlitchOffset != 0.0f && OptionsManager::xAllowFlashingImages)
    {
        glTranslatef(0.0f, cGlitchOffset, 0.0f);
    }
    DebugManager::Print("Offset VHS handler.\n");

    //--[Background]
    //--If currently fading, render the previous background under the current one.
    if(mFadeTimer < FADE_TICKS_MAX)
    {
        //--Depth.
        glTranslatef(0.0f, 0.0f, DEPTH_BACKGROUND);

        //--GL Setup.
        if(rDialogue->IsBackgroundInverted() && OptionsManager::xAllowFlashingImages) DisplayManager::Fetch()->ActivateProgram("PairanormalLevel ColorInvert");

        //--If the previous background exists, render it.
        if(rPreviousBackground)
        {
            glScalef(cScl, cScl, 1.0f);
            rPreviousBackground->Draw(cUseBgOffset, BG_OFFSET_Y);
            glScalef(cInv, cInv, 1.0f);
        }

        //--If the current background exists, render it.
        if(rCurrentBackground)
        {
            StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, mFadeTimer / (float)FADE_TICKS_MAX);
            glScalef(cScl, cScl, 1.0f);
            rCurrentBackground->Draw(cUseBgOffset, BG_OFFSET_Y);
            glScalef(cInv, cInv, 1.0f);
            StarlightColor::ClearMixer();
        }
        //--Otherwise, the current background is black.
        else
        {
            StarlightColor::SetMixer(0.0f, 0.0f, 0.0f, mFadeTimer / (float)FADE_TICKS_MAX);
            glDisable(GL_TEXTURE_2D);
            glBegin(GL_QUADS);
                glVertex2f(            0.0f,             0.0f);
                glVertex2f(VIRTUAL_CANVAS_X,             0.0f);
                glVertex2f(VIRTUAL_CANVAS_X, VIRTUAL_CANVAS_Y);
                glVertex2f(            0.0f, VIRTUAL_CANVAS_Y);
            glEnd();
            glEnable(GL_TEXTURE_2D);
            StarlightColor::ClearMixer();
        }

        //--Clean up.
        DisplayManager::Fetch()->ActivateProgram(NULL);
        glTranslatef(0.0f, 0.0f, -DEPTH_BACKGROUND);
    }
    //--Normal. Just render the current background. If it doesn't exist, leave it black.
    else if(rCurrentBackground)
    {
        //--Depth.
        glTranslatef(0.0f, 0.0f, DEPTH_BACKGROUND);

        //--GL Setup.
        if(rDialogue->IsBackgroundInverted() && OptionsManager::xAllowFlashingImages) DisplayManager::Fetch()->ActivateProgram("PairanormalLevel ColorInvert");
        glScalef(cScl, cScl, 1.0f);

        //--Render.
        rCurrentBackground->Draw(cUseBgOffset, BG_OFFSET_Y);

        //--Clean.
        glScalef(cInv, cInv, 1.0f);
        DisplayManager::Fetch()->ActivateProgram(NULL);
        glTranslatef(0.0f, 0.0f, -DEPTH_BACKGROUND);
    }
    DebugManager::Print("Rendered background.\n");

    //--[Dialogue]
    //--It handles its own rendering.
    rDialogue->Render();
    DebugManager::Print("Rendered Dialogue.\n");

    //--[Buttons]
    //--Exit and Settings. Renders over whatever the dialogue UI does.
    Images.Data.rIconClose->Draw(0.0f, 0.0f);
    Images.Data.rIconSettings->Draw(0.0f, 0.0f);
    DebugManager::Print("Rendered exit buttons.\n");

    //--[VHS Second Render]
    //--Render a second set of everything offset so it appears as if the screen is modulusing off the bottom.
    //  OptionsManager can block this.
    if(cGlitchOffset != 0.0f && OptionsManager::xAllowFlashingImages)
    {
        //--Position.
        float cOffset = -768.0f;
        if(cGlitchOffset < 0.0f) cOffset = cOffset * -1.0f;
        glTranslatef(0.0f, cOffset, 0.0f);

        //--[Background]
        //--If currently fading, render the previous background under the current one.
        if(mFadeTimer < FADE_TICKS_MAX)
        {
            //--Depth.
            glTranslatef(0.0f, 0.0f, DEPTH_BACKGROUND);

            //--GL Setup.
            if(rDialogue->IsBackgroundInverted()) DisplayManager::Fetch()->ActivateProgram("PairanormalLevel ColorInvert");

            //--If the previous background exists, render it.
            if(rPreviousBackground)
            {
                glScalef(cScl, cScl, 1.0f);
                rPreviousBackground->Draw(cUseBgOffset, BG_OFFSET_Y);
                glScalef(cInv, cInv, 1.0f);
            }

            //--If the current background exists, render it.
            if(rCurrentBackground)
            {
                StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, mFadeTimer / (float)FADE_TICKS_MAX);
                glScalef(cScl, cScl, 1.0f);
                rCurrentBackground->Draw(cUseBgOffset, BG_OFFSET_Y);
                glScalef(cInv, cInv, 1.0f);
                StarlightColor::ClearMixer();
            }
            //--Otherwise, the current background is black.
            else
            {
                StarlightColor::SetMixer(0.0f, 0.0f, 0.0f, mFadeTimer / (float)FADE_TICKS_MAX);
                glDisable(GL_TEXTURE_2D);
                glBegin(GL_QUADS);
                    glVertex2f(            0.0f,             0.0f);
                    glVertex2f(VIRTUAL_CANVAS_X,             0.0f);
                    glVertex2f(VIRTUAL_CANVAS_X, VIRTUAL_CANVAS_Y);
                    glVertex2f(            0.0f, VIRTUAL_CANVAS_Y);
                glEnd();
                glEnable(GL_TEXTURE_2D);
                StarlightColor::ClearMixer();
            }

            //--Clean up.
            DisplayManager::Fetch()->ActivateProgram(NULL);
            glTranslatef(0.0f, 0.0f, -DEPTH_BACKGROUND);
        }
        //--Normal. Just render the current background. If it doesn't exist, leave it black.
        else if(rCurrentBackground)
        {
            //--GL Setup.
            glTranslatef(0.0f, 0.0f, DEPTH_BACKGROUND);
            if(rDialogue->IsBackgroundInverted()) DisplayManager::Fetch()->ActivateProgram("PairanormalLevel ColorInvert");
            glScalef(cScl, cScl, 1.0f);

            //--Render.
            rCurrentBackground->Draw(cUseBgOffset, BG_OFFSET_Y);

            //--Clean.
            glScalef(cInv, cInv, 1.0f);
            DisplayManager::Fetch()->ActivateProgram(NULL);
            glTranslatef(0.0f, 0.0f, -DEPTH_BACKGROUND);
        }

        //--Dialogue.
        rDialogue->Render();

        //--Icons.
        Images.Data.rIconClose->Draw(0.0f, 0.0f);
        Images.Data.rIconSettings->Draw(0.0f, 0.0f);

        //--Clean everything up.
        glTranslatef(0.0f, -cOffset, 0.0f);
        glTranslatef(0.0f, -cGlitchOffset, 0.0f);
    }
    DebugManager::Print("Second VHS handler.\n");

    //--[Creepy Overlay]
    //--Renders at partial transparency. Can come back NULL. Can be blocked by the OptionsManager.
    SugarBitmap *rCreepyOverlay = rDialogue->GetCreepyOverlay();
    if(rCreepyOverlay && OptionsManager::xAllowFlashingImages)
    {
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, 0.12f);
        rCreepyOverlay->Draw();
        StarlightColor::ClearMixer();
    }
    DebugManager::Print("Rendered overlay.\n");

    //--[Screen Shake]
    glTranslatef(mScreenShakeCurrent, 0.0f, 0.0f);

    //--[Settings Menu]
    //--Renders over everything else.
    PairanormalSettingsMenu *rSettingsMenu = PairanormalSettingsMenu::Fetch();
    rSettingsMenu->Render();
    DebugManager::Print("Rendered Settings.\n");

    //--Over fade.
    if(mInitialFadeTimer < FADE_TICKS_MAX)
    {
        float cAlpha = 1.0f - ((float)mInitialFadeTimer / (float)FADE_TICKS_MAX);
        SugarBitmap::DrawRectFill(-100.0f, 0.0f, VIRTUAL_CANVAS_X + 100.0f, VIRTUAL_CANVAS_Y, StarlightColor::MapRGBAF(0.0f, 0.0f, 0.0f, cAlpha));
    }

    //--Debug.
    DebugManager::PopPrint("Done rendering PairanormalLevel.\n");
}

//======================================= Pointer Routing =========================================
//====================================== Static Functions =========================================
PairanormalLevel *PairanormalLevel::Fetch()
{
    //--Dummy level used if no active level is found.
    static PairanormalLevel *xDummyLevel = NULL;

    //--Check the level.
    RootLevel *rActiveLevel = MapManager::Fetch()->GetActiveLevel();
    if(!rActiveLevel || rActiveLevel->GetType() != POINTER_TYPE_PAIRANORMALLEVEL)
    {
        if(!xDummyLevel) xDummyLevel = new PairanormalLevel();
        return xDummyLevel;
    }

    //--Return it.
    return (PairanormalLevel *)rActiveLevel;
}

//========================================= Lua Hooking ===========================================
void PairanormalLevel::HookToLuaState(lua_State *pLuaState)
{
    /* PairanormalLevel_IsLoading()
       Returns true if the game is currently loading, false if not. */
    lua_register(pLuaState, "PairanormalLevel_IsLoading", &Hook_PairanormalLevel_IsLoading);

    /* PairanormalLevel_SetBackground(sBackgroundPath)
       Manually changes the background. Use this when loading the game. */
    lua_register(pLuaState, "PairanormalLevel_SetBackground", &Hook_PairanormalLevel_SetBackground);

    /* PairanormalLevel_SaveCheckpoint(sCheckpointName, sFile, sArg)
       Saves the game at the listed checkpoint. Immediately writes a save-file, so be sure to account
       for the time this may take. */
    lua_register(pLuaState, "PairanormalLevel_SaveCheckpoint", &Hook_PairanormalLevel_SaveCheckpoint);

    /* PairanormalLevel_FlagCheckpoint(sCheckpointName)
       Orders the game to handle the checkpoint receiver. If not loading the game, this does nothing.
       If loading, the game will resume if this checkpoint matches the one in the save file. */
    lua_register(pLuaState, "PairanormalLevel_FlagCheckpoint", &Hook_PairanormalLevel_FlagCheckpoint);
}

//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
int Hook_PairanormalLevel_IsLoading(lua_State *L)
{
    //PairanormalLevel_IsLoading()
    lua_pushboolean(L, PairanormalLevel::xIsRunningToCheckpoint);
    return 1;
}
int Hook_PairanormalLevel_SetBackground(lua_State *L)
{
    //PairanormalLevel_SetBackground(sBackgroundPath)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return 0;

    //--Active object. Set background.
    PairanormalLevel *rLevel = PairanormalLevel::Fetch();
    rLevel->SetBackground(lua_tostring(L, 1));
    return 0;
}
int Hook_PairanormalLevel_SaveCheckpoint(lua_State *L)
{
    //PairanormalLevel_SaveCheckpoint(sCheckpointName, sFile, sArg)
    int tArgs = lua_gettop(L);
    if(tArgs < 3) return 0;

    //--Does nothing if currently running to a checkpoint.
    if(PairanormalLevel::xIsRunningToCheckpoint || !PairanormalLevel::xActiveGamePath) return 0;

    //--Active object.
    PairanormalLevel *rLevel = PairanormalLevel::Fetch();

    //--Update the checkpoint state.
    rLevel->SetCheckpoint(lua_tostring(L, 1), lua_tostring(L, 2), lua_tostring(L, 3));

    //--Save the game.
    SaveManager::Fetch()->SavePairanormalFile(PairanormalLevel::xActiveGamePath);

    //--Also save the gallery data.
    char tSaveBuf[256];
    const char *rPairanormalPath = DataLibrary::Fetch()->GetGamePath("Root/Paths/System/Startup/sPairanormalPath");
    sprintf(tSaveBuf, "%s/../../Saves/GalleryData.slf", rPairanormalPath);
    SaveManager::Fetch()->SavePairanormalGallery(tSaveBuf);

    //--Backup work.
    return 0;
}
int Hook_PairanormalLevel_FlagCheckpoint(lua_State *L)
{
    //PairanormalLevel_FlagCheckpoint(sCheckpointName)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return 0;

    //--Active object.
    PairanormalLevel *rLevel = PairanormalLevel::Fetch();

    //--Order it to handle the checkpoint.
    rLevel->HandleCheckpoint(lua_tostring(L, 1));
    return 0;
}
