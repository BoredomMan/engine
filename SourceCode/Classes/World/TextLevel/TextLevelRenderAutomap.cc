//--Base
#include "TextLevel.h"

//--Classes
#include "TileLayer.h"
#include "StringEntry.h"
#include "WorldDialogue.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarFont.h"
#include "SugarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "EasingFunctions.h"
#include "HitDetection.h"
#include "Subdivide.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "ControlManager.h"
#include "DisplayManager.h"
#include "LuaManager.h"
#include "MapManager.h"

//--Rendering Temporary Variables
float TextLevel::cRenderX[4];
float TextLevel::cRenderY[4];
float TextLevel::cTxL;
float TextLevel::cTxT;
float TextLevel::cTxR;
float TextLevel::cTxB;
float TextLevel::cTxLExc;
float TextLevel::cTxTExc;
float TextLevel::cTxRExc;
float TextLevel::cTxBExc;
float TextLevel::cTxLQue;
float TextLevel::cTxTQue;
float TextLevel::cTxRQue;
float TextLevel::cTxBQue;

void TextLevel::RenderAutomap()
{
    //--[No Automap]
    //--If the fade timer is maxed out, don't even render the automap since it'd be fullblack anyway.
    if(mMapFadeTimer >= 1.0f) return;

    //--Renders the automap. These are simple squares that indicate which areas can path to other areas.
    if(true)
    {
        glEnable(GL_STENCIL_TEST);
        glColorMask(false, false, false, false);
        glDepthMask(false);
        glStencilMask(0xFF);
        glStencilFunc(GL_ALWAYS, TL_STENCIL_CODE_MAP, 0xFF);
        glStencilOp(GL_REPLACE, GL_REPLACE, GL_REPLACE);
        Images.Data.rMask_World->Draw();

        //--Now turn on stencil-controlled rendering.
        Images.Data.rMapParts->Bind();
        glColorMask(true, true, true, true);
        glDepthMask(true);
        glStencilMask(0xFF);
        glStencilFunc(GL_EQUAL, TL_STENCIL_CODE_MAP, 0xFF);
        glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
    }

    //--[Blank Automap]
    //--Uses squares and simple shapes to represent the world. Works well in absence of a map tileset.
    if(mUseBlankAutomap || !rAutomapTileset)
    {
        RenderBlankAutomap(mMapAreaDim);
    }
    //--[Tileset Map]
    //--Used with a tileset, allows rendering of furniture, walls, and the like.
    else
    {
        RenderTiledAutomap(mMapAreaDim);
    }
    StarlightColor::ClearMixer();

    //--[Harsh Overlay]
    if(mHarshOverlayTimer > 0)
    {
        //--Compute alpha.
        float cAlphaMix = (float)mHarshOverlayTimer / (float)TL_OVERLAY_TICKS;
        StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, cAlphaMix);

        //--Iterate across the packages and render them.
        int i = 0;
        HarshOverlayPack *rOverlayPack = (HarshOverlayPack *)mHarshOverlayList->PushIterator();
        while(rOverlayPack)
        {
            //--Must have a valid image.
            if(!rOverlayPack->rImage)
            {
                i ++;
                rOverlayPack = (HarshOverlayPack *)mHarshOverlayList->AutoIterate();
                continue;
            }

            //--Exclusion layers render with a fixed size and increment the stencil. They stop
            //  normal layers from rendering.
            float cScale = mBackgroundScale;
            if(rOverlayPack->mOverlayFlags == TL_OVERLAY_EXCLUSION)
            {
                //--Set.
                glColorMask(true, true, true, true);
                glDepthMask(true);
                glStencilMask(0xFF);
                glStencilFunc(GL_EQUAL, TL_STENCIL_CODE_MAP, 0xFF);
                glStencilOp(GL_INCR, GL_INCR, GL_INCR);

                //--Render the image over the player's position.
                float cHFX = rOverlayPack->rImage->GetTrueWidth()  * 0.50f;
                float cHFY = rOverlayPack->rImage->GetTrueHeight() * 0.50f;
                float tX = mMapAreaDim.mXCenter;
                float tY = mMapAreaDim.mYCenter;
                glTranslatef(tX, tY, 0.0f);
                glScalef(cScale, cScale, 1.0f);
                rOverlayPack->rImage->Draw(-cHFX, -cHFY);
                glScalef(1.0f / cScale, 1.0f / cScale, 1.0f);
                glTranslatef(-tX, -tY, 0.0f);
            }
            //--Normal layers render wherever the exclusion layer is not.
            else if(rOverlayPack->mOverlayFlags == TL_OVERLAY_NORMAL)
            {
                //--Set.
                cScale = 1.0f;
                glColorMask(true, true, true, true);
                glDepthMask(true);
                glStencilMask(0xFF);
                glStencilFunc(GL_EQUAL, TL_STENCIL_CODE_MAP, 0xFF);
                glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);

                //--Compute texture snapping.
                float cTxL = rOverlayPack->mScrollX / cScale / rOverlayPack->mScrollClampX;
                float cTxT = rOverlayPack->mScrollY / cScale / rOverlayPack->mScrollClampY;
                float cTxR = (cTxL) + (mMapAreaDim.GetWidth()  / rOverlayPack->mScrollClampX / cScale);
                float cTxB = (cTxT) + (mMapAreaDim.GetHeight() / rOverlayPack->mScrollClampY / cScale);

                //--Draw it.
                rOverlayPack->rImage->Bind();
                glBegin(GL_QUADS);
                    glTexCoord2f(cTxL, cTxB); glVertex2f(mMapAreaDim.mLft, mMapAreaDim.mTop);
                    glTexCoord2f(cTxR, cTxB); glVertex2f(mMapAreaDim.mRgt, mMapAreaDim.mTop);
                    glTexCoord2f(cTxR, cTxT); glVertex2f(mMapAreaDim.mRgt, mMapAreaDim.mBot);
                    glTexCoord2f(cTxL, cTxT); glVertex2f(mMapAreaDim.mLft, mMapAreaDim.mBot);
                glEnd();
            }
            //--Bypass exclusion and render everywhere.
            else
            {
                //--Setup.
                cScale = 1.0f;
                glColorMask(true, true, true, true);
                glDepthMask(true);
                glStencilMask(0xFF);
                glStencilFunc(GL_LEQUAL, TL_STENCIL_CODE_MAP, 0xFF);
                glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);

                //--Compute texture snapping.
                float cTxL = rOverlayPack->mScrollX / cScale / rOverlayPack->mScrollClampX;
                float cTxT = rOverlayPack->mScrollY / cScale / rOverlayPack->mScrollClampY;
                float cTxR = (cTxL) + (mMapAreaDim.GetWidth()  / rOverlayPack->mScrollClampX / cScale);
                float cTxB = (cTxT) + (mMapAreaDim.GetHeight() / rOverlayPack->mScrollClampY / cScale);

                //--Draw it.
                rOverlayPack->rImage->Bind();
                glBegin(GL_QUADS);
                    glTexCoord2f(cTxL, cTxB); glVertex2f(mMapAreaDim.mLft, mMapAreaDim.mTop);
                    glTexCoord2f(cTxR, cTxB); glVertex2f(mMapAreaDim.mRgt, mMapAreaDim.mTop);
                    glTexCoord2f(cTxR, cTxT); glVertex2f(mMapAreaDim.mRgt, mMapAreaDim.mBot);
                    glTexCoord2f(cTxL, cTxT); glVertex2f(mMapAreaDim.mLft, mMapAreaDim.mBot);
                glEnd();
            }

            //--Next.
            i ++;
            rOverlayPack = (HarshOverlayPack *)mHarshOverlayList->AutoIterate();
        }
        StarlightColor::ClearMixer();
    }

    //--Fade handler.
    if(mMapFadeTimer > 0.0f)
    {
        SugarBitmap::DrawFullBlack(mMapFadeTimer);
    }

    //--Clean.
    glDisable(GL_STENCIL_TEST);
    StarlightColor::ClearMixer();
}
void TextLevel::RenderBlankAutomap(TwoDimensionReal pAreaDim)
{
    //--Renders the graphics-less version of the automap, using GL primitives.
    float cSizePerRoomX = 24.0f;
    float cSizePerRoomY = 24.0f;
    float cHfX = cSizePerRoomX * 0.50f;
    float cHfY = cSizePerRoomY * 0.50f;
    float cEffectiveCenterX = pAreaDim.mXCenter;
    float cEffectiveCenterY = pAreaDim.mYCenter;
    float cMapCenterX = pAreaDim.mXCenter + (mBGFocusX * cSizePerRoomX);
    float cMapCenterY = pAreaDim.mYCenter + (mBGFocusY * cSizePerRoomY);

    //--[Map Parts]
    //--Static lookups that represent entities on the map.
    static bool xHasBuiltLookups = false;
    static TwoDimensionReal xLookups[MAP_PARTS_TOTAL];
    if(!xHasBuiltLookups)
    {
        //--Setup.
        xHasBuiltLookups = true;
        float cBaseSize = cSizePerRoomX / Images.Data.rMapParts->GetTrueWidth();

        //--Build the lookups.
        for(int i = 0; i < MAP_PARTS_TOTAL; i ++)
        {
            xLookups[i].SetWH(i * cBaseSize, 0.0f, cBaseSize, 1.0f);
        }
    }

    //--Scaling.
    float cScale = 2.0f;
    glTranslatef(cEffectiveCenterX, cEffectiveCenterY, 0.0f);
    glScalef(cScale, cScale, 1.0f);
    glTranslatef(-cEffectiveCenterX, -cEffectiveCenterY, 0.0f);

    //--Render all the squares which represent the world.
    AutomapPack *rPackage = (AutomapPack *)mAutomapList->PushIterator();
    glBegin(GL_QUADS);
    while(rPackage)
    {
        //--[Unexplored Check]
        //--If this room is unexplored, don't render it.
        if(!rPackage->mIsDiscovered)
        {
            rPackage = (AutomapPack *)mAutomapList->AutoIterate();
            continue;
        }

        //--[Basic Rendering]
        //--If this room is explored but not visible, grey it out and don't render entities.
        StarlightColor::ClearMixer();
        if(!rPackage->mIsVisibleNow) StarlightColor::cxGrey.SetAsMixer();

        //--Setup.
        float pOffsetX = (rPackage->mX * cSizePerRoomX) + cMapCenterX;
        float cY = (rPackage->mY * cSizePerRoomY) + cMapCenterY;

        //--Render the base.
        glTexCoord2f(xLookups[MAP_PART_ROOM].mLft, xLookups[MAP_PART_ROOM].mTop); glVertex2f(pOffsetX - cHfX, cY - cHfY);
        glTexCoord2f(xLookups[MAP_PART_ROOM].mRgt, xLookups[MAP_PART_ROOM].mTop); glVertex2f(pOffsetX + cHfX, cY - cHfY);
        glTexCoord2f(xLookups[MAP_PART_ROOM].mRgt, xLookups[MAP_PART_ROOM].mBot); glVertex2f(pOffsetX + cHfX, cY + cHfY);
        glTexCoord2f(xLookups[MAP_PART_ROOM].mLft, xLookups[MAP_PART_ROOM].mBot); glVertex2f(pOffsetX - cHfX, cY + cHfY);

        //--Connectivity rendering.
        uint16_t tFlag = TL_CONNECT_N;
        for(int i = 0; i < 8; i ++)
        {
            if(rPackage->mConnectionFlags & tFlag)
            {
                glTexCoord2f(xLookups[i + MAP_PART_CONNECT_N].mLft, xLookups[i + MAP_PART_CONNECT_N].mBot); glVertex2f(pOffsetX - cHfX, cY - cHfY);
                glTexCoord2f(xLookups[i + MAP_PART_CONNECT_N].mRgt, xLookups[i + MAP_PART_CONNECT_N].mBot); glVertex2f(pOffsetX + cHfX, cY - cHfY);
                glTexCoord2f(xLookups[i + MAP_PART_CONNECT_N].mRgt, xLookups[i + MAP_PART_CONNECT_N].mTop); glVertex2f(pOffsetX + cHfX, cY + cHfY);
                glTexCoord2f(xLookups[i + MAP_PART_CONNECT_N].mLft, xLookups[i + MAP_PART_CONNECT_N].mTop); glVertex2f(pOffsetX - cHfX, cY + cHfY);
            }

            tFlag = tFlag * 2;
        }

        //--Up indicator.
        if(rPackage->mConnectionFlags & TL_CONNECT_UP)
        {
            glTexCoord2f(xLookups[MAP_PART_UPSTAIR].mLft, xLookups[MAP_PART_UPSTAIR].mBot); glVertex2f(pOffsetX - cHfX, cY - cHfY);
            glTexCoord2f(xLookups[MAP_PART_UPSTAIR].mRgt, xLookups[MAP_PART_UPSTAIR].mBot); glVertex2f(pOffsetX + cHfX, cY - cHfY);
            glTexCoord2f(xLookups[MAP_PART_UPSTAIR].mRgt, xLookups[MAP_PART_UPSTAIR].mTop); glVertex2f(pOffsetX + cHfX, cY + cHfY);
            glTexCoord2f(xLookups[MAP_PART_UPSTAIR].mLft, xLookups[MAP_PART_UPSTAIR].mTop); glVertex2f(pOffsetX - cHfX, cY + cHfY);
        }

        //--Down indicator.
        if(rPackage->mConnectionFlags & TL_CONNECT_UP)
        {
            glTexCoord2f(xLookups[MAP_PART_DNSTAIR].mLft, xLookups[MAP_PART_DNSTAIR].mBot); glVertex2f(pOffsetX - cHfX, cY - cHfY);
            glTexCoord2f(xLookups[MAP_PART_DNSTAIR].mRgt, xLookups[MAP_PART_DNSTAIR].mBot); glVertex2f(pOffsetX + cHfX, cY - cHfY);
            glTexCoord2f(xLookups[MAP_PART_DNSTAIR].mRgt, xLookups[MAP_PART_DNSTAIR].mTop); glVertex2f(pOffsetX + cHfX, cY + cHfY);
            glTexCoord2f(xLookups[MAP_PART_DNSTAIR].mLft, xLookups[MAP_PART_DNSTAIR].mTop); glVertex2f(pOffsetX - cHfX, cY + cHfY);
        }

        //--[Entity Rendering]
        //--Don't render entities if not visible.
        if(!rPackage->mIsVisibleNow)
        {
            rPackage = (AutomapPack *)mAutomapList->AutoIterate();
            continue;
        }

        //--Hostile.
        if(rPackage->mHasEnemyIndicator)
        {
            glTexCoord2f(xLookups[MAP_PART_HOSTILE].mLft, xLookups[MAP_PART_HOSTILE].mBot); glVertex2f(pOffsetX - cHfX, cY - cHfY);
            glTexCoord2f(xLookups[MAP_PART_HOSTILE].mRgt, xLookups[MAP_PART_HOSTILE].mBot); glVertex2f(pOffsetX + cHfX, cY - cHfY);
            glTexCoord2f(xLookups[MAP_PART_HOSTILE].mRgt, xLookups[MAP_PART_HOSTILE].mTop); glVertex2f(pOffsetX + cHfX, cY + cHfY);
            glTexCoord2f(xLookups[MAP_PART_HOSTILE].mLft, xLookups[MAP_PART_HOSTILE].mTop); glVertex2f(pOffsetX - cHfX, cY + cHfY);
        }

        //--Friendly.
        if(rPackage->mHasFriendlyIndicator)
        {
            glTexCoord2f(xLookups[MAP_PART_FRIENDLY].mLft, xLookups[MAP_PART_FRIENDLY].mBot); glVertex2f(pOffsetX - cHfX, cY - cHfY);
            glTexCoord2f(xLookups[MAP_PART_FRIENDLY].mRgt, xLookups[MAP_PART_FRIENDLY].mBot); glVertex2f(pOffsetX + cHfX, cY - cHfY);
            glTexCoord2f(xLookups[MAP_PART_FRIENDLY].mRgt, xLookups[MAP_PART_FRIENDLY].mTop); glVertex2f(pOffsetX + cHfX, cY + cHfY);
            glTexCoord2f(xLookups[MAP_PART_FRIENDLY].mLft, xLookups[MAP_PART_FRIENDLY].mTop); glVertex2f(pOffsetX - cHfX, cY + cHfY);
        }

        //--Examinable.
        if(rPackage->mHasExaminableIndicator)
        {
            glTexCoord2f(xLookups[MAP_PART_EXAMINABLE].mLft, xLookups[MAP_PART_EXAMINABLE].mBot); glVertex2f(pOffsetX - cHfX, cY - cHfY);
            glTexCoord2f(xLookups[MAP_PART_EXAMINABLE].mRgt, xLookups[MAP_PART_EXAMINABLE].mBot); glVertex2f(pOffsetX + cHfX, cY - cHfY);
            glTexCoord2f(xLookups[MAP_PART_EXAMINABLE].mRgt, xLookups[MAP_PART_EXAMINABLE].mTop); glVertex2f(pOffsetX + cHfX, cY + cHfY);
            glTexCoord2f(xLookups[MAP_PART_EXAMINABLE].mLft, xLookups[MAP_PART_EXAMINABLE].mTop); glVertex2f(pOffsetX - cHfX, cY + cHfY);
        }

        //--Item.
        if(rPackage->mHasItemIndicator)
        {
            glTexCoord2f(xLookups[MAP_PART_ITEM].mLft, xLookups[MAP_PART_ITEM].mBot); glVertex2f(pOffsetX - cHfX, cY - cHfY);
            glTexCoord2f(xLookups[MAP_PART_ITEM].mRgt, xLookups[MAP_PART_ITEM].mBot); glVertex2f(pOffsetX + cHfX, cY - cHfY);
            glTexCoord2f(xLookups[MAP_PART_ITEM].mRgt, xLookups[MAP_PART_ITEM].mTop); glVertex2f(pOffsetX + cHfX, cY + cHfY);
            glTexCoord2f(xLookups[MAP_PART_ITEM].mLft, xLookups[MAP_PART_ITEM].mTop); glVertex2f(pOffsetX - cHfX, cY + cHfY);
        }

        //--[Next]
        rPackage = (AutomapPack *)mAutomapList->AutoIterate();
    }

    //--Render the player's indicator.
    StarlightColor::ClearMixer();
    float cPlayerX = cMapCenterX - (mBGFocusX * cSizePerRoomX);
    float cPlayerY = cMapCenterY - (mBGFocusY * cSizePerRoomY);
    glTexCoord2f(xLookups[MAP_PART_PLAYER].mLft, xLookups[MAP_PART_PLAYER].mTop); glVertex2f(cPlayerX - cHfX, cPlayerY - cHfY);
    glTexCoord2f(xLookups[MAP_PART_PLAYER].mRgt, xLookups[MAP_PART_PLAYER].mTop); glVertex2f(cPlayerX + cHfX, cPlayerY - cHfY);
    glTexCoord2f(xLookups[MAP_PART_PLAYER].mRgt, xLookups[MAP_PART_PLAYER].mBot); glVertex2f(cPlayerX + cHfX, cPlayerY + cHfY);
    glTexCoord2f(xLookups[MAP_PART_PLAYER].mLft, xLookups[MAP_PART_PLAYER].mBot); glVertex2f(cPlayerX - cHfX, cPlayerY + cHfY);

    //--Clean up.
    glEnd();
    glTranslatef(cEffectiveCenterX, cEffectiveCenterY, 0.0f);
    glScalef(1.0f / cScale, 1.0f / cScale, 1.0f);
    glTranslatef(-cEffectiveCenterX, -cEffectiveCenterY, 0.0f);
}
void TextLevel::RenderTiledAutomap(TwoDimensionReal pAreaDim)
{
    //--Renders the automap using the more complicated tileset routines.
    rAutomapTileset->Bind();

    //--Recompute the player's movement offsets.
    mPlayerMovePercent = EasingFunction::QuadraticOut(mPlayerMoveTimer, mPlayerMoveTimerMax);
    mPlayerTranslationX = mScreenTranslationX * (1.0f - mPlayerMovePercent);
    mPlayerTranslationY = mScreenTranslationY * (1.0f - mPlayerMovePercent);

    //--Map scaling.
    float cEffectiveCenterX = pAreaDim.mXCenter;
    float cEffectiveCenterY = pAreaDim.mYCenter;
    float cMapCenterX = pAreaDim.mXCenter + (mBGFocusX * mSizePerRoomX) - mPlayerTranslationX;
    float cMapCenterY = pAreaDim.mYCenter + (mBGFocusY * mSizePerRoomY) - mPlayerTranslationY;

    //--Scaling.
    float cScale = mBackgroundScale;
    glTranslatef(cEffectiveCenterX, cEffectiveCenterY, 0.0f);
    glScalef(cScale, cScale, 1.0f);
    glTranslatef(-cEffectiveCenterX, -cEffectiveCenterY, 0.0f);

    //--Begin rendering.
    glBegin(GL_QUADS);
    AutomapPack *rHighlightTile = NULL;

    //--[Render-Under]
    //--Render-under. This renders all floors under the current one. If -1000, ignore it.
    if(mUnderRenderingZLo != -1000)
    {
        //--Render the base tilemap.
        AutomapPack *rPackage = (AutomapPack *)mAutomapList->PushIterator();
        while(rPackage)
        {
            //--Render it.
            if(rPackage->mZ >= mUnderRenderingZLo && rPackage->mZ <= mUnderRenderingZHi)
            {
                float cXPos = (rPackage->mX * mSizePerRoomX) + cMapCenterX;
                float cYPos = (rPackage->mY * mSizePerRoomY) + cMapCenterY;
                RenderAutomapTile(rPackage, cXPos, cYPos, AM_RENDER_BELOW | AM_RENDER_TILE | AM_RENDER_ANIMATIONS);
            }

            //--[Next]
            rPackage = (AutomapPack *)mAutomapList->AutoIterate();
        }

        //--Render doors over the tilemap, with forced greyout.
        rPackage = (AutomapPack *)mAutomapList->PushIterator();
        while(rPackage)
        {
            //--Render it.
            if(rPackage->mZ >= mUnderRenderingZLo && rPackage->mZ <= mUnderRenderingZHi)
            {
                float cXPos = (rPackage->mX * mSizePerRoomX) + cMapCenterX;
                float cYPos = (rPackage->mY * mSizePerRoomY) + cMapCenterY;
                RenderAutomapTileDoors(rPackage, cXPos, cYPos, true);
            }

            //--[Next]
            rPackage = (AutomapPack *)mAutomapList->AutoIterate();
        }
    }

    //--[Render Primary]
    //--Render all the squares which represent the world.
    AutomapPack *rPackage = (AutomapPack *)mAutomapList->PushIterator();
    while(rPackage)
    {
        //--Render it.
        if(rPackage->mZ == mCurrentRenderingZ)
        {
            if(rPackage->mIsHighlighted) rHighlightTile = rPackage;
            float cXPos = (rPackage->mX * mSizePerRoomX) + cMapCenterX;
            float cYPos = (rPackage->mY * mSizePerRoomY) + cMapCenterY;
            RenderAutomapTile(rPackage, cXPos, cYPos, (AM_RENDER_TILE | AM_RENDER_ANIMATIONS | AM_RENDER_FADES));
        }

        //--[Next]
        rPackage = (AutomapPack *)mAutomapList->AutoIterate();
    }

    //--Render Indicators.
    rPackage = (AutomapPack *)mAutomapList->PushIterator();
    while(rPackage)
    {
        //--Render it.
        if(rPackage->mZ == mCurrentRenderingZ)
        {
            float cXPos = (rPackage->mX * mSizePerRoomX) + cMapCenterX;
            float cYPos = (rPackage->mY * mSizePerRoomY) + cMapCenterY;
            RenderAutomapTile(rPackage, cXPos, cYPos, AM_RENDER_INDICATORS);
        }

        //--[Next]
        rPackage = (AutomapPack *)mAutomapList->AutoIterate();
    }

    //--Render doors over the tilemap, with forced greyout.
    rPackage = (AutomapPack *)mAutomapList->PushIterator();
    while(rPackage)
    {
        //--Render it.
        if(rPackage->mZ == mCurrentRenderingZ)
        {
            float cXPos = (rPackage->mX * mSizePerRoomX) + cMapCenterX;
            float cYPos = (rPackage->mY * mSizePerRoomY) + cMapCenterY;
            RenderAutomapTileDoors(rPackage, cXPos, cYPos, false);
        }

        //--[Next]
        rPackage = (AutomapPack *)mAutomapList->AutoIterate();
    }

    //--Clean up.
    glEnd();

    //--Render the highlight tile.
    if(rHighlightTile)
    {
        //--Position.
        float cXPos = (rHighlightTile->mX * mSizePerRoomX) + cMapCenterX - mHfX;
        float cYPos = (rHighlightTile->mY * mSizePerRoomY) + cMapCenterY - mHfY;

        //--No door highlight:
        if(rHighlightTile->mDoorHighlight == AMP_NONE)
        {
            Images.Data.rOverlayMove->Draw(cXPos, cYPos);
        }
        else if(rHighlightTile->mDoorHighlight == AMP_NORTH)
        {
            Images.Data.rOverlayDoorH->Draw(cXPos + (cDoorNorth.mLft * mSizePerRoomX) - 1.0f, cYPos + (cDoorNorth.mTop * mSizePerRoomY) - 6.0f);
        }
        else if(rHighlightTile->mDoorHighlight == AMP_EAST)
        {
            Images.Data.rOverlayDoorV->Draw(cXPos + (cDoorEast.mLft * mSizePerRoomX)  - 2.0f,  cYPos + (cDoorEast.mTop * mSizePerRoomY) - 2.0f);
        }
        else if(rHighlightTile->mDoorHighlight == AMP_SOUTH)
        {
            Images.Data.rOverlayDoorH->Draw(cXPos + (cDoorSouth.mLft * mSizePerRoomX) - 1.0f, cYPos + (cDoorSouth.mTop * mSizePerRoomY) - 2.0f);
        }
        else if(rHighlightTile->mDoorHighlight == AMP_WEST)
        {
            Images.Data.rOverlayDoorV->Draw(cXPos + (cDoorWest.mLft * mSizePerRoomX)  - 6.0f,  cYPos + (cDoorWest.mTop * mSizePerRoomY) - 2.0f);
        }
    }

    //--Clean up.
    glTranslatef(cEffectiveCenterX, cEffectiveCenterY, 0.0f);
    glScalef(1.0f / cScale, 1.0f / cScale, 1.0f);
    glTranslatef(-cEffectiveCenterX, -cEffectiveCenterY, 0.0f);
}
void TextLevel::RenderAutomapTile(AutomapPack *pTile, float pOffsetX, float pOffsetY, uint16_t pRenderProperties)
{
    //--Renders the provided tile. Different properties (entities, animations, etc) can be toggled on or off with the properties flag.
    if(!pTile) return;

    //--Depth offset.
    float cDepthOffset = 0.000000f;
    float cDepthOffsetHostile = -0.000002f;

    //--[Alphas]
    //--Compute rendering alphas.
    float tPrevAlpha      = pTile->mPrevFadeTimer  / (float)AM_FADE_TICKS;
    float tFadeAlpha      = pTile->mFadeTimer      / (float)AM_FADE_TICKS;
    float tPrevInsetAlpha = pTile->mPrevInsetTimer / (float)AM_FADE_TICKS;
    float tFadeInsetAlpha = pTile->mInsetTimer     / (float)AM_FADE_TICKS;

    //--Color Mixer
    float cWhiteMixer = 1.0f;
    if(pRenderProperties & AM_RENDER_BELOW)
    {
        cDepthOffset = -0.1f;
        cWhiteMixer = 0.25f;
    }

    //--Set color mixing.
    StarlightColor::SetMixer(cWhiteMixer, cWhiteMixer, cWhiteMixer, 1.0f);

    //--Position.
    int tX = (int)pTile->mX;
    int tY = (int)pTile->mY;
    //float cMapCenterX = pAreaDim.mXCenter + (mBGFocusX * cSizePerRoomX);
    //float cMapCenterY = pAreaDim.mYCenter + (mBGFocusY * cSizePerRoomY);

    //--[Unexplored Check]
    //--If this room is unexplored, don't render its terrain. Entities can still render, but only
    //  as they are moving away.
    if(pTile->mIsDiscovered)
    {
        //--We must be flagged to render terrain:
        if(pRenderProperties & AM_RENDER_TILE)
        {
            //--Stop rendering, we need to use a new set of tilesets.
            glEnd();

            //--Get the matching Z level.
            TextLevZLevel *rZLevel = (TextLevZLevel *)mZLevels->PushIterator();
            while(rZLevel)
            {
                //--Match, render.
                if(rZLevel->mZLevel == (int)pTile->mZ)
                {
                    //--Compute layer position. cXOffCon/cYOffCon represents the left/top "padding" on the map.
                    int cXOffCon = TL_STANDARD_PAD;
                    int cYOffCon = TL_STANDARD_PAD;
                    float tXOffset = pOffsetX - 32.0f;
                    float tYOffset = pOffsetY - 32.0f;
                    glTranslatef(tXOffset, tYOffset, cDepthOffset);
                    pTile->mLastRenderX = tXOffset;
                    pTile->mLastRenderY = tYOffset;

                    //--Compute which tile to render. We offset to handle the edge padding and different sizes between Z levels.
                    int tTileX = tX + cXOffCon - mLftMost - (rZLevel->mTileOffX / 64);
                    int tTileY = tY + cYOffCon - mTopMost - (rZLevel->mTileOffY / 64);
                    //fprintf(stderr, "%i X off %i\n", rZLevel->mZLevel, rZLevel->mTileOffX);

                    //--Iterate across all layers and render them.
                    TileLayer *rLayer = (TileLayer *)rZLevel->mTileLayers->PushIterator();
                    while(rLayer)
                    {
                        rLayer->RenderTile(tTileX, tTileY);
                        rLayer = (TileLayer *)rZLevel->mTileLayers->AutoIterate();
                    }

                    //--Clean.
                    glTranslatef(-tXOffset, -tYOffset, -cDepthOffset);
                }
                rZLevel = (TextLevZLevel *)mZLevels->AutoIterate();
            }

            //--Rebind the tileset.
            rAutomapTileset->Bind();
            glBegin(GL_QUADS);
        }

        //--[Animated Overlays]
        //--These render overtop of the room but under the fade.
        if(pRenderProperties & AM_RENDER_ANIMATIONS)
        {
            //--Setup.
            int tUseFrameX, tUseFrameY;

            //--Iterate.
            TilesetAnimation *rAnimation = (TilesetAnimation *)pTile->mrAnimationsList->PushIterator();
            while(rAnimation)
            {
                //--Add the random offset to the frame, if the animation is on one row.
                if(rAnimation->mYChangeEveryXFrames < 1)
                {
                    tUseFrameX = rAnimation->mFrameStartX + ((rAnimation->mTotalFramesRun + pTile->mAnimationRandomOffset) % rAnimation->mTotalFrames);
                    tUseFrameY = rAnimation->mFrameStartY;
                }
                //--Multiple rows.
                else
                {
                    int tTotalRows = rAnimation->mTotalFrames / rAnimation->mYChangeEveryXFrames;
                    tUseFrameX = rAnimation->mFrameStartX + ((rAnimation->mTotalFramesRun + pTile->mAnimationRandomOffset) % rAnimation->mYChangeEveryXFrames);
                    tUseFrameY = rAnimation->mFrameStartY + (((rAnimation->mTotalFramesRun + pTile->mAnimationRandomOffset) / rAnimation->mYChangeEveryXFrames)) % tTotalRows;
                }

                //--Positions.
                cTxL = mTexXPerTile * tUseFrameX;
                cTxT = mTexYPerTile * tUseFrameY;
                cTxR = cTxL + mTexXPerTile;
                cTxB = cTxT + mTexYPerTile;
                cTxT = 1.0f - cTxT;
                cTxB = 1.0f - cTxB;
                glTexCoord2f(cTxL, cTxT); glVertex3f(pOffsetX - mHfX, pOffsetY - mHfY, cDepthTerrain + cDepthOffset);
                glTexCoord2f(cTxR, cTxT); glVertex3f(pOffsetX + mHfX, pOffsetY - mHfY, cDepthTerrain + cDepthOffset);
                glTexCoord2f(cTxR, cTxB); glVertex3f(pOffsetX + mHfX, pOffsetY + mHfY, cDepthTerrain + cDepthOffset);
                glTexCoord2f(cTxL, cTxB); glVertex3f(pOffsetX - mHfX, pOffsetY + mHfY, cDepthTerrain + cDepthOffset);

                //--Next.
                rAnimation = (TilesetAnimation *)pTile->mrAnimationsList->AutoIterate();
            }
        }
    }

    //--[Fade Handler]
    //--Fades don't render for unexplored rooms.
    if(pTile->mIsDiscovered && pRenderProperties & AM_RENDER_FADES)
    {
        //--Render normally.
        if(pTile->mIsVisibleNow)
        {
            RenderFadeBorder(pOffsetX, pOffsetY, pTile->mPrevFadeCode, pTile->mPrevInsetCode, tPrevAlpha, tPrevInsetAlpha);
            RenderFadeBorder(pOffsetX, pOffsetY, pTile->mFadeCode,     pTile->mInsetCode,     tFadeAlpha, tFadeInsetAlpha);
        }
        //--Full fade.
        else
        {
            RenderFadeBorder(pOffsetX, pOffsetY, pTile->mPrevFadeCode, pTile->mPrevInsetCode, tPrevAlpha, tPrevInsetAlpha);
            RenderFadeBorder(pOffsetX, pOffsetY, AM_FADE_OVER,         AM_FADE_NONE,          tFadeAlpha, tFadeInsetAlpha);
        }
    }

    //--[Entity Rendering]
    //--Entities can render when not in a visible room, but to do this they must be moving from
    //  a room which was visible, or specially flagged.
    if(pRenderProperties & AM_RENDER_INDICATORS)
    {
        //--Position offset.
        float cGUIOffsetX = mMapAreaDim.mXCenter + (mBGFocusX * mSizePerRoomX) - mPlayerTranslationX;
        float cGUIOffsetY = mMapAreaDim.mYCenter + (mBGFocusY * mSizePerRoomY) - mPlayerTranslationY;

        //--Fast-access variables.
        float cLft, cTop, cRgt, cBot, cDep;

        //--Iterate across the indicators.
        AutomapPackTilemapLayer *rLayer = (AutomapPackTilemapLayer *)pTile->mEntityIndicators->PushIterator();
        while(rLayer)
        {
            //--Compute render position.
            cLft = rLayer->mXRender - mHfX + cGUIOffsetX;
            cTop = rLayer->mYRender - mHfY + cGUIOffsetY;
            cRgt = rLayer->mXRender + mHfX + cGUIOffsetX;
            cBot = rLayer->mYRender + mHfY + cGUIOffsetY;
            cDep = rLayer->mZRender + cDepthOffset;

            //--Special: The player renders at the GUI center regardless of other factors.
            if(rLayer->mPriority == AMTL_PRIORITY_PLAYER)
            {
                cLft = pOffsetX + mPlayerTranslationX - mHfX;
                cTop = pOffsetY + mPlayerTranslationY - mHfY;
                cRgt = pOffsetX + mPlayerTranslationX + mHfX;
                cBot = pOffsetY + mPlayerTranslationY + mHfY;
            }

            //--Render.
            StarlightColor::SetMixer(cWhiteMixer, cWhiteMixer, cWhiteMixer, rLayer->mARender);
            glTexCoord2f(rLayer->mTxDim.mLft, rLayer->mTxDim.mTop); glVertex3f(cLft, cTop, cDep);
            glTexCoord2f(rLayer->mTxDim.mRgt, rLayer->mTxDim.mTop); glVertex3f(cRgt, cTop, cDep);
            glTexCoord2f(rLayer->mTxDim.mRgt, rLayer->mTxDim.mBot); glVertex3f(cRgt, cBot, cDep);
            glTexCoord2f(rLayer->mTxDim.mLft, rLayer->mTxDim.mBot); glVertex3f(cLft, cBot, cDep);

            //--If hostile, render:
            if(rLayer->mHasHostilityMarker)
            {
                glTexCoord2f(rLayer->mHsDim.mLft, rLayer->mHsDim.mTop); glVertex3f(cLft, cTop, cDep + cDepthOffsetHostile);
                glTexCoord2f(rLayer->mHsDim.mRgt, rLayer->mHsDim.mTop); glVertex3f(cRgt, cTop, cDep + cDepthOffsetHostile);
                glTexCoord2f(rLayer->mHsDim.mRgt, rLayer->mHsDim.mBot); glVertex3f(cRgt, cBot, cDep + cDepthOffsetHostile);
                glTexCoord2f(rLayer->mHsDim.mLft, rLayer->mHsDim.mBot); glVertex3f(cLft, cBot, cDep + cDepthOffsetHostile);
            }

            //--Next.
            rLayer = (AutomapPackTilemapLayer *)pTile->mEntityIndicators->AutoIterate();
        }

        //--Clean up.
        StarlightColor::ClearMixer();
    }

    //--[Discovery]
    //--When a room is first discovered, an overlay renders over it and fades out.
    if(pTile->mDiscoveryTimer < AM_FADE_TICKS && pRenderProperties & AM_RENDER_FADES)
    {
        float tDiscoveryAlpha = 1.0f - (pTile->mDiscoveryTimer / (float)AM_FADE_TICKS);
        RenderFadeBorder(pOffsetX, pOffsetY, AM_FADE_FULLBLACK, AM_FADE_NONE, tDiscoveryAlpha, 0.0f);
    }

    //--Clean alpha.
    StarlightColor::ClearMixer();
}
void TextLevel::RenderAutomapTileDoors(AutomapPack *pTile, float pOffsetX, float pOffsetY, bool pIsForceGreyed)
{
    //--Renders doors in the given room. Similar to rendering the tile, but doesn't use special flags.
    float cDepthOffset = 0.0f;
    StarlightColor tWhiteCol = StarlightColor::MapRGBAF(1.0f, 1.0f, 1.0f, 1.0f);
    StarlightColor tGreyCol = StarlightColor::MapRGBAF(1.0f, 1.0f, 1.0f, 1.0f);
    if(pIsForceGreyed)
    {
        cDepthOffset = cDepthOffset -0.1f;
        tWhiteCol = StarlightColor::MapRGBAF(0.25f, 0.25f, 0.25f, 1.0f);
        tGreyCol = StarlightColor::MapRGBAF(0.125f, 0.125f, 0.125f, 1.0f);
    }

    //--Get the room's coordinates in the fixed map.
    int tX = ((int)pTile->mX) - mWorldOffsetX;
    int tY = ((int)pTile->mY) - mWorldOffsetY;
    int tZ = ((int)pTile->mZ) - mWorldOffsetZ;

    //--Set flags.
    bool tIsEDiscovered = false;
    bool tIsSDiscovered = false;
    bool tIsEVisible = false;
    bool tIsSVisible = false;

    //--Discovered rooms render all doors.
    if(pTile->mIsDiscovered)
    {
        tIsEDiscovered = true;
        tIsSDiscovered = true;
    }
    //--Undiscovered rooms can still render doors if adjacent rooms are discovered.
    else
    {
        if(mrFixedWorld[tX+1][tY][tZ] && mrFixedWorld[tX+1][tY][tZ]->mIsDiscovered) tIsEDiscovered = true;
        if(mrFixedWorld[tX][tY+1][tZ] && mrFixedWorld[tX][tY+1][tZ]->mIsDiscovered) tIsSDiscovered = true;
    }

    //--Visiblity. Whether or not the player can see it right now.
    if(pTile->mIsVisibleNow)
    {
        tIsEVisible = true;
        tIsSVisible = true;
    }
    //--Invisible rooms can still render doors if adjacent rooms are visible.
    else
    {
        if(mrFixedWorld[tX+1][tY][tZ] && mrFixedWorld[tX+1][tY][tZ]->mIsVisibleNow) tIsEVisible = true;
        if(mrFixedWorld[tX][tY+1][tZ] && mrFixedWorld[tX][tY+1][tZ]->mIsVisibleNow) tIsSVisible = true;
    }

    //--Iterate along the layers. We're only looking for door slots.
    for(int i = 0; i < pTile->mTilemapLayersTotal; i ++)
    {
        //--Skip unflagged cases.
        int tSlot = pTile->mTilemapLayers[i].mSlotOffset;
        if(tSlot != AM_SLOT_DOOR_S_CLOSED && tSlot != AM_SLOT_DOOR_S_OPEN && tSlot != AM_SLOT_DOOR_E_CLOSED && tSlot != AM_SLOT_DOOR_E_OPEN) continue;

        //--Texture positions.
        cTxL = mTexXPerTile * pTile->mTilemapLayers[i].mX;
        cTxT = mTexYPerTile * pTile->mTilemapLayers[i].mY;
        cTxR = cTxL + mTexXPerTile;
        cTxB = cTxT + mTexYPerTile;

        //--Invert vertical texture coordinates.
        cTxT = 1.0f - cTxT;
        cTxB = 1.0f - cTxB;

        //--Flag setup.
        uint8_t tUseRotation = pTile->mTilemapLayers[i].mRotationFlags;
        float cUseDepth = cDepthTerrain;
        tWhiteCol.SetAsMixer();

        //--Resolve positions.
        int tUseXInd = -1;
        int tUseYInd = -1;
        float cLft = pOffsetX - mHfX;
        float cTop = pOffsetY - mHfY;
        float cIndLft = cLft;
        float cIndTop = cTop;
        if(pTile->mTilemapLayers[i].mSlotOffset == AM_SLOT_DOOR_S_CLOSED && tIsSDiscovered)
        {
            if(!tIsSVisible) tGreyCol.SetAsMixer();
            cUseDepth = cUseDepth + 0.000001f;
            cLft = cLft + 22.0f;
            cTop = cTop + 60.0f;
            cIndLft = cLft + 20.0f;
            cIndTop = cTop + 0.0f;
            tUseXInd = pTile->mDoorSXTile;
            tUseYInd = pTile->mDoorSYTile;
        }
        else if(pTile->mTilemapLayers[i].mSlotOffset == AM_SLOT_DOOR_S_OPEN && tIsSDiscovered)
        {
            if(!tIsSVisible) tGreyCol.SetAsMixer();
            tUseRotation |= AM_ROTATE90;
            cUseDepth = cUseDepth + 0.000001f;
            cLft = cLft + 23.0f - 43.0f;
            cTop = cTop + 61.0f - 17.0f;
            cIndLft = cLft + 20.0f;
            cIndTop = cTop + 0.0f;
            tUseXInd = pTile->mDoorSXTile;
            tUseYInd = pTile->mDoorSYTile;
        }
        else if(pTile->mTilemapLayers[i].mSlotOffset == AM_SLOT_DOOR_E_CLOSED && tIsEDiscovered)
        {
            if(!tIsEVisible) tGreyCol.SetAsMixer();
            tUseRotation |= AM_ROTATE90;
            cUseDepth = cUseDepth + 0.000001f;
            cLft = cLft + 22.0f - 18.0f;
            cTop = cTop + 60.0f - 39.0f;
            cIndLft = cLft + 52.0f;
            cIndTop = cTop + 21.0f;
            tUseXInd = pTile->mDoorEXTile;
            tUseYInd = pTile->mDoorEYTile;
        }
        else if(pTile->mTilemapLayers[i].mSlotOffset == AM_SLOT_DOOR_E_OPEN && tIsEDiscovered)
        {
            if(!tIsEVisible) tGreyCol.SetAsMixer();
            cUseDepth = cUseDepth + 0.000001f;
            cLft = cLft + 23.0f + 41.0f;
            cTop = cTop + 61.0f - 24.0f;
            cIndLft = cLft + 52.0f;
            cIndTop = cTop + 21.0f;
            tUseXInd = pTile->mDoorEXTile;
            tUseYInd = pTile->mDoorEYTile;
        }
        //--Could not rendering since the directions were not visible.
        else
        {
            continue;
        }

        //--Finish.
        float cRgt = cLft + mSizePerRoomX;
        float cBot = cTop + mSizePerRoomY;

        //--HFlip and VFlip.
        if(tUseRotation & AM_HFLIP)
        {
            SwapF(cLft, cRgt);
        }
        if(tUseRotation & AM_VFLIP)
        {
            SwapF(cTop, cBot);
        }

        //--Rotate 90 degrees.
        if(tUseRotation & AM_ROTATE90)
        {
            cRenderX[3] = cLft; cRenderY[3] = cTop;
            cRenderX[0] = cRgt; cRenderY[0] = cTop;
            cRenderX[1] = cRgt; cRenderY[1] = cBot;
            cRenderX[2] = cLft; cRenderY[2] = cBot;
        }
        //--Rotate 180 degrees.
        else if(tUseRotation & AM_ROTATE180)
        {
            cRenderX[2] = cLft; cRenderY[2] = cTop;
            cRenderX[3] = cRgt; cRenderY[3] = cTop;
            cRenderX[0] = cRgt; cRenderY[0] = cBot;
            cRenderX[1] = cLft; cRenderY[1] = cBot;
        }
        //--Rotate 270 degrees.
        else if(tUseRotation & AM_ROTATE270)
        {
            cRenderX[1] = cLft; cRenderY[1] = cTop;
            cRenderX[2] = cRgt; cRenderY[2] = cTop;
            cRenderX[3] = cRgt; cRenderY[3] = cBot;
            cRenderX[0] = cLft; cRenderY[0] = cBot;
        }
        //--No rotation.
        else
        {
            cRenderX[0] = cLft; cRenderY[0] = cTop;
            cRenderX[1] = cRgt; cRenderY[1] = cTop;
            cRenderX[2] = cRgt; cRenderY[2] = cBot;
            cRenderX[3] = cLft; cRenderY[3] = cBot;
        }

        //--Render.
        glTexCoord2f(cTxL, cTxT); glVertex3f(cRenderX[0], cRenderY[0], cUseDepth + cDepthOffset);
        glTexCoord2f(cTxR, cTxT); glVertex3f(cRenderX[1], cRenderY[1], cUseDepth + cDepthOffset);
        glTexCoord2f(cTxR, cTxB); glVertex3f(cRenderX[2], cRenderY[2], cUseDepth + cDepthOffset);
        glTexCoord2f(cTxL, cTxB); glVertex3f(cRenderX[3], cRenderY[3], cUseDepth + cDepthOffset);

        //--If there is an indicator, render that as well.
        if(tUseXInd != -1 && tUseYInd != -1)
        {
            //--Coordinates.
            cTxL = mTexXPerTile * tUseXInd;
            cTxT = mTexYPerTile * tUseYInd;
            cTxR = cTxL + mTexXPerTile;
            cTxB = cTxT + mTexYPerTile;
            cTxT = 1.0f - cTxT;
            cTxB = 1.0f - cTxB;
            float cIndRgt = cIndLft + mSizePerRoomX;
            float cIndBot = cIndTop + mSizePerRoomY;

            //--Render.
            cRenderX[0] = cIndLft; cRenderY[0] = cIndTop;
            cRenderX[1] = cIndRgt; cRenderY[1] = cIndTop;
            cRenderX[2] = cIndRgt; cRenderY[2] = cIndBot;
            cRenderX[3] = cIndLft; cRenderY[3] = cIndBot;
            glTexCoord2f(cTxL, cTxT); glVertex3f(cRenderX[0], cRenderY[0], cUseDepth + cDepthOffset);
            glTexCoord2f(cTxR, cTxT); glVertex3f(cRenderX[1], cRenderY[1], cUseDepth + cDepthOffset);
            glTexCoord2f(cTxR, cTxB); glVertex3f(cRenderX[2], cRenderY[2], cUseDepth + cDepthOffset);
            glTexCoord2f(cTxL, cTxB); glVertex3f(cRenderX[3], cRenderY[3], cUseDepth + cDepthOffset);
        }
    }

    //--Clean alpha.
    StarlightColor::ClearMixer();
}
void Inlay(float &sLft, float &sTop, float &sRgt, float &sBot)
{
    //sLft = sLft + 0.0005f;
    //sTop = sTop + 0.0005f;
    //sRgt = sRgt - 0.0005f;
    //sBot = sBot - 0.0005f;
}
void TextLevel::RenderFadeBorder(float pX, float pY, int pBorderCode, int pInsetCode, float pBorderAlpha, float pInsetAlpha)
{
    //--Renders the fade border around a given room. Expects the texture to already be bound.
    if(pBorderCode == AM_FADE_NONE && pInsetCode == AM_FADE_INSET_NONE) return;

    //--Set alpha mixer.
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pBorderAlpha);

    //--Texture sizing.
    float cSizePerRoomX = 63.9950f;
    float cSizePerRoomY = 63.9950f;
    float cExtraEdge = 0.00f;
    float cTexXPerTile = mTilemapXSizePerTile / mTilemapXSize;
    float cTexYPerTile = mTilemapYSizePerTile / mTilemapYSize;
    float cHfX = cSizePerRoomX * 0.50f;
    float cHfY = cSizePerRoomY * 0.50f;
    float cDepthFade = -0.000004f;

    //--Case to skip to just insets.
    if(pBorderCode == AM_FADE_NONE)
    {

    }
    //--Full overlay.
    else if(pBorderCode == AM_FADE_OVER)
    {
        //--Texcoords.
        float cTxL = mLineShadeX + (11.0f / mTilemapXSize);
        float cTxT = mLineShadeY + ( 2.0f / mTilemapYSize);
        cTxT = 1.0f - cTxT;

        //--Render.
        glTexCoord2f(cTxL, cTxT); glVertex3f(pX - cHfX, pY - cHfY, cDepthFade);
        glTexCoord2f(cTxL, cTxT); glVertex3f(pX + cHfX, pY - cHfY, cDepthFade);
        glTexCoord2f(cTxL, cTxT); glVertex3f(pX + cHfX, pY + cHfY + cExtraEdge, cDepthFade);
        glTexCoord2f(cTxL, cTxT); glVertex3f(pX - cHfX, pY + cHfY + cExtraEdge, cDepthFade);
    }
    //--Black overlay. Used for rooms that are revealed for the first time.
    else if(pBorderCode == AM_FADE_FULLBLACK)
    {
        //--Texcoords.
        float cTxL = mFullblackShadeX;
        float cTxT = mFullblackShadeY;
        float cTxR = cTxL + cTexXPerTile;
        float cTxB = cTxT + cTexYPerTile;
        cTxT = 1.0f - cTxT;
        cTxB = 1.0f - cTxB;
        Inlay(cTxL, cTxT, cTxR, cTxB);

        //--Render.
        glTexCoord2f(cTxL, cTxT); glVertex3f(pX - cHfX, pY - cHfY, cDepthFade);
        glTexCoord2f(cTxR, cTxT); glVertex3f(pX + cHfX, pY - cHfY, cDepthFade);
        glTexCoord2f(cTxR, cTxB); glVertex3f(pX + cHfX, pY + cHfY + cExtraEdge, cDepthFade);
        glTexCoord2f(cTxL, cTxB); glVertex3f(pX - cHfX, pY + cHfY + cExtraEdge, cDepthFade);
    }
    else if(pBorderCode == AM_FADE_LINE_N)
    {
        //--Texcoords.
        float cTxL = mLineShadeX;
        float cTxT = mLineShadeY;
        float cTxR = cTxL + cTexXPerTile;
        float cTxB = cTxT + cTexYPerTile;
        cTxT = 1.0f - cTxT;
        cTxB = 1.0f - cTxB;
        Inlay(cTxL, cTxT, cTxR, cTxB);

        //--Render.
        glTexCoord2f(cTxL, cTxT); glVertex3f(pX - cHfX, pY - cHfY, cDepthFade);
        glTexCoord2f(cTxR, cTxT); glVertex3f(pX + cHfX, pY - cHfY, cDepthFade);
        glTexCoord2f(cTxR, cTxB); glVertex3f(pX + cHfX, pY + cHfY + cExtraEdge, cDepthFade);
        glTexCoord2f(cTxL, cTxB); glVertex3f(pX - cHfX, pY + cHfY + cExtraEdge, cDepthFade);
    }
    else if(pBorderCode == AM_FADE_LINE_E)
    {
        //--Texcoords.
        float cTxL = mLineShadeX;
        float cTxT = mLineShadeY;
        float cTxR = cTxL + cTexXPerTile;
        float cTxB = cTxT + cTexYPerTile;
        cTxT = 1.0f - cTxT;
        cTxB = 1.0f - cTxB;
        Inlay(cTxL, cTxT, cTxR, cTxB);

        //--Render.
        glTexCoord2f(cTxL, cTxB); glVertex3f(pX - cHfX, pY + cHfY + cExtraEdge, cDepthFade);
        glTexCoord2f(cTxR, cTxB); glVertex3f(pX - cHfX, pY - cHfY, cDepthFade);
        glTexCoord2f(cTxR, cTxT); glVertex3f(pX + cHfX, pY - cHfY, cDepthFade);
        glTexCoord2f(cTxL, cTxT); glVertex3f(pX + cHfX, pY + cHfY + cExtraEdge, cDepthFade);
    }
    else if(pBorderCode == AM_FADE_LINE_S)
    {
        //--Texcoords.
        float cTxL = mLineShadeX;
        float cTxT = mLineShadeY;
        float cTxR = cTxL + cTexXPerTile;
        float cTxB = cTxT + cTexYPerTile;
        cTxT = 1.0f - cTxT;
        cTxB = 1.0f - cTxB;
        Inlay(cTxL, cTxT, cTxR, cTxB);

        //--Render.
        glTexCoord2f(cTxL, cTxT); glVertex3f(pX - cHfX, pY + cHfY + cExtraEdge, cDepthFade);
        glTexCoord2f(cTxR, cTxT); glVertex3f(pX + cHfX, pY + cHfY + cExtraEdge, cDepthFade);
        glTexCoord2f(cTxR, cTxB); glVertex3f(pX + cHfX, pY - cHfY, cDepthFade);
        glTexCoord2f(cTxL, cTxB); glVertex3f(pX - cHfX, pY - cHfY, cDepthFade);
    }
    else if(pBorderCode == AM_FADE_LINE_W)
    {
        //--Texcoords.
        float cTxL = mLineShadeX;
        float cTxT = mLineShadeY;
        float cTxR = cTxL + cTexXPerTile;
        float cTxB = cTxT + cTexYPerTile;
        cTxT = 1.0f - cTxT;
        cTxB = 1.0f - cTxB;
        Inlay(cTxL, cTxT, cTxR, cTxB);

        //--Render.
        glTexCoord2f(cTxL, cTxT); glVertex3f(pX - cHfX, pY + cHfY + cExtraEdge, cDepthFade);
        glTexCoord2f(cTxR, cTxT); glVertex3f(pX - cHfX, pY - cHfY, cDepthFade);
        glTexCoord2f(cTxR, cTxB); glVertex3f(pX + cHfX, pY - cHfY, cDepthFade);
        glTexCoord2f(cTxL, cTxB); glVertex3f(pX + cHfX, pY + cHfY + cExtraEdge, cDepthFade);
    }
    else if(pBorderCode == AM_FADE_CORN_NW)
    {
        float cTxL = mLineCornerShadeX;
        float cTxT = mLineCornerShadeY;
        float cTxR = cTxL + cTexXPerTile;
        float cTxB = cTxT + cTexYPerTile;
        cTxT = 1.0f - cTxT;
        cTxB = 1.0f - cTxB;
        Inlay(cTxL, cTxT, cTxR, cTxB);

        //--Render.
        glTexCoord2f(cTxL, cTxT); glVertex3f(pX - cHfX, pY - cHfY, cDepthFade);
        glTexCoord2f(cTxR, cTxT); glVertex3f(pX + cHfX, pY - cHfY, cDepthFade);
        glTexCoord2f(cTxR, cTxB); glVertex3f(pX + cHfX, pY + cHfY + cExtraEdge, cDepthFade);
        glTexCoord2f(cTxL, cTxB); glVertex3f(pX - cHfX, pY + cHfY + cExtraEdge, cDepthFade);
    }
    else if(pBorderCode == AM_FADE_CORN_NE)
    {
        float cTxL = mLineCornerShadeX;
        float cTxT = mLineCornerShadeY;
        float cTxR = cTxL + cTexXPerTile;
        float cTxB = cTxT + cTexYPerTile;
        cTxT = 1.0f - cTxT;
        cTxB = 1.0f - cTxB;
        Inlay(cTxL, cTxT, cTxR, cTxB);

        //--Render.
        glTexCoord2f(cTxR, cTxT); glVertex3f(pX - cHfX, pY - cHfY, cDepthFade);
        glTexCoord2f(cTxL, cTxT); glVertex3f(pX + cHfX, pY - cHfY, cDepthFade);
        glTexCoord2f(cTxL, cTxB); glVertex3f(pX + cHfX, pY + cHfY + cExtraEdge, cDepthFade);
        glTexCoord2f(cTxR, cTxB); glVertex3f(pX - cHfX, pY + cHfY + cExtraEdge, cDepthFade);
    }
    else if(pBorderCode == AM_FADE_CORN_SE)
    {
        float cTxL = mLineCornerShadeX;
        float cTxT = mLineCornerShadeY;
        float cTxR = cTxL + cTexXPerTile;
        float cTxB = cTxT + cTexYPerTile;
        cTxT = 1.0f - cTxT;
        cTxB = 1.0f - cTxB;
        Inlay(cTxL, cTxT, cTxR, cTxB);

        //--Render.
        glTexCoord2f(cTxR, cTxB); glVertex3f(pX - cHfX, pY - cHfY, cDepthFade);
        glTexCoord2f(cTxL, cTxB); glVertex3f(pX + cHfX, pY - cHfY, cDepthFade);
        glTexCoord2f(cTxL, cTxT); glVertex3f(pX + cHfX, pY + cHfY + cExtraEdge, cDepthFade);
        glTexCoord2f(cTxR, cTxT); glVertex3f(pX - cHfX, pY + cHfY + cExtraEdge, cDepthFade);
    }
    else if(pBorderCode == AM_FADE_CORN_SW)
    {
        float cTxL = mLineCornerShadeX;
        float cTxT = mLineCornerShadeY;
        float cTxR = cTxL + cTexXPerTile;
        float cTxB = cTxT + cTexYPerTile;
        cTxT = 1.0f - cTxT;
        cTxB = 1.0f - cTxB;
        Inlay(cTxL, cTxT, cTxR, cTxB);

        //--Render.
        glTexCoord2f(cTxL, cTxB); glVertex3f(pX - cHfX, pY - cHfY, cDepthFade);
        glTexCoord2f(cTxR, cTxB); glVertex3f(pX + cHfX, pY - cHfY, cDepthFade);
        glTexCoord2f(cTxR, cTxT); glVertex3f(pX + cHfX, pY + cHfY + cExtraEdge, cDepthFade);
        glTexCoord2f(cTxL, cTxT); glVertex3f(pX - cHfX, pY + cHfY + cExtraEdge, cDepthFade);
    }
    else if(pBorderCode == AM_FADE_TWOLINE_V)
    {
        //--Texcoords.
        float cTxL = mLineShadeX;
        float cTxT = mLineShadeY;
        float cTxR = cTxL + cTexXPerTile;
        float cTxB = cTxT + cTexYPerTile;
        cTxT = 1.0f - cTxT;
        cTxB = 1.0f - cTxB;
        Inlay(cTxL, cTxT, cTxR, cTxB);

        //--Render.
        glTexCoord2f(cTxL, cTxB); glVertex3f(pX - cHfX, pY + cHfY + cExtraEdge, cDepthFade);
        glTexCoord2f(cTxR, cTxB); glVertex3f(pX - cHfX, pY - cHfY, cDepthFade);
        glTexCoord2f(cTxR, cTxT); glVertex3f(pX + cHfX, pY - cHfY, cDepthFade);
        glTexCoord2f(cTxL, cTxT); glVertex3f(pX + cHfX, pY + cHfY + cExtraEdge, cDepthFade);

        //--Render.
        glTexCoord2f(cTxL, cTxT); glVertex3f(pX - cHfX, pY + cHfY, cDepthFade);
        glTexCoord2f(cTxR, cTxT); glVertex3f(pX - cHfX, pY - cHfY, cDepthFade);
        glTexCoord2f(cTxR, cTxB); glVertex3f(pX + cHfX, pY - cHfY, cDepthFade);
        glTexCoord2f(cTxL, cTxB); glVertex3f(pX + cHfX, pY + cHfY, cDepthFade);
    }
    else if(pBorderCode == AM_FADE_TWOLINE_H)
    {
        //--Texcoords.
        float cTxL = mLineShadeX;
        float cTxT = mLineShadeY;
        float cTxR = cTxL + cTexXPerTile;
        float cTxB = cTxT + cTexYPerTile;
        cTxT = 1.0f - cTxT;
        cTxB = 1.0f - cTxB;
        Inlay(cTxL, cTxT, cTxR, cTxB);

        //--Render.
        glTexCoord2f(cTxL, cTxT); glVertex3f(pX - cHfX, pY - cHfY, cDepthFade);
        glTexCoord2f(cTxR, cTxT); glVertex3f(pX + cHfX, pY - cHfY, cDepthFade);
        glTexCoord2f(cTxR, cTxB); glVertex3f(pX + cHfX, pY + cHfY + cExtraEdge, cDepthFade);
        glTexCoord2f(cTxL, cTxB); glVertex3f(pX - cHfX, pY + cHfY + cExtraEdge, cDepthFade);

        //--Render.
        glTexCoord2f(cTxL, cTxT); glVertex3f(pX - cHfX, pY + cHfY + cExtraEdge, cDepthFade);
        glTexCoord2f(cTxR, cTxT); glVertex3f(pX + cHfX, pY + cHfY + cExtraEdge, cDepthFade);
        glTexCoord2f(cTxR, cTxB); glVertex3f(pX + cHfX, pY - cHfY, cDepthFade);
        glTexCoord2f(cTxL, cTxB); glVertex3f(pX - cHfX, pY - cHfY, cDepthFade);
    }
    else if(pBorderCode == AM_FADE_ALLEY_N)
    {
        //--Texcoords.
        float cTxL = mLineAlleyShadeX;
        float cTxT = mLineAlleyShadeY;
        float cTxR = cTxL + cTexXPerTile;
        float cTxB = cTxT + cTexYPerTile;
        cTxT = 1.0f - cTxT;
        cTxB = 1.0f - cTxB;
        Inlay(cTxL, cTxT, cTxR, cTxB);

        //--Render.
        glTexCoord2f(cTxL, cTxT); glVertex3f(pX - cHfX, pY - cHfY, cDepthFade);
        glTexCoord2f(cTxR, cTxT); glVertex3f(pX + cHfX, pY - cHfY, cDepthFade);
        glTexCoord2f(cTxR, cTxB); glVertex3f(pX + cHfX, pY + cHfY + cExtraEdge, cDepthFade);
        glTexCoord2f(cTxL, cTxB); glVertex3f(pX - cHfX, pY + cHfY + cExtraEdge, cDepthFade);
    }
    else if(pBorderCode == AM_FADE_ALLEY_E)
    {
        //--Texcoords.
        float cTxL = mLineAlleyShadeX;
        float cTxT = mLineAlleyShadeY;
        float cTxR = cTxL + cTexXPerTile;
        float cTxB = cTxT + cTexYPerTile;
        cTxT = 1.0f - cTxT;
        cTxB = 1.0f - cTxB;
        Inlay(cTxL, cTxT, cTxR, cTxB);

        //--Render.
        glTexCoord2f(cTxL, cTxB); glVertex3f(pX - cHfX, pY + cHfY + cExtraEdge, cDepthFade);
        glTexCoord2f(cTxR, cTxB); glVertex3f(pX - cHfX, pY - cHfY, cDepthFade);
        glTexCoord2f(cTxR, cTxT); glVertex3f(pX + cHfX, pY - cHfY, cDepthFade);
        glTexCoord2f(cTxL, cTxT); glVertex3f(pX + cHfX, pY + cHfY + cExtraEdge, cDepthFade);
    }
    else if(pBorderCode == AM_FADE_ALLEY_S)
    {
        //--Texcoords.
        float cTxL = mLineAlleyShadeX;
        float cTxT = mLineAlleyShadeY;
        float cTxR = cTxL + cTexXPerTile;
        float cTxB = cTxT + cTexYPerTile;
        cTxT = 1.0f - cTxT;
        cTxB = 1.0f - cTxB;
        Inlay(cTxL, cTxT, cTxR, cTxB);

        //--Render.
        glTexCoord2f(cTxL, cTxT); glVertex3f(pX - cHfX, pY + cHfY + cExtraEdge, cDepthFade);
        glTexCoord2f(cTxR, cTxT); glVertex3f(pX + cHfX, pY + cHfY + cExtraEdge, cDepthFade);
        glTexCoord2f(cTxR, cTxB); glVertex3f(pX + cHfX, pY - cHfY, cDepthFade);
        glTexCoord2f(cTxL, cTxB); glVertex3f(pX - cHfX, pY - cHfY, cDepthFade);
    }
    else if(pBorderCode == AM_FADE_ALLEY_W)
    {
        //--Texcoords.
        float cTxL = mLineAlleyShadeX;
        float cTxT = mLineAlleyShadeY;
        float cTxR = cTxL + cTexXPerTile;
        float cTxB = cTxT + cTexYPerTile;
        cTxT = 1.0f - cTxT;
        cTxB = 1.0f - cTxB;
        Inlay(cTxL, cTxT, cTxR, cTxB);

        //--Render.
        glTexCoord2f(cTxL, cTxT); glVertex3f(pX - cHfX, pY + cHfY + cExtraEdge, cDepthFade);
        glTexCoord2f(cTxR, cTxT); glVertex3f(pX - cHfX, pY - cHfY, cDepthFade);
        glTexCoord2f(cTxR, cTxB); glVertex3f(pX + cHfX, pY - cHfY, cDepthFade);
        glTexCoord2f(cTxL, cTxB); glVertex3f(pX + cHfX, pY + cHfY + cExtraEdge, cDepthFade);
    }
    else if(pBorderCode == AM_FADE_FULL)
    {
        float cTxL = mLineBoxShadeX;
        float cTxT = mLineBoxShadeY;
        float cTxR = cTxL + cTexXPerTile;
        float cTxB = cTxT + cTexYPerTile;
        cTxT = 1.0f - cTxT;
        cTxB = 1.0f - cTxB;
        Inlay(cTxL, cTxT, cTxR, cTxB);

        //--Render.
        glTexCoord2f(cTxL, cTxT); glVertex3f(pX - cHfX, pY - cHfY, cDepthFade);
        glTexCoord2f(cTxR, cTxT); glVertex3f(pX + cHfX, pY - cHfY, cDepthFade);
        glTexCoord2f(cTxR, cTxB); glVertex3f(pX + cHfX, pY + cHfY + cExtraEdge, cDepthFade);
        glTexCoord2f(cTxL, cTxB); glVertex3f(pX - cHfX, pY + cHfY + cExtraEdge, cDepthFade);
    }

    //--Insets. All four can render at once!
    if(pInsetCode == AM_FADE_INSET_NONE) return;
    StarlightColor::SetMixer(1.0f, 1.0f, 1.0f, pInsetAlpha);

    //--Texcoords.
    float cTxL = mLineInsetShadeX;
    float cTxT = mLineInsetShadeY;
    float cTxR = cTxL + cTexXPerTile;
    float cTxB = cTxT + cTexYPerTile;
    cTxT = 1.0f - cTxT;
    cTxB = 1.0f - cTxB;
    Inlay(cTxL, cTxT, cTxR, cTxB);

    //--NW.
    if(pInsetCode & AM_FADE_INSET_NW)
    {
        glTexCoord2f(cTxL, cTxT); glVertex3f(pX - cHfX, pY - cHfY, cDepthFade);
        glTexCoord2f(cTxR, cTxT); glVertex3f(pX + cHfX, pY - cHfY, cDepthFade);
        glTexCoord2f(cTxR, cTxB); glVertex3f(pX + cHfX, pY + cHfY + cExtraEdge, cDepthFade);
        glTexCoord2f(cTxL, cTxB); glVertex3f(pX - cHfX, pY + cHfY + cExtraEdge, cDepthFade);
    }
    //--NE.
    if(pInsetCode & AM_FADE_INSET_NE)
    {
        glTexCoord2f(cTxR, cTxT); glVertex3f(pX - cHfX, pY - cHfY, cDepthFade);
        glTexCoord2f(cTxL, cTxT); glVertex3f(pX + cHfX, pY - cHfY, cDepthFade);
        glTexCoord2f(cTxL, cTxB); glVertex3f(pX + cHfX, pY + cHfY + cExtraEdge, cDepthFade);
        glTexCoord2f(cTxR, cTxB); glVertex3f(pX - cHfX, pY + cHfY + cExtraEdge, cDepthFade);
    }
    //--SE.
    if(pInsetCode & AM_FADE_INSET_SE)
    {
        glTexCoord2f(cTxR, cTxB); glVertex3f(pX - cHfX, pY - cHfY, cDepthFade);
        glTexCoord2f(cTxL, cTxB); glVertex3f(pX + cHfX, pY - cHfY, cDepthFade);
        glTexCoord2f(cTxL, cTxT); glVertex3f(pX + cHfX, pY + cHfY + cExtraEdge, cDepthFade);
        glTexCoord2f(cTxR, cTxT); glVertex3f(pX - cHfX, pY + cHfY + cExtraEdge, cDepthFade);
    }
    //--SW.
    if(pInsetCode & AM_FADE_INSET_SW)
    {
        glTexCoord2f(cTxL, cTxB); glVertex3f(pX - cHfX, pY - cHfY, cDepthFade);
        glTexCoord2f(cTxR, cTxB); glVertex3f(pX + cHfX, pY - cHfY, cDepthFade);
        glTexCoord2f(cTxR, cTxT); glVertex3f(pX + cHfX, pY + cHfY + cExtraEdge, cDepthFade);
        glTexCoord2f(cTxL, cTxT); glVertex3f(pX - cHfX, pY + cHfY + cExtraEdge, cDepthFade);
    }
}
