//--[ALGTextureData]
//--Texture data used by the AdventureLevelGenerator. This .h file is included directly in the
//  AdventureLevelGenerator.h and does not need to be included in .cc files.

#pragma once

//--[Definitions]
//--Wall Pattern
#define WALL_HIGH_EW 0
#define WALL_HIGH_NS 1
#define WALL_HIGH_CORNER_ULN 2
#define WALL_HIGH_CORNER_ULS 3
#define WALL_HIGH_CORNER_URN 4
#define WALL_HIGH_CORNER_URS 5
#define WALL_HIGH_JUNCTION_LN 6
#define WALL_HIGH_JUNCTION_LS 7
#define WALL_HIGH_JUNCTION_RN 8
#define WALL_HIGH_JUNCTION_RS 9
#define WALL_HIGH_JUNCTION_MN 10
#define WALL_HIGH_JUNCTION_MS 11
#define WALL_HIGH_CORNER_LL 12
#define WALL_HIGH_CORNER_LR 13
#define WALL_HIGH_UV_TOTAL 14

//--Blackout Pattern
#define BLACKOUT_FULL 0
#define BLACKOUT_CORNERLL 1
#define BLACKOUT_LOWMIDDLE 2
#define BLACKOUT_CORNERLR 3
#define BLACKOUT_CORNERIL 4
#define BLACKOUT_CORNERIR 5
#define BLACKOUT_LEFT 6
#define BLACKOUT_RIGHT 7
#define BLACKOUT_TOTAL 8

//--Cliff Pattern
#define CLIFF_TOPLFT 0
#define CLIFF_TOPMID 1
#define CLIFF_TOPRGT 2
#define CLIFF_LFTMID 3
#define CLIFF_RGTMID 4
#define CLIFF_BOTLFT 5
#define CLIFF_BOTMID 6
#define CLIFF_BOTRGT 7
#define CLIFF_CORNUL 8
#define CLIFF_CORNUR 9
#define CLIFF_CORNBL 10
#define CLIFF_CORNBR 11
#define CLIFF_TRITOP 12
#define CLIFF_TRIRGT 13
#define CLIFF_TRIBOT 14
#define CLIFF_TRILFT 15
#define CLIFF_ULUR 16
#define CLIFF_BLBR 17
#define CLIFF_ALL 18
#define CLIFF_TOTAL 19

//--[Class]
typedef struct ALGTextureData
{
    //--Floors. Multiple floor tiles are legal.
    int mFloorsTotal;
    TwoDimensionReal *mFloorUVs;

    //--Wall pattern. Uses a standard format.
    float mPatternXStart;
    float mPatternYStart;
    TwoDimensionReal mErrorUV;
    TwoDimensionReal mWallUV;
    TwoDimensionReal mUVList[WALL_HIGH_UV_TOTAL];
    TwoDimensionReal mBlackoutList[BLACKOUT_TOTAL];
    TwoDimensionReal mCliffUV[CLIFF_TOTAL];

    //--Other UV properties
    TwoDimensionReal mLadderDnUV[3];
    TwoDimensionReal mLadderUpUV[3];
    TwoDimensionReal mFloorEdgeUV[8];
    TwoDimensionReal mTreasureUV;
    TwoDimensionReal mEnemyUV[3];

    //--Floor Doodads
    int mFloorDoodadsTotal;
    float *mFloorDoodadX;
    float *mFloorDoodadY;

    //--Properties.
    float mWidPerTile;
    float mHeiPerTile;

    //--Function.
    void Initialize()
    {
        //--Floors
        mFloorsTotal = 0;
        mFloorUVs = NULL;

        //--Wall Pattern
        mPatternXStart = 0.0f;
        mPatternYStart = 0.0f;
        mErrorUV.Set(0.0f, 0.0f, 1.0f, 1.0f);
        mWallUV.Set(0.0f, 0.0f, 1.0f, 1.0f);
        memset(mUVList,       0, sizeof(TwoDimensionReal) * WALL_HIGH_UV_TOTAL);
        memset(mBlackoutList, 0, sizeof(TwoDimensionReal) * BLACKOUT_TOTAL);
        memset(mCliffUV,      0, sizeof(TwoDimensionReal) * CLIFF_TOTAL);

        //--UV Properties
        memset(mLadderDnUV,  0, sizeof(TwoDimensionReal) * 3);
        memset(mLadderUpUV,  0, sizeof(TwoDimensionReal) * 3);
        memset(mFloorEdgeUV, 0, sizeof(TwoDimensionReal) * 8);
        memset(&mTreasureUV, 0, sizeof(TwoDimensionReal) * 1);
        memset(mEnemyUV,     0, sizeof(TwoDimensionReal) * 3);

        //--Floor Doodads
        mFloorDoodadsTotal = 0;
        mFloorDoodadX = NULL;
        mFloorDoodadY = NULL;

        //--Properties
        mWidPerTile = 0.0f;
        mHeiPerTile = 0.0f;
    }
}ALGTextureData;
