//--Base
#include "AdventureLevelGenerator.h"

//--Classes
#include "ALGTile.h"
#include "ALGRoom.h"

//--CoreClasses
#include "SugarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"

//--Libraries
//--Managers

//--[Entry Points]
void AdventureLevelGenerator::MarkTilesByRange(int pX, int pY, uint8_t pSpecialFlag)
{
    //--Run a pather algorithm. While it's not the fastest algorithm, it has a 100% success rate.
    //  Any room not connected to this one needs to be tunneled to.
    //--Tiles are marked based on the pulse count, and store their distance. Any tiles that do not
    //  have the current pulse value after the function completes are disconnected from the main
    //  room set, and need to be tunneled to.
    SugarLinkedList *tPulseList = new SugarLinkedList(true);

    //--Increment the pulse counter.
    mPulse ++;
    mLongestDistanceLastPulse = 0;

    //--Get the X and Y positions of the room.
    if(pX < 0 || pX >= mXSize) return;
    if(pY < 0 || pY >= mYSize) return;

    //--Create the first point.
    SetMemoryData(__FILE__, __LINE__);
    IntPoint2D *nPulsePoint = (IntPoint2D *)starmemoryalloc(sizeof(IntPoint2D));
    nPulsePoint->mX = pX;
    nPulsePoint->mY = pY;
    nPulsePoint->mDistance = 1;
    tPulseList->AddElement("X", nPulsePoint, &FreeThis);

    //--Mark the zeroth room as the pulse start.
    mMapData[pX][pY]->mPulseCheck = mPulse;
    mMapData[pX][pY]->mPulseDistance = 0;

    //--Iterate until all pulses are extinguished.
    RangeIterator(tPulseList, pSpecialFlag);

    //--Delete the pulse list.
    delete tPulseList;
}
void AdventureLevelGenerator::MarkTilesByRange(int pXA, int pYA, int pXB, int pYB, uint8_t pSpecialFlag)
{
    //--Same as above, except uses two starting points. This is used when distance from entrance and exit
    //  are what matter, otherwise it's identical to the base routine.
    SugarLinkedList *tPulseList = new SugarLinkedList(true);

    //--Increment the pulse counter.
    mPulse ++;
    mLongestDistanceLastPulse = 0;

    //--Get the X and Y positions of the room.
    if(pXA < 0 || pXA >= mXSize) return;
    if(pYA < 0 || pYA >= mYSize) return;
    if(pXB < 0 || pXB >= mXSize) return;
    if(pYB < 0 || pYB >= mYSize) return;

    //--Create the first point.
    SetMemoryData(__FILE__, __LINE__);
    IntPoint2D *nPulsePoint = (IntPoint2D *)starmemoryalloc(sizeof(IntPoint2D));
    nPulsePoint->mX = pXA;
    nPulsePoint->mY = pYA;
    nPulsePoint->mDistance = 1;
    tPulseList->AddElement("X", nPulsePoint, &FreeThis);

    //--Mark the zeroth room as the pulse start.
    mMapData[pXA][pYA]->mPulseCheck = mPulse;
    mMapData[pXA][pYA]->mPulseDistance = 0;

    //--Create the second point.
    SetMemoryData(__FILE__, __LINE__);
    nPulsePoint = (IntPoint2D *)starmemoryalloc(sizeof(IntPoint2D));
    nPulsePoint->mX = pXB;
    nPulsePoint->mY = pYB;
    nPulsePoint->mDistance = 1;
    tPulseList->AddElement("X", nPulsePoint, &FreeThis);

    //--Mark this as zero distance as well.
    mMapData[pXB][pYB]->mPulseCheck = mPulse;
    mMapData[pXB][pYB]->mPulseDistance = 0;

    //--Iterate until all pulses are extinguished.
    RangeIterator(tPulseList, pSpecialFlag);

    //--Delete the pulse list.
    delete tPulseList;
}
void AdventureLevelGenerator::MarkTilesByRange(SugarLinkedList *pList, uint8_t pSpecialFlag)
{
    //--As above, except can use any arbitrary number of points. Expects the list to contain IntPoint2D types,
    //  and will leave the original list untouched. It will clone the points off.
    if(!pList || pList->GetListSize() < 1) return;
    SugarLinkedList *tPulseList = new SugarLinkedList(true);

    //--Increment the pulse counter.
    mPulse ++;
    mLongestDistanceLastPulse = 0;

    //--Begin creating points.
    IntPoint2D *rOrigPoint = (IntPoint2D *)pList->PushIterator();
    while(rOrigPoint)
    {
        //--Confirm point legality.
        if(rOrigPoint->mX < 0 || rOrigPoint->mX >= mXSize || rOrigPoint->mY < 0 || rOrigPoint->mY >= mYSize)
        {
            rOrigPoint = (IntPoint2D *)pList->AutoIterate();
            continue;
        }

        //--Create.
        SetMemoryData(__FILE__, __LINE__);
        IntPoint2D *nPulsePoint = (IntPoint2D *)starmemoryalloc(sizeof(IntPoint2D));
        nPulsePoint->mX = rOrigPoint->mX;
        nPulsePoint->mY = rOrigPoint->mY;
        nPulsePoint->mDistance = 1;
        tPulseList->AddElement("X", nPulsePoint, &FreeThis);

        //--Mark the zeroth room as the pulse start.
        mMapData[rOrigPoint->mX][rOrigPoint->mY]->mPulseCheck = mPulse;
        mMapData[rOrigPoint->mX][rOrigPoint->mY]->mPulseDistance = 0;

        //--Next.
        rOrigPoint = (IntPoint2D *)pList->AutoIterate();
    }

    //--Iterate until all pulses are extinguished.
    RangeIterator(tPulseList, pSpecialFlag);

    //--Delete the pulse list.
    delete tPulseList;
}

//--[Workers]
void AdventureLevelGenerator::RangeIterator(SugarLinkedList *pIteratorList, uint8_t pSpecialFlag)
{
    //--Iterates during the range-finding algorithm. Does not deallocate the list when done.
    if(!pIteratorList) return;

    //--Iterate until all pulses are extinguished.
    IntPoint2D *rCurrentPosition = (IntPoint2D *)pIteratorList->SetToHeadAndReturn();
    while(rCurrentPosition)
    {
        //--Fast-access variables.
        int cX = rCurrentPosition->mX;
        int cY = rCurrentPosition->mY;

        //--Stop on clipped positions if not specially flagged:
        if(!(pSpecialFlag & ALLOW_PITS))
        {
            if(!mMapData[cX][cY]->IsFloorTile())
            {
                pIteratorList->DeleteHead();
                rCurrentPosition = (IntPoint2D *)pIteratorList->SetToHeadAndReturn();
                continue;
            }
        }
        //--Allow pits when checking for collisions:
        else
        {
            if(!mMapData[cX][cY]->IsFloorTile() && !mMapData[cX][cY]->IsPitTile())
            {
                pIteratorList->DeleteHead();
                rCurrentPosition = (IntPoint2D *)pIteratorList->SetToHeadAndReturn();
                continue;
            }
        }

        //--Check if this is greater than the previous max distance.
        if(rCurrentPosition->mDistance > mLongestDistanceLastPulse) mLongestDistanceLastPulse = rCurrentPosition->mDistance;

        //--North check:
        if(rCurrentPosition->mY > 0 && mMapData[cX][cY-1]->mPulseCheck < mPulse)
        {
            //--Mark the map.
            mMapData[cX][cY-1]->mPulseCheck = mPulse;
            mMapData[cX][cY-1]->mPulseDistance = rCurrentPosition->mDistance + 1;

            //--Generate a new point.
            SetMemoryData(__FILE__, __LINE__);
            IntPoint2D *nPulsePoint = (IntPoint2D *)starmemoryalloc(sizeof(IntPoint2D));
            nPulsePoint->mX = cX;
            nPulsePoint->mY = cY-1;
            nPulsePoint->mDistance = rCurrentPosition->mDistance + 1;
            pIteratorList->AddElementAsTail("X", nPulsePoint, &FreeThis);
        }

        //--East check:
        if(rCurrentPosition->mX < mXSize - 1 && mMapData[cX+1][cY]->mPulseCheck < mPulse)
        {
            //--Mark the map.
            mMapData[cX+1][cY]->mPulseCheck = mPulse;
            mMapData[cX+1][cY]->mPulseDistance = rCurrentPosition->mDistance + 1;

            //--Generate a new point.
            SetMemoryData(__FILE__, __LINE__);
            IntPoint2D *nPulsePoint = (IntPoint2D *)starmemoryalloc(sizeof(IntPoint2D));
            nPulsePoint->mX = cX+1;
            nPulsePoint->mY = cY;
            nPulsePoint->mDistance = rCurrentPosition->mDistance + 1;
            pIteratorList->AddElementAsTail("X", nPulsePoint, &FreeThis);
        }

        //--South check:
        if(rCurrentPosition->mY < mYSize - 1 && mMapData[cX][cY+1]->mPulseCheck < mPulse)
        {
            //--Mark the map.
            mMapData[cX][cY+1]->mPulseCheck = mPulse;
            mMapData[cX][cY+1]->mPulseDistance = rCurrentPosition->mDistance + 1;

            //--Generate a new point.
            SetMemoryData(__FILE__, __LINE__);
            IntPoint2D *nPulsePoint = (IntPoint2D *)starmemoryalloc(sizeof(IntPoint2D));
            nPulsePoint->mX = cX;
            nPulsePoint->mY = cY+1;
            nPulsePoint->mDistance = rCurrentPosition->mDistance + 1;
            pIteratorList->AddElementAsTail("X", nPulsePoint, &FreeThis);
        }

        //--West check:
        if(rCurrentPosition->mX > 0 && mMapData[cX-1][cY]->mPulseCheck < mPulse)
        {
            //--Mark the map.
            mMapData[cX-1][cY]->mPulseCheck = mPulse;
            mMapData[cX-1][cY]->mPulseDistance = rCurrentPosition->mDistance + 1;

            //--Generate a new point.
            SetMemoryData(__FILE__, __LINE__);
            IntPoint2D *nPulsePoint = (IntPoint2D *)starmemoryalloc(sizeof(IntPoint2D));
            nPulsePoint->mX = cX-1;
            nPulsePoint->mY = cY;
            nPulsePoint->mDistance = rCurrentPosition->mDistance + 1;
            pIteratorList->AddElementAsTail("X", nPulsePoint, &FreeThis);
        }

        //--Remove the 0th element.
        pIteratorList->DeleteHead();
        rCurrentPosition = (IntPoint2D *)pIteratorList->SetToHeadAndReturn();
    }
}

//--[Queries]
bool AdventureLevelGenerator::AreAnyTilesDisconnected()
{
    //--Check across all rooms. If the center tile of a room is not pulsed, it's disconnected.
    for(int i = 0; i < mRoomsTotal; i ++)
    {
        //--Fast-access variables.
        int cX = mRooms[i]->mCenterXA;
        int cY = mRooms[i]->mCenterYA;
        if(mMapData[cX][cY]->mPulseCheck < mPulse) return true;
    }

    //--All rooms are connected.
    return false;
}
