//--Base
#include "PandemoniumLevel.h"

//--Classes
#include "Actor.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarFont.h"
#include "SugarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"
#include "EasingFunctions.h"

//--GUI
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"

//--When an Actor takes damage within sight of the player, or at least gets attacked in some fashion,
//  these functions will animate that behavior.
//--This only occurs if xAnimateDamage is true.

//--[Property Queries]
bool PandemoniumLevel::IsDamageAnimating()
{
    return mIsDamageAnimating;
}

//--[Manipulators]
void PandemoniumLevel::BeginDamageAnimation(Actor *pTarget, int pHPLoss, int pWPLoss)
{
    //--Starts animating damage and stores the values. Auto-fails if there's no target, or if the static flag is false.
    if(!pTarget || !xAnimateDamage) return;

    //--Flags.
    mIsDamageAnimating = true;

    //--Create a new package.
    SetMemoryData(__FILE__, __LINE__);
    DamageAnimPack *nPack = (DamageAnimPack *)starmemoryalloc(sizeof(DamageAnimPack));
    nPack->Clear();

    //--Set variables.
    nPack->mAnimatePoof = false;
    nPack->mDamageTimer = 0;
    nPack->mDamageHP = pHPLoss;
    nPack->mDamageWP = pWPLoss;
    nPack->rTargetRefPtr = pTarget;

    //--Register the package.
    mDamageAnimationQueue->AddElementAsTail("X", nPack, &FreeThis);

    //--Resolve which bitmap to use from the Actor.
    nPack->rDamageImage = pTarget->GetActiveImage();

    //--If this attack would fell the target, animate a poof if it doesn't get stunned upon downing.
    if(!pTarget->PersistsThroughKnockout() && (pTarget->GetCombatStatistics().mHP < 1 || pTarget->GetCombatStatistics().mWillPower < 1))
    {
        nPack->mAnimatePoof = true;
    }
}

//--[Update]
void PandemoniumLevel::UpdateDamageAnimation()
{
    //--Get the 0th pack. Do nothing if there are none.
    DamageAnimPack *rPack = (DamageAnimPack *)mDamageAnimationQueue->GetElementBySlot(0);
    if(!rPack)
    {
        mIsDamageAnimating = false;
        return;
    }

    //--Zeroth tick: Play sound effect.
    if(rPack->mDamageTimer == 0)
    {
        //--Both damage types are zero, so the attack missed.
        if(rPack->mDamageHP < 1 && rPack->mDamageWP < 1)
        {
            AudioManager::Fetch()->PlaySound("Combat|AttackMiss");
        }
        //--Non-zero, play this sound.
        else
        {
            AudioManager::Fetch()->PlaySound("Combat|AttackHit");
        }
    }

    //--Increment the timer. All rendering is based on the timer.
    rPack->mDamageTimer ++;

    //--Ending case check.
    if(rPack->mDamageTimer >= PL_DAM_TICKS_TOTAL)
    {
        //--SFX.
        if(rPack->mAnimatePoof && rPack->mDamageTimer == PL_DAM_TICKS_TOTAL)
        {
            AudioManager::Fetch()->PlaySound("Combat|MonsterDie");
        }

        //--Poof adds some extra ticks.
        if(rPack->mAnimatePoof && rPack->mDamageTimer >= PL_DAM_TICKS_TOTAL + PL_DAM_POOF_TICKS)
        {
            mDamageAnimationQueue->RemoveElementI(0);
            mIsDamageAnimating = (mDamageAnimationQueue->GetListSize() > 0);
            return;
        }
        //--Normal case.
        else if(!rPack->mAnimatePoof)
        {
            mDamageAnimationQueue->RemoveElementI(0);
            mIsDamageAnimating = (mDamageAnimationQueue->GetListSize() > 0);
            return;
        }
    }
}

//--[Drawing]
void PandemoniumLevel::RenderDamageAnimation()
{
    //--This replaces rendering of the active portrait over the character pane. It uses the same code base
    //  and expects the same translation.
    DamageAnimPack *rPack = (DamageAnimPack *)mDamageAnimationQueue->GetElementBySlot(0);
    if(!rPack) return;
    if(!rPack->rDamageImage) return;

    //--If the attack did not miss, we sometimes skip out on rendering to make it appear to flash.
    if(rPack->mDamageTimer < PL_DAM_FLASH_TICKS && rPack->mDamageTimer % 4 < 2 && (rPack->mDamageHP > 0 || rPack->mDamageWP > 0))
    {

    }
    //--Rendering case.
    else
    {
        //--Constants.
        float cPorWid = CharPane.cWid;
        float cPorHei = CharPane.cHei - 100.0f;

        //--Sizes
        float cImgWidth = rPack->rDamageImage->GetTrueWidth();
        float cImgHeight = rPack->rDamageImage->GetTrueHeight();

        //--Error check. It's nominally not possible for these to be zero.
        if(cImgWidth == 0.0f) cImgWidth = 1.0f;
        if(cImgHeight == 0.0f) cImgHeight = 1.0f;

        //--Calculate the max amount of stretch available.
        float cHSize = cPorWid - (CharPane.cIndent * 2.0f);
        float cVSize = cPorHei - (CharPane.cIndent * 2.0f);
        float cHMult = cHSize / cImgWidth;
        float cVMult = cVSize / cImgHeight;

        //--Error check.
        if(cHMult == 0.0f || cVMult == 0.0f)
        {

        }
        //--Safe case.
        else
        {
            //--Rendering positions.
            glTranslatef(cPorWid * 0.50f, cPorHei / 2.0f, 0.0f);
            float tRenderX = cImgWidth / -2.0f;
            float tRenderY = cImgHeight / -2.0f;

            //--Render down slightly.
            tRenderY = tRenderY + 25.0f;

            //--If there are more (or equal) HMult, use the VMult.
            if(cHMult > cVMult)
            {
                glScalef(cVMult, cVMult, 1.0f);
                SubRenderDamageAnimation(tRenderX, tRenderY);
                glScalef(1.0f / cVMult, 1.0f / cVMult, 1.0f);
            }
            //--Use the HMult.
            else
            {
                glScalef(cHMult, cHMult, 1.0f);
                SubRenderDamageAnimation(tRenderX, tRenderY);
                glScalef(1.0f / cHMult, 1.0f / cHMult, 1.0f);
            }

            //--Clean.
            glTranslatef(cPorWid * -0.50f, cPorHei / -2.0f, 0.0f);
        }
    }

    //--Damage rendering. We need a font for this.
    SugarFont *rRenderFont = DataLibrary::Fetch()->GetFont("Classic Level Damage");
    if(!rRenderFont) return;

    //--Don't render if in the poof ticks.
    if(rPack->mDamageTimer >= PL_DAM_TICKS_TOTAL) return;

    //--Compute the position offset.
    float cTextScale = 3.0f;
    float tOffset = 130.0f + (EasingFunction::QuadraticInOut(rPack->mDamageTimer, PL_DAM_TICKS_TOTAL) * 60.0f);

    //--If the HP damage is 0 and the WP is -1, this is a "miss".
    if(rPack->mDamageHP == 0 && rPack->mDamageWP < 0)
    {
        //--Miss is rendered in grey.
        glColor3f(0.5f, 0.5f, 0.5f);

        //--Compute the position.
        float tRenderX = (CharPane.cWid * 0.5f)- (rRenderFont->GetTextWidth("Miss!") * 0.5f * cTextScale);
        float tRenderY = (CharPane.cHei * 0.5f) - tOffset;

        //--Render it.
        rRenderFont->DrawText(tRenderX, tRenderY, 0, cTextScale, "Miss!");
    }
    //--If the HP damage is -1 and the WP is 0, this is a "resist".
    else if(rPack->mDamageHP < 0 && rPack->mDamageWP == 0)
    {
        //--Resist is rendered in violet.
        glColor3f(1.0f, 0.4f, 0.9f);

        //--Compute the position.
        float tRenderX = (CharPane.cWid * 0.5f)- (rRenderFont->GetTextWidth("Resist!") * 0.5f * cTextScale);
        float tRenderY = (CharPane.cHei * 0.5f) - tOffset;

        //--Render it.
        rRenderFont->DrawText(tRenderX, tRenderY, 0, cTextScale, "Resist!");
    }
    //--Otherwise, animate whichever damage is above 0. This can be both.
    else
    {
        //--HP damage.
        if(rPack->mDamageHP > 0)
        {
            //--HP damage is red.
            glColor3f(1.0f, 0.0f, 0.0f);

            //--Buffer the damage.
            char tBuffer[16];
            sprintf(tBuffer, "%i", rPack->mDamageHP);

            //--Compute the position.
            float tRenderX = (CharPane.cWid * 0.5f)- (rRenderFont->GetTextWidth(tBuffer) * 0.5f * cTextScale);
            float tRenderY = (CharPane.cHei * 0.5f) - tOffset;

            //--If WP damage was dealt, the X render moves to the left.
            if(rPack->mDamageWP > 0) tRenderX = tRenderX - tOffset;

            //--Render it.
            rRenderFont->DrawText(tRenderX, tRenderY, 0, cTextScale, tBuffer);
        }

        //--WP damage. Not exclusive with HP damage!
        if(rPack->mDamageWP > 0)
        {
            //--HP damage is violet.
            glColor3f(0.8f, 0.1f, 1.0f);

            //--Buffer the damage.
            char tBuffer[16];
            sprintf(tBuffer, "%i", rPack->mDamageWP);

            //--Compute the position.
            float tRenderX = (CharPane.cWid * 0.5f)- (rRenderFont->GetTextWidth(tBuffer) * 0.5f * cTextScale);
            float tRenderY = (CharPane.cHei * 0.5f) - tOffset;

            //--If HP damage was dealt, the X render moves to the right.
            if(rPack->mDamageHP > 0) tRenderX = tRenderX + tOffset;

            //--Render it.
            rRenderFont->DrawText(tRenderX, tRenderY, 0, cTextScale, tBuffer);
        }
    }

    //--Clean up.
    StarlightColor::ClearMixer();
}
void PandemoniumLevel::SubRenderDamageAnimation(float pRenderX, float pRenderY)
{
    //--Subroutine to actually render the portrait. This is because the portrait can switch colors if
    //  the entity in question is dying, and that code would take up a lot of space inside an already
    //  fairly complicated rendering function.
    DamageAnimPack *rPack = (DamageAnimPack *)mDamageAnimationQueue->GetElementBySlot(0);
    if(!rPack || !rPack->rDamageImage) return;

    //--Normal case, no poof.
    if(!rPack->mAnimatePoof || rPack->mDamageTimer < PL_DAM_TICKS_TOTAL)
    {
        rPack->rDamageImage->Draw(pRenderX, pRenderY);
    }
    //--This is a poof case. First, fade up to white.
    else if(rPack->mDamageTimer < PL_DAM_TICKS_TOTAL + PL_DAM_POOF_WHITE_TICKS)
    {
        //--Use this timer.
        int tUseTimer = rPack->mDamageTimer - PL_DAM_TICKS_TOTAL;

        //--Setup.
        glEnable(GL_STENCIL_TEST);
        glColorMask(true, true, true, true);
        glDepthMask(true);
        glStencilFunc(GL_ALWAYS, 2, 0xFF);
        glStencilOp(GL_REPLACE, GL_REPLACE, GL_REPLACE);
        glStencilMask(0xFF);

        //--Render.
        rPack->rDamageImage->Draw(pRenderX, pRenderY);

        //--Now switch to rendering mode.
        glColorMask(true, true, true, true);
        glDepthMask(true);
        glStencilFunc(GL_EQUAL, 2, 0xFF);
        glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
        glStencilMask(0xFF);

        //--Blend based on the timing.
        float tPercent = EasingFunction::QuadraticOut(tUseTimer, PL_DAM_POOF_WHITE_TICKS);
        glDisable(GL_TEXTURE_2D);
        glColor4f(1.0f, 1.0f, 1.0f, tPercent);
        rPack->rDamageImage->Draw(pRenderX, pRenderY);

        //--Clean up.
        glEnable(GL_TEXTURE_2D);
        glDisable(GL_STENCIL_TEST);
        StarlightColor::ClearMixer();
    }
    //--Now fade out to black.
    else if(rPack->mDamageTimer < PL_DAM_TICKS_TOTAL + PL_DAM_POOF_WHITE_TICKS + PL_DAM_POOF_BLACK_TICKS)
    {
        //--Use this timer.
        int tUseTimer = rPack->mDamageTimer - PL_DAM_TICKS_TOTAL - PL_DAM_POOF_WHITE_TICKS;

        //--Setup.
        glEnable(GL_STENCIL_TEST);
        glColorMask(false, false, false, false);
        glDepthMask(false);
        glStencilFunc(GL_ALWAYS, 2, 0xFF);
        glStencilOp(GL_REPLACE, GL_REPLACE, GL_REPLACE);
        glStencilMask(0xFF);

        //--Render.
        rPack->rDamageImage->Draw(pRenderX, pRenderY);

        //--Now switch to rendering mode.
        glColorMask(true, true, true, true);
        glDepthMask(true);
        glStencilFunc(GL_EQUAL, 2, 0xFF);
        glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
        glStencilMask(0xFF);

        //--Render based on the timing.
        float tPercent = 1.0f - EasingFunction::QuadraticInOut(tUseTimer, PL_DAM_POOF_BLACK_TICKS);
        glDisable(GL_TEXTURE_2D);
        glColor4f(0.0f, 0.0f, 0.0f, tPercent);
        rPack->rDamageImage->Draw(pRenderX, pRenderY);

        //--Clean up.
        glEnable(GL_TEXTURE_2D);
        glDisable(GL_STENCIL_TEST);
        StarlightColor::ClearMixer();
    }
    //--Don't render anything.
    else
    {

    }
}
