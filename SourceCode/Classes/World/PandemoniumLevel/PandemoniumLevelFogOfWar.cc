//--Base
#include "PandemoniumLevel.h"

//--Classes
#include "Actor.h"
#include "PandemoniumRoom.h"

//--CoreClasses
//--Definitions
#include "HitDetection.h"

//--GUI
//--Libraries
//--Managers
#include "EntityManager.h"

//--If enabled, parts of the map will be darkened or brightened by an overlay based on what the player can see.
#define AMBIENCE 0.15f

void PandemoniumLevel::ClearFog()
{
    //--Resets the fog values back to full darkness. Stores the previous values for the purposes of crossfades.
    if(xDisableDynamicFog) return;
    mFogTimer = 0;
    memcpy(mOldFogIndices, mFogIndices, sizeof(float) * FOG_ACCY * FOG_ACCY);

    //--Set to the ambient value.
    for(int x = 0; x < FOG_ACCY; x ++)
    {
        for(int y = 0; y < FOG_ACCY; y ++)
        {
            mFogIndices[x][y] = AMBIENCE;
        }
    }
}
void PandemoniumLevel::AddBrightnessAt(float pX, float pY, float pAddValue)
{
    //--Computes the distance between each of the fog points and adds to the overlay's value at the points
    //  that are near enough to be lit.
    if(xDisableDynamicFog) return;

    //--The provided coordinates are room coordinates, so they need to be offset to match the center of the fog.
    pX = pX + (FOG_MAP * 0.5f);
    pY = pY + (FOG_MAP * 0.5f);

    //--Iterate.
    for(int x = 0; x < FOG_ACCY; x ++)
    {
        for(int y = 0; y < FOG_ACCY; y ++)
        {
            //--The position of the index...
            float cIndexX = x * (FOG_MAP / (float)FOG_ACCY);
            float cIndexY = y * (FOG_MAP / (float)FOG_ACCY);

            //--The distance between the two.
            float cDistance = GetPlanarDistance(cIndexX, cIndexY, pX, pY);

            //--Influence is reduced by distance.
            if(cDistance == 0.0f)
            {
                mFogIndices[x][y] = mFogIndices[x][y] + pAddValue;
            }
            //--Inverse square law.
            else
            {
                mFogIndices[x][y] = mFogIndices[x][y] + (pAddValue * (1.0f / cDistance / cDistance));
            }

            //--Clamp.
            if(mFogIndices[x][y] > 1.0f) mFogIndices[x][y] = 1.0f;
        }
    }
}
void PandemoniumLevel::RebuildFogInfo()
{
    //--Resets and rebuilds information concerning fog of war.
    if(xDisableDynamicFog) return;
    ClearFog();

    //--Constants.
    const float cLuminosity = 750.0f;

    //--Locate the player.
    Actor *rPlayerEntity = EntityManager::Fetch()->GetLastPlayerEntity();
    if(!rPlayerEntity) return;

    //--Impose brightness at the player's world location. This always happens.
    AddBrightnessAt(rPlayerEntity->GetWorldX() * 100.0f, rPlayerEntity->GetWorldY() * 100.0f, cLuminosity);

    //--If the player is in a room, apply the brightness to each adjacent room.
    PandemoniumRoom *rPlayerRoom = rPlayerEntity->GetCurrentRoom();
    if(!rPlayerRoom) return;

    //--Note: We do not bother with the up/down navigation direction.
    PandemoniumRoom *rNRoom = rPlayerRoom->GetConnection(NAV_NORTH);
    if(rNRoom) AddBrightnessAt(rNRoom->GetWorldX() * 100.0f, rNRoom->GetWorldY() * 100.0f, cLuminosity);
    PandemoniumRoom *rERoom = rPlayerRoom->GetConnection(NAV_EAST);
    if(rERoom) AddBrightnessAt(rERoom->GetWorldX() * 100.0f, rERoom->GetWorldY() * 100.0f, cLuminosity);
    PandemoniumRoom *rSRoom = rPlayerRoom->GetConnection(NAV_SOUTH);
    if(rSRoom) AddBrightnessAt(rSRoom->GetWorldX() * 100.0f, rSRoom->GetWorldY() * 100.0f, cLuminosity);
    PandemoniumRoom *rWRoom = rPlayerRoom->GetConnection(NAV_WEST);
    if(rWRoom) AddBrightnessAt(rWRoom->GetWorldX() * 100.0f, rWRoom->GetWorldY() * 100.0f, cLuminosity);
}
void PandemoniumLevel::RenderFogOverlay()
{
    //--Renders the fog overlay relative to the center point of the mansion. We further offset it
    //  to center it around the middle of each room, not the top left edge.
    if(xDisableDynamicFog) return;

    //--Reposition.
    glTranslatef(FOG_MAP * -0.5f, FOG_MAP * -0.5f, 0.0f);
    glTranslatef(          25.0f,           25.0f, 0.0f);

    //--GL Setup.
    glDisable(GL_TEXTURE_2D);
    glBegin(GL_QUADS);

    //--Compute sizing.
    float cQuad = (FOG_MAP / (float)FOG_ACCY);
    float tCrossfadePercent = (float)mFogTimer / (float)FOG_CROSSFADE_TICKS;

    //--Render each sub-quad.
    for(int x = 1; x < FOG_ACCY; x ++)
    {
        for(int y = 1; y < FOG_ACCY; y ++)
        {
            //--Compute values.
            float tTLVal = 1.0f - mFogIndices[x-1][y-1];
            float tTRVal = 1.0f - mFogIndices[x+0][y-1];
            float tBRVal = 1.0f - mFogIndices[x+0][y+0];
            float tBLVal = 1.0f - mFogIndices[x-1][y+0];

            //--Merging.
            if(mFogTimer < FOG_CROSSFADE_TICKS)
            {
                //--Old index values.
                float tOldTLVal = 1.0f - mOldFogIndices[x-1][y-1];
                float tOldTRVal = 1.0f - mOldFogIndices[x+0][y-1];
                float tOldBRVal = 1.0f - mOldFogIndices[x+0][y+0];
                float tOldBLVal = 1.0f - mOldFogIndices[x-1][y+0];

                //--Apply the crossfade.
                tTLVal = (tTLVal * tCrossfadePercent) + (tOldTLVal * (1.0f - tCrossfadePercent));
                tTRVal = (tTRVal * tCrossfadePercent) + (tOldTRVal * (1.0f - tCrossfadePercent));
                tBLVal = (tBLVal * tCrossfadePercent) + (tOldBLVal * (1.0f - tCrossfadePercent));
                tBRVal = (tBRVal * tCrossfadePercent) + (tOldBRVal * (1.0f - tCrossfadePercent));
            }

            //--Top-Left.
            glColor4f(0.0f, 0.0f, 0.0f, tTLVal);
            glVertex2f(cQuad * (x+0), cQuad * (y+0));

            //--Top-Right.
            glColor4f(0.0f, 0.0f, 0.0f, tTRVal);
            glVertex2f(cQuad * (x+1), cQuad * (y+0));

            //--Bottom-Right.
            glColor4f(0.0f, 0.0f, 0.0f, tBRVal);
            glVertex2f(cQuad * (x+1), cQuad * (y+1));

            //--Bottom-Left.
            glColor4f(0.0f, 0.0f, 0.0f, tBLVal);
            glVertex2f(cQuad * (x+0), cQuad * (y+1));
        }
    }

    //--Clean.
    glEnd();
    glEnable(GL_TEXTURE_2D);
    glTranslatef(        -25.0f,         -25.0f, 0.0f);
    glTranslatef(FOG_MAP * 0.5f, FOG_MAP * 0.5f, 0.0f);
}
