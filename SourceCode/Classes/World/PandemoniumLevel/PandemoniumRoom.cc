//--Base
#include "PandemoniumRoom.h"

//--Classes
#include "Actor.h"
#include "InventorySubClass.h"
#include "InventoryItem.h"
#include "PandemoniumLevel.h"
#include "RootEffect.h"
#include "VisualLevel.h"
#include "WorldContainer.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarLinkedList.h"
#include "SugarFont.h"

//--Definitions
#include "Global.h"
#include "DeletionFunctions.h"

//--GUI
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "DebugManager.h"
#include "EntityManager.h"
#include "OptionsManager.h"

//=========================================== System ==============================================
PandemoniumRoom::PandemoniumRoom()
{
    //--[RootObject]
    //--System
    mType = POINTER_TYPE_PANDEMONIUM_ROOM;

    //--[PandemoniumRoom]
    //--System
    mLocalName = InitializeString("Unnamed Room");

    //--Position
    mWorldX = 0;
    mWorldY = 0;
    mWorldZ = 0;

    //--Connectivity
    mConnectionRenderList = 0;
    memset(&mConnectivityNames, 0, sizeof(char *) * NAV_TOTAL);
    memset(&rConnection, 0, sizeof(PandemoniumRoom *) * NAV_TOTAL);
    mShortestPathSize = 0;
    mShortestPaths = NULL;

    //--Entities
    mEntityList = new SugarLinkedList(false);

    //--Zone Effects
    mEffectList = new SugarLinkedList(true);

    //--Inventory
    mLocalInventory = new InventorySubClass();
    mCompressedInventory = new SugarLinkedList(true);

    //--Container Listing
    mContainerList = new SugarLinkedList(true);

    //--Rendering
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    rUpArrow   = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/GUI/Map/UpArrow");
    rDnArrow   = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/GUI/Map/DnArrow");
    rUpDnArrow = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/GUI/Map/UpDnArrow");

    //--Public Variables
    mPathDistance = -1;

    //--[Statics]
    //--The first time a room is booted, also initialize the statics.
    if(xHasSetupRenderingPositions) return;
    xHasSetupRenderingPositions = true;

    //--The arrays have a rising action set. Obviously the 1st entry has 1 position, 2nd has 2
    //  and so on. If you go out of bounds, it's your fault, idiot!
    //--The maximum dots to display is MAX_DIE. More than that will be ignored.
    SetMemoryData(__FILE__, __LINE__);
    xDicePackages = (DicePackage **)starmemoryalloc(sizeof(DicePackage *) * MAX_DIE);
    for(int i = 0; i < MAX_DIE; i ++)
    {
        SetMemoryData(__FILE__, __LINE__);
        xDicePackages[i] = (DicePackage *)starmemoryalloc(sizeof(DicePackage) * (i + 1));
        memset(xDicePackages[i], 0, sizeof(DicePackage) * (i + 1));
    }

    //--One dot: Duh, dead center.
    xDicePackages[0][0].Set(0.0f, 8.0f, 16.0f);

    //--Two dots: Aligned diagonally.
    xDicePackages[1][0].Set(-8.0f, -8.0f, 8.0f);
    xDicePackages[1][1].Set( 8.0f,  8.0f, 8.0f);

    //--Three dots: Form a triangle.
    xDicePackages[2][0].Set(-8.0f, -8.0f, 8.0f);
    xDicePackages[2][1].Set( 8.0f, -8.0f, 8.0f);
    xDicePackages[2][2].Set( 0.0f,  8.0f, 8.0f);

    //--Four dots: Form a square.
    xDicePackages[3][0].Set(-8.0f, -8.0f, 8.0f);
    xDicePackages[3][1].Set( 8.0f, -8.0f, 8.0f);
    xDicePackages[3][2].Set(-8.0f,  8.0f, 8.0f);
    xDicePackages[3][3].Set( 8.0f,  8.0f, 8.0f);

    //--Five Dots: Form a cross.
    xDicePackages[4][0].Set( 0.0f, -8.0f, 6.0f);
    xDicePackages[4][1].Set(-8.0f, -8.0f, 6.0f);
    xDicePackages[4][2].Set(-8.0f,  8.0f, 6.0f);
    xDicePackages[4][3].Set( 8.0f, -8.0f, 6.0f);
    xDicePackages[4][4].Set( 8.0f,  8.0f, 6.0f);

    //--Six Dots: Two vertical lines.
    xDicePackages[5][0].Set(-8.0f, -8.0f, 6.0f);
    xDicePackages[5][1].Set(-8.0f,  0.0f, 6.0f);
    xDicePackages[5][2].Set(-8.0f,  8.0f, 6.0f);
    xDicePackages[5][3].Set( 8.0f, -8.0f, 6.0f);
    xDicePackages[5][4].Set( 8.0f,  0.0f, 6.0f);
    xDicePackages[5][5].Set( 8.0f,  8.0f, 6.0f);
}
PandemoniumRoom::~PandemoniumRoom()
{
    free(mLocalName);
    for(int i = 0; i < NAV_TOTAL; i ++) free(mConnectivityNames[i]);
    delete mEntityList;
    delete mLocalInventory;
    delete mCompressedInventory;
    delete mContainerList;
    delete mEffectList;
    glDeleteLists(mConnectionRenderList, 1);
}

//--[Private Statics]
//--Rendering Positions (Static Constant)
bool PandemoniumRoom::xHasSetupRenderingPositions = false;
DicePackage **PandemoniumRoom::xDicePackages = NULL;

//--[Public Statics]
bool PandemoniumRoom::xFirstPath = true;
bool PandemoniumRoom::xHasBuiltDirectionNames = false;
char PandemoniumRoom::xDirectionNames[NAV_TOTAL][32];
char PandemoniumRoom::xReverseNames[NAV_TOTAL][32];

//--[Static Constants]
const float PandemoniumRoom::cSpacing = 100.0f;
const float PandemoniumRoom::cSquareOuter = 50.0f;
const float PandemoniumRoom::cSquareInner = 40.0f;
const float PandemoniumRoom::cLineWidth = 5.0f;

//====================================== Property Queries =========================================
char *PandemoniumRoom::GetName()
{
    return mLocalName;
}
int PandemoniumRoom::GetWorldX()
{
    return mWorldX;
}
int PandemoniumRoom::GetWorldY()
{
    return mWorldY;
}
int PandemoniumRoom::GetWorldZ()
{
    return mWorldZ;
}
Actor *PandemoniumRoom::IsPlayerPresent()
{
    //--Searches the entity list. If a player-controlled entity is found, return it. Otherwise
    //  return NULL.
    Actor *rActor = (Actor *)mEntityList->PushIterator();
    while(rActor)
    {
        if(rActor->IsPlayerControlled())
        {
            mEntityList->PopIterator();
            return rActor;
        }
        rActor = (Actor *)mEntityList->AutoIterate();
    }

    //--None found.
    return NULL;
}
bool PandemoniumRoom::IsPlayerAdjacent()
{
    //--Returns true if the player is in any room adjacent to this one. This does check up and down!
    //  This does NOT check if the player is in this room, use IsPlayerPresent() for that.
    for(int i = 0; i < NAV_TOTAL; i ++)
    {
        //--Ignore empty connections.
        if(!rConnection[i]) continue;

        //--Ask the room if the player is present.
        if(rConnection[i]->IsPlayerPresent()) return true;
    }

    //--All checks failed, player is not adjacent.
    return false;
}
PandemoniumRoom *PandemoniumRoom::GetConnection(int pFlag)
{
    //--Uses the connections NAV_NORTH to NAV_DOWN in Definitions.h. can legally return null
    //  on error or if there's no connection.
    if(pFlag < 0 || pFlag >= NAV_TOTAL) return NULL;
    return rConnection[pFlag];
}
PandemoniumRoom *PandemoniumRoom::GetConnection(const char *pDirectionName)
{
    //--Uses human-readable strings to get the navigation effect.
    if(!pDirectionName) return NULL;

    //--North.
    if(!strcasecmp(pDirectionName, "North") || !strcasecmp(pDirectionName, "N"))
    {
        return GetConnection(NAV_NORTH);
    }
    //--East.
    else if(!strcasecmp(pDirectionName, "East") || !strcasecmp(pDirectionName, "E"))
    {
        return GetConnection(NAV_EAST);
    }
    //--South.
    else if(!strcasecmp(pDirectionName, "South") || !strcasecmp(pDirectionName, "S"))
    {
        return GetConnection(NAV_SOUTH);
    }
    //--West.
    else if(!strcasecmp(pDirectionName, "West") || !strcasecmp(pDirectionName, "W"))
    {
        return GetConnection(NAV_WEST);
    }
    //--Up.
    else if(!strcasecmp(pDirectionName, "Up") || !strcasecmp(pDirectionName, "U"))
    {
        return GetConnection(NAV_UP);
    }
    //--Down.
    else if(!strcasecmp(pDirectionName, "Down") || !strcasecmp(pDirectionName, "D"))
    {
        return GetConnection(NAV_DOWN);
    }

    //--Failed!
    return NULL;
}
int PandemoniumRoom::GetEntitiesPresentTotal()
{
    return mEntityList->GetListSize();
}
Actor *PandemoniumRoom::GetEntity(int pSlot)
{
    return (Actor *)mEntityList->GetElementBySlot(pSlot);
}
int PandemoniumRoom::GetItemsTotal()
{
    return mLocalInventory->GetInventorySize();
}
int PandemoniumRoom::GetCompressedInventorySize()
{
    return mCompressedInventory->GetListSize();
}
InventoryPack *PandemoniumRoom::GetItemPack(int pSlot)
{
    //--This uses the compressed inventory, not the proper inventory. This makes it a lot easier when there
    //  are many items in a room.
    if(mLocalInventory->mRequiresReconstitution) ReconstituteInventory();

    //--Make sure it exists.
    InventoryPack *rCheckPack = (InventoryPack *)mCompressedInventory->GetElementBySlot(pSlot);
    if(rCheckPack)
    {
        return rCheckPack;
    }

    //--Doesn't exist, return NULL.
    return NULL;
}
InventoryItem *PandemoniumRoom::GetItem(int pSlot)
{
    //--Note: This returns directly from the local inventory, not the compressed inventory.
    return mLocalInventory->GetItemL(pSlot);
}
InventoryItem *PandemoniumRoom::GetItem(const char *pName)
{
    return mLocalInventory->GetItemS(pName);
}
int PandemoniumRoom::GetContainersTotal()
{
    //--Purges containers which are self-destructing. This is because AIs will sometimes attempt to open
    //  a container that should be destructing, and they call this at that time.
    WorldContainer *rContainer = (WorldContainer *)mContainerList->SetToHeadAndReturn();
    while(rContainer)
    {
        if(rContainer->IsSelfDestructing()) mContainerList->RemoveRandomPointerEntry();
        rContainer = (WorldContainer *)mContainerList->IncrementAndGetRandomPointerEntry();
    }

    //--Return the modified size.
    return mContainerList->GetListSize();
}
bool PandemoniumRoom::IsOneItemPresent()
{
    //--Returns true if at least one consumable is in the room.
    int tItemCount = mLocalInventory->GetInventorySize();
    for(int i = 0; i < tItemCount; i ++)
    {
        //--Check the item.
        InventoryItem *rItem = mLocalInventory->GetItemL(i);
        if(!rItem) continue;

        //--If the item is a consumable, return true.
        if(rItem->GetItemType() == ITEM_TYPE_CONSUMABLE) return true;
    }

    //--All checks failed.
    return false;
}
bool PandemoniumRoom::IsOneWeaponPresent()
{
    //--Returns true if at least one weapon is in the room.
    int tItemCount = mLocalInventory->GetInventorySize();
    for(int i = 0; i < tItemCount; i ++)
    {
        //--Check the item.
        InventoryItem *rItem = mLocalInventory->GetItemL(i);
        if(!rItem) continue;

        //--If the item is a consumable, return true.
        if(rItem->GetItemType() == ITEM_TYPE_WEAPON) return true;
    }

    //--All checks failed.
    return false;
}
bool PandemoniumRoom::IsEffectPresent(const char *pName)
{
    //--Returns true if at least one effect with the given name is in this room.
    if(!pName) return false;

    //--Scan. Effects are not guaranteed to have a given name.
    RootEffect *rEffect = (RootEffect *)mEffectList->PushIterator();
    while(rEffect)
    {
        //--Name can legally be null, it must be checked for that first.
        const char *rEffectName = rEffect->GetName();
        if(rEffectName && !strcasecmp(rEffectName, pName))
        {
            mEffectList->PopIterator();
            return true;
        }

        //--Next.
        rEffect = (RootEffect *)mEffectList->AutoIterate();
    }

    //--All checks failed, effect does not exist.
    return false;
}

//========================================= Manipulators ==========================================
void PandemoniumRoom::SetName(const char *pName)
{
    if(!pName) return;
    ResetString(mLocalName, pName);
}
void PandemoniumRoom::SetPosition(int pX, int pY, int pZ)
{
    mWorldX = pX;
    mWorldY = pY;
    mWorldZ = pZ;
}
void PandemoniumRoom::SetConnectivity(int pDirection, const char *pName)
{
    if(pDirection < 0 || pDirection >= NAV_TOTAL) return;
    ResetString(mConnectivityNames[pDirection], pName);
}
void PandemoniumRoom::RegisterActor(Actor *pActor)
{
    //--Registers the Actor to the room. Fails if it's already on the list.
    if(!pActor) return;

    //--Check for a match.
    if(mEntityList->IsElementOnList(pActor)) return;

    //--Register.
    mEntityList->AddElement("X", pActor);

    //--If required, reconstitute.
    ReconstituteInventory();
}
void PandemoniumRoom::UnregisterActor(Actor *pActor)
{
    //--Unregisters the actor. Fails silently if it was null or not present.
    mEntityList->RemoveElementP(pActor);

    //--If required, reconstitute.
    ReconstituteInventory();
}
void PandemoniumRoom::RegisterItem(InventoryItem *pItem)
{
    //--Registers a raw item. It's not in a container.
    if(!pItem) return;

    //--Reg it.
    mLocalInventory->RegisterItem(pItem);

    //--If required, reconstitute.
    ReconstituteInventory();
}
void PandemoniumRoom::LiberateItem(InventoryItem *pItem)
{
    //--Removes the given item from this room's ownership.
    if(!pItem) return;
    mLocalInventory->LiberateItemP(pItem);

    //--Find the item pack which matches this item. Remove one quantity from it.
    InventoryPack *rPack = (InventoryPack *)mCompressedInventory->PushIterator();
    while(rPack)
    {
        if(rPack->rItem && !strcasecmp(rPack->rItem->GetName(), pItem->GetName()))
        {
            rPack->mQuantity --;
            mCompressedInventory->PopIterator();
            break;
        }

        rPack = (InventoryPack *)mCompressedInventory->AutoIterate();
    }

    //--If required, reconstitute.
    ReconstituteInventory();
}
void PandemoniumRoom::RegisterContainer(WorldContainer *pContainer)
{
    //--Registers the container to the container list. Takes ownership.
    mContainerList->AddElement("X", pContainer, &RootObject::DeleteThis);

    //--Tell the container we own it.
    if(pContainer) pContainer->SetOwner(this);

    //--If required, reconstitute.
    ReconstituteInventory();
}
void PandemoniumRoom::OpenContainer(int pSlot, const char *pOpenerName, bool pIsFirstPerson)
{
    //--[System]
    //--Opens the container in the requested slot, printing information if the player is present.
    //  If the item doesn't exist in that slot, does nothing.

    //--Fetch.
    WorldContainer *rContainer = (WorldContainer *)mContainerList->GetElementBySlot(pSlot);
    if(!rContainer) return;

    //--[Printing]
    //--Is the player present? If so, print something. We need a name to do this.
    char *tConsoleString = rContainer->AssembleOpenString(pOpenerName, pIsFirstPerson);
    if(!pOpenerName || IsPlayerPresent() == NULL)
    {
        //--Open it and reconstitute if necessary.
        rContainer->Open();
        ReconstituteInventory();

        //--Clean up.
        mContainerList->RemoveElementI(pSlot);
        free(tConsoleString);
        return;
    }
    PandemoniumLevel::AppendToConsoleStatic(tConsoleString);
    PandemoniumLevel::AppendToConsoleStatic(" ");
    free(tConsoleString);

    //--[Execution]
    //--Open.
    rContainer->Open();

    //--If required, reconstitute.
    ReconstituteInventory();

    //--Sound effect can be heard if the player is present.
    WorldContainer::PlayOpenSound();

    //--[Clean]
    //--Remove the container from the list.
    mContainerList->RemoveElementI(pSlot);

    //--[Visual Level Stuff]
    //--Create a new ExtraEntityRenderPack if there is a VisualLevel to handle it. This is used in 3D
    //  mode to cause a container to fade out.
    VisualLevel *rVisualLevel = VisualLevel::Fetch();
    if(!rVisualLevel || rContainer->GetVisibilityTimer() > 0) return;

    //--Create.
    SetMemoryData(__FILE__, __LINE__);
    ExtraEntityRenderPack *nRenderPack = (ExtraEntityRenderPack *)starmemoryalloc(sizeof(ExtraEntityRenderPack));
    memset(nRenderPack, 0, sizeof(ExtraEntityRenderPack));
    nRenderPack->mParentID = 0;
    nRenderPack->mRenderSlot = rContainer->mLastRenderSlot;
    nRenderPack->mRenderTicksLeft = VL_ENTITY_FADE_TICKS;
    nRenderPack->rRenderImg = (SugarBitmap *)DataLibrary::Fetch()->GetEntry("Root/Images/GUI/3DUI/TreasureChest");

    //--Register.
    rVisualLevel->RegisterExtraEntityPack(nRenderPack);
}
void PandemoniumRoom::RegisterEffect(RootEffect *pEffect)
{
    if(!pEffect) return;
    mEffectList->AddElement("X", pEffect, &RootObject::DeleteThis);

    //--If required, reconstitute.
    ReconstituteInventory();
}

//========================================= Core Methods ==========================================
void PandemoniumRoom::BuildConnectivity(int pRoomsTotal, PandemoniumRoom **pRoomList)
{
    //--Given a list of rooms (provided by a PandemoniumLevel), builds the connectivity information
    //  and checks for rooms that are named incorrectly. A missing connection is printed as an error.
    if(pRoomsTotal < 1 || !pRoomList) return;

    //--Begin.
    for(int i = 0; i < NAV_TOTAL; i ++)
    {
        //--If the nav name is null, or "None", don't bother.
        if(!mConnectivityNames[i] || !strcasecmp(mConnectivityNames[i], "None")) continue;

        //--Otherwise, scan the list for the named room. Note that it can't be *this* room!
        for(int p = 0; p < pRoomsTotal; p ++)
        {
            //--Error check.
            if(!pRoomList[p] || pRoomList[p] == this) continue;

            //--If the name matches, set it.
            if(!strcasecmp(pRoomList[p]->GetName(), mConnectivityNames[i]))
            {
                DebugManager::Print(" Room %s connects to %s\n", mLocalName, pRoomList[p]->GetName());
                rConnection[i] = pRoomList[p];
                break;
            }
        }

        //--Nothing found, error!
        if(!rConnection[i])
        {
            //DebugManager::ForcePrint("Error: %s No connection %s\n", mLocalName, mConnectivityNames[i]);
        }
    }
}
void PandemoniumRoom::SetupPathingNames()
{
    //--Builds, or rebuilds, pathing names. This is 'north', 'south', etc.
    if(xHasBuiltDirectionNames) return;
    xHasBuiltDirectionNames = true;

    //--Build.
    strcpy(xDirectionNames[NAV_NORTH], "north");
    strcpy(xDirectionNames[NAV_SOUTH], "south");
    strcpy(xDirectionNames[NAV_WEST],  "west");
    strcpy(xDirectionNames[NAV_EAST],  "east");
    strcpy(xDirectionNames[NAV_UP],    "up");
    strcpy(xDirectionNames[NAV_DOWN],  "down");

    //--Build reverses.
    strcpy(xReverseNames[NAV_NORTH], "the south");
    strcpy(xReverseNames[NAV_SOUTH], "the north");
    strcpy(xReverseNames[NAV_WEST],  "the east");
    strcpy(xReverseNames[NAV_EAST],  "the west");
    strcpy(xReverseNames[NAV_UP],    "below");
    strcpy(xReverseNames[NAV_DOWN],  "above");
}
bool PandemoniumRoom::HasHostileEntity(uint8_t pTeamFlags)
{
    //--If any one entity in the room is hostile to the given flags, returns true. Otherwise, false.
    Actor *rActor = (Actor *)mEntityList->PushIterator();
    while(rActor)
    {
        if(rActor->IsHostileTo(pTeamFlags) && !rActor->IsStunned())
        {
            mEntityList->PopIterator();
            return true;
        }

        rActor = (Actor *)mEntityList->AutoIterate();
    }
    return false;
}
bool PandemoniumRoom::HasDarknessCloud()
{
    //--Method that determines if a room has a [Cloud of Darkness] in it. Rooms with this property will
    //  display differently during RenderBody().
    RootEffect *rEffect = (RootEffect *)mEffectList->PushIterator();
    while(rEffect)
    {
        //--Is the name "[Cloud of Darkness]"? That's all we care about.
        if(!strcasecmp(rEffect->GetDisplayName(), "[Cloud of Darkness]"))
        {
            mEffectList->PopIterator();
            return true;
        }

        //--Next.
        rEffect = (RootEffect *)mEffectList->AutoIterate();
    }

    //--Nothing detected, return false.
    return false;
}
void PandemoniumRoom::ReconstituteInventory()
{
    //--[Documenation]
    //--When called, updates mCompressedInventory to contain what is currently in the inventory.
    //  Objects with the same name get stacked together.
    if(!mLocalInventory->mRequiresReconstitution) return;

    //--[Complex Reconstitue]
    //--If we have a VisualLevel, then item packages cannot be deleted willy-nilly since they need to track
    //  their fading timers. This routine is slower but preserves the item packages.
    VisualLevel *rVisualLevel = VisualLevel::Fetch();
    if(rVisualLevel)
    {
        //--Go through the compressed inventory. Remove instances that no longer exist, create new ones if necessary,
        //  or update the quantity.
        InventoryPack *rPack = (InventoryPack *)mCompressedInventory->SetToHeadAndReturn();
        while(rPack)
        {
            //--If the item is NULL, remove it right away. This is erroneous.
            if(!rPack->rItem)
            {
                mCompressedInventory->RemoveRandomPointerEntry();
            }
            //--If the item's quantity hit 0, remove it with a fade.
            else if(rPack->mQuantity < 1)
            {
                //--Create a rendering package around it.
                if(rPack->mIsVisibleTimer > 0)
                {
                    SetMemoryData(__FILE__, __LINE__);
                    ExtraEntityRenderPack *nRenderPack = (ExtraEntityRenderPack *)starmemoryalloc(sizeof(ExtraEntityRenderPack));
                    memset(nRenderPack, 0, sizeof(ExtraEntityRenderPack));
                    nRenderPack->mParentID = 0;
                    nRenderPack->mRenderSlot = rPack->mLastRenderSlot;
                    nRenderPack->mRenderTicksLeft = VL_ENTITY_FADE_TICKS;
                    nRenderPack->rRenderImg = rPack->rItem->Get3DImageFor();
                    rVisualLevel->RegisterExtraEntityPack(nRenderPack);
                }

                //--Remove it.
                mCompressedInventory->RemoveRandomPointerEntry();
            }
            //--If the item in the pack is not on the inventory list, the pack is invalid. Remove it.
            else if(mLocalInventory->GetItemCountP(rPack->rItem) < 1)
            {
                mCompressedInventory->RemoveRandomPointerEntry();
            }
            //--Otherwise, zero the quantity. The next loop will reset the quantities as required.
            else
            {
                rPack->mQuantity = 0;
            }

            //--Next.
            rPack = (InventoryPack *)mCompressedInventory->IncrementAndGetRandomPointerEntry();
        }

        //--Now create new packages for new items, if any.
        for(int i = 0; i < mLocalInventory->GetInventorySize(); i ++)
        {
            //--Get the item in question.
            InventoryItem *rItem = mLocalInventory->GetItemL(i);
            if(!rItem) continue;

            //--Scan the inventory list. Is there one with a matching name?
            InventoryPack *rCheckPack = (InventoryPack *)mCompressedInventory->GetElementByName(rItem->GetName());
            if(rCheckPack)
            {
                rCheckPack->mQuantity ++;
            }
            //--No match, create a new one.
            else
            {
                SetMemoryData(__FILE__, __LINE__);
                InventoryPack *nNewPack = (InventoryPack *)starmemoryalloc(sizeof(InventoryPack));
                nNewPack->mQuantity = 1;
                nNewPack->rItem = rItem;
                nNewPack->mIsVisibleTimer = 0;
                nNewPack->mLastRenderSlot = -1;
                nNewPack->mLastSetVisible = 0;
                mCompressedInventory->AddElement(rItem->GetName(), nNewPack, &FreeThis);
            }
        }
    }
    //--[No Visual Level]
    //--This version simply discards the packages and recreates them. Faster, simpler, doesn't care about timers.
    else
    {
        //--Clear the list.
        mCompressedInventory->ClearList();

        //--Go through the local inventory...
        for(int i = 0; i < mLocalInventory->GetInventorySize(); i ++)
        {
            //--Get the item in question.
            InventoryItem *rItem = mLocalInventory->GetItemL(i);
            if(!rItem) continue;

            //--Scan the inventory list. Is there one with a matching name?
            InventoryPack *rCheckPack = (InventoryPack *)mCompressedInventory->GetElementByName(rItem->GetName());
            if(rCheckPack)
            {
                rCheckPack->mQuantity ++;
            }
            //--No match, create a new one.
            else
            {
                SetMemoryData(__FILE__, __LINE__);
                InventoryPack *nNewPack = (InventoryPack *)starmemoryalloc(sizeof(InventoryPack));
                nNewPack->mQuantity = 1;
                nNewPack->rItem = rItem;
                nNewPack->mIsVisibleTimer = VL_ENTITY_FADE_TICKS;
                nNewPack->mLastRenderSlot = -1;
                nNewPack->mLastSetVisible = 0;
                mCompressedInventory->AddElement(rItem->GetName(), nNewPack, &FreeThis);
            }
        }
    }

    //--Unflag the inventory's reconstitution flag.
    mLocalInventory->mRequiresReconstitution = false;
}

//===================================== Private Core Methods ======================================
//============================================ Update =============================================
void PandemoniumRoom::UpdatePulse()
{
    //--Called once per tick by the owning PandemoniumLevel. Presently, checks for containers
    //  that need to be removed.
    bool tDidAnything = false;
    WorldContainer *rContainer = (WorldContainer *)mContainerList->SetToHeadAndReturn();
    while(rContainer)
    {
        if(rContainer->IsSelfDestructing())
        {
            tDidAnything = true;
            mContainerList->RemoveRandomPointerEntry();
        }
        rContainer = (WorldContainer *)mContainerList->IncrementAndGetRandomPointerEntry();
    }

    //--Now check for items that may need to be removed.
    if(mLocalInventory->PurgeMarkedItems()) tDidAnything = true;

    //--If required, reconstitute.
    if(tDidAnything) ReconstituteInventory();
}
void PandemoniumRoom::EffectPulse()
{
    //--Updates all the local effects. Any effects that need to be removed are removed after their update.
    bool tDidAnything = false;
    RootEffect *rEffect = (RootEffect *)mEffectList->SetToHeadAndReturn();
    while(rEffect)
    {
        //--Run the effect.
        rEffect->Update();

        //--Remove if pending destruct.
        if(rEffect->mSelfDestruct)
        {
            tDidAnything = true;
            mEffectList->RemoveRandomPointerEntry();
        }

        //--Next.
        rEffect = (RootEffect *)mEffectList->IncrementAndGetRandomPointerEntry();
    }

    //--If required, reconstitute. Effects could (theoretically) move items around.
    if(tDidAnything) ReconstituteInventory();
}
void PandemoniumRoom::RespondToActorExit(Actor *pActor)
{
    //--When an Actor leaves this room, it removes itself from the local entity list, *then* calls
    //  this function. This is mostly to inform the player of their exit, if the player is present.
    if(!pActor) return;

    //--The player must be in the current room, *not* the room of the Actor that just left. Just
    //  check if they're in the current room.
    Actor *rPlayerActor = EntityManager::Fetch()->GetLastPlayerEntity();
    if(!rPlayerActor || rPlayerActor->GetCurrentRoom() != this) return;

    //--Common code.
    SetupPathingNames();
    int tLastDirection = pActor->GetLastInstruction() - NAVIGATION_OFFSET;

    //--If this action was a fleeing one, change the printcode.
    if(pActor->WasLastMoveFlee())
    {
        //--Find the first entity that is hostile to the Actor. Use their name.
        const char *rUseName = NULL;
        Actor *rActor = (Actor *)mEntityList->PushIterator();
        while(rActor)
        {
            if(rActor != pActor && pActor->IsHostileTo(rActor))
            {
                rUseName = rActor->GetName();
                mEntityList->PopIterator();
                break;
            }

            rActor = (Actor *)mEntityList->AutoIterate();
        }

        pActor->PrintByCode(PRINTCODE_MOVE_FLEEING_EXIT, false, pActor->GetName(), rUseName, xDirectionNames[tLastDirection]);
    }
    //--Normal movement action.
    else
    {
        pActor->PrintByCode(PRINTCODE_MOVE_EXIT, false, pActor->GetName(), "Name2", xDirectionNames[tLastDirection]);
    }
}
void PandemoniumRoom::RespondToActorEntry(Actor *pActor, PandemoniumRoom *pPreviousRoom)
{
    //--When an Actor changes which room it's in, this gets called with the Actor in question
    //  as the parameter. It should not be called with a NULL or it fails silently.
    //--pPreviousRoom is not used in this formulation, but it is used in the daughter classes.
    if(!pActor) return;

    //--Common code.
    SetupPathingNames();
    int tLastDirection = pActor->GetLastInstruction() - NAVIGATION_OFFSET;

    //--If this action was a fleeing one, change the printcode.
    bool tPrintedAnything = false;
    if(pActor->WasLastMoveFlee())
    {
        tPrintedAnything = pActor->PrintByCode(PRINTCODE_MOVE_FLEEING_ENTER, true, pActor->GetName(), mLocalName, xReverseNames[tLastDirection]);
    }
    //--Normal movement action.
    else
    {
        tPrintedAnything = pActor->PrintByCode(PRINTCODE_MOVE_ENTER, true, pActor->GetName(), mLocalName, xReverseNames[tLastDirection]);
    }

    //--If we printed anything (because the player was present) we should also spit out a list of
    //  the entities that are visible.
    if(tPrintedAnything && pActor == EntityManager::Fetch()->GetLastPlayerEntity())
    {
         Actor::PrintEntitiesInRoom(mEntityList);
    }
}

//=========================================== File I/O ============================================
//=========================================== Drawing =============================================
void PandemoniumRoom::Render()
{
    //--Renders a square showing the room, and renders connectivity lines if needed.

    //--Reposition.
    glTranslatef(mWorldX * cSpacing, mWorldY * cSpacing, 0.0f);
    glTranslatef(cSpacing * 0.50f, cSpacing * 0.50f, 0.0f);

    //--Render the body and connections if underlays are disabled.
    if(!PandemoniumLevel::xShow2DUnderlays)
    {
        RenderBody();
        RenderConnections();
    }
    else
    {
        RenderReducedBody();
        RenderConnections();
    }

    //--Render local entities. Typically, only player-controlled entities render.
    RenderEntities(false);

    //--Clean.
    glTranslatef(cSpacing * -0.50f, cSpacing * -0.50f, 0.0f);
    glTranslatef(mWorldX * -cSpacing, mWorldY * -cSpacing, 0.0f);
}
void PandemoniumRoom::RenderReducedBody()
{
    //--Renders the square body of the room, with a de-emphasis because an underlay is beneath it.
    glDisable(GL_TEXTURE_2D);
    StarlightColor cOutline = StarlightColor::MapRGBAI(191, 191, 191, 255);

    //--Size of the squares.
    float cWid = cSquareOuter;
    float cHei = cSquareOuter;
    float cInd = 2.0f;

    //--Normal case: Display a square.
    if(!HasDarknessCloud() && (IsPlayerPresent() || IsPlayerAdjacent()))
    {
        cOutline.SetAsMixer();
    }
    //--Darkness cloud!
    else if(HasDarknessCloud() && (IsPlayerPresent() || IsPlayerAdjacent()))
    {
        float cRadians = ((float)Global::Shared()->gTicksElapsed / 90.0f) * 3.1415926f;
        float cTickFactor = sinf(cRadians);
        float cFactor = 0.20f + (cTickFactor * 0.10f);
        glColor3f(cOutline.r * cFactor, cOutline.g * cFactor, cOutline.b * cFactor);
    }
    //--Player is not adjacent or present. Grey it out slightly.
    else
    {
        float cFactor = 0.60f;
        glColor4f(cOutline.r * cFactor, cOutline.g * cFactor, cOutline.b * cFactor, 1.0f);
    }

    //--Render it.
    glTranslatef(cSquareOuter / -2.0f, cSquareOuter / -2.0f, 0.0f);
    glBegin(GL_QUADS);
        //--Top bar
        glVertex2f(0.0f, 0.0f);
        glVertex2f(cWid, 0.0f);
        glVertex2f(cWid, cInd);
        glVertex2f(0.0f, cInd);

        //--Bot bar
        glVertex2f(0.0f, cHei - cInd);
        glVertex2f(cWid, cHei - cInd);
        glVertex2f(cWid, cHei);
        glVertex2f(0.0f, cHei);

        //--Lft bar
        glVertex2f(0.0f, cInd);
        glVertex2f(cInd, cInd);
        glVertex2f(cInd, cHei - cInd);
        glVertex2f(0.0f, cHei - cInd);

        //--Rgt bar
        glVertex2f(cWid - cInd, cInd);
        glVertex2f(cWid,        cInd);
        glVertex2f(cWid,        cHei - cInd);
        glVertex2f(cWid - cInd, cHei - cInd);
    glEnd();

    //--Clean.
    glTranslatef(cSquareOuter / 2.0f, cSquareOuter / 2.0f, 0.0f);
    glColor3f(1.0f, 1.0f, 1.0f);
    glEnable(GL_TEXTURE_2D);
}
void PandemoniumRoom::RenderBody()
{
    //--Renders the square body of the room. Assumes the rendering cursor is at the exact center.
    glDisable(GL_TEXTURE_2D);
    StarlightColor cOutline = StarlightColor::MapRGBAI(191, 191, 191, 255);
    StarlightColor cInline  = StarlightColor::MapRGBAI(255, 255, 255, 255);

    //--Size of the squares.
    float cOut = cSquareOuter / 2.0f;
    float cInn = cSquareInner / 2.0f;

    //--Normal case: Display a square.
    if(!HasDarknessCloud() && (IsPlayerPresent() || IsPlayerAdjacent()))
    {
        //--Outer square.
        cOutline.SetAsMixer();
        glBegin(GL_QUADS);
            glVertex2f(-cOut, -cOut);
            glVertex2f( cOut, -cOut);
            glVertex2f( cOut,  cOut);
            glVertex2f(-cOut,  cOut);
        glEnd();

        //--Inner square.
        cInline.SetAsMixer();
        glBegin(GL_QUADS);
            glVertex2f(-cInn, -cInn);
            glVertex2f( cInn, -cInn);
            glVertex2f( cInn,  cInn);
            glVertex2f(-cInn,  cInn);
        glEnd();
    }
    //--Darkness cloud!
    else if(HasDarknessCloud() && (IsPlayerPresent() || IsPlayerAdjacent()))
    {
        //--Outer square.
        float cRadians = ((float)Global::Shared()->gTicksElapsed / 90.0f) * 3.1415926f;
        float cTickFactor = sinf(cRadians);
        float cFactor = 0.20f + (cTickFactor * 0.10f);
        glColor3f(cOutline.r * cFactor, cOutline.g * cFactor, cOutline.b * cFactor);
        glBegin(GL_QUADS);
            glVertex2f(-cOut, -cOut);
            glVertex2f( cOut, -cOut);
            glVertex2f( cOut,  cOut);
            glVertex2f(-cOut,  cOut);
        glEnd();

        //--Inner square.
        glColor4f(cInline.r * cFactor, cInline.g * cFactor, cInline.b * cFactor, 1.0f);
        glBegin(GL_QUADS);
            glVertex2f(-cInn, -cInn);
            glVertex2f( cInn, -cInn);
            glVertex2f( cInn,  cInn);
            glVertex2f(-cInn,  cInn);
        glEnd();
    }
    //--Player is not adjacent or present. Grey it out slightly.
    else
    {
        //--Outer square.
        float cFactor = 0.60f;
        glColor4f(cOutline.r * cFactor, cOutline.g * cFactor, cOutline.b * cFactor, 1.0f);
        glBegin(GL_QUADS);
            glVertex2f(-cOut, -cOut);
            glVertex2f( cOut, -cOut);
            glVertex2f( cOut,  cOut);
            glVertex2f(-cOut,  cOut);
        glEnd();

        //--Inner square.
        glColor4f(cInline.r * cFactor, cInline.g * cFactor, cInline.b * cFactor, 1.0f);
        glBegin(GL_QUADS);
            glVertex2f(-cInn, -cInn);
            glVertex2f( cInn, -cInn);
            glVertex2f( cInn,  cInn);
            glVertex2f(-cInn,  cInn);
        glEnd();
    }

    //--Clean.
    glColor3f(1.0f, 1.0f, 1.0f);
    glEnable(GL_TEXTURE_2D);
}
void PandemoniumRoom::RenderEntities(bool pOnlyRenderIcons)
{
    //--Renders symbols to indicate which entities are in the room. Note that flags determine
    //  what the player can see, so not all entities will be displayed.
    //--The flag pOnlyRenderIcons means only Actor icons should render (humans, monsters). It is called
    //  if the PandemoniumRoom is on a different Z-level from the player.
    bool tRadarFlag = OptionsManager::Fetch()->GetOptionB("Always Show Radar Indicators");
    bool tFogFlag   = OptionsManager::Fetch()->GetOptionB("No Fog of War for Items");

    //--Special icons. These render independent of visibility.
    if(!pOnlyRenderIcons && ((IsPlayerPresent() || IsPlayerAdjacent()) || tFogFlag || tRadarFlag))
    {
        RenderSpecialIcons();
    }

    //--First, count how many 'visible' entities there are. There are several sizes and organizations
    //  required for visible entities.
    int tLegalCount = 0;
    Actor *rActor = (Actor *)mEntityList->PushIterator();
    while(rActor)
    {
        //--When using the xActorIconsMove flag, Actors will fade out if they can't be seen automatically. Therefore,
        //  all Actors count if they have a legal icon.
        if(PandemoniumLevel::xActorIconsMove)
        {
            //--Actor needs to have a legal icon.
            if((rActor->GetRenderFlag() == RENDER_MONSTER || rActor->GetRenderFlag() == RENDER_HUMAN))
            {
                tLegalCount ++;
            }
        }
        //--Normal, direct-rendering case. Entities don't render when obscured or out of LOS.
        else
        {
            //--If an entity is obscured, don't count it.
            if(rActor->IsObscured())
            {

            }
            //--If the tRadarFlag is false, only show entities that are the player, or are adjacent to the player.
            else if(!tRadarFlag)
            {
                //--If the entity is the player, this is legal.
                if(rActor->IsPlayerControlled())
                {
                    tLegalCount ++;
                }
                //--If the entity is in the same room as the player, they are visible.
                else if(IsPlayerPresent() && (rActor->GetRenderFlag() == RENDER_MONSTER || rActor->GetRenderFlag() == RENDER_HUMAN))
                {
                    tLegalCount ++;
                }
                //--If the entity is adjacent to a room with the player, they should render.
                else if(IsPlayerAdjacent() && (rActor->GetRenderFlag() == RENDER_MONSTER || rActor->GetRenderFlag() == RENDER_HUMAN))
                {
                    tLegalCount ++;
                }
            }
            //--All checks passed, this is legal.
            else if((rActor->GetRenderFlag() == RENDER_MONSTER || rActor->GetRenderFlag() == RENDER_HUMAN))
            {
                tLegalCount ++;
            }
        }

        //--Next.
        rActor = (Actor *)mEntityList->AutoIterate();
    }

    //--Hey, no need to render anything.
    if(tLegalCount < 1) return;

    //--Other setup.
    int tSlot = 0;
    int i = tLegalCount - 1;
    if(i >= MAX_DIE) i = MAX_DIE - 1;

    //--Begin rendering.
    rActor = (Actor *)mEntityList->PushIterator();
    while(rActor)
    {
        //--If this flag is set, the Actor is added to the rendering list instead of rendering directly.
        if(PandemoniumLevel::xActorIconsMove)
        {
            //--The actor can be skipped if it doesn't have a legal icon.
            if(rActor->GetRenderFlag() == RENDER_HUMAN || rActor->GetRenderFlag() == RENDER_MONSTER)
            {
                //--Resolve position.
                float tX = (mWorldX * cSpacing) + (cSpacing * 0.50f) + xDicePackages[i][tSlot].mX;
                float tY = (mWorldY * cSpacing) + (cSpacing * 0.50f) + xDicePackages[i][tSlot].mY;
                tSlot ++;
                if(tSlot >= MAX_DIE) tSlot --;

                //--Add it.
                PandemoniumLevel::Fetch()->AddActorToList(rActor, tX, tY);
            }
        }
        //--Render the icon directly, assuming it's visible.
        else
        {
            //--Fail to render when obscured.
            bool tShouldRender = false;
            if(rActor->IsObscured())
            {
            }
            //--Radar flag is off, so only show things the player can see.
            else if(!tRadarFlag)
            {
                //--Player can always see herself.
                if(rActor->IsPlayerControlled())
                {
                    tShouldRender = true;
                }
                //--If the entity is in the same room as the player, they are visible.
                else if(IsPlayerPresent())
                {
                    tShouldRender = true;
                }
                //--If the entity is adjacent to a room with the player, they should render.
                else if(IsPlayerAdjacent())
                {
                    tShouldRender = true;
                }
            }
            //--All checks passed, render.
            else
            {
                tShouldRender = true;
            }

            //--Render.
            if(tShouldRender)
            {
                //--Base.
                rActor->GetLocalColor().SetAsMixer();

                //--Humans:
                if(rActor->GetRenderFlag() == RENDER_HUMAN)
                {
                    RenderHumanSymbol(xDicePackages[i][tSlot].mX, xDicePackages[i][tSlot].mY);
                    tSlot ++;
                }
                //--Monsters:
                else if(rActor->GetRenderFlag() == RENDER_MONSTER)
                {
                    RenderMonsterSymbol(xDicePackages[i][tSlot].mX, xDicePackages[i][tSlot].mY);
                    tSlot ++;
                }

                //--Stop if the slot goes over the edge. Should be logically impossible, but this is in
                //  case someone messes with the code.
                //--I'm looking at YOU, person reading this!
                if(tSlot > i)
                {
                    mEntityList->PopIterator();
                    break;
                }
            }
        }

        //--Next.
        rActor = (Actor *)mEntityList->AutoIterate();
    }

    //--Clean.
    glColor3f(1.0f, 1.0f, 1.0f);
}
void PandemoniumRoom::RenderConnections()
{
    //--Renders lines representing connection directions. Note that a line represents
    //  half a connection, since the other half should return the favour.
    if(mConnectionRenderList)
    {
        glCallList(mConnectionRenderList);
    }
    else
    {
        //--Create the list.
        mConnectionRenderList = glGenLists(1);
        glNewList(mConnectionRenderList, GL_COMPILE_AND_EXECUTE);

        float cGrey = 191.0f / 255.0f;
        glDisable(GL_TEXTURE_2D);
        glColor3f(cGrey, cGrey, cGrey);

        //--Constants.
        float cWid = cLineWidth / 2.0f;
        float cLen = (cSpacing - cSquareOuter) / 2.0f;
        float cExt = cSquareOuter / 2.0f;

        //--North connection.
        if(rConnection[NAV_NORTH])
        {
            glBegin(GL_QUADS);
                glVertex2f(-cWid, -cExt - cLen);
                glVertex2f( cWid, -cExt - cLen);
                glVertex2f( cWid, -cExt);
                glVertex2f(-cWid, -cExt);
            glEnd();
        }

        //--East connection.
        if(rConnection[NAV_EAST])
        {
            glBegin(GL_QUADS);
                glVertex2f(cExt       , -cWid);
                glVertex2f(cExt + cLen, -cWid);
                glVertex2f(cExt + cLen,  cWid);
                glVertex2f(cExt       ,  cWid);
            glEnd();
        }

        //--South
        if(rConnection[NAV_SOUTH])
        {
            glBegin(GL_QUADS);
                glVertex2f(-cWid, cExt + cLen);
                glVertex2f( cWid, cExt + cLen);
                glVertex2f( cWid, cExt);
                glVertex2f(-cWid, cExt);
            glEnd();
        }

        //--West connection.
        if(rConnection[NAV_WEST])
        {
            glBegin(GL_QUADS);
                glVertex2f(-cExt - cLen, -cWid);
                glVertex2f(-cExt       , -cWid);
                glVertex2f(-cExt       ,  cWid);
                glVertex2f(-cExt - cLen,  cWid);
            glEnd();
        }

        //--Finish.
        glEndList();
    }

    //--Upwards connection. Represented by an arrow.
    glEnable(GL_TEXTURE_2D);
    glColor3f(1.0f, 1.0f, 1.0f);
    if(rConnection[NAV_UP] && !rConnection[NAV_DOWN])
    {
        if(rUpArrow) rUpArrow->Draw(rUpArrow->GetWidth() / -2.0f, rUpArrow->GetHeight() / -2.0f);
    }
    else if(!rConnection[NAV_UP] && rConnection[NAV_DOWN])
    {
        if(rDnArrow) rDnArrow->Draw(rDnArrow->GetWidth() / -2.0f, rDnArrow->GetHeight() / -2.0f);
    }
    else if(rConnection[NAV_UP] && rConnection[NAV_DOWN])
    {
        if(rUpDnArrow) rUpDnArrow->Draw(rUpDnArrow->GetWidth() / -2.0f, rUpDnArrow->GetHeight() / -2.0f);
    }
}
void PandemoniumRoom::RenderCircle(float pXCenter, float pYCenter, float pRadius, int pAccuracy)
{
    //--Renders a filled circle at the given location (duh). The circle will cycle from 0 to 360 degrees,
    //  but how many points are rendered is based on the provided accuracy.
    //--Color and such should be set by the glState prior to calling this.
    if(pAccuracy <= 0) return;

    //--Position.
    glTranslatef(pXCenter, pYCenter, 0.0f);

    //--Setup.
    glBegin(GL_POLYGON);

    //--Loop.
    float tActualStep = (float)pAccuracy * TORADIAN;
    for(float i = 0.0f; i < 3.1415926f * 2.0f; i = i + tActualStep)
    {
        glVertex2f(cosf(i) * pRadius, sinf(i) * pRadius);
    }

    //--Clean.
    glEnd();
    glTranslatef(-pXCenter, -pYCenter, 0.0f);
}
void PandemoniumRoom::RenderHumanSymbol(float pXCenter, float pYCenter)
{
    //--Given a position, renders a human symbol there. Used for the player (when human) and their allies.
    SugarBitmap *rHumanBitmap = (SugarBitmap *)DataLibrary::Fetch()->GetEntry("Root/Images/GUI/Navigation/Human");
    if(!rHumanBitmap) return;

    //--Note: A small Y offset is added since the figures are taller than wide.
    rHumanBitmap->Draw(pXCenter - (rHumanBitmap->GetWidth() / 2), pYCenter - (rHumanBitmap->GetHeight()) + 5.0f);
}
void PandemoniumRoom::RenderMonsterSymbol(float pXCenter, float pYCenter)
{
    //--Given a position, renders a monster symbol there. Demons!
    SugarBitmap *rMonsterBitmap = (SugarBitmap *)DataLibrary::Fetch()->GetEntry("Root/Images/GUI/Navigation/Monster");
    if(!rMonsterBitmap) return;

    rMonsterBitmap->Draw(pXCenter - (rMonsterBitmap->GetWidth() / 2), pYCenter - (rMonsterBitmap->GetHeight()) + 5.0f);
}
void PandemoniumRoom::RenderTubeSymbol(float pXCenter, float pYCenter)
{
    //--Used for Golem Tubes. Same dimensions and offset as Human.
    SugarBitmap *rTubeBitmap = (SugarBitmap *)DataLibrary::Fetch()->GetEntry("Root/Images/GUI/Navigation/Tube");
    if(!rTubeBitmap) return;
    rTubeBitmap->Draw(pXCenter - (rTubeBitmap->GetWidth() / 2), pYCenter - (rTubeBitmap->GetHeight()) + 5.0f);
}
void PandemoniumRoom::RenderMirrorSymbol(float pXCenter, float pYCenter)
{
    //--Used for Rilmani Mirrors. Same dimensions and offset as Human.
    SugarBitmap *rMirrorBitmap = (SugarBitmap *)DataLibrary::Fetch()->GetEntry("Root/Images/GUI/Navigation/Mirror");
    if(!rMirrorBitmap) return;
    rMirrorBitmap->Draw(pXCenter - (rMirrorBitmap->GetWidth() / 2), pYCenter - (rMirrorBitmap->GetHeight()) + 5.0f);
}
void PandemoniumRoom::RenderNestSymbol(float pXCenter, float pYCenter)
{
    //--Harpy nest. Similar size to a monster icon.
    SugarBitmap *rNestBitmap = (SugarBitmap *)DataLibrary::Fetch()->GetEntry("Root/Images/GUI/Navigation/Nest");
    if(!rNestBitmap) return;
    rNestBitmap->Draw(pXCenter - (rNestBitmap->GetWidth() / 2), pYCenter - (rNestBitmap->GetHeight()));
}
void PandemoniumRoom::RenderCoffinSymbol(float pXCenter, float pYCenter)
{
    //--Vampire coffin.
    SugarBitmap *rCoffinBitmap = (SugarBitmap *)DataLibrary::Fetch()->GetEntry("Root/Images/GUI/Navigation/Coffin");
    if(!rCoffinBitmap) return;
    rCoffinBitmap->Draw(pXCenter - (rCoffinBitmap->GetWidth() / 2), pYCenter - (rCoffinBitmap->GetHeight()));
}
void PandemoniumRoom::RenderGlyphSymbol(float pXCenter, float pYCenter)
{
    //--Glyph of Power, from Corrupter Mode.
    SugarBitmap *rGlyphBitmap = (SugarBitmap *)DataLibrary::Fetch()->GetEntry("Root/Images/GUI/Navigation/Glyph");
    if(!rGlyphBitmap) return;
    rGlyphBitmap->Draw(pXCenter - (rGlyphBitmap->GetWidth() / 2.0f), pYCenter - (rGlyphBitmap->GetHeight() / 2.0f));
}
void PandemoniumRoom::RenderChestSymbol(float pXCenter, float pYCenter)
{
    //--Chest. Shows that at least one item is present in a chest.
    SugarBitmap *rChestBitmap = (SugarBitmap *)DataLibrary::Fetch()->GetEntry("Root/Images/GUI/Navigation/Chest");
    if(!rChestBitmap) return;
    rChestBitmap->Draw(pXCenter - (rChestBitmap->GetWidth() / 2), pYCenter - (rChestBitmap->GetHeight()));
}
void PandemoniumRoom::RenderItemSymbol(float pXCenter, float pYCenter)
{
    //--Item. Represents any non-weapon.
    SugarBitmap *rBitmap = (SugarBitmap *)DataLibrary::Fetch()->GetEntry("Root/Images/GUI/Navigation/Item");
    if(!rBitmap) return;
    rBitmap->Draw(pXCenter - (rBitmap->GetWidth() / 2), pYCenter - (rBitmap->GetHeight()));
}
void PandemoniumRoom::RenderWeaponSymbol(float pXCenter, float pYCenter)
{
    //--Weapon. Represents any equippable weapon.
    SugarBitmap *rBitmap = (SugarBitmap *)DataLibrary::Fetch()->GetEntry("Root/Images/GUI/Navigation/Weapon");
    if(!rBitmap) return;
    rBitmap->Draw(pXCenter - (rBitmap->GetWidth() / 2), pYCenter - (rBitmap->GetHeight()));
}

//======================================= Pointer Routing =========================================
SugarLinkedList *PandemoniumRoom::GetEntityList()
{
    return mEntityList;
}
InventorySubClass *PandemoniumRoom::GetLocalInventory()
{
    return mLocalInventory;
}
SugarLinkedList *PandemoniumRoom::GetContainerList()
{
    return mContainerList;
}
SugarLinkedList *PandemoniumRoom::GetEffectList()
{
    return mEffectList;
}

//====================================== Static Functions =========================================
//========================================= Lua Hooking ===========================================
void PandemoniumRoom::HookToLuaState(lua_State *pLuaState)
{
    /* PR_SetInternalName(sName)
       This is the name that will be displayed when the room is examined. It is also the name used
       when determining connectivity. */
    lua_register(pLuaState, "PR_SetInternalName", &Hook_PR_SetInternalName);

    /* PR_SetPosition(iX, iY, iZ)
       Sets the relative position of this room in the world. Doesn't affect connectivity, only
       affects the position on the minimap. */
    lua_register(pLuaState, "PR_SetPosition", &Hook_PR_SetPosition);

    /* PR_GetPosition() (3 integers)
       Returns the position of the given room in integers. */
    lua_register(pLuaState, "PR_GetPosition", &Hook_PR_GetPosition);

    /* PR_AddConnection("N", sConnectionName)
       PR_AddConnection("W", sConnectionName)
       PR_AddConnection("E", sConnectionName)
       PR_AddConnection("S", sConnectionName)
       PR_AddConnection("U", sConnectionName)
       PR_AddConnection("D", sConnectionName)
       Adds a new connection by name. When compiled, the name (if found) will be added as a connection
       at the provided direction. */
    lua_register(pLuaState, "PR_AddConnection", &Hook_PR_AddConnection);

    /* PR_HasConnection(sDirection)
       Returns the name of the room in the given direction, if it exists, otherwise "Null". The
       direction is a one-letter string, N/W/E/S/D/U. */
    lua_register(pLuaState, "PR_HasConnection", &Hook_PR_HasConnection);

    /* PR_GetEntitiesTotal()
       Returns how many entities are currently in this room. This only includes Actors, not Items. */
    lua_register(pLuaState, "PR_GetEntitiesTotal", &Hook_PR_GetEntitiesTotal);

    /* PR_PushEntity(iIndex)
       Pushes the requested entity atop the DataLibrary. */
    lua_register(pLuaState, "PR_PushEntity", &Hook_PR_PushEntity);

    /* PR_GetItemsTotal()
       Returns how many items are currently in the room. */
    lua_register(pLuaState, "PR_GetItemsTotal", &Hook_PR_GetItemsTotal);

    /* PR_GetContainersTotal()
       Returns how many containers are currently in the room. */
    lua_register(pLuaState, "PR_GetContainersTotal", &Hook_PR_GetContainersTotal);

    /* PR_PushItem(iIndex)
       Pushes the requested item atop the DataLibrary. */
    lua_register(pLuaState, "PR_PushItem", &Hook_PR_PushItem);

    /* PR_IsPlayerPresent()
       Returns true if a player-controlled entity is in the Active Room. */
    lua_register(pLuaState, "PR_IsPlayerPresent", &Hook_PR_IsPlayerPresent);

    /* PR_OpenContainer(iSlot, sActorName, bIsFirstPerson)
       Opens the container in the required slot, or does nothing if there's no container. */
    lua_register(pLuaState, "PR_OpenContainer", &Hook_PR_OpenContainer);

    /* PR_GetNextPathTo(sTargetLocation)
       Returns a string indicating the next room on the path to the target location. Ex:
       A -> B -> C. If at A, passing "C" will return "B". Returns "NULL" on error. */
    lua_register(pLuaState, "PR_GetNextPathTo", &Hook_PR_GetNextPathTo);

    /* PR_IsEffectPresent(sEffectName)
       Returns true if at least one effect with the requested name is in the active PandemoniumRoom. */
    lua_register(pLuaState, "PR_IsEffectPresent", &Hook_PR_IsEffectPresent);
}

//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
int Hook_PR_SetInternalName(lua_State *L)
{
    //PR_SetInternalName(sName)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("PR_SetInternalName");

    //--Type check.
    PandemoniumRoom *rRoom = (PandemoniumRoom *)DataLibrary::Fetch()->rActiveObject;
    if(!rRoom || !rRoom->IsOfType(POINTER_TYPE_PANDEMONIUM_ROOM)) return LuaTypeError("PR_SetInternalName");

    //--Set.
    rRoom->SetName(lua_tostring(L, 1));
    return 0;
}
int Hook_PR_SetPosition(lua_State *L)
{
    //PR_SetPosition(iX, iY, iZ)
    int tArgs = lua_gettop(L);
    if(tArgs < 3) return LuaArgError("PR_SetPosition");

    //--Type check.
    PandemoniumRoom *rRoom = (PandemoniumRoom *)DataLibrary::Fetch()->rActiveObject;
    if(!rRoom || !rRoom->IsOfType(POINTER_TYPE_PANDEMONIUM_ROOM)) return LuaTypeError("PR_SetPosition");

    //--Set.
    rRoom->SetPosition(lua_tointeger(L, 1), lua_tointeger(L, 2), lua_tointeger(L, 3));
    return 0;
}
int Hook_PR_GetPosition(lua_State *L)
{
    //PR_GetPosition() (3 integers)

    //--Type check.
    PandemoniumRoom *rRoom = (PandemoniumRoom *)DataLibrary::Fetch()->rActiveObject;
    if(!rRoom || !rRoom->IsOfType(POINTER_TYPE_PANDEMONIUM_ROOM))
    {
        LuaTypeError("PR_GetPosition");
        lua_pushinteger(L, -100);
        lua_pushinteger(L, -100);
        lua_pushinteger(L, -100);
        return 3;
    }

    //--Return.
    lua_pushinteger(L, rRoom->GetWorldX());
    lua_pushinteger(L, rRoom->GetWorldY());
    lua_pushinteger(L, rRoom->GetWorldZ());
    return 3;
}
int Hook_PR_AddConnection(lua_State *L)
{
    //PR_AddConnection("N", sConnectionName)
    //PR_AddConnection("W", sConnectionName)
    //PR_AddConnection("E", sConnectionName)
    //PR_AddConnection("S", sConnectionName)
    //PR_AddConnection("U", sConnectionName)
    //PR_AddConnection("D", sConnectionName)
    int tArgs = lua_gettop(L);
    if(tArgs < 2) return LuaArgError("PR_AddConnection");

    //--Type check.
    PandemoniumRoom *rRoom = (PandemoniumRoom *)DataLibrary::Fetch()->rActiveObject;
    if(!rRoom || !rRoom->IsOfType(POINTER_TYPE_PANDEMONIUM_ROOM)) return LuaTypeError("PR_AddConnection");

    //--Get the direction setting.
    int tDirection = NAV_NORTH;
    const char *rDirection = lua_tostring(L, 1);
    if(rDirection[0] == 'N')
        tDirection = NAV_NORTH;
    else if(rDirection[0] == 'W')
        tDirection = NAV_WEST;
    else if(rDirection[0] == 'S')
        tDirection = NAV_SOUTH;
    else if(rDirection[0] == 'E')
        tDirection = NAV_EAST;
    else if(rDirection[0] == 'U')
        tDirection = NAV_UP;
    else if(rDirection[0] == 'D')
        tDirection = NAV_DOWN;
    else
    {
        return LuaPropertyError("PR_AddConnection", rDirection, 2);
    }

    //--Set.
    rRoom->SetConnectivity(tDirection, lua_tostring(L, 2));
    return 0;
}
int Hook_PR_HasConnection(lua_State *L)
{
    //PR_HasConnection(sDirection)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("PR_HasConnection", L);

    //--Type check.
    PandemoniumRoom *rRoom = (PandemoniumRoom *)DataLibrary::Fetch()->rActiveObject;
    if(!rRoom || !rRoom->IsOfType(POINTER_TYPE_PANDEMONIUM_ROOM)) return LuaTypeError("PR_HasConnection", L);

    //--Get the connection's name.
    PandemoniumRoom *rConnection = rRoom->GetConnection(lua_tostring(L, 1));

    //--If the room exists, return its name.
    if(rConnection)
    {
        lua_pushstring(L, rConnection->GetName());
    }
    //--Otherwise, push "Null".
    else
    {
        lua_pushstring(L, "Null");
    }

    //--In all cases, return 1.
    return 1;
}
int Hook_PR_GetEntitiesTotal(lua_State *L)
{
    //PR_GetEntitiesTotal()

    //--Type check.
    PandemoniumRoom *rRoom = (PandemoniumRoom *)DataLibrary::Fetch()->rActiveObject;
    if(!rRoom || !rRoom->IsOfType(POINTER_TYPE_PANDEMONIUM_ROOM)) return LuaTypeError("PR_GetEntitiesTotal", L);

    //--Get the value and push it.
    lua_pushinteger(L, rRoom->GetEntitiesPresentTotal());
    return 1;
}
int Hook_PR_PushEntity(lua_State *L)
{
    //PR_PushEntity(iIndex)

    //--Setup.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    PandemoniumRoom *rRoom = (PandemoniumRoom *)rDataLibrary->rActiveObject;
    rDataLibrary->PushActiveEntity();

    //--Type check.
    if(!rRoom || !rRoom->IsOfType(POINTER_TYPE_PANDEMONIUM_ROOM)) return LuaTypeError("PR_PushEntity", L);

    //--Arg check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("PR_PushEntity", L);

    //--Set it as the top.
    rDataLibrary->rActiveObject = rRoom->GetEntity(lua_tointeger(L, 1));
    return 0;
}
int Hook_PR_GetItemsTotal(lua_State *L)
{
    //PR_GetItemsTotal()
    PandemoniumRoom *rRoom = (PandemoniumRoom *)DataLibrary::Fetch()->rActiveObject;
    if(!rRoom || !rRoom->IsOfType(POINTER_TYPE_PANDEMONIUM_ROOM)) return LuaTypeError("PR_GetItemsTotal", L);

    //--Get the value and push it.
    lua_pushinteger(L, rRoom->GetItemsTotal());

    return 1;
}
int Hook_PR_GetContainersTotal(lua_State *L)
{
    //PR_GetContainersTotal()
    PandemoniumRoom *rRoom = (PandemoniumRoom *)DataLibrary::Fetch()->rActiveObject;
    if(!rRoom || !rRoom->IsOfType(POINTER_TYPE_PANDEMONIUM_ROOM)) return LuaTypeError("PR_GetContainersTotal", L);

    //--Get the value and push it.
    lua_pushinteger(L, rRoom->GetContainersTotal());

    return 1;
}
int Hook_PR_PushItem(lua_State *L)
{
    //PR_PushItem(iIndex)

    //--Setup.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    PandemoniumRoom *rRoom = (PandemoniumRoom *)rDataLibrary->rActiveObject;
    rDataLibrary->PushActiveEntity();

    //--Type check.
    if(!rRoom || !rRoom->IsOfType(POINTER_TYPE_PANDEMONIUM_ROOM)) return LuaTypeError("PR_PushItem", L);

    //--Arg check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("PR_PushItem", L);

    //--Set it as the top.
    rDataLibrary->rActiveObject = rRoom->GetItem(lua_tointeger(L, 1));
    return 0;
}
int Hook_PR_IsPlayerPresent(lua_State *L)
{
    //PR_IsPlayerPresent()
    PandemoniumRoom *rRoom = (PandemoniumRoom *)DataLibrary::Fetch()->rActiveObject;
    if(!rRoom || !rRoom->IsOfType(POINTER_TYPE_PANDEMONIUM_ROOM)) return LuaTypeError("PR_IsPlayerPresent", L);

    //--Check.
    lua_pushboolean(L, (rRoom->IsPlayerPresent() != NULL));

    return 1;
}
int Hook_PR_OpenContainer(lua_State *L)
{
    //PR_OpenContainer(iSlot, sActorName, bIsFirstPerson)
    int tArgs = lua_gettop(L);
    if(tArgs < 3) return LuaArgError("PR_OpenContainer");

    //--Get the room.
    PandemoniumRoom *rRoom = (PandemoniumRoom *)DataLibrary::Fetch()->rActiveObject;
    if(!rRoom || !rRoom->IsOfType(POINTER_TYPE_PANDEMONIUM_ROOM)) return LuaTypeError("PR_OpenContainer", L);

    //--Open that container.
    rRoom->OpenContainer(lua_tointeger(L, 1), lua_tostring(L, 2), lua_toboolean(L, 3));
    return 0;
}
int Hook_PR_GetNextPathTo(lua_State *L)
{
    //PR_GetNextPathTo(sDestination)
    int tArgs = lua_gettop(L);
    if(tArgs < 1)
    {
        LuaArgError("PR_GetNextPathTo", L);
        lua_pushstring(L, "Null");
        return 1;
    }

    //--Get the room.
    PandemoniumRoom *rRoom = (PandemoniumRoom *)DataLibrary::Fetch()->rActiveObject;
    if(!rRoom || !rRoom->IsOfType(POINTER_TYPE_PANDEMONIUM_ROOM))
    {
        LuaTypeError("PR_GetNextPathTo", L);
        lua_pushstring(L, "Null");
        return 1;
    }

    //--Get the path case.
    PathInstructions *tPathData = rRoom->GetPathTo(lua_tostring(L, 1));

    //--Error:
    if(!tPathData || (tPathData && tPathData->mListSize < 1))
    {
        lua_pushstring(L, "Null");
        return 1;
    }

    //--Success. Pass back the name of the 0th move. Make sure it exists.
    PandemoniumRoom *rTargetRoom = rRoom->GetConnection(tPathData->mPathList[0]);
    if(!rTargetRoom)
    {
        lua_pushstring(L, "Null");
        return 1;
    }
    lua_pushstring(L, rTargetRoom->GetName());
    return 1;
}
int Hook_PR_IsEffectPresent(lua_State *L)
{
    //PR_IsEffectPresent(sEffectName)
    int tArgs = lua_gettop(L);
    if(tArgs < 1)
    {
        LuaArgError("PR_IsEffectPresent", L);
        lua_pushboolean(L, false);
        return 1;
    }

    //--Get the room.
    PandemoniumRoom *rRoom = (PandemoniumRoom *)DataLibrary::Fetch()->rActiveObject;
    if(!rRoom || !rRoom->IsOfType(POINTER_TYPE_PANDEMONIUM_ROOM))
    {
        LuaTypeError("PR_IsEffectPresent", L);
        lua_pushboolean(L, false);
        return 1;
    }

    //--Exec.
    lua_pushboolean(L, rRoom->IsEffectPresent(lua_tostring(L, 1)));
    return 1;
}
