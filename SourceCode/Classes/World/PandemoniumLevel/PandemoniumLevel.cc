//--Base
#include "PandemoniumLevel.h"

//--Classes
#include "Actor.h"
#include "ContextMenu.h"
#include "CorrupterMenu.h"
#include "InventoryItem.h"
#include "FlexMenu.h"
#include "PandemoniumRoom.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarLinkedList.h"
#include "SugarFont.h"

//--Definitions
#include "HitDetection.h"

//--GUI
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "ControlManager.h"
#include "DebugManager.h"
#include "EntityManager.h"
#include "LuaManager.h"
#include "MapManager.h"
#include "OptionsManager.h"

//--Debug
//#define PL_DEBUG
#ifdef PL_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

//=========================================== System ==============================================
PandemoniumLevel::PandemoniumLevel()
{
    //--[RootObject]
    //--System
    mType = POINTER_TYPE_PANDEMONIUMLEVEL;

    //--[IRenderable]
    //--System

    //--[RootLevel]
    //--System
    ResetString(mName, "Unnamed PandemoniumLevel");

    //--[PandemoniumLevel]
    //--System
    //--Construction
    mRoomPrototypeList = new SugarLinkedList(true);

    //--Final Layout
    mRoomsTotal = 0;
    mRooms = NULL;

    //--Virtual Buttons
    mHasSetupButtons = false;
    memset(mVirtualButtons, 0, sizeof(ButtonStruct) * VIRTUAL_BUTTONS_TOTAL);

    //--Console Strings
    mIsConsoleWaitingOnKeypress = false;
    mPostExecScript = NULL;
    mPostExecFiringCode = 0;
    mConsoleStrings = new SugarLinkedList(true);

    //--Status Strings
    mStatusStringsTotal = 0;
    mStatusStrings = NULL;

    //--Context Menu
    mShowContextMenu = false;
    mContextMenu = new ContextMenu();

    //--Character Pane
    mExtraIsWeapon = false;
    mIsUseSelected = false;
    mIsDropSelected = false;
    mSelectedInventorySlot = -1;
    mMaxInventorySlots = 0;
    mExtraInventorySlots = 0;
    mActivePortraitName = NULL;
    rActivePortrait = NULL;

    //--Inventory Command
    mInventoryList = new SugarLinkedList(true);

    //--Entity Pane
    mSelectedEntity = -1;
    mMaxEntities = 0;
    mEntityScrollOffset = 0;
    mTotalValidEntities = 0;
    mIsScrollbarClicked = false;
    mScrollbarClickStart = 0;
    mScrollbarClickY = 0;

    //--Menu Activity Stack
    mHasPendingClose = false;
    mSelfMenuPopulationScript = NULL;
    mSystemMenuPopulationScript = NULL;

    //--Corrupter Menu
    mCorrupterMenu = new CorrupterMenu();
    mMagicMenuPopulationScript = NULL;

    //--Actor Icon Animation Storage
    mActorIconList = new SugarLinkedList(true);

    //--Damage Animation
    mIsDamageAnimating = false;
    mDamageAnimationQueue = new SugarLinkedList(true);

    //--Underlay Maps
    mUnderlayMapsTotal = 0;
    mUnderlayZLevels = NULL;
    mUnderMapOffsetX = NULL;
    mUnderMapOffsetY = NULL;
    rUnderlayMaps = NULL;

    //--Fog of War
    mFogTimer = FOG_CROSSFADE_TICKS;
    memset(mFogIndices, 0, sizeof(float) * FOG_ACCY * FOG_ACCY);
    memset(mOldFogIndices, 0, sizeof(float) * FOG_ACCY * FOG_ACCY);

    //--Images
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    memset(&Images, 0, sizeof(Images));
    Images.Data.rNavBase      = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/GUI/Navigation/NavBase");
    Images.Data.rNorth        = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/GUI/Navigation/North");
    Images.Data.rEast         = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/GUI/Navigation/East");
    Images.Data.rSouth        = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/GUI/Navigation/South");
    Images.Data.rWest         = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/GUI/Navigation/West");
    Images.Data.rUp           = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/GUI/Navigation/Up");
    Images.Data.rDown         = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/GUI/Navigation/Down");
    Images.Data.rSkip         = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/GUI/Navigation/Skip");
    Images.Data.rScrollBtnDn  = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/GUI/EntitiesUI/ScrollBtnDn");
    Images.Data.rBackground   = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/GUI/Background/MenuBG");
    Images.Data.rHideConsole  = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/GUI/General/HideConsole");
    Images.Data.rShowConsole  = (SugarBitmap *)rDataLibrary->GetEntry("Root/Images/GUI/General/ShowConsole");
    Images.Data.rUIFontMedium = rDataLibrary->GetFont("Classic Level Main");
    Images.Data.rUIFontSmall  = rDataLibrary->GetFont("Classic Level Small");
    Images.Data.rConsoleFont = Images.Data.rUIFontMedium;
    if(xUseSmallConsoleFont) Images.Data.rConsoleFont = Images.Data.rUIFontSmall;
    Images.mIsReady = VerifyStructure(&Images.Data, sizeof(Images.Data), sizeof(void *));

    //--Now setup button positions.
    SetupButtons();

    //--Other UI constants.
    SetupUIConstants();

    //--Update the map zoom factor.
    PandemoniumLevel::xMapZoom = OptionsManager::Fetch()->GetOptionF("Map Zoom") / 100.0f;
}
PandemoniumLevel::~PandemoniumLevel()
{
    delete mRoomPrototypeList;
    for(int i = 0; i < mRoomsTotal; i ++) delete mRooms[i];
    free(mRooms);
    free(mActivePortraitName);
    delete mConsoleStrings;
    for(int i = 0; i < mStatusStringsTotal; i ++) free(mStatusStrings[i]);
    delete mContextMenu;
    free(mStatusStrings);
    free(mPostExecScript);
    free(mSelfMenuPopulationScript);
    free(mSystemMenuPopulationScript);
    delete mCorrupterMenu;
    free(mMagicMenuPopulationScript);
    delete mInventoryList;
    delete mActorIconList;
    free(mUnderlayZLevels);
    free(mUnderMapOffsetX);
    free(mUnderMapOffsetY);
    free(rUnderlayMaps);
    delete mDamageAnimationQueue;
}

//--Public Static Constants
const float PandemoniumLevel::cInvOffset = -85.0f;

//--Public Statics
bool PandemoniumLevel::xNextLineHasBreak = false;
float PandemoniumLevel::xMapZoom = 1.00f;

//--Flag dictates whether or not actor icons move instantly on the map (false) or whether it takes a few
//  ticks as they animate moving (true). Set through the OptionsManager.
bool PandemoniumLevel::xActorIconsMove = false;

//--How many ticks it takes the Actors to animate moving.
int PandemoniumLevel::xActorMoveTicks = 15;

//--Flag dictating whether or not damage animations play. The animations slow the game down slightly
//  by making the program wait until they've stopped animating to continue turn orders, but many
//  players will appreciate the immersion. Set through the OptionsManager.
bool PandemoniumLevel::xAnimateDamage = false;

//--Flag to show a 2D bitmap under the level that is rendering. Set through the OptionsManager.
bool PandemoniumLevel::xShow2DUnderlays = false;

//--Flag, use the small font instead of the medium font for the console. Set through the OptionsManager.
bool PandemoniumLevel::xUseSmallConsoleFont = true;

//--Flag, shows dynamic lighting over the world as the player moves to indicate what they can see. Set through the OptionsManager.
bool PandemoniumLevel::xDisableDynamicFog = false;

//====================================== Property Queries =========================================
bool PandemoniumLevel::IsWaitingOnKeypress()
{
    return mIsConsoleWaitingOnKeypress;
}
bool PandemoniumLevel::IsBlockingTurnChange()
{
    //--Presently, the only thing that blocks turn changes are damage animations, which can be disabled.
    return IsDamageAnimating();
}

//========================================= Manipulators ==========================================
void PandemoniumLevel::RegisterRoom(PandemoniumRoom *pRoom)
{
    if(!pRoom) return;
    mRoomPrototypeList->AddElement("X", pRoom, &RootObject::DeleteThis);
}
void PandemoniumLevel::WaitOnKeypress()
{
    mIsConsoleWaitingOnKeypress = true;
    ResetString(mPostExecScript, NULL);
}
void PandemoniumLevel::WaitOnKeypress(const char *pCallScript, int pFiringCode)
{
    mIsConsoleWaitingOnKeypress = true;
    ResetString(mPostExecScript, pCallScript);
    mPostExecFiringCode = pFiringCode;
}
void PandemoniumLevel::SetSelfMenuScript(const char *pScriptPath)
{
    ResetString(mSelfMenuPopulationScript, pScriptPath);
}
void PandemoniumLevel::SetSystemMenuScript(const char *pScriptPath)
{
    ResetString(mSystemMenuPopulationScript, pScriptPath);
}
void PandemoniumLevel::SetMagicScript(const char *pScript)
{
    ResetString(mMagicMenuPopulationScript, pScript);
}
void PandemoniumLevel::SetTotalSystemLines(int pTotal)
{
    //--Safety check.
    if(pTotal == mStatusStringsTotal) return;

    //--Deallocate.
    for(int i = 0; i < mStatusStringsTotal; i ++) free(mStatusStrings[i]);
    free(mStatusStrings);
    mStatusStrings = NULL;
    mStatusStringsTotal = 0;
    if(pTotal < 1) return;

    //--Allocate.
    mStatusStringsTotal = pTotal;
    SetMemoryData(__FILE__, __LINE__);
    mStatusStrings = (char **)starmemoryalloc(sizeof(char *) * mStatusStringsTotal);
    for(int i = 0; i < mStatusStringsTotal; i ++) mStatusStrings[i] = NULL;
}
void PandemoniumLevel::SetSystemLine(int pSlot, const char *pString)
{
    if(pSlot < 0 || pSlot >= mStatusStringsTotal) return;
    ResetString(mStatusStrings[pSlot], pString);
}

//========================================= Core Methods ==========================================
void PandemoniumLevel::FinalizeRooms()
{
    //--Takes the prototype list and compiles it into a fixed-size list of rooms. The rooms then
    //  resolve their connectivity based on the fixed list.
    if(mRoomPrototypeList->GetListSize() < 1) return;
    DebugManager::PushPrint(false, "Pandemonium Level: Beginning construction.\n");

    //--Clear old list, if it exists.
    DebugManager::Print("Clearing old data.\n");
    for(int i = 0; i < mRoomsTotal; i ++) delete mRooms[i];
    free(mRooms);
    mRoomsTotal = 0;
    mRooms = NULL;

    //--Allocate the fixed-size list.
    DebugManager::Print("Allocate and fill list.\n");
    mRoomsTotal = mRoomPrototypeList->GetListSize();
    SetMemoryData(__FILE__, __LINE__);
    mRooms = (PandemoniumRoom **)starmemoryalloc(sizeof(PandemoniumRoom *) * mRoomsTotal);
    for(int i = 0; i < mRoomsTotal; i ++)
    {
        mRoomPrototypeList->SetRandomPointerToHead();
        mRooms[i] = (PandemoniumRoom *)mRoomPrototypeList->LiberateRandomPointerEntry();
    }

    //--Debug.
    DebugManager::Print(" Room list:\n");
    for(int i = 0; i < mRoomsTotal; i ++)
    {
        DebugManager::Print("  %s %p\n", mRooms[i]->GetName(), mRooms[i]);
    }

    //--Now build the connectivity list.
    DebugManager::Print("Building connectivity.\n");
    for(int i = 0; i < mRoomsTotal; i ++)
    {
        mRooms[i]->BuildConnectivity(mRoomsTotal, mRooms);
    }

    //--Run the recursive pathing on the rooms.
    DebugManager::Print("Building paths.\n");
    for(int i = 0; i < mRoomsTotal; i ++)
    {
        mRooms[i]->BuildPathing(mRoomsTotal, mRooms);
    }

    //--All done.
    DebugManager::PopPrint("Pandemonium Level: Construction completed.\n");
}
void PandemoniumLevel::SpawnItemIn(InventoryItem *pItem, const char *pRoomName)
{
    //--Attemps to place the given item in the provided room. Note that this is a spawner, so the
    //  item is taken ownership of (the caller should not delete it).
    //--If the item can't be spawned, it is deleted.
    if(!pItem) return;
    if(!pRoomName)
    {
        delete pItem;
        return;
    }

    //--Is this a special code to place it in the player's inventory?
    if(!strcmp(pRoomName, "PLAYERINV"))
    {
        return;
    }
    //--Is this a special code to place it in a specific character's inventory?
    else if(!strcmp(pRoomName, "CHARINV|"))
    {
        return;
    }

    //--If we got this far, it's a room name. Try to find it.
    for(int i = 0; i < mRoomsTotal; i ++)
    {
        //--Matching name? Not case sensitive!
        if(strcasecmp(pRoomName, mRooms[i]->GetName())) continue;

        //--Register it.
        mRooms[i]->RegisterItem(pItem);
        return;
    }

    //--If we got this far, the room was not found. Delete the item and bark an error.
    DebugManager::ForcePrint("SpawnItemIn: Error, no room %s\n", pRoomName);
    delete pItem;
}
void PandemoniumLevel::HandleNewTurn()
{
    //--Called when a new turn starts. Presently, clears and resets lighting information.
    RebuildFogInfo();
}

//===================================== Private Core Methods ======================================
//============================================ Update =============================================
void PandemoniumLevel::Update()
{
    //--Timer.
    mFogTimer ++;

    //--Update actor icons if the flag is set.
    if(xActorIconsMove) UpdateActorIcons();

    //--Damage update. Locks the rest of the update out.
    if(IsDamageAnimating())
    {
        UpdateDamageAnimation();
        return;
    }

    //--Update. Handles GUI controls here.
    for(int i = 0; i < mRoomsTotal; i ++)
    {
        mRooms[i]->UpdatePulse();
    }

    //--If the CorrupterMenu is handling the update, run that and stop here.
    if(mCorrupterMenu->IsVisible())
    {
        mCorrupterMenu->Update();
        return;
    }

    //--If there's something on the menu stack, let that handle the update.
    if(MapManager::Fetch()->UpdateMenuStack(mHasPendingClose))
    {
        mHasPendingClose = false;
        return;
    }

    //--Get mouse position.
    ControlManager *rControlManager = ControlManager::Fetch();
    int tMouseX, tMouseY, tMouseZ;
    rControlManager->GetMouseCoords(tMouseX, tMouseY, tMouseZ);

    //--Reset this flag.
    mHasPendingClose = false;

    //--If the level is currently waiting on a keypress, handle that.
    if(mIsConsoleWaitingOnKeypress)
    {
        //--Wipe player controls!
        Actor::xAreControlsEnabled = false;

        //--Upon pressing a key, unflip the flag.
        if(rControlManager->IsAnyKeyPressed())
        {
            //--Flag.
            mIsConsoleWaitingOnKeypress = false;

            //--If we have a pending instruction, run that.
            if(mPostExecScript)
            {
                //--Free the old copy. We don't do this after the script runs, since it might replace
                //  the path (or not).
                char *tOldScriptPtr = mPostExecScript;
                mPostExecScript = NULL;

                //--Execute.
                LuaManager::Fetch()->ExecuteLuaFile(tOldScriptPtr, 1, "N", (float)mPostExecFiringCode);

                //--Clean up.
                free(tOldScriptPtr);
            }
        }

        return;
    }

    //--Recheck the button visibility.
    RecheckDisabledCases();

    //--Update button visibility timers.
    for(int i = 0; i < VIRTUAL_BUTTONS_TOTAL; i ++)
    {
        if(mVirtualButtons[i].mIsDisabled)
        {
            mVirtualButtons[i].mVisTimer --;
            if(mVirtualButtons[i].mVisTimer < 0) mVirtualButtons[i].mVisTimer = 0;
        }
        else
        {
            mVirtualButtons[i].mVisTimer ++;
            if(mVirtualButtons[i].mVisTimer > PL_VIS_TIMER_MAX) mVirtualButtons[i].mVisTimer = PL_VIS_TIMER_MAX;
        }
    }

    //--Update the context menu if it's visible.
    if(mShowContextMenu)
    {
        //--Run the update.
        mContextMenu->Update(tMouseX, tMouseY);

        //--If the update was not handled, check if a left-click occurred. If it did, hide the menu.
        if(!mContextMenu->HandledUpdate() && rControlManager->IsFirstPress("MouseLft"))
        {
            HideContextMenu();
        }

        //--If the menu handled the update but still wants to close, do that now.
        if(mContextMenu->HasPendingClose())
        {
            HideContextMenu();
        }

        //--In any case, ignore the rest of the update.
        return;
    }

    //--If the mouse is over the system menu button, handle that.
    if(rControlManager->IsFirstPress("MouseLft") && IsPointWithin2DReal(tMouseX, tMouseY, mSystemMenuBtn))
    {
        FlexMenu *nNewMenu = new FlexMenu();
        LuaManager::Fetch()->PushExecPop(nNewMenu, mSystemMenuPopulationScript);
        MapManager::Fetch()->PushMenuStack(nNewMenu);
        return;
    }

    //--If the mouse is over the corrupter menu button, and it exists, handle that.
    if(mMagicMenuPopulationScript && rControlManager->IsFirstPress("MouseLft") && IsPointWithin2DReal(tMouseX, tMouseY, mCorrupterMenuBtn))
    {
        mCorrupterMenu->SetVisibility(true);
        mCorrupterMenu->SetToStatusMode();
        return;
    }

    //--Magic spell stuff from the unrolled menu.
    if(mMagicMenuPopulationScript)
    {
        mUnrolledMenuDim = mCorrupterMenu->CalculateUnrolledSize(CharPane.cLft - 225.0f, ConsolePane.cTop, CharPane.cLft);
        if(mCorrupterMenu->UpdateUnrolledMenu(mUnrolledMenuDim)) return;
    }

    //--If the mouse is over the character window, change the inventory slot. It defaults to -1.
    mIsUseSelected = false;
    mIsDropSelected = false;
    if(tMouseX >= CharPane.cLft && tMouseX <= CharPane.cLft + CharPane.cWid &&
       tMouseY >= CharPane.cTop && tMouseY <= CharPane.cTop + CharPane.cHei)
    {
        //--Set the selected inventory slot.
        mSelectedInventorySlot = (tMouseY - (CharPane.cHei + ((mInventoryMembers * cFontSpacing * -1.0f) + cInvOffset) + cFontSpacing + (cFontSpacing * 1.5f))) / cFontSpacing;

        //--Clamp.
        if(tMouseY < (CharPane.cHei + ((mInventoryMembers * cFontSpacing * -1.0f) + cInvOffset) + cFontSpacing + (cFontSpacing * 1.5f))) mSelectedInventorySlot = -1;
        if(mSelectedInventorySlot >= mMaxInventorySlots+mExtraInventorySlots) mSelectedInventorySlot = -1;

        //--If the inventory slot is not -1, and the mouse is towards the right side, then
        //  turn on highlighting of the sub-buttons.
        if(mSelectedInventorySlot != -1)
        {
            //--"Use" button.
            if(tMouseX >= CharPane.cLft + CharPane.cWid - CharPane.cIndent - 115.0f &&
               tMouseX <  CharPane.cLft + CharPane.cWid - CharPane.cIndent - 115.0f + Images.Data.rUIFontSmall->GetTextWidth("Unequip"))
            {
                mIsUseSelected = true;
            }
            //--"Drop" button.
            else if(tMouseX >= CharPane.cLft + CharPane.cWid - CharPane.cIndent - 60.0f &&
                    tMouseX <  CharPane.cLft + CharPane.cWid - CharPane.cIndent)
            {
                mIsDropSelected = true;
            }
        }

        //--If the player clicks, perform the selected action. This is handled by a subroutine.
        if(rControlManager->IsFirstPress("MouseLft"))
        {
            CharacterHandleItemUse(mSelectedInventorySlot, mIsUseSelected, mIsDropSelected);
        }
    }
    //--Not over the pane, cancel.
    else
    {
        mSelectedInventorySlot = -1;
    }

    //--Recheck the scroll entity offsets so they never overcap.
    if(mEntityScrollOffset >= mTotalValidEntities - ENTITIES_DISPLAY_PER_GROUP - 1) mEntityScrollOffset = mTotalValidEntities - ENTITIES_DISPLAY_PER_GROUP - 1;
    if(mEntityScrollOffset < 0) mEntityScrollOffset = 0;

    //--If the mouse is over the entity window, change the entity slot. It defaults to -1.
    if(tMouseX >= EntityPane.cLft && tMouseX <= EntityPane.cLft + EntityPane.cWid - 30.0f &&
       tMouseY >= EntityPane.cTop && tMouseY <= EntityPane.cTop + EntityPane.cHei)
    {
        //--Set selected entity.
        mSelectedEntity = (tMouseY - (EntityPane.cTop + (EntityPane.cIndent * 3.0f) + cFontSpacing)) / (cFontSpacing + 2.0f);

        //--If the entity passes the edge of the list, ignore it.
        if(mSelectedEntity > mMaxEntities) mSelectedEntity = -1;
        if(tMouseY < EntityPane.cTop + (EntityPane.cIndent * 3.0f) + cFontSpacing) mSelectedEntity = -1;

        //--If the player has clicked, we need to handle that. Spawn a context menu on the
        //  selected entity, and re-position it if too wide.
        if(rControlManager->IsFirstPress("MouseLft"))
        {
            ShowContextMenuAround(mSelectedEntity + mEntityScrollOffset);
        }
    }
    //--Not over the pane, cancel.
    else
    {
        mSelectedEntity = -1;
    }

    //--Check if the mouse click is over the scrollbars on the entity pane.
    if(tMouseX >= EntityPane.cLft + EntityPane.cWid - 21.0f && tMouseX <= EntityPane.cLft + EntityPane.cWid &&
       tMouseY >= EntityPane.cTop                           && tMouseY <= EntityPane.cTop + EntityPane.cHei)
    {
        //--Must be a click happening.
        if(rControlManager->IsFirstPress("MouseLft"))
        {
            //--Top scroll bar.
            if(tMouseY < EntityPane.cTop + 21.0f)
            {
                if(mEntityScrollOffset > 0) mEntityScrollOffset --;
            }
            //--Bottom scroll bar.
            else if(tMouseY > EntityPane.cTop + EntityPane.cHei - 21.0f)
            {
                if(mEntityScrollOffset < mTotalValidEntities - ENTITIES_DISPLAY_PER_GROUP - 1) mEntityScrollOffset ++;
            }
            //--Otherwise, it's a click on the scrollbar.
            else
            {
                mIsScrollbarClicked = true;
                mScrollbarClickStart = mEntityScrollOffset;
                mScrollbarClickY = tMouseY;
            }
        }
    }

    //--If the scrollbar was clicked, then move the entity scroll offset around.
    if(mIsScrollbarClicked)
    {
        //--Position.
        if(cFontSpacing < 1.0f) cFontSpacing = 1.0f;
        mEntityScrollOffset = mScrollbarClickStart + ((tMouseY - mScrollbarClickY) / cFontSpacing);

        //--Recheck the clamps.
        if(mEntityScrollOffset >= mTotalValidEntities - ENTITIES_DISPLAY_PER_GROUP - 1) mEntityScrollOffset = mTotalValidEntities - ENTITIES_DISPLAY_PER_GROUP - 1;
        if(mEntityScrollOffset < 0) mEntityScrollOffset = 0;
    }

    //--If the mouse is not down, unset this flag.
    if(!rControlManager->IsDown("MouseLft")) mIsScrollbarClicked = false;

    //--Check if we need to reconstitute the inventory.
    Actor *rPlayerCharacter = EntityManager::Fetch()->GetLastPlayerEntity();
    if(rPlayerCharacter && rPlayerCharacter->GetInventoryReconstituteFlag() == true) ReconstituteInventory();
}
void PandemoniumLevel::EffectPulse()
{
    //--Tells the rooms to run their effects.
    for(int i = 0; i < mRoomsTotal; i ++)
    {
        mRooms[i]->EffectPulse();
    }
}

//=========================================== File I/O ============================================
//=========================================== Drawing =============================================
void PandemoniumLevel::AddToRenderList(SugarLinkedList *pRenderList)
{
    pRenderList->AddElement(mName, this);
}
void PandemoniumLevel::Render()
{
    //--Render the current minimap. Checks the player's position if centering is turned on.
    if(mRoomsTotal < 0 || !mRooms) return;

    //--Debug.
    DebugPush(true, "PandemoniumLevel: Begin Render.\n");

    //--World.
    RenderWorld();
    DebugPrint("Rendered World.\n");

    //--Navigation Pane.
    RenderNavigation();
    DebugPrint("Rendered Navigation.\n");

    //--Character Pane.
    RenderCharacterPane();
    DebugPrint("Rendered Character Pane.\n");

    //--Entities Pane.
    RenderEntitiesPane();
    DebugPrint("Rendered Entities Pane.\n");

    //--Console Pane.
    RenderConsolePane();
    DebugPrint("Rendered Console Pane.\n");

    //--Render the context menu over everything else.
    if(mShowContextMenu) mContextMenu->Render();
    DebugPrint("Rendered Context Menu.\n");

    //--Render mouse cursor for debug.
    if(OptionsManager::Fetch()->GetOptionB("ShowMouseDebug"))
    {
        //--Setup.
        glColor3f(0.0f, 1.0f, 0.0f);
        glDisable(GL_TEXTURE_2D);

        //--Mouse position.
        int tMouseX, tMouseY, tMouseZ;
        ControlManager::Fetch()->GetMouseCoords(tMouseX, tMouseY, tMouseZ);

        //--Render.
        glBegin(GL_QUADS);
            glVertex2f(tMouseX-3.0f, tMouseY-3.0f);
            glVertex2f(tMouseX+3.0f, tMouseY-3.0f);
            glVertex2f(tMouseX+3.0f, tMouseY+3.0f);
            glVertex2f(tMouseX-3.0f, tMouseY+3.0f);
        glEnd();

        //--Clean.
        glEnable(GL_TEXTURE_2D);
        glColor3f(1.0f, 1.0f, 1.0f);

        //--Debug.
        DebugPrint("Rendered Mouse Debug.\n");
    }

    //--[System Menu]
    //--Render a button in the top-left to open up the System Menu.
    MapManager::xHasRenderedMenus = true;
    if(!MapManager::Fetch()->MenuStackHasContents())
    {
        //--Setup.
        glColor4f(0.25f, 0.25f, 0.25f, 1.0f);
        glDisable(GL_TEXTURE_2D);

        //--Render.
        glBegin(GL_QUADS);
            glVertex2f(mSystemMenuBtn.mLft, mSystemMenuBtn.mTop);
            glVertex2f(mSystemMenuBtn.mRgt, mSystemMenuBtn.mTop);
            glVertex2f(mSystemMenuBtn.mRgt, mSystemMenuBtn.mBot);
            glVertex2f(mSystemMenuBtn.mLft, mSystemMenuBtn.mBot);
        glEnd();

        //--Clean.
        glEnable(GL_TEXTURE_2D);

        //--Font.
        float cScale = 1.0f;
        StarlightColor::ClearMixer();
        Images.Data.rUIFontSmall->DrawText(mSystemMenuBtn.mLft, mSystemMenuBtn.mTop, 0, cScale, ">Open System Menu<");

        //--Debug.
        DebugPrint("Rendered System Menu button.\n");
    }

    //--[Corrupter Menu Button]
    //--Renders a button in the top left, under the System Menu, to open the CorrupterMenu.
    if(mMagicMenuPopulationScript && !MapManager::Fetch()->MenuStackHasContents())
    {
        //--Setup.
        glColor4f(0.25f, 0.25f, 0.25f, 1.0f);
        glDisable(GL_TEXTURE_2D);

        //--Render.
        glBegin(GL_QUADS);
            glVertex2f(mCorrupterMenuBtn.mLft, mCorrupterMenuBtn.mTop);
            glVertex2f(mCorrupterMenuBtn.mRgt, mCorrupterMenuBtn.mTop);
            glVertex2f(mCorrupterMenuBtn.mRgt, mCorrupterMenuBtn.mBot);
            glVertex2f(mCorrupterMenuBtn.mLft, mCorrupterMenuBtn.mBot);
        glEnd();

        //--Clean.
        glEnable(GL_TEXTURE_2D);

        //--Font.
        float cScale = 1.0f;
        StarlightColor::ClearMixer();
        Images.Data.rUIFontSmall->DrawText(mCorrupterMenuBtn.mLft, mCorrupterMenuBtn.mTop, 0, cScale, ">Open Corrupter Menu<");

        //--Debug.
        DebugPrint("Rendered Corrupter Menu button.\n");
    }

    //--[Menu Render]
    //--If the menu stack has something on it, render them. We also render an overlay to grey
    //  out everything else.
    if(MapManager::Fetch()->MenuStackHasContents())
    {
        //--Overlay.
        glColor4f(0.0f, 0.0f, 0.0f, 0.75f);
        glDisable(GL_TEXTURE_2D);

        //--Render.
        glBegin(GL_QUADS);
            glVertex2f(            0.0f,             0.0f);
            glVertex2f(VIRTUAL_CANVAS_X,             0.0f);
            glVertex2f(VIRTUAL_CANVAS_X, VIRTUAL_CANVAS_Y);
            glVertex2f(            0.0f, VIRTUAL_CANVAS_Y);
        glEnd();

        //--Clean.
        glColor3f(1.0f, 1.0f, 1.0f);
        glEnable(GL_TEXTURE_2D);

        //--Now render the menu components.
        MapManager::Fetch()->RenderMenuStack();

        //--Debug.
        DebugPrint("Rendered Menu overlay.\n");
    }

    //--[Corrupter Menu]
    //--If open, render the CorrupterMenu.
    if(mCorrupterMenu->IsVisible())
    {
        //--Render.
        mCorrupterMenu->Render();

        //--Debug.
        DebugPrint("Rendered Corrupter Menu subobject.\n");
    }

    //--[Debug]
    DebugPop("PandemoniumLevel: Render Complete.\n");
}
void PandemoniumLevel::RenderWorld()
{
    //--Renders the map, showing the rooms and the connections between each room.
    Actor *rPlayerEntity = EntityManager::Fetch()->GetLastPlayerEntity();

    //--Attempt to center things to the viewport. The viewport is in the top left, with the UI being
    //  the right and bottom side of the screen.
    float cTranslateX = MAP_VIEW_SIZE_X * 0.35f;
    float cTranslateY = MAP_VIEW_SIZE_Y * 0.25f;
    cTranslateX = cTranslateX + (PandemoniumRoom::cSquareOuter * -0.5f);
    cTranslateY = cTranslateY + (PandemoniumRoom::cSquareOuter * -0.5f);
    glTranslatef(cTranslateX, cTranslateY, 0.0f);

    //--Zooming. Occurs after centering!
    if(xMapZoom != 0.0f)
    {
        glScalef(xMapZoom, xMapZoom, 1.0f);
    }

    //--Centering on the player. Occurs post-zoom.
    float cCenterX = 0.0f;
    float cCenterY = 0.0f;
    if(rPlayerEntity)
    {
        //--In non-animating mode, the player's position is always exactly their world position.
        if(!xActorIconsMove)
        {
            cCenterX = rPlayerEntity->GetWorldX() * -100.0f;
            cCenterY = rPlayerEntity->GetWorldY() * -100.0f;
        }
        //--The position may be dynamic, so we need to find the player's slot.
        else
        {
            //--Find the player and use their offsets.
            ActorIconPack *rPlayerIconPack = FindIconPack(rPlayerEntity->GetID());
            if(rPlayerIconPack)
            {
                cCenterX = (rPlayerIconPack->mCurX - PandemoniumRoom::cSquareOuter) * -1.0f;
                cCenterY = (rPlayerIconPack->mCurY - PandemoniumRoom::cSquareOuter) * -1.0f;
            }
            //--Icon pack not found. Use the world coordinates.
            else
            {
                cCenterX = rPlayerEntity->GetWorldX() * -100.0f;
                cCenterY = rPlayerEntity->GetWorldY() * -100.0f;
            }
        }
    }

    //--Reposition.
    glTranslatef(cCenterX, cCenterY, 0.0f);

    //--Get the map Z of the acting entity. If there isn't one, it defaults to zero.
    int tVisibleZ = 0;
    if(rPlayerEntity) tVisibleZ = rPlayerEntity->GetWorldZ();

    //--Storage. If the player changes Z heights, everything rechecks visibility.
    static int xLastVisibleZ = 0;
    if(xLastVisibleZ != tVisibleZ && xActorIconsMove)
    {
        xLastVisibleZ = tVisibleZ;
    }

    //--Underlays. May or may not render anything depending on game options.
    RenderUnderlay(tVisibleZ);

    //--Render.
    for(int i = 0; i < mRoomsTotal; i ++)
    {
        //--If the room is on the visible Z-level, render as normal.
        if(mRooms[i]->GetWorldZ() == tVisibleZ)
        {
            mRooms[i]->Render();
        }
        //--If not on the visible Z-level, we only render the entities, and only if xActorIconsMove is true.
        else if(xActorIconsMove)
        {
            mRooms[i]->RenderEntities(true);
        }
    }

    //--If flagged, render the actor icons through a list.
    if(xActorIconsMove) RenderActorIcons();

    //--Fog of war.
    RenderFogOverlay();

    //--Clean.
    glTranslatef(-cCenterX, -cCenterY, 0.0f);
    if(xMapZoom != 0.0f)
    {
        glScalef(1.0f / xMapZoom, 1.0f / xMapZoom, 1.0f);
    }
    glTranslatef(-cTranslateX, -cTranslateY, 0.0f);
    StarlightColor::ClearMixer();
}
void PandemoniumLevel::RenderBorder(float pLft, float pTop, float pRgt, float pBot, float pBrd, StarlightColor pBorderCol, StarlightColor pInteriorColor)
{
    //--Renders a standard border around the given area. Does not render if the alpha value for
    //  a given section of the colors is 0.0f, so if you're using this for masking, remember that!
    glDisable(GL_TEXTURE_2D);

    //--Border.
    if(pBorderCol.a > 0.0f && pBrd > 0.0f)
    {
        pBorderCol.SetAsMixer();
        glBegin(GL_QUADS);
            //--Top bar
            glVertex2f(pLft, pTop);
            glVertex2f(pRgt, pTop);
            glVertex2f(pRgt, pTop + pBrd);
            glVertex2f(pLft, pTop + pBrd);

            //--Bot bar
            glVertex2f(pLft, pBot - pBrd);
            glVertex2f(pRgt, pBot - pBrd);
            glVertex2f(pRgt, pBot);
            glVertex2f(pLft, pBot);

            //--Lft bar
            glVertex2f(pLft,        pTop + pBrd);
            glVertex2f(pLft + pBrd, pTop + pBrd);
            glVertex2f(pLft + pBrd, pBot - pBrd);
            glVertex2f(pLft,        pBot - pBrd);

            //--Rgt bar
            glVertex2f(pRgt - pBrd, pTop + pBrd);
            glVertex2f(pRgt,        pTop + pBrd);
            glVertex2f(pRgt,        pBot - pBrd);
            glVertex2f(pRgt - pBrd, pBot - pBrd);
        glEnd();
    }

    //--Window.
    if(pInteriorColor.a > 0.0f)
    {
        pInteriorColor.SetAsMixer();
        glBegin(GL_QUADS);
            glVertex2f(pLft + pBrd, pTop + pBrd);
            glVertex2f(pRgt - pBrd, pTop + pBrd);
            glVertex2f(pRgt - pBrd, pBot - pBrd);
            glVertex2f(pLft + pBrd, pBot - pBrd);
        glEnd();
    }

    //--Clean up.
    StarlightColor::ClearMixer();
    glEnable(GL_TEXTURE_2D);
}

//======================================= Pointer Routing =========================================
PandemoniumRoom *PandemoniumLevel::GetRoom(const char *pName)
{
    //--Error check.
    if(!pName) return NULL;

    //--Note: Only checks for rooms in the compiled list.
    for(int i = 0; i < mRoomsTotal; i ++)
    {
        if(!strcasecmp(pName, mRooms[i]->GetName())) return mRooms[i];
    }

    //--Failed to locate it.
    return NULL;
}
CorrupterMenu *PandemoniumLevel::GetCorrupterMenu()
{
    return mCorrupterMenu;
}

//====================================== Static Functions =========================================
PandemoniumLevel *PandemoniumLevel::Fetch()
{
    //--Note: May legally return NULL.
    RootLevel *rActiveLevel = MapManager::Fetch()->GetActiveLevel();
    if(rActiveLevel && rActiveLevel->IsOfType(POINTER_TYPE_PANDEMONIUMLEVEL)) return (PandemoniumLevel *)rActiveLevel;
    return NULL;
}

//========================================= Lua Hooking ===========================================
void PandemoniumLevel::HookToLuaState(lua_State *pLuaState)
{
    /* PL_BeginLevelConstruction(sLevelName)
       Begins level construction. The requested name will be given to a new level which will be
       pushed on the activity stack. */
    lua_register(pLuaState, "PL_BeginLevelConstruction", &Hook_PL_BeginLevelConstruction);

    /* PL_AddRoom(sScript)
       Creates a new PandemoniumRoom and pushes it on the activity stack. The script provided is
       then called to finish setup, and the activity stack is popped. */
    lua_register(pLuaState, "PL_AddRoom", &Hook_PL_AddRoom);

    /* PL_FinalizeRooms()
       Once all rooms have been added and finished setting up, this call will compile them into a
       playable level. */
    lua_register(pLuaState, "PL_FinalizeRooms", &Hook_PL_FinalizeRooms);

    /* PL_EndLevelConstruction()
       Once all level construction is done, pack the level up and send it to the MapManager. */
    lua_register(pLuaState, "PL_EndLevelConstruction", &Hook_PL_EndLevelConstruction);

    /* PL_PushRoom(sName)
       Pushes the activity stack with the given PandemoniumRoom, if it exists. Pushes NULL otherwise. */
    lua_register(pLuaState, "PL_PushRoom", &Hook_PL_PushRoom);

    /* PL_WriteToConsole(sText)
       PL_WriteToConsole(sText, bAllowExtraLine)
       PL_WriteToConsole(sText, fFontSize, fR, fG, fB, fA)
       PL_WriteToConsole(sText, fFontSize, fR, fG, fB, fA, bAllowExtraLine)
       Immediately writes the provided text to the console. If padding is enable, the second argument
       will block padding if false. */
    lua_register(pLuaState, "PL_WriteToConsole", &Hook_PL_WriteToConsole);

    /* PL_SetWaitForKeypress()
       PL_SetWaitForKeypress(sPostExecScript, iFiringCode)
       Tells the level to wait for the player to push a key. In addition, you can optionally specify
       a script and firing code for when the player does press a key. Game updates stop until the
       key is pressed. */
    lua_register(pLuaState, "PL_SetWaitForKeypress", &Hook_PL_SetWaitForKeypress);

    /* PL_SetSelfMenuScript(sScriptPath)
       Sets the script which is called when the player selects the self-actions menu. */
    lua_register(pLuaState, "PL_SetSelfMenuScript", &Hook_PL_SetSelfMenuScript);

    /* PL_SetSystemMenuScript(sScriptPath)
       Sets the script which is called when the player selects the system menu button. */
    lua_register(pLuaState, "PL_SetSystemMenuScript", &Hook_PL_SetSystemMenuScript);

    /* PL_SetMagicMenuScript(sScriptPath)
       Sets the script which is called when the player selects the magic menu button. If not set, the player can't use magic. */
    lua_register(pLuaState, "PL_SetMagicMenuScript", &Hook_PL_SetMagicMenuScript);

    /* PL_SetSystemLines(iLinesTotal)
       PL_SetSystemLines(iLine, sContents)
       Sets what the system lines in the top left corner read, or how many lines there are. Passing
       the same number of lines as before does nothing. Passing an out-of-range line fails safely. */
    lua_register(pLuaState, "PL_SetSystemLines", &Hook_PL_SetSystemLines);

    /* PL_IsPlayerPresent(sRoomName)
       Macro, pushes the room provided and returns true if a player entity is present. */
    lua_register(pLuaState, "PL_IsPlayerPresent", &Hook_PL_IsPlayerPresent);

    /* PL_RunEffectPulse()
       Orders all the Effects in the rooms to update. */
    lua_register(pLuaState, "PL_RunEffectPulse", &Hook_PL_RunEffectPulse);

    /* PL_BeginDamageAnimation(iActorID, iDamageHP, iDamageWP)
       Starts damage animation, though nothing will happen if the flag is toggled off. There is a damage
       flag for both HP and WP. To indicate your attack was not targetting HP/WP, pass -1 for that value.
       An attack which does 0 HP damage is a "Miss", and 0 WP damage is a "Resist". */
    lua_register(pLuaState, "PL_BeginDamageAnimation", &Hook_PL_BeginDamageAnimation);

    /* PL_AllocateUnderlays(iCount)
       Allocate underlays to be used in the 2D level. Does nothing for VisualLevels. Automatically deallocates
       any existing underlay data. A PandemoniumLevel must be available. */
    lua_register(pLuaState, "PL_AllocateUnderlays", &Hook_PL_AllocateUnderlays);

    /* PL_SetUnderlay(iSlot, iZLevel, fXOffset, fYOffset, sPathToImage)
       Sets an underlay after allocation. A PandemoniumLevel must be available. */
    lua_register(pLuaState, "PL_SetUnderlay", &Hook_PL_SetUnderlay);

    /* PL_RebuildFogInfo()
       Clears existing fog data and rebuilds it from scratch. Should be called only once at startup
       as it will automatically occur with each new turn. */
    lua_register(pLuaState, "PL_RebuildFogInfo", &Hook_PL_RebuildFogInfo);
}

//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
int Hook_PL_BeginLevelConstruction(lua_State *L)
{
    //PL_BeginLevelConstruction(sLevelName)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("PL_BeginLevelConstruction");

    //--Push on the activity stack.
    PandemoniumLevel *nLevel = new PandemoniumLevel();
    DataLibrary::Fetch()->PushActiveEntity(nLevel);

    return 0;
}
int Hook_PL_AddRoom(lua_State *L)
{
    //PL_AddRoom(sScript)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("PL_AddRoom");

    //--Who to register it to.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    PandemoniumLevel *rLevel = (PandemoniumLevel *)rDataLibrary->rActiveObject;
    if(!rDataLibrary->IsActiveValid(POINTER_TYPE_PANDEMONIUMLEVEL)) return LuaTypeError("PL_AddRoom");

    //--Create and push the room.
    PandemoniumRoom *nRoom = new PandemoniumRoom();
    rDataLibrary->PushActiveEntity(nRoom);

    //--Run the script.
    LuaManager::Fetch()->ExecuteLuaFile(lua_tostring(L, 1));

    //--Register the room.
    rLevel->RegisterRoom(nRoom);

    //--Clean up.
    rDataLibrary->PopActiveEntity();

    return 0;
}
int Hook_PL_FinalizeRooms(lua_State *L)
{
    //PL_FinalizeRooms()

    //--Check the activity stack.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    PandemoniumLevel *rLevel = (PandemoniumLevel *)rDataLibrary->rActiveObject;
    if(!rDataLibrary->IsActiveValid(POINTER_TYPE_PANDEMONIUMLEVEL)) return LuaTypeError("PL_FinalizeRooms");

    rLevel->FinalizeRooms();

    return 0;
}
int Hook_PL_EndLevelConstruction(lua_State *L)
{
    //PL_EndLevelConstruction()

    //--Check the activity stack.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    if(!rDataLibrary->IsActiveValid(POINTER_TYPE_PANDEMONIUMLEVEL)) return LuaTypeError("PL_EndLevelConstruction");

    //--Set as the active level.
    MapManager::Fetch()->ReceiveLevel((PandemoniumLevel *)rDataLibrary->PopActiveEntity());

    return 0;
}
int Hook_PL_PushRoom(lua_State *L)
{
    //PL_PushRoom(sName)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("PL_PushRoom");

    //--Get the level itself. Check for errors.
    PandemoniumLevel *rActiveLevel = PandemoniumLevel::Fetch();
    if(!rActiveLevel)
    {
        DebugManager::ForcePrint("PL_PushRoom - Error, no active PandemoniumLevel.\n");
        DataLibrary::Fetch()->PushActiveEntity();
    }
    //--Push it.
    else
    {
        DataLibrary::Fetch()->PushActiveEntity(rActiveLevel->GetRoom(lua_tostring(L, 1)));
    }

    return 0;
}
int Hook_PL_WriteToConsole(lua_State *L)
{
    //PL_WriteToConsole(sText)
    //PL_WriteToConsole(sText, bAllowExtraLine)
    //PL_WriteToConsole(sText, fFontSize, fR, fG, fB, fA)
    //PL_WriteToConsole(sText, fFontSize, fR, fG, fB, fA, bAllowExtraLine)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("PL_WriteToConsole");

    //--Write.
    if(tArgs < 6)
    {
        PandemoniumLevel::AppendToConsoleStatic(lua_tostring(L, 1));
    }
    //--Full case.
    else
    {
        PandemoniumLevel::AppendToConsoleStatic(lua_tostring(L, 1), lua_tonumber(L, 2), StarlightColor::MapRGBAF(lua_tonumber(L, 3), lua_tonumber(L, 4), lua_tonumber(L, 5), lua_tonumber(L, 6)));
    }

    //--Padding.
    if(tArgs == 1 || (tArgs == 2 && lua_toboolean(L, 2) == true) || tArgs == 6 || (tArgs == 7 && lua_toboolean(L, 7) == true))
    {
        if(Actor::xPrintPaddingLines) PandemoniumLevel::AppendToConsoleStatic(" ");
    }

    return 0;
}
int Hook_PL_SetWaitForKeypress(lua_State *L)
{
    //PL_SetWaitForKeypress()
    //PL_SetWaitForKeypress(sPostExecScript, iFiringCode)
    int tArgs = lua_gettop(L);

    PandemoniumLevel *rActiveLevel = PandemoniumLevel::Fetch();
    if(!rActiveLevel)
    {
        DebugManager::ForcePrint("PL_SetWaitForKeypress - Error, no active PandemoniumLevel.\n");
        return 0;
    }

    //--No arguments (or one argument): Just flag to wait.
    if(tArgs < 2)
    {
        rActiveLevel->WaitOnKeypress();
    }
    //--Two or more args: Set a post-exec case.
    else
    {
        rActiveLevel->WaitOnKeypress(lua_tostring(L, 1), lua_tointeger(L, 2));
    }
    return 0;
}
int Hook_PL_SetMagicMenuScript(lua_State *L)
{
    //PL_SetMagicMenuScript(sMagicScript)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("PL_SetMagicFlag");

    //--Check.
    PandemoniumLevel *rActiveLevel = PandemoniumLevel::Fetch();
    if(!rActiveLevel)
    {
        DebugManager::ForcePrint("PL_SetMagicFlag - Error, no active PandemoniumLevel.\n");
        return 0;
    }

    //--Set
    rActiveLevel->SetMagicScript(lua_tostring(L, 1));

    return 0;
}
int Hook_PL_SetSelfMenuScript(lua_State *L)
{
    //PL_SetSelfMenuScript(sScript)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("PL_SetSelfMenuScript");

    PandemoniumLevel *rActiveLevel = PandemoniumLevel::Fetch();
    if(!rActiveLevel)
    {
        DebugManager::ForcePrint("PL_SetSelfMenuScript - Error, no active PandemoniumLevel.\n");
        return 0;
    }

    //--Set
    rActiveLevel->SetSelfMenuScript(lua_tostring(L, 1));

    return 0;
}
int Hook_PL_SetSystemMenuScript(lua_State *L)
{
    //PL_SetSystemMenuScript(sScript)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("PL_SetSystemMenuScript");

    PandemoniumLevel *rActiveLevel = PandemoniumLevel::Fetch();
    if(!rActiveLevel)
    {
        DebugManager::ForcePrint("PL_SetSystemMenuScript - Error, no active PandemoniumLevel.\n");
        return 0;
    }

    //--Set
    rActiveLevel->SetSystemMenuScript(lua_tostring(L, 1));

    return 0;
}
int Hook_PL_SetSystemLines(lua_State *L)
{
    //PL_SetSystemLines(iLinesTotal)
    //PL_SetSystemLines(iLine, sContents)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("PL_SetSystemLines");

    //--Get and check.
    PandemoniumLevel *rActiveLevel = PandemoniumLevel::Fetch();
    if(!rActiveLevel)
    {
        DebugManager::ForcePrint("PL_SetSystemLines - Error, no active PandemoniumLevel.\n");
        return 0;
    }

    //--1 argument: Set lines.
    if(tArgs == 1)
    {
        rActiveLevel->SetTotalSystemLines(lua_tointeger(L, 1));
    }
    //--Modify an existing line.
    else
    {
        rActiveLevel->SetSystemLine(lua_tointeger(L, 1), lua_tostring(L, 2));
    }
    return 0;
}
int Hook_PL_IsPlayerPresent(lua_State *L)
{
    //PL_IsPlayerPresent(sRoomName)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) { LuaArgError("PL_IsPlayerPresent"); lua_pushboolean(L, false); return 1; }

    //--Get and check.
    PandemoniumLevel *rActiveLevel = PandemoniumLevel::Fetch();
    if(!rActiveLevel)
    {
        DebugManager::ForcePrint("PL_IsPlayerPresent - Error, no active PandemoniumLevel.\n");
        lua_pushboolean(L, false);
        return 1;
    }

    //--Get the level.
    PandemoniumRoom *rRoom = rActiveLevel->GetRoom(lua_tostring(L, 1));
    if(!rRoom)
    {
        DebugManager::ForcePrint("PL_IsPlayerPresent - Error, no room %s in active level.\n", lua_tostring(L, 1));
        lua_pushboolean(L, false);
        return 1;
    }

    //--Return case.
    lua_pushboolean(L, (rRoom->IsPlayerPresent() != NULL));
    return 1;
}
int Hook_PL_RunEffectPulse(lua_State *L)
{
    //PL_RunEffectPulse()
    PandemoniumLevel::Fetch()->EffectPulse();
    return 0;
}
int Hook_PL_BeginDamageAnimation(lua_State *L)
{
    //PL_BeginDamageAnimation(iActorID, iDamageHP, iDamageWP)
    int tArgs = lua_gettop(L);
    if(tArgs < 3) return LuaArgError("PL_BeginDamageAnimation");

    //--Doesn't even run checks if the flag is toggled off.
    if(!PandemoniumLevel::xAnimateDamage) return 0;

    //--Get and check.
    PandemoniumLevel *rActiveLevel = PandemoniumLevel::Fetch();
    if(!rActiveLevel)
    {
        DebugManager::ForcePrint("PL_BeginDamageAnimation - Error, no active PandemoniumLevel.\n");
        return 0;
    }

    //--Resolve the Actor from its ID.
    Actor *rActor = (Actor *)EntityManager::Fetch()->GetEntityI(lua_tointeger(L, 1));
    if(!rActor || !rActor->IsOfType(POINTER_TYPE_ACTOR))
    {
        DebugManager::ForcePrint("PL_BeginDamageAnimation - Error, no Actor with ID %i.\n", lua_tointeger(L, 1));
        return 0;
    }

    //--Call.
    rActiveLevel->BeginDamageAnimation(rActor, lua_tointeger(L, 2), lua_tointeger(L, 3));
    return 0;
}
int Hook_PL_AllocateUnderlays(lua_State *L)
{
    //PL_AllocateUnderlays(iCount)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("PL_AllocateUnderlays");

    //--Get and check.
    PandemoniumLevel *rActiveLevel = PandemoniumLevel::Fetch();
    if(!rActiveLevel)
    {
        DebugManager::ForcePrint("PL_AllocateUnderlays - Error, no active PandemoniumLevel.\n");
        return 0;
    }

    //--Call.
    rActiveLevel->AllocateUnderlays(lua_tointeger(L, 1));
    return 0;
}
int Hook_PL_SetUnderlay(lua_State *L)
{
    //PL_SetUnderlay(iSlot, iZLevel, fXOffset, fYOffset, sPathToImage)
    int tArgs = lua_gettop(L);
    if(tArgs < 5) return LuaArgError("PL_SetUnderlay");

    //--Get and check.
    PandemoniumLevel *rActiveLevel = PandemoniumLevel::Fetch();
    if(!rActiveLevel)
    {
        DebugManager::ForcePrint("PL_SetUnderlay - Error, no active PandemoniumLevel.\n");
        return 0;
    }

    //--Call.
    rActiveLevel->SetUnderlay(lua_tointeger(L, 1), lua_tointeger(L, 2), lua_tonumber(L, 3), lua_tonumber(L, 4), lua_tostring(L, 5));
    return 0;
}
int Hook_PL_RebuildFogInfo(lua_State *L)
{
    //PL_RebuildFogInfo()

    //--Get and check.
    PandemoniumLevel *rActiveLevel = PandemoniumLevel::Fetch();
    if(!rActiveLevel)
    {
        DebugManager::ForcePrint("PL_RebuildFogInfo - Error, no active PandemoniumLevel.\n");
        return 0;
    }

    //--Call.
    rActiveLevel->RebuildFogInfo();
    return 0;
}
