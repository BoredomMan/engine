//--Base
#include "PandemoniumRoom.h"

//--Classes
//--CoreClasses
//--Definitions
//--GUI
//--Libraries
//--Managers
#include "DebugManager.h"

PathInstructions *PandemoniumRoom::GetPathTo(const char *pRoomTarget)
{
    //--Returns the pathing information to the given room. Can return NULL if the room didn't exist.
    //  Will also return NULL if the target can't be pathed to for some reason.
    if(!pRoomTarget) return NULL;

    //--Get the pathing pack.
    PathInstructions *rPathPack = GetPathingPackS(pRoomTarget);
    if(!rPathPack || rPathPack->mShortestPath == -1) return NULL;

    //--Legal, so return it safely.
    return rPathPack;
}
void PandemoniumRoom::ClearPathing()
{
    //--Deallocate and clear any outstanding path information.
    for(int i = 0; i < mShortestPathSize; i ++)
    {
        free(mShortestPaths[i].mTargetName);
        free(mShortestPaths[i].mPathList);
    }
    free(mShortestPaths);

    //--Clear.
    mShortestPathSize = 0;
    mShortestPaths = NULL;
}
void PandemoniumRoom::BuildPathing(int pListSize, PandemoniumRoom **pRoomList)
{
    //--Provided with a list of all rooms, creates a path list for all of them that shows the
    //  shortest possible path between them. Note that if two paths have the exact same length
    //  one will be selected for storage and the others ignored.
    //--This should be called after the rooms have all been compiled together. It is legal for
    //  the list to contain this room (the instruction list will be 0 long).
    //--It is expected that the pRoomList is *full* and *valid*, it is not checked.
    ClearPathing();

    //--Error check.
    if(pListSize < 1 || !pRoomList) return;

    //--Allocate pathing information for all rooms.
    mShortestPathSize = pListSize;
    SetMemoryData(__FILE__, __LINE__);
    mShortestPaths = (PathInstructions *)starmemoryalloc(sizeof(PathInstructions) * mShortestPathSize);

    //--Initialize all the packs for the recursive pather.
    for(int i = 0; i < mShortestPathSize; i ++)
    {
        //--Reset.
        mShortestPaths[i].Reset();

        //--Associate this room with the real version.
        mShortestPaths[i].rAttachedRoom = pRoomList[i];
        ResetString(mShortestPaths[i].mTargetName, pRoomList[i]->GetName());
    }

    //--Run the recursive pather.
    DebugManager::PushPrint(false, "Running pather!\n");
    RecursivePather(this, 0, NULL);
    DebugManager::PopPrint("Done.\n");
    /*
    if(!strcasecmp(mLocalName, "Main Hall"))
    {
        fprintf(stderr, "Path listing for Main Hall:\n");
        for(int i = 0; i < mShortestPathSize; i ++)
        {
            fprintf(stderr, " %s: %i\n", mShortestPaths[i].mTargetName, mShortestPaths[i].mListSize);
        }
    }*/

    //--Debug.
    int tLongestPath = 0;
    for(int i = 0; i < mShortestPathSize; i ++)
    {
        if(mShortestPaths[i].mShortestPath > tLongestPath) tLongestPath = mShortestPaths[i].mShortestPath;
    }
    DebugManager::Print("%s finishes building path nodes. Longest path: %i\n", mLocalName, tLongestPath);

    //--Debug: Flip this flag off.
    xFirstPath = false;
}
bool PandemoniumRoom::RecursivePather(PandemoniumRoom *pCurrentRoom, int pMovesSoFar, int *pMoveList)
{
    //--Recursive pathing function. Constructs an instruction list that leads back to the room
    //  which originated the call. The room in question is to be passed in, and it can legally
    //  be the caller (on the first pulse).
    //--Error check:
    if(!pCurrentRoom) return false;

    //--Get the pathing pack associated with this room.
    PathInstructions *rPack = GetPathingPackP(pCurrentRoom);
    if(!rPack) return false;
    //DebugManager::Print("Room %p - Pack %s %p\n", pCurrentRoom, pCurrentRoom->GetName(), rPack);

    //--If the move distance is -1, then no pather has reached it yet. Immediately copy.
    if(rPack->mShortestPath == -1)
    {
        //DebugManager::Print("First path created.\n");
        rPack->mShortestPath = pMovesSoFar;
        rPack->CopyPathList(pMovesSoFar, pMoveList);
        if(xFirstPath) pCurrentRoom->mPathDistance = pMovesSoFar;
    }
    //--Otherwise, only copy if the moves is shorter than the previous list. If this happens,
    //  the new path is shorter, so use that instead.
    else if(rPack->mShortestPath > pMovesSoFar)
    {
        //DebugManager::Print("Shorter path located.\n");
        rPack->mShortestPath = pMovesSoFar;
        rPack->CopyPathList(pMovesSoFar, pMoveList);
        if(xFirstPath) pCurrentRoom->mPathDistance = pMovesSoFar;
    }
    //--If neither case is true, then we're done. There is already a shorter/equidistant path.
    else
    {
        //DebugManager::Print("Done, shorter path.\n");
        return true;
    }

    //--Increment the moves count.
    pMovesSoFar ++;

    //--For each of the connections, create a new list and run the recursive pather on that.
    for(int i = 0; i < NAV_TOTAL; i ++)
    {
        //--Get the connection.
        PandemoniumRoom *rConnection = pCurrentRoom->GetConnection(i);

        //--If there's no connection here, skip it.
        if(!rConnection) continue;
        //DebugManager::PushPrint(true, "Recurse %s\n", rConnection->GetName());

        //--Create a new list.
        SetMemoryData(__FILE__, __LINE__);
        int *tMoveList = (int *)starmemoryalloc(sizeof(int) * pMovesSoFar);
        for(int p = 0; p < pMovesSoFar - 1; p ++) tMoveList[p] = pMoveList[p];

        //--The last movement is the NAV_ movement.
        tMoveList[pMovesSoFar-1] = i;

        //--Run the recursive pather.
        RecursivePather(rConnection, pMovesSoFar, tMoveList);

        //--Clean.
        free(tMoveList);
        //DebugManager::PopPrint("Done.\n");
    }

    //--Return true to indicate something happened.
    return true;
}
PathInstructions *PandemoniumRoom::GetPathingPackS(const char *pName)
{
    //--Finds and returns the pathing pack with the associated name. May return NULL on error.
    if(!pName) return NULL;

    //--Search.
    for(int i = 0; i < mShortestPathSize; i ++)
    {
        if(!strcmp(pName, mShortestPaths[i].mTargetName)) return &mShortestPaths[i];
    }

    //--Unable to find that room.
    fprintf(stderr, "Unable to find %s\n", pName);
    return NULL;
}
PathInstructions *PandemoniumRoom::GetPathingPackP(PandemoniumRoom *pRoom)
{
    //--Find and returns the pathing pack with the associated room.
    if(!pRoom) return NULL;

    //--Search.
    for(int i = 0; i < mShortestPathSize; i ++)
    {
        if(mShortestPaths[i].rAttachedRoom == pRoom) return &mShortestPaths[i];
    }

    //--Unable to find that room.
    fprintf(stderr, "Unable to find %p %s\n", pRoom, pRoom->GetName());
    return NULL;
}
