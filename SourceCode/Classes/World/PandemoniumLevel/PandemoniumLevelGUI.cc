//--Base
#include "PandemoniumLevel.h"

//--Classes
#include "Actor.h"
#include "CorrupterMenu.h"
#include "PandemoniumRoom.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarFont.h"

//--Definitions
#include "HitDetection.h"

//--GUI
//--Libraries
//--Managers
#include "EntityManager.h"

//--System
void PandemoniumLevel::SetupButtons()
{
    //--Sets the button positions and rendering data. Button positions are all constants based on the nav UI images.
    if(mHasSetupButtons) return;
    mHasSetupButtons = true;

    //--Set.
    mVirtualButtons[0].Set(INSTRUCTION_MOVEUP,   127, 237, Images.Data.rUp);
    mVirtualButtons[1].Set(INSTRUCTION_MOVEDOWN, 127, 299, Images.Data.rDown);
    mVirtualButtons[2].Set(INSTRUCTION_MOVENORTH, 96, 228, Images.Data.rNorth);
    mVirtualButtons[3].Set(INSTRUCTION_MOVEEAST, 136, 268, Images.Data.rEast);
    mVirtualButtons[4].Set(INSTRUCTION_MOVESOUTH, 96, 308, Images.Data.rSouth);
    mVirtualButtons[5].Set(INSTRUCTION_MOVEWEST,  56, 268, Images.Data.rWest);
    mVirtualButtons[6].Set(INSTRUCTION_SKIPTURN,  96, 268, Images.Data.rSkip);
}

//--Core Methods
void PandemoniumLevel::RecheckDisabledCases()
{
    //--Check which buttons are available for the player to use. All buttons are disabled if it's
    //  not the player's turn.
    for(int i = 0; i < VIRTUAL_BUTTONS_TOTAL; i ++)
    {
        mVirtualButtons[i].mIsDisabled = true;
    }

    //--Reset this flag. The context menu cannot be displayed if it's not the player's turn.
    bool tOldContextFlag = mShowContextMenu;
    mShowContextMenu = false;

    //--Get the player. Is it their turn?
    Actor *rActor = EntityManager::Fetch()->GetActingEntity();
    Actor *rPlayer = EntityManager::Fetch()->GetLastPlayerEntity();
    if(!rActor || !rPlayer || rActor != rPlayer) return;

    //--Turn skipping is always on (if it's their turn).
    mVirtualButtons[6].mIsDisabled = false;

    //--If we got this far, the context menu can legally be open.
    mShowContextMenu = tOldContextFlag;

    //--Okay, now get the room they're in.
    PandemoniumRoom *rActiveRoom = rPlayer->GetCurrentRoom();
    if(!rActiveRoom) return;

    //--Now go by the room's own logic to check its connections.
    if(rActiveRoom->GetConnection(NAV_UP))    mVirtualButtons[0].mIsDisabled = false;
    if(rActiveRoom->GetConnection(NAV_DOWN))  mVirtualButtons[1].mIsDisabled = false;
    if(rActiveRoom->GetConnection(NAV_NORTH)) mVirtualButtons[2].mIsDisabled = false;
    if(rActiveRoom->GetConnection(NAV_EAST))  mVirtualButtons[3].mIsDisabled = false;
    if(rActiveRoom->GetConnection(NAV_SOUTH)) mVirtualButtons[4].mIsDisabled = false;
    if(rActiveRoom->GetConnection(NAV_WEST))  mVirtualButtons[5].mIsDisabled = false;
}
int PandemoniumLevel::EmulateClick(int pXStart, int pYStart, int pXEnd, int pYEnd)
{
    //--The GUI on the screen is not handled by the GUI, but by this class. Therefore, clicks
    //  can be emulated directly through it instead of the GUI.
    //--Returns the instruction of the matching button, if there was one. If not, returns the
    //  "No Instruction" code, meaning we didn't handle this click. Other objects may still
    //  handle the click!
    //--First, fail if the CorrupterMenu is onscreen and therefore handles the clicks.
    if(mCorrupterMenu->IsVisible()) return INSTRUCTION_NOENTRY;

    //--Button size is 32 pixels.
    int cBtnSize = 32;

    //--Check the buttons.
    for(int i = 0; i < VIRTUAL_BUTTONS_TOTAL; i ++)
    {
        //--If the button is disabled, do nothing.
        if(mVirtualButtons[i].mIsDisabled) continue;

        //--Check if the click is inside the button.
        if(pXStart >= mVirtualButtons[i].mX && pXStart <= mVirtualButtons[i].mX + cBtnSize && pYStart >= mVirtualButtons[i].mY && pYStart <= mVirtualButtons[i].mY + cBtnSize &&
           pXEnd   >= mVirtualButtons[i].mX && pXEnd   <= mVirtualButtons[i].mX + cBtnSize && pYEnd   >= mVirtualButtons[i].mY && pYEnd   <= mVirtualButtons[i].mY + cBtnSize)
        {
            return mVirtualButtons[i].mInstruction;
        }
    }

    //--All button checks failed, so return "No Instruction".
    return INSTRUCTION_NOENTRY;
}

//--Rendering
void PandemoniumLevel::RenderNavigation()
{
    //--Renders the navigation buttons independent of map scaling. These are all in a structure for
    //  super easy rendering!
    if(!Images.Data.rNavBase) return;

    //--Constants.
    int cNavLft =  57;
    int cNavTop = 229;
    Images.Data.rNavBase->Draw(cNavLft, cNavTop);

    //--Loop!
    for(int i = 0; i < VIRTUAL_BUTTONS_TOTAL; i ++)
    {
        //--Fail if the button graphics are invalid.
        if(!mVirtualButtons[i].rRenderImg) continue;
        float tVisPercent = 0.25f + ((mVirtualButtons[i].mVisTimer / (float)PL_VIS_TIMER_MAX) * 0.75f);
        StarlightColor::SetMixer(tVisPercent, tVisPercent, tVisPercent, 1.0f);
        mVirtualButtons[i].rRenderImg->Draw(mVirtualButtons[i].mX, mVirtualButtons[i].mY);
    }
    StarlightColor::ClearMixer();
}
