//--[Skybox]
//--A skybox! Can store its own images or references, if specified. Can render on the stencil buffer or not,
//  depending on the flags set.

#pragma once

//--[Includes]
#include "Definitions.h"
#include "Structures.h"

//--[Local Structures]
//--[Local Definitions]
#define SKYBOX_N 0
#define SKYBOX_E 1
#define SKYBOX_S 2
#define SKYBOX_W 3
#define SKYBOX_U 4
#define SKYBOX_D 5
#define SKYBOX_TOTAL 6

//--[Classes]
class Skybox : public RootObject
{
    private:
    //--System
    //--Images
    bool mOwnsImagesFlags[SKYBOX_TOTAL];
    SugarBitmap *rCubeImages[SKYBOX_TOTAL];

    protected:

    public:
    //--System
    Skybox();
    virtual ~Skybox();

    //--Public Variables
    //--Property Queries
    //--Manipulators
    void SetImage(int pSlot, bool pTakeOwnership, SugarBitmap *pImage);
    void SetImage(int pSlot, const char *pDLPath);
    void SetImagesFromPattern(const char *pPattern);

    //--Core Methods
    private:
    //--Private Core Methods
    public:
    //--Update
    //--File I/O
    //--Drawing
    void Render();
    void RenderWithStencil(uint32_t pStencilFlag);

    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions

