//--[ContextMenu]
//--The menu that appears over the PandemoniumLevel's GUI when the player is interacting with
//  something. The menu will be populated with commands by an external source, it only handles
//  the execution of those commands.

#pragma once

//--[Includes]
#include "Definitions.h"
#include "Structures.h"

//--[Local Structures]
//--Represents an option that appears on the context menu. Can run either a lua script or a function pointer.
//  If the structure is going to own its own data for the function pointer, that data must have been allocated
//  via a simple starmemoryalloc() call, it does not handle complex destructors.
typedef struct ContextOption
{
    //--Display.
    char *mString;

    //--Lua execution.
    bool mRunLua;
    char *mLuaScript;

    //--Function Pointer.
    bool mRunFunction;
    LogicFnPtr rLogicFunction;

    //--Data.
    bool mOwnsData;
    void *mData;

    //--Setup Functions
    void Setup(const char *pString, const char *pLuaScript)
    {
        mString = InitializeString(pString);
        mRunLua = true;
        mLuaScript = InitializeString(pLuaScript);
        mRunFunction = false;
        rLogicFunction = NULL;
        mData = NULL;
    }
    void Setup(const char *pString, LogicFnPtr pLogicFn, bool pOwnsData, void *pData)
    {
        mString = InitializeString(pString);
        mRunLua = false;
        mLuaScript = NULL;
        mRunFunction = true;
        rLogicFunction = pLogicFn;
        mOwnsData = pOwnsData;
        mData = pData;
    }

    //--Deletion
    static void DeleteThis(void *pPtr)
    {
        ContextOption *rPtr = (ContextOption *)pPtr;
        free(rPtr->mString);
        free(rPtr->mLuaScript);
        if(rPtr->mOwnsData) free(rPtr->mData);
    }

}ContextOption;

//--[Local Definitions]
//--[Classes]
class ContextMenu
{
    private:
    //--System
    bool mHandledUpdate;
    bool mHasPendingClose;

    //--Rendering
    float mIndent;
    float mTextSize;
    float mTextSpacing;
    StarlightColor mTextColor;
    StarlightColor mBorderColor;
    StarlightColor mInnerColor;
    SugarFont *rRenderFont;

    //--Position
    float mLft, mTop;
    float mWid, mHei;

    //--Storage
    int mSelectionCursor;
    SugarLinkedList *mCommandList;

    protected:

    public:
    //--System
    ContextMenu();
    ~ContextMenu();

    //--Public Variables
    //--Property Queries
    bool HasContents();
    bool HasPendingClose();
    bool HandledUpdate();
    float GetWidth();
    float GetHeight();

    //--Manipulators
    void ClearMenu();
    void RegisterLuaCommand(const char *pString, const char *pScript);
    void RegisterFunctionCommand(const char *pString, LogicFnPtr pFunction);
    void RegisterCommand(ContextOption *pOption);
    void SetPosition(float pLft, float pTop);
    void Reset();

    //--Menu Functions
    static void ExamineTargetActor(void *pTargetPtr);
    static void ExamineTargetItem(void *pTargetPtr);
    static void ExamineTargetContainer(void *pTargetPtr);
    static void PickUpItem(void *pTargetPtr);
    static void UseGroundItem(void *pTargetPtr);
    static void AttackTargetActor(void *pTargetPtr);
    static void ExecuteLuaContextOption(void *pStoragePackPtr);
    static void OpenContainer(void *pTargetPtr);

    //--Core Methods
    void PopulateAroundActor(Actor *pTarget);
    void PopulateAroundItem(InventoryItem *pItem, bool pActorCantPickup);
    void PopulateAroundContainer(WorldContainer *pContainer, bool pActorCantPickup);

    private:
    //--Private Core Methods
    void CalculateAndResize();

    public:
    //--Update
    void Update(int pMouseX, int pMouseY);

    //--File I/O
    //--Drawing
    void Render();

    //--Pointer Routing
    //--Static Functions
    static void ExecuteContextOption(ContextOption *pOption);

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions

