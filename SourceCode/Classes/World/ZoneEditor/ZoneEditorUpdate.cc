//--Base
#include "ZoneEditor.h"

//--Classes
#include "ZoneNode.h"

//--CoreClasses
#include "SugarLinkedList.h"

//--Definitions
#include "HitDetection.h"

//--GUI
//--Libraries
//--Managers
#include "ControlManager.h"

//--Contains functions pertaining to the update cycle, which can get somewhat hairy due to the branching paths.
//  All the functions here are used exclusively during the update cycle!

//--[Entry Function]
bool ZoneEditor::Update()
{
    //--Updates the ZoneEditor's status based on mouse clicks and hotkeys. It should be up to the caller
    //  to determine if we need to update, the ZE does not internally track if it is visible or not.
    //--Returns true if keyboard input is being caught and we should ignore non-mouse controls.
    float tMouseX, tMouseY, tMouseZ;
    ControlManager *rControlManager = ControlManager::Fetch();
    rControlManager->GetMouseCoordsF(tMouseX, tMouseY, tMouseZ);

    //--Scrolling. This occurs regardless of mode.
    if(mLastMouseZ != tMouseZ)
    {
        mNodeCountOffset += (mLastMouseZ - tMouseZ);
        if(mNodeCountOffset + 25 >= mNodeList->GetListSize()) mNodeCountOffset = mNodeList->GetListSize() - 25;
        if(mNodeCountOffset < 0) mNodeCountOffset = 0;
    }
    mLastMouseZ = tMouseZ;

    //--Set node/command highlighting.
    rHighlightedNode = GetHighlightedNode(tMouseX, tMouseY);
    rHighlightedCommand = GetHighlightedCommand(tMouseX, tMouseY);

    //--Confirmation dialog. Can stop update if it handles the input.
    if(HandleConfirmationDialog()) return true;

    //--Check if we're editing node facing.
    if(HandleFacingUpdate()) return true;

    //--Check if we're editing automatic transitions.
    if(HandleAutoTransitionUpdate()) return true;

    //--Check if we're deleting a node.
    if(HandleNodeDeletionUpdate()) return true;

    //--Renaming case.
    if(HandleRenameNodeUpdate()) return true;

    //--If we got this far, it means nobody else handled the update, so nothing special is occurring. Do the normal update.
    HandleNormalUpdate();
    return false;
}

//--[Sub-Functions]
bool ZoneEditor::HandleConfirmationDialog()
{
    //--Handles the update of the confirmation dialog, which consists of two commands, Yes or No.
    //--Returns true if it handled the update, false if not.
    if(!mShowConfirmDialog) return false;

    //--Setup.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Left-click checking.
    if(rControlManager->IsFirstPress("MouseLft"))
    {
        //--If a command is highlighted, execute it. This gets priority over node changing. It also
        //  always unsets the confirmation dialogue, regardless of yes/no case.
        if(rHighlightedCommand && rHighlightedCommand->mRenderFlag == ZE_RENDER_CONFIRM)
        {
            //--Flags.
            mShowConfirmDialog = false;
            ExecuteCommand(rHighlightedCommand->mFunctionCode);

            //--We handled the update for the last time.
            return true;
        }
    }

    //--We handled the update, but didn't do anything.
    return true;
}
bool ZoneEditor::HandleFacingUpdate()
{
    //--Handles the update if editing the facing connections for a node. Does nothing otherwise.
    //--Returns true upon handling the update, false if not.
    if(mIsNodeSelectionMode == ZE_NO_FACING_EDIT) return false;

    //--Setup.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Left-click checking.
    if(rControlManager->IsFirstPress("MouseLft"))
    {
        //--Selecting a facing?
        if(mIsNodeSelectionMode == ZE_MUST_SELECT_FACING)
        {
            //--If we clicked something, go to the next segment.
            if(rHighlightedCommand != NULL)
            {
                rSelectedFacingCommand = rHighlightedCommand;
                mIsNodeSelectionMode = ZE_MUST_SELECT_NODE;
            }
            //--We clicked off the nodes, cancel the update.
            else
            {
                mIsNodeSelectionMode = ZE_NO_FACING_EDIT;
            }
        }
        //--Selecting a node? A successful selection sends the information down.
        else
        {
            //--Get the node. It contains a UniqueID.
            if(rHighlightedNode && rActiveNode)
            {
                //--Which facing to edit?
                int tFacingZero = mCommandsList->GetSlotOfElementByPtr(FindCommandPack("Facing A"));
                int tFacingThis = mCommandsList->GetSlotOfElementByPtr(rSelectedFacingCommand);

                //--Set.
                fprintf(stderr, "Nodes: %i %i - %i\n", tFacingThis, tFacingZero, tFacingThis - tFacingZero);
                rActiveNode->SetFacingConnection(tFacingThis - tFacingZero, rHighlightedNode->GetNodeID());

                //--Unset the flag.
                mIsNodeSelectionMode = ZE_NO_FACING_EDIT;
            }
            //--No node was highlighted, cancel.
            else
            {
                mIsNodeSelectionMode = ZE_NO_FACING_EDIT;
            }
        }
    }

    //--In all cases, we handled the update.
    return true;
}
bool ZoneEditor::HandleAutoTransitionUpdate()
{
    //--Handles the case where the Node Editor is selecting nodes for automatic transition cases.
    //--Returns true if we handled the update, false if not.
    if(mIsAutoTransitionMode == ZE_AUTO_NONE) return false;

    //--Setup.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Left-click checking.
    if(rControlManager->IsFirstPress("MouseLft"))
    {
        //--Selecting the source node?
        if(mIsAutoTransitionMode == ZE_AUTO_SELECT_SOURCE_NODE)
        {
            //--User clicked a node, store the node's ID.
            if(rHighlightedNode != NULL)
            {
                mOriginNodeID = rHighlightedNode->GetNodeID();
                mIsAutoTransitionMode = ZE_AUTO_SELECT_DEST_NODE;
            }
            //--User clicked on something else, cancel.
            else
            {
                mOriginNodeID = 0;
                mIsAutoTransitionMode = ZE_AUTO_NONE;
            }
        }
        //--Selecting the destination node?
        else if(mIsAutoTransitionMode == ZE_AUTO_SELECT_DEST_NODE)
        {
            //--In all cases, cancel out of this.
            mIsAutoTransitionMode = ZE_AUTO_NONE;

            //--Attempt to add the transition case. It will fail internally if something was illegal.
            if(rHighlightedNode && rActiveNode)
            {
                //--We need to check if the transition worked. If it did, add a new command to the editor.
                if(rActiveNode->AddAutoTransition(mOriginNodeID, rHighlightedNode->GetNodeID()))
                {
                    AddTransitionToNodeEditor();
                }
            }
        }
    }

    //--We handled the update.
    return true;
}
bool ZoneEditor::HandleNodeDeletionUpdate()
{
    //--Handles the update case where the user is deleting a node.
    //--Returns true if it handled the update, false if not.
    if(mIsNodeDeletionMode == ZE_NDEL_NONE) return false;

    //--Left-click checking.
    ControlManager *rControlManager = ControlManager::Fetch();
    if(rControlManager->IsFirstPress("MouseLft"))
    {
        //--User clicked a node, delete it.
        if(rHighlightedNode != NULL)
        {
            DeleteNodeI(mNodeList->GetSlotOfElementByPtr(rHighlightedNode));
            rHighlightedNode = NULL;
        }

        //--In all cases, we're done node deletion mode.
        mIsNodeDeletionMode = ZE_NDEL_NONE;
    }

    //--We handled the update.
    return true;
}
bool ZoneEditor::HandleRenameNodeUpdate()
{
    //--Handles the update case where the user is renaming a node, which captures keyboard input.
    //--Returns true if it handled the update, false if not.
    if(!mIsRenamingMode) return false;

    //--Setup.
    ControlManager *rControlManager = ControlManager::Fetch();

    //--Get the key.
    char tInKey = rControlManager->PopKeyboardInputBuffer();
    while(tInKey != '\0')
    {
        //--Standard keycodes.
        if(tInKey > 0)
        {
            //--Put the input key in a buffer.
            char tBuffer[2];
            tBuffer[0] = tInKey;
            tBuffer[1] = '\0';

            //--Append it if there's space.
            if((int)strlen(mRenameBuffer) < 63)
            {
                strcat(mRenameBuffer, tBuffer);
            }
            //--Otherwise, the last letter becomes the typed one.
            else
            {
                mRenameBuffer[63] = tInKey;
            }
        }
        //--Backspace, remove the last letter.
        else if(tInKey == SPECIALCODE_BACKSPACE)
        {
            int tLen = (int)strlen(mRenameBuffer);
            if(tLen > 0) mRenameBuffer[tLen-1] = '\0';
        }
        //--Enter.  Clears off the input buffer, and sends it to the console commands list.
        else if(tInKey == SPECIALCODE_RETURN)
        {
            //--Flags.
            mIsRenamingMode = false;
            ClearNodeEditorList();

            //--Node must exist!
            if(rActiveNode)
            {
                //--Reset the node's name.
                rActiveNode->SetName(mRenameBuffer);

                //--Reset the node's name on the list.
                mNodeList->SetRandomPointerToThis(rActiveNode);
                SugarLinkedListEntry *rWholeEntry = mNodeList->GetRandomPointerWholeEntry();
                if(rWholeEntry) ResetString(rWholeEntry->mName, mRenameBuffer);
            }
        }
        //--Cancel.  Clears off the input buffer, doesn't execute command list.
        else if(tInKey == SPECIALCODE_CANCEL)
        {
            mIsRenamingMode = false;
        }

        //--Next key.
        tInKey = rControlManager->PopKeyboardInputBuffer();
    }

    return true;
}
void ZoneEditor::HandleNormalUpdate()
{
   //--Handles the update case where there is no dialogue box up and no special instructions for the mouse to
   //  deal with. This is the 'else' case, and has no internal checks.
   //--Unlike the other updates, doesn't return anything.

    //--Setup.
    float tMouseX, tMouseY, tMouseZ;
    ControlManager *rControlManager = ControlManager::Fetch();
    rControlManager->GetMouseCoordsF(tMouseX, tMouseY, tMouseZ);

    //--Save the Z for later.
    mLastMouseZ = tMouseZ;

    //--Left-click checking.
    if(rControlManager->IsFirstPress("MouseLft"))
    {
        //--If a command is highlighted, execute it. This gets priority over node changing.
        if(rHighlightedCommand && rHighlightedCommand->mRenderFlag != ZE_RENDER_CONFIRM)
        {
            ExecuteCommand(rHighlightedCommand->mFunctionCode);
            return;
        }
        //--Check if any node was highlighted. Set that as the active node. It can be NULL.
        else
        {
            //--If we have an active node we are editing, a click within the boundary of the editor
            //  will not do anything.
            if(IsPointWithin2DReal(tMouseX, tMouseY, mBoxCoords))
            {

            }
            //--Otherwise, change the active node. This can NULL it.
            else
            {
                ResetNodeEditor(rHighlightedNode);
                return;
            }
        }
    }

    //--Right-click checking.
    if(rControlManager->IsFirstPress("MouseRgt"))
    {
        //--If it's a facing/transition, remove that facing/transition.
        if(rHighlightedCommand && rActiveNode)
        {
            //--Get the slot. Fail if it's out of range.
            int tFacingZero = mCommandsList->GetSlotOfElementByPtr(FindCommandPack("Facing A"));
            int tFacingThis = mCommandsList->GetSlotOfElementByPtr(rHighlightedCommand);
            if(tFacingThis - tFacingZero >= 0 && tFacingThis - tFacingZero < rActiveNode->GetFacingsTotal())
            {
                //--Delete.
                rActiveNode->RemoveFacing(tFacingThis - tFacingZero);

                //--Invalidate the active commands, since the below functions will likely purge them.
                rHighlightedCommand = NULL;

                //--Run both rebuilder functions. Removing a facing can reset transition packs!
                ResetFacingPacks(rActiveNode);
                ResetTransitionPacks(rActiveNode);
            }

            //--Check if it's a transition. If so, remove that instead.
            int tTransZero = mCommandsList->GetSlotOfElementByPtr(FindCommandPack("Trans A"));
            int tTransThis = mCommandsList->GetSlotOfElementByPtr(rHighlightedCommand);
            if(tTransThis - tTransZero >= 0 && tTransThis - tTransZero < rActiveNode->GetAutoTransitionsTotal())
            {
                //--Delete.
                rActiveNode->RemoveAutoTransition(tTransThis - tTransZero);

                //--Invalidate the active commands, since the below functions will likely purge them.
                rHighlightedCommand = NULL;

                //--Run the rebuilder function.
                ResetTransitionPacks(rActiveNode);
            }
        }
        //--If it's a node, look at that node.
        else if(rHighlightedNode)
        {
            LookAtNode(rHighlightedNode);
        }
    }
}
