//--Base
#include "ZoneEditor.h"

//--Classes
#include "ZoneNode.h"

//--CoreClasses
#include "SugarLinkedList.h"

//--Definitions
//--GUI
//--Libraries
//--Managers
#include "DebugManager.h"

//--Contains routines for saving and loading to and from files. To simplify the process, the data formats
//  are blocks of fixed-size data whenever possible.

#define EXPECTED_VERSION 10

void ZoneEditor::SaveToFile(const char *pPath)
{
    //--Writes all outstanding node information to the provided file. Also provides a version tag. If
    //  there was any node data in the current class, it gets overwritten!
    if(!pPath) return;

    //--Open the file.
    FILE *fOutfile = fopen(pPath, "wb");
    if(!fOutfile) return;

    //--Write the version.
    uint32_t tWriteVersion = EXPECTED_VERSION;
    fwrite(&tWriteVersion, sizeof(uint32_t), 1, fOutfile);

    //--Write how many nodes are in the current table.
    uint32_t tNodeCount = (uint32_t)mNodeList->GetListSize();
    fwrite(&tNodeCount, sizeof(uint32_t), 1, fOutfile);

    //--Begin writing node information.
    ZoneNode *rNode = (ZoneNode *)mNodeList->PushIterator();
    while(rNode)
    {
        //--Handled internally by the node.
        rNode->WriteToFile(fOutfile);

        //--Next.
        rNode = (ZoneNode *)mNodeList->AutoIterate();
    }

    //--Finish up.
    fclose(fOutfile);
}
void ZoneEditor::LoadFromFile(const char *pPath)
{
    //--Reads all information from the file. If the versions don't match, fails.
    if(!pPath) return;

    //--Open the file.
    FILE *fInfile = fopen(pPath, "rb");
    if(!fInfile) return;

    //--Set this flag. We implicitly "move" the player to the nearest node.
    mHasMovedPlayer = true;

    //--Check the file type. It's a 32-bit integer indicating the version number. Version 100 is the version
    //  where the node table is "complete", though beta versions exist before that.
    uint32_t tCheckVersion = 0;
    fread(&tCheckVersion, sizeof(uint32_t), 1, fInfile);
    if(tCheckVersion != EXPECTED_VERSION)
    {
        fclose(fInfile);
        return;
    }

    //--Version was legal, so we can wipe the node list.
    mNodeList->ClearList();

    //--Read how many nodes we expect.
    uint32_t tExpectedNodes;
    fread(&tExpectedNodes, sizeof(uint32_t), 1, fInfile);

    //--Begin reading nodes.
    for(int i = 0; i < (int)tExpectedNodes; i ++)
    {
        //--Create a node. Read.
        ZoneNode *nNewNode = new ZoneNode();

        //--Check for errors.
        if(!nNewNode->ReadFromFile(fInfile))
        {
            delete nNewNode;
            DebugManager::ForcePrint("ZoneEditor:LoadFromFile - Error, ZoneNode %i had a problem when reading.\n", i);
            break;
        }

        //--Register.
        mNodeList->AddElement(nNewNode->GetName(), nNewNode, &ZoneNode::DeleteThis);
    }

    //--All done.
    fclose(fInfile);
}
