//--[ZoneEditor]
//--When constructing a VisualLevel, this object stores information concerning room positions and
//  orientations. It has a visual interface available using the mouse and can save/load from the hard drive.
//--This ostensibly requires a WADLevel to work, but the information stored is generic.

#pragma once

//--[Includes]
#include "Definitions.h"
#include "Structures.h"
#include "RootObject.h"

//--[Local Structures]
typedef struct CommandPack
{
    //--[Members]
    char mText[32];
    float mXPos;
    float mYPos;
    int mFunctionCode;
    uint8_t mRenderFlag;
    float mTextSize;
}CommandPack;

//--[Local Definitions]
//--Command function lookup
#define ZE_FUNCTION_NEW_NODE 0
#define ZE_FUNCTION_LOOK_STRAIGHT 1
#define ZE_FUNCTION_START_RENAME 2
#define ZE_FUNCTION_NODE_SET_POSITION 3
#define ZE_FUNCTION_NODE_MOVETO_POSITION 4
#define ZE_FUNCTION_SAVE_CONFIRM 5
#define ZE_FUNCTION_LOAD_CONFIRM 6
#define ZE_FUNCTION_SAVE 7
#define ZE_FUNCTION_LOAD 8
#define ZE_FUNCTION_CLOSE_CONFIRM 9
#define ZE_FUNCTION_NODE_NEW_FACING 10
#define ZE_FUNCTION_NODE_SET_FACING 11
#define ZE_FUNCTION_NODE_LOOK_AT 12
#define ZE_FUNCTION_NODE_CONNECT_FACING 14
#define ZE_FUNCTION_NODE_CREATE_TRANSITION 15
#define ZE_FUNCTION_START_NODE_REMOVAL 16
#define ZE_FUNCTION_TOGGLE_TELEPORT 17
#define ZE_SNAP_BY_8 18
#define ZE_SNAP_BY_16 19

//--Rendering flags for commands
#define ZE_RENDER_BASE 0
#define ZE_RENDER_NODE_EDITOR 1
#define ZE_RENDER_CONFIRM 2

//--Rendering
#define ZE_NAME_INDENT_X 10.0f
#define ZE_NAME_INDENT_Y -15.0f
#define ZE_NAME_SIZE_PER 25.0f

//--Joining Node and Facings
#define ZE_NO_FACING_EDIT 0
#define ZE_MUST_SELECT_FACING 1
#define ZE_MUST_SELECT_NODE 2

//--Automatic Transition Mode
#define ZE_AUTO_NONE 0
#define ZE_AUTO_SELECT_SOURCE_NODE 1
#define ZE_AUTO_SELECT_DEST_NODE 2
#define ZE_AUTO_TOGGLE_TELEPORT 3

//--Node Deletion Mode
#define ZE_NDEL_NONE 0
#define ZE_NDEL_SELECT_NODE 1

//--Positions for the Node Editor
#define ZE_NE_AUTOTRANS_X -150.0f
#define ZE_NE_AUTOTRANS_Y  100.0f

//--[Classes]
class ZoneEditor : public RootObject
{
    private:
    //--System
    bool mRenderNodesInWorld;
    char *mCurrentSavePath;

    //--Node Listing
    int mLastMouseZ;
    int mNodeCountOffset;
    float mStoredNodeY;
    ZoneNode *rActiveNode;
    ZoneNode *rHighlightedNode;
    SugarLinkedList *mNodeList;

    //--Node Editor
    float cIndent;
    bool mIsRenamingMode;
    char mRenameBuffer[64];
    TwoDimensionReal mBoxCoords;

    //--"Are You Sure" Box
    bool mShowConfirmDialog;
    TwoDimensionReal mConfirmDialogCoords;

    //--Selecting Node Connections
    int mIsNodeSelectionMode;
    CommandPack *rSelectedFacingCommand;

    //--Selecting Auto Transitions
    int mIsAutoTransitionMode;
    uint32_t mOriginNodeID;

    //--Selecting Nodes for Deletion
    int mIsNodeDeletionMode;

    //--Font
    float mFontSize;
    SugarFont *mRenderFont;

    //--Command Strings
    CommandPack *rHighlightedCommand;
    SugarLinkedList *mCommandsList;

    //--Rendering Lists
    uint32_t mNodeListHandle;
    uint32_t mNodeWorldHandle;
    uint32_t mNodeEditorHandle;

    protected:

    public:
    //--System
    ZoneEditor();
    virtual ~ZoneEditor();

    //--Public Variables
    bool mHasMovedPlayer;

    //--Property Queries
    bool IsMouseOverNode(float pMouseX, float pMouseY, float pYCursor, ZoneNode *pNode);
    bool IsMouseOverCommand(float pMouseX, float pMouseY, CommandPack *pPack);
    ZoneNode *GetHighlightedNode(float pMouseX, float pMouseY);
    CommandPack *GetHighlightedCommand(float pMouseX, float pMouseY);
    uint32_t GetNextAvailableNodeID();

    //--Manipulators
    void SetSavePath(const char *pPath);
    void AddNode(const char *pName);
    void SetActiveNodeS(const char *pName);
    void SetActiveNodeP(void *pPointer);
    void SetNodePosition(float pX, float pY, float pZ);
    void SetNodePosition(const char *pName, float pX, float pY, float pZ);
    void DeleteNodeI(int pSlot);
    void ClearNodeListList();
    void ClearNodeWorldList();
    void ClearNodeEditorList();

    //--Core Methods
    void LookAtNode(ZoneNode *pNode);
    CommandPack *CreateCommandPack(const char *pString, float pX, float pY, int pFunctionCode, float pTextSize, bool pIsRightAligned, bool pIsBottomAligned, uint8_t pRenderFlag);

    private:
    //--Private Core Methods
    CommandPack *FindCommandPack(const char *pName);

    public:
    //--Commands
    void ExecuteCommand(int pCode);

    //--Node Editor
    void ResetNodeEditor(ZoneNode *pNode);
    void ResetFacingPacks(ZoneNode *pNode);
    void ResetTransitionPacks(ZoneNode *pNode);
    void AddTransitionToNodeEditor();

    //--Saving
    void SaveToFile(const char *pPath);
    void LoadFromFile(const char *pPath);

    //--Update
    bool Update();
    bool HandleConfirmationDialog();
    bool HandleFacingUpdate();
    bool HandleAutoTransitionUpdate();
    bool HandleNodeDeletionUpdate();
    bool HandleRenameNodeUpdate();
    void HandleNormalUpdate();

    //--File I/O
    //--Drawing
    void Render();
    void RenderNodesInWorld();
    void RenderNodeNames();

    //--GUI Rendering
    void RenderBox(TwoDimensionReal pDimensions, StarlightColor pInteriorColor, StarlightColor pBorderColor);
    void RenderCommands(uint8_t pFlag);
    void RenderConfirmDialog();
    void RenderNodeEditor(ZoneNode *pNode);
    void RenderNodeSetFacing();
    void RenderNodeSetTransition();
    void RenderNodeDeletion();

    //--Pointer Routing
    ZoneNode *GetNode(uint32_t pID);
    ZoneNode *GetNode(const char *pName);
    ZoneNode *GetNodeNearestTo(float pX, float pY);

    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions

