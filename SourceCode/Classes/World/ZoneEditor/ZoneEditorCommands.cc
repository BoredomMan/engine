//--Base
#include "ZoneEditor.h"

//--Classes
#include "ZoneNode.h"

//--CoreClasses
#include "SugarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"

//--GUI
//--Libraries
//--Managers
#include "CameraManager.h"
#include "ControlManager.h"

//--Contains a single routing function which executes commands based on the code passed in. This gives a nice,
//  centralized command pathway (unlike function pointers, which need tons of routing calls).

void ZoneEditor::ExecuteCommand(int pCode)
{
    //--Executes the associated function retrieved from a CommandPack's integer code. Does nothing if the code
    //  was out of range.

    //--Creates a new node and sets it as the active node.
    if(pCode == ZE_FUNCTION_NEW_NODE)
    {
        //--Create.
        AddNode("New Node");
    }
    //--Camera looks straight ahead.
    else if(pCode == ZE_FUNCTION_LOOK_STRAIGHT)
    {
        float tXRot, tYRot, tZRot;
        SugarCamera3D *rCamera = CameraManager::FetchActiveCamera3D();
        rCamera->Get3DRotation(tXRot, tYRot, tZRot);
        rCamera->SetRotation(90.0f, tYRot, tZRot);
    }
    //--Activates renaming mode.
    else if(pCode == ZE_FUNCTION_START_RENAME)
    {
        mIsRenamingMode = true;
        mRenameBuffer[0] = '\0';
        if(rActiveNode) strncpy(mRenameBuffer, rActiveNode->GetName(), 63);
        ControlManager::Fetch()->ClearInputBuffer();
    }
    //--Sets the current node, if there is one, to the current camera position.
    else if(pCode == ZE_FUNCTION_NODE_SET_POSITION)
    {
        //--Get the position.
        float tXPos, tYPos, tZPos;
        SugarCamera3D *rCamera = CameraManager::FetchActiveCamera3D();
        rCamera->Get3DPosition(tXPos, tYPos, tZPos);

        //--Set the node. We need to recompile display lists.
        if(rActiveNode) rActiveNode->SetPosition(-tXPos, tYPos, -tZPos);

        //--Flags.
        ClearNodeEditorList();
    }
    //--Moves the player to the position of the node.
    else if(pCode == ZE_FUNCTION_NODE_MOVETO_POSITION)
    {
        //--Flag.
        mHasMovedPlayer = true;

        //--Move the camera.
        SugarCamera3D *rCamera = CameraManager::FetchActiveCamera3D();
        if(rActiveNode) rCamera->SetPosition(rActiveNode->GetX(), rActiveNode->GetY(), rActiveNode->GetZ());
    }
    //--Activate the confirmation dialogue for saving.
    else if(pCode == ZE_FUNCTION_SAVE_CONFIRM)
    {
        mShowConfirmDialog = true;
        CommandPack *rCommandPack = FindCommandPack("Yes");
        if(rCommandPack) rCommandPack->mFunctionCode = ZE_FUNCTION_SAVE;
    }
    //--Activate the confirmation dialogue for loading.
    else if(pCode == ZE_FUNCTION_LOAD_CONFIRM)
    {
        mShowConfirmDialog = true;
        CommandPack *rCommandPack = FindCommandPack("Yes");
        if(rCommandPack) rCommandPack->mFunctionCode = ZE_FUNCTION_LOAD;
    }
    //--Save the current node data.
    else if(pCode == ZE_FUNCTION_SAVE)
    {
        mShowConfirmDialog = false;
        SaveToFile(mCurrentSavePath);
    }
    //--Load node data.
    else if(pCode == ZE_FUNCTION_LOAD)
    {
        mShowConfirmDialog = false;
        LoadFromFile(mCurrentSavePath);

        //--Flags.
        ClearNodeListList();
        ClearNodeWorldList();
        ClearNodeEditorList();
    }
    //--Close the confirmation dialogue.
    else if(pCode == ZE_FUNCTION_CLOSE_CONFIRM)
    {
        mShowConfirmDialog = false;
    }
    //--Create a new facing. Gives it the current camera direction.
    else if(pCode == ZE_FUNCTION_NODE_NEW_FACING)
    {
        //--Get the camera position.
        float tXRot, tYRot, tZRot;
        SugarCamera3D *rCamera = CameraManager::FetchActiveCamera3D();
        rCamera->Get3DRotation(tXRot, tYRot, tZRot);

        //--Add the facing.
        if(!rActiveNode) return;
        rActiveNode->AddFacing(tXRot, tYRot, tZRot);

        //--Move down the highlighted command.
        if(rHighlightedCommand) rHighlightedCommand->mYPos = rHighlightedCommand->mYPos + ZE_NAME_SIZE_PER;

        //--Flags.
        ClearNodeEditorList();

        //--Add a command for the new facing.
        char tBuffer[32];
        int tFacingsTotal = rActiveNode->GetFacingsTotal();
        sprintf(tBuffer, "Facing %c", 'A' + tFacingsTotal - 1);
        mCommandsList->AddElement("X", CreateCommandPack(tBuffer, mBoxCoords.mLft + cIndent, mBoxCoords.mTop + (ZE_NAME_SIZE_PER*(float)(4 + tFacingsTotal - 1)), ZE_FUNCTION_NODE_SET_FACING, mFontSize * 0.75f, false, false, ZE_RENDER_NODE_EDITOR), &FreeThis);
    }
    //--Set the facing matching the command to the current camera direction.
    else if(pCode == ZE_FUNCTION_NODE_SET_FACING)
    {
        //--Camera position.
        float tXRot, tYRot, tZRot;
        SugarCamera3D *rCamera = CameraManager::FetchActiveCamera3D();
        rCamera->Get3DRotation(tXRot, tYRot, tZRot);

        //--Node must be active, a command must be highlighted.
        if(!rActiveNode && rHighlightedCommand) return;

        //--Find the 0th command.
        void *rZerothCommand = FindCommandPack("Facing A");
        int tZerothCommandIndex = mCommandsList->GetSlotOfElementByPtr(rZerothCommand);
        int tThisCommandIndex   = mCommandsList->GetSlotOfElementByPtr(rHighlightedCommand);
        if(tZerothCommandIndex == -1 || tThisCommandIndex == -1) return;

        //--Set.
        rActiveNode->SetFacingDirection(tThisCommandIndex - tZerothCommandIndex, tXRot, tYRot, tZRot);

        //--Flags.
        ClearNodeEditorList();
    }
    //--Look at the given node.
    else if(pCode == ZE_FUNCTION_NODE_LOOK_AT)
    {
        LookAtNode(rActiveNode);
    }
    //--Begin node connection editing.
    else if(pCode == ZE_FUNCTION_NODE_CONNECT_FACING)
    {
        mIsNodeSelectionMode = ZE_MUST_SELECT_FACING;
    }
    //--Begin automatic transition editing.
    else if(pCode == ZE_FUNCTION_NODE_CREATE_TRANSITION)
    {
        mIsAutoTransitionMode = ZE_AUTO_SELECT_SOURCE_NODE;
    }
    //--Begin the node removal procedure.
    else if(pCode == ZE_FUNCTION_START_NODE_REMOVAL)
    {
        mIsNodeDeletionMode = ZE_NDEL_SELECT_NODE;
    }
    //--Activates toggle mode.
    else if(pCode == ZE_FUNCTION_TOGGLE_TELEPORT)
    {
        if(rActiveNode) rActiveNode->ToggleTeleportCase();

        //--Flags.
        ClearNodeEditorList();
    }
    //--Snaps the node's position down to the nearest multiple of 8.
    else if(pCode == ZE_SNAP_BY_8)
    {
        if(!rActiveNode) return;
        float tNodeX = rActiveNode->GetX();
        float tNodeY = rActiveNode->GetY();
        float tNodeZ = rActiveNode->GetZ();
        rActiveNode->SetPosition((int)(tNodeX / 8.0f) * 8.0f, (int)(tNodeY / 8.0f) * 8.0f, tNodeZ);
    }
    //--Snaps the node's position down to the nearest multiple of 16.
    else if(pCode == ZE_SNAP_BY_16)
    {
        if(!rActiveNode) return;
        float tNodeX = rActiveNode->GetX();
        float tNodeY = rActiveNode->GetY();
        float tNodeZ = rActiveNode->GetZ();
        rActiveNode->SetPosition((int)(tNodeX / 16.0f) * 16.0f, (int)(tNodeY / 16.0f) * 16.0f, tNodeZ);
    }
}
