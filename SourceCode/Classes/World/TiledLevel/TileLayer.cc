//--Base
#include "TileLayer.h"

//--Classes
#include "AdventureLevel.h"
#include "TiledLevel.h"

//--CoreClasses
#include "SugarBitmap.h"
#include "SugarCamera2D.h"

//--Definitions
#include "Global.h"
#include "OpenGLMacros.h"

//--Libraries
//--Managers
#include "CameraManager.h"
#include "DisplayManager.h"

//=========================================== System ==============================================
TileLayer::TileLayer()
{
    //--[TileLayer]
    //--System
    //--Renderable
    mDisableAnimation = false;
    mIsAnimated = false;
    mIsAnimatedSlowly = false;
    mIsOscillating = false;
    mDoNotRender = false;
    mIsMidground = false;
    mDepth = 0.0f;
    mDisableDepthMark = false;

    //--Special Animation
    mAnimationFrames = 0;
    mAnimationOffset = 0;
    mAnimationTPF = 0;

    //--Tile Data.
    mXSize = 0;
    mYSize = 0;
    mXOffset = 0;
    mYOffset = 0;
    mData = NULL;

    //--Tile Storage.
    mTiles = NULL;

    //--External.
    mExternalTilesetsTotal = 0;
    rTilesetPacks = NULL;

    //--Public Variables.
    mIsUsingPaddedTiles = PAD_TILE_EDGES;
}
TileLayer::~TileLayer()
{
    for(int x = 0; x < mXSize; x ++)
    {
        free(mData[x]);
        free(mTiles[x]);
    }
    free(mData);
    free(mTiles);
}
void TileLayer::DeleteThis(void *pPtr)
{
    delete ((TileLayer *)pPtr);
}

//--[Public Variables]
//--How large a tile is, in pixels, on the tilemaps.
float TileLayer::cxSizePerTile = 16.0f;

//====================================== Property Queries =========================================
float TileLayer::GetDepth()
{
    return mDepth;
}
int TileLayer::GetXSize()
{
    return mXSize;
}
int TileLayer::GetYSize()
{
    return mYSize;
}
int TileLayer::GetTileIndexAt(int pX, int pY)
{
    if(pX < 0 || pY < 0 || pX >= mXSize || pY >= mYSize || !mTiles) return 0;
    if(!mTiles[pX][pY].rUseTileset) return 0;
    return mTiles[pX][pY].mOriginalIndex;
}

//========================================= Manipulators ==========================================
void TileLayer::SetDepth(float pDepth)
{
    mDepth = pDepth;
}
void TileLayer::SetDepthMaskFlag(bool pFlag)
{
    mDisableDepthMark = pFlag;
}
void TileLayer::SetDoNotRenderFlag(bool pFlag)
{
    mDoNotRender = pFlag;
}
void TileLayer::SetMidgroundFlag(bool pFlag)
{
    mIsMidground = pFlag;
}
void TileLayer::SetAnimationDisable(bool pFlag)
{
    mDisableAnimation = pFlag;
}
void TileLayer::SetAnimatedFlag(bool pFlag)
{
    mIsAnimated = pFlag;
}
void TileLayer::SetAnimatedSlowlyFlag(bool pFlag)
{
    mIsAnimatedSlowly = pFlag;
}
void TileLayer::AnimatedManual(int pFrames, int pOffset, int pTicksPerFrame)
{
    mAnimationFrames = pFrames;
    mAnimationOffset = pOffset;
    mAnimationTPF = pTicksPerFrame;
}
void TileLayer::SetOscillationFlag(bool pFlag)
{
    mIsOscillating = pFlag;
}
void TileLayer::ProvideTileReferences(int pTilesetsTotal, TilesetInfoPack *pTileData)
{
    mExternalTilesetsTotal = pTilesetsTotal;
    rTilesetPacks = pTileData;
}
void TileLayer::ProvideTileData(int pXSize, int pYSize, int16_t *pData, bool pFlipTileTx)
{
    //--Size and allocate.
    mXSize = pXSize;
    mYSize = pYSize;
    SetMemoryData(__FILE__, __LINE__);
    mData = (int **)starmemoryalloc(sizeof(int *) * mXSize);

    //--Read.
    int tCounter = 0;
    for(int x = 0; x < mXSize; x ++)
    {
        SetMemoryData(__FILE__, __LINE__);
        mData[x] = (int *)starmemoryalloc(sizeof(int) * mYSize);
        for(int y = 0; y < mYSize; y ++)
        {
            mData[x][y] = pData[tCounter];
            tCounter ++;
        }
    }

    //--Allocate size for the tile data.
    SetMemoryData(__FILE__, __LINE__);
    mTiles = (TilesetTile **)starmemoryalloc(sizeof(TilesetTile *) * mXSize);
    for(int x = 0; x < mXSize; x ++)
    {
        SetMemoryData(__FILE__, __LINE__);
        mTiles[x] = (TilesetTile *)starmemoryalloc(sizeof(TilesetTile) * mYSize);
        for(int y = 0; y < mYSize; y ++)
        {
            //--Using the tile index, figure out the tile positions to use when rendering.
            SaveTileInformation(mData[x][y], mTiles[x][y], pFlipTileTx);
        }
    }
}
void TileLayer::ProvideFlipData(int pXSize, int pYSize, int16_t *pData)
{
    //--Optional call, this modifies the tile information by flipping or rotating the texture
    //  coordinates after they've been allocated. If there's no tile data yet, does nothing.
    if(!mData) return;

    int tCounter = 0;
    for(int x = 0; x < pXSize; x ++)
    {
        for(int y = 0; y < pYSize; y ++)
        {
            //--Edge checker. Should be identical...
            if(x >= mXSize || y >= mYSize) continue;
            mTiles[x][y].mFlipFlags |= (pData[tCounter] & 0x00FF);
            if(mTiles[x][y].mFlipFlags == 7)
            {
                mTiles[x][y].mFlipFlags = 4;
            }
            else if(mTiles[x][y].mFlipFlags == 4)
            {
                mTiles[x][y].mFlipFlags = 7;
            }

            //--Counter.
            tCounter ++;
        }
    }
}

//========================================= Core Methods ==========================================
void TileLayer::Clear()
{
    //--Clears the layer back to factory-zero.
    for(int x = 0; x < mXSize; x ++) free(mData[x]);
    free(mData);

    //--[TileLayer]
    //--System
    //--Renderable
    mDepth = 0.0f;

    //--Tile Data.
    mXSize = 0;
    mYSize = 0;
    mXOffset = 0;
    mYOffset = 0;
    mData = NULL;

    //--External.
    mExternalTilesetsTotal = 0;
    rTilesetPacks = NULL;
}
uint8_t **TileLayer::ProcessCollision(int &sXSize, int &sYSize)
{
    //--Given that the tile layer is a collision layer, processes it into a usable collision
    //  array and passes it back to the caller.
    sXSize = mXSize;
    sYSize = mYSize;

    //--Constants.
    //float cPerTileX = cxSizePerTile / 160.0f;
    //float cPerTileY = cxSizePerTile / 48.0f;
    //float cTilesPerRow = 10.0f;

    //--Allocate.
    SetMemoryData(__FILE__, __LINE__);
    uint8_t **nClips = (uint8_t **)starmemoryalloc(sizeof(uint8_t *) * sXSize);
    for(int x = 0; x < sXSize; x ++)
    {
        SetMemoryData(__FILE__, __LINE__);
        nClips[x] = (uint8_t *)starmemoryalloc(sizeof(uint8_t) * sYSize);
        for(int y = 0; y < sYSize; y ++)
        {
            //--Get the tile in question.
            TilesetTile *rTile = &mTiles[x][y];
            nClips[x][y] = CLIP_NONE;

            //--Tile has no image. Therefore, no clip.
            if(!rTile->rUseTileset) continue;

            //--Calculate the zero tile.
            int tZeroTile = rTile->mOriginalIndex;
            for(int i = 0; i < mExternalTilesetsTotal; i ++)
            {
                if(tZeroTile >= rTilesetPacks[i].mTilesTotal)
                {
                    tZeroTile -= rTilesetPacks[i].mTilesTotal;
                }
                else
                {
                    break;
                }
            }

            //--Tile exists at this location, therefore it is clipped.
            nClips[x][y] = tZeroTile + 1;
        }
    }
    return nClips;
}
uint16_t **TileLayer::ProcessCollisionFlips(int &sXSize, int &sYSize)
{
    //--Given that the tile layer is a collision layer, stores the flip flags for collision purposes.
    //  Only horizontal and vertical flips are processed.
    sXSize = mXSize;
    sYSize = mYSize;

    //--Allocate.
    SetMemoryData(__FILE__, __LINE__);
    uint16_t **nFlips = (uint16_t **)starmemoryalloc(sizeof(uint16_t *) * sXSize);
    for(int x = 0; x < sXSize; x ++)
    {
        SetMemoryData(__FILE__, __LINE__);
        nFlips[x] = (uint16_t *)starmemoryalloc(sizeof(uint16_t) * sYSize);
        for(int y = 0; y < sYSize; y ++)
        {
            //--Get the tile in question.
            TilesetTile *rTile = &mTiles[x][y];
            nFlips[x][y] = rTile->mFlipFlags;
        }
    }
    return nFlips;
}

//===================================== Private Core Methods ======================================
void TileLayer::SaveTileInformation(int pTileIndex, TilesetTile &sTileData, bool pIsFlipped)
{
    //--Given a tile index, populates the sTileData structure with all the information it needs
    //  to render. May set the image to NULL if nothing should render.
    sTileData.rUseTileset = NULL;
    if(pTileIndex < 0)
    {
        sTileData.mOriginalIndex = 0;
        sTileData.mTexL = -2.0f;
        sTileData.mTexT = -2.0f;
        sTileData.mTexR = -1.0f;
        sTileData.mTexB = -1.0f;
        return;
    }
    sTileData.mOriginalIndex = pTileIndex;

    //--This is a valid tile. Time to calc! First, figure out which tileset it uses.
    for(int i = 0; i < mExternalTilesetsTotal; i ++)
    {
        if(pTileIndex < rTilesetPacks[i].mTilesTotal)
        {
            sTileData.rUseTileset = rTilesetPacks[i].mTileset;
            break;
        }
        pTileIndex -= rTilesetPacks[i].mTilesTotal;
    }

    //--If the tileset is still null, fail here.
    if(!sTileData.rUseTileset) return;

    //--Tileset values.
    int tTilesetWidInTiles = sTileData.rUseTileset->GetWidth() / cxSizePerTile;
    if(mIsUsingPaddedTiles) tTilesetWidInTiles = sTileData.rUseTileset->GetWidth() / (cxSizePerTile+2);

    //--Calc the X position.
    float tXPos = (float)(pTileIndex % tTilesetWidInTiles);
    float tYPos = pTileIndex / tTilesetWidInTiles;

    float cInteriorX = (1.0f / (float)sTileData.rUseTileset->GetWidth()) * 0.50f;
    float cInteriorY = (1.0f / (float)sTileData.rUseTileset->GetHeight()) * 0.50f;

    //--Normal: No tile padding.
    if(!mIsUsingPaddedTiles)
    {
        sTileData.mTexL = (tXPos * cxSizePerTile) / sTileData.rUseTileset->GetWidth();
        sTileData.mTexT = (tYPos * cxSizePerTile) / sTileData.rUseTileset->GetHeight();
        sTileData.mTexR = sTileData.mTexL + (cxSizePerTile / sTileData.rUseTileset->GetWidth());
        sTileData.mTexB = sTileData.mTexT + (cxSizePerTile / sTileData.rUseTileset->GetHeight());

        //--Move all the texel values in by half a texel.
        sTileData.mTexL = sTileData.mTexL + cInteriorX;
        sTileData.mTexT = sTileData.mTexT + cInteriorY;
        sTileData.mTexR = sTileData.mTexR - cInteriorX;
        sTileData.mTexB = sTileData.mTexB - cInteriorY;
    }
    //--Tile is padded so the magni/minification flags don't mess up.
    else
    {
        sTileData.mTexL = ((tXPos * (cxSizePerTile+2)) + 1) / sTileData.rUseTileset->GetWidth();
        sTileData.mTexT = ((tYPos * (cxSizePerTile+2)) + 1) / sTileData.rUseTileset->GetHeight();
        sTileData.mTexR = sTileData.mTexL + (cxSizePerTile / sTileData.rUseTileset->GetWidth());
        sTileData.mTexB = sTileData.mTexT + (cxSizePerTile / sTileData.rUseTileset->GetHeight());
    }

    //--Default flip data.
    sTileData.mFlipFlags = 0;

    //--Flip if specified.
    if(pIsFlipped)
    {
        float tTemp = sTileData.mTexT;
        sTileData.mTexT = sTileData.mTexB;
        sTileData.mTexB = tTemp;
    }
}

//============================================ Update =============================================
//=========================================== File I/O ============================================
//=========================================== Drawing =============================================
void TileLayer::Render()
{
    //--Renders all tiles directly to the world. Some tiles render at different depths if this
    //  is a midground layer.
    RenderRange(0, 0, mXSize, mYSize);
}
void TileLayer::RenderRange(int pLft, int pTop, int pRgt, int pBot)
{
    //--Renders the tile layer, but renders a fixed range instead of the entire thing.
    if(mDoNotRender) return;

    //--Setup.
    DisplayManager::StdModelPush();
    SugarBitmap *rCurrentBoundImage = NULL;

    //--If flagged, don't write into the depth buffer.
    glDepthMask(true);
    if(mDisableDepthMark) glDepthMask(false);

    //--Range checking.
    if(pLft < 0) pLft = 0;
    if(pTop < 0) pTop = 0;
    if(pRgt > mXSize) pRgt = mXSize;
    if(pBot > mYSize) pBot = mYSize;

    //--Fast-access statics.
    static float xUseL;
    static float xUseT;
    static float xUseR;
    static float xUseB;

    //--Translation factors.
    float mXTranslate = 0.0f;
    float mYTranslate = 0.0f;
    float mZTranslate = 0.0f;

    //--Compute oscillation factor.
    float tOscillationFactor = 0.0f;
    if(mIsOscillating)
    {
        tOscillationFactor = (sinf(Global::Shared()->gTicksElapsed / 360.0f * 3.1415926f)) * 1.1f;
    }

    //--Depth.
    mZTranslate = mZTranslate + mDepth;
    //glTranslatef(0.0f, 0.0f, mDepth);

    //--Lighting. Some layers use a different effective Y value than others when lighting is active. This
    //  prevents lights from rendering behind them and looking odd.
    bool tUseSpecialLightDepth = false;
    uint32_t cShaderHandle = 0;
    AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
    if(rActiveLevel)
    {
        cShaderHandle = DisplayManager::Fetch()->mLastProgramHandle;
        if(rActiveLevel->IsLightingActive() && mIsMidground && mDepth >= -0.499810f)
        {
            //--Common.
            tUseSpecialLightDepth = true;

            //--Lighting depth for "WallsHi" layers.
            if(mDepth < -0.499710)
            {
                tUseSpecialLightDepth = true;
                ShaderUniform1i(cShaderHandle, 3, "uUseOverrideY");
            }
            //--Lighting depth for "WallsVeryHi" and higher.
            else
            {
                ShaderUniform1i(cShaderHandle, 4, "uUseOverrideY");
            }
        }
        else if(rActiveLevel->IsLightingActive())
        {
            ShaderUniform1i(cShaderHandle, 0, "uUseOverrideY");
        }
    }

    //--Render across the tables.
    glBegin(GL_QUADS);
    for(int y = pTop; y < pBot; y ++)
    {
        //--Additional depth translation. Only applies to midground tiles.
        if(mIsMidground)
        {
            //--Base.
            mZTranslate = mZTranslate + (DEPTH_PER_TILE * (float)y);
            //glTranslatef(0.0f, 0.0f, DEPTH_PER_TILE * (float)y);

            //--Special light depth.
            if(tUseSpecialLightDepth && rActiveLevel)
            {
                float cPosition = (((y+1) * cxSizePerTile) - rActiveLevel->GetCameraTop()) * rActiveLevel->GetCameraScale();
                glEnd();
                ShaderUniform1f(cShaderHandle, cPosition, "uOverrideY");
                glBegin(GL_QUADS);
            }
        }

        for(int x = pLft; x < pRgt; x ++)
        {
            //--Make sure something needs rendering.
            if(mTiles[x][y].rUseTileset)
            {
                //--Send the bind only if it's needed.
                if(rCurrentBoundImage != mTiles[x][y].rUseTileset)
                {
                    glEnd();
                    mTiles[x][y].rUseTileset->Bind();
                    glBegin(GL_QUADS);
                    rCurrentBoundImage = mTiles[x][y].rUseTileset;
                }

                //--Compute Y. When oscillating, Y moves periodically.
                float tYPos = (y * cxSizePerTile) + tOscillationFactor;

                //--Position.
                mXTranslate = mXTranslate + (x * cxSizePerTile);
                mYTranslate = mYTranslate + tYPos;
                //glTranslatef(x * cxSizePerTile, tYPos, 0.0f);

                //--Store
                xUseL = mTiles[x][y].mTexL;
                xUseT = mTiles[x][y].mTexT;
                xUseR = mTiles[x][y].mTexR;
                xUseB = mTiles[x][y].mTexB;

                //--Animated, but animation is disabled.
                if(mDisableAnimation)
                {

                }
                //--Animated tiles.
                else if(mIsAnimated && Global::Shared()->gTicksElapsed % 16 < 8)
                {
                    xUseL = xUseL + 0.5f;
                    xUseR = xUseR + 0.5f;
                }
                //--Slow animation.
                else if(mIsAnimatedSlowly && Global::Shared()->gTicksElapsed % 32 < 16)
                {
                    xUseL = xUseL + 0.5f;
                    xUseR = xUseR + 0.5f;
                }
                //--Manually specified animation.
                else if(mAnimationFrames > 1 && mAnimationOffset >= 1 && mAnimationTPF >= 1)
                {
                    //--Compute. Remember to add 2 pixels for edge padding!
                    int tMaxTicks = mAnimationTPF * mAnimationFrames;
                    int tTileOffset = (int)((Global::Shared()->gTicksElapsed % tMaxTicks) / mAnimationTPF) * (mAnimationOffset * (16.0f + 2.0f));
                    float cTexelsX = rCurrentBoundImage->GetTrueWidth();
                    float cOffset = tTileOffset / cTexelsX;

                    //--Apply the offsets. Wrap around the image.
                    float cDif = xUseR - xUseL;
                    xUseL = xUseL + cOffset;
                    while(xUseL > 1.0f) xUseL = xUseL - 1.0f;
                    xUseR = xUseL + cDif;
                }

                //--Flips
                if(mTiles[x][y].mFlipFlags & 0x01)
                {
                    float tTemp = xUseL;
                    xUseL = xUseR;
                    xUseR = tTemp;
                }
                if(mTiles[x][y].mFlipFlags & 0x02)
                {
                    float tTemp = xUseT;
                    xUseT = xUseB;
                    xUseB = tTemp;
                }

                //--Render (normal)
                if(!(mTiles[x][y].mFlipFlags & 0x04))
                {
                    glTexCoord2f(xUseL, xUseT); glVertex3f(         0.0f + mXTranslate,          0.0f + mYTranslate, mZTranslate);
                    glTexCoord2f(xUseR, xUseT); glVertex3f(cxSizePerTile + mXTranslate,          0.0f + mYTranslate, mZTranslate);
                    glTexCoord2f(xUseR, xUseB); glVertex3f(cxSizePerTile + mXTranslate, cxSizePerTile + mYTranslate, mZTranslate);
                    glTexCoord2f(xUseL, xUseB); glVertex3f(         0.0f + mXTranslate, cxSizePerTile + mYTranslate, mZTranslate);
                }
                //--Render (antidiagonal flip)
                else
                {
                    glTexCoord2f(xUseR, xUseB); glVertex3f(         0.0f + mXTranslate,          0.0f + mYTranslate, mZTranslate);
                    glTexCoord2f(xUseR, xUseT); glVertex3f(cxSizePerTile + mXTranslate,          0.0f + mYTranslate, mZTranslate);
                    glTexCoord2f(xUseL, xUseT); glVertex3f(cxSizePerTile + mXTranslate, cxSizePerTile + mYTranslate, mZTranslate);
                    glTexCoord2f(xUseL, xUseB); glVertex3f(         0.0f + mXTranslate, cxSizePerTile + mYTranslate, mZTranslate);
                }

                //--Clean.
                mXTranslate = mXTranslate - (x * cxSizePerTile);
                mYTranslate = mYTranslate - tYPos;
                //glTranslatef(x * cxSizePerTile * -1.0f, tYPos * -1.0f, 0.0f);
            }
        }

        //--Additional depth translation. Only applies to midground tiles.
        if(mIsMidground)
        {
            //glTranslatef(0.0f, 0.0f, DEPTH_PER_TILE * (float)y * -1.0f);
            mZTranslate = mZTranslate - (DEPTH_PER_TILE * (float)y);
        }
    }
    glEnd();

    //--Depth.
    //glTranslatef(0.0f, 0.0f, mDepth * -1.0f);
    mZTranslate = mZTranslate - mDepth;

    //--Clean.
    DisplayManager::StdModelPop();
}
void TileLayer::RenderTile(int pX, int pY)
{
    //--Renders a single tile. Faster than the area-render handler.
    if(mDoNotRender) return;

    //--If flagged, don't write into the depth buffer.
    glDepthMask(true);
    if(mDisableDepthMark) glDepthMask(false);

    //--Range checking.
    if(pX < 0) pX = 0;
    if(pY < 0) pY = 0;
    if(pX >= mXSize) pX = mXSize-1;
    if(pY >= mYSize) pY = mYSize-1;

    //--Fast-access statics.
    static float xUseL;
    static float xUseT;
    static float xUseR;
    static float xUseB;

    //--Translation factors.
    float mXTranslate = 0.0f;
    float mYTranslate = 0.0f;
    float mZTranslate = 0.0f;

    //--Depth.
    mZTranslate = mZTranslate + mDepth;

    //--Make sure something needs rendering.
    if(mTiles[pX][pY].rUseTileset)
    {
        //--Send the bind only if it's needed.
        mTiles[pX][pY].rUseTileset->Bind();
        glBegin(GL_QUADS);

        //--Compute Y. When oscillating, Y moves periodically.
        //float tYPos = 0.0f;

        //--Position.
        mXTranslate = mXTranslate;
        mYTranslate = mYTranslate;
        //glTranslatef(x * cxSizePerTile, tYPos, 0.0f);

        //--Store
        xUseL = mTiles[pX][pY].mTexL;
        xUseT = mTiles[pX][pY].mTexT;
        xUseR = mTiles[pX][pY].mTexR;
        xUseB = mTiles[pX][pY].mTexB;

        //--Animated, but animation is disabled.
        if(mDisableAnimation)
        {

        }
        //--Animated tiles.
        else if(mIsAnimated && Global::Shared()->gTicksElapsed % 16 < 8)
        {
            xUseL = xUseL + 0.5f;
            xUseR = xUseR + 0.5f;
        }
        //--Slow animation.
        else if(mIsAnimatedSlowly && Global::Shared()->gTicksElapsed % 32 < 16)
        {
            xUseL = xUseL + 0.5f;
            xUseR = xUseR + 0.5f;
        }
        //--Manually specified animation.
        else if(mAnimationFrames > 1 && mAnimationOffset >= 1 && mAnimationTPF >= 1)
        {
            //--Compute. Remember to add 2 pixels for edge padding!
            int tMaxTicks = mAnimationTPF * mAnimationFrames;
            int tTileOffset = (int)((Global::Shared()->gTicksElapsed % tMaxTicks) / mAnimationTPF) * (mAnimationOffset * (16.0f + 2.0f));
            float cTexelsX = mTiles[pX][pY].rUseTileset->GetTrueWidth();
            float cOffset = tTileOffset / cTexelsX;

            //--Apply the offsets. Wrap around the image.
            float cDif = xUseR - xUseL;
            xUseL = xUseL + cOffset;
            while(xUseL > 1.0f) xUseL = xUseL - 1.0f;
            xUseR = xUseL + cDif;
        }

        //--Flips
        if(mTiles[pX][pY].mFlipFlags & 0x01)
        {
            float tTemp = xUseL;
            xUseL = xUseR;
            xUseR = tTemp;
        }
        if(mTiles[pX][pY].mFlipFlags & 0x02)
        {
            float tTemp = xUseT;
            xUseT = xUseB;
            xUseB = tTemp;
        }

        //--Render (normal)
        if(!(mTiles[pX][pY].mFlipFlags & 0x04))
        {
            glTexCoord2f(xUseL, xUseT); glVertex3f(         0.0f + mXTranslate,          0.0f + mYTranslate, mZTranslate);
            glTexCoord2f(xUseR, xUseT); glVertex3f(cxSizePerTile + mXTranslate,          0.0f + mYTranslate, mZTranslate);
            glTexCoord2f(xUseR, xUseB); glVertex3f(cxSizePerTile + mXTranslate, cxSizePerTile + mYTranslate, mZTranslate);
            glTexCoord2f(xUseL, xUseB); glVertex3f(         0.0f + mXTranslate, cxSizePerTile + mYTranslate, mZTranslate);
        }
        //--Render (antidiagonal flip)
        else
        {
            glTexCoord2f(xUseR, xUseB); glVertex3f(         0.0f + mXTranslate,          0.0f + mYTranslate, mZTranslate);
            glTexCoord2f(xUseR, xUseT); glVertex3f(cxSizePerTile + mXTranslate,          0.0f + mYTranslate, mZTranslate);
            glTexCoord2f(xUseL, xUseT); glVertex3f(cxSizePerTile + mXTranslate, cxSizePerTile + mYTranslate, mZTranslate);
            glTexCoord2f(xUseL, xUseB); glVertex3f(         0.0f + mXTranslate, cxSizePerTile + mYTranslate, mZTranslate);
        }
        glEnd();
    }
}
void TileLayer::RenderDislocated(int pLft, int pTop, int pRgt, int pBot, float pXRender, float pYRender)
{
    //--Renders the tile layer, but uses a specific range of tiles rendered at an offset.
    if(mDoNotRender) return;

    //--Setup.
    DisplayManager::StdModelPush();
    SugarBitmap *rCurrentBoundImage = NULL;

    //--If flagged, don't write into the depth buffer.
    glDepthMask(true);
    if(mDisableDepthMark) glDepthMask(false);

    //--Range checking.
    if(pLft < 0) pLft = 0;
    if(pTop < 0) pTop = 0;
    if(pRgt > mXSize) pRgt = mXSize;
    if(pBot > mYSize) pBot = mYSize;

    //--Fast-access statics.
    static float xUseL;
    static float xUseT;
    static float xUseR;
    static float xUseB;

    //--Translation factors.
    float mXTranslate = pXRender;
    float mYTranslate = pYRender;
    float mZTranslate = 0.0f;

    //--Compute oscillation factor.
    float tOscillationFactor = 0.0f;
    if(mIsOscillating)
    {
        tOscillationFactor = (sinf(Global::Shared()->gTicksElapsed / 360.0f * 3.1415926f)) * 1.1f;
    }

    //--Depth.
    mZTranslate = mZTranslate + mDepth;
    //glTranslatef(0.0f, 0.0f, mDepth);

    //--Lighting. Some layers use a different effective Y value than others when lighting is active. This
    //  prevents lights from rendering behind them and looking odd.
    bool tUseSpecialLightDepth = false;
    AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
    uint32_t cShaderHandle = DisplayManager::Fetch()->mLastProgramHandle;
    if(rActiveLevel->IsLightingActive() && mIsMidground && mDepth >= -0.499810f)
    {
        //--Common.
        tUseSpecialLightDepth = true;

        //--Lighting depth for "WallsHi" layers.
        if(mDepth < -0.499710)
        {
            tUseSpecialLightDepth = true;
            ShaderUniform1i(cShaderHandle, 3, "uUseOverrideY");
        }
        //--Lighting depth for "WallsVeryHi" and higher.
        else
        {
            ShaderUniform1i(cShaderHandle, 4, "uUseOverrideY");
        }
    }
    else if(rActiveLevel->IsLightingActive())
    {
        ShaderUniform1i(cShaderHandle, 0, "uUseOverrideY");
    }

    //--Render across the tables.
    glBegin(GL_QUADS);
    for(int y = pTop; y < pBot; y ++)
    {
        //--Additional depth translation. Only applies to midground tiles.
        if(mIsMidground)
        {
            //--Base.
            mZTranslate = mZTranslate + (DEPTH_PER_TILE * (float)y);
            //glTranslatef(0.0f, 0.0f, DEPTH_PER_TILE * (float)y);

            //--Special light depth.
            if(tUseSpecialLightDepth)
            {
                float cPosition = (((y+1) * cxSizePerTile) - rActiveLevel->GetCameraTop()) * rActiveLevel->GetCameraScale();
                glEnd();
                ShaderUniform1f(cShaderHandle, cPosition, "uOverrideY");
                glBegin(GL_QUADS);
            }
        }

        for(int x = pLft; x < pRgt; x ++)
        {
            //--Make sure something needs rendering.
            if(mTiles[x][y].rUseTileset)
            {
                //--Send the bind only if it's needed.
                if(rCurrentBoundImage != mTiles[x][y].rUseTileset)
                {
                    glEnd();
                    mTiles[x][y].rUseTileset->Bind();
                    glBegin(GL_QUADS);
                    rCurrentBoundImage = mTiles[x][y].rUseTileset;
                }

                //--Compute Y. When oscillating, Y moves periodically.
                float tYPos = ((y-pTop) * cxSizePerTile) + tOscillationFactor;

                //--Position.
                mXTranslate = mXTranslate + ((x-pLft) * cxSizePerTile);
                mYTranslate = mYTranslate + tYPos;
                //glTranslatef(x * cxSizePerTile, tYPos, 0.0f);

                //--Store
                xUseL = mTiles[x][y].mTexL;
                xUseT = mTiles[x][y].mTexT;
                xUseR = mTiles[x][y].mTexR;
                xUseB = mTiles[x][y].mTexB;

                //--Animated tiles.
                if(mIsAnimated && Global::Shared()->gTicksElapsed % 16 < 8)
                {
                    xUseL = xUseL + 0.5f;
                    xUseR = xUseR + 0.5f;
                }
                //--Slow animation.
                else if(mIsAnimatedSlowly && Global::Shared()->gTicksElapsed % 32 < 16)
                {
                    xUseL = xUseL + 0.5f;
                    xUseR = xUseR + 0.5f;
                }
                //--Manually specified animation.
                else if(mAnimationFrames > 1 && mAnimationOffset >= 1 && mAnimationTPF > 1)
                {
                    //--Compute. Remember to add 2 pixels for edge padding!
                    int tMaxTicks = mAnimationTPF * mAnimationFrames;
                    int tTileOffset = (int)((Global::Shared()->gTicksElapsed % tMaxTicks) / mAnimationTPF) * (mAnimationOffset * (16.0f + 2.0f));
                    float cTexelsX = rCurrentBoundImage->GetTrueWidth();
                    float cOffset = tTileOffset / cTexelsX;

                    //--Apply the offsets. Wrap around the image.
                    float cDif = xUseR - xUseL;
                    xUseL = xUseL + cOffset;
                    while(xUseL > 1.0f) xUseL = xUseL - 1.0f;
                    xUseR = xUseL + cDif;
                }

                //--Flips
                if(mTiles[x][y].mFlipFlags & 0x01)
                {
                    float tTemp = xUseL;
                    xUseL = xUseR;
                    xUseR = tTemp;
                }
                if(mTiles[x][y].mFlipFlags & 0x02)
                {
                    float tTemp = xUseT;
                    xUseT = xUseB;
                    xUseB = tTemp;
                }

                //--Render (normal)
                if(!(mTiles[x][y].mFlipFlags & 0x04))
                {
                    glTexCoord2f(xUseL, xUseT); glVertex3f(         0.0f + mXTranslate,          0.0f + mYTranslate, mZTranslate);
                    glTexCoord2f(xUseR, xUseT); glVertex3f(cxSizePerTile + mXTranslate,          0.0f + mYTranslate, mZTranslate);
                    glTexCoord2f(xUseR, xUseB); glVertex3f(cxSizePerTile + mXTranslate, cxSizePerTile + mYTranslate, mZTranslate);
                    glTexCoord2f(xUseL, xUseB); glVertex3f(         0.0f + mXTranslate, cxSizePerTile + mYTranslate, mZTranslate);
                }
                //--Render (antidiagonal flip)
                else
                {
                    glTexCoord2f(xUseR, xUseB); glVertex3f(         0.0f + mXTranslate,          0.0f + mYTranslate, mZTranslate);
                    glTexCoord2f(xUseR, xUseT); glVertex3f(cxSizePerTile + mXTranslate,          0.0f + mYTranslate, mZTranslate);
                    glTexCoord2f(xUseL, xUseT); glVertex3f(cxSizePerTile + mXTranslate, cxSizePerTile + mYTranslate, mZTranslate);
                    glTexCoord2f(xUseL, xUseB); glVertex3f(         0.0f + mXTranslate, cxSizePerTile + mYTranslate, mZTranslate);
                }

                //--Clean.
                mXTranslate = mXTranslate - ((x-pLft) * cxSizePerTile);
                mYTranslate = mYTranslate - tYPos;
                //glTranslatef(x * cxSizePerTile * -1.0f, tYPos * -1.0f, 0.0f);
            }
        }

        //--Additional depth translation. Only applies to midground tiles.
        if(mIsMidground)
        {
            //glTranslatef(0.0f, 0.0f, DEPTH_PER_TILE * (float)y * -1.0f);
            mZTranslate = mZTranslate - (DEPTH_PER_TILE * (float)y);
        }
    }
    glEnd();

    //--Depth.
    //glTranslatef(0.0f, 0.0f, mDepth * -1.0f);
    mZTranslate = mZTranslate - mDepth;

    //--Clean.
    DisplayManager::StdModelPop();
}

//======================================= Pointer Routing =========================================
//====================================== Static Functions =========================================
//========================================= Lua Hooking ===========================================
//======================================= Command Hooking =========================================
//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
//=================================================================================================
//                                       Command Functions                                       ==
//=================================================================================================
