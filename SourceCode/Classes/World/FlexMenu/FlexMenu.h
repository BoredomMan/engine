//--[FlexMenu]
//--A Flexible Menu (duh) based off the SugarMenu class. Stored in a stack by the Level, it
//  allows Lua to have a greater degree of control over the player's actions. Implements
//  rendering behaviors that the SugarMenu does not have by default.

#pragma once

//--[Includes]
#include "Definitions.h"
#include "Structures.h"
#include "SugarMenu.h"

//--[Local Structures]
typedef struct HeaderPack
{
    char *mText;
    float cFontSize;
}HeaderPack;

//--Package that represents a hard-drive file.
typedef struct LoadingPack
{
    //--System
    char mFileName[512];
    char mFilePath[512];
    char mFilePathShort[512];

    //--Party Members
    char mPartyNames[4][STD_MAX_LETTERS];
    int mPartyLevels[4];

    //--Location
    char mMapLocation[STD_MAX_LETTERS];

    //--Timestamp.
    char mTimestamp[STD_MAX_LETTERS];

    //--Reference Images
    SugarBitmap *rRenderImg[4];
}LoadingPack;

//--[Local Definitions]
//--Audio timing
#define FM_SILENCE_TICKS 15

//--Text Sizing
#define FM_DESCRIPTION_LINES 8
#define FM_DESCRIPTION_CHARS 80

//--Loading Screen Characters
#define FM_LOAD_MEI 0
#define FM_LOAD_FLORENTINA 1
#define FM_LOAD_CHRISTINE 2
#define FM_LOAD_2855 3
#define FM_LOAD_JX101 4
#define FM_LOAD_SX399 5
#define FM_LOAD_TOTAL 6

//--Loading Screen Sizes
#define FM_FORM_HUMAN 0
#define FM_FORM_ALRAUNE 1
#define FM_FORM_BEE 2
#define FM_FORM_SLIME 3
#define FM_FORM_WERECAT 4
#define FM_FORM_GHOST 5
#define FM_FORM_MALE 6
#define FM_FORM_GOLEM 7
#define FM_FORM_LATEX 8
#define FM_FORM_DOLL 9
#define FM_FORM_STEAMDROID 10
#define FM_FORM_DARKMATTER 11
#define FM_FORM_ELDRITCH 12
#define FM_FORM_STEAMLORD 13
#define FM_FORM_RAIJU 14
#define FM_FORM_ELECTROSPRITE 15
#define FM_FORM_TOTAL 16

//--Control Rebinding Constants
#define FM_CONTROLS_TOTAL 9

//--Loading Constants
#define FM_FILES_PER_PAGE 4

//--Cheat Codes
#define COUNT_TREASURES_WORD "glitteringprizes"
#define COUNT_TREASURES_LEN 16

//--[Classes]
class FlexMenu : public SugarMenu
{
    private:
    //--System
    int mCountTreasureProgress;
    bool mClearedSelfThisTick;
    bool mHasSortedRecently;

    //--Selection
    int mSelectedOption;
    int mPreviousMouseX, mPreviousMouseY, mPreviousMouseZ;

    //--Introduction Mode
    bool mIsIntroMode;
    int mIntroTimer;

    //--Loading Mode
    bool mIsLoadingMode;
    int mLoadingTimer;
    int mLoadingCursor;
    int mLoadingOffset;
    TwoDimensionReal mBackBtn; //Shared with Rebinding mode
    TwoDimensionReal mControlsBtn;
    TwoDimensionReal mScrollUp;
    TwoDimensionReal mScrollDn;
    SugarLinkedList *mLoadingPackList;
    bool mIsEnteringString;
    int mFileStringEntering;
    StringEntry *mStringEntryForm;

    //--Scrollbar Click
    bool mIsClickingScrollbar;
    float mScrollbarClickStartY;

    //--Rebinding Mode
    bool mIsRebindingMode;
    int mRebindingCursor;
    bool mIsRebindSecondary;
    int mCurrentRebindKey;
    char **mControlNames;
    int mCurrentScancodes[FM_CONTROLS_TOTAL][6];
    TwoDimensionReal mControlCoordinates[FM_CONTROLS_TOTAL];
    TwoDimensionReal mSaveBtn;
    TwoDimensionReal mDefaultsBtn;

    //--Header
    bool mHasHeader;
    int mHeaderLinesTotal;
    HeaderPack *mHeaderLines;
    float mHeaderWid, mHeaderHei;
    float cHeaderPadding;

    //--Descriptions
    int mLinesOccupied;
    bool mShowDescriptions;
    void *rLastHighlightedBtn;
    SugarBitmap *rDescriptionImg;
    TwoDimensionReal mDescriptionDim;
    TwoDimensionReal mDescriptionImgDim;
    char mDescriptionLines[FM_DESCRIPTION_LINES][FM_DESCRIPTION_CHARS];

    //--Rendering Constants
    bool mCanRender;
    float cBorder;
    float cButtonIndentX, cButtonIndentY;
    float cFontSize;
    float cStepRate;
    char *mNameRemappings[FM_LOAD_TOTAL];
    char *mFormRemappings[FM_FORM_TOTAL];

    //--Rendering Variables
    SugarFont *rOverrideFont;

    //--Location
    TwoDimensionReal mCoordinates;

    //--Storage
    SugarLinkedList *mButtonStorage;

    //--Images
    struct
    {
        bool mIsReady;
        struct
        {
            //--Base.
            SugarBitmap *rBorderCard;
            SugarBitmap *rUpArrow;
            SugarBitmap *rDnArrow;
            SugarFont *rUIFont;

            //--Sprites.
        }Data;

        //--Loading sprites. Not verified, some can be NULL.
        SugarBitmap *rSprites[FM_LOAD_TOTAL * FM_FORM_TOTAL];
    }Images;

    protected:

    public:
    //--System
    FlexMenu();
    virtual ~FlexMenu();
    void Construct();

    //--Public Variables
    static bool xUseDummySprites;
    static bool xUseClassicUI;
    static int xNoHighlightTimer;
    static bool xNeedsToLoadControls;

    //--Property Queries
    SugarFont *ResolveFont();

    //--Manipulators
    virtual void Clear();
    void SetDescriptionFlag(bool pFlag);
    void ResetHeader(int pHeaderLines);
    void SetHeaderLine(int pIndex, const char *pString, float pFontSize);
    virtual void RegisterButton(SugarButton *pButton);
    void ActivateIntroMode();

    //--Core Methods
    void RefreshDescriptionFrom(FlexButton *pButton);

    //--Loading
    void ActivateLoadingMode();
    void DeactivateLoadingMode();
    void CrossloadSaveImages(LoadingPack *pPack);
    void UpdateLoadingMode();
    void RenderLoadingMode();

    //--Rebinding
    bool IsRebindingMode();
    void ActivateRebindingMode();
    void DeactivateRebindingMode(bool pSave);
    void DeactivateRebindingModeDefaults();
    void UpdateRebindingMode();
    void RenderRebindingMode();

    private:
    //--Private Core Methods
    void AutoSortButtons();
    static int CompareButtonPriorities(const void *pEntryA, const void *pEntryB);
    bool HandleCheatCode(const char *pPressedKey, int &sProgressVar, int pMaxLength, const char *pCheatCode);
    void CountTreasures();

    public:
    //--Update
    void Update();
    void UpdateInactive();

    //--File I/O
    //--Drawing
    void Render();

    //--Pointer Routing
    SugarLinkedList *GetButtonList();
    SugarButton *GetButtonI(int pIndex);
    SugarButton *GetButtonS(const char *pName);

    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_FlexMenu_Open(lua_State *L);
int Hook_FlexMenu_FlagClose(lua_State *L);
int Hook_FlexMenu_Clear(lua_State *L);
int Hook_FlexMenu_RegisterButton(lua_State *L);
int Hook_FlexMenu_PushButton(lua_State *L);
int Hook_FlexMenu_SetHeaderSize(lua_State *L);
int Hook_FlexMenu_SetHeaderLine(lua_State *L);
int Hook_FlexMenu_ActivateIntro(lua_State *L);
int Hook_FlexMenu_ActivateLoading(lua_State *L);
int Hook_FlexMenu_SetDummySpriteFlag(lua_State *L);

