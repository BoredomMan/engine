//--[CameraEvent]
//--Event which modifies the camera in some way.

#pragma once

//--[Includes]
#include "Definitions.h"
#include "Structures.h"
#include "RootEvent.h"

//--[Local Structures]
//--[Local Definitions]
#define CE_TYPE_NONE 0
#define CE_TYPE_FOCUS_ACTOR 1
#define CE_TYPE_FOCUS_POINT 2

//--[Classes]
class CameraEvent : public RootEvent
{
    private:

    protected:
    //--System
    bool mIsComplete;
    int mInstructionType;

    //--Speeds
    float mMaxMoveSpeed;

    //--Data Storage
    union
    {
        struct
        {
            uint32_t mActorID;
        }FocusActor;
        struct
        {
            float mPointX;
            float mPointY;
        }FocusPoint;
    }Data;

    public:
    //--System
    CameraEvent();
    virtual ~CameraEvent();

    //--Public Variables
    //--Property Queries
    virtual bool IsOfType(int pType);

    //--Manipulators
    void SetMaxMoveSpeed(float pAmount);
    void SetFocusActorByID(uint32_t pID);
    void SetFocusActorByName(const char *pName);
    void SetFocusPoint(float pX, float pY);

    //--Core Methods
    bool MoveCameraTowards(TwoDimensionReal &sSubject, TwoDimensionReal pTarget);
    virtual bool IsComplete();

    private:
    //--Private Core Methods
    public:
    //--Update
    virtual void Update();

    //--File I/O
    //--Drawing
    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_CameraEvent_SetProperty(lua_State *L);
int Hook_CameraEvent_GetProperty(lua_State *L);
