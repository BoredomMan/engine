//--[TimerEvent]
//--Event that uses an internal timer to complete.

#pragma once

//--[Includes]
#include "Definitions.h"
#include "Structures.h"
#include "RootEvent.h"

//--[Local Structures]
//--[Local Definitions]
//--[Classes]
class TimerEvent : public RootEvent
{
    private:

    protected:
    //--System
    //--Storage
    int mTimer;
    int mTimerMax;

    public:
    //--System
    TimerEvent();
    virtual ~TimerEvent();

    //--Public Variables
    //--Property Queries
    virtual bool IsOfType(int pType);
    int GetTicks();
    int GetTicksMax();

    //--Manipulators
    void SetTicks(int pTicks);
    void SetTicks(int pTicks, int pTicksMax);

    //--Core Methods
    virtual bool IsComplete();

    private:
    //--Private Core Methods
    public:
    //--Update
    virtual void Update();

    //--File I/O
    //--Drawing
    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_TimeEvent_SetProperty(lua_State *L);
int Hook_TimeEvent_GetProperty(lua_State *L);
