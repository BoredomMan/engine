//--Base
#include "TransformPack.h"

//--Classes
//--CoreClasses
#include "SugarLinkedList.h"

//--Definitions
//--GUI
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "LuaManager.h"

//=========================================== System ==============================================
TransformPack::TransformPack()
{
    //--[RootObject]
    //--System
    mType = POINTER_TYPE_TRANSFORMPACK;

    //--[TransformPack]
    //--System
    mLocalName = NULL;
    mScriptPath = NULL;

    //--Display
    rDisplayImage = NULL;
    mDescription = NULL;
}
TransformPack::~TransformPack()
{
    free(mLocalName);
    free(mScriptPath);
    free(mDescription);
}

//--[Public Statics]
//--List of all transformations currently available, held statically for easy access.
SugarLinkedList *TransformPack::xTransformationList = new SugarLinkedList(true);

//====================================== Property Queries =========================================
const char *TransformPack::GetName()
{
    return mLocalName;
}
const char *TransformPack::GetScript()
{
    return mScriptPath;
}
const char *TransformPack::GetDescription()
{
    return mDescription;
}
SugarBitmap *TransformPack::GetDisplayImage()
{
    return rDisplayImage;
}

//========================================= Manipulators ==========================================
void TransformPack::SetName(const char *pName)
{
    ResetString(mLocalName, pName);
}
void TransformPack::SetScript(const char *pPath)
{
    ResetString(mScriptPath, pPath);
}
void TransformPack::SetDescription(const char *pPath)
{
    ResetString(mDescription, pPath);
}
void TransformPack::SetDisplayImageS(const char *pDLPath)
{
    rDisplayImage = (SugarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath);
}

//========================================= Core Methods ==========================================
void TransformPack::Execute()
{
    //--Executes the script for the TransformPack, assuming it exists. The pack will push itself
    //  atop the Activity Stack.
    LuaManager::Fetch()->PushExecPop(this, mScriptPath, 1, "N", (float)TFPACK_EXECUTE);
}

//===================================== Private Core Methods ======================================
//============================================ Update =============================================
//=========================================== File I/O ============================================
//=========================================== Drawing =============================================
//======================================= Pointer Routing =========================================
//====================================== Static Functions =========================================
//========================================= Lua Hooking ===========================================
void TransformPack::HookToLuaState(lua_State *pLuaState)
{
    /* TF_Register(sTransformationName)
       Creates, registers, and pushes a new TransformPack. Remember to pop it when you're done. */
    lua_register(pLuaState, "TF_Register", &Hook_TF_Register);

    /* TF_ClearList()
       Clears all TransformPacks from the static list. Calling this while one is in use will have
       undefined results, so don't. */
    lua_register(pLuaState, "TF_ClearList", &Hook_TF_ClearList);

    /* TF_GetProperty("Name") (1 string)
       Returns the requested property in the TransformPack on top of the Activity Stack. */
    lua_register(pLuaState, "TF_GetProperty", &Hook_TF_GetProperty);

    /* TF_SetProperty("Path", sPath)
       TF_SetProperty("Image", sDLPath)
       TF_SetProperty("Description", sDescription)
       Sets the requested property in the TransformPack on top of the Activity Stack. */
    lua_register(pLuaState, "TF_SetProperty", &Hook_TF_SetProperty);
}

//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
int Hook_TF_Register(lua_State *L)
{
    //TF_Register(sTransformationName)
    DataLibrary::Fetch()->PushActiveEntity();

    //--Arg check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("TF_Register");

    //--Create.
    TransformPack *nPack = new TransformPack();
    nPack->SetName(lua_tostring(L, 1));

    //--Register.
    DataLibrary::Fetch()->rActiveObject = nPack;
    TransformPack::xTransformationList->AddElement(lua_tostring(L, 1), nPack, &RootObject::DeleteThis);
    return 0;
}
int Hook_TF_ClearList(lua_State *L)
{
    //TF_ClearList()
    TransformPack::xTransformationList->ClearList();
    return 0;
}
int Hook_TF_GetProperty(lua_State *L)
{
    //TF_GetProperty("Name") (1 string)
    int tArgs = lua_gettop(L);

    //--Get the class.
    TransformPack *rTransformPack = (TransformPack *)DataLibrary::Fetch()->rActiveObject;
    if(!rTransformPack || !rTransformPack->IsOfType(POINTER_TYPE_TRANSFORMPACK)) return LuaTypeError("TF_GetProperty");

    //--Switching.
    int tReturns = 0;
    const char *rSwitchType = lua_tostring(L, 1);

    //--Transformation's name.
    if(!strcasecmp(rSwitchType, "Name") && tArgs == 1)
    {
        tReturns = 1;
        lua_pushstring(L, rTransformPack->GetName());
    }
    //--Unknown type.
    else
    {
        LuaPropertyNoMatchError("Perk_GetProperty", rSwitchType);
    }

    return tReturns;
}
int Hook_TF_SetProperty(lua_State *L)
{
    //TF_SetProperty("Path", sPath)
    //TF_SetProperty("Image", sDLPath)
    //TF_SetProperty("Description", sDescription)
    int tArgs = lua_gettop(L);
    const char *rSwitchType = lua_tostring(L, 1);

    //--Get the class.
    TransformPack *rTransformPack = (TransformPack *)DataLibrary::Fetch()->rActiveObject;
    if(!rTransformPack || !rTransformPack->IsOfType(POINTER_TYPE_TRANSFORMPACK)) return LuaTypeError("TF_SetProperty");

    //--What it costs to unlock.
    if(!strcasecmp(rSwitchType, "Path") && tArgs == 2)
    {
        rTransformPack->SetScript(lua_tostring(L, 2));
    }
    //--How many times the perk can be unlocked.
    else if(!strcasecmp(rSwitchType, "Image") && tArgs == 2)
    {
        rTransformPack->SetDisplayImageS(lua_tostring(L, 2));
    }
    //--How many times the perk can be unlocked.
    else if(!strcasecmp(rSwitchType, "Description") && tArgs == 2)
    {
        rTransformPack->SetDescription(lua_tostring(L, 2));
    }
    //--Unknown type.
    else
    {
        LuaPropertyNoMatchError("TF_SetProperty", rSwitchType);
    }

    return 0;
}
