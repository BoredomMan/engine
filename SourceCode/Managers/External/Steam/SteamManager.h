//--[SteamManager]
//--Manages the Steam interface. Does nothing if the defines are not set.

#pragma once

//--[Includes]
#include "Definitions.h"
#include "Structures.h"
#if defined _STEAM_API_
    #include "steam_api.h"
#endif

//--[Local Structures]
//--[Local Definitions]
//--[Classes]
class SteamManager
{
    private:
    #if defined _STEAM_API_
    //--System
    bool mIsSteamRunning;




    #endif

    protected:

    public:
    //--System
    SteamManager();
    ~SteamManager();

    //--Public Variables
    static int xSteamAppID;

    //--Property Queries
    //--Manipulators
    //--Core Methods
    private:
    //--Private Core Methods
    public:
    //--Update
    //--File I/O
    //--Drawing
    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions

