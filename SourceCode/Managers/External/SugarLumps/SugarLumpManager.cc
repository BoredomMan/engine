//--Base
#include "SugarLumpManager.h"

//--Classes
//--CoreClasses
#include "SugarLinkedList.h"
#include "VirtualFile.h"

//--Definitions
//--GUI
//--Libraries
//--Managers
#include "DebugManager.h"

//#define SLF_DEBUG
#ifdef SLF_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

//=========================================== System ==============================================
SugarLumpManager::SugarLumpManager()
{
    //--System
    mIsOpen = false;
    mOpenPath = NULL;
    mFileVersion = 0;

    //--File
    mVFile = NULL;

    //--Chunks
    mTotalLumps = 0;
    mLumpList = NULL;

    //--New Lumps
    mNewLumpList = new SugarLinkedList(true);
}
SugarLumpManager::~SugarLumpManager()
{
    Clear();
    delete mNewLumpList;
}
void SugarLumpManager::DeleteRootLump(void *pPtr)
{
    RootLump *rPtr = (RootLump *)pPtr;
    free(rPtr->mLookup.mName);
    if(rPtr->mOwnsData) free(rPtr->mData);
    free(rPtr);
}

//--[Public Statics]
bool SugarLumpManager::xIsCaseInsensitive = false;

//--Used to toggle on and off debug prints. The #define above must also be set or this does nothing.
bool SugarLumpManager::xDebugFlag = false;

//--Sizes for tiles. Default is 16. Used when extracting tile information from SLF level files.
int SugarLumpManager::xTileSizeX = DEFAULT_TILE_SIZE;
int SugarLumpManager::xTileSizeY = DEFAULT_TILE_SIZE;

//====================================== Property Queries =========================================
bool SugarLumpManager::IsFileOpen()
{
    return mIsOpen;
}
char *SugarLumpManager::GetOpenPath()
{
    return mOpenPath;
}
int SugarLumpManager::GetTotalLumps()
{
    return mTotalLumps;
}
bool SugarLumpManager::DoesLumpExist(const char *pName)
{
    if(!pName) return false;
    int tArrayPos = BinaryGetLookup(pName, 0, mTotalLumps-1);
    if(tArrayPos == -1) return false;
    return true;
}
char *SugarLumpManager::GetHeaderOf(const char *pName)
{
    //--Note: Can return NULL if there's no lump by that name. The name will otherwise be a heap
    //  copy that must be deleted when you're done with it.
    int tArrayPos = BinaryGetLookup(pName, 0, mTotalLumps-1);
    if(tArrayPos == -1) return NULL;

    //--Store the old position.
    uint32_t tOldPos = mVFile->GetCurPosition();

    //--Seek to this position.
    mVFile->Seek(mLumpList[tArrayPos].mLookup.mPosition);

    //--Read out the Lump header.
    SetMemoryData(__FILE__, __LINE__);
    char *nHeaderString = (char *)starmemoryalloc(sizeof(char) * 11);
    mVFile->Read(nHeaderString, sizeof(char), 10);
    nHeaderString[10] = '\0';

    //--Return the VFile to the original position.
    mVFile->Seek(tOldPos);

    //--Pass back the copied name.
    return nHeaderString;
}

//========================================= Manipulators ==========================================
//========================================= Core Methods ==========================================
int SugarLumpManager::BinaryGetLookup(const char *pName, int pMin, int pMax)
{
    //--Does a binary search on the mLumpList array.  This is a recursive call, and therefore does
    //  less NULL checking than would be necessary.

    //--Array is empty
    if(pMax < pMin)
    {
        return -1;
    }

    //--Find the halfway point, cut the set in half
    int tMid = (pMin + pMax) / 2;

    //--Check the given position.
    const char *rCheckName = mLumpList[tMid].mLookup.mName;

    //--Case sensitive:
    int tRet = 0;
    if(!xIsCaseInsensitive)
    {
        tRet = strcmp(pName, rCheckName);
    }
    else
    {
        tRet = strcasecmp(pName, rCheckName);
    }

    //--After Midpoint
    if(tRet > 0)
    {
        return BinaryGetLookup(pName, tMid+1, pMax);
    }
    //--Before midpoint
    else if(tRet < 0)
    {
        return BinaryGetLookup(pName, pMin, tMid-1);
    }
    //--Match
    else
    {
        return tMid;
    }
    return -1;
}
bool SugarLumpManager::StandardSeek(const char *pLumpName, const char *pHeaderType)
{
    //--Locates the named Lump, and checks if it is of the named Type.  If all went well, returns
    //  true, false on error.  The VFile is left at the end of the header.
    if(!mIsOpen || !pLumpName || !pHeaderType) return false;
    DebugManager::PushPrint(false, "Seeking to %s\n", pLumpName);

    //--Locate the Lump.  We can use a binary search since the headers are sorted.
    int tArrayPos = BinaryGetLookup(pLumpName, 0, mTotalLumps-1);
    if(tArrayPos == -1)
    {
        DebugManager::PopPrint("Lump wasn't found\n");
        return false;
    }

    //--Seek to the location in the file.
    DebugManager::Print("Slot %i\n", tArrayPos);
    DebugManager::Print("Position %i\n", mLumpList[tArrayPos].mLookup.mPosition);
    mVFile->Seek(mLumpList[tArrayPos].mLookup.mPosition);

    //--Read out the Lump header.
    char tHeader[11];
    mVFile->Read(tHeader, sizeof(char), 10);
    tHeader[10] = '\0';
    DebugManager::Print("Check %s vs %s\n", tHeader, pHeaderType);
    if(strncmp(tHeader, pHeaderType, strlen(pHeaderType)))
    {
        DebugManager::PopPrint("Lump was of the wrong type\n");
        return false;
    }
    DebugManager::PopPrint("Finished, Header was %s\n", tHeader);
    return true;
}
bool SugarLumpManager::SlotwiseSeek(int pSlot)
{
    //--Seeks to a specific slot, instead of doing a binary search.  The type is not checked, but
    //  the header is still seeked past.  Returns true on success, false on error.
    if(pSlot < 0 || pSlot >= (int)mTotalLumps) return false;

    //--Seek.
    mVFile->Seek(mLumpList[pSlot].mLookup.mPosition);

    //--Read out the Lump header.
    char tHeader[11];
    mVFile->Read(tHeader, sizeof(char), 10);
    tHeader[10] = '\0';

    //--Done.
    return true;
}
void SugarLumpManager::Clear()
{
    //--[Delete]
    //--Clears all existing data and resets back to factory-zero.
    DebugPush(xDebugFlag, "Clearing all data from SLM.\n");
    free(mOpenPath);
    delete mVFile;

    //--Lumps
    DebugPrint("Clearing lump info.\n");
    for(uint32_t i = 0; i < mTotalLumps; i ++) free(mLumpList[i].mLookup.mName);
    free(mLumpList);

    //--New Lumps
    DebugPrint("Clearing new lump list.\n");
    mNewLumpList->ClearList();

    //--[Initialize]
    //--System
    DebugPrint("Re-nulling system variables.\n");
    mIsOpen = false;
    mOpenPath = NULL;
    mFileVersion = 0;

    //--File
    mVFile = NULL;

    //--Lumps
    mTotalLumps = 0;
    mLumpList = NULL;
    DebugPop("Finished clearing SLM data.\n");
}

//============================================ Update =============================================
//=========================================== File I/O ============================================
void SugarLumpManager::Open(const char *pPath)
{
    //--Overload for the Open() function below. This assumes the standard header is in use.
    Open(pPath, STANDARD_HEADER);
}
void SugarLumpManager::Open(const char *pPath, const char *pHeader)
{
    //--[Documentation and Setup]
    //--Opens a file, parses out all its data and stores it in the list and header.
    DebugPush(xDebugFlag, "==> SugarLumpManager: Opening file.\n");
    if(!pPath)
    {
        mSuppressErrorOnce = false;
        DebugManager::ForcePrint("No path provided, exiting.\n");
        DebugPop("");
        return;
    }
    DebugPrint("File was %s\n", pPath);

    //--Was the path identical to the last opened SLF?  Then it's already open!
    if(mOpenPath && !strcmp(pPath, mOpenPath))
    {
        mSuppressErrorOnce = false;
        DebugPop("File was already open, exiting.\n");
        return;
    }
    DebugPrint("Closing previous file.\n");
    if(mOpenPath)
    {
        DebugPrint(" Previous file was %s\n", mOpenPath);
    }
    Close();
    DebugPrint("Finished closing previous file.\n");

    //--Virtual file, should specify the loading flag as virtual or real depending on how much of
    //  the file you intend to read.
    DebugPrint("Opening virtual file.\n");
    mIsOpen = true;
    mVFile = new VirtualFile(pPath, false);
    if(!mVFile->IsReady())
    {
        if(!mSuppressErrorOnce) DebugManager::ForcePrint("SugarLumpManager:  Error, could not find %s\n", pPath);
        mSuppressErrorOnce = false;
        Close();
        DebugPop("Could not find file.\n");
        return;
    }

    //--Store the path
    DebugPrint("Storing path.\n");
    ResetString(mOpenPath, pPath);

    //--Header. Do not read more characters than the expected maximum!
    DebugPrint("Checking header.\n");
    int tCharsToRead = (int)strlen(pHeader);
    if(tCharsToRead >= HEADER_MAX_LEN) tCharsToRead = HEADER_MAX_LEN - 1;

    //--Parse the header into the buffer.
    DebugPrint("Reading header %i.\n", tCharsToRead);
    mVFile->mDebugFlag = xDebugFlag;
    mVFile->Read(mHeader, sizeof(char), tCharsToRead);
    DebugPrint("Finished reading header.\n", tCharsToRead);
    mVFile->mDebugFlag = false;
    mHeader[tCharsToRead] = '\0';

    //--Compare the received header against the expected one. If they don't match, this file is not
    //  the right type.
    DebugPrint("Comparing header.\n");
    if(strcmp(mHeader, pHeader))
    {
        if(!mSuppressErrorOnce) DebugManager::ForcePrint("Error, file header %s does not match expected %s\n", mHeader, pHeader);
        mSuppressErrorOnce = false;
        Close();
        DebugPop("File had unexpected header.\n");
        return;
    }

    //--Total number of Lumps
    DebugPrint("Booting lumps.\n");
    mVFile->Read(&mTotalLumps, sizeof(uint32_t), 1);
    SetMemoryData(__FILE__, __LINE__);
    mLumpList = (RootLump *)starmemoryalloc(sizeof(RootLump) * mTotalLumps);
    for(int i = 0; i < (int)mTotalLumps; i ++)
    {
        mLumpList[i].Init();
    }

    //--Read each of the lookups. Set the data pointers for the lumps to these lookups. This is faster
    //  when in memory mode, but not mandated.
    DebugPrint("Reading lumps.\n");
    if(mVFile->IsInMemoryMode())
    {
        //--Get the master data pointer.
        uint8_t *rMasterDataPtr = mVFile->GetData();

        //--For each lump, copy it from the file.
        uint8_t tLen = 0;
        for(uint32_t i = 0; i < mTotalLumps; i ++)
        {
            //--These lumps do not own their own data, the virtual file does.
            mLumpList[i].mOwnsData = false;

            //--Read the length of the name.
            mVFile->Read(&tLen, sizeof(uint8_t), 1);

            //--Read the name, store it, and add a null.
            SetMemoryData(__FILE__, __LINE__);
            mLumpList[i].mLookup.mName = (char *)starmemoryalloc(sizeof(char) * (tLen+1));
            mVFile->Read(mLumpList[i].mLookup.mName, sizeof(char), tLen);
            mLumpList[i].mLookup.mName[tLen] = '\0';

            //--Get the position and set the data pointer.
            mVFile->Read(&mLumpList[i].mLookup.mPosition, sizeof(uint64_t), 1);
            mLumpList[i].mData = &rMasterDataPtr[mLumpList[i].mLookup.mPosition];
        }
    }
    //--When in stream mode, the lump takes ownership but does not read its data yet, because we don't
    //  know how large the lump is.
    else
    {
        //--Setup.
        uint8_t tLen = 0;

        //--Read.
        for(uint32_t i = 0; i < mTotalLumps; i ++)
        {
            //--The lumps own their own data.
            mLumpList[i].mOwnsData = true;

            //--Read the length of the name.
            mVFile->Read(&tLen, sizeof(uint8_t), 1);

            //--Read the name, store it, and add a null.
            SetMemoryData(__FILE__, __LINE__);
            mLumpList[i].mLookup.mName = (char *)starmemoryalloc(sizeof(char) * (tLen+1));
            mVFile->Read(mLumpList[i].mLookup.mName, sizeof(char), tLen);
            mLumpList[i].mLookup.mName[tLen] = '\0';

            //--Get the position.
            mVFile->Read(&mLumpList[i].mLookup.mPosition, sizeof(uint64_t), 1);

            //--Mark the lump as having no data - yet.
            mLumpList[i].mData = NULL;
        }
    }

    //--Determine how many bytes each lump should be.
    DebugPrint("Indexing lumps.\n");
    for(uint32_t i = 0; i < mTotalLumps-1; i ++)
    {
        //--The size of a lump is the next lump's position minus this one. Lumps are always space-filling.
        mLumpList[i].mDataSize = mLumpList[i+1].mLookup.mPosition -  mLumpList[i+0].mLookup.mPosition;
        //fprintf(stderr, "Lump %i %i\n", i, mLumpList[i].mDataSize);

        //--In stream mode, we need to get the lump's data here.
        if(!mVFile->IsInMemoryMode())
        {
            //--Seek to the lump's memory position.
            mVFile->Seek(mLumpList[i].mLookup.mPosition);

            //--Read it.
            mVFile->Read(mLumpList[i].mData, mLumpList[i].mDataSize, 1);
        }
    }

    //--Last lump uses the end-of-file for its size.
    DebugPrint("Setting EOF.\n");
    if(mTotalLumps > 0)
    {
        mLumpList[mTotalLumps-1].mDataSize = mVFile->GetLength() -  mLumpList[mTotalLumps-1].mLookup.mPosition - 1;
        //fprintf(stderr, "Lump %i %i\n", mTotalLumps-1, mLumpList[mTotalLumps-1].mDataSize);
    }

    //--Clear flags.
    mSuppressErrorOnce = false;
    DebugPop("Finished opening file.\n");
}
void SugarLumpManager::Close()
{
    //--Closes and clears everything, resetting to a neutral state. Basically identical to close.
    //  Note that this can therefore be used after Write() to clear the data.
    Clear();
}

//=========================================== Drawing =============================================
//======================================= Pointer Routing =========================================
VirtualFile *SugarLumpManager::GetVirtualFile()
{
    return mVFile;
}

//====================================== Static Functions =========================================
#include "Global.h"
SugarLumpManager *SugarLumpManager::Fetch()
{
    return Global::Shared()->gSugarLumpManager;
}
RootLump *SugarLumpManager::CreateLump(const char *pName, const char *pHeader, uint32_t pExpectedSize)
{
    //--Creates and returns a RootLump with the given name and expected size. Headers are usually
    //  exactly 10 bytes with no closing null.
    if(!pName || !pHeader || pExpectedSize < 10 || pExpectedSize < strlen(pHeader)) return NULL;

    //--Subroutine.
    RootLump *nLump = SugarLumpManager::CreateLump(pName);

    //--Allocate. The lump now owns the given data, though the space is empty.
    nLump->mOwnsData = true;
    nLump->mDataSize = pExpectedSize;
    SetMemoryData(__FILE__, __LINE__);
    nLump->mData = (uint8_t *)starmemoryalloc(sizeof(uint8_t) * nLump->mDataSize);
    memcpy(nLump->mData, pHeader, (int)strlen(pHeader) * sizeof(char));

    //--Done.
    return nLump;
}
RootLump *SugarLumpManager::CreateLump(const char *pName)
{
    //--Creates and returns a RootLump with nullified data. You must provide data before sending it
    //  off for use. Good if you don't know how big the lump is going to be.
    //--The flag mOwnsData is used to determine if this lump should deallocate its data block when
    //  it gets deallocated. If it's referencing something else, leave it false.
    if(!pName) return NULL;

    //--Allocate.
    SetMemoryData(__FILE__, __LINE__);
    RootLump *nLump = (RootLump *)starmemoryalloc(sizeof(RootLump));
    nLump->Init();
    ResetString(nLump->mLookup.mName, pName);

    //--Done.
    return nLump;
}

//========================================= Lua Hooking ===========================================
void SugarLumpManager::HookToLuaState(lua_State *pLuaState)
{
    /* SLF_SetDebugFlag(bFlag)
       Sets the debug flag on or off. Debug shows prints when opening files. */
    lua_register(pLuaState, "SLF_SetDebugFlag", &Hook_SLF_SetDebugFlag);

    /* SLF_Open(sPath[])
       Opens the specified SugarLumpFile.  All read operations will fail if a file is not open.
       Opening multiple files concurrently is not supported, and opening a new file will close the
       other one, if there was one.*/
    lua_register(pLuaState, "SLF_Open", &Hook_SLF_Open);

    /* SLF_Close()
       Closes and deallocated the opened SLF file.  Does nothing if one wasn't open. */
    lua_register(pLuaState, "SLF_Close", &Hook_SLF_Close);

    /* SLF_IsFileOpen()
       Returns true if a file is currently open in the SLF, false if not. */
    lua_register(pLuaState, "SLF_IsFileOpen", &Hook_SLF_IsFileOpen);

    /* SLF_GetOpenPath()
       Returns the path of the currently open file.  Returns "NULL" on failure. */
    lua_register(pLuaState, "SLF_GetOpenPath", &Hook_SLF_GetOpenPath);
}

//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
int Hook_SLF_SetDebugFlag(lua_State *L)
{
    //SLF_SetDebugFlag(bFlag)
    int pArgs = lua_gettop(L);
    if(pArgs != 1) return LuaArgError("SugarLumpManager");
    SugarLumpManager::xDebugFlag = lua_toboolean(L, 1);
    return 0;
}
int Hook_SLF_Open(lua_State *L)
{
    //SLF_Open(sPath[])
    int pArgs = lua_gettop(L);
    if(pArgs != 1) return LuaArgError("SugarLumpManager");

    SugarLumpManager::Fetch()->Open(lua_tostring(L, 1));

    return 0;
}
int Hook_SLF_Close(lua_State *L)
{
    //SLF_Close()

    SugarLumpManager::Fetch()->Close();

    return 0;
}
int Hook_SLF_IsFileOpen(lua_State *L)
{
    //SLF_IsFileOpen()
    lua_pushboolean(L, SugarLumpManager::Fetch()->IsFileOpen());
    return 1;
}
int Hook_SLF_GetOpenPath(lua_State *L)
{
    //SLF_GetOpenPath()
    char *rPath = SugarLumpManager::Fetch()->GetOpenPath();
    if(!rPath)
    {
        lua_pushstring(L, "NULL");
    }
    else
    {
        lua_pushstring(L, rPath);
    }
    return 1;
}
