//--Base
#include "SugarLumpManager.h"

//--Classes
//--CoreClasses
#include "VirtualFile.h"

//--Definitions
//--GUI
//--Libraries
//--Managers
#include "DebugManager.h"

int SugarLumpManager::GetMapType(const char *pLumpName)
{
    //--Scans for the requested Lump.  If it exists, returns the type of the map that this file
    //  contains.  If not, returns an error.
    if(!mIsOpen || !pLumpName) return MAP_INVALID;
    DebugManager::PushPrint(false, "Finding map type in %s\n", pLumpName);

    //--Locate it and seek to it.
    if(!StandardSeek(pLumpName, "MAPINFO"))
    {
        DebugManager::PopPrint("Error in SLF file\n");
        return MAP_INVALID;
    }

    //--Get the map's type.  That's all we need.
    uint8_t tType = 0;
    mVFile->Read(&tType, sizeof(uint8_t), 1);
    DebugManager::PopPrint("Map type %i\n", tType);
    return tType;
}
