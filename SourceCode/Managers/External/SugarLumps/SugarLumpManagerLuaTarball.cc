//--Base
#include "SugarLumpManager.h"

//--Classes
//--CoreClasses
#include "VirtualFile.h"

//--Definitions
//--GUI
//--Libraries
//--Managers
#include "DebugManager.h"

LuaTarballEntry *SugarLumpManager::GetTarball(int pSlot)
{
    //--Extracts the data from a LuaTarball and passes it back as a heap object.  You are responsible
    //  for deallocation of the object, and the object is a COPY.  Multiple tarball objects can be
    //  extracted from the same slot.
    //--This is done by SLOT, not PATH or NAME.  NULL is returned on error.
    if(pSlot < 0 || pSlot >= (int)mTotalLumps) return NULL;
    DebugManager::PushPrint(false, "Loading Tarball Data %i\n", pSlot);

    //--Create a heap tarball object.
    SetMemoryData(__FILE__, __LINE__);
    LuaTarballEntry *nEntry = (LuaTarballEntry *)starmemoryalloc(sizeof(LuaTarballEntry));
    nEntry->mName = NULL;
    nEntry->mBinarySize = 0;
    nEntry->mBinaryData = NULL;

    //--Get the lookup and seek to the entry.
    SlotwiseSeek(pSlot);

    //--[Program Only]
    //--To save execution space, append "Data/Scripts/" to the name here.
    char tBuffer[512];
    strcpy(tBuffer, "Data/Scripts/");
    strcat(tBuffer, mLumpList[pSlot].mLookup.mName);

    //--Save the name of the lump.
    ResetString(nEntry->mName, tBuffer);

    //--Header is auto-read.  Read the size and allocate space.
    mVFile->Read(&nEntry->mBinarySize, sizeof(uint32_t), 1);
    SetMemoryData(__FILE__, __LINE__);
    nEntry->mBinaryData = (uint8_t *)starmemoryalloc(sizeof(uint8_t) * nEntry->mBinarySize);

    //--Read the data.
    mVFile->Read(nEntry->mBinaryData, nEntry->mBinarySize * sizeof(uint8_t), 1);

    //--Done.
    DebugManager::PopPrint("Tarball data loaded\n");
    return nEntry;
}
