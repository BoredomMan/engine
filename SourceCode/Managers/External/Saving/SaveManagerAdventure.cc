//--Base
#include "SaveManager.h"

//--Classes
#include "AdvCombat.h"
#include "AdventureInventory.h"
#include "AdventureLevel.h"
#include "FlexMenu.h"
#include "WorldDialogue.h"

//--CoreClasses
#include "SugarAutoBuffer.h"
#include "VirtualFile.h"

//--Definitions
//--Libraries
#include "DataLibrary.h"

//--Managers
#include "AudioManager.h"
#include "ControlManager.h"
#include "DebugManager.h"
#include "LuaManager.h"

//--[Debug]
//#define SAVE_DEBUG
#ifdef SAVE_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

//--[Forward Declarations]
bool CheckHeader(VirtualFile *fInfile, const char *pComparison);

//--[Verification]
LoadingPack *SaveManager::GetSaveInfo(const char *pFilePath)
{
    //--Given a file on the hard drive, checks if it's a valid save file. If it is, returns a heap-allocated
    //  LoadingPack (defined in FlexMenu.h) which stores basic information about the save.
    //--If the file does not exist or is not a valid save, returns NULL.
    if(!pFilePath) return NULL;

    //--[Header Check]
    //--Open, check it's a valid file. It should have the correct header.
    VirtualFile *fInfile = new VirtualFile(pFilePath, false);
    if(!fInfile->IsReady()) { delete fInfile; return NULL; }

    //--This is done manually since there are two legal headers.
    xIsStarv100Savefile = false;
    char tInBuffer[32];
    memset(tInBuffer, 0, sizeof(char) * 32);
    fInfile->Read(tInBuffer, sizeof(char), (int)strlen("STARv101"));

    //--If this is a STARv100 header, the DataLibrary will be using a single digit to indicate string length.
    if(!strcasecmp(tInBuffer, "STARv100"))
    {
        xIsStarv100Savefile = true;
    }
    //--If this is a STARv101 header, load operations go as normal.
    else if(!strcasecmp(tInBuffer, "STARv101"))
    {
        fInfile->SetUseOneByteForStringLengths(false);
    }
    //--Error, incorrect file type.
    else
    {
        delete fInfile;
        return NULL;
    }

    //--[Data Loading]
    //--This is a valid file. We expect the next header to be "LOADINFO_", and to be used for what to put in the LoadingPack.
    if(!CheckHeader(fInfile, "LOADINFO_")) { delete fInfile; return NULL; }

    //--This pack will store the loading information.
    SetMemoryData(__FILE__, __LINE__);
    LoadingPack *nPack = (LoadingPack *)starmemoryalloc(sizeof(LoadingPack));
    memset(nPack, 0, sizeof(LoadingPack));

    //--Get the name of the savegame. Store it in the pack.
    char *tSavegameName = fInfile->ReadLenString();
    strcpy(nPack->mFileName, tSavegameName);

    //--Store the file path.
    strcpy(nPack->mFilePath, pFilePath);

    //--Store the "Short" file path. This is the file path with only the name and extension.
    int tStart = 0;
    for(int i = (int)strlen(pFilePath)-1; i >= 0; i --)
    {
        if(pFilePath[i] == '/' || pFilePath[i] == '\\')
        {
            tStart = i+1;
            break;
        }
    }
    strcpy(nPack->mFilePathShort, &pFilePath[tStart]);

    //--Get the name of the map.
    char *tMapName = fInfile->ReadLenString();
    strcpy(nPack->mMapLocation, tMapName);

    //--Timestamp.
    char *tTimestamp = fInfile->ReadLenString();
    strcpy(nPack->mTimestamp, tTimestamp);

    //--Finish up.
    free(tSavegameName);
    free(tMapName);
    free(tTimestamp);
    delete fInfile;
    xIsStarv100Savefile = false;
    return nPack;
}

//--[Entry]
void SaveManager::SaveAdventureFile(const char *pFilePath)
{
    //--[Documentation]
    //--Entry point for the saving sequence in AdventureMode. Requires a valid hard drive path. Overwrites
    //  if the file already exists.
    if(!pFilePath) return;

    //--Debug.
    DebugPush(true, "SaveManager:SaveAdventureFile - Begin, path is %s\n", pFilePath);

    //--[File Handling]
    //--Open the file, check it.
    FILE *fOutfile = fopen(pFilePath, "wb");
    if(!fOutfile)
    {
        DebugPop("Failed, file could not be opened.\n");
        return;
    }

    //--Create an auto-buffer to store the writes. It is faster to write the data to the hard drive
    //  in one stream rather than piecemeal. This gets the m-master letter since it's so vital.
    SugarAutoBuffer *mOutBuffer = new SugarAutoBuffer();

    //--[Header]
    //--Write the four-byte header.
    mOutBuffer->AppendStringWithoutNull("STAR");

    //--Write the saving version.
    mOutBuffer->AppendStringWithoutNull("v101");

    //--[Load Info]
    //--Information that will be displayed by the loading screen later.
    mOutBuffer->AppendStringWithoutNull("LOADINFO_");
    mOutBuffer->AppendStringWithLen(mSavegameName);
    AdvCombat::Fetch()->WriteLoadInfo(mOutBuffer);

    //--[Combat Information]
    //--Header for the combat information.
    mOutBuffer->AppendStringWithoutNull("COMBAT_");
    AdvCombat::Fetch()->WriteGameInfo(mOutBuffer);

    //--[Inventory]
    //--Header for the inventory data.
    mOutBuffer->AppendStringWithoutNull("INVENTORY_");
    AdventureInventory::Fetch()->WriteToBuffer(mOutBuffer);

    //--[Topics]
    //--Header for the topic listing
    mOutBuffer->AppendStringWithoutNull("TOPICS_");
    WorldDialogue::Fetch()->WriteToBuffer(mOutBuffer);

    //--[Data Library]
    //--Update with AudioManager data.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    SysVar *rMusicVolume = (SysVar *)rDataLibrary->GetEntry("Root/Variables/System/Special/fMusicVolume");
    SysVar *rSoundVolume = (SysVar *)rDataLibrary->GetEntry("Root/Variables/System/Special/fSoundVolume");
    if(rMusicVolume && rSoundVolume)
    {
        rMusicVolume->mNumeric = AudioManager::xMusicVolume;
        rSoundVolume->mNumeric = AudioManager::xSoundVolume;
    }

    //--Update the memory flag.
    SysVar *rMemoryCursorVar = (SysVar *)rDataLibrary->GetEntry("Root/Variables/System/Special/iMemoryCursor");
    if(rMemoryCursorVar)
    {
        if(AdvCombat::Fetch()->IsMemoryCursor())
        {
            rMemoryCursorVar->mNumeric = 1.0f;
        }
        else
        {
            rMemoryCursorVar->mNumeric = 0.0f;
        }
    }

    //--Header for the script variables.
    mOutBuffer->AppendStringWithoutNull("SCRIPTVARS_");
    rDataLibrary->WriteToBuffer(mOutBuffer);

    //--[Map Information]
    //--Header for the map information.
    mOutBuffer->AppendStringWithoutNull("MAP_");

    //--Name of the level this saving action was called with.
    AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
    if(rActiveLevel)
    {
        rActiveLevel->WriteToBuffer(mOutBuffer);
    }
    //--Level didn't exist, so write "NOLEVEL". This is used between chapters.
    else
    {
        mOutBuffer->AppendStringWithLen("NOLEVEL");
    }

    //--[Finish Up]
    //--Liberate the data.
    int mDataLen = mOutBuffer->GetCursor();
    uint8_t *mDataPtr = mOutBuffer->LiberateData();

    //--Write the buffer to the file.
    fwrite(mDataPtr, mDataLen, sizeof(uint8_t), fOutfile);

    //--Close the file, debug write.
    fclose(fOutfile);
    delete mOutBuffer;
    DebugPop("SaveManager:SaveAdventureFile - Completed normally.");

    //--[Control Handling]
    //--Print the current control setup to a save file.
    SaveControlsFile();
}
void SaveManager::SaveControlsFile()
{
    //--Writes a control overbind file. Does not take a pathname, uses the library-specific definition.
    SetMemoryData(__FILE__, __LINE__);
    char **tControlNames = (char **)starmemoryalloc(sizeof(char *) * FM_CONTROLS_TOTAL);
    int tCurrentScancodes[FM_CONTROLS_TOTAL][6];

    //--First, build a name lookup.
    tControlNames[0] = InitializeString("Up");
    tControlNames[1] = InitializeString("Left");
    tControlNames[2] = InitializeString("Down");
    tControlNames[3] = InitializeString("Right");
    tControlNames[4] = InitializeString("Activate");
    tControlNames[5] = InitializeString("Cancel");
    tControlNames[6] = InitializeString("Run");
    tControlNames[7] = InitializeString("UpLevel");
    tControlNames[8] = InitializeString("DnLevel");

    //--Get scancode information.
    ControlManager *rControlManager = ControlManager::Fetch();
    for(int i = 0; i < FM_CONTROLS_TOTAL; i ++)
    {
        //--Set all six entries to -1, to indicate they aren't bound.
        for(int p = 0; p < 6; p ++) tCurrentScancodes[i][p] = -1;

        //--Get the control.
        ControlState *rControlState = rControlManager->GetControlState(tControlNames[i]);
        if(!rControlState) continue;

        //--Fill.
        tCurrentScancodes[i][0] = rControlState->mWatchKeyPri;
        tCurrentScancodes[i][1] = rControlState->mWatchMouseBtnPri;
        tCurrentScancodes[i][2] = rControlState->mWatchJoyPri;
        tCurrentScancodes[i][3] = rControlState->mWatchKeySec;
        tCurrentScancodes[i][4] = rControlState->mWatchMouseBtnSec;
        tCurrentScancodes[i][5] = rControlState->mWatchJoySec;
    }

    //--Assemble the path to the controls file.
    char tControlPath[256];
    const char *rAdventurePath = DataLibrary::GetGamePath("Root/Paths/System/Startup/sAdventurePath");
    #ifdef _ALLEGRO_PROJECT_
        sprintf(tControlPath, "%s/../../Saves/AdventureControlsAL.lua", rAdventurePath);
    #elif defined _SDL_PROJECT_
        sprintf(tControlPath, "%s/../../Saves/AdventureControlsSDL.lua", rAdventurePath);
    #else
        fprintf(stderr, "Error: No primary control library, cannot boot controls!");
        tControlPath[0] = '\0';
    #endif

    //--Open a file and write.
    FILE *fControlOutfile = fopen(tControlPath, "w");
    if(fControlOutfile)
    {
        //--Header.
        fprintf(fControlOutfile, "--[Adventure Mode Control Rebinds]\n");
        fprintf(fControlOutfile, "--These rebinds are stored whenever the game is saved and are loaded when a game is loaded or started.\n");
        fprintf(fControlOutfile, "-- Do not hand-edit this file as it will be overwritten during the next save.\n");

        //--Now write the overbind commands.
        for(int i = 0; i < FM_CONTROLS_TOTAL; i ++)
        {
            fprintf(fControlOutfile, "CM_Overbind(\"%s\", false, %i, %i, %i)\n", tControlNames[i], tCurrentScancodes[i][0], tCurrentScancodes[i][1], tCurrentScancodes[i][2]);
            fprintf(fControlOutfile, "CM_Overbind(\"%s\", true, %i, %i, %i)\n",  tControlNames[i], tCurrentScancodes[i][3], tCurrentScancodes[i][4], tCurrentScancodes[i][5]);
        }
        fclose(fControlOutfile);
    }

    //--Clean.
    for(int i = 0; i < FM_CONTROLS_TOTAL; i ++) free(tControlNames[i]);
    free(tControlNames);
}

//--[Worker Function]
bool CheckHeader(VirtualFile *fInfile, const char *pComparison)
{
    //--Given a VirtualFile with its cursor in position, seeks forward by the size of pComparison's string
    //  and returns true if the file is identical. Used for checking headers.
    if(!fInfile || !pComparison) return false;

    //--Read.
    char tInBuffer[32];
    memset(tInBuffer, 0, sizeof(char) * 32);
    fInfile->Read(tInBuffer, sizeof(char), (int)strlen(pComparison));

    //--Compare.
    return (!strcmp(tInBuffer, pComparison));
}

//--[Loading Entry]
void SaveManager::LoadFrom(const char *pPath)
{
    //--[Documentation]
    //--Given a file path, loads from the file all of the data needed to reconstruct the game state. Loading should be the
    //  first thing done in the game after the initial state is built (that is, instead of going to the scenario launcher, load).
    if(!pPath) return;

    //--Flag reset.
    xIsStarv100Savefile = false;

    //--Debug.
    DebugPush(true, "SaveManager:LoadFrom - Begin, path is %s\n", pPath);

    //--[Basic File Checking]
    //--Open the file, check it. Do this is a VirtualFile to speed up reading.
    VirtualFile *fInfile = new VirtualFile(pPath, false);
    if(!fInfile->IsReady())
    {
        DebugPop("Failed, file could not be opened.\n");
        return;
    }

    //--Make sure this is a "STARv100" file or a "STARv101" savefile. Both headers are valid.
    char tInBuffer[32];
    memset(tInBuffer, 0, sizeof(char) * 32);
    fInfile->Read(tInBuffer, sizeof(char), (int)strlen("STARv101"));

    //--If this is a STARv100 header, the DataLibrary will be using a single digit to indicate string length.
    if(!strcasecmp(tInBuffer, "STARv100"))
    {
        xIsStarv100Savefile = true;
    }
    //--If this is a STARv101 header, load operations go as normal.
    else if(!strcasecmp(tInBuffer, "STARv101"))
    {
        fInfile->SetUseOneByteForStringLengths(false);
    }
    //--Error, incorrect file type.
    else
    {
        DebugPop("Failed, file is not a STARv100 or STARv101 file.\n");
        xIsStarv100Savefile = false;
        return;
    }

    //--[Loading Info]
    //--Skip this, we're not interested in most of it.
    if(!CheckHeader(fInfile, "LOADINFO_"))
    {
        DebugPop("Failed, expected LOADINFO_ lump.\n");
        xIsStarv100Savefile = false;
        return;
    }

    //--Savegame name. Set this as the active name.
    char *tSavegameName = fInfile->ReadLenString();
    SetSavegameName(tSavegameName);
    free(tSavegameName);

    //--Skip the name of the level. This is only for the loading pack and may not be the same as the final name.
    fInfile->SkipLenString();

    //--Skip the timestamp. We don't need it.
    fInfile->SkipLenString();

    //--[Combat Data]
    //--Check header.
    if(!CheckHeader(fInfile, "COMBAT_"))
    {
        DebugPop("Failed, combat info not found.\n");
        xIsStarv100Savefile = false;
        return;
    }

    //--Run.
    AdvCombat::Fetch()->ReadFromFile(fInfile);

    //--[Inventory]
    //--Check header.
    if(!CheckHeader(fInfile, "INVENTORY_"))
    {
        DebugPop("Failed, inventory info not found.\n");
        xIsStarv100Savefile = false;
        return;
    }

    //--Run.
    AdventureInventory::Fetch()->ReadFromFile(fInfile);

    //--[Topics]
    //--Check header.
    if(!CheckHeader(fInfile, "TOPICS_"))
    {
        DebugPop("Failed, topic info not found.\n");
        xIsStarv100Savefile = false;
        return;
    }

    //--Run.
    WorldDialogue::Fetch()->ReadFromFile(fInfile);

    //--[Script Variables]
    //--Check header.
    if(!CheckHeader(fInfile, "SCRIPTVARS_"))
    {
        DebugPop("Failed, script variable info not found.\n");
        xIsStarv100Savefile = false;
        return;
    }

    //--Run.
    DataLibrary::Fetch()->ReadFromFile(fInfile);

    //--[Map Data]
    //--Check header.
    bool tNoMapInfo = false;
    if(!CheckHeader(fInfile, "MAP_"))
    {
        tNoMapInfo = true;
        DebugPrint("Failed, map info not found. Using default level.\n");
    }

    //--If no map info was found, we default to EvermoonW.
    if(tNoMapInfo)
    {
        //--Set to EvermoonW as default.
        char tUseString[128];
        strcpy(tUseString, "EvermoonW");

        //--Search for a remapping. If not found, use the default.
        const char *rUsePathing = tUseString;
        for(int i = 0; i < AdventureLevel::xRemappingsTotal; i ++)
        {
            if(!strcasecmp(AdventureLevel::xRemappingsCheck[i], tUseString))
            {
                rUsePathing = AdventureLevel::xRemappingsResult[i];
                break;
            }
        }

        //--Build the level directory.
        char tBuffer[256];
        sprintf(tBuffer, "%s/Maps/%s/Constructor.lua", AdventureLevel::xRootPath, rUsePathing);

        //--Execute this level's construction sequence.
        LuaManager::Fetch()->ExecuteLuaFile(tBuffer);

        //--Post-construction.
        LuaManager::Fetch()->ExecuteLuaFile(tBuffer, 1, "N", 1.0f);
    }
    //--Map info was found:
    else
    {
        //--This string is the name of the level. If the level didn't exist, it will be "NOLEVEL".
        char *tLevelName = fInfile->ReadLenString();
        if(strcasecmp(tLevelName, "NOLEVEL"))
        {
            //--Search for a remapping. If not found, use the default.
            const char *rUsePathing = tLevelName;
            for(int i = 0; i < AdventureLevel::xRemappingsTotal; i ++)
            {
                if(!strcasecmp(AdventureLevel::xRemappingsCheck[i], tLevelName))
                {
                    rUsePathing = AdventureLevel::xRemappingsResult[i];
                    break;
                }
            }

            //--Build the level directory.
            char tBuffer[256];
            sprintf(tBuffer, "%s/Maps/%s/Constructor.lua", AdventureLevel::xRootPath, rUsePathing);

            //--Execute this level's construction sequence.
            LuaManager::Fetch()->ExecuteLuaFile(tBuffer);

            //--If the level exists, order it to finish its loading sequence.
            AdventureLevel *rActiveLevel = AdventureLevel::Fetch();
            if(rActiveLevel) rActiveLevel->ReadFromFile(fInfile);

            //--Post-construction.
            LuaManager::Fetch()->ExecuteLuaFile(tBuffer, 1, "N", 1.0f);
        }
        //--No level was found.
        else
        {
            DebugPop("Failed, NOLEVEL was not expected!\n");
            xIsStarv100Savefile = false;
            return;
        }
    }

    //--Debug.
    delete fInfile;
    DebugPop("SaveManager:LoadFrom - Completed normally.\n");
    xIsStarv100Savefile = false;
}
