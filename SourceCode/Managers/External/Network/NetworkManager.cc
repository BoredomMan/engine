//--Base
#include "NetworkManager.h"

//--Classes
//--CoreClasses
#include "SugarLinkedList.h"
#include "StarlightSocket.h"

//--Definitions
#include "Global.h"

//--GUI
//--Libraries
//--Managers

//=========================================== System ==============================================
NetworkManager::NetworkManager()
{
    //--System
    //--Socket Controls
    mSocketList = new SugarLinkedList(true);

    //--Client Handling
    mRetryConnection = false;
    mRetryTimer = 0;
    mRetriesLeft = 0;

    //--Server Handling
    //Placeholder.
}
NetworkManager::~NetworkManager()
{
    delete mSocketList;
    StarlightSocket::TakeDownNetwork();
}

//====================================== Property Queries =========================================
bool NetworkManager::IsConnectedTo(const char *pName)
{
    return (mSocketList->GetElementByName(pName) != NULL);
}

//========================================= Manipulators ==========================================
void NetworkManager::AttemptConnection(const char *pNameIdentifier, const char *pIPAddress, int pPort)
{
    //--Creates a new StarlightSocket and tells it to connect to the provided position.  The Socket is
    //  added to the update list, and may terminate later if the connection fails.
    StarlightSocket *nSocket = new StarlightSocket();
    nSocket->StartSocket();
    nSocket->ConnectTo(pIPAddress, pPort);
    mSocketList->AddElement(pNameIdentifier, nSocket, &StarlightSocket::DeleteThis);
}
void NetworkManager::FlagSugarBeats()
{
    mRetryConnection = true;
}

//========================================= Core Methods ==========================================
void NetworkManager::SendPacketTo(const char *pName, StarlightPacket pPacket)
{
    //--Locates the given socket.  If it exists, ships the packet to it.
    StarlightSocket *rSocket = (StarlightSocket *)mSocketList->GetElementByName(pName);
    if(!rSocket)
    {
        return;
    }

    rSocket->SendData(pPacket);
}

//============================================ Update =============================================
void NetworkManager::Update()
{
    //--[VeryBBQ]
    //--If the retry flags are set, attempts to connect to the SugarBeat program.
    if(mRetryConnection)
    {
        //--Glip the flag off.
        mRetryConnection = false;

        //--Scan the list.  If a connection is pending/active, do nothing.
        if(!mSocketList->GetElementByName("SugarBeats"))
            AttemptConnection("SugarBeats", "127.0.0.1", 6012);
    }

    //--Updates all sockets that are currently active.  Terminates sockets that have failed their
    //  connection, if applicable.
    StarlightSocket *rSocket = (StarlightSocket *)mSocketList->SetToHeadAndReturn();
    while(rSocket)
    {
        //--Issue the update.
        rSocket->Update();

        //--If the socket is "dead", remove it.
        if(!rSocket->IsSocketOpen()) mSocketList->RemoveRandomPointerEntry();

        //--Iterate up.
        rSocket = (StarlightSocket *)mSocketList->IncrementAndGetRandomPointerEntry();
    }
}
void NetworkManager::UpdatePaused(uint8_t pPauseFlags)
{
    //--When paused, the NetworkManager may filter packets, or it may just keep acting normally.
    //  The default Pure Engine code does nothing.
}

//=========================================== File I/O ============================================
//=========================================== Drawing =============================================
//======================================= Pointer Routing =========================================
//====================================== Static Functions =========================================
NetworkManager *NetworkManager::Fetch()
{
    return Global::Shared()->gNetworkManager;
}

//========================================= Lua Hooking ===========================================
//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
