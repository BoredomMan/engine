#include "Structures.h"
#include "LuaManager.h"
int LuaArgError(const char *pFunctionName)
{
    if(!pFunctionName) return 0;
    if(!LuaManager::xFailSilently)
        fprintf(stderr, "%s: Failed, invalid argument list. %s.\n", pFunctionName, LuaManager::Fetch()->GetCallStack(0));
    return 0;
}
int LuaArgError(const char *pFunctionName, lua_State *L)
{
    LuaArgError(pFunctionName);
    lua_pushnil(L);
    return 1;
}
int LuaArgError(const char *pFunctionName, const char *pSwitchType, int pArgs)
{
    if(!pFunctionName || !pSwitchType) return 0;
    if(!LuaManager::xFailSilently)
        fprintf(stderr, "%s: Failed, invalid argument list. %s %i - %s.\n", pFunctionName, pSwitchType, pArgs, LuaManager::Fetch()->GetCallStack(0));
    return 0;
}
int LuaTypeError(const char *pFunctionName)
{
    if(!pFunctionName) return 0;
    if(!LuaManager::xFailSilently)
        fprintf(stderr, "%s: Failed, rActiveObject was wrong type, or NULL. %s\n", pFunctionName, LuaManager::Fetch()->GetCallStack(0));
    return 0;
}
int LuaTypeError(const char *pFunctionName, lua_State *L)
{
    LuaTypeError(pFunctionName);
    lua_pushnil(L);
    return 1;
}
int LuaPropertyError(const char *pFunctionName, const char *pSwitchType, int pArgs)
{
    if(!pFunctionName || !pSwitchType) return 0;
    if(!LuaManager::xFailSilently)
        fprintf(stderr, "%s: Failed, cannot resolve %s with %i args. %s.\n", pFunctionName, pSwitchType, pArgs, LuaManager::Fetch()->GetCallStack(0));
    return 0;
}
int LuaPropertyNoMatchError(const char *pFunctionName, const char *pSwitchType)
{
    if(!pFunctionName || !pSwitchType) return 0;
    fprintf(stderr, "%s: Failed, %s does not match any property. %s.\n", pFunctionName, pSwitchType, LuaManager::Fetch()->GetCallStack(0));
    return 0;
}
