//--External
class LuaManager;
class NetworkManager;
class SaveManager;
class SteamManager;
class SugarLumpManager;

//--Game
class CutsceneManager;
class EntityManager;
class MapManager;
class MapManagerBGModule;
class ResetManager;

//--System
class AudioManager;
class AudioPackage;
class CameraManager;
class ControlManager;
struct ControlState;
class DebugManager;
class DisplayManager;
class OptionsManager;
class TransitionManager;
class MemoryManager;
