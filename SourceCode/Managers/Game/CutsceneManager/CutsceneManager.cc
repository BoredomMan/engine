//--Base
#include "CutsceneManager.h"

//--Classes
#include "RootEvent.h"
#include "ActorEvent.h"
#include "AudioEvent.h"
#include "CameraEvent.h"
#include "TimerEvent.h"
#include "PairanormalDialogue.h"
#include "WorldDialogue.h"

//--CoreClasses
#include "SugarLinkedList.h"

//--Definitions
#include "Global.h"

//--Libraries
#include "DataLibrary.h"

//--Managers
#include "DebugManager.h"
#include "LuaManager.h"

//--[Debug]
//#define CS_DEBUG
#ifdef CS_DEBUG
    #define DebugPush(pFlag, pString, ...) DebugManager::PushPrint(pFlag, pString, ##__VA_ARGS__)
    #define DebugPrint(pString, ...) DebugManager::Print(pString, ##__VA_ARGS__)
    #define DebugPop(pString, ...) DebugManager::PopPrint(pString, ##__VA_ARGS__)
#else
    #define DebugPush(pFlag, pString, ...) ;
    #define DebugPrint(pString, ...) ;
    #define DebugPop(pString, ...) ;
#endif

//=========================================== System ==============================================
CutsceneManager::CutsceneManager()
{
    //--[CutsceneManager]
    //--System
    //--Storage
    mEventList = new SugarLinkedList(true);

    //--Bucketed Addition
    mIsBucketedAddition = false;
    mBypassBucket = false;
    mBucketedList = new SugarLinkedList(true);

    //--Drop List.
    mDropList = NULL;

    //--Parallel Simultaneous Scripts
    mClearAllParallelLists = false;
    mSuspendActiveScript = false;
    mRemoveActiveScript = false;
    rActivePackage = NULL;
    mParallelScriptList = new SugarLinkedList(true);
    mSuspendedParallelScripts = new SugarLinkedList(true);
    mParallelClearList = new SugarLinkedList(true);
}
CutsceneManager::~CutsceneManager()
{
    delete mEventList;
    delete mBucketedList;
    delete mDropList;
    delete mParallelScriptList;
    delete mSuspendedParallelScripts;
    delete mParallelClearList;
}

//--[Public Statics]
//--Used to turn on and off flags related to printing debug text.
bool CutsceneManager::xFlag = true;

//====================================== Property Queries =========================================
bool CutsceneManager::HasAnyEvents()
{
    //--Note: Ignores parallel script events. Only the lockout-list has this.
    return (mEventList->GetListSize() > 0);
}

//========================================= Manipulators ==========================================
void CutsceneManager::CreateParallelScript(const char *pName, const char *pPath)
{
    //--Creates a new parallel script. Also immediately executes it to populate its event list.
    if(!pName || !pPath) return;
    SetMemoryData(__FILE__, __LINE__);
    ParallelScriptPack *nNewScript = (ParallelScriptPack *)starmemoryalloc(sizeof(ParallelScriptPack));
    nNewScript->Initialize();

    //--Give it its name and register it.
    ResetString(nNewScript->mRefresherScript, pPath);
    mParallelScriptList->AddElement(pName, nNewScript, &ParallelScriptPack::DeleteThis);

    //--Run it for the first time. It will also re-run when its event list empties.
    rActivePackage = nNewScript;
    LuaManager::Fetch()->ExecuteLuaFile(pPath);
    rActivePackage = NULL;
    //fprintf(stderr, "Registered parallel pack %s %p\n", pName, nNewScript);
}
void CutsceneManager::SuspendParallelScript(const char *pName)
{
    //--Find the named script and suspend it.
    void *rCheckStruct = mParallelScriptList->GetElementByName(pName);
    if(!rCheckStruct) return;

    //--Liberate the entry.
    mParallelScriptList->SetRandomPointerToThis(rCheckStruct);
    mParallelScriptList->LiberateRandomPointerEntry();

    //--Re-register it to the suspended list.
    mSuspendedParallelScripts->AddElement(pName, rCheckStruct, &ParallelScriptPack::DeleteThis);
}
void CutsceneManager::SuspendActiveScript()
{
    mSuspendActiveScript = true;
}
void CutsceneManager::UnsuspendParallelScript(const char *pName)
{
    //--Find the named script and unsuspend it.
    void *rCheckStruct = mSuspendedParallelScripts->GetElementByName(pName);
    if(!rCheckStruct) return;

    //--Liberate the entry.
    mSuspendedParallelScripts->SetRandomPointerToThis(rCheckStruct);
    mSuspendedParallelScripts->LiberateRandomPointerEntry();

    //--Re-register it to the parallel list.
    mParallelScriptList->AddElement(pName, rCheckStruct, &ParallelScriptPack::DeleteThis);
}
void CutsceneManager::RemoveParallelScript(const char *pName)
{
    //--Moves the named script onto the removal list, which gets dumped at the end of the tick.
    while(true)
    {
        //--Check the main list.
        void *rCheckStruct = mParallelScriptList->GetElementByName(pName);
        if(rCheckStruct)
        {
            mParallelScriptList->SetRandomPointerToThis(rCheckStruct);
            mParallelScriptList->LiberateRandomPointerEntry();
            mParallelClearList->AddElement(pName, rCheckStruct, &ParallelScriptPack::DeleteThis);
            continue;
        }

        //--Check the suspend list.
        rCheckStruct = mSuspendedParallelScripts->GetElementByName(pName);
        if(rCheckStruct)
        {
            mSuspendedParallelScripts->SetRandomPointerToThis(rCheckStruct);
            mSuspendedParallelScripts->LiberateRandomPointerEntry();
            mParallelClearList->AddElement(pName, rCheckStruct, &ParallelScriptPack::DeleteThis);
            continue;
        }

        //--No matches, exit.
        break;
    }
}
void CutsceneManager::RemoveActiveScript()
{
    mRemoveActiveScript = true;
}
void CutsceneManager::ClearParallelScripts()
{
    mClearAllParallelLists = true;
}

//========================================= Core Methods ==========================================
void CutsceneManager::RegisterEvent(RootEvent *pEvent)
{
    //--Registers the event to the end of the event list. Names can be duplicated on the list.
    if(!pEvent) return;

    //--Parallel List: Add to that event list, if it exists.
    if(rActivePackage)
    {
        //fprintf(stderr, " Intercepted, event registered to parallel package %p\n", rActivePackage);
        rActivePackage->mEventList->AddElementAsTail(pEvent->GetName(), pEvent, &RootObject::DeleteThis);
        return;
    }

    //--Normal case: Add this to the list.
    if(!mIsBucketedAddition || (mIsBucketedAddition && mBypassBucket))
    {
        mEventList->AddElementAsTail(pEvent->GetName(), pEvent, &RootObject::DeleteThis);
    }
    //--Otherwise, add this to the bucket.
    else
    {
        mBucketedList->AddElementAsTail(pEvent->GetName(), pEvent, &RootObject::DeleteThis);
    }
}
RootEvent *CutsceneManager::CreateBlocker()
{
    //--Quick-access subroutine: Creates a basic blocker with no other properties.
    RootEvent *nEvent = new RootEvent();
    nEvent->SetName("Blocker");
    nEvent->SetBlockingFlag(true);
    RegisterEvent(nEvent);
    return nEvent;
}
void CutsceneManager::DropAllEvents()
{
    //--Drops all events. Events get moved to a non-executing list and will be purged at the start
    //  of the next tick.
    if(mDropList)
    {
        //--Liberate the events and move them to the drop list.
        while(mEventList->GetListSize() > 0)
        {
            mEventList->SetRandomPointerToHead();
            void *rPtr = mEventList->LiberateRandomPointerEntry();
            mDropList->AddElementAsTail("X", rPtr, &RootObject::DeleteThis);
        }
    }
    //--Just reposition the event list.
    else
    {
        mDropList = mEventList;
        mEventList = new SugarLinkedList(true);
    }
}

//===================================== Private Core Methods ======================================
//============================================ Update =============================================
void CutsceneManager::Update()
{
    //--Updates the event cycle currently in the manager. Does nothing if there are no events.
    bool tHasIncompleteEvent = false;
    mIsBucketedAddition = true;
    mBypassBucket = false;

    //--Debug.
    DebugPush(xFlag, "CutsceneManager:Update - Begin.\n");
    DebugPrint("Event count: %i\n", mEventList->GetListSize());

    //--Check the dialogue handler. If it's blocking, then we consider events to start as partially
    //  incomplete.
    WorldDialogue *rDialogue = WorldDialogue::Fetch();
    if(rDialogue->IsStoppingEvents()) tHasIncompleteEvent = true;
    DebugPrint("Has dialogue event: %i\n", tHasIncompleteEvent);

    //--If the Pairanormal dialogue handler is stopping events, the same thing happens.
    PairanormalDialogue *rPairanormalDialogue = PairanormalDialogue::Fetch();
    if(rPairanormalDialogue->IsStoppingEvents()) tHasIncompleteEvent = true;
    DebugPrint("Has pairanormal dialogue event: %i\n", tHasIncompleteEvent);

    //--Iterate.
    RootEvent *rEvent = (RootEvent *)mEventList->SetToHeadAndReturn();
    while(rEvent)
    {
        //--Debug.
        DebugPrint(" Update event: %s %p\n", rEvent->GetName(), rEvent);

        //--Run the update.
        rEvent->Update();

        //--Ask this event if it is complete.
        if(rEvent->IsComplete())
        {
            mEventList->RemoveRandomPointerEntry();
            DebugPrint("  Removed from completion.\n");
        }
        //--If this event is a blocker and the incomplete flag is not set, remove it and keep going.
        else if(rEvent->IsBlocker() && !tHasIncompleteEvent)
        {
            mEventList->RemoveRandomPointerEntry();
            DebugPrint("  Removed from cleared blocker.\n");
        }
        //--If this event is a blocker and the incomplete flag is set, stop here.
        else if(rEvent->IsBlocker() && tHasIncompleteEvent)
        {
            DebugPrint("  Stopping due to blocker.\n");
            break;
        }
        //--If this event was not complete yet, set this flag.
        else
        {
            tHasIncompleteEvent = true;
            DebugPrint("  Event is incomplete.\n");
        }

        //--After each event, the WorldDialogue can stop proceedings if it hit a blocker.
        if(rDialogue->IsStoppingEvents() || rPairanormalDialogue->IsStoppingEvents())
        {
            tHasIncompleteEvent = true;
            DebugPrint("  Event is stopping iteration.\n");
        }

        //--Next event.
        DebugPrint("  Getting next event.\n");
        rEvent = (RootEvent *)mEventList->IncrementAndGetRandomPointerEntry();
    }

    //--Flags.
    mIsBucketedAddition = false;
    DebugPrint("Completed iterations.\n");

    //--After iteration, if any events were put in the bucket, add them to the end of the list. This allows events to create the next
    //  set of events ala enclosures or repeated usages of the same script.
    RootEvent *rAppendEvent = (RootEvent *)mBucketedList->SetToHeadAndReturn();
    while(rAppendEvent)
    {
        mBucketedList->LiberateRandomPointerEntry();
        RegisterEvent(rAppendEvent);
        rAppendEvent = (RootEvent *)mBucketedList->SetToHeadAndReturn();
    }

    //--Debug.
    DebugPop("CutsceneManager: Update - Complete.\n");
}
void CutsceneManager::UpdateParallelEvents()
{
    //--Updates parallel event lists. Unlike the main lists, these can run even if there are no events on the main list.
    //  This inherently means the player still has control over the game, and the event still runs!
    //--Pausing the game, however, still stops this update. Obviously.
    ParallelScriptPack *rParallelPack = (ParallelScriptPack *)mParallelScriptList->SetToHeadAndReturn();
    while(rParallelPack)
    {
        //--Clear these flags.
        mRemoveActiveScript = false;
        mSuspendActiveScript = false;

        //--Run the update.
        rActivePackage = rParallelPack;
        UpdateParallelPackage(rParallelPack);
        rActivePackage = NULL;

        //--If the order was received to remove this script, do it now.
        if(mRemoveActiveScript)
        {
            mParallelScriptList->RemoveRandomPointerEntry();
        }
        //--If the order was received to suspend the script, do it now.
        else if(mSuspendActiveScript)
        {
            mSuspendedParallelScripts->AddElement(mParallelScriptList->GetRandomPointerName(), rParallelPack, &ParallelScriptPack::DeleteThis);
            mParallelScriptList->LiberateRandomPointerEntry();
        }

        //--Next.
        rParallelPack = (ParallelScriptPack *)mParallelScriptList->IncrementAndGetRandomPointerEntry();
    }
}
void CutsceneManager::UpdateParallelPackage(ParallelScriptPack *pPack)
{
    //--Updates a parallel event list package. This always occurs right after normal event updates occur, so
    //  a parallel package created by an event will execute for the first time on the same tick.
    //  Note that parallel packages have full permission sets, including the ability to spawn their own
    //  parallel scripts. Try not to break things, please.
    //--When a parallel package runs out of events, it re-runs its refresher script automatically to
    //  rebuild the event list.
    //--The special event "SUSPENDTHIS" will suspend the calling package and only the calling package.
    if(!pPack) return;

    //--Setup.
    bool tExecutedAnyEvents = false;
    bool tHasIncompleteEvent = false;

    //--Iterate.
    RootEvent *rEvent = (RootEvent *)pPack->mEventList->SetToHeadAndReturn();
    while(rEvent)
    {
        //--Debug.
        //DebugPrint(" Update event: %s %p\n", rEvent->GetName(), rEvent);

        //--Run the update.
        tExecutedAnyEvents = true;
        rEvent->Update();

        //--Ask this event if it is complete.
        if(rEvent->IsComplete())
        {
            pPack->mEventList->RemoveRandomPointerEntry();
            //DebugPrint("  Removed from completion.\n");
        }
        //--If this event is a blocker and the incomplete flag is not set, remove it and keep going.
        else if(rEvent->IsBlocker() && !tHasIncompleteEvent)
        {
            pPack->mEventList->RemoveRandomPointerEntry();
            //DebugPrint("  Removed from cleared blocker.\n");
        }
        //--If this event is a blocker and the incomplete flag is set, stop here.
        else if(rEvent->IsBlocker() && tHasIncompleteEvent)
        {
            //DebugPrint("  Stopping due to blocker.\n");
            break;
        }
        //--If this event was not complete yet, set this flag.
        else
        {
            tHasIncompleteEvent = true;
            //DebugPrint("  Event is incomplete.\n");
        }

        //--Next event.
        //DebugPrint("  Getting next event.\n");
        rEvent = (RootEvent *)pPack->mEventList->IncrementAndGetRandomPointerEntry();
    }

    //--If the event list was completely empty, re-execute the populating script. Don't do this if
    //  the order to suspend or delete this script was received.
    if(!tExecutedAnyEvents && !mSuspendActiveScript && !mRemoveActiveScript)
    {
        //fprintf(stderr, " Out of events: Refreshing.\n");
        LuaManager::Fetch()->ExecuteLuaFile(pPack->mRefresherScript);
    }
}
void CutsceneManager::DropListAtEndOfTick()
{
    //--This list always drops.
    mParallelClearList->ClearList();

    //--If this flag is set, clear all parallel lists.
    if(mClearAllParallelLists)
    {
        mClearAllParallelLists = false;
        mParallelScriptList->ClearList();
        mSuspendedParallelScripts->ClearList();
    }

    //--This list only contains mainline events. It drops if it exists.
    if(!mDropList) return;
    delete mDropList;
    mDropList = NULL;
}

//=========================================== File I/O ============================================
//=========================================== Drawing =============================================
//======================================= Pointer Routing =========================================
//====================================== Static Functions =========================================
CutsceneManager *CutsceneManager::Fetch()
{
    return Global::Shared()->gCutsceneManager;
}

//========================================= Lua Hooking ===========================================
void CutsceneManager::HookToLuaState(lua_State *pLuaState)
{
    /* Cutscene_CreateEvent("DROPALLEVENTS") (Does not push)
       Cutscene_CreateEvent(sEventName)
       Cutscene_CreateEvent("Blocker")
       Cutscene_CreateEvent(sEventName, "Timer")
       Cutscene_CreateEvent(sEventName, "Actor")
       Cutscene_CreateEvent(sEventName, "Camera")
       Cutscene_CreateEvent(sEventName, "Audio")
       Creates, registers, and pushes a new event to the default CutsceneManager. */
    lua_register(pLuaState, "Cutscene_CreateEvent", &Hook_Cutscene_CreateEvent);

    /* Cutscene_HandleParallel("Create", sName, sPath)
       Cutscene_HandleParallel("Suspend", sName)
       Cutscene_HandleParallel("Unsuspend", sName)
       Cutscene_HandleParallel("Remove", sName)
       Cutscene_HandleParallel("ClearAll")
       Cutscene_HandleParallel("SUSPENDTHIS") --Special, suspends the executing parallel list only.
       Cutscene_HandleParallel("DELETETHIS") --Special, deletes the executing parallel list only.
       Master function to handle parallel execution lists. */
    lua_register(pLuaState, "Cutscene_HandleParallel", &Hook_Cutscene_HandleParallel);
}

//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
int Hook_Cutscene_CreateEvent(lua_State *L)
{
    //Cutscene_CreateEvent("DROPALLEVENTS") (Does not push)
    //Cutscene_CreateEvent(sEventName)
    //Cutscene_CreateEvent("Blocker")
    //Cutscene_CreateEvent(sEventName, "Timer")
    //Cutscene_CreateEvent(sEventName, "Actor")
    //Cutscene_CreateEvent(sEventName, "Camera")
    //Cutscene_CreateEvent(sEventName, "Audio")
    DataLibrary::Fetch()->PushActiveEntity();

    //--Argument check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("Cutscene_CreateEvent");

    //--Switching types.
    const char *rName = lua_tostring(L, 1);
    const char *rType = NULL;
    if(tArgs == 2) rType = lua_tostring(L, 2);

    //--Special case: Drops all events. Doesn't push an event.
    if(!strcasecmp(rName, "DROPALLEVENTS"))
    {
        CutsceneManager::Fetch()->DropAllEvents();
    }
    //--Special case: Create a blocker immediately.
    else if(!strcasecmp(rName, "Blocker"))
    {
        DataLibrary::Fetch()->rActiveObject = CutsceneManager::Fetch()->CreateBlocker();
    }
    //--Special case: Timer type event.
    else if(tArgs == 2 && !strcasecmp(rType, "Timer"))
    {
        TimerEvent *nEvent = new TimerEvent();
        nEvent->SetName(lua_tostring(L, 1));
        CutsceneManager::Fetch()->RegisterEvent(nEvent);
        DataLibrary::Fetch()->rActiveObject = nEvent;
    }
    //--Special case: Actor type event.
    else if(tArgs == 2 && !strcasecmp(rType, "Actor"))
    {
        ActorEvent *nEvent = new ActorEvent();
        nEvent->SetName(lua_tostring(L, 1));
        CutsceneManager::Fetch()->RegisterEvent(nEvent);
        DataLibrary::Fetch()->rActiveObject = nEvent;
    }
    //--Special case: Camera type event.
    else if(tArgs == 2 && !strcasecmp(rType, "Camera"))
    {
        CameraEvent *nEvent = new CameraEvent();
        nEvent->SetName(lua_tostring(L, 1));
        CutsceneManager::Fetch()->RegisterEvent(nEvent);
        DataLibrary::Fetch()->rActiveObject = nEvent;
    }
    //--Special case: Audio type event.
    else if(tArgs == 2 && !strcasecmp(rType, "Audio"))
    {
        AudioEvent *nEvent = new AudioEvent();
        nEvent->SetName(lua_tostring(L, 1));
        CutsceneManager::Fetch()->RegisterEvent(nEvent);
        DataLibrary::Fetch()->rActiveObject = nEvent;
    }
    //--Normal case.
    else
    {
        RootEvent *nEvent = new RootEvent();
        nEvent->SetName(lua_tostring(L, 1));
        CutsceneManager::Fetch()->RegisterEvent(nEvent);
        DataLibrary::Fetch()->rActiveObject = nEvent;
    }
    return 0;
}
int Hook_Cutscene_HandleParallel(lua_State *L)
{
    //Cutscene_HandleParallel("Create", sName, sPath)
    //Cutscene_HandleParallel("Suspend", sName)
    //Cutscene_HandleParallel("Unsuspend", sName)
    //Cutscene_HandleParallel("Remove", sName)
    //Cutscene_HandleParallel("ClearAll")
    //Cutscene_HandleParallel("SUSPENDTHIS") --Special, suspends the executing parallel list only.
    //Cutscene_HandleParallel("DELETETHIS") --Special, deletes the executing parallel list only.

    //--Argument check.
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("Cutscene_HandleParallel");

    //--Switching types.
    const char *rSwitchType = lua_tostring(L, 1);

    //--Creates a new parallel execution list.
    if(!strcasecmp(rSwitchType, "Create") && tArgs == 3)
    {
        CutsceneManager::Fetch()->CreateParallelScript(lua_tostring(L, 2), lua_tostring(L, 3));
    }
    //--Suspends the named script.
    else if(!strcasecmp(rSwitchType, "Suspend") && tArgs == 2)
    {
        CutsceneManager::Fetch()->SuspendParallelScript(lua_tostring(L, 2));
    }
    //--Unsuspends the named script.
    else if(!strcasecmp(rSwitchType, "Unsuspend") && tArgs == 2)
    {
        CutsceneManager::Fetch()->UnsuspendParallelScript(lua_tostring(L, 2));
    }
    //--Removes the named script.
    else if(!strcasecmp(rSwitchType, "Remove") && tArgs == 2)
    {
        CutsceneManager::Fetch()->RemoveParallelScript(lua_tostring(L, 2));
    }
    //--Clears all parallel scripts.
    else if(!strcasecmp(rSwitchType, "ClearAll") && tArgs == 1)
    {
        CutsceneManager::Fetch()->ClearParallelScripts();
    }
    //--Suspends the currently active parallel script. Does nothing if none is active.
    else if(!strcasecmp(rSwitchType, "SUSPENDTHIS") && tArgs == 1)
    {
        CutsceneManager::Fetch()->SuspendActiveScript();
    }
    //--Deletes the currently active parallel script. Does nothing if none is active.
    else if(!strcasecmp(rSwitchType, "DELETETHIS") && tArgs == 1)
    {
        CutsceneManager::Fetch()->RemoveActiveScript();
    }
    return 0;
}
