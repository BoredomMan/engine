//--Base
#include "EntityManager.h"

//--Classes
//--CoreClasses
#include "SugarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"

//--GUI
//--Libraries
//--Managers
#include "DebugManager.h"

//--Special lists are cases where a list is stored of entities that are only on one particular list
//  and not the "main" list.  For example, all ladders are on the ladders list!  This is because
//  ladders are never updated normally, they are only updated by specific functions, and there
//  is no need to include them in non-ladder scanning actions.  This speeds up the program.
//--Operations which delete named entities, though, will run through these entities.  Some other
//  functions exist which deal with the Special-List entities and they are all handled here.
//--Objects on the main list should *not* be regged to a sub-list, or the list risks becoming
//  unstable, and you may encounter a deallocated-pointer-access crash.

void EntityManager::BootSpecialLists()
{
    //--Creates all the sublists.  Called after the EM boots itself.
    mSpecialListList->AddElement("Particle List", new SugarLinkedList(true), &SugarLinkedList::DeleteThis);
}
void EntityManager::RegisterSpecialObject(const char *pSubListName, const char *pName, void *pObject)
{
    //--Overload, default is &DontDeleteThis.
    RegisterSpecialObject(pSubListName, pName, pObject, &DontDeleteThis);
}
void EntityManager::RegisterSpecialObject(const char *pSubListName, const char *pName, void *pObject, DeletionFunctionPtr pDelFunc)
{
    //--Registers the given object to the given sublist.  If the sub-list does not exist, then
    //  the object is not regged and an error is barked.
    if(!pSubListName || !pName || !pObject) return;

    //--Retrieve and check the sub-list.
    SugarLinkedList *rSubList = (SugarLinkedList *)mSpecialListList->GetElementByName(pSubListName);
    if(!rSubList)
    {
        DebugManager::ForcePrint("EM::RegisterSpecialObject:  Error, no sub-list %s\n", pSubListName);
        return;
    }

    //--Register it.
    rSubList->AddElement(pName, pObject, pDelFunc);
}
void EntityManager::AutoPurge(void *pPtr)
{
    //--Given a particular pointer, removes any instances of it from all existing sub-lists.  This
    //  is necessary if an object had to be regged to multiple lists for some reason.
    //--The purging victim is *not* deleted, but ideally it wasn't regged with a deletion function.
    //--Since it's possible for a list to contain the same object multiple times (bad practice...)
    //  we cannot say we're done if we found the entry once!
    if(!pPtr) return;

    //--Iterate over all sub-lists.
    SugarLinkedList *rSubList = (SugarLinkedList *)mSpecialListList->PushIterator();
    while(rSubList)
    {
        //--Use Random-Access pointer for safe removal.
        void *rEntry = rSubList->SetToHeadAndReturn();
        while(rEntry)
        {
            //--Liberate matching cases.
            if(rEntry == pPtr)
            {
                rSubList->LiberateRandomPointerEntry();
            }

            //--Iterate up.
            rEntry = rSubList->IncrementAndGetRandomPointerEntry();
        }

        //--Iterate up.
        rSubList = (SugarLinkedList *)mSpecialListList->AutoIterate();
    }
}
SugarLinkedList *EntityManager::GetSpecialList(const char *pSubListName)
{
    //--Fetches a given sub-list.  Can return NULL!
    return (SugarLinkedList *)mSpecialListList->GetElementByName(pSubListName);
}
void EntityManager::ClearSpecialLists()
{
    mSpecialListList->ClearList();
    BootSpecialLists();
    //--DEBUG
    //DebugSpecialLists();
}
void EntityManager::DebugSpecialLists()
{
    //--Output the contents of the special lists
    SugarLinkedList *rSubList = (SugarLinkedList *)mSpecialListList->PushIterator();
    while(rSubList)
    {
        fprintf(stderr, "Special list %s has %d contents\n", mSpecialListList->GetIteratorName(), rSubList->GetListSize());
        rSubList = (SugarLinkedList *)mSpecialListList->AutoIterate();
    }
}
