//--Base
#include "EntityManager.h"

//--Classes
#include "Actor.h"
#include "RootEntity.h"
#include "PandemoniumLevel.h"
#include "VisualLevel.h"

//--CoreClasses
#include "SugarLinkedList.h"

//--Definitions
//--GUI
//--Libraries
//--Managers
#include "DebugManager.h"
#include "LuaManager.h"

int EntityManager::GetTurnCount()
{
    return mTurnCount;
}
Actor *EntityManager::GetActingEntity()
{
    //--Returns the Actor who is currently taking their turn. This may legally return NULL if there
    //  is no game currently taking place or on the off chance that everything is AI controlled.
    return rActingEntity;
}
Actor *EntityManager::GetLastPlayerEntity()
{
    //--Returns the last Actor who was flagged as player controlled.
    if(rLastPlayerEntity) return rLastPlayerEntity;

    //--If that wasn't found, try to find an entity that is player controlled.
    RootEntity *rEntity = (RootEntity *)mEntityList->PushIterator();
    while(rEntity)
    {
        //--If this is an Actor...
        if(rEntity->IsOfType(POINTER_TYPE_ACTOR))
        {
            //--Cast, check if it's player controlled.
            Actor *rCheckActor = (Actor *)rEntity;
            if(rCheckActor->IsPlayerControlled())
            {
                rLastPlayerEntity = rCheckActor;
                mEntityList->PopIterator();
                return rCheckActor;
            }
        }

        //--Next entity.
        rEntity = (RootEntity *)mEntityList->AutoIterate();
    }

    //--Couldn't find a player entity, return NULL.
    return NULL;
}
void EntityManager::SetTurnControlScript(const char *pPath)
{
    ResetString(mTurnControlScript, pPath);
}
void EntityManager::UpdateTurns()
{
    //--Run the turns of all entities until one comes up who doesn't handle the turn immediately.
    //  This may require ending the turn if, for example, the player is stunned.
    //--First, we need to error-check to make sure that at least one entity exists. Duh. Note that
    //  PlayerPony does *not* count and is ignored for turn considerations.
    if(mEntityList->GetListSize() < 1) return;

    //--Fail if there's no turn control script. If this is the case, there is no active game anyway.
    if(!mTurnControlScript) return;

    //--If the console is currently waiting on a keypress, don't execute the update. Also fail if the level in
    //  question is animating something.
    PandemoniumLevel *rActiveLevel = PandemoniumLevel::Fetch();
    if(rActiveLevel && (rActiveLevel->IsWaitingOnKeypress() || rActiveLevel->IsBlockingTurnChange())) return;

    //--If there is a VisualLevel, and it's moving, then ignore the update until it finishes.
    VisualLevel *rCheckLevel = VisualLevel::Fetch();
    if(rCheckLevel && rCheckLevel->IsMoving()) return;

    //--Clear this flag.
    rActingEntity = NULL;

    //--Begin loop.
    bool tIsDone = false;
    while(!tIsDone)
    {
        //--Seek through the list until an entity is found who hasn't acted yet.
        RootEntity *rEntity = (RootEntity *)mEntityList->PushIterator();
        while(rEntity)
        {
            //--Not an Actor? Ignore it. Entities self-destructing are also ignored.
            if(rEntity->GetType() != POINTER_TYPE_ACTOR || rEntity->mSelfDestruct)
            {
                rEntity = (RootEntity *)mEntityList->AutoIterate();
                continue;
            }

            //--Run the update function.
            rActingEntity = (Actor *)rEntity;
            rEntity->HandleTurnUpdate();

            //--If the entity is not immediately done its turn, we need to stop until it is.
            if(!rEntity->HasHandledTurn())
            {
                tIsDone = true;
                mEntityList->PopIterator();
                break;
            }
            //--Entity performed its turn, wipe old variables.
            else
            {
                Actor::xInstruction = INSTRUCTION_NOENTRY;
                rActingEntity = NULL;
            }

            //--If the console is currently waiting on a keypress, stop updating.
            if(rActiveLevel && (rActiveLevel->IsWaitingOnKeypress() || rActiveLevel->IsBlockingTurnChange()))
            {
                tIsDone = true;
                mEntityList->PopIterator();
                break;
            }

            //--Next.
            rEntity = (RootEntity *)mEntityList->AutoIterate();
        }

        //--If we reached the end of the list and were not done, then the turn has ended.
        //  Now we need to reset everything and run the loop again.
        if(!tIsDone)
        {
            NewTurn();
            break;
        }
    }

    //--The loop has completed because something is awaiting an action.
    //fprintf(stderr, "Loop is waiting for %s %p to act.\n", rActingEntity->GetName(), rLastPlayerEntity);

    //--If the acting entity is player controlled, store it.
    if(rActingEntity && rActingEntity->IsPlayerControlled()) rLastPlayerEntity = rActingEntity;
}
void EntityManager::NewTurn()
{
    //--At the end of a turn, all entities reset their flags and begin their turns anew. This
    //  is also where the spawner script is run, to spawn items or enemies.
    //--This is also where the turn count is incremented, for those who care about such things.
    mTurnCount ++;

    //--Debug.
    DebugManager::PushPrint(false, "==Ending Turn==\n");

    //--Order the PandemoniumLevel's console to mark the next line with a break.
    PandemoniumLevel::xNextLineHasBreak = true;

    //--Reset the flag on all entities. Some ignore this call.
    RootEntity *rEntity = (RootEntity *)mEntityList->PushIterator();
    while(rEntity)
    {
        rEntity->HandleEndOfTurn();
        rEntity = (RootEntity *)mEntityList->AutoIterate();
    }

    //--Tell the level about the new turn, if it exists.
    PandemoniumLevel *rCheckLevel = PandemoniumLevel::Fetch();
    if(rCheckLevel) rCheckLevel->HandleNewTurn();

    //--Run the AI Controller script.
    if(mTurnControlScript) LuaManager::Fetch()->ExecuteLuaFile(mTurnControlScript);

    //--Debug.
    DebugManager::PopPrint("==Turn ended successfully==\n");
}
