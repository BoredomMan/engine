#include "EntityManager.h"
#include "LuaManager.h"
#include "RootEntity.h"
#include "DataLibrary.h"
#include "HitDetection.h"
#include "SugarLinkedList.h"
#include "DebugManager.h"

//========================================= Lua Hooking ===========================================
void EntityManager::HookToLuaState(lua_State *pLuaState)
{
    //--[Normal Functions]
    /* EM_Exists(sName)
       EM_Exists("By ID", iUniqueID)
       Returns true if the named entity exists in the EntityManager. */
    lua_register(pLuaState, "EM_Exists", &Hook_EM_Exists);

    /* EM_PushEntity(sName)
       EM_PushEntity("By ID", iUniqueID)
       EM_PushEntity("SpecialList", sSpecialListName, sName)
       EM_PushEntity("Last Player", iDummy)
       EM_PushEntity("Acting Entity", iDummy)
       EM_PushEntity("Adventure Party", iIndex)
       Pushes the named entity to the DL's active object stack, if it exists.  Will fail silently if
       the Entity was not found, but the stack is still pushed. */
    lua_register(pLuaState, "EM_PushEntity", &Hook_EM_PushEntity);

    /* EM_GetLastID()
       Returns, as an integer, the UniqueID of the last registered entity to the EM. */
    lua_register(pLuaState, "EM_GetLastID", &Hook_EM_GetLastID);

    /* EM_SetTurnController(sPath)
       Sets the script that gets fired at the start of each turn. */
    lua_register(pLuaState, "EM_SetTurnController", &Hook_EM_SetTurnController);

    /* EM_GetTurnCount()
       Returns an integer representing how many turns have elapsed since game start. */
    lua_register(pLuaState, "EM_GetTurnCount", &Hook_EM_GetTurnCount);

    /* EM_PushIterator()
       Pushes the 0th entity on the entity list as the Active Object, and begins iteration. */
    lua_register(pLuaState, "EM_PushIterator", &Hook_EM_PushIterator);

    /* EM_AutoIterate() (1 boolean)
       Replaces the Active Object with the next entity on the list. Pops the activity stack
       if the list was at its end. Returns true if it pushed something, false if not. */
    lua_register(pLuaState, "EM_AutoIterate", &Hook_EM_AutoIterate);

    /* EM_PopIterator()
       If you need to stop iterating without going through all the entities, call this. */
    lua_register(pLuaState, "EM_PopIterator", &Hook_EM_PopIterator);

    /* EM_SetCleanerScript()
       Sets the script that cleans the program during a reset. */
    lua_register(pLuaState, "EM_SetCleanerScript", &Hook_EM_SetCleanerScript);
}

//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
//--[Normal Functions]
int Hook_EM_Exists(lua_State *L)
{
    //EM_Exists(sName)
    //EM_Exists("By ID", iUniqueID)
    uint32_t tArgs = lua_gettop(L);
    if(tArgs != 1 && tArgs != 2) return LuaArgError("EM_Exists");

    //--String method.
    if(tArgs == 1)
    {
        RootEntity *rEntity = EntityManager::Fetch()->GetEntity(lua_tostring(L, 1));
        lua_pushboolean(L, (rEntity != NULL));
    }
    //--ID method.
    else
    {
        RootEntity *rEntity = EntityManager::Fetch()->GetEntityI(lua_tointeger(L, 2));
        lua_pushboolean(L, (rEntity != NULL));
    }

    return 1;
}
int Hook_EM_PushEntity(lua_State *L)
{
    //EM_PushEntity(sName)
    //EM_PushEntity("By ID", iUniqueID)
    //EM_PushEntity("SpecialList", sSpecialListName[], sName)
    //EM_PushEntity("Last Player", iDummy)
    //EM_PushEntity("Acting Entity", iDummy)
    //EM_PushEntity("Adventure Party", iIndex)
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    rDataLibrary->PushActiveEntity();
    uint32_t tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("EM_PushEntity");

    //--Type
    const char *rSwitchType = lua_tostring(L, 1);

    //--Normal behavior
    if(tArgs == 1)
    {
        RootEntity *rEntity = EntityManager::Fetch()->GetEntity(rSwitchType);
        rDataLibrary->rActiveObject = rEntity;
    }
    //--By UniqueID
    else if(!strcasecmp("By ID", rSwitchType) && tArgs == 2)
    {
        RootEntity *rEntity = EntityManager::Fetch()->GetEntityI(lua_tointeger(L, 2));
        rDataLibrary->rActiveObject = rEntity;
    }
    //--Special list. List must exist first!
    else if(!strcasecmp("SpecialList", rSwitchType) && tArgs == 3)
    {
        SugarLinkedList *rSpecialList = EntityManager::Fetch()->GetSpecialList(lua_tostring(L, 2));
        if(rSpecialList)
        {
            RootEntity *rEntity = (RootEntity *) rSpecialList->GetElementByName(lua_tostring(L, 3));
            rDataLibrary->rActiveObject = rEntity;
        }
    }
    //--Last acting player entity. May legally push NULL.
    else if(!strcasecmp("Last Player", rSwitchType) && tArgs == 2)
    {
        rDataLibrary->rActiveObject = EntityManager::Fetch()->GetLastPlayerEntity();
        //fprintf(stderr, "Pushing LP %p\n", EntityManager::Fetch()->GetLastPlayerEntity());
    }
    //--Last acting entity. Can be the player, might not.
    else if(!strcasecmp("Acting Entity", rSwitchType) && tArgs == 2)
    {
        rDataLibrary->rActiveObject = EntityManager::Fetch()->GetActingEntity();
        //fprintf(stderr, "Pushing AE %p\n", EntityManager::Fetch()->GetLastPlayerEntity());
    }
    //--Party character, index is 0-3 inclusive. These are always TilemapActors.
    else if(!strcasecmp("Adventure Party", rSwitchType) && tArgs == 2)
    {
        //TODO: Make this robust!
        rDataLibrary->rActiveObject = EntityManager::Fetch()->GetEntity("Mei");
    }

    //--Error case.
    else
    {
        DebugManager::ForcePrint("Error: EM unable to resolve push type %s.\n", rSwitchType);
    }

    return 0;
}
int Hook_EM_GetLastID(lua_State *L)
{
    //EM_GetLastID()
    lua_pushinteger(L, EntityManager::Fetch()->GetLastReggedID());
    return 1;
}
int Hook_EM_SetTurnController(lua_State *L)
{
    //EM_SetTurnController(sName)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("EM_SetTurnController");

    EntityManager::Fetch()->SetTurnControlScript(lua_tostring(L, 1));
    return 0;
}
int Hook_EM_GetTurnCount(lua_State *L)
{
    //EM_GetTurnCount()
    lua_pushinteger(L, EntityManager::Fetch()->GetTurnCount());
    return 1;
}
int Hook_EM_PushIterator(lua_State *L)
{
    //EM_PushIterator()
    SugarLinkedList *rEntityList = EntityManager::Fetch()->GetEntityList();
    DataLibrary::Fetch()->PushActiveEntity(rEntityList->PushIterator());
    return 0;
}
int Hook_EM_AutoIterate(lua_State *L)
{
    //EM_AutoIterate()
    SugarLinkedList *rEntityList = EntityManager::Fetch()->GetEntityList();

    //--Get and check the next entity.
    void *rNextEntity = rEntityList->AutoIterate();
    if(rNextEntity)
    {
        DataLibrary::Fetch()->rActiveObject = rNextEntity;
        lua_pushboolean(L, true);
    }
    //--If it's NULL, pop the stack.
    else
    {
        DataLibrary::Fetch()->PopActiveEntity();
        lua_pushboolean(L, false);
    }

    return 1;
}
int Hook_EM_PopIterator(lua_State *L)
{
    //EM_PopIterator()
    SugarLinkedList *rEntityList = EntityManager::Fetch()->GetEntityList();
    rEntityList->PopIterator();
    DataLibrary::Fetch()->PopActiveEntity();
    return 0;
}
int Hook_EM_SetCleanerScript(lua_State *L)
{
    //EM_SetCleanerScript(sScriptPath)
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("EM_SetCleanerScript");

    EntityManager::Fetch()->SetCleanerScript(lua_tostring(L, 1));
    return 0;
}
