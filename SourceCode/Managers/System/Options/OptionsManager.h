//--[Options Manager]
//--Holds options which can be publically accessed and modified at any
//  time. Also contains Lua functions and other routines for accessing the
//  options, and automatic security protections.

//--This manager is also controlling various system functions, such as storing
//  and handling the command line and doing some low-level requests like gear
//  queries. It's a quadruple threat!

//--PLEASE NOTE: The macros for option manipulation are mostly there for Lua
//  and are not supposed to be used by the programmer, the options for C++ use
//  are public variables, or can be found in their respective managers.

#pragma once

#include "Definitions.h"
#include "Structures.h"

//--[Local Typedefs]
typedef void(*ConsoleFunctionPtr)(const char *);

//--[Local Definitions]
#define OM_ERROR_INT -700
#define OM_ERROR_FLOAT -700.0f
#define OM_ERROR_STRING NULL
#define OM_ERROR_BOOL false

#define OM_DEBUGVARS 10

#define OM_ORBITCODE_UNKNOWN -1

#define LOWRES_SCALE 0.25f
#define LOWRES_SCALEINV 4.00f

//--[Local Structures]
typedef struct
{
    //--Members
    bool mWasChanged;
    bool mIsDefinedExternally;
    bool mIsProgramLocal;
    void *rPtr;
    int mTypeFlag;
    char mFilePath[STD_MAX_LETTERS];

    //--Functions
    void Initialize()
    {
        mWasChanged = false;
        mIsDefinedExternally = false;
        mIsProgramLocal = false;
        rPtr = NULL;
        mTypeFlag = POINTER_TYPE_FAIL;
        memset(mFilePath, 0, sizeof(mFilePath));
    }
}OptionPack;

class OptionsManager
{
    private:
    //--System
    bool mErrorFlag;

    //--Storage
    SugarLinkedList *mOptionList; //OptionPack *, master
    SugarLinkedList *mDebugFlagList; //bool *, master

    //--Debug Variables
    float mDebugVariables[OM_DEBUGVARS];

    //--Console Commands
    static SugarLinkedList *xConsoleCommandList;

    //--Configuration File List
    SugarLinkedList *mConfigFilePaths;//char *, master

    protected:

    public:
    //--System
    OptionsManager();
    ~OptionsManager();

    //--Public Variables. Configuration.
    bool mIsFullscreen;
    bool mIsMemoryMode;
    int mWinSpawnX;
    int mWinSpawnY;
    int mWinSizeX;
    int mWinSizeY;
    int mMusicVolume;
    int mSoundVolume;

    //--Hardware Configuration
    bool mForcePowerOfTwo;
    bool mBlockDepthBuffer;
    bool mBlockStencilbuffer;
    int mRequestedAccelerateVisual;
    int mRequestedOpenGLMajor;
    int mRequestedOpenGLMinor;

    //--Engine Options
    bool mNoAudioSuppression;
    bool mForceDirectAspectRatios;
    bool mBlockDoubleBuffering;
    bool mUseRAMLoading;

    //--Pairanormal Options
    static bool xAllowFlashingImages;

    //--Statics
    static bool xIsDefiningOption;
    static bool xAutoLaunch3DMode;

    //--Property Queries
    bool GetErrorFlag();
    bool GetDebugFlag(const char *pName);
    OptionPack *GetOption(const char *pName);
    int GetOptionType(const char *pName);
    int GetOptionI(const char *pName);
    float GetOptionF(const char *pName);
    char *GetOptionS(const char *pName);
    bool GetOptionB(const char *pName);
    float GetDebugVar(int pSlot);
    int GetOrbitCode();

    //--Manipulators
    OptionPack *AddOption(const char *pName, void *pPtr, int pType, const char *pFileName);
    void SetOptionI(const char *pName, int pValue);
    void SetOptionF(const char *pName, float pValue);
    void SetOptionS(const char *pName, const char *pValue);
    void SetOptionB(const char *pName, bool pValue);
    void RemOption(const char *pName);
    void SetDebugVar(int pSlot, float pValue);
    void RegisterConfigPath(const char *pPath);

    //--Core Methods
    void SetupOptionList();
    bool CheckPtr(OptionPack *pPack, int pExpectedType);
    void PostProcess();
    void PrintReport();
    void RunConfigFiles();
    void ClearUpdateFlags();

    //--Update
    //--File I/O
    void WriteConfigFiles();
    void WriteLoadFile(const char *pPath);

    //--Drawing
    //--Pointer Routing
    //--Static Functions
    static OptionsManager *Fetch();

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);

    //--Command Functions
    static bool xErrorFlag;
    static void RegisterCommand(const char *pName, ConsoleFunctionPtr pFuncPtr);
    static void ExecuteCommand(const char *pCommandLine);
};

//--Hooking Functions
int Hook_OM_RegisterConfigPath(lua_State *L);
int Hook_OM_DefineOption(lua_State *L);
int Hook_OM_GetOption(lua_State *L);
int Hook_OM_SetOption(lua_State *L);
int Hook_OM_WriteConfigFiles(lua_State *L);
int Hook_OM_WriteLoadFile(lua_State *L);
