//--Base
#include "TransitionManager.h"

//--Classes
#include "RootExecutor.h"

//--CoreClasses
//--Definitions
#include "Global.h"
#include "GlDfn.h"

//--GUI
//--Libraries
//--Managers
#include "DebugManager.h"
#include "DisplayManager.h"

//=========================================== System ==============================================
TransitionManager::TransitionManager()
{
    //--System
    //--Executor
    mOwnsExecutor = false;
    mExecutor = NULL;

    //--Fading
    mTransitionStage = TM_TRANSITION_FAIL;
    mAlpha = 0.0f;
    mAlphaDeltaOut = TM_STANDARD_DELTA;
    mAlphaDeltaIn = TM_STANDARD_DELTA;

    //--Screen storage
    mTextureHandle = 0;
}
TransitionManager::~TransitionManager()
{
    delete mExecutor;
    glDeleteTextures(1, &mTextureHandle);
}

//====================================== Property Queries =========================================
bool TransitionManager::IsTransitioning()
{
    //--This is queried by a Gear when checking whether or not other Managers should render/do logic.
    //  It should actually report false for the first tick as the screen's data needs to be captured.
    if(mTransitionStage == TM_TRANSITION_FAIL   || mTransitionStage == TM_TRANSITION_GRAB ||
       mTransitionStage == TM_TRANSITION_FADEIN || mTransitionStage == TM_TRANSITION_RELEASE) return false;
    return true;
}

//========================================= Manipulators ==========================================
void TransitionManager::SetFadeSpeeds(float pDeltaOut, float pDeltaIn)
{
    if(pDeltaOut <= 0.0f) pDeltaOut = TM_STANDARD_DELTA;
    if(pDeltaIn  <= 0.0f) pDeltaIn  = TM_STANDARD_DELTA;
    mAlphaDeltaOut = pDeltaOut;
    mAlphaDeltaIn  = pDeltaIn;
}
void TransitionManager::SetExecutor(RootExecutor *pExecutor, bool pAcceptOwnership)
{
    if(mOwnsExecutor) delete mExecutor;
    mExecutor = pExecutor;
    mOwnsExecutor = pAcceptOwnership;
}
void TransitionManager::BeginTransition(RootExecutor *pExecutor, bool pAcceptOwnership)
{
    //--Causes the TM to begin transition activities.  For most Gears, Transitioning should cause
    //  everything else to stop activity and rendering once the TM says it's okay.
    if(!pExecutor) return;

    SetExecutor(pExecutor, pAcceptOwnership);
    mTransitionStage = TM_TRANSITION_GRAB;
    mAlpha = 0.0f;
}

//========================================= Core Methods ==========================================
//============================================ Update =============================================
void TransitionManager::Update()
{
    //--Update timers and perform actions as necessary.
    if(mTransitionStage == TM_TRANSITION_FAIL || mTransitionStage == TM_TRANSITION_GRAB) return;

    //--Fade out.
    if(mTransitionStage == TM_TRANSITION_FADEOUT)
    {
        mAlpha = mAlpha + mAlphaDeltaOut;
        if(mAlpha >= 1.0f)
        {
            mAlpha = 1.0f;
            mTransitionStage = TM_TRANSITION_SWITCH;
        }
    }
    //--Run the logic.
    else if(mTransitionStage == TM_TRANSITION_SWITCH)
    {
        if(mExecutor) mExecutor->Execute();
        SetExecutor(NULL, false);
        mTransitionStage = TM_TRANSITION_FADEIN;
    }
    //--Fade in.  Program executes during this time.
    else if(mTransitionStage == TM_TRANSITION_FADEIN)
    {
        mAlpha = mAlpha - mAlphaDeltaIn;
        if(mAlpha <= 0.0f)
        {
            mAlpha = 0.0f;
            mTransitionStage = TM_TRANSITION_RELEASE;
        }
    }
    //--Cleanup, if it was necessary.
    else if(mTransitionStage == TM_TRANSITION_RELEASE)
    {
        mTransitionStage = TM_TRANSITION_FAIL;
        glDeleteTextures(1, &mTextureHandle);
    }
}
void TransitionManager::UpdatePaused(uint8_t pPauseFlags)
{
    //--By default, a paused program does nothing. It may be desirable to have a special kind of
    //  pause that allows Transitions to occur.
}

//=========================================== File I/O ============================================
//=========================================== Drawing =============================================
#include "SugarBitmap.h"
#include "DataLibrary.h"
void TransitionManager::Render()
{
    //--Transition Manager should always be rendered LAST.  This is because it renders an overlay
    //  or otherwise needs to grab data from the screen.

    //--If not transitioning, render nothing.
    if(mTransitionStage == TM_TRANSITION_FAIL) return;

    //--GL Setup
    DisplayManager::StdModelPush();

    //--In the "Grab" stage, we need to copy the screen into a texture to fade over.
    if(mTransitionStage == TM_TRANSITION_GRAB)
    {
        glGenTextures(1, &mTextureHandle);
        glEnable(GL_TEXTURE_2D);
        glReadBuffer(GL_FRONT);
        glBindTexture(GL_TEXTURE_2D, mTextureHandle);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);

        int tWindowX = 0;
        int tWindowY = 0;
        int tWindowWid = Global::Shared()->gRenderedPixelsW;
        int tWindowHei = Global::Shared()->gRenderedPixelsH;
        glCopyTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, tWindowX, tWindowY, tWindowWid, tWindowHei, 0);
        mTransitionStage = TM_TRANSITION_FADEOUT;
        //fprintf(stderr, "Copied texture is %i %i\n", tWindowWid, tWindowHei);
        //fprintf(stderr, "Dest target is texture is %i %i\n", Global::Shared()->gRenderedPixelsW, Global::Shared()->gRenderedPixelsH);
    }

    //--Variables
    int tScreenWid = Global::Shared()->gScreenWidthPixels;
    int tScreenHei = Global::Shared()->gScreenHeightPixels;

    //--If fading out, we want to show the buffer we captured during the Grab.
    if(mTransitionStage == TM_TRANSITION_FADEOUT)
    {
        //--Cancel the viewport, which will remove letterboxing. The 'raw' image is captured directly
        //  from the buffer and includes black borders, so the viewport would just complicate the
        //  math unnecessarily.
        glViewport(0, 0, Global::Shared()->gWindowWidth, Global::Shared()->gWindowHeight);

        //--Background. The viewport may have changed, so it needs to be rendered with an offset.
        glEnable(GL_TEXTURE_2D);
        glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
        glBindTexture(GL_TEXTURE_2D, mTextureHandle);

        glBegin(GL_QUADS);
            glTexCoord2f(0, 1); glVertex3f(         0,          0, 0);
            glTexCoord2f(1, 1); glVertex3f(tScreenWid,          0, 0);
            glTexCoord2f(1, 0); glVertex3f(tScreenWid, tScreenHei, 0);
            glTexCoord2f(0, 0); glVertex3f(         0, tScreenHei, 0);
        glEnd();
    }

    //--Overlay
    glDisable(GL_TEXTURE_2D);
    glColor4f(0.0f, 0.0f, 0.0f, mAlpha);
    glBegin(GL_QUADS);
        glVertex3f(         0,          0, 0);
        glVertex3f(tScreenWid,          0, 0);
        glVertex3f(tScreenWid, tScreenHei, 0);
        glVertex3f(         0, tScreenHei, 0);
    glEnd();

    //--Clean
    DisplayManager::StdModelPop();
}

//======================================= Pointer Routing =========================================
//====================================== Static Functions =========================================
TransitionManager *TransitionManager::Fetch()
{
    return Global::Shared()->gTransitionManager;
}

//========================================= Lua Hooking ===========================================
void TransitionManager::HookToLuaState(lua_State *pLuaState)
{
    /* TM_SetupTransition("MapTarget")
       Creates a new Executor of the specified type.  Not all Executors are valid for transitions,
       and not all of the Executors that are can be created via scripts.  Once the Executor is created
       it is placed atop the Activity Stack and should be popped with TM_ExecTransition.
       Until execution, TM_ExecutorSetProperty can customize it to your liking. */
    lua_register(pLuaState, "TM_SetupTransition", &Hook_TM_SetupTransition);

    /* TM_ExecutorSetProperty("FadeSpeeds", fFadeOut, fFadeIn)
       TM_ExecutorSetProperty("MapPath", sName[])    - "MapTarget" only
       Sets the specified property.  Some properties may not be available to all Executors, in which
       case an error will be barked. */
    lua_register(pLuaState, "TM_ExecutorSetProperty", &Hook_TM_ExecutorSetProperty);

    /* TM_ExecTransition()
       Pops off the top of the Activity Stack and executes it as a transition Executor. */
    lua_register(pLuaState, "TM_ExecTransition", &Hook_TM_ExecTransition);
}

//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
int Hook_TM_SetupTransition(lua_State *L)
{
    //TM_SetupTransition("MapTarget")
    int tArgs = lua_gettop(L);
    DataLibrary::Fetch()->PushActiveEntity();
    if(tArgs < 1) return LuaArgError("TM_SetupTransition");

    //--Setup
    const char *rType = lua_tostring(L, 1);

    //--Sets a transition to a different map.
    if(!strcmp(rType, "MapTarget"))
    {
    }
    //--Bark a warning
    else
    {
        DebugManager::ForcePrint("TM_SetupTransition: Error, cannot resolve %s with %i args\n", rType, tArgs);
    }

    return 0;
}
int Hook_TM_ExecutorSetProperty(lua_State *L)
{
    //TM_ExecutorSetProperty("FadeSpeeds", fFadeOut, fFadeIn)
    //TM_ExecutorSetProperty("MapPath", sName[])    - "MapTarget" only
    int tArgs = lua_gettop(L);
    if(tArgs < 1) return LuaArgError("TM_ExecutorSetProperty");

    //--Setup
    const char *rType = lua_tostring(L, 1);

    //--Gets a RootExecutor.  Not all calls actually check it, so just set validity.
    RootExecutor *rExecutor = NULL;
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    if(rDataLibrary->IsActiveValid(POINTER_TYPE_EXECUTOR_BEGIN, POINTER_TYPE_EXECUTOR_END))
    {
        rExecutor = (RootExecutor *)rDataLibrary->rActiveObject;
    }

    //--Changes fading speeds for the next Transition.  Can also (theoretically) affect one if it
    //  is currently happening.
    if(!strcmp(rType, "FadeSpeeds") && tArgs == 3)
    {
        TransitionManager::Fetch()->SetFadeSpeeds(lua_tonumber(L, 2), lua_tonumber(L, 3));
    }
    //--Sample.
    else if(!strcmp(rType, "Sample"))
    {
        rExecutor->GetType();
    }
    //--Bark a warning
    else
    {
        DebugManager::ForcePrint("TM_ExecutorSetProperty: Error, cannot resolve %s with %i args\n", rType, tArgs);
    }

    return 0;
}
int Hook_TM_ExecTransition(lua_State *L)
{
    //TM_ExecTransition()

    //--Get and check the top of the stack.
    DataLibrary *rDataLibrary = DataLibrary::Fetch();
    if(!rDataLibrary->IsActiveValid(POINTER_TYPE_EXECUTOR_BEGIN, POINTER_TYPE_EXECUTOR_END)) return 0;
    RootExecutor *rExecutor = (RootExecutor *)rDataLibrary->PopActiveEntity();

    TransitionManager::Fetch()->BeginTransition(rExecutor, true);

    return 0;
}
