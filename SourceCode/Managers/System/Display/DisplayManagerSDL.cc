//--Base
#include "DisplayManager.h"

//--Classes
//--CoreClasses
#include "SugarLinkedList.h"
#include "SugarBitmap.h"

//--Definitions
#include "Global.h"

//--GUI
//--Libraries
//--Managers
#include "DebugManager.h"
#include "OptionsManager.h"

//--[SDL Version]
//--DisplayManager post-setup using SDL. This will create the display and store any screen info
//  it can.
#if defined _SDL_PROJECT_
void DisplayManager::ConstructorTail()
{
    //--Function is called immediately after the DisplayManager has called its setup functions.
    //  It will create the display.
    DebugManager::PushPrint(true, "[DisplayManager] Creating display...\n");

    //--Setup.
    uint32_t tWindowFlags = SDL_WINDOW_OPENGL;
    GLOBAL *rGlobal = Global::Shared();
    OptionsManager *rOptionsManager = OptionsManager::Fetch();

    //--OpenGL Library.
    SDL_GL_LoadLibrary(NULL);

    //--Version request.
    SDL_GL_SetAttribute(SDL_GL_ACCELERATED_VISUAL, rOptionsManager->mRequestedAccelerateVisual);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, rOptionsManager->mRequestedOpenGLMajor);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, rOptionsManager->mRequestedOpenGLMinor);

    //--Allow backwards compatibility.
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_COMPATIBILITY);

    //--Color buffer request.
    SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 16);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

    //--Fullscreen or Windowed Flag
    DebugManager::Print("Handling fullscreen/windowed flags.\n");
    bool tUseFullscreen = rOptionsManager->GetOptionB("Fullscreen");
    if(!tUseFullscreen)
    {
        mIsFullscreen = false;
    }
    else
    {
        tWindowFlags = tWindowFlags | SDL_WINDOW_FULLSCREEN;
        mIsFullscreen = true;
    }

    //--Sets up the Depth and Stencil Buffer stuff.  These can be blocked for debug purposes
    //  by the options.
    DebugManager::Print("Checking depth buffer flag.\n");
    bool tBlockDepthBuffer = rOptionsManager->GetOptionB("BlockDepthBuffer");
    if(!tBlockDepthBuffer)
    {
        //--This option is disabled in SDL.
    }

    DebugManager::Print("Checking stencil buffer flag.\n");
    SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);
    bool tBlockStencilBuffer = rOptionsManager->GetOptionB("BlockStencilBuffer");
    if(!tBlockStencilBuffer)
    {
        //--This option is disabled in SDL.
    }

    //--Get the location to spawn the window.  Only executes if either is non
    //  zero for compatibility reasons.
    DebugManager::Print("Checking window location flags.\n");
    int tWindowX = rOptionsManager->GetOptionI("StartWinX");
    int tWindowY = rOptionsManager->GetOptionI("StartWinY");

    //--Flag for stereo buffers. Can be switched off.
    DebugManager::Print("Checking block-double-buffer flag.\n");
    if(!rOptionsManager->GetOptionB("BlockDoubleBuffering"))
    {
        //--This option is disabled in SDL.
    }

    //--Get the size of the window (not the virtual canvas).
    rGlobal->gWindowWidth = rOptionsManager->GetOptionI("WinSizeX");
    rGlobal->gWindowHeight = rOptionsManager->GetOptionI("WinSizeY");

    //--Create the Display and test it.
    DebugManager::ForcePrint("\n");
    DebugManager::ForcePrint("If the program crashes at this point, then your display could not be created.\n");
    DebugManager::ForcePrint("This means your OGL drivers are not updated, or the program is unable to run.\n");
    mWindow = SDL_CreateWindow("Starlight Engine", tWindowX, tWindowY, rGlobal->gWindowWidth, rGlobal->gWindowHeight, tWindowFlags);
    if(!mWindow)
    {
        DebugManager::ForcePrint("Display of size %i by %i failed to create. Error: %s\n", rGlobal->gWindowWidth, rGlobal->gWindowHeight, SDL_GetError());
        DebugManager::ForcePrint("With no display, the program will now exit\n");
        rGlobal->gQuit = true;
        return;
    }

    //--Associate the GL Context with this window.
    mGLContext = SDL_GL_CreateContext(mWindow);

    //--Find the display mode that worked and set it as active in the DisplayInfo list.
    DebugManager::Print("Storing active display mode.\n");
    for(int i = 0; i < mDisplayModesTotal; i ++)
    {
        if(mDisplayModes[i].mWidth == rGlobal->gWindowWidth && mDisplayModes[i].mHeight == rGlobal->gWindowHeight)
        {
            mActiveDisplaySlot = i;
            break;
        }
    }

    //--Standard GL Settings (GL Stack pops back to these)
    DebugManager::Print("Setting OpenGL standards.\n");
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_ALPHA_TEST);
    glAlphaFunc(GL_GREATER, 0);

    //--Static setup.
    DebugManager::Print("Building enumerations list.\n");
    ConstructEnumerationList();

    //--Store GL statistics.
    DebugManager::Print("Getting GL statistics.\n");
    glGetIntegerv(GL_MAX_TEXTURE_SIZE, &SugarBitmap::xMaxTextureSize);
    SugarBitmap::xAtlasMaxSize = SugarBitmap::xMaxTextureSize / 2;
    //SugarBitmap::xMaxTextureSize = 4096;

    //--These may not be the same if fullscreen was set, so store them.
    DebugManager::Print("Refreshing screen sizes.\n");
    SDL_GetWindowSize(mWindow, &rGlobal->gWindowWidth, &rGlobal->gWindowHeight);

    //--Clean up.
    DebugManager::PopPrint("[DisplayManager] Display setup complete.\n");
}
void DisplayManager::SaveDisplayModes()
{
    //--Checks the available display modes and stores their data inside a library-independent structure.
    //--Collects a list of all available display modes. We are only interested in unique modes:
    //  two modes with the same W/H but different refresh rates are ignored, we only want modes
    //  with refresh rates of 60.0 fps. Likewise, only 32bit formats are acceptable.
    int tRawModesTotal = SDL_GetNumDisplayModes(0);
    SetMemoryData(__FILE__, __LINE__);
    SDL_DisplayMode *tModeList = (SDL_DisplayMode *)starmemoryalloc(sizeof(SDL_DisplayMode) * tRawModesTotal);
    for(int i = 0; i < tRawModesTotal; i ++)
    {
        //--Get the raw mode. Allegro passes NULL back on failure.
        if(SDL_GetDisplayMode(0, i, &tModeList[i])) continue;

        //--Check backwards. If the previous display mode was the same size, we can ignore this one.
        //  We are only interested in display sizes.
        if(i == 0 || (tModeList[i-1].w != tModeList[i].w || tModeList[i-1].h != tModeList[i].h))
        {
            mDisplayModesTotal ++;
        }
    }

    //--Allocate space for all the modes that matched.
    char tBuffer[64];
    int tScans = 0;
    SetMemoryData(__FILE__, __LINE__);
    mDisplayModes = (DisplayInfo *)starmemoryalloc(sizeof(DisplayInfo) * mDisplayModesTotal);
    for(int i = 0; i < mDisplayModesTotal; i ++)
    {
        //--Find the next applicable resolution and store it. It's logically impossible for a
        //  particular structure to not be initialized, but I NULL the string here for safety anyway.
        mDisplayModes[i].mDetails = NULL;
        for(int p = tScans; p < tRawModesTotal; p ++)
        {
            if(p == 0 || (tModeList[p-1].w != tModeList[p].w || tModeList[p-1].h != tModeList[p].h))
            {
                //--Save for later.
                tScans = p+1;

                //--Copy data.
                mDisplayModes[i].mWidth = tModeList[p].w;
                mDisplayModes[i].mHeight = tModeList[p].h;
                mDisplayModes[i].mRefreshRate = tModeList[p].refresh_rate;

                //--Create a string which has this as simple, human-readable data.
                sprintf(tBuffer, "%ix%ix%i", mDisplayModes[i].mWidth, mDisplayModes[i].mHeight, mDisplayModes[i].mRefreshRate);
                ResetString(mDisplayModes[i].mDetails, tBuffer);

                //--Print.
                //fprintf(stderr, "Accepted Display Mode %i\n", i);
                //fprintf(stderr, " %i x %i - %s\n", mDisplayModes[i].mWidth, mDisplayModes[i].mHeight, mDisplayModes[i].mDetails);
                break;
            }
        }
    }

    //--Dummy display mode. Do not deallocate until destructor!
    mDummyDisplayInfo.mWidth = 0;
    mDummyDisplayInfo.mHeight = 0;
    mDummyDisplayInfo.mRefreshRate = 0;
    mDummyDisplayInfo.mDetails = NULL;
    ResetString(mDummyDisplayInfo.mDetails, "ERROR");

    //--Clean.
    free(tModeList);
}
#endif
