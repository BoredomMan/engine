//--Base
#include "DisplayManager.h"

//--Classes
//--Definitions
//--Generics
//--GUI
//--Libraries
//--Managers

char *DisplayManager::TextFileRead(const char *pFilePath)
{
    //--Provided a file, read the whole thing and spit it back out as a single
    //  string.  This is needed for shaders.
	FILE *fInfile;
	char *nContent = NULL;

	int tCharCount = 0;

	if (pFilePath != NULL)
    {
		fInfile = fopen(pFilePath,"rt");
		if(!fInfile) return NULL;

        fseek(fInfile, 0, SEEK_END);
        tCharCount = ftell(fInfile);
        rewind(fInfile);

        if(tCharCount > 0)
        {
            SetMemoryData(__FILE__, __LINE__);
            nContent = (char *)starmemoryalloc(sizeof(char) * (tCharCount+1));
            tCharCount = fread(nContent, sizeof(char), tCharCount, fInfile);
            nContent[tCharCount] = '\0';
        }
        fclose(fInfile);
	}
	return nContent;
}
