//--Base
#include "DisplayManager.h"

//--Classes
//--CoreClasses
//--Definitions
#include "Global.h"

//--GUI
//--Libraries
//--Managers

//--[Forward Declarations]
//--These are GLUT worker functions found at the bottom of the file. If you're going to sift them,
//  wear a hazmat suit.
int glhProjectf(float objx, float objy, float objz, float *modelview, float *projection, int *viewport, float *windowCoordinate);
void MultiplyMatrices4by4OpenGL_FLOAT(float *result, float *matrix1, float *matrix2);
void MultiplyMatrixByVector4by4OpenGL_FLOAT(float *resultvector, const float *matrix, const float *pvector);
int glhInvertMatrixf2(float *m, float *out);
int glhUnProjectf(float winx, float winy, float winz, float *modelview, float *projection, int *viewport, float *objectCoordinate);

void DisplayManager::StdFrustum()
{
    //--Standard Frustum(3D) projection.  Always call in conjuction with the clean operation.
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glPushMatrix();

    //--Near clipping plane. Since we're not doing anything like megatexturing, we just need to make the
    //  near clip plane reasonably close. This allows us to see the glorious/ugly detail of walls without
    //  having them clip into the visible frustum while walking next to them.
    float cNearPlane = 0.1f;
    float cFarPlane = 4000.0f;

    //--Aspect ratio. If the window hasn't been set up correctly, assume the aspect ratio is the same
    //  as it the virtual canvas ratio.
    GLOBAL *rGlobal = Global::Shared();
    float tVerticalAspect = 0.75f;
    float tWidthOverHeight = (float)VIRTUAL_CANVAS_X / (float)VIRTUAL_CANVAS_Y;

    //--If the window was set up correctly, compute the true aspect ratio. Frustums take advantage of the
    //  entire viewing space and don't rescale.
    if(rGlobal->gWindowHeight != 0.0f)
    {
        tWidthOverHeight = (float)rGlobal->gWindowWidth / (float)rGlobal->gWindowHeight;
        glViewport(0.0f, 0.0f, rGlobal->gWindowWidth, rGlobal->gWindowHeight);
    }

    //--Build the Frustum,
    float tHorizontalAspect = tVerticalAspect * tWidthOverHeight;
    glFrustum(tHorizontalAspect * -1.0f * cNearPlane, tHorizontalAspect * cNearPlane, tVerticalAspect * cNearPlane, tVerticalAspect * -1.0f * cNearPlane, cNearPlane, cFarPlane);

    //--Switch to the model view (cubes/entities/etc) and save the stack properties.
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glPushMatrix();

    //--Default GLState. Everything should pop back to this.
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_ALPHA_TEST);
    glAlphaFunc(GL_GREATER, 0);

    //--Store the attributes.
    glPushAttrib(GL_ALL_ATTRIB_BITS);
}
void DisplayManager::CleanFrustum()
{
    //--Clean operation.
    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();
    glMatrixMode(GL_PROJECTION);
    glPopMatrix();
    glPopAttrib();
}
void DisplayManager::StdOrtho(int pXSize, int pYSize)
{
    //--Orthographic projection.  The standard refers to the depth being 0 to 1  while still retaining
    //  a depth buffer.  This is used for GUI and screen effects.
    //--Always call this in conjuction with CleanOrtho.  Do NOT use your own functions, as a change
    //  to the Std would cause them to invalidate!
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0, pXSize, pYSize, 0, 0, 1);
    glPushMatrix();

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glPushMatrix();

    //--Default GLState. Everything should pop back to this.
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_ALPHA_TEST);
    glAlphaFunc(GL_GREATER, 0);

    //--Also stores the attributes.
    glPushAttrib(GL_ALL_ATTRIB_BITS);
}
void DisplayManager::StdOrtho()
{
    //--Overload to pass in standard size automatically. Also automatically handles letterboxing.
    GLOBAL *rGlobal = Global::Shared();
    int tXSize = Global::Shared()->gScreenWidthPixels;
    int tYSize = Global::Shared()->gScreenHeightPixels;

    //--Reset.
    xViewportScaleX = 1.0f;
    xViewportScaleY = 1.0f;

    //--Normal case. Hard letterbox, nothing can render off the edge.
    if(xViewportW != 0 && xViewportH != 0 && !xDontSetViewport && xUseHardLetterbox)
    {
        DisplayManager::StdOrtho(tXSize, tYSize);
        glViewport(xHorizontalOffset, xVerticalOffset, xViewportW, xViewportH);
    }
    //--Special: Attempt to dynamically resize everything, but leave a full viewport. This allows rendering
    //  off the screen edge (assuming there's window space) but maintains the aspect ratio.
    else if(xViewportW != 0 && xViewportH != 0)
    {
        //--Max out the viewport.
        DisplayManager::StdOrtho(rGlobal->gWindowWidth, rGlobal->gWindowHeight);
        glViewport(0.0f, 0.0f, rGlobal->gWindowWidth, rGlobal->gWindowHeight);

        //--Initial ratios.
        xViewportScaleX = (float)rGlobal->gWindowWidth  / (float)VIRTUAL_CANVAS_X;
        xViewportScaleY = (float)rGlobal->gWindowHeight / (float)VIRTUAL_CANVAS_Y;

        //--Compute the boost due to the aspect ratio.
        if(xViewportScaleX > xViewportScaleY)
        {
            xViewportScaleX = xViewportScaleY;
            float tEffectiveWidth = (float)VIRTUAL_CANVAS_X * xViewportScaleY;
            xHorizontalOffset = ((float)rGlobal->gWindowWidth - tEffectiveWidth) / 2.0f;
            xVerticalOffset = 0;
        }
        else
        {
            xViewportScaleY = xViewportScaleX;
            float tEffectiveHeight = (float)VIRTUAL_CANVAS_Y * xViewportScaleX;
            xVerticalOffset = ((float)rGlobal->gWindowHeight - tEffectiveHeight) / 2.0f;
            xHorizontalOffset = 0;
        }

        //--Upscale everything.
        glTranslatef(xHorizontalOffset, xVerticalOffset, 0.0f);
        glScalef(xViewportScaleX, xViewportScaleY, 1.0f);
    }

    //--Compute scaling. This is how thick the borders are at the left or top of the screen, modified according to the scale
    //  and size of the offset. This does apply to fullscreen.
    xScaledOffsetX = 0.0f;
    xScaledOffsetY = 0.0f;
    if(xViewportScaleX != 0.0f && xViewportScaleY != 0.0f)
    {
        xScaledOffsetX = ((float)xHorizontalOffset / xViewportScaleX);
        xScaledOffsetY = ((float)xVerticalOffset   / xViewportScaleY);
    }
    else
    {
        xScaledOffsetX = 0.0f;
        xScaledOffsetY = 0.0f;
    }

    //--Debug report.
    if(false)
    {
        static int xCount = 0;
        xCount ++;
        if(xCount % 180 == 0)
        {
            fprintf(stderr, "Report:\n");
            fprintf(stderr, " Viewport size %i %i\n", xViewportW, xViewportH);
            fprintf(stderr, " Window Sizes %i %i\n", rGlobal->gWindowWidth, rGlobal->gWindowHeight);
            fprintf(stderr, " Offsets %i %i\n", xHorizontalOffset, xVerticalOffset);
            fprintf(stderr, " Viewport scales: %f %f\n", xViewportScaleX, xViewportScaleY);
            fprintf(stderr, " Modded Offsets: %f %f\n", xScaledOffsetX, xScaledOffsetY);
        }
    }
}
void DisplayManager::CleanOrtho()
{
    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();
    glMatrixMode(GL_PROJECTION);
    glPopMatrix();
    glPopAttrib();
}
void DisplayManager::StdModelPush()
{
    //--Pushes the modelview matrix as it is currently.  Also pushes any attributes.
    //  Always pair this with StdModelPop().
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glPushAttrib(GL_ALL_ATTRIB_BITS);
}
void DisplayManager::StdModelPop()
{
    //--Converse of StdModelPush().
    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();
    glPopAttrib();
}
void DisplayManager::StdAttribPush()
{
    glPushAttrib(GL_ALL_ATTRIB_BITS);
}
void DisplayManager::StdAttribPop()
{
    glPopAttrib();
}

//--[Projection Code]
//--This static function uses the worker function to resolve the position automatically, based on
//  what the display manager is currently projecting.
void DisplayManager::Project(float pX, float pY, float pZ, float *pReturns)
{
    //--If some jerkwad gave us a null pointer to return the data, fail.
    if(!pReturns) return;

    //--Get the projection/modelview/viewport data.
    float tModelview[16];
    glGetFloatv(GL_MODELVIEW_MATRIX, tModelview);
    float tProjection[16];
    glGetFloatv(GL_PROJECTION_MATRIX, tProjection);
    int tViewport[4];
    glGetIntegerv(GL_VIEWPORT, tViewport);

    //--Run the code.
    glhProjectf(pX, pY, pZ, tModelview, tProjection, tViewport, pReturns);

    //--Flip the Y coordinate, since GL uses upside-down projection.
    pReturns[1] = VIRTUAL_CANVAS_Y - pReturns[1];
}

//--[Unprojection Code]
//--Helpful static function to use the current GL state for unprojecting.
void DisplayManager::Unproject(float pX, float pY, float pZ, float *pReturns)
{
    //--If some jerkwad gave us a null pointer to return the data, fail.
    if(!pReturns) return;

    //--Get the projection/modelview/viewport data.
    float tModelview[16];
    glGetFloatv(GL_MODELVIEW_MATRIX, tModelview);
    float tProjection[16];
    glGetFloatv(GL_PROJECTION_MATRIX, tProjection);
    int tViewport[4];
    glGetIntegerv(GL_VIEWPORT, tViewport);

    //--Run the code.
    glhUnProjectf(pX, pY, pZ, tModelview, tProjection, tViewport, pReturns);

    //--Flip the Y coordinate, since GL uses upside-down projection.
    pReturns[1] = VIRTUAL_CANVAS_Y - pReturns[1];
}

//--[Worker Functions]
//--Abandon all hope, ye who enter here. These functions were pulled off the GLUT website and
//  not proofread. Good luck.
int glhProjectf(float objx, float objy, float objz, float *modelview, float *projection, int *viewport, float *windowCoordinate)
{
    //Transformation vectors
    float fTempo[8];
    //Modelview transform
    fTempo[0]=modelview[0]*objx+modelview[4]*objy+modelview[8]*objz+modelview[12];  //w is always 1
    fTempo[1]=modelview[1]*objx+modelview[5]*objy+modelview[9]*objz+modelview[13];
    fTempo[2]=modelview[2]*objx+modelview[6]*objy+modelview[10]*objz+modelview[14];
    fTempo[3]=modelview[3]*objx+modelview[7]*objy+modelview[11]*objz+modelview[15];
    //Projection transform, the final row of projection matrix is always [0 0 -1 0]
    //so we optimize for that.
    fTempo[4]=projection[0]*fTempo[0]+projection[4]*fTempo[1]+projection[8]*fTempo[2]+projection[12]*fTempo[3];
    fTempo[5]=projection[1]*fTempo[0]+projection[5]*fTempo[1]+projection[9]*fTempo[2]+projection[13]*fTempo[3];
    fTempo[6]=projection[2]*fTempo[0]+projection[6]*fTempo[1]+projection[10]*fTempo[2]+projection[14]*fTempo[3];
    fTempo[7]=-fTempo[2];
    //The result normalizes between -1 and 1
    if(fTempo[7]==0.0)	//The w value
     return 0;
    fTempo[7]=1.0/fTempo[7];
    //Perspective division
    fTempo[4]*=fTempo[7];
    fTempo[5]*=fTempo[7];
    fTempo[6]*=fTempo[7];
    //Window coordinates
    //Map x, y to range 0-1
    windowCoordinate[0]=(fTempo[4]*0.5+0.5)*viewport[2]+viewport[0];
    windowCoordinate[1]=(fTempo[5]*0.5+0.5)*viewport[3]+viewport[1];
    //This is only correct when glDepthRange(0.0, 1.0)
    windowCoordinate[2]=(1.0+fTempo[6])*0.5;	//Between 0 and 1
    return 1;
}
int glhUnProjectf(float winx, float winy, float winz, float *modelview, float *projection, int *viewport, float *objectCoordinate)
{
    //Transformation matrices
    float m[16], A[16];
    float in[4], out[4];
    //Calculation for inverting a matrix, compute projection x modelview
    //and store in A[16]
    MultiplyMatrices4by4OpenGL_FLOAT(A, projection, modelview);
    //Now compute the inverse of matrix A
    if(glhInvertMatrixf2(A, m)==0)
     return 0;
    //Transformation of normalized coordinates between -1 and 1
    in[0]=(winx-(float)viewport[0])/(float)viewport[2]*2.0-1.0;
    in[1]=(winy-(float)viewport[1])/(float)viewport[3]*2.0-1.0;
    in[2]=2.0*winz-1.0;
    in[3]=1.0;
    //Objects coordinates
    MultiplyMatrixByVector4by4OpenGL_FLOAT(out, m, in);
    if(out[3]==0.0)
     return 0;
    out[3]=1.0/out[3];
    objectCoordinate[0]=out[0]*out[3];
    objectCoordinate[1]=out[1]*out[3];
    objectCoordinate[2]=out[2]*out[3];
    return 1;
}
void MultiplyMatrices4by4OpenGL_FLOAT(float *result, float *matrix1, float *matrix2)
  {
    result[0]=matrix1[0]*matrix2[0]+
      matrix1[4]*matrix2[1]+
      matrix1[8]*matrix2[2]+
      matrix1[12]*matrix2[3];
    result[4]=matrix1[0]*matrix2[4]+
      matrix1[4]*matrix2[5]+
      matrix1[8]*matrix2[6]+
      matrix1[12]*matrix2[7];
    result[8]=matrix1[0]*matrix2[8]+
      matrix1[4]*matrix2[9]+
      matrix1[8]*matrix2[10]+
      matrix1[12]*matrix2[11];
    result[12]=matrix1[0]*matrix2[12]+
      matrix1[4]*matrix2[13]+
      matrix1[8]*matrix2[14]+
      matrix1[12]*matrix2[15];
    result[1]=matrix1[1]*matrix2[0]+
      matrix1[5]*matrix2[1]+
      matrix1[9]*matrix2[2]+
      matrix1[13]*matrix2[3];
    result[5]=matrix1[1]*matrix2[4]+
      matrix1[5]*matrix2[5]+
      matrix1[9]*matrix2[6]+
      matrix1[13]*matrix2[7];
    result[9]=matrix1[1]*matrix2[8]+
      matrix1[5]*matrix2[9]+
      matrix1[9]*matrix2[10]+
      matrix1[13]*matrix2[11];
    result[13]=matrix1[1]*matrix2[12]+
      matrix1[5]*matrix2[13]+
      matrix1[9]*matrix2[14]+
      matrix1[13]*matrix2[15];
    result[2]=matrix1[2]*matrix2[0]+
      matrix1[6]*matrix2[1]+
      matrix1[10]*matrix2[2]+
      matrix1[14]*matrix2[3];
    result[6]=matrix1[2]*matrix2[4]+
      matrix1[6]*matrix2[5]+
      matrix1[10]*matrix2[6]+
      matrix1[14]*matrix2[7];
    result[10]=matrix1[2]*matrix2[8]+
      matrix1[6]*matrix2[9]+
      matrix1[10]*matrix2[10]+
      matrix1[14]*matrix2[11];
    result[14]=matrix1[2]*matrix2[12]+
      matrix1[6]*matrix2[13]+
      matrix1[10]*matrix2[14]+
      matrix1[14]*matrix2[15];
    result[3]=matrix1[3]*matrix2[0]+
      matrix1[7]*matrix2[1]+
      matrix1[11]*matrix2[2]+
      matrix1[15]*matrix2[3];
    result[7]=matrix1[3]*matrix2[4]+
      matrix1[7]*matrix2[5]+
      matrix1[11]*matrix2[6]+
      matrix1[15]*matrix2[7];
    result[11]=matrix1[3]*matrix2[8]+
      matrix1[7]*matrix2[9]+
      matrix1[11]*matrix2[10]+
      matrix1[15]*matrix2[11];
    result[15]=matrix1[3]*matrix2[12]+
      matrix1[7]*matrix2[13]+
      matrix1[11]*matrix2[14]+
      matrix1[15]*matrix2[15];
  }

  void MultiplyMatrixByVector4by4OpenGL_FLOAT(float *resultvector, const float *matrix, const float *pvector)
  {
    resultvector[0]=matrix[0]*pvector[0]+matrix[4]*pvector[1]+matrix[8]*pvector[2]+matrix[12]*pvector[3];
    resultvector[1]=matrix[1]*pvector[0]+matrix[5]*pvector[1]+matrix[9]*pvector[2]+matrix[13]*pvector[3];
    resultvector[2]=matrix[2]*pvector[0]+matrix[6]*pvector[1]+matrix[10]*pvector[2]+matrix[14]*pvector[3];
    resultvector[3]=matrix[3]*pvector[0]+matrix[7]*pvector[1]+matrix[11]*pvector[2]+matrix[15]*pvector[3];
  }

#define SWAP_ROWS_DOUBLE(a, b) { double *_tmp = a; (a)=(b); (b)=_tmp; }
#define SWAP_ROWS_FLOAT(a, b) { float *_tmp = a; (a)=(b); (b)=_tmp; }
#define MAT(m,r,c) (m)[(c)*4+(r)]

  //This code comes directly from GLU except that it is for float
  int glhInvertMatrixf2(float *m, float *out)
  {
   float wtmp[4][8];
   float m0, m1, m2, m3, s;
   float *r0, *r1, *r2, *r3;
   r0 = wtmp[0], r1 = wtmp[1], r2 = wtmp[2], r3 = wtmp[3];
   r0[0] = MAT(m, 0, 0), r0[1] = MAT(m, 0, 1),
      r0[2] = MAT(m, 0, 2), r0[3] = MAT(m, 0, 3),
      r0[4] = 1.0, r0[5] = r0[6] = r0[7] = 0.0,
      r1[0] = MAT(m, 1, 0), r1[1] = MAT(m, 1, 1),
      r1[2] = MAT(m, 1, 2), r1[3] = MAT(m, 1, 3),
      r1[5] = 1.0, r1[4] = r1[6] = r1[7] = 0.0,
      r2[0] = MAT(m, 2, 0), r2[1] = MAT(m, 2, 1),
      r2[2] = MAT(m, 2, 2), r2[3] = MAT(m, 2, 3),
      r2[6] = 1.0, r2[4] = r2[5] = r2[7] = 0.0,
      r3[0] = MAT(m, 3, 0), r3[1] = MAT(m, 3, 1),
      r3[2] = MAT(m, 3, 2), r3[3] = MAT(m, 3, 3),
      r3[7] = 1.0, r3[4] = r3[5] = r3[6] = 0.0;
   /* choose pivot - or die */
   if (fabsf(r3[0]) > fabsf(r2[0]))
      SWAP_ROWS_FLOAT(r3, r2);
   if (fabsf(r2[0]) > fabsf(r1[0]))
      SWAP_ROWS_FLOAT(r2, r1);
   if (fabsf(r1[0]) > fabsf(r0[0]))
      SWAP_ROWS_FLOAT(r1, r0);
   if (0.0 == r0[0])
      return 0;
   /* eliminate first variable     */
   m1 = r1[0] / r0[0];
   m2 = r2[0] / r0[0];
   m3 = r3[0] / r0[0];
   s = r0[1];
   r1[1] -= m1 * s;
   r2[1] -= m2 * s;
   r3[1] -= m3 * s;
   s = r0[2];
   r1[2] -= m1 * s;
   r2[2] -= m2 * s;
   r3[2] -= m3 * s;
   s = r0[3];
   r1[3] -= m1 * s;
   r2[3] -= m2 * s;
   r3[3] -= m3 * s;
   s = r0[4];
   if (s != 0.0) {
      r1[4] -= m1 * s;
      r2[4] -= m2 * s;
      r3[4] -= m3 * s;
   }
   s = r0[5];
   if (s != 0.0) {
      r1[5] -= m1 * s;
      r2[5] -= m2 * s;
      r3[5] -= m3 * s;
   }
   s = r0[6];
   if (s != 0.0) {
      r1[6] -= m1 * s;
      r2[6] -= m2 * s;
      r3[6] -= m3 * s;
   }
   s = r0[7];
   if (s != 0.0) {
      r1[7] -= m1 * s;
      r2[7] -= m2 * s;
      r3[7] -= m3 * s;
   }
   /* choose pivot - or die */
   if (fabsf(r3[1]) > fabsf(r2[1]))
      SWAP_ROWS_FLOAT(r3, r2);
   if (fabsf(r2[1]) > fabsf(r1[1]))
      SWAP_ROWS_FLOAT(r2, r1);
   if (0.0 == r1[1])
      return 0;
   /* eliminate second variable */
   m2 = r2[1] / r1[1];
   m3 = r3[1] / r1[1];
   r2[2] -= m2 * r1[2];
   r3[2] -= m3 * r1[2];
   r2[3] -= m2 * r1[3];
   r3[3] -= m3 * r1[3];
   s = r1[4];
   if (0.0 != s) {
      r2[4] -= m2 * s;
      r3[4] -= m3 * s;
   }
   s = r1[5];
   if (0.0 != s) {
      r2[5] -= m2 * s;
      r3[5] -= m3 * s;
   }
   s = r1[6];
   if (0.0 != s) {
      r2[6] -= m2 * s;
      r3[6] -= m3 * s;
   }
   s = r1[7];
   if (0.0 != s) {
      r2[7] -= m2 * s;
      r3[7] -= m3 * s;
   }
   /* choose pivot - or die */
   if (fabsf(r3[2]) > fabsf(r2[2]))
      SWAP_ROWS_FLOAT(r3, r2);
   if (0.0 == r2[2])
      return 0;
   /* eliminate third variable */
   m3 = r3[2] / r2[2];
   r3[3] -= m3 * r2[3], r3[4] -= m3 * r2[4],
      r3[5] -= m3 * r2[5], r3[6] -= m3 * r2[6], r3[7] -= m3 * r2[7];
   /* last check */
   if (0.0 == r3[3])
      return 0;
   s = 1.0 / r3[3];		/* now back substitute row 3 */
   r3[4] *= s;
   r3[5] *= s;
   r3[6] *= s;
   r3[7] *= s;
   m2 = r2[3];			/* now back substitute row 2 */
   s = 1.0 / r2[2];
   r2[4] = s * (r2[4] - r3[4] * m2), r2[5] = s * (r2[5] - r3[5] * m2),
      r2[6] = s * (r2[6] - r3[6] * m2), r2[7] = s * (r2[7] - r3[7] * m2);
   m1 = r1[3];
   r1[4] -= r3[4] * m1, r1[5] -= r3[5] * m1,
      r1[6] -= r3[6] * m1, r1[7] -= r3[7] * m1;
   m0 = r0[3];
   r0[4] -= r3[4] * m0, r0[5] -= r3[5] * m0,
      r0[6] -= r3[6] * m0, r0[7] -= r3[7] * m0;
   m1 = r1[2];			/* now back substitute row 1 */
   s = 1.0 / r1[1];
   r1[4] = s * (r1[4] - r2[4] * m1), r1[5] = s * (r1[5] - r2[5] * m1),
      r1[6] = s * (r1[6] - r2[6] * m1), r1[7] = s * (r1[7] - r2[7] * m1);
   m0 = r0[2];
   r0[4] -= r2[4] * m0, r0[5] -= r2[5] * m0,
      r0[6] -= r2[6] * m0, r0[7] -= r2[7] * m0;
   m0 = r0[1];			/* now back substitute row 0 */
   s = 1.0 / r0[0];
   r0[4] = s * (r0[4] - r1[4] * m0), r0[5] = s * (r0[5] - r1[5] * m0),
      r0[6] = s * (r0[6] - r1[6] * m0), r0[7] = s * (r0[7] - r1[7] * m0);
   MAT(out, 0, 0) = r0[4];
   MAT(out, 0, 1) = r0[5], MAT(out, 0, 2) = r0[6];
   MAT(out, 0, 3) = r0[7], MAT(out, 1, 0) = r1[4];
   MAT(out, 1, 1) = r1[5], MAT(out, 1, 2) = r1[6];
   MAT(out, 1, 3) = r1[7], MAT(out, 2, 0) = r2[4];
   MAT(out, 2, 1) = r2[5], MAT(out, 2, 2) = r2[6];
   MAT(out, 2, 3) = r2[7], MAT(out, 3, 0) = r3[4];
   MAT(out, 3, 1) = r3[5], MAT(out, 3, 2) = r3[6];
   MAT(out, 3, 3) = r3[7];
   return 1;
  }


