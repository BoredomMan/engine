//--Base
#include "DebugManager.h"

//--Classes
//--Definitions
#include "DeletionFunctions.h"
#include "Global.h"

//--Generics
#include "SugarLinkedList.h"

//--GUI
//--Libraries
//--Managers
#include "OptionsManager.h"

//=========================================== System ==============================================
DebugManager::DebugManager()
{
}
DebugManager::~DebugManager()
{
}

//--Static Debug Flags. These can be modified at any time to change which debug flags print.
//  Note that some flags are disabled by defines and require recompilations (for speed reasons)
//  while others are not.
SugarLinkedList *DebugManager::xDebugFlagList = new SugarLinkedList(true);


bool DebugManager::xFullDisable = false;
SugarLinkedList *DebugManager::xSuppressList = new SugarLinkedList(true);
int DebugManager::xCurrentIndent = 0;

//====================================== Property Queries =========================================
//========================================= Manipulators ==========================================
//========================================= Core Methods ==========================================
//============================================ Update =============================================
//=========================================== File I/O ============================================
void DebugManager::Indent()
{
    //--Print spaces for every element on the list.
    for(int i = 0; i < DebugManager::xCurrentIndent; i ++) fprintf(stderr, " ");
}
void DebugManager::IndentSubOne()
{
    //--PushPrint and PopPrint must print with one space indentation down.  This is to make the
    //  appearance of a paragraph indentation.
    for(int i = 0; i < DebugManager::xCurrentIndent-1; i ++) fprintf(stderr, " ");
}
void DebugManager::Push(bool pFlag)
{
    //--Increments the indent counter by 1.
    if(DebugManager::xFullDisable) return;

    SetMemoryData(__FILE__, __LINE__);
    bool *nFlag = (bool *)starmemoryalloc(sizeof(bool));
    *nFlag = pFlag;
    DebugManager::xSuppressList->AddElementAsHead("X", nFlag, &FreeThis);

    //--Modify indentation
    if(pFlag) DebugManager::xCurrentIndent ++;
}
void DebugManager::Pop()
{
    //--Decrements the indent counter by 1.  Cannot go below 0.
    if(DebugManager::xFullDisable) return;

    //--Modify indentation
    bool *rFlag = (bool *)DebugManager::xSuppressList->GetElementBySlot(0);
    if(rFlag && *rFlag) DebugManager::xCurrentIndent --;

    //--Destroy the pointer
    DebugManager::xSuppressList->RemoveElementI(0);
}
void DebugManager::Print(const char *pString, ...)
{
    //--Prints the specified message with a number of spaces placed ahead of
    //  it.  This gives the appearance of indentation.
    //  Remember to \n the debug arg!
    if(DebugManager::xFullDisable) return;
    bool *rFlag = (bool *)DebugManager::xSuppressList->GetElementBySlot(0);
    if(!rFlag || !(*rFlag)) return;

    DebugManager::Indent();

    va_list tArgList;
    va_start(tArgList, pString);
    vfprintf(stderr, pString, tArgList);
    va_end(tArgList);
}
void DebugManager::PushPrint(const char *pVarName, const char *pString, ...)
{
    //--Resolves the pVarName from the OptionsManager and uses that argument.  Because the DM can
    //  exist before the OM is created, we check the OM for NULL first.
    if(DebugManager::xFullDisable) return;
    if(!pVarName || !pString) return;
    if(!Global::Shared()->gOptionsManager) return;

    //--Passing "TRUE" always executes.
    if(!strcmp(pVarName, "TRUE"))
    {
        DebugManager::Push(true);
    }
    //--Query the variable.
    else
    {
        bool tFlag = OptionsManager::Fetch()->GetOptionB(pVarName);
        DebugManager::Push(tFlag);
    }

    //--Print it
    bool *rFlag = (bool *)DebugManager::xSuppressList->GetElementBySlot(0);
    if(!rFlag || !(*rFlag)) return;

    DebugManager::IndentSubOne();

    va_list tArgList;
    va_start(tArgList, pString);
    vfprintf(stderr, pString, tArgList);
    va_end(tArgList);
}
void DebugManager::PushPrint(bool pFlag, const char *pString, ...)
{
    //--Macro, combines Print and Push.  Note the push occurs before the print.
    if(DebugManager::xFullDisable) return;
    DebugManager::Push(pFlag);
    bool *rFlag = (bool *)DebugManager::xSuppressList->GetElementBySlot(0);
    if(!rFlag || !(*rFlag)) return;

    DebugManager::IndentSubOne();

    va_list tArgList;
    va_start(tArgList, pString);
    vfprintf(stderr, pString, tArgList);
    va_end(tArgList);
}
void DebugManager::PopPrint(const char *pString, ...)
{
    //--Macro, combines Print and Pop.  Note the Pop occurs after the print.
    if(DebugManager::xFullDisable) return;

    //--If the flag was false, we pop and don't print.
    bool *rFlag = (bool *)DebugManager::xSuppressList->GetElementBySlot(0);
    if(!rFlag || !(*rFlag))
    {
        DebugManager::Pop();
        return;
    }

    //--Print it.
    DebugManager::IndentSubOne();

    va_list tArgList;
    va_start(tArgList, pString);
    vfprintf(stderr, pString, tArgList);
    va_end(tArgList);

    //--Destroy the flag.
    DebugManager::Pop();
}
void DebugManager::ForcePrint(const char *pString, ...)
{
    //--Regardless of the flag state, print the message with indentation.
    DebugManager::Indent();

    va_list tArgList;
    va_start(tArgList, pString);
    vfprintf(stderr, pString, tArgList);
    va_end(tArgList);
}

//=========================================== Drawing =============================================
//======================================= Pointer Routing =========================================
//====================================== Static Functions =========================================
void DebugManager::RegisterDebugFlag(const char *pName, bool pFlag)
{
    //--Registers a new debug flag. If it already exists, just changes it.
    if(!pName) return;

    //--Check existing flag.
    bool *rCheckFlag = (bool *)xDebugFlagList->GetElementByName(pName);
    if(rCheckFlag)
    {
        *rCheckFlag = pFlag;
        return;
    }

    //--New flag.
    bool *nFlag = (bool *)starmemoryalloc(sizeof(bool));
    *nFlag = pFlag;
    xDebugFlagList->AddElement(pName, nFlag, &FreeThis);
}
void DebugManager::SetDebugFlag(const char *pName, bool pFlag)
{
    //--Set the debug flag if it exists, do nothing otherwise.
    bool *rCheckFlag = (bool *)xDebugFlagList->GetElementByName(pName);
    if(rCheckFlag)
    {
        *rCheckFlag = pFlag;
        return;
    }
}
bool DebugManager::GetDebugFlag(const char *pName)
{
    //--Returns the named flag, or false on error. No warnings are printed.
    bool *rCheckFlag = (bool *)xDebugFlagList->GetElementByName(pName);
    if(rCheckFlag)
    {
        return *rCheckFlag;
    }

    //--Not found.
    return false;
}

//========================================= Lua Hooking ===========================================
void DebugManager::HookToLuaState(lua_State *pLuaState)
{
    //--[Versioning]
    /* Debug_SetVersion(sString)
       Sets the debug string that prints on top of the main menu. */
    lua_register(pLuaState, "Debug_SetVersion", &Hook_Debug_SetVersion);

    //--[Printing]
    /* Debug_PushPrint(bFlag, sString)
       Pushes the debug stack and prints the given message if the flag is true. */
    lua_register(pLuaState, "Debug_PushPrint", &Hook_Debug_PushPrint);

    /* Debug_PushPrintS(sFlag, sString)
       Pushes the debug stack and prints the given message if the OptionsManager named flag is
       true. Pass "TRUE" to always print. */
    lua_register(pLuaState, "Debug_PushPrintS", &Hook_Debug_PushPrintS);

    /* Debug_PopPrint(sString)
       Pops the debug stack and prints the message if the stack was active. */
    lua_register(pLuaState, "Debug_PopPrint", &Hook_Debug_PopPrint);

    /* Debug_Print(sString)
       Prints the given message if the debug stack is active. */
    lua_register(pLuaState, "Debug_Print", &Hook_Debug_Print);

    /* Debug_ForcePrint(sString)
       Prints the given message, ignoring the debug stack. */
    lua_register(pLuaState, "Debug_ForcePrint", &Hook_Debug_ForcePrint);

    //--[System]
    /* Debug_DropEvents()
       Causes the gear handler to drop all pending events, usually called after loading something
       which may build up timer/input events we don't want to handle. */
    lua_register(pLuaState, "Debug_DropEvents", &Hook_Debug_DropEvents);

    //--[Flag Management]
    /* Debug_RegisterFlag(sName, bValue)
       Creates a new debug flag. This flag can be queried later. A list of internally existing flags
       can be found in Startup.cc after the managers initialize. */
    lua_register(pLuaState, "Debug_RegisterFlag", &Hook_Debug_RegisterFlag);

    /* Debug_GetFlag(sName) (1 Boolean)
       Returns the named flag, or false if the flag is not found. */
    lua_register(pLuaState, "Debug_GetFlag", &Hook_Debug_GetFlag);

    /* Debug_SetFlag(sName, bValue)
       Sets the named flag. Does nothing if it does not exist. */
    lua_register(pLuaState, "Debug_SetFlag", &Hook_Debug_SetFlag);
}

//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
int Hook_Debug_SetVersion(lua_State *L)
{
    //Debug_SetVersion(sString)
    if(lua_gettop(L) < 1) return 0;
    strcpy(Global::Shared()->gVersionString, lua_tostring(L, 1));
    return 0;
}
int Hook_Debug_PushPrint(lua_State *L)
{
    //Debug_PushPrint(xFlag, sString)
    if(lua_gettop(L) != 2) return 0;

    DebugManager::PushPrint(lua_toboolean(L, 1), lua_tostring(L, 2));

    return 0;
}
int Hook_Debug_PushPrintS(lua_State *L)
{
    //Debug_PushPrintS(sVariableName, sString)
    if(lua_gettop(L) != 2) return 0;

    DebugManager::PushPrint(lua_tostring(L, 1), lua_tostring(L, 2));

    return 0;
}
int Hook_Debug_PopPrint(lua_State *L)
{
    //Debug_PopPrint(sString)
    if(lua_gettop(L) != 1) return 0;

    DebugManager::PopPrint(lua_tostring(L, 1));

    return 0;
}
int Hook_Debug_Print(lua_State *L)
{
    //Debug_Print(sString)
    if(lua_gettop(L) != 1) return 0;

    DebugManager::Print(lua_tostring(L, 1));

    return 0;
}
int Hook_Debug_ForcePrint(lua_State *L)
{
    //Debug_ForcePrint(sString)
    if(lua_gettop(L) != 1) return 0;

    DebugManager::ForcePrint(lua_tostring(L, 1));

    return 0;
}
    #if defined _ALLEGRO_PROJECT_
#include <allegro5/allegro.h>
#endif
int Hook_Debug_DropEvents(lua_State *L)
{
    //Debug_DropEvents()

    //--[Allegro Version]
    #if defined _ALLEGRO_PROJECT_

        //--Setup.
        GLOBAL *rGlobal = Global::Shared();
        SugarLinkedList *tEventList = new SugarLinkedList(true);

        //--Iterate across the event listing.
        while(true)
        {
            //--If the next event is a timer event, drop it:
            if(al_get_next_event(rGlobal->gEventQueue, &rGlobal->gEvent))
            {
                //--Timer event. Drop it by doing nothing.
                if(rGlobal->gEvent.type == ALLEGRO_EVENT_TIMER)
                {
                }
                //--Non-timer event. Re-emit it onto the end of the queue.
                else
                {
                    SetMemoryData(__FILE__, __LINE__);
                    ALLEGRO_EVENT *nCopyEvent = (ALLEGRO_EVENT *)starmemoryalloc(sizeof(ALLEGRO_EVENT));
                    memcpy(nCopyEvent, &rGlobal->gEvent, sizeof(ALLEGRO_EVENT));
                    tEventList->AddElementAsTail("X", nCopyEvent, &FreeThis);
                }
            }
            //--Queue is empty:
            else
            {
                break;
            }
        }

        //--Re-emit any events.
        ALLEGRO_EVENT *rEvent = (ALLEGRO_EVENT *)tEventList->PushIterator();
        while(rEvent)
        {
            al_emit_user_event(&rGlobal->gCustomEventSource, rEvent, NULL);
            rEvent = (ALLEGRO_EVENT *)tEventList->AutoIterate();
        }

        //--Clean up.
        delete tEventList;

    //--[SDL Version]
    #elif defined _SDL_PROJECT_
        SDL_FlushEvent(SDL_USEREVENT);
    #endif

    return 0;
}

///--[Flag Management]
int Hook_Debug_RegisterFlag(lua_State *L)
{
    //Debug_RegisterFlag(sName, bValue)
    int tArgs = lua_gettop(L);
    if(tArgs != 2) return LuaArgError("Debug_RegisterFlag");
    DebugManager::RegisterDebugFlag(lua_tostring(L, 1), lua_toboolean(L, 2));
    return 0;
}
int Hook_Debug_GetFlag(lua_State *L)
{
    //Debug_GetFlag(sName) (1 Boolean)
    int tArgs = lua_gettop(L);
    if(tArgs != 1) { LuaArgError("Debug_GetFlag"); lua_pushboolean(L, false); return 1; }
    DebugManager::GetDebugFlag(lua_tostring(L, 1));
    return 1;
}
int Hook_Debug_SetFlag(lua_State *L)
{
    //Debug_SetFlag(sName, bValue)
    int tArgs = lua_gettop(L);
    if(tArgs != 2) return LuaArgError("Debug_SetFlag");
    DebugManager::SetDebugFlag(lua_tostring(L, 1), lua_toboolean(L, 2));
    return 0;
}
