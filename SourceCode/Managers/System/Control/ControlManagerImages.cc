//--Base
#include "ControlManager.h"

//--Classes
//--CoreClasses
#include "SugarBitmap.h"
#include "SugarLinkedList.h"

//--Definitions
//--Libraries
#include "DataLibrary.h"

//--Managers

void ControlManager::AllocateImages(int pImages)
{
    //--Deallocate.
    free(mControlImagePacks);

    //--Reset.
    mControlImagePacksTotal = 0;
    mControlImagePacks = NULL;
    if(pImages < 1) return;

    //--Allocate.
    mControlImagePacksTotal = pImages;
    SetMemoryData(__FILE__, __LINE__);
    mControlImagePacks = (ControlImagePack *)starmemoryalloc(sizeof(ControlImagePack) * mControlImagePacksTotal);
    for(int i = 0; i < mControlImagePacksTotal; i ++) mControlImagePacks[i].Initialize();
}
void ControlManager::SetErrorImage(const char *pDLPath)
{
    rErrorImage = (SugarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath);
    fprintf(stderr, "Set error image %s %p\n", pDLPath, rErrorImage);
}
void ControlManager::SetImage(int pSlot, int pKeyCode, int pMouseCode, int pJoyCode, const char *pDLPath)
{
    if(pSlot < 0 || pSlot >= mControlImagePacksTotal) return;
    mControlImagePacks[pSlot].mKeyCode = pKeyCode;
    mControlImagePacks[pSlot].mMouseCode = pMouseCode;
    mControlImagePacks[pSlot].mJoyCode = pJoyCode;
    mControlImagePacks[pSlot].rImage = (SugarBitmap *)DataLibrary::Fetch()->GetEntry(pDLPath);
    fprintf(stderr, "Set %i %i control image %s %p\n", pSlot, pKeyCode, pDLPath, mControlImagePacks[pSlot].rImage);
}
SugarBitmap *ControlManager::ResolveControlImage(const char *pControlName)
{
    //--Find the control that matches the name.
    ControlState *rControlState = (ControlState *)mControlList->GetElementByName(pControlName);
    if(!rControlState) return rErrorImage;

    //--Control exists, now scan the images packs for a match.
    for(int i = 0; i < mControlImagePacksTotal; i ++)
    {
        //--Pass if the control isn't identical.
        if(mControlImagePacks[i].mKeyCode   != rControlState->mWatchKeyPri)      continue;
        if(mControlImagePacks[i].mMouseCode != rControlState->mWatchMouseBtnPri) continue;
        if(mControlImagePacks[i].mJoyCode   != rControlState->mWatchJoyPri)      continue;

        //--Match!
        return mControlImagePacks[i].rImage;
    }

    //--Control didn't find a match.
    return rErrorImage;
}
