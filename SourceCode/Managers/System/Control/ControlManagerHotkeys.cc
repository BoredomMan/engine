//--Base
#include "ControlManager.h"

//--Classes
//--CoreClasses
//--Definitions
//--GUI
//--Libraries
//--Managers
#include "DebugManager.h"
#include "DisplayManager.h"

void ControlManager::CheckHotkeys()
{
    //--These are system hotkeys, for things like killing the audio or swapping fullscreen. Most game
    //  hotkeys are handled in UI/World objects.
    if(IsFirstPress("ToggleFullscreen"))
    {
        //--Switch on/off fullscreen.
        DisplayManager::Fetch()->FlipFullscreen();

        //--Wipe all input, since the game will probably have to pause here.
        BlankControls();
    }
}
