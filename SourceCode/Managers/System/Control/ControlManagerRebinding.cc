//--Base
#include "ControlManager.h"

//--Classes
//--CoreClasses
#include "SugarLinkedList.h"

//--Definitions
#include "DeletionFunctions.h"

//--GUI
//--Libraries
//--Managers
#include "DebugManager.h"

void ControlManager::OverbindControl(const char *pName, bool pIsSecondary, int pKeyboard, int pMouse, int pJoystick)
{
    //--Direct binding, only use this if the exact keycode (which may be driver-dependent!) is known.
    //  Manually changes what code the control is watching.
    //--Passing -1 will unbind the control.
    if(!pName) return;

    //--Control resolve.
    ControlState *rControl = (ControlState *)mControlList->GetElementByName(pName);
    if(!rControl) return;

    //--Bind is primary or secondary based on a flag.
    if(!pIsSecondary)
    {
        rControl->mWatchKeyPri = pKeyboard;
        rControl->mWatchMouseBtnPri = pMouse;
        rControl->mWatchJoyPri = pJoystick;
    }
    else
    {
        rControl->mWatchKeySec = pKeyboard;
        rControl->mWatchMouseBtnSec = pMouse;
        rControl->mWatchJoySec = pJoystick;
    }

    //--Flag resets.
    rControl->mIsFirstPress = false;
    rControl->mIsFirstRelease = false;
    rControl->mIsDown = false;
    rControl->mTicksSincePress = -1;
    rControl->mHasPendingRelease = false;
}
void ControlManager::RebindControl(const char *pName, const char *pPriCode, const char *pSecCode)
{
    //--Resolves the control requested, and then sets its watch values as appropriate.  Note that a
    //  control can watch multiple devices (mouse/keyboard/joystick) so the codes are given as strings.
    //--NULL is valid, it unbinds that code.  The name cannot be NULL.
    if(!pName) return;

    //--If currently in profile mode, does not actually edit the control. Instead, takes the active
    //  profile and registers the strings to it.
    if(mIsInProfileMode)
    {
        //--Check!
        if(!rLastReggedProfile)
        {
            fprintf(stderr, "[ControlManager] Error storing control, no active profile\n");
            return;
        }

        //--Copy and reg the three strings.
        char *nStringA = NULL;
        char *nStringB = NULL;
        char *nStringC = NULL;
        ResetString(nStringA, pName);
        ResetString(nStringB, pPriCode);
        ResetString(nStringC, pSecCode);
        rLastReggedProfile->AddElement("X", nStringA, &FreeThis);
        rLastReggedProfile->AddElement("X", nStringB, &FreeThis);
        rLastReggedProfile->AddElement("X", nStringC, &FreeThis);
        return;
    }
    //fprintf(stderr, "Rebinding %s %s %s\n", pName, pPriCode, pSecCode);

    //--Control resolve.
    ControlState *rControl = (ControlState *)mControlList->GetElementByName(pName);
    if(!rControl) return;

    //--Worker does the rest.
    ResolveCode(pPriCode, rControl->mWatchKeyPri, rControl->mWatchMouseBtnPri, rControl->mWatchJoyPri);
    ResolveCode(pSecCode, rControl->mWatchKeySec, rControl->mWatchMouseBtnSec, rControl->mWatchJoySec);

    rControl->mIsFirstPress = false;
    rControl->mIsFirstRelease = false;
    rControl->mIsDown = false;
    rControl->mTicksSincePress = -1;
    rControl->mHasPendingRelease = false;
}
void ControlManager::ResolveCode(const char *pCode, int &sKeyboard, int &sMouse, int &sJoystick)
{
    //--Worker function, does the actual job of resolving the control's name and places the value
    //  resolved in the three provided integers.  Does NOT check if that is a valid control.
    //  (EG: the joystick has enough sticks/axes).
    //--The values are set to -1. If NULL is passed, or no value is resolved, the key is neutralized.
    sKeyboard = -1;
    sMouse = -1;
    sJoystick = -1;
    if(!pCode) return;

    //--If the user deliberately passes "NULL" then stop here.
    if(!strcmp(pCode, "NULL")) return;

    //--Code is a keyboard key, begins with "KEY"
    if(!strncmp(pCode, "KEY_", 4))
    {
        //--Remove the "KEY_" and begin the search. If there is an "_ARROW" trailing then remove that
        //  as well, since Allegro consideres "LEFT" to be adequate.
        char tBuffer[32];
        strcpy(tBuffer, &pCode[4]);

        //--Check for the _ARROW part.
        if(strlen(tBuffer) >= 7)
        {
            for(uint32_t i = 0; i < strlen(tBuffer) - 5; i ++)
            {
                if(!strncmp(&tBuffer[i], "_ARROW", 6))
                {
                    tBuffer[i] = '\0';
                    break;
                }
            }
        }

        //--Search the list of keyboard names and rebind.
        for(int i = 0; i < mKeyboardNamesTotal; i ++)
        {
            if(!strcmp(tBuffer, mKeyboardNames[i]))
            {
                sKeyboard = i;
                break;
            }
        }
    }
    //--Code is a mouse button, begins with "MOS"
    else if(!strncmp(pCode, "MOS", 3))
    {

    }
    //--Code is a joystick button or axis, begins with "JOY"
    else if(!strncmp(pCode, "JOY", 3))
    {
        //--Next, determine if it's a button "JOYBTN"
        uint32_t tLength = strlen(pCode);
        if(!strncmp(pCode, "JOYBTN", 6) && tLength > 6)
        {
            //--Resolve the joystick number (it cannot exceed 99)
            if(tLength == 7)
            {
                sJoystick = pCode[6] - '0';
            }
            else
            {
                sJoystick =  (pCode[6] - '0') * 10;
                sJoystick += (pCode[7] - '0') *  1;
            }
            //DebugManager::ForcePrint("Set joystick button to %i\n", sJoystick);
        }
        //--Or, if it's a joy stick axis "JOYSTK"
        else if(!strncmp(pCode, "JOYSTK", 6) && tLength >= 12)
        {
            //--The format for these is "JOYSTK_X_Y_Z" where X is the stick number, Y is the axis
            //  number, and Z is 'P' or 'N' for positive or negative on that axis.
            //--Once again, the existence of those axis is not checked until the control is actually
            //  queried by the ControlManager.
            sJoystick = JOYSTICK_OFFSET_STICK;
            sJoystick += ((pCode[7] - '0') * JOYSTICK_STICK_CONSTANT);
            sJoystick += ((pCode[9] - '0') * JOYSTICK_AXIS_CONSTANT);
            if(pCode[11] == 'P')
            {
                sJoystick ++;
                DebugManager::ForcePrint("Successfully bound %s to %i\n", pCode, sJoystick);
            }
            else if(pCode[11] == 'N')
            {
                DebugManager::ForcePrint("Successfully bound %s to %i\n", pCode, sJoystick);
            }
            else
            {
                DebugManager::ForcePrint("Warning, type %s should be P or N, assuming N for now.\n", pCode);
            }
        }
        //--Something else?  Failed!
        else
        {
            DebugManager::ForcePrint("Warning, type %s is not a valid Joystick type.\n", pCode);
        }
    }
    //--Failure case.
    else
    {
        DebugManager::ForcePrint("Warning, could not resolve control type %s\n", pCode);
    }
}
