//--Base
#include "CameraManager.h"

//--Classes
//--CoreClasses
#include "SugarCamera.h"
#include "SugarCamera2D.h"
#include "SugarCamera3D.h"

//--Definitions
#include "Global.h"

//--Generics
#include "SugarLinkedList.h"

//--GUI
//--Libraries
//--Managers

//=========================================== System ==============================================
CameraManager::CameraManager()
{
    //--[CameraManager]
    //--System
    //--Storage
    mCameraList = new SugarLinkedList(true);

    //--Public Variables
    mLastSwitchFailed = false;
    rActiveCamera2D = NULL;
    rActiveCamera3D = NULL;

    //--[Construction]
    //--Default 2D Camera
    rActiveCamera2D = new SugarCamera2D();
    mCameraList->AddElement("Default2D", rActiveCamera2D, &SugarCamera::DeleteThis);

    //--Default 3D Camera
    rActiveCamera3D = new SugarCamera3D();
    mCameraList->AddElement("Default3D", rActiveCamera3D, &SugarCamera::DeleteThis);
}
CameraManager::~CameraManager()
{
    delete mCameraList;
}

//====================================== Property Queries =========================================
//========================================= Manipulators ==========================================
void CameraManager::CreateCamera(const char *pName)
{
    //--Macro.  Creates a Camera then adds it to the list. In this program, this is always a 2D
    //  camera, because there are no 3D sections.
    if(!pName) return;
    SugarCamera2D *nCamera = new SugarCamera2D();
    mCameraList->AddElement(pName, nCamera, &SugarCamera::DeleteThis);
}
void CameraManager::RegisterCamera(const char *pName, SugarCamera *pCamera)
{
    //--Registers a Camera that has already been created.
    if(!pName || !pCamera) return;
    mCameraList->AddElement(pName, pCamera, &SugarCamera::DeleteThis);
}
void CameraManager::SwitchCamera(const char *pName)
{
    //--Changes which Camera is the Active one.  If the Camera isn't found, the ActiveCamera will
    //  not be changed.
    if(!pName) return;

    SugarCamera *rCandidate = (SugarCamera *)mCameraList->GetElementByName(pName);
    if(rCandidate && rCandidate->Is2DCamera()) rActiveCamera2D = (SugarCamera2D *)rCandidate;
    if(rCandidate && rCandidate->Is3DCamera()) rActiveCamera3D = (SugarCamera3D *)rCandidate;

    //--This flag reports if the Switch failed to anything that asks.
    mLastSwitchFailed = (rCandidate == NULL);
}
void CameraManager::RemoveCamera(const char *pName)
{
    //--Remove a Camera from the list, for whatever reason.  Note that this *WILL* delete the
    //  Camera. Also, if this was the ActiveCamera, NULL that so PreserveCameraActivity will handle it.
    if(!pName) return;

    //--Locate the camera. It can be of either type.
    SugarCamera *rCameraCandidate = (SugarCamera *)mCameraList->GetElementByName(pName);
    if(!rCameraCandidate) return;

    //--Removal.
    if(rCameraCandidate == rActiveCamera2D) rActiveCamera2D = NULL;
    if(rCameraCandidate == rActiveCamera3D) rActiveCamera3D = NULL;
    mCameraList->RemoveElementS(pName);

    //--If one of the active cameras got removed, a new camera will be found/created.
    PreserveCameraActivity();
}
void CameraManager::ClearList()
{
    //--Remove all Cameras on the list and return the CM to default mode.
    mCameraList->ClearList();
    rActiveCamera2D = NULL;
    rActiveCamera3D = NULL;
    PreserveCameraActivity();
}

//========================================= Core Methods ==========================================
void CameraManager::PreserveCameraActivity()
{
    //--Macro that is run each time the CameraManager has the potential to have an empty list. This
    //  is intolerable, there must always be at least one active Camera. This function will create
    //  a Camera and set it as the active one should the list ever be emptied.
    //--An active camera of each type will always exist, so we check both the 2D and 3D cases.

    //--Check for a Default2D case. If it doesn't exist, make one.
    SugarCamera2D *rDefault2D = (SugarCamera2D *)mCameraList->GetElementByName("Default2D");
    if(!rDefault2D)
    {
        rActiveCamera2D = new SugarCamera2D();
        mCameraList->AddElement("Default2D", rActiveCamera2D, &SugarCamera::DeleteThis);
    }

    //--Check for a Default3D case. Make it.
    SugarCamera3D *rDefault3D = (SugarCamera3D *)mCameraList->GetElementByName("Default3D");
    if(!rDefault3D)
    {
        rActiveCamera3D = new SugarCamera3D();
        mCameraList->AddElement("Default3D", rActiveCamera3D, &SugarCamera::DeleteThis);
    }
}
void CameraManager::Reset(const char *pResetType)
{
    //--All camera resets just clear the list and reset it back to default.
    ClearList();
}

//===================================== Private Core Methods ======================================
//============================================ Update =============================================
void CameraManager::Update()
{
    //--Order the active camera to update itself.  Only the active one!
    if(rActiveCamera2D) rActiveCamera2D->Update();
    if(rActiveCamera3D) rActiveCamera3D->Update();
}
void CameraManager::UpdatePaused(uint8_t pPauseFlags)
{
    //--By default, a pause CameraManager does nothing.
}

//=========================================== File I/O ============================================
//=========================================== Drawing =============================================
//======================================= Pointer Routing =========================================
SugarCamera *CameraManager::GetCamera(int pSlot)
{
    return (SugarCamera *)mCameraList->GetElementBySlot(pSlot);
}
SugarCamera *CameraManager::GetCamera(const char *pName)
{
    return (SugarCamera *)mCameraList->GetElementByName(pName);
}

//====================================== Static Functions =========================================
CameraManager *CameraManager::Fetch()
{
    return Global::Shared()->gCameraManager;
}
SugarCamera2D *CameraManager::FetchActiveCamera2D()
{
    return Global::Shared()->gCameraManager->rActiveCamera2D;
}
SugarCamera3D *CameraManager::FetchActiveCamera3D()
{
    return Global::Shared()->gCameraManager->rActiveCamera3D;
}

//========================================= Lua Hooking ===========================================
void CameraManager::HookToLuaState(lua_State *pLuaState)
{
    /* CM_AddCamera(sName[])
       Adds a new camera with the given name. No setup or type information is known. */
    lua_register(pLuaState, "CM_AddCamera", &Hook_CM_AddCamera);

    /* CM_RemoveCamera(sName[])
       Deletes the requested camera. */
    lua_register(pLuaState, "CM_RemoveCamera", &Hook_CM_RemoveCamera);

    /* CM_SetActiveCamera(sName[])
       Changes which camera is the active one, if it exists. */
    lua_register(pLuaState, "CM_SetActiveCamera", &Hook_CM_SetActiveCamera);
}

//=================================================================================================
//                                       Hooking Functions                                       ==
//=================================================================================================
int Hook_CM_AddCamera(lua_State *L)
{
    //CM_AddCamera(Name[])
    if(lua_gettop(L) != 1) return LuaArgError("CM_AddCamera");

    CameraManager *rCameraManager = CameraManager::Fetch();
    rCameraManager->CreateCamera(lua_tostring(L, 1));

    return 0;
}
int Hook_CM_RemoveCamera(lua_State *L)
{
    //CM_RemoveCamera(Name[])
    if(lua_gettop(L) != 1) return LuaArgError("CM_RemoveCamera");

    CameraManager *rCameraManager = CameraManager::Fetch();
    rCameraManager->RemoveCamera(lua_tostring(L, 1));

    return 0;
}
int Hook_CM_SetActiveCamera(lua_State *L)
{
    //CM_SetActiveCamera(Name[])
    if(lua_gettop(L) != 1) return LuaArgError("CM_SetActiveCamera");

    CameraManager *rCameraManager = CameraManager::Fetch();
    rCameraManager->SwitchCamera(lua_tostring(L, 1));

    return 0;
}
