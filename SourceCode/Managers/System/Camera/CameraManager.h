//--[CameraManager]
//--Contains all the Cameras currently in use by the game, and functions to manipulate them.

#pragma once

#include "Definitions.h"
#include "Structures.h"
#include "SugarCamera2D.h"
#include "SugarCamera3D.h"
#include "IResettable.h"

class CameraManager : public IResettable
{
    private:
    //--System

    //--Storage
    SugarLinkedList *mCameraList;
    SugarCamera2D *rActiveCamera2D;
    SugarCamera3D *rActiveCamera3D;

    protected:

    public:
    //--System
    CameraManager();
    ~CameraManager();

    //--Public Variables
    bool mLastSwitchFailed;

    //--Property Queries
    //--Manipulators
    void CreateCamera(const char *pName);
    void RegisterCamera(const char *pName, SugarCamera *pCamera);
    void SwitchCamera(const char *pName);
    void RemoveCamera(const char *pName);
    void ClearList();

    //--Core Methods
    void PreserveCameraActivity();
    virtual void Reset(const char *pResetType);

    //--Update
    void Update();
    void UpdatePaused(uint8_t pPauseFlags);

    //--File I/O
    //--Drawing
    //--Pointer Routing
    SugarCamera *GetCamera(int pSlot);
    SugarCamera *GetCamera(const char *pName);

    //--Static Functions
    static CameraManager *Fetch();
    static SugarCamera2D *FetchActiveCamera2D();
    static SugarCamera3D *FetchActiveCamera3D();

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_CM_AddCamera(lua_State *L);
int Hook_CM_RemoveCamera(lua_State *L);
int Hook_CM_SetActiveCamera(lua_State *L);

