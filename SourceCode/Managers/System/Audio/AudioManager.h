//--[AudioManager]
//--Manages audio packages, both music and sound, as well as global controls like volume. If you
//  can hear it, this is your manager!
//--Note: The blocking flags for Music and Sound can be flipped on and off. The AM still registers
//  the samples/streams. The xBlockAllAudio flag should only be flipped by the command line.

#pragma once

#include "Definitions.h"
#include "Structures.h"

#if defined _FMOD_AUDIO_
    #include "fmod.hpp"
#endif

//--Local Definitions
#define AM_DEFAULT_MUSIC_VOLUME 0.50f
#define AM_DEFAULT_SOUND_VOLUME 0.650001f

//--Local Structures
typedef struct
{
    uint64_t mStartInBytes;
}AudioPackageLoopData;

class AudioManager
{
    private:
    //--System
    //--Storage Lists
    SugarLinkedList *mMusicList;
    SugarLinkedList *mSoundList;
    SugarLinkedList *mRandomSoundList;

    //--Music controllers
    AudioPackage *rCurrentMusic;

    //--Speech controllers
    AudioPackage *mCurrentSpeech;

    protected:

    public:
    //--System
    AudioManager();
    ~AudioManager();

    //--Public Variables
    AudioPackage *rLastPackage;
    static bool xBlockAllAudio;
    static bool xBlockSound;
    static bool xBlockMusic;
    static float xMusicVolume;
    static float xSoundVolume;
    static float xMasterVolume;

    //--FMOD. AudioPackages get these a lot.
    #if defined _FMOD_AUDIO_
        static FMOD_SYSTEM *xFMODSystem;
        //FMOD_SOUND *mSound, *mSoundToPlay;
        //FMOD_CHANNEL *rChannel;
        //FMOD_RESULT mFMODResult;
        static uint32_t xFMODVersion;
        static void *xExtraDriverData;
        //int mNumSubSounds;
    #endif

    //--Property Queries
    bool MusicExists(const char *pName);
    bool SoundExists(const char *pName);

    //--Manipulators
    void RegisterMusic(const char *pName, AudioPackage *pMusicPack);
    void RegisterSound(const char *pName, AudioPackage *pSoundPack);
    void RegisterRandomSound(const char *pName, int pMaxSounds);
    void ClearLastPackage();
    void ChangeMusicVolume(float pDelta);
    void ChangeSoundVolume(float pDelta);
    void ChangeMusicVolumeTo(float pValue);
    void ChangeSoundVolumeTo(float pValue);
    void SeekMusicTo(float pSeconds);
    void AdvanceMusicToLoopPoint();
    void ModifyLoopPointForward();
    void ModifyLoopPointBackward();

    //--Core Methods
    void PlayMusic(const char *pName);
    void PlayMusicStartingAt(const char *pName, float pStartPoint);
    void PlayMusicNoFade(const char *pName);
    void FadeMusic(int pTickDuration);
    void StopAllMusic();
    void PlaySound(const char *pName);
    void StopSound(const char *pName);
    void PlayRandomSound(const char *pName);
    void PlayRandomSound(const char *pName, int pMaxRoll);
    void StopAllSound();
    void PlaySpeech(const char *pPath);
    bool StopSpeech();

    void PauseSound(const char *pName);

    //--Special Audio functions
    #if defined _BASS_AUDIO_
    static void CALLBACK LoopCallback(HSYNC pHandle, DWORD pChannel, DWORD pData, void *pUserPtr);
    #endif

    //--Update
    void Update();

    //--File I/O
    //--Drawing
    //--Pointer Routing
    AudioPackage *GetPlayingMusic();
    AudioPackage *GetMusicPack(const char *pName);
    AudioPackage *GetSoundPack(const char *pName);
    AudioPackage *GetVoicePack();

    //--Static Functions
    static AudioManager *Fetch();

    //--Lua Hooking
    static void HookToLuaState(lua_State *pLuaState);
};

//--Hooking Functions
int Hook_AudioManager_Register(lua_State *L);
int Hook_AudioManager_PlayMusic(lua_State *L);
int Hook_AudioManager_PlaySound(lua_State *L);
int Hook_AudioManager_PlaySpeech(lua_State *L);
int Hook_AudioManager_StopSound(lua_State *L);
int Hook_AudioManager_StopMusic(lua_State *L);
int Hook_AudioManager_FadeMusic(lua_State *L);
int Hook_AudioManager_GetProperty(lua_State *L);
int Hook_AudioManager_SetProperty(lua_State *L);

