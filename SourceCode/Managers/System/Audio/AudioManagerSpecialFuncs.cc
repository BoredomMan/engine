//--Base
#include "AudioManager.h"

//--Classes
//--CoreClasses
//--Definitions
//--GUI
//--Libraries
//--Managers

#if defined _BASS_AUDIO_
void CALLBACK AudioManager::LoopCallback(HSYNC pHandle, DWORD pChannel, DWORD pData, void *pUserPtr)
{
    //--Function which resets the using stream to a specific point, when it has reached a specific
    //  point.  This is essentially looping.
    AudioPackageLoopData *rUserPtr = (AudioPackageLoopData *)pUserPtr;
    BASS_ChannelSetPosition(pChannel, rUserPtr->mStartInBytes, BASS_POS_BYTE);
}
#endif
