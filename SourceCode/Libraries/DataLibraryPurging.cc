//--Base
#include "DataLibrary.h"

//--Classes
//--Definitions
//--Generics
#include "SugarLinkedList.h"

//--GUI
//--Libraries
//--Managers

//--Functions for removing whole areas of the DataLibrary.

void *DataLibrary::Purge(const char *pDLPath)
{
    //--Overload of below. The block deallocation flag is implied to be false;
    return Purge(pDLPath, false);
}
void *DataLibrary::Purge(const char *pDLPath, bool pBlockDeallocation)
{
    //--Removes the last part of the pDLPath.  If that was a heading, then the heading is cleared
    //  off. If that was a catalogue, the catalogue is cleared off, etc.
    //--It is acceptable to remove Entries themselves as well as sorters. If a name is passed then
    //  the RemoveEntry function is called.
    //--Also removes the named part from what underlay it, so a cleared heading is removed from its
    //  catalogue.
    //--If the string "ALL" is passed, the ENTIRE DataLibrary is purged, and a new mSectionList is
    //  created. Drastic times, and all that.
    //--Whatever was removed is returned, though if that thing was deallocated, the resulting pointer
    //  will be unstable. If not, you're responsible for deallocating it.
    if(!strcmp(pDLPath, "ALL"))
    {
        if(pBlockDeallocation) mSectionList->SetDeallocation(false);
        delete mSectionList;
        mSectionList = new SugarLinkedList(true);
        return NULL;
    }

    //--Resolve everything.
    ResolvePack *tResolvePack = CreateResolvePack(pDLPath);

    //--Check for the special case of RemoveEntry.
    if(tResolvePack->rEntry)
    {
        free(tResolvePack);
        return RemovePointer(pDLPath);
    }

    //--Determine the target list, parent list. Because we checked for RemoveEntry we are
    //  guaranteed that everything here is a list.
    SugarLinkedList *rTargetList = NULL;
    SugarLinkedList *rParentList = NULL;

    //--Target was a heading.
    if(tResolvePack->rHeading)
    {
        rTargetList = tResolvePack->rHeading;
        rParentList = tResolvePack->rCatalogue;
    }
    //--Target was a catalogue.
    else if(tResolvePack->rCatalogue && tResolvePack->mHeading[0] == '\0')
    {
        rTargetList = tResolvePack->rCatalogue;
        rParentList = tResolvePack->rSection;
    }
    //--Target was a section
    else if(tResolvePack->rSection && tResolvePack->mCatalogue[0] == '\0')
    {
        rTargetList = tResolvePack->rSection;
        rParentList = mSectionList;
    }
    //--Failure
    else
    {
        free(tResolvePack);
        return NULL;
    }

    //--Destroy the targetted list. Lists always have their deallocation flags set to true by
    //  default, so flip it back on if we blocked it.
    if(pBlockDeallocation) rParentList->SetDeallocation(false);
    rParentList->RemoveElementP(rTargetList);
    if(pBlockDeallocation) rParentList->SetDeallocation(true);

    //--Clean off activity ratings, in case the active objects got destroyed.  The ActivityStack
    //  must be scanned for deleted entries.
    //--This may cause instability, so be careful when purging sections of the DL!
    //mActiveEntityStack->ClearList();
    //rActiveObject = NULL;

    //--Clean up
    free(tResolvePack);
    return NULL;
}
