//--Base
#include "DataLibrary.h"

//--Classes
//--Definitions
#include "DeletionFunctions.h"
#include "Global.h"

//--Generics
#include "SugarLinkedList.h"

//--GUI
//--Libraries
//--Managers
#include "DebugManager.h"
#include "LuaManager.h"

//--These are the lowest-level of DataLibrary functions.  Everything else refers to them through
//  overloads of various types.

//--Local Functions
void ScanInToSlash(const char *pResolveString, char *pString, int &sCursor, int pStringLen)
{
    //--Scans the pResolveString into pString, starting at pStartLoc, until a / is located or the
    //  end of the string is reached.
    //--If a NULL pString is passed, the cursor advances anyway but nothing is copied.
    //--A negative sCursor is set to 0 before starting.
    if(!pResolveString || pStringLen < 1) return;

    //--Reset
    if(sCursor < 0) sCursor = 0;

    //--Scan
    int tStringCursor = 0;
    while(pResolveString[sCursor] != '/' && sCursor < pStringLen)
    {
        if(pString)
        {
            pString[tStringCursor+0]  = pResolveString[sCursor];
            pString[tStringCursor+1] = '\0';
        }
        tStringCursor ++;
        sCursor ++;
    }

    //--Add one to the cursor to 'skip' the /.
    sCursor ++;
}

//--Error Printer
void DataLibrary::ErrorNotFound(const char *pCallerName, const char *pResolveString)
{
    //--Prints an error to the DebugManager, automatically checking error flags.
    if(!pCallerName || !pResolveString) return;
    if(!mErrorMode || mFailSilently) return;
    DebugManager::ForcePrint("[DataLibrary] %s:  Error, %s not found\n", pCallerName, pResolveString);
}

//--Resolving Functions
ResolvePack *DataLibrary::CreateResolvePack(const char *pResolveString)
{
    //--Overload, pSkipLists is assumed to be false, and a full pack is created.
    return CreateResolvePack(pResolveString, false);
}
ResolvePack *DataLibrary::CreateResolvePack(const char *pResolveString, bool pSkipLists)
{
    //--Taking in a properly formatted string, returns a new ResolvePack with the appropriate data
    //  filled in.  Any parts that were not found come back as NULL.  The function will return an
    //  allocated object even if it fails, and thus must be free()'d when done.
    //--If pSkipLists is true, the list pointers remain NULL.  This is for efficiency.
    SetMemoryData(__FILE__, __LINE__);
    ResolvePack *nPack = (ResolvePack *)starmemoryalloc(sizeof(ResolvePack));
    memset(nPack, 0, sizeof(ResolvePack));

    //--Pull the names out
    GetNames(pResolveString, *nPack);

    //--Get the lists out.
    if(!pSkipLists) GetLists(*nPack);

    return nPack;
}
void DataLibrary::GetNames(const char *pResolveString, ResolvePack &sResolvePack)
{
    //--Taking in a string in the format of Root/Section/Catalogue/Heading/Name, returns the four
    //  name pieces into the sPack.  If a piece is not found, that part of the pack is cleared.
    //--Returns false on error, else true.
    if(!pResolveString) return;

    //--Setup
    uint32_t tLength = strlen(pResolveString);
    int mCursor = -1;
    ScanInToSlash(pResolveString, NULL, mCursor, tLength);
    ScanInToSlash(pResolveString, &sResolvePack.mSection[0], mCursor, tLength);
    ScanInToSlash(pResolveString, &sResolvePack.mCatalogue[0], mCursor, tLength);
    ScanInToSlash(pResolveString, &sResolvePack.mHeading[0], mCursor, tLength);
    ScanInToSlash(pResolveString, &sResolvePack.mName[0], mCursor, tLength);
}
void DataLibrary::GetLists(ResolvePack &sResolvePack)
{
    //--Taking in a resolve pack, places the pointers to the objects specified onto the proper
    //  parts of the resolve pack, based on the names.  Blank names result in NULL pointers.

    //--Section
    if(sResolvePack.mSection[0] == '\0') return;
    sResolvePack.rSection = (SugarLinkedList *)mSectionList->GetElementByName(sResolvePack.mSection);
    if(!sResolvePack.rSection) return;

    //--Catalogue
    if(sResolvePack.mCatalogue[0] == '\0') return;
    sResolvePack.rCatalogue = (SugarLinkedList *)sResolvePack.rSection->GetElementByName(sResolvePack.mCatalogue);
    if(!sResolvePack.rCatalogue) return;

    //--Heading
    if(sResolvePack.mHeading[0] == '\0') return;
    sResolvePack.rHeading = (SugarLinkedList *)sResolvePack.rCatalogue->GetElementByName(sResolvePack.mHeading);
    if(!sResolvePack.rHeading) return;

    //--Name
    if(sResolvePack.mName[0] == '\0') return;
    sResolvePack.rEntry = sResolvePack.rHeading->GetElementByName(sResolvePack.mName);
}

//--ResolveString macros
char *DataLibrary::GetPathPart(const char *pResolveString, int pPartFlag)
{
    //--Returns an allocated string that represents the requested part, see the definitions in the
    //  DataLibrary's header.
    //--Returns NULL on failure.
    if(!pResolveString || pPartFlag < DL_SECTION || pPartFlag > DL_NAME) return NULL;

    //--Setup
    uint32_t tLength = strlen(pResolveString);
    int mCursor = -1;
    char tCheckString[256];
    //memset(&tCheckString, 0, sizeof(char) * 256);

    //--Run it
    for(int i = 0; i < pPartFlag+1; i ++)
    {
        ScanInToSlash(pResolveString, NULL, mCursor, tLength);
    }
    ScanInToSlash(pResolveString, &tCheckString[0], mCursor, tLength);

    //--If the 0th character was '\0' we assume the name failed and return NULL.  This is because
    //  the ScanInToSlash doesn't check for existence first.
    if(tCheckString[0] == '\0') return NULL;

    //--Allocate the string and return it.
    SetMemoryData(__FILE__, __LINE__);
    char *nString = (char *)starmemoryalloc(sizeof(char) * (strlen(tCheckString) + 1));
    strcpy(nString, tCheckString);
    return nString;
}
