--This lua file is the first thing that gets run once the LuaManager is constructed.  This is where you
-- should redirect it to Tarball mode, if you're using that.
LM_BootTarballMode("Data/Scripts.slf")
DM_SetShaderPath("Shaders/ZShaderExec.lua")