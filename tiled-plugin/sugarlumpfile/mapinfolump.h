//--[MapInfoLump]
//--Inherits from a RootLump. Indicates what type the map is. The current construction
//  only allows the layered-tiled type.

#pragma once

#define _CRT_SECURE_NO_WARNINGS

#include "RootLump.h"

#define MAP_2DLINEART 0
#define MAP_2DTWODEPTH 1
#define MAP_3DVOXEL 2
#define MAP_3DVECTOR 3
#define MAP_2DTILEDDEPTH 4

namespace Tiled {
class GidMapper;
class Map;
class TileLayer;
class Tileset;
}

typedef struct
{
    char *mTilesetName;
    char *mTilesetPath;
    int mTilesToThisPoint;
    int mTilesTotal;
}TilesetInfoPack;

class MapInfoLump : public RootLump
{
    private:
    //--System
    //--Type
    uint8_t mTypeFlag;

    public:
    //--System
    MapInfoLump();
    virtual ~MapInfoLump();

    //--Public Variables
    static int xTilesetsTotal;
    static TilesetInfoPack *xTilesetPacks;

    //--Property Queries
    //--Manipulators
    //--Core Methods
    void BootTilesetData(const Tiled::Map *pMap);

    //--Update
    //--File I/O
    virtual void WriteData(uint64_t &, FILE *);

    //--Drawing
    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking
};

