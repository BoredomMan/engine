/*
 * SLF Tiled Plugin
 * SaltyJustice 2015. Copyright him. Nobody is going to read this.
 */

#pragma once

#include <QtCore/qglobal.h>
#define _CRT_SECURE_NO_WARNINGS

#if defined(SLF_LIBRARY)
#  define SLFSHARED_EXPORT Q_DECL_EXPORT
#else
#  define SLFSHARED_EXPORT Q_DECL_IMPORT
#endif

