//--Base
#include "mapinfolump.h"

//--System
#include <stdio.h>
#include <stdint.h>

//--Classes
#include "tileset.h"
#include "gidmapper.h"
#include "map.h"
#include "qlist.h"

//--CoreClasses
//--Definitions
//--GUI
//--Libraries
//--Managers

MapInfoLump::MapInfoLump()
{
    //--[MapInfoLump]
    //--System
    //--Type
    mTypeFlag = MAP_2DTILEDDEPTH;
}
MapInfoLump::~MapInfoLump()
{
}

//--Statics
int MapInfoLump::xTilesetsTotal = 0;
TilesetInfoPack *MapInfoLump::xTilesetPacks = NULL;

//--Core Methods
void MapInfoLump::BootTilesetData(const Tiled::Map *pMap)
{
    //--Stores all tileset data into the gidmapper class. This provides unique tile IDs to all
    //  tiles even if they're of different tilesets.
    if(!pMap) return;

    //--Clean any old data.
    for(int i = 0; i < xTilesetsTotal; i ++)
    {
        free(xTilesetPacks[i].mTilesetName);
        free(xTilesetPacks[i].mTilesetPath);
    }
    free(xTilesetPacks);
    xTilesetsTotal = 0;
    xTilesetPacks = NULL;

    //--First, figure out how many tilesets there are.
    QSet<Tiled::SharedTileset> rTilesetSet = pMap->usedTilesets();
    //QList<Tiled::Tileset *> rTilesetList = pMap->usedTilesets();
    QList<Tiled::SharedTileset> rTilesetList = rTilesetSet.toList();
    xTilesetsTotal = rTilesetList.count();

    //--Allocate.
    xTilesetPacks = (TilesetInfoPack *)malloc(sizeof(TilesetInfoPack) * xTilesetsTotal);

    //--Setup.
    int tTileCount = 0;

    //--Fill the data.
    for(int i = 0; i < xTilesetsTotal; i ++)
    {
        //--Get the tileset.
        Tiled::SharedTileset rTileset = rTilesetList[i];

        //--Get the tileset path. It is probably relative. That's fine!
        QByteArray tPathData = rTileset->imageSource().toEncoded();
        const char *rTruePath = tPathData.data();
        fprintf(stdout, " True path %i is %s\n", i, rTruePath);

        //--Save the path.
        xTilesetPacks[i].mTilesetPath = (char *)malloc(sizeof(char) * (strlen(rTruePath) + 1));
        strcpy(xTilesetPacks[i].mTilesetPath, rTruePath);

        //--Get the name's data out.
        QByteArray tData = rTileset->name().toLocal8Bit();
        const char *rTrueName = tData.data();

        //--Save the name.
        xTilesetPacks[i].mTilesetName = (char *)malloc(sizeof(char) * (strlen(rTrueName) + 1));
        strcpy(xTilesetPacks[i].mTilesetName, rTrueName);

        //--Store the tile count.
        xTilesetPacks[i].mTilesToThisPoint = tTileCount;
        xTilesetPacks[i].mTilesTotal = rTileset->tileCount();
        tTileCount += xTilesetPacks[i].mTilesTotal;
    }
}

//--File I/O
void MapInfoLump::WriteData(uint64_t &sCounter, FILE *pOutfile)
{
    //--Overloaded member. Write the local data to the file. In the case of the Sugar^3 exporter,
    //  the MapInfoLump is of a fixed version type.
    if(!pOutfile) return;

    //--Write the Lump's type as a 10-byte string.
    const char tString[11] = "MAPINFO000";
    fwrite(tString, sizeof(char), 10, pOutfile);
    sCounter += (sizeof(char) * 10);

    //--Write the map's type.
    fwrite(&mTypeFlag, sizeof(uint8_t), 1, pOutfile);
    sCounter += (sizeof(uint8_t) * 1);

    //--Tileset info.
    fwrite(&xTilesetsTotal, sizeof(int16_t), 1, pOutfile);
    sCounter += (sizeof(int16_t) * 1);

    //--Tileset data.
    for(int i = 0; i < xTilesetsTotal; i ++)
    {
        //--Name.
        uint8_t tLen = (uint8_t)strlen(xTilesetPacks[i].mTilesetName);
        fwrite(&tLen, sizeof(uint8_t), 1, pOutfile);
        fwrite(xTilesetPacks[i].mTilesetName, sizeof(char), tLen, pOutfile);
        sCounter += (sizeof(uint8_t) * 1) + (sizeof(char) * tLen);

        //--Sizes.
        fwrite(&xTilesetPacks[i].mTilesToThisPoint, sizeof(int16_t), 1, pOutfile);
        fwrite(&xTilesetPacks[i].mTilesTotal, sizeof(int16_t), 1, pOutfile);
        sCounter += (sizeof(int16_t) * 2);
    }
}
