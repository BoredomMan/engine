#pragma once

#include <stdint.h>
#include <stdio.h>

class RootLump
{
private:
protected:
    //--System
    uint32_t mTypeCode;
    uint64_t mAddressInFile;
    uint64_t mAddressOfAddress;

public:
    //--System
    RootLump();
    virtual ~RootLump();

    //--Public Variables
    char *mName;

    //--Property Queries
    int GetType();

    //--Manipulators
    void SetAddressOfAddress(uint64_t pAddress);

    //--Core Methods
    //--Update
    //--File I/O
    void WriteTo(uint64_t &sCounter, FILE *pOutfile);
    virtual void WriteData(uint64_t &, FILE *);
    void WriteAddress(FILE *pOutfile);

    //--Drawing
    //--Pointer Routing
    //--Static Functions
    //--Lua Hooking

};

