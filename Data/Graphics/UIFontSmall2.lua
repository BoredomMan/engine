--[UI Font: Small]
--Edging script.

--Local Functions
local fnCreateLetter = function(sLetterString, iLft, iTop, iWid, iHei, iStdSpacing)
	iWid = iWid + 2
	iHei = iHei + 1
	SugarFont_SetBitmapCharacter(string.byte(sLetterString), iLft, iTop, iLft + iWid, iTop + iHei, iWid + iStdSpacing)
end

--Letters
fnCreateLetter("A",  88, 77, 17, 16, 1)
fnCreateLetter("B", 113, 77, 13, 16, 1)
fnCreateLetter("C", 134, 77, 14, 16, 1)
fnCreateLetter("D", 156, 77, 17, 16, 1)
fnCreateLetter("E", 181, 77, 13, 16, 1)
fnCreateLetter("F", 201, 77, 12, 16, 1)
fnCreateLetter("G", 220, 77, 17, 16, 1)
fnCreateLetter("H", 243, 77, 19, 16, 1)
fnCreateLetter("I", 269, 77,  8, 16, 1)
fnCreateLetter("J", 282, 77, 11, 21, 1)
fnCreateLetter("K", 299, 77, 16, 16, 1)
fnCreateLetter("L", 324, 77, 13, 16, 1)
fnCreateLetter("M", 345, 77, 20, 16, 1)
fnCreateLetter("N", 373, 77, 17, 16, 1)
fnCreateLetter("O", 398, 77, 16, 16, 1)
fnCreateLetter("P", 421, 77, 13, 16, 1)
fnCreateLetter("Q", 442, 77, 18, 21, 1)
fnCreateLetter("R", 465, 77, 16, 16, 1)
fnCreateLetter("S", 488, 77, 10, 16, 1)
fnCreateLetter("T", 507, 77, 14, 16, 1)
fnCreateLetter("U", 528, 77, 19, 16, 1)
fnCreateLetter("V", 552, 77, 18, 16, 1)
fnCreateLetter("W", 575, 77, 23, 16, 1)
fnCreateLetter("X", 603, 77, 17, 16, 1)
fnCreateLetter("Y", 626, 77, 16, 16, 1)
fnCreateLetter("Z", 649, 77, 12, 16, 1)

fnCreateLetter("a",  89, 12, 10, 17, 1)
fnCreateLetter("b", 106, 12, 12, 17, 1)
fnCreateLetter("c", 126, 12,  9, 17, 1)
fnCreateLetter("d", 143, 12, 12, 17, 1)
fnCreateLetter("e", 162, 12,  9, 17, 1)
fnCreateLetter("f", 180, 12, 10, 17, 1)
fnCreateLetter("g", 193, 12, 13, 24, 1)
fnCreateLetter("h", 213, 12, 14, 17, 1)
fnCreateLetter("i", 233, 12,  6, 17, 1)
fnCreateLetter("j", 244, 12,  6, 23, 1)
fnCreateLetter("k", 260, 12, 12, 17, 1)
fnCreateLetter("l", 279, 12,  6, 17, 1)
fnCreateLetter("m", 292, 12, 22, 17, 1)
fnCreateLetter("n", 320, 12, 14, 17, 1)
fnCreateLetter("o", 341, 12, 10, 17, 1)
fnCreateLetter("p", 359, 12, 12, 24, 1)
fnCreateLetter("q", 379, 12, 12, 24, 1)
fnCreateLetter("r", 398, 12,  9, 17, 1)
fnCreateLetter("s", 414, 12,  8, 17, 1)
fnCreateLetter("t", 429, 12,  8, 17, 1)
fnCreateLetter("u", 443, 12, 14, 17, 1)
fnCreateLetter("v", 463, 12, 13, 17, 1)
fnCreateLetter("w", 481, 12, 17, 17, 1)
fnCreateLetter("x", 504, 12, 12, 17, 1)
fnCreateLetter("y", 523, 12, 13, 25, 1)
fnCreateLetter("z", 541, 12, 10, 17, 1)

--Numbers
fnCreateLetter("1",  89, 140,  9, 17, 1)
fnCreateLetter("2", 107, 140, 10, 17, 1)
fnCreateLetter("3", 125, 140, 10, 17, 1)
fnCreateLetter("4", 142, 140, 12, 17, 1)
fnCreateLetter("5", 161, 140, 10, 17, 1)
fnCreateLetter("6", 179, 140, 10, 17, 1)
fnCreateLetter("7", 197, 140, 11, 17, 1)
fnCreateLetter("8", 215, 140, 10, 17, 1)
fnCreateLetter("9", 233, 140, 11, 17, 1)
fnCreateLetter("0", 251, 140, 10, 17, 1)

--Misc
fnCreateLetter(" ",    0, 0,  4,  1, 1)
fnCreateLetter("/",    0, 0,  4,  1, 1)
fnCreateLetter("'",    0, 0,  4,  1, 1)
fnCreateLetter("\"",   0, 0,  4,  1, 1)
fnCreateLetter(";",    0, 0,  4,  1, 1)
fnCreateLetter(":",    0, 0,  4,  1, 1)
fnCreateLetter("[",    0, 0,  4,  1, 1)
fnCreateLetter("]",    0, 0,  4,  1, 1)
fnCreateLetter("-",    0, 0,  4,  1, 1)
fnCreateLetter("+",    0, 0,  4,  1, 1)
fnCreateLetter("_",    0, 0,  4,  1, 1)
fnCreateLetter("=",    0, 0,  4,  1, 1)
fnCreateLetter("!",  288, 140,  4, 17, 1)
fnCreateLetter("@",    0, 0,  4,  1, 1)
fnCreateLetter("#",    0, 0,  4,  1, 1)
fnCreateLetter("$",    0, 0,  4,  1, 1)
fnCreateLetter("%",    0, 0,  4,  1, 1)
fnCreateLetter("^",    0, 0,  4,  1, 1)
fnCreateLetter("&",    0, 0,  4,  1, 1)
fnCreateLetter("*",    0, 0,  4,  1, 1)
fnCreateLetter("(",  299, 140,  6, 21, 1)
fnCreateLetter(")",  313, 140,  6, 21, 1)
fnCreateLetter("?",    0, 0,  4,  1, 1)
fnCreateLetter("~",  269, 149, 12,  6, 1)

fnCreateLetter(".",  327, 140,  2, 17, 1)
fnCreateLetter(",",  333, 140,  4, 21, 1)







