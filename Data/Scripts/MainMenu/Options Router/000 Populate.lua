--[Options Routing Menu]
--Allows the player to select one of several submenus with their own option sets.

--[Setup]
local sBasePath = fnResolvePath()
Debug_PushPrint(false, "Populating options router.\n")

--[Header]
FlexMenu_SetHeaderSize(1)
FlexMenu_SetHeaderLine(0, "Options")

--[Buttons]
--Engine and Display, sets things that affect the engine itself. This includes things like resolution and mipmapping.
FlexMenu_RegisterButton("Engine and Display Options")
	SugarButton_SetProperty("Priority", 0)
	SugarButton_SetProperty("NewLuaExec", 0, sBasePath .. "100 Go To Submenu.lua", 0)
DL_PopActiveObject()

--Game options. These affect various game display properties.
FlexMenu_RegisterButton("Game Options")
	SugarButton_SetProperty("Priority", 1)
	SugarButton_SetProperty("NewLuaExec", 0, sBasePath .. "100 Go To Submenu.lua", 1)
DL_PopActiveObject()

--Back to the main menu.
FlexMenu_RegisterButton("Back")
	SugarButton_SetProperty("Priority", 10000)
	SugarButton_SetProperty("NewLuaExec", 0, sBasePath .. "900 Close.lua", 0)
DL_PopActiveObject()

--Debug.
Debug_PopPrint("Completed populating main menu.\n")