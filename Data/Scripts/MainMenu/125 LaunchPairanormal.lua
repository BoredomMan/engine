--[Launch Pairanormal]
--Pairanormal, written and art'd by CocoMint. Has a special title screen which can override the one
-- provided by the base game.
local iGameIndex = fnGetGameIndex("Pairanormal")
if(iGameIndex == 0 or gsaGameEntries[iGameIndex].sActivePath == "Null") then return end

--Execute the file in question.
LM_ExecuteScript(gsaGameEntries[iGameIndex].sActivePath)
