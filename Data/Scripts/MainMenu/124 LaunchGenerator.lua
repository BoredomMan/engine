--[Launch Level Generator Mode]
--This tool allows a player to see how the random level generator works.
local iAdventureIndex = fnGetGameIndex("Adventure Mode")
local iGeneratorIndex = fnGetGameIndex("Level Generator")
if(iAdventureIndex == 0 or gsaGameEntries[iAdventureIndex].sActivePath == "Null") then return end
if(iGeneratorIndex == 0 or gsaGameEntries[iGeneratorIndex].sActivePath == "Null") then return end

--[Adventure Check]
--Use the adventure path for the sprite file.
if(iAdventureIndex == 0) then return end
local sSpriteFilePath = string.sub(gsaGameEntries[iAdventureIndex].sActivePath, 1, -12) .. "Datafiles/Sprites.slf"

--[Generator Boot]
--Go to system boot.
MapM_PushMenuStackHead()
	FlexMenu_FlagClose()
DL_PopActiveObject()

--Store the grids path.
VM_SetVar("Root/Paths/System/Startup/sGridsPath", "S", gsaGameEntries[iGeneratorIndex].sActivePath)

--Fonts.
LM_ExecuteScript(string.sub(gsaGameEntries[iGeneratorIndex].sActivePath, 1, -10) .. "Fonts.lua")

--Create level.
AdlevGenerator_Create()

--Texture settings.
local iNearest = DM_GetEnumeration("GL_NEAREST")
ALB_SetTextureProperty("MagFilter", iNearest)
ALB_SetTextureProperty("MinFilter", iNearest)

--Load the textures for the level.
SLF_Open(sSpriteFilePath)
DL_AddPath("Root/Images/Sprites/LevelTextures/")
DL_ExtractBitmap("Level|RegulusCave", "Root/Images/Sprites/LevelTextures/RegulusCave")
SLF_Close()
ALB_SetTextureProperty("Restore Defaults")

--Tell the level to use this texture mapping.
AdlevGenerator_SetProperty("Texture Path", "Root/Images/Sprites/LevelTextures/RegulusCave")
AdlevGenerator_SetProperty("Pattern TL", 0, 0)
AdlevGenerator_SetProperty("Floors Total", 4)
AdlevGenerator_SetProperty("Floor Position", 0,  0, 112)
AdlevGenerator_SetProperty("Floor Position", 1, 16, 112)
AdlevGenerator_SetProperty("Floor Position", 2,  0, 128)
AdlevGenerator_SetProperty("Floor Position", 3, 16, 128)

--Other UV Positions
AdlevGenerator_SetProperty("Blackout Pos", 6, 0)
AdlevGenerator_SetProperty("Ladder Pos", 3, 4, 4, 4)
AdlevGenerator_SetProperty("Cliff Pos", 0, 9)
AdlevGenerator_SetProperty("Floor Edge Pos", 3, 0)
AdlevGenerator_SetProperty("Treasure Pos", 3, 3)
AdlevGenerator_SetProperty("Enemy Pos", 5, 4)

--Run the setup scripts.
LM_ExecuteScript(gsaGameEntries[iGeneratorIndex].sActivePath)
