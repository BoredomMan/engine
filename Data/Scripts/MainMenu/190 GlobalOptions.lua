--[Global Options]
--Spawns a menu which has all the general-purpose options on it.
MapM_PushMenuStackHead()
	FlexMenu_Clear()
	LM_ExecuteScript("Data/Scripts/MainMenu/Options Router/000 Populate.lua")
DL_PopActiveObject()