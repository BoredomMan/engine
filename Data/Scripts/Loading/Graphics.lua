--[Graphics]
--Handles graphics related to the main menu and other must-haves.
Debug_PushPrint(false, "[Loading] System Graphics\n")

--[Reader Function]
local fnReadImages = function(sPrefix, iCount, sDLPath)
	for i = 1, iCount, 1 do
		DL_ExtractBitmap(sPrefix .. (i-1), sDLPath .. sPrefix .. (i-1))
	end
end

--[Title Screen]
SLF_Open("Data/UISystem.slf")

--Font, uses different filters.
DL_AddPath("Root/Images/GUI/System/")
ALB_SetTextureProperty("MagFilter", DM_GetEnumeration("GL_NEAREST"))
ALB_SetTextureProperty("MinFilter", DM_GetEnumeration("GL_LINEAR"))
DL_ExtractBitmap("UIFontSmall",  "Root/Images/GUI/System/UIFontSmall")
ALB_SetTextureProperty("MagFilter", DM_GetEnumeration("GL_NEAREST"))
ALB_SetTextureProperty("MinFilter", DM_GetEnumeration("GL_LINEAR"))
DL_ExtractBitmap("UIFontVSmall", "Root/Images/GUI/System/UIFontVSmall")
ALB_SetTextureProperty("Restore Defaults")

--Background and Title
DL_AddPath("Root/Images/GUI/Background/")
DL_ExtractBitmap("MenuBG",     "Root/Images/GUI/Background/MenuBG")
DL_ExtractBitmap("MenuBGOver", "Root/Images/GUI/Background/MenuBGOver")

--Text and Borders
DL_AddPath("Root/Images/GUI/System/")
DL_ExtractBitmap("CardBorder", "Root/Images/GUI/System/CardBorder")
DL_ExtractBitmap("UpArrow",    "Root/Images/GUI/System/UpArrow")
DL_ExtractBitmap("DownArrow",  "Root/Images/GUI/System/DnArrow")

--String Entry
DL_AddPath("Root/Images/AdventureUI/String/")
DL_ExtractBitmap("AdvString|Base",    "Root/Images/AdventureUI/String/Base")
DL_ExtractBitmap("AdvString|Pressed", "Root/Images/AdventureUI/String/Pressed")

--[Adventure Mode Loading Icons]
--These icons are used on the UI which allows the player to select a file.
local iNearest = DM_GetEnumeration("GL_NEAREST")
ALB_SetTextureProperty("MagFilter", iNearest)
ALB_SetTextureProperty("MinFilter", iNearest)
SLF_Open("Data/Sprites.slf")
DL_AddPath("Root/Images/GUI/LoadingImg/")

--2855
DL_ExtractBitmap("55|SW|1", "Root/Images/GUI/LoadingImg/55_Doll")

--Florentina
DL_ExtractBitmap("Florentina|SW|1", "Root/Images/GUI/LoadingImg/Florentina_Alraune")

--JX-101
DL_ExtractBitmap("JX101|South|1", "Root/Images/GUI/LoadingImg/JX-101_SteamDroid")

--SX-399
DL_ExtractBitmap("SX399Lord|SW|1", "Root/Images/GUI/LoadingImg/SX-399_SteamLord")

--Mei
DL_ExtractBitmap("Mei_Human|SW|1",   "Root/Images/GUI/LoadingImg/Mei_Human")
DL_ExtractBitmap("Mei_Alraune|SW|1", "Root/Images/GUI/LoadingImg/Mei_Alraune")
DL_ExtractBitmap("Mei_Bee|SW|1",     "Root/Images/GUI/LoadingImg/Mei_Bee")
DL_ExtractBitmap("Mei_Slime|SW|1",   "Root/Images/GUI/LoadingImg/Mei_Slime")
DL_ExtractBitmap("Mei_Werecat|SW|1", "Root/Images/GUI/LoadingImg/Mei_Werecat")
DL_ExtractBitmap("Mei_Ghost|SW|1",   "Root/Images/GUI/LoadingImg/Mei_Ghost")

--Christine
DL_ExtractBitmap("Christine_Human|SW|1",         "Root/Images/GUI/LoadingImg/Christine_Human")
DL_ExtractBitmap("Christine_Doll|SW|1",          "Root/Images/GUI/LoadingImg/Christine_Doll")
DL_ExtractBitmap("Christine_Male|SW|1",          "Root/Images/GUI/LoadingImg/Christine_Male")
DL_ExtractBitmap("Christine_Golem|SW|1",         "Root/Images/GUI/LoadingImg/Christine_Golem")
DL_ExtractBitmap("Christine_Electrosprite|SW|1", "Root/Images/GUI/LoadingImg/Christine_Electrosprite")
DL_ExtractBitmap("Christine_Latex|SW|1",         "Root/Images/GUI/LoadingImg/Christine_Latex")
DL_ExtractBitmap("Christine_Darkmatter|SW|1",    "Root/Images/GUI/LoadingImg/Christine_Darkmatter")
DL_ExtractBitmap("Christine_SteamDroid|SW|1",    "Root/Images/GUI/LoadingImg/Christine_SteamDroid")
DL_ExtractBitmap("Christine_DreamGirl|SW|1",     "Root/Images/GUI/LoadingImg/Christine_Eldritch")
DL_ExtractBitmap("Christine_Raiju|SW|1",         "Root/Images/GUI/LoadingImg/Christine_Raiju")

--Clean.
ALB_SetTextureProperty("Restore Defaults")

--[Clean]
SLF_Close()
Debug_PopPrint("[Loading] System Graphics Done\n")
