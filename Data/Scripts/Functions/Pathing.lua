--[Pathing]
--Allows entities to figure out how to get from point A to point B. That's what pathing is you nob.

--[BuildMoveList]
--Builds a list including all the connections this room has to another. The list is in this format:
--[1] -> Direction ("N", "S", "E", etc)
--[2] -> Name ("Main Hall", "Kitchen")

--The function requires a PandemoniumRoom to be atop the activity stack. If not, you'll get an
-- empty list and no pathing data.
--The return will be (iListSize, saList), in that order. This will be the case even on error.
fnBuildMoveList = function()
	
	--Setup.
	local iListSize = 0
	local saList = {}
	
	--Make a quick list.
	local i = 1
	local saCheckList = {"N", "E", "S", "W", "D", "U"}
	
	--Check all directions on the list.
	while(saCheckList[i] ~= nil) do
		
		--Get the name.
		local sConnectionName = PR_HasConnection(saCheckList[i])
		
		--Add it to the list if it's not "Null"
		if(sConnectionName ~= "Null") then
			
			iListSize = iListSize + 1
			saList[iListSize] = {saCheckList[i], sConnectionName}
			
		end
		
		--Next.
		i = i + 1
		
	end

	--Now pass back the list and its size.
	return iListSize, saList

end
