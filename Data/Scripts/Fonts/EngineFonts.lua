--[ ======================================== Engine Fonts ======================================= ]
--Boots all the fonts used by the core engine. This is called by the c++ program immediately after
-- Freetype is initialized.
if(gbBootedCoreFonts == true) then return end
gbBootedCoreFonts = true

--[Setup]
--Paths.
local sFontPath = "Data/"
local sKerningPath = "Data/Scripts/Fonts/"

--Special Flags
local ciFontNoFlags = 0
local ciFontNearest  = Font_GetProperty("Constant Precache With Nearest")
local ciFontEdge     = Font_GetProperty("Constant Precache With Edge")
local ciFontDownfade = Font_GetProperty("Constant Precache With Downfade")
local ciFontSpecialS = Font_GetProperty("Constant Precache With Special S")

--[ ======================================= Font Registry ======================================= ]
--Sanchez
Font_SetProperty("Downfade", 0.90, 0.60)
Font_SetProperty("Outline Width", 3)
Font_Register("Sanchez 35 DFO", sFontPath .. "SanchezRegular.otf", sKerningPath .. "Sanchez30_Kerning.lua", 35, ciFontNearest + ciFontEdge + ciFontDownfade)

Font_SetProperty("Downfade", 0.90, 0.60)
Font_SetProperty("Outline Width", 1)
Font_Register("Sanchez 22 DFO", sFontPath .. "SanchezRegular.otf", sKerningPath .. "Sanchez20_Kerning.lua", 22,  ciFontEdge + ciFontDownfade)

--[ ========================================== Aliases ========================================== ]
--Sanchez 35
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "String Entry Header")

--Sanchez 22
Font_SetProperty("Add Alias", "Sanchez 22 DFO", "String Entry Main")

--[ ====================================== System Aliases ======================================= ]
--The fonts "System Font", "Bitmap Font", and "Bitmap Font Small" are system fonts guaranteed to exist.
-- We can register aliases for them here.

--System Font
Font_SetProperty("Add Alias", "System Font", "Virtual Console Main")

--Bitmap Font
Font_SetProperty("Add Alias", "Bitmap Font", "Adventure Debug Font")
Font_SetProperty("Add Alias", "Bitmap Font", "Corrupter Menu Main")
Font_SetProperty("Add Alias", "Bitmap Font", "Flex Button Main")
Font_SetProperty("Add Alias", "Bitmap Font", "Flex Menu Main")
Font_SetProperty("Add Alias", "Bitmap Font", "Gallery Menu Main")
Font_SetProperty("Add Alias", "Bitmap Font", "Classic Level Main")
Font_SetProperty("Add Alias", "Bitmap Font", "Classic Level Damage")
Font_SetProperty("Add Alias", "Bitmap Font", "Zone Editor Main")

--Bitmap Font Small
Font_SetProperty("Add Alias", "Bitmap Font Small", "Corrupter Menu Small")
Font_SetProperty("Add Alias", "Bitmap Font Small", "Blue Sphere Main")
Font_SetProperty("Add Alias", "Bitmap Font Small", "Context Menu Main")
Font_SetProperty("Add Alias", "Bitmap Font Small", "Classic Level Small")
