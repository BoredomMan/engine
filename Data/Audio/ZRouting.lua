--[ ===================================== Audio Routing File ==================================== ]
--Loads music/sfx/voice samples. These are common across all game modes.
local sBasePath = fnResolvePath()

--[Menu SFX]
--The only thing that all game modes need universally is the menu SFX. Everything else is game-specific.
local sGUISFXPath = sBasePath .. "MenuSFX/"
AudioManager_Register("Menu|ChangeHighlight", "AsSound", "AsSample", sGUISFXPath .. "ChangeHighlight.ogg")
AudioManager_Register("Menu|Failed",          "AsSound", "AsSample", sGUISFXPath .. "Failed.wav")
AudioManager_Register("Menu|Select",          "AsSound", "AsSample", sGUISFXPath .. "Select.ogg")
AudioManager_Register("Menu|Save",            "AsSound", "AsSample", sGUISFXPath .. "Save.ogg")