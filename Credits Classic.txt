===================================================================================================
==                                             Forward                                           ==
===================================================================================================
The main website for this game is located at https://hopremastered.wordpress.com/
Project updates can be found there.

The project is funded by Patreon. https://www.patreon.com/Saltyjustice
Updates are mirrored there. If you like the game and want more, support it!

The game has a TFGS database entry for reviews:
 https://tfgamessite.com/index.php?module=viewgame&id=843

If you got the game from somewhere other than one of these three locations, and there were no links
to said locations, then it was done without permission. Please notify the author:
 Saltyjustice AT StarlightStudios DOT org


===================================================================================================
==                                          Game Credits                                         ==
===================================================================================================
Programming, UI, Design
 SaltyJustice (Saltyjustice AT StarlightStudios DOT org)

Sprites, UI
 Urimas "The Spaceman" Ebonheart (https://www.patreon.com/urimasebonheart)

Very Helpful Testers
 Klaysee, Aaaac, Trinity, and many others!

Person Named Jobel
 Joel (Fooled you!)

Original Game, 3DCG Art
 Pashaimeru


===================================================================================================
==                                         Audio Credits                                         ==
===================================================================================================
Much of this audio was taken from open-license libraries on www.OpenGameArt.org. Consider 
supporting them if you like what you heard!

Any music not specified here was composed by the world-famous DrDissonance.

--[ ======================================= Sound Effects ======================================= ]
Lokif (???) for the GUI SFX.
Kenney Vleugels (www.kenney.nl) for some of the combat SFX.
qat (???) for the weapon-pickup SFX.
