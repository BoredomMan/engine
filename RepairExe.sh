install_name_tool -change /usr/local/lib/libSDL2-2.0.0.dylib @loader_path/osxbin/libSDL2-2.0.0.dylib StarlightEngineSDLOSX
install_name_tool -change /usr/local/opt/lua/lib/liblua.5.2.dylib @loader_path/osxbin/liblua.5.2.4.dylib StarlightEngineSDLOSX
install_name_tool -change /usr/lib/libc++.1.dylib @loader_path/osxbin/libc++.1.dylib StarlightEngineSDLOSX
install_name_tool -change /usr/local/opt/boost/lib/libboost_filesystem.dylib @loader_path/osxbin/libboost_filesystem.dylib StarlightEngineSDLOSX
install_name_tool -change @loader_path/libbass.dylib @loader_path/osxbin/libbass.dylib StarlightEngineSDLOSX
install_name_tool -change @loader_path/libbass_fx.dylib @loader_path/osxbin/libbass_fx.dylib StarlightEngineSDLOSX