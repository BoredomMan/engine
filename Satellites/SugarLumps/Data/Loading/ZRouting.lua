--[Loading]
--Only used for loading images.
local sBasePath = fnResolvePath()

--Setup
SLF_Open("Output/Loading.slf")
ImageLump_SetCompression(1)

--Characters running. Sprite sheets, requires padding to work.
ImageLump_SetBlockTrimmingFlag(true)
ImageLump_Rip("ChristineRun", sBasePath .. "ChristineRun.png", 0, 0, -1, -1, 0)
ImageLump_Rip("RaijuRun",     sBasePath .. "RaijuRun.png",     0, 0, -1, -1, 0)
ImageLump_SetBlockTrimmingFlag(false)

--Other UI parts.
ImageLump_Rip("AnyKeyOver",   sBasePath .. "AnyKeyOver.png",   0, 0, -1, -1, 0)
ImageLump_Rip("AnyKeyUnder",  sBasePath .. "AnyKeyUnder.png",  0, 0, -1, -1, 0)
ImageLump_Rip("LoadingOver",  sBasePath .. "LoadingOver.png",  0, 0, -1, -1, 0)
ImageLump_Rip("LoadingUnder", sBasePath .. "LoadingUnder.png", 0, 0, -1, -1, 0)

--Finish
SLF_Close()