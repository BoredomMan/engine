--[Common Functions]
--Execute this before any other files.  It contains functions that are useful to the other parts
-- of a compression project.

--Function that resolves the path of the currently executing script.
fnResolvePath = function()
	local sStartPath = debug.getinfo(2, "S").source
	local iLen = string.len(sStartPath)

	--Determine where the last slash is
	local sLetter = ""
	for i = iLen, 1, -1 do
		sLetter = string.sub(sStartPath, i, i)
		if(sLetter == "/" or sLetter == "\\") then
			iLen = i
			break
		end
	end

	local sFinishPath = ""
	for i = 2, iLen, 1 do

		sLetter = string.sub(sStartPath, i, i)
		sFinishPath = sFinishPath .. sLetter
	end

	return sFinishPath
end

--Function that appends two arrays (or tables) together.
-- Table2 is appended to the end of Table1.  Table2 is unchanged.
fnAppendTables = function(saTable1, saTable2)
	local iArrayLen2 = #saTable2
	for i = 1, iArrayLen2, 1 do
		table.insert(saTable1, saTable2[i])
	end

	return saTable1
end

--[Variables for these functions]
--By default, these variables return to the neutral-state and have no effect.
gbRipHitboxes = false
gsRipHitboxesExt = ""

--Function that rips images in a provided pattern.  Each Animation must be horizontally attached and
-- have constant width and height, otherwise the pattern is adaptable.
fnRipAnimations = function(sBaseName, sImagePath, iTotal, saNames, iaCounts, iaX, iaY, iWid, iHei)

	--Setup
	local iRipCurX = 0
	local iRipCurY = 0

	--For each unique animation...
	for i = 1, iTotal, 1 do

		--Seek to start position of the animation
		iRipCurX = iaX[i] * iWid
		iRipCurY = iaY[i] * iHei

		--For each frame of the animation...
		for p = 1, iaCounts[i], 1 do

			--Name resolver.  If there's only one frame, only put the name, not Name0.
			local sName = sBaseName .. saNames[i] .. (p-1)
			if(iaCounts[i] == 1) then sName = sBaseName .. saNames[i] end

			--Rip the frame
			io.write("Name: " .. saNames[i] .. " ")
			ImageLump_Rip(sName, sImagePath, iRipCurX, iRipCurY, iWid, iHei, 0)

			--Are we in hitbox adding mode?
			if(gbRipHitboxes == true) then

				local sHitboxImg = string.gsub(sImagePath, ".png", gsRipHitboxesExt .. ".png")
				--io.write("Hitbox: " .. sHitboxImg .. "\n")
				ImageLump_AddHitbox(sHitboxImg, iRipCurX, iRipCurY, iWid, iHei, "R")
				ImageLump_AddHitbox(sHitboxImg, iRipCurX, iRipCurY, iWid, iHei, "G")
				ImageLump_AddHitbox(sHitboxImg, iRipCurX, iRipCurY, iWid, iHei, "B")

			end

			--Pop it to keep the stack intact
			DL_PopActiveEntity()

			--Rip the frame as named, move 1 X to the right.
			iRipCurX = iRipCurX + iWid

		end

	end

end

--Function where the animations are ripped with a fixed width/height, however, the iaX and iaY describe
-- the topleft start position, and are not multiples of the width/height.
fnRipAnimationsLit = function(sBaseName, sBasePath, saImagePaths, iTotal, saNames, iaCounts, iaX, iaY, iaWid, iaHei)

	--Setup
	local iRipCurX = 0
	local iRipCurY = 0
	local iRipWid = 0
	local iRipHei = 0

	--For each unique animation...
	for i = 1, iTotal, 1 do

		--Seek to start position of the animation
		iRipCurX = iaX[i]
		iRipCurY = iaY[i]
		iRipWid = iaWid[i]
		iRipHei = iaHei[i]

		--If there's only one, don't append a number to the name, just rip it
		if(iaCounts[i] == 1) then

			ImageLump_Rip(sBaseName .. saNames[i], sBasePath .. saImagePaths[i] .. ".png", iRipCurX, iRipCurY, iRipWid, iRipHei, 0)
			--Are we in hitbox adding mode?
			if(gbRipHitboxes == true) then

				local sHitboxImg = string.gsub(sBasePath .. saImagePaths[i] .. ".png", ".png", gsRipHitboxesExt .. ".png")
				--io.write("Hitbox: " .. sHitboxImg .. "\n")
				ImageLump_AddHitbox(sHitboxImg, iRipCurX, iRipCurY, iRipWid, iRipHei, "R")
				ImageLump_AddHitbox(sHitboxImg, iRipCurX, iRipCurY, iRipWid, iRipHei, "G")
				ImageLump_AddHitbox(sHitboxImg, iRipCurX, iRipCurY, iRipWid, iRipHei, "B")

			end
			DL_PopActiveEntity()
		
		else

			--For each frame of the animation...
			for p = 1, iaCounts[i], 1 do

				--Rip the frame
				--Name|AnimationName0
				local sRipName = sBaseName .. saNames[i] .. (p-1)
				ImageLump_Rip(sRipName, sBasePath .. saImagePaths[i] .. ".png", iRipCurX, iRipCurY, iRipWid, iRipHei, 0)

				--Are we in hitbox adding mode?
				if(gbRipHitboxes == true) then

					local sHitboxImg = string.gsub(sBasePath .. saImagePaths[i] .. ".png", ".png", gsRipHitboxesExt .. ".png")
					--io.write("Hitbox: " .. sHitboxImg .. "\n")
					ImageLump_AddHitbox(sHitboxImg, iRipCurX, iRipCurY, iRipWid, iRipHei, "R")
					ImageLump_AddHitbox(sHitboxImg, iRipCurX, iRipCurY, iRipWid, iRipHei, "G")
					ImageLump_AddHitbox(sHitboxImg, iRipCurX, iRipCurY, iRipWid, iRipHei, "B")

				end
				DL_PopActiveEntity()

				--Rip the frame as named, move 1 X to the right.
				iRipCurX = iRipCurX + iRipWid

			end

		end


	end

end


--Function causes all subfolders in a folder to execute a specified file.
-- Relies on the Allegro filesystem.
fnExecAll = function(sCurrentPath, sFilePattern)

	--Scan
	--io.write("Opening filesystem...")
	FS_Open(sCurrentPath, false)
	--io.write("done!\n")

	--For each entry, appends the sFilePattern and try to run it.
	-- Ignore non-directories.  Directories end in '/'.
	sPath = FS_Iterate()
	while (sPath ~= "NULL") do

		--Check the last byte for '/'
		local iLength = string.len(sPath)
		if(string.byte(sPath, iLength) == string.byte("/")) then

			--Append the sFilePattern on the end and call it
			--io.write("Exec " .. sPath .. sFilePattern .. "\n")
			LM_ExecuteScript(sPath .. sFilePattern)

		end

		sPath = FS_Iterate()

	end

	--Clean
	--io.write("Closing filesystem...")
	FS_Close()
	--io.write("done!\n")

end
