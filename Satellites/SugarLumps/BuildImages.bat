SugarLumps.exe -Redirect "BuildImagesLog.txt" -Call BuildImages.lua
cd Output/
if exist "Loading.slf" (
	copy "Loading.slf" "../../../Data/Loading.slf"
	del "Loading.slf" 
	echo "Copying Loading.slf"
)
if exist "Sprites.slf" (
	copy "Sprites.slf" "../../../Data/Sprites.slf"
	copy "Sprites.slf" "../../../Games/AdventureMode/Datafiles/Sprites.slf"
	del "Sprites.slf" 
	echo "Copying Sprites.slf"
)
if exist "UISystem.slf" (
	copy "UISystem.slf" "../../../Data/UISystem.slf"
	del "UISystem.slf" 
	echo "Copying UISystem.slf"
)