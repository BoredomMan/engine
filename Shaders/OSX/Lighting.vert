//--Vertex half of the lighting shader. Just provides the position data.
varying vec3 vPosition;
void main(void)
{
    //--Fetch texturing information for both textures.
    gl_TexCoord[0] = gl_MultiTexCoord0;

    //--Standard transformation.
    gl_Position = ftransform();
    vPosition = gl_Vertex;
}
