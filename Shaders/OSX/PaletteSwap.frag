//--Fragment half of the palette swapping routine.  The texel is queried for its color, then this
//  color is checked against a list of existing colors. In case of a match, the output is changed,
//  else the texel is placed.
//--Lighting and other considerations happen after the texel color is selected.  This allows the
//  palettes to precede lighting and color multiplications.
#version 120
uniform sampler2D xTexSampler;

uniform int uTotalColors;
uniform vec4 uSrcColor[128];
uniform vec4 uDstColor[128];

void main(void)
{
    //--Retrieve the texel.
    vec4 tColor = texture2D(xTexSampler, gl_TexCoord[0].st);

    //--Query
    int tFoundAMatch = 0;
    for(int i = 0; i < uTotalColors; i ++)
    {
        if(tColor == uSrcColor[i])
        {
            tColor = uDstColor[i];
            tFoundAMatch = 1;
        }
    }

    gl_FragColor = tColor;
}
