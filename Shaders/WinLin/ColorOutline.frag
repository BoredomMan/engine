//--[Color Outline]
//--Creates an outline around a sprite. The outline's RGBA values should be provided to the shader when it is run.
#version 150 compatibility
uniform sampler2D xTexSampler;
uniform vec4 uOutlineColor;
void main(void)
{
    //--Retrieve the texel. If the color is not transparent, we're done.
    vec4 tColor = texture2D(xTexSampler, gl_TexCoord[0].st);
    if(tColor.a != 0.0)
    {
        gl_FragColor = tColor;
        return;
    }

    //--If the color is transparent, check each of the four texels surrounding this texel. If any of them
    //  are not transparent, we're on an edge, so render an outline.
    bool tRenderOutlineHere = false;
    ivec2 cTexSize = textureSize(xTexSampler, 0);
    float cTexelX = 1.0 / float(cTexSize.x);
    float cTexelY = 1.0 / float(cTexSize.y);
    vec2 tQuery = gl_TexCoord[0].st;

    //--Edge checker. If we happen to be exactly on the edge of the sprite, don't render. Sprites
    //  should be padded by 2 to make this work correctly.
    if(tQuery.x == 0.0 || tQuery.x == 1.0 - cTexelX || tQuery.y == 0.0 || tQuery.y == 1.0 - cTexelY)
    {
        tRenderOutlineHere = false;
        gl_FragColor = tColor;
        return;
    }

    //--Left check.
    if(tRenderOutlineHere == false)
    {
        tQuery.x = tQuery.x - cTexelX;
        tColor = texture2D(xTexSampler, tQuery);
        if(tColor.a != 0.0)
        {
            tRenderOutlineHere = true;
        }
        tQuery.x = tQuery.x + cTexelX;
    }
    //--Up check.
    if(tRenderOutlineHere == false)
    {
        tQuery.y = tQuery.y - cTexelY;
        tColor = texture2D(xTexSampler, tQuery);
        if(tColor.a != 0.0)
        {
            tRenderOutlineHere = true;
        }
        tQuery.y = tQuery.y + cTexelY;
    }
    //--Right check.
    if(tRenderOutlineHere == false)
    {
        tQuery.x = tQuery.x + cTexelX;
        tColor = texture2D(xTexSampler, tQuery);
        if(tColor.a != 0.0)
        {
            tRenderOutlineHere = true;
        }
        tQuery.x = tQuery.x - cTexelX;
    }
    //--Down check.
    if(tRenderOutlineHere == false)
    {
        tQuery.y = tQuery.y + cTexelY;
        tColor = texture2D(xTexSampler, tQuery);
        if(tColor.a != 0.0)
        {
            tRenderOutlineHere = true;
        }
        tQuery.y = tQuery.y - cTexelY;
    }

    //--Now override the color. Normal case...
    if(!tRenderOutlineHere)
    {
        gl_FragColor = tColor;
    }
    //--Grey case.
    else
    {
        gl_FragColor = uOutlineColor;
    }
}
